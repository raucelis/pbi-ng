import { __decorate, __awaiter } from 'tslib';
import { EventEmitter, HostBinding, HostListener, Input, Output, Component, Injectable, ElementRef, Renderer2, ViewChild, NgModule, ɵɵdefineInjectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DxTreeViewComponent, DxButtonModule, DxColorBoxModule, DxDataGridModule, DxDateBoxModule, DxDropDownBoxModule, DxDropDownButtonModule, DxFilterBuilderModule, DxListModule, DxMenuModule, DxNumberBoxModule, DxPopupModule, DxRadioGroupModule, DxScrollViewModule, DxSelectBoxModule, DxTemplateModule, DxTextBoxModule, DxTreeViewModule, DxValidationGroupModule, DxValidatorModule } from 'devextreme-angular';
import notify from 'devextreme/ui/notify';
import { Subject } from 'rxjs';
import { buffer, debounceTime } from 'rxjs/operators';
import DataSource from 'devextreme/data/data_source';
import Resizable from 'devextreme/ui/resizable';
import validationEngine from 'devextreme/ui/validation_engine';
import { confirm } from 'devextreme/ui/dialog';

let NavigationMenuComponent = class NavigationMenuComponent {
    constructor(router) {
        this.router = router;
        this._config = {
            showToggleButton: true,
            collapsed: true,
            sections: []
        };
        this.action = new EventEmitter();
    }
    get collapsed() {
        return this.config ? this.config.collapsed : true;
    }
    onDocumentClick(target) {
        let el = target.closest('pbi-navigation-menu');
        if (el === null)
            this.dismiss();
    }
    onKeyDown(event) {
        this.keyboardEvent = event;
    }
    onKeyUp(event) {
        this.keyboardEvent = null;
    }
    get config() {
        return this._config;
    }
    set config(v) {
        if (v) {
            this._config = v;
        }
    }
    get shiftKey() {
        return this.keyboardEvent && this.keyboardEvent.shiftKey;
    }
    get ctrlKey() {
        return this.keyboardEvent && this.keyboardEvent.ctrlKey;
    }
    get newTab() {
        return this.ctrlKey || this.shiftKey;
    }
    ngOnInit() { }
    toggle() {
        if (!this.config) {
            return;
        }
        this.config.collapsed = !this.config.collapsed;
    }
    dismiss() {
        this.activeSectionConfig = null;
        this.config.collapsed = true;
    }
    browse(config, event) {
        event.stopImmediatePropagation();
        if (this.isBrowsing(config)) {
            this.activeSectionConfig = null;
            return;
        }
        if (config.content) {
            this.activeSectionConfig = config;
        }
        else {
            this.navigateTo(config, this.newTab);
        }
    }
    isBrowsing(config) {
        return this.activeSectionConfig === config;
    }
    navigateTo(config, newTab) {
        this.dismiss();
        newTab ? this.windowNavigateTo(config) : this.ngNavigateTo(config);
        this.keyboardEvent = null;
    }
    handleAction(actionId) {
        this.dismiss();
        this.action.emit(actionId);
    }
    windowNavigateTo(config) {
        const link = config.link;
        const queryParams = config.queryParams;
        if (this.isExternalLink(link))
            this.openNewBrowserTab(link, queryParams);
        else
            this.openNewBrowserTab(this.config.useHash ? `#${link}` : link, queryParams);
    }
    ngNavigateTo(config) {
        const link = config.link;
        const queryParams = config.queryParams;
        if (this.isExternalLink(link))
            this.openNewBrowserTab(link, queryParams);
        else {
            //TODO: run validation checks if ever needed.
            queryParams ? this.router.navigate([link], { queryParams: queryParams }) : this.router.navigate([link]);
        }
    }
    isExternalLink(link) {
        return link.includes('http://') || link.includes('https://');
    }
    openNewBrowserTab(link, queryParams) {
        let l = this.config.href ? `${this.config.href}${link}` : `${link}`;
        if (queryParams) {
            let params = [];
            for (let property in queryParams) {
                if (!queryParams.hasOwnProperty(property)) {
                    continue;
                }
                params.push(`${property}=${queryParams[property]}`);
            }
            l += `?${params.join('&')}`;
        }
        this.shiftKey ? window.open(l) : window.open(l, '_blank');
    }
};
NavigationMenuComponent.ctorParameters = () => [
    { type: Router }
];
__decorate([
    HostBinding('class.collapsed')
], NavigationMenuComponent.prototype, "collapsed", null);
__decorate([
    HostListener('document:click', ['$event.target'])
], NavigationMenuComponent.prototype, "onDocumentClick", null);
__decorate([
    HostListener('document:keydown', ['$event'])
], NavigationMenuComponent.prototype, "onKeyDown", null);
__decorate([
    HostListener('document:keyup', ['$event'])
], NavigationMenuComponent.prototype, "onKeyUp", null);
__decorate([
    Input()
], NavigationMenuComponent.prototype, "config", null);
__decorate([
    Output()
], NavigationMenuComponent.prototype, "action", void 0);
NavigationMenuComponent = __decorate([
    Component({
        selector: 'pbi-navigation-menu',
        template: "<ng-template [ngIf]=\"config.showToggleButton\">\r\n    <div (click)=\"toggle()\" class=\"navigation-item toggle\">\r\n        <div>\r\n            <div>\r\n                <i class=\"fa fa-bars icon\"></i><span></span>\r\n            </div>\r\n            <div></div>\r\n        </div>\r\n    </div>\r\n</ng-template>\r\n<ng-template [ngIf]=\"config\">\r\n    <div *ngFor=\"let sectionConfig of config.sections\" class=\"navigation-item\">\r\n        <div [title]=\"sectionConfig.title\">\r\n            <div (click)=\"browse(sectionConfig, $event)\">\r\n                <i class=\"{{sectionConfig.icon}} icon\"></i><span class=\"text\">{{sectionConfig.title}}</span>\r\n            </div>\r\n            <div>\r\n                <div *ngIf=\"sectionConfig.openNewTab\" (click)=\"navigateTo(sectionConfig, true)\">\r\n                    <i class=\"fa fa-external-link icon\"></i>\r\n                </div>\r\n            </div>\r\n            <div>\r\n                <div *ngIf=\"sectionConfig.content\" (click)=\"browse(sectionConfig, $event)\">\r\n                    <i *ngIf=\"isBrowsing(sectionConfig); else notBrowsing\" class=\"fa fa-angle-down icon\"></i>\r\n                    <ng-template #notBrowsing>\r\n                        <i class=\"fa fa-angle-right icon\"></i>\r\n                    </ng-template>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ng-template [ngIf]=\"isBrowsing(sectionConfig)\">\r\n            <pbi-navigation-section [config]=\"sectionConfig\" (action)=\"handleAction($event)\" (navigateTo)=\"navigateTo($event, newTab)\" (navigateToNewTab)=\"navigateTo($event, true)\"></pbi-navigation-section>\r\n        </ng-template>\r\n    </div>\r\n</ng-template>"
    })
], NavigationMenuComponent);

let SearchService = class SearchService {
    /**
     * Returns elements of the array that contain the given string, case insensitive, anywhere in their object hierarchy.
     * @param input The array to search.
     * @param text The string to match, case insensitive.
     * @param exclude The list of properties to exclude from the search.
     */
    deepArrayContains(input, text, exclude) {
        if (!text) {
            return input;
        }
        let textLower = text.toLowerCase();
        let filtered = this.deepArrayFilter(input, val => val !== null && val !== undefined && val.toString().toLowerCase().indexOf(textLower) > -1, exclude);
        return Array.from(filtered);
    }
    /**
     * Returns elements of the array that match the provided function.
     * @param input The array to search.
     * @param filter The filter function to apply.
     */
    *deepArrayFilter(input, filter, exclude) {
        for (const item of input) {
            if (this.deepMatch(item, filter, exclude)) {
                yield item;
            }
        }
    }
    /**
     * Finds if any properties of the given object, or their children, match the given string, case insensitive.
     * @param obj The object to search.
     * @param text The string to match, case insensitive.
     */
    deepContains(obj, text) {
        if (!text) {
            return true;
        }
        const textLower = text.toLowerCase();
        return this.deepMatch(obj, val => val !== null && val !== undefined && val.toString().toLowerCase().indexOf(textLower) > -1);
    }
    /**
     * Finds if any properties of the given object, or their children, match the given filter.
     * @param obj The object to search.
     * @param filter The filter function to apply.
     */
    deepMatch(obj, filter, exclude) {
        for (const prop in obj) {
            if (!obj.hasOwnProperty(prop))
                continue;
            if (exclude && exclude.includes(prop))
                continue;
            const propVal = obj[prop];
            if (Array.isArray(propVal)) {
                for (const item of propVal) {
                    if (this.deepMatch(item, filter, exclude)) {
                        return true;
                    }
                }
            }
            else {
                if (propVal instanceof Date) {
                    let a = propVal.toLocaleDateString();
                    let b = a.split('/').join('-');
                    if (filter(a) || filter(b)) {
                        return true;
                    }
                }
                else if (filter(propVal)) {
                    return true;
                }
            }
        }
        return false;
    }
    treeWalk(arr, childPath, callback) {
        if (!arr) {
            return undefined;
        }
        for (let item of arr) {
            callback(item);
            const children = item[childPath];
            this.treeWalk(children, childPath, callback);
        }
    }
};
SearchService = __decorate([
    Injectable()
], SearchService);

let NavigationSectionComponent = class NavigationSectionComponent {
    constructor(searchService) {
        this.searchService = searchService;
        this.action = new EventEmitter();
        this.navigateTo = new EventEmitter();
        this.navigateToNewTab = new EventEmitter();
    }
    ;
    get contentConfig() {
        return this.config ?
            this.searchQuery ? this._filteredContentConfig : this.config.content :
            null;
    }
    get infoItems() {
        let items = [];
        if (this.config.informational && this.config.content.groups) {
            items = this.config.content.groups[0].items;
        }
        return items;
    }
    ngOnInit() { }
    search() {
        if (!this.searchQuery) {
            return;
        }
        let items = [];
        this.flattenTree(this.config.content, items);
        this._filteredContentConfig = {
            groups: [{
                    title: items.length > 0 ? 'results' : 'no matches',
                    items: items
                }]
        };
    }
    onActionClick(action) {
        this.action.emit(action.id);
    }
    onNavigateTo(config, newTab) {
        newTab ? this.navigateToNewTab.emit(config) : this.navigateTo.emit(config);
    }
    flattenTree(config, items) {
        config.groups.forEach(g => {
            g.items.forEach(i => {
                if (i.content) {
                    this.flattenTree(i.content, items);
                }
                else if (this.searchService.deepContains(i, this.searchQuery)) {
                    items.push(i);
                }
            });
        });
    }
};
NavigationSectionComponent.ctorParameters = () => [
    { type: SearchService }
];
__decorate([
    Input()
], NavigationSectionComponent.prototype, "config", void 0);
__decorate([
    Output()
], NavigationSectionComponent.prototype, "action", void 0);
__decorate([
    Output()
], NavigationSectionComponent.prototype, "navigateTo", void 0);
__decorate([
    Output()
], NavigationSectionComponent.prototype, "navigateToNewTab", void 0);
NavigationSectionComponent = __decorate([
    Component({
        selector: 'pbi-navigation-section',
        template: "<ng-template [ngIf]=\"config\">\r\n    <ng-template [ngIf]=\"config.searchable\">\r\n        <div class=\"search-box dark\">\r\n            <div class=\"form-group\" role=\"search\">\r\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"searchQuery\" name=\"search\" (keyup)=\"search()\" autocomplete=\"off\" />\r\n            </div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template [ngIf]=\"config.actions\">\r\n        <div class=\"actions\">\r\n            <div>\r\n                <div *ngFor=\"let action of config.actions\" class=\"navigation-action\" (click)=\"onActionClick(action)\">\r\n                    <i [class]=\"action.icon\" [title]=\"action.title\"></i>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template [ngIf]=\"config.informational\" [ngIfElse]=\"navigationContent\">\r\n        <div class=\"info\">\r\n            <div *ngFor=\"let item of infoItems\">{{item.title}}</div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template #navigationContent>\r\n        <pbi-navigation-content [config]=\"contentConfig\" (navigateTo)=\"onNavigateTo($event)\" (navigateToNewTab)=\"onNavigateTo($event, true)\"></pbi-navigation-content>\r\n    </ng-template>\r\n</ng-template>"
    })
], NavigationSectionComponent);

let NavigationContentComponent = class NavigationContentComponent {
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.navigateTo = new EventEmitter();
        this.navigateToNewTab = new EventEmitter();
        this.activeItemConfig = null;
    }
    set config(v) {
        this._config = v;
        this.computeContentMaxHeight();
    }
    get config() {
        return this._config;
    }
    set top(v) {
        this.renderer.setStyle(this.el.nativeElement, 'top', `${v}px`);
        this.computeContentMaxHeight();
    }
    get browsing() {
        return !!this.activeItemConfig;
    }
    get contentTop() {
        return this._contentTop;
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.computeContentMaxHeight();
    }
    browse(config, event) {
        event.stopImmediatePropagation();
        if (this.isBrowsing(config)) {
            this.activeItemConfig = null;
            return;
        }
        if (config.content) {
            const target = event.target;
            const p = target.closest('.navigation-item');
            this._contentTop = p.offsetTop - this.items.nativeElement.scrollTop;
            this.activeItemConfig = config;
        }
        else {
            this.onNavigateTo(config);
        }
    }
    browseOrNavigate(config, event) {
        config.link ? this.onNavigateTo(config) : this.browse(config, event);
    }
    isBrowsing(config) {
        return this.activeItemConfig === config;
    }
    onNavigateTo(config, newTab) {
        this.activeItemConfig = null;
        newTab ? this.navigateToNewTab.emit(config) : this.navigateTo.emit(config);
    }
    computeContentMaxHeight() {
        if (!this.items) {
            return;
        }
        this.activeItemConfig = null;
        let top = this.el.nativeElement.offsetTop;
        let parent = this.el.nativeElement.offsetParent;
        while (parent) {
            top += parent.offsetTop;
            parent = parent.offsetParent;
        }
        const maxHeight = window.innerHeight - top;
        this.renderer.setStyle(this.items.nativeElement, 'max-height', `${maxHeight}px`);
    }
};
NavigationContentComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
__decorate([
    Input()
], NavigationContentComponent.prototype, "config", null);
__decorate([
    Input()
], NavigationContentComponent.prototype, "top", null);
__decorate([
    HostBinding('class.browsing')
], NavigationContentComponent.prototype, "browsing", null);
__decorate([
    Output()
], NavigationContentComponent.prototype, "navigateTo", void 0);
__decorate([
    Output()
], NavigationContentComponent.prototype, "navigateToNewTab", void 0);
__decorate([
    ViewChild('items')
], NavigationContentComponent.prototype, "items", void 0);
NavigationContentComponent = __decorate([
    Component({
        selector: 'pbi-navigation-content',
        template: "<div #items class=\"items\">\r\n    <div *ngFor=\"let groupConfig of config.groups\" class=\"navigation-group\">\r\n        <span *ngIf=\"groupConfig.title\">{{groupConfig.title}}</span>\r\n        <div *ngFor=\"let itemConfig of groupConfig.items\"  class=\"navigation-item\">\r\n            <div [title]=\"itemConfig.title\">\r\n                <div (click)=\"browseOrNavigate(itemConfig, $event)\">\r\n                    <i class=\"{{itemConfig.icon}} icon\"></i>\r\n                    <span class=\"text\">{{itemConfig.title}}</span>\r\n                </div>\r\n                <div>\r\n                    <div *ngIf=\"itemConfig.openNewTab\" (click)=\"onNavigateTo(itemConfig, true)\">\r\n                        <i class=\"fa fa-external-link icon\"></i>\r\n                    </div>\r\n                </div>\r\n                <div>\r\n                    <div *ngIf=\"itemConfig.content\" (click)=\"browse(itemConfig, $event)\">\r\n                        <i *ngIf=\"isBrowsing(itemConfig); else notBrowsing\" class=\"fa fa-angle-down icon\"></i>\r\n                        <ng-template #notBrowsing>\r\n                            <i class=\"fa fa-angle-right icon\"></i>\r\n                        </ng-template>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <ng-template [ngIf]=\"isBrowsing(itemConfig)\">\r\n                <pbi-navigation-content class=\"child\" \r\n                    [config]=\"itemConfig.content\" \r\n                    (navigateTo)=\"onNavigateTo($event)\" \r\n                    (navigateToNewTab)=\"onNavigateTo($event, true)\">\r\n                </pbi-navigation-content>\r\n            </ng-template>\r\n        </div>\r\n    </div>\r\n</div>\r\n<ng-template [ngIf]=\"activeItemConfig\">\r\n    <pbi-navigation-content \r\n        class=\"flyout\"\r\n        [config]=\"activeItemConfig.content\"\r\n        [top]=\"contentTop\" \r\n        (navigateTo)=\"onNavigateTo($event)\" \r\n        (navigateToNewTab)=\"onNavigateTo($event, true)\">\r\n    </pbi-navigation-content>\r\n</ng-template> "
    })
], NavigationContentComponent);

const components = [
    NavigationMenuComponent,
    NavigationSectionComponent,
    NavigationContentComponent
];
let PortfolioBICoreModule = class PortfolioBICoreModule {
};
PortfolioBICoreModule = __decorate([
    NgModule({
        declarations: components,
        imports: [
            CommonModule,
            FormsModule
        ],
        exports: components,
        providers: [SearchService]
    })
], PortfolioBICoreModule);

var CGFFlyOutEnum;
(function (CGFFlyOutEnum) {
    CGFFlyOutEnum[CGFFlyOutEnum["none"] = -1] = "none";
    CGFFlyOutEnum[CGFFlyOutEnum["refreshData"] = 1] = "refreshData";
    CGFFlyOutEnum[CGFFlyOutEnum["filter"] = 2] = "filter";
    CGFFlyOutEnum[CGFFlyOutEnum["columnChooser"] = 3] = "columnChooser";
    CGFFlyOutEnum[CGFFlyOutEnum["customColumns"] = 4] = "customColumns";
    CGFFlyOutEnum[CGFFlyOutEnum["viewSelection"] = 5] = "viewSelection";
    CGFFlyOutEnum[CGFFlyOutEnum["gridSettings"] = 6] = "gridSettings";
    CGFFlyOutEnum[CGFFlyOutEnum["conditionFormatting"] = 7] = "conditionFormatting";
    CGFFlyOutEnum[CGFFlyOutEnum["newTab"] = 8] = "newTab"; // for open url in new tab
})(CGFFlyOutEnum || (CGFFlyOutEnum = {}));
var CGFEventsEnum;
(function (CGFEventsEnum) {
    CGFEventsEnum[CGFEventsEnum["addUnboundColumns"] = 0] = "addUnboundColumns";
    CGFEventsEnum[CGFEventsEnum["applyEntityParameters"] = 1] = "applyEntityParameters";
    CGFEventsEnum[CGFEventsEnum["cancelEntityParameters"] = 2] = "cancelEntityParameters";
    CGFEventsEnum[CGFEventsEnum["deleteUnboundColumns"] = 3] = "deleteUnboundColumns";
    CGFEventsEnum[CGFEventsEnum["editSecurity"] = 4] = "editSecurity";
    CGFEventsEnum[CGFEventsEnum["getCachedData"] = 5] = "getCachedData";
    CGFEventsEnum[CGFEventsEnum["refreshEntityData"] = 6] = "refreshEntityData";
    CGFEventsEnum[CGFEventsEnum["refreshTheme"] = 7] = "refreshTheme";
    CGFEventsEnum[CGFEventsEnum["saveParameterOrder"] = 8] = "saveParameterOrder";
    // layout related enums
    CGFEventsEnum[CGFEventsEnum["addNewView"] = 9] = "addNewView";
    CGFEventsEnum[CGFEventsEnum["applyView"] = 10] = "applyView";
    CGFEventsEnum[CGFEventsEnum["cloneView"] = 11] = "cloneView";
    CGFEventsEnum[CGFEventsEnum["deleteView"] = 12] = "deleteView";
    CGFEventsEnum[CGFEventsEnum["getViews"] = 13] = "getViews";
    CGFEventsEnum[CGFEventsEnum["saveAsView"] = 14] = "saveAsView";
    CGFEventsEnum[CGFEventsEnum["saveSelectedView"] = 15] = "saveSelectedView";
    CGFEventsEnum[CGFEventsEnum["updateViews"] = 16] = "updateViews";
    CGFEventsEnum[CGFEventsEnum["updateViewVisibility"] = 17] = "updateViewVisibility";
    CGFEventsEnum[CGFEventsEnum["updateLayouts"] = 18] = "updateLayouts";
})(CGFEventsEnum || (CGFEventsEnum = {}));
var CGFStorageKeys;
(function (CGFStorageKeys) {
    CGFStorageKeys[CGFStorageKeys["conditionalFormatting"] = 0] = "conditionalFormatting";
    CGFStorageKeys[CGFStorageKeys["dictionaryFormatData"] = 1] = "dictionaryFormatData";
    CGFStorageKeys[CGFStorageKeys["formatData"] = 2] = "formatData";
    CGFStorageKeys[CGFStorageKeys["childGridView"] = 3] = "childGridView";
})(CGFStorageKeys || (CGFStorageKeys = {}));
var CGFSettingsEnum;
(function (CGFSettingsEnum) {
    CGFSettingsEnum[CGFSettingsEnum["theme"] = 1] = "theme";
    CGFSettingsEnum[CGFSettingsEnum["gridLines"] = 2] = "gridLines";
    CGFSettingsEnum[CGFSettingsEnum["autoFit"] = 3] = "autoFit";
    CGFSettingsEnum[CGFSettingsEnum["expAllData"] = 4] = "expAllData";
    CGFSettingsEnum[CGFSettingsEnum["expExcelLink"] = 5] = "expExcelLink";
    CGFSettingsEnum[CGFSettingsEnum["expEmailReport"] = 6] = "expEmailReport";
    CGFSettingsEnum[CGFSettingsEnum["advDem"] = 7] = "advDem";
})(CGFSettingsEnum || (CGFSettingsEnum = {}));
var CGFFeatureUpdateEnum;
(function (CGFFeatureUpdateEnum) {
    CGFFeatureUpdateEnum[CGFFeatureUpdateEnum["clear"] = 0] = "clear";
    CGFFeatureUpdateEnum[CGFFeatureUpdateEnum["customColumnUpdated"] = 1] = "customColumnUpdated";
    CGFFeatureUpdateEnum[CGFFeatureUpdateEnum["cachedData"] = 2] = "cachedData";
})(CGFFeatureUpdateEnum || (CGFFeatureUpdateEnum = {}));
var SecurityMasterAction;
(function (SecurityMasterAction) {
    SecurityMasterAction[SecurityMasterAction["AddSecurity"] = 0] = "AddSecurity";
    SecurityMasterAction[SecurityMasterAction["EditSecurity"] = 1] = "EditSecurity";
})(SecurityMasterAction || (SecurityMasterAction = {}));

const ɵ0 = CGFFlyOutEnum.refreshData, ɵ1 = CGFFlyOutEnum.filter, ɵ2 = CGFFlyOutEnum.columnChooser, ɵ3 = CGFFlyOutEnum.viewSelection, ɵ4 = CGFFlyOutEnum.gridSettings, ɵ5 = CGFFlyOutEnum.conditionFormatting, ɵ6 = CGFFlyOutEnum.newTab;
const cgfFlyOutList = [
    {
        id: ɵ0,
        src: 'far fa-sync-alt',
        title: 'Refresh Data',
        isVisible: false,
        enableLoader: true
    },
    {
        id: ɵ1,
        src: 'fas fa-filter',
        title: 'Filters',
        isVisible: false,
    },
    {
        id: ɵ2,
        src: 'fas fa-list',
        title: 'Columns',
        isVisible: false
    },
    {
        id: ɵ3,
        src: 'fas fa-columns',
        title: 'View Selection',
        isVisible: false
    },
    {
        id: ɵ4,
        src: 'fas fa-cogs',
        title: 'Settings',
        isVisible: false
    },
    {
        id: ɵ5,
        src: 'fas fa-tools',
        title: 'Conditional Formatting',
        isVisible: false
    },
    {
        id: ɵ6,
        src: 'fas fa-external-link-alt',
        title: 'Opens a new tab',
        isVisible: true
    }
];
const columnFormattingOptions = {
    format: [
        {
            type: 'currency',
            key: 'currency',
            title: 'Dollar',
            icon: 'fas fa-dollar-sign',
            isSelected: false
        },
        {
            type: 'percentage',
            key: 'percentage',
            title: 'Percentage',
            icon: 'fas fa-percent',
            isSelected: false
        },
        {
            type: 'comma',
            key: 'comma',
            title: 'Comma',
            icon: 'fad fa-quote-right',
            isSelected: false
        },
        {
            type: 'removeDecimal',
            key: 'removeDecimal',
            title: 'Remove Decimal',
            icon: 'fas fa-greater-than',
            isSelected: false
        },
        {
            type: 'addDecimal',
            key: 'addDecimal',
            title: 'Add Decimal',
            icon: 'fas fa-less-than',
            isSelected: false
        }
    ],
    alignment: [
        {
            type: 'left',
            key: 'leftAlignment',
            title: 'Left Alignment',
            icon: 'fas fa-align-left',
            isSelected: false
        },
        {
            type: 'center',
            key: 'centerAlignment',
            title: 'Center Alignment',
            icon: 'fas fa-align-center',
            isSelected: false
        },
        {
            type: 'right',
            key: 'rightAlignment',
            title: 'Right Alignment',
            icon: 'fas fa-align-right',
            isSelected: false
        }
    ],
    pin: [
        {
            type: 'left',
            key: 'leftPin',
            title: 'Left Pin',
            icon: 'fas fa-chart-bar left-pin',
            isSelected: false
        },
        {
            type: 'right',
            key: 'rightPin',
            title: 'Right Pin',
            icon: 'fas fa-chart-bar right-pin',
            isSelected: false
        }
    ],
    fontSize: '',
    fontStyle: [
        {
            type: 'bold',
            key: 'bold',
            title: 'Bold',
            icon: 'fas fa-bold',
            isSelected: false
        },
        {
            type: 'underline',
            key: 'underline',
            title: 'Underline',
            icon: 'fas fa-underline',
            isSelected: false
        },
        {
            type: 'italic',
            key: 'italic',
            title: 'Italic',
            icon: 'fas fa-italic',
            isSelected: false
        }
    ]
};
const rowTypeInfo = {
    data: 'data',
    group: 'group',
    totalFooter: 'totalFooter'
};
const customActionColumnInfo = {
    dataField: '_row_actions_column_',
    groupName: '_row_actions_group_',
    caption: 'Actions'
};
const crudOperation = {
    add: 'add',
    edit: 'edit',
    clone: 'clone'
};
const ɵ7 = CGFSettingsEnum.expAllData, ɵ8 = CGFSettingsEnum.expExcelLink, ɵ9 = CGFSettingsEnum.expEmailReport;
const SettingExportOptions = [
    { id: ɵ7, key: 'exportExcel', icon: 'fas fa-align-left rotate-270deg', title: 'All Data' },
    { id: ɵ8, key: 'excelLink', icon: 'fas fa-file-excel', title: 'Excel Link' },
    { id: ɵ9, key: 'emailReport', icon: 'fas fa-envelope', title: 'Email Report' },
];
const actionTypes = {
    entity: 'Entity',
    url: 'URL',
    addSecurity: 'Add Security',
    editSecurity: 'Edit Security',
    childEntity: 'Child Entity',
    dispatcherJob: 'Dispatcher Job',
    lookup: 'Lookup',
    securityType: 'Security Type'
};
// tslint:disable-next-line: one-variable-per-declaration
const today = new Date(), y = today.getFullYear(), m = today.getMonth();
const localization = { usLocale: 'en-US' };
const dateShortcuts = [
    { Id: '$Today$', Name: `Today ${today.toLocaleDateString(localization.usLocale)}`, Value: today.toLocaleDateString(localization.usLocale), IsSelected: false },
    {
        Id: '$Yesterday$', Name: `Yesterday ${new Date(today.setDate(today.getDate() - 1)).toLocaleDateString(localization.usLocale)}`,
        Value: today.toLocaleDateString(localization.usLocale), IsSelected: false
    },
    { Id: '$SOM$', Name: `Start of Month ${new Date(y, m, 1).toLocaleDateString(localization.usLocale)}`, Value: new Date(y, m, 1).toLocaleDateString(localization.usLocale), IsSelected: false },
    {
        Id: '$EOM$', Name: `End of Previous Month ${new Date(y, m, 0).toLocaleDateString(localization.usLocale)}`,
        Value: new Date(y, m, 0).toLocaleDateString(localization.usLocale), IsSelected: false
    },
    { Id: '$SOY$', Name: `Start of Year  ${new Date(y, 0, 1).toLocaleDateString(localization.usLocale)}`, Value: new Date(y, 0, 1).toLocaleDateString(localization.usLocale), IsSelected: false },
    {
        Id: '$EOY$', Name: `End of Previous Year ${new Date(y - 1, 11, 31).toLocaleDateString(localization.usLocale)}`,
        Value: new Date(y - 1, 11, 31).toLocaleDateString(localization.usLocale), IsSelected: false
    },
];

// this is duplicate function specific to models.
function getStorageKey(gridInstance, keyValue, isMasterGrid = true) {
    const masterDetailInfo = gridInstance === null || gridInstance === void 0 ? void 0 : gridInstance.option('masterDetail');
    return isMasterGrid ? keyValue : `${keyValue}_${masterDetailInfo.template}`;
}
function getClassNameByThemeName(themeName) {
    switch (themeName) {
        case 'np.compact':
        case 'dx-swatch-default':
            return 'dx-swatch-default';
        case 'light.compact':
        case 'dx-swatch-regular':
            return 'dx-swatch-regular';
        case 'light.regular':
        default:
            return '';
    }
}
function applyFilterCssClass(filter = [], gridInstance) {
    let i, cssClasses, columnInfo;
    for (i = 0; filter && i < filter.length; i++) {
        if (Array.isArray(filter[i])) {
            if (Array.isArray(filter[i][0])) {
                applyFilterCssClass(filter[i], gridInstance);
            }
            else {
                columnInfo = gridInstance.columnOption(filter[i][0]);
                if (columnInfo && (columnInfo.dataField === filter[i][0])) {
                    cssClasses = gridInstance.columnOption(filter[i][0], 'cssClass');
                    gridInstance.columnOption(filter[i][0], 'cssClass', cssClasses + ' filterApplied');
                }
            }
        }
        else {
            columnInfo = gridInstance.columnOption(filter[i]);
            if (columnInfo && (columnInfo.dataField === filter[i])) {
                cssClasses = gridInstance.columnOption(filter[i], 'cssClass');
                gridInstance.columnOption(filter[i], 'cssClass', cssClasses ? cssClasses + ' filterApplied' : 'filterApplied');
            }
        }
    }
}
function applyFiltersToGrid(gridInstance, filterValue) {
    if (gridInstance) {
        // const listOfVisibleColumns = gridInstance.getVisibleColumns();
        // let cssClasses: string;
        // for (let i = 0; i < listOfVisibleColumns.length; i++) {
        //     cssClasses = gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass');
        //     if (cssClasses) {
        //         gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass', cssClasses.replace(new RegExp('filterApplied', 'g'), '').trim());
        //     }
        // }
        if (filterValue) {
            // applyFilterCssClass(filterValue, gridInstance);
            const alertOptions = {
                type: 'warning', message: 'To permanently apply this filter,please go to the \'View Selection\' and click \'Save\''
                    + ' in order to associate this filter with a view.'
            };
            appToast(alertOptions);
        }
    }
}
function appToast(options) {
    const defaultOptions = {
        message: 'Oops.Something went wrong !',
        closeOnClick: true,
        width: '550px',
        type: 'error',
        displayTime: 3000,
        position: {
            my: 'center top',
            at: 'center top'
        },
    };
    // * extend the below switch for other toast(notify) types
    if (typeof options.type === 'string') {
        switch (options.type.toLowerCase()) {
            case 'warning':
                defaultOptions.displayTime = 10000;
                break;
            default:
                defaultOptions.displayTime = 3000;
                break;
        }
    }
    options = Object.assign({}, defaultOptions, options);
    notify(options);
}
function ToDictionary(array, key) {
    const dic = {};
    array.forEach(e => {
        const k = key(e);
        if (!dic.hasOwnProperty(k)) {
            dic[key(e)] = e;
        }
        else {
            console.warn('Duplicate Key in Array', e);
        }
        ;
    });
    return dic;
}
function filterSelectedRowData(gridInstance, rowData) {
    const visibleColumns = gridInstance.getVisibleColumns().map((item) => {
        return item['dataField'];
    });
    let selectedRowData = [];
    if (rowData && Object.keys(rowData).length && Object.keys(rowData.data).length) {
        selectedRowData.push(JSON.parse(JSON.stringify(rowData.data)));
    }
    else {
        selectedRowData = JSON.parse(JSON.stringify(gridInstance.getSelectedRowsData()));
    }
    if (selectedRowData.length) {
        for (const columnProperty in selectedRowData[0]) {
            if (!visibleColumns.includes(columnProperty)) {
                delete selectedRowData[0][columnProperty];
            }
        }
    }
    return selectedRowData;
}
function openDestination(data, selectedRowData, entityParameterInfo = [], appService = null) {
    var _a, _b;
    let urlDynamicParamCount = 0, paramFilter = [], url, urlParams, paramterMapping, delimiter, destinationUrl, staticParams;
    let undefinedSourceColumnNames = '';
    const comma = ',';
    switch ((_a = data.DestinationType) === null || _a === void 0 ? void 0 : _a.toLowerCase().trim()) {
        case actionTypes.addSecurity.toLowerCase():
            if (appService) {
                appService.showSM({ SecurityId: null, Action: SecurityMasterAction.AddSecurity });
            }
            return;
        case actionTypes.dispatcherJob.toLowerCase():
            url = `${location.href.split('#')[0]}#/DispatcherManagement?webMenuId=${getWebMenuIdByKey('DispatcherManagement')}&jobId=${data.jobId}`;
            open(url, '_blank');
            return;
        case actionTypes.lookup.toLowerCase():
            url = `${location.href.split('#')[0]}#/LookupTables?webMenuId=${getWebMenuIdByKey('LookupTables')}&lookupId=${data.LookupId}`;
            open(url, '_blank');
            return;
        case actionTypes.entity.toLowerCase():
        case actionTypes.editSecurity.toLowerCase():
        case actionTypes.url.toLowerCase():
            if (selectedRowData && selectedRowData.length) {
                switch ((_b = data.DestinationType) === null || _b === void 0 ? void 0 : _b.toLowerCase()) {
                    case actionTypes.editSecurity.toLowerCase():
                        if (data.ShowAsContextMenu) {
                            return true;
                        }
                        else {
                            let secKey = '';
                            for (const key in selectedRowData[0]) {
                                if (selectedRowData[0].hasOwnProperty(key) && (key.toLowerCase() === 'securityid' || key.toLowerCase() === 'security id')) {
                                    secKey = key;
                                }
                            }
                            if (selectedRowData[0][secKey] && appService) {
                                appService.showSM({ SecurityId: selectedRowData[0][secKey], Action: SecurityMasterAction.EditSecurity });
                                return true;
                            }
                            else {
                                appToast({ type: 'error', message: `Following column(s) Security Id is required. Please make sure same exist(s) in the grid with value.`, displayTime: 4000 });
                                return false;
                            }
                        }
                        break;
                    case actionTypes.entity.toLowerCase():
                        const queryParams = location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                        queryParams['entity'] = data.DestinationEntity;
                        queryParams['webMenuId'] = data.DestinationWebMenuItemId;
                        queryParams['layout'] = 0;
                        queryParams['gridname'] = data.DestinationEntityName + '_' + data.DestinationEntity;
                        paramterMapping = data.ParameterMapping;
                        const paramInfo = extractDestinationEntityParameters(selectedRowData, paramterMapping, entityParameterInfo);
                        undefinedSourceColumnNames = paramInfo.undefinedSourceColumnNames;
                        url = location.hash.split('?')[0] + '?webMenuId=' + queryParams['webMenuId'] + '&entity=' + queryParams['entity'] +
                            '&gridname=' + queryParams['gridname'] + paramInfo.param + '&layout=' + queryParams['layout'];
                        break;
                    case actionTypes.url.toLowerCase():
                        destinationUrl = data.DestinationUrl.split('?'), paramterMapping = data.ParameterMapping;
                        url = destinationUrl[0], staticParams = destinationUrl[1];
                        if (staticParams !== undefined) {
                            url = url + '?' + staticParams;
                        }
                        urlParams = url.match(/{[^{}]*}/gi);
                        if (urlParams != null) {
                            urlParams.forEach((item) => {
                                paramterMapping.forEach((paramItem) => {
                                    if (paramItem.ParameterName === item.substr(1).slice(0, -1)) {
                                        if (paramItem.IsSourceParam) {
                                            const sourceParamName = paramItem.SourceColumnName.split('@')[1].trim().toLowerCase();
                                            const parameter = entityParameterInfo.filter(par => par.Name.trim().toLowerCase() === sourceParamName
                                                || par.Name.trim().toLowerCase() === paramItem.SourceColumnName.trim().toLowerCase())[0];
                                            if (parameter.Value) {
                                                url = url.replace(item, checkAndFormatDate(parameter.Value));
                                            }
                                            else {
                                                undefinedSourceColumnNames += paramItem.SourceColumnName + comma;
                                            }
                                        }
                                        else {
                                            if (selectedRowData[0][item.SourceColumnName] || selectedRowData[0][paramItem.SourceColumnName]) {
                                                url = url.replace(item, checkAndFormatDate(selectedRowData[0][paramItem.SourceColumnName]));
                                            }
                                            else {
                                                undefinedSourceColumnNames += item.SourceColumnName ? item.SourceColumnName + comma : item + comma;
                                            }
                                        }
                                    }
                                });
                            });
                        }
                        paramterMapping.forEach((item, index) => {
                            if (urlParams != null) {
                                paramFilter = urlParams.filter((urlItem) => item.ParameterName === urlItem.substr(1).slice(0, -1));
                            }
                            if (!paramFilter.length) {
                                delimiter = (urlDynamicParamCount === 0 && staticParams === undefined) ? '?' : '&';
                                if (selectedRowData[0][item.SourceColumnName]) {
                                    url = url + delimiter + item.ParameterName + '=' + checkAndFormatDate(selectedRowData[0][item.SourceColumnName]);
                                }
                                else {
                                    undefinedSourceColumnNames += item.SourceColumnName + comma;
                                }
                                urlDynamicParamCount++;
                            }
                        });
                        break;
                }
                if (!undefinedSourceColumnNames) {
                    open(url, '_blank');
                }
                else {
                    if (undefinedSourceColumnNames.endsWith(comma)) {
                        undefinedSourceColumnNames = undefinedSourceColumnNames.substring(0, undefinedSourceColumnNames.length - 1);
                        appToast({
                            type: 'error', message: `Following column(s) ${undefinedSourceColumnNames} is required. Please make sure same exist(s) in the grid or parameter with value.`,
                            displayTime: 4000
                        });
                    }
                }
            }
            else {
                appToast({ type: 'warning', message: 'Please select a row.' });
            }
    }
    return false;
}
function extractDestinationEntityParameters(selectedRowData, parameterMapping, entityParameterInfo) {
    let param = '';
    let undefinedSourceColumnNames = '';
    const comma = ',';
    parameterMapping.forEach((item) => {
        const paramName = item.OriginalParameterName || item.ParameterName;
        if (item.IsSourceParam) {
            const sourceParamName = item.SourceColumnName.split('@')[1].trim().toLowerCase();
            const parameter = entityParameterInfo.filter(par => par.Name.trim().toLowerCase() === sourceParamName
                || par.Name.trim().toLowerCase() === item.SourceColumnName.trim().toLowerCase())[0];
            if (parameter === null || parameter === void 0 ? void 0 : parameter.Value) {
                param = param + '&' + paramName + '=' + checkAndFormatDate(parameter.Value);
            }
            else {
                undefinedSourceColumnNames += item.SourceColumnName + comma;
            }
        }
        else {
            if (selectedRowData[0][item.SourceColumnName]) {
                param = param + '&' + paramName + '=' + checkAndFormatDate(selectedRowData[0][item.SourceColumnName]);
            }
            else {
                undefinedSourceColumnNames += item.SourceColumnName + comma;
            }
        }
    });
    return { param, undefinedSourceColumnNames };
}
function convertDateToUsFormat(value) {
    const dateValue = new Date(value);
    let dd = dateValue.getDate();
    let mm = dateValue.getMonth() + 1;
    const yyyy = dateValue.getFullYear();
    dd = dd < 10 ? '0' + dd : dd;
    mm = mm < 10 ? '0' + mm : mm;
    return `${mm}/${dd}/${yyyy}`;
}
/**
 * this method will return web menu id based on key
 * @param  {string} key
 * @returns number
 */
function getWebMenuIdByKey(key) {
    const webMenus = window['menuData'] || [];
    if (webMenus.length) {
        const filteredItem = webMenus.filter((arrItem) => { var _a; return ((_a = arrItem.Key) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === key.toLowerCase(); })[0];
        return filteredItem ? filteredItem.OptomasToolId : undefined;
    }
}
// convert from YYYY-MM-DDTHH:mm:ss.SSS to MM/DD/YYYY, else return the param as it is
function checkAndFormatDate(value) {
    // const moment = window['moment'];
    // // ref: https://flaviocopes.com/momentjs/
    // if (moment(value, moment.HTML5_FMT.DATETIME_LOCAL_MS, true).isValid()) {
    //     return moment(value).format('MM/DD/YYYY');
    // } else {
    //     return value;
    // }
    return value;
}

let CGFUtilityService = class CGFUtilityService {
    constructor() { }
    applyFiltersToGrid(gridInstance, filterValue) {
        if (gridInstance) {
            const listOfVisibleColumns = gridInstance.getVisibleColumns();
            let cssClasses;
            for (let i = 0; i < listOfVisibleColumns.length; i++) {
                cssClasses = gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass');
                if (cssClasses) {
                    gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass', cssClasses.replace(new RegExp('filterApplied', 'g'), '').trim());
                }
            }
            if (filterValue) {
                this.applyFilterClass(filterValue, gridInstance);
                const alertOptions = {
                    type: 'warning', message: 'To permanently apply this filter,please go to the \'View Selection\' and click \'Save\''
                        + ' in order to associate this filter with a view.'
                };
                appToast(alertOptions);
            }
        }
    }
    applyFilterClass(filter = [], gridInstance) {
        let i;
        let cssClasses;
        let columnInfo;
        for (i = 0; filter && i < filter.length; i++) {
            if (Array.isArray(filter[i])) {
                if (Array.isArray(filter[i][0])) {
                    this.applyFilterClass(filter[i], gridInstance);
                }
                else {
                    columnInfo = gridInstance.columnOption(filter[i][0]);
                    if (columnInfo && (columnInfo.dataField === filter[i][0])) {
                        cssClasses = gridInstance.columnOption(filter[i][0], 'cssClass');
                        gridInstance.columnOption(filter[i][0], 'cssClass', cssClasses + ' filterApplied');
                    }
                }
            }
            else {
                columnInfo = gridInstance.columnOption(filter[i]);
                if (columnInfo && (columnInfo.dataField === filter[i])) {
                    cssClasses = gridInstance.columnOption(filter[i], 'cssClass');
                    gridInstance.columnOption(filter[i], 'cssClass', cssClasses ? cssClasses + ' filterApplied' : 'filterApplied');
                }
            }
        }
    }
    getStorageKey(gridInstance, keyValue, isMasterGrid = true) {
        const masterDetailInfo = gridInstance === null || gridInstance === void 0 ? void 0 : gridInstance.option('masterDetail');
        return isMasterGrid ? keyValue : `${keyValue}_${masterDetailInfo.template}`;
    }
};
CGFUtilityService.ɵprov = ɵɵdefineInjectable({ factory: function CGFUtilityService_Factory() { return new CGFUtilityService(); }, token: CGFUtilityService, providedIn: "root" });
CGFUtilityService = __decorate([
    Injectable({ providedIn: 'root' })
], CGFUtilityService);

let ColorFormatComponent = class ColorFormatComponent {
    constructor(utilityService) {
        this.utilityService = utilityService;
        this.enableAlignment = true;
        this.enableBackgroundColor = true;
        this.enableFontStyle = true;
        this.enableFormat = true;
        this.enableFormatToggle = true;
        this.enableInputCaptionField = true;
        this.enableInputDataFieldCheck = false;
        this.enablePin = true;
        this.enableStorage = true;
        this.enableTextColor = true;
        this.gridInstanceList = [];
        this.isMasterGridSelected = true;
        this.showBasicFormat = false;
        this.storageKey = CGFStorageKeys[CGFStorageKeys.formatData];
        this.formatOptionsChanged = new EventEmitter();
        this.formatToggled = new EventEmitter();
        this.backgroundColor = '';
        this.basicFormatIconsAndOptionsData = columnFormattingOptions;
        this.captionValidationPattern = /^[a-zA-Z0-9_\s]+$/;
        this.columnsActivePropertiesTabsList = [{ title: 'FORMATTING' }];
        this.decimalCounter = 0;
        this.decimalTypes = ['comma', 'percent', 'currency'];
        this.disableDecimalPrecision = false;
        this.formatObject = {};
        this.isColorReadOnly = true;
        this.textColor = '';
    }
    set disableDecimalPrecisionInput(data) {
        this.disableDecimalPrecision = data;
        this.basicFormatIconsAndOptionsData = this.disableDecimalPrecision ? this.removePrecisionControls(columnFormattingOptions) : columnFormattingOptions;
        this.isColorReadOnly = !this.disableDecimalPrecision;
    }
    get disableDecimalPrecisionInput() { return this.disableDecimalPrecision; }
    set selectedColumnInfo(columnItem) {
        var _a;
        this.canUpdateColumnFormatting = false;
        this.selectedColumnData = columnItem || { caption: '', dataField: '' };
        if (((_a = this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField.length) > 0) {
            this.prepareSelectedOption();
        }
        else {
            this.resetAllFormatOptions();
        }
    }
    get selectedColumnInfo() { return this.selectedColumnData; }
    ngOnInit() {
        this.resetBasicSettings();
        if (this.selectedColumnData.dataField && this.selectedColumnData.dataField.length > 0) {
            this.prepareSelectedOption();
        }
    }
    //#region Public Methods
    onCaptionValueChanged(args) {
        if (args.event) {
            this.canUpdateColumnFormatting = true;
        }
        this.formatObject.caption = this.selectedColumnInfo.caption || '';
        const data = {
            caption: this.selectedColumnInfo.caption || ''
        };
        if (data.caption.match(this.captionValidationPattern)) {
            this.formatOptionsChanged.emit(data);
            this.onFormatOptionsChanged();
        }
        // else {
        //   this.selectedColumnInfo.caption = args && args.previousValue ? args.previousValue : '';
        // }
    }
    onChangeOfTextColor() {
        this.formatObject.textColor = this.textColor || '';
        this.onFormatOptionsChanged();
    }
    onChangeOfBackgroundColor() {
        this.formatObject.backgroundColor = this.backgroundColor || '';
        this.onFormatOptionsChanged();
    }
    onColorFocusIn() {
        if (this.isColorReadOnly) {
            appToast({ type: 'warning', message: 'Column Format: Please select a column.' });
        }
        this.canUpdateColumnFormatting = true;
    }
    formatColumn(args, event) {
        if (this.selectedColumnData.dataField === customActionColumnInfo.dataField ||
            ((!this.selectedColumnData.dataField) || (this.selectedColumnData.dataField && this.selectedColumnData.dataField.length === 0))
                && this.enableInputDataFieldCheck) {
            appToast({ type: 'warning', message: 'Column Format: Please select a column.' });
            return;
        }
        if (args.type === 'right') {
            if (this.gridInstanceList.length) {
                this.gridInstanceList.forEach(grid => {
                    grid.columnOption(customActionColumnInfo.dataField, { fixed: true, fixedPosition: 'right' });
                });
            }
        }
        this.prepareLoaderLocation(event);
        args.isSelected = !args.isSelected;
        if (event) {
            this.canUpdateColumnFormatting = true;
        }
        if (!this.selectFormat(args)) {
            args.isSelected = !args.isSelected;
        }
        if (args.key.toLowerCase().indexOf('decimal') > -1) {
            args.isSelected = false; // this case makes sure remove/add decimal are not highlighted.
        }
    }
    onFormatOptionsChanged() {
        if (!this.canUpdateColumnFormatting) {
            return;
        }
        if (this.selectedColumnData) {
            setTimeout(() => {
                if (this.gridInstanceList.length) {
                    this.gridInstanceList.forEach(item => item.repaint());
                }
                document.querySelector('.loader').classList.add('display-none');
            }, 0);
            const formatDataCollection = this.getOrSetSessionData(this.storageKey, 'get');
            let index, dataUpdated;
            dataUpdated = false;
            this.formatObject.dataField = this.selectedColumnData.dataField;
            for (index = 0; index < formatDataCollection.length; index++) {
                if (formatDataCollection[index].dataField === this.selectedColumnData.dataField) {
                    formatDataCollection[index] = this.formatObject;
                    dataUpdated = true;
                    break;
                }
            }
            if (!dataUpdated) {
                formatDataCollection.push(this.formatObject);
            }
            this.getOrSetSessionData(this.storageKey, 'set', formatDataCollection);
        }
    }
    toggleBasicFormat() {
        this.showBasicFormat = !this.showBasicFormat;
        this.formatToggled.emit(this.showBasicFormat);
    }
    //#endregion Public Methods
    //#region Private Methods
    selectFormat(args) {
        let formatNotAllowed = false;
        this.formatObject.dataType = this.formatObject.dataType || this.selectedColumnData.dataType;
        switch (args.key) {
            case 'leftAlignment':
                this.setAlignmentFormat(args, 'left');
                break;
            case 'rightAlignment':
                this.setAlignmentFormat(args, 'right');
                break;
            case 'centerAlignment':
                this.setAlignmentFormat(args, 'center');
                break;
            case 'leftPin':
                this.setPinPosition(args, 'left');
                break;
            case 'rightPin':
                this.setPinPosition(args, 'right');
                break;
            case 'bold':
                this.prepareColumnFormatUsingClassName('cssClass', 'bold', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'underline':
                this.prepareColumnFormatUsingClassName('cssClass', 'underline', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'italic':
                this.prepareColumnFormatUsingClassName('cssClass', 'italic', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'currency':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'currency', args.isSelected);
                    this.decimalCounter = 0;
                    this.formatObject.format = args.isSelected ? { type: 'currency', precision: 2 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'currency', 'currency', 2);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'addDecimal':
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    if (this.formatObject.format) {
                        this.decimalCounter = this.formatObject.format.precision || 0;
                    }
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    this.decimalCounter++;
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, true);
                    this.formatObject.format = {
                        type: this.getDecimalType(),
                        precision: this.decimalCounter
                    };
                    this.onFormatOptionsChanged();
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'removeDecimal':
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    if (this.formatObject.format) {
                        this.decimalCounter = this.formatObject.format.precision || 0;
                    }
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    if (this.decimalCounter > 0) {
                        this.decimalCounter--;
                        this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, true);
                        this.formatObject.format = {
                            type: this.getDecimalType(),
                            precision: this.decimalCounter
                        };
                    }
                    else if (this.decimalCounter === 0) {
                        this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    }
                    this.onFormatOptionsChanged();
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'percentage':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'percentage', args.isSelected);
                    this.formatObject.format = args.isSelected ? { type: 'percent', precision: 2 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'percent', 'percentage', 2);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'comma':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.decimalCounter = 0;
                    this.prepareColumnFormatUsingClassName('cssClass', 'comma', args.isSelected);
                    this.formatObject.format = args.isSelected ? { type: 'comma', precision: 0 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'comma', 'comma', 0);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'calendar':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType.indexOf('date') > -1 || !this.enableInputDataFieldCheck) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'calendar', args.isSelected);
                    this.formatObject.format = args.isSelected ? 'shortDate' : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.formatObject.format = args.isSelected ? 'shortDate' : '';
                    break;
                }
                formatNotAllowed = true;
                break;
        }
        if (!this.enableInputCaptionField) {
            setTimeout(() => {
                document.querySelector('.loader').classList.add('display-none');
            }, 0);
        }
        if (formatNotAllowed) {
            appToast({ type: 'warning', message: 'Format can\'t be applied on the selected column.' });
            document.querySelector('.loader').classList.add('display-none');
            return false;
        }
        return true;
    }
    setAlignmentFormat(args, alignmentType) {
        this.resetAlignmentExceptForCurrent(args.type);
        this.formatObject.alignment = args.isSelected ? alignmentType : '';
        if (this.enableInputCaptionField) {
            this.onFormatOptionsChanged();
        }
    }
    setPinPosition(args, position) {
        this.resetPinExceptForCurrent(args.type);
        this.formatObject.fixed = args.isSelected;
        this.formatObject.fixedPosition = args.isSelected ? position : '';
        if (this.gridInstanceList.length) {
            this.gridInstanceList.forEach(grid => {
                grid.columnOption(this.selectedColumnData.dataField, { fixed: args.isSelected, fixedPosition: this.formatObject.fixedPosition });
            });
        }
        if (this.enableInputCaptionField) {
            this.onFormatOptionsChanged();
        }
    }
    columnFormatChangeForDataDictionary(args, type, formatClass, precision) {
        this.prepareColumnFormatUsingClassName('cssClass', formatClass, args.isSelected);
        this.formatObject.format = args.isSelected ? { type, precision } : '';
    }
    resetAlignmentExceptForCurrent(type = '') {
        for (let i = 0; i < this.basicFormatIconsAndOptionsData.alignment.length; i++) {
            if (this.basicFormatIconsAndOptionsData.alignment[i].type !== type) {
                this.basicFormatIconsAndOptionsData.alignment[i].isSelected = false;
            }
        }
    }
    resetPinExceptForCurrent(type = '') {
        this.basicFormatIconsAndOptionsData.pin.forEach(item => {
            if (item.type !== type) {
                item.isSelected = false;
            }
        });
    }
    prepareColumnFormatUsingClassName(formatType, formatValue, addFormat) {
        let listOfExistingFormats = this.formatObject[formatType] || '';
        if (addFormat) {
            if (listOfExistingFormats && listOfExistingFormats.length) {
                const formatGroupData = ['currency', 'percentage', 'comma', 'calendar'];
                if (formatGroupData.indexOf(formatValue) > -1) {
                    for (let formatIndex = 0; formatIndex < formatGroupData.length; formatIndex++) {
                        listOfExistingFormats = listOfExistingFormats.replace(formatGroupData[formatIndex], '');
                    }
                }
                listOfExistingFormats = listOfExistingFormats.replace(new RegExp(formatValue, 'g'), '').trim();
                listOfExistingFormats = listOfExistingFormats + ' ' + formatValue;
            }
            else {
                listOfExistingFormats = formatValue;
            }
        }
        else if (listOfExistingFormats && listOfExistingFormats.indexOf(formatValue) > -1) {
            listOfExistingFormats = listOfExistingFormats.replace(new RegExp(formatValue, 'g'), '').trim();
        }
        this.formatObject[formatType] = listOfExistingFormats;
        const _data = {};
        if (formatType === 'cssClass') {
            _data[formatType] = listOfExistingFormats;
        }
        else {
            _data[formatType] = addFormat ? formatValue : listOfExistingFormats;
        }
        return _data;
    }
    resetSelectedFormat(type = '') {
        let listOfClasses = this.formatObject.cssClass;
        if (listOfClasses) {
            listOfClasses = listOfClasses.replace(/percentage/g, '').replace(/comma/g, '').replace(/currency/g, '');
            this.formatObject.cssClass = listOfClasses;
        }
        for (let i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
            if (this.basicFormatIconsAndOptionsData.format[i].type !== type) {
                this.basicFormatIconsAndOptionsData.format[i].isSelected = false;
            }
        }
    }
    resetBasicSettings() {
        let i;
        for (i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
            this.basicFormatIconsAndOptionsData.format[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.alignment.length; i++) {
            this.basicFormatIconsAndOptionsData.alignment[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.pin.length; i++) {
            this.basicFormatIconsAndOptionsData.pin[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.fontStyle.length; i++) {
            this.basicFormatIconsAndOptionsData.fontStyle[i].isSelected = false;
        }
        this.textColor = '';
        this.backgroundColor = '';
        this.formatObject = {};
        this.decimalCounter = 0;
    }
    prepareSelectedOption() {
        this.resetBasicSettings();
        let selectedColumnDef = new Object();
        if (this.enableStorage) {
            const columnFormatCollection = this.getOrSetSessionData(this.storageKey, 'get');
            const baseFormatCollection = this.isMasterGridSelected ?
                JSON.parse(sessionStorage.getItem(CGFStorageKeys[CGFStorageKeys.dictionaryFormatData])) || [] : [];
            selectedColumnDef = columnFormatCollection.find(arrItem => arrItem.dataField === this.selectedColumnData.dataField) ||
                baseFormatCollection.find(arrItem => arrItem.dataField === this.selectedColumnData.dataField) || {};
        }
        if (!Object.keys(selectedColumnDef).length && this.selectedColumnData) {
            selectedColumnDef = this.selectedColumnData;
        }
        if (selectedColumnDef) {
            if (selectedColumnDef.cssClass) {
                const classList = selectedColumnDef.cssClass.split(' ');
                let i;
                classList.forEach((cssClass) => {
                    if (cssClass === 'bold' || cssClass === 'underline' || cssClass === 'italic') {
                        for (i = 0; i < this.basicFormatIconsAndOptionsData.fontStyle.length; i++) {
                            if (this.basicFormatIconsAndOptionsData.fontStyle[i].type === cssClass) {
                                this.basicFormatIconsAndOptionsData.fontStyle[i].isSelected = true;
                                this.formatObject.cssClass = this.formatObject.cssClass ? this.formatObject.cssClass + ' ' + cssClass : ' ' + cssClass;
                                break;
                            }
                        }
                    }
                    if (cssClass === 'currency' || cssClass === 'percentage' || cssClass === 'comma' || cssClass === 'calendar') {
                        for (i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
                            if (this.basicFormatIconsAndOptionsData.format[i].type === cssClass) {
                                this.basicFormatIconsAndOptionsData.format[i].isSelected = true;
                                this.decimalCounter = selectedColumnDef.format ? selectedColumnDef.format.precision : 0;
                                this.formatObject.cssClass = this.formatObject.cssClass ? this.formatObject.cssClass + ' ' + cssClass : ' ' + cssClass;
                                break;
                            }
                        }
                    }
                    if (cssClass.indexOf('decimal') > -1) {
                        this.decimalCounter = selectedColumnDef.format.precision;
                    }
                });
            }
            if (selectedColumnDef.fixed) {
                const isPositionLeft = selectedColumnDef.fixedPosition === 'left' ? true : false;
                for (let ind = 0; ind < this.basicFormatIconsAndOptionsData.pin.length; ind++) {
                    if (isPositionLeft && this.basicFormatIconsAndOptionsData.pin[ind].key === 'leftPin') {
                        this.basicFormatIconsAndOptionsData.pin[ind].isSelected = true;
                        break;
                    }
                    else if (this.basicFormatIconsAndOptionsData.pin[ind].key === 'rightPin') {
                        this.basicFormatIconsAndOptionsData.pin[ind].isSelected = true;
                        break;
                    }
                }
            }
            let j;
            for (j = 0; j < this.basicFormatIconsAndOptionsData.alignment.length; j++) {
                if (this.basicFormatIconsAndOptionsData.alignment[j].type === selectedColumnDef.alignment) {
                    this.basicFormatIconsAndOptionsData.alignment[j].isSelected = true;
                }
            }
            for (j = 0; j < this.basicFormatIconsAndOptionsData.pin.length; j++) {
                if (this.basicFormatIconsAndOptionsData.pin[j].type === selectedColumnDef.fixedPosition) {
                    this.basicFormatIconsAndOptionsData.pin[j].isSelected = true;
                }
            }
            this.textColor = selectedColumnDef.textColor || '';
            this.backgroundColor = selectedColumnDef.backgroundColor || '';
            this.formatObject = selectedColumnDef;
        }
        this.isColorReadOnly = !this.selectedColumnData;
    }
    removePrecisionControls(data) {
        const columnFormatData = Object.assign({}, data);
        const formatData = columnFormatData.format.filter((item) => {
            if (item.key !== 'removeDecimal' && item.key !== 'addDecimal') {
                return item;
            }
        });
        columnFormatData.format = formatData;
        return columnFormatData;
    }
    getDecimalType() {
        let decimalFormatType = 'fixedPoint';
        if (this.formatObject.format) {
            decimalFormatType = this.decimalTypes.some(t => t === this.formatObject.format.type)
                ? this.formatObject.format.type : 'fixedPoint';
        }
        return decimalFormatType;
    }
    getOrSetSessionData(key, type, data = null) {
        if (!this.enableStorage) {
            return [];
        }
        key = this.gridInstanceList.length ? this.utilityService.getStorageKey(this.gridInstanceList[0], key, this.isMasterGridSelected) : key;
        switch (type) {
            case 'get':
                return JSON.parse(sessionStorage.getItem(key)) || [];
            case 'set':
                sessionStorage.setItem(key, JSON.stringify(data));
                break;
        }
        return [];
    }
    resetAllFormatOptions() {
        this.resetBasicSettings();
        this.resetPinExceptForCurrent();
        this.resetSelectedFormat();
        this.resetAlignmentExceptForCurrent();
    }
    prepareLoaderLocation(event) {
        const cord = event.currentTarget.getBoundingClientRect();
        const leftCords = cord.left + 4 + 'px';
        const topCords = cord.top + 4 + 'px';
        const firstChild = document.querySelector('.loader');
        firstChild.classList.remove('display-none');
        firstChild.style.top = topCords;
        firstChild.style.left = leftCords;
    }
};
ColorFormatComponent.ctorParameters = () => [
    { type: CGFUtilityService }
];
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableAlignment", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableBackgroundColor", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableFontStyle", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableFormat", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableFormatToggle", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableInputCaptionField", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableInputDataFieldCheck", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enablePin", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableStorage", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableTextColor", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "gridInstanceList", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "isMasterGridSelected", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "showBasicFormat", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "storageKey", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "disableDecimalPrecisionInput", null);
__decorate([
    Input()
], ColorFormatComponent.prototype, "selectedColumnInfo", null);
__decorate([
    Output()
], ColorFormatComponent.prototype, "formatOptionsChanged", void 0);
__decorate([
    Output()
], ColorFormatComponent.prototype, "formatToggled", void 0);
ColorFormatComponent = __decorate([
    Component({
        selector: 'pbi-color-format',
        template: "<div id=\"loader-container\">\r\n  <div class=\"loader display-none\"></div>\r\n</div>\r\n\r\n<div>\r\n  <div class=\"active-properties-tab\">\r\n    <ul>\r\n      <li *ngFor=\"let tabs of columnsActivePropertiesTabsList\"\r\n        [class.property-tab-in-column-chooser]=\"enableFormatToggle\"\r\n        [class.property-tab-in-data-dictionary]=\"!enableFormatToggle\"\r\n        (click)=\"enableFormatToggle && toggleBasicFormat()\">{{tabs.title}}\r\n        <span *ngIf=\"enableFormatToggle\" [class.fa-angle-down]=\"!showBasicFormat\" [class.fa-angle-up]=\"showBasicFormat\"\r\n          class=\"fa float-right basic-format-toggle-icon\"></span>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <div class=\"active-properties-basic-tab-content\" *ngIf=\"showBasicFormat || !enableFormatToggle\">\r\n    <div class=\"format-controls-container\">\r\n      <!-- <div *ngIf=\"enableInputDataFieldCheck\">\r\n                <div>Column ID: <span class=\"bold\">{{selectedColumnInfo.dataField}}</span></div>\r\n            </div> -->\r\n      <div *ngIf=\"enableInputCaptionField\">\r\n        <div>Caption:</div>\r\n        <div class=\"caption-font-style\">\r\n          <dx-text-box class=\"caption-input\" [(value)]=\"selectedColumnInfo.caption\"\r\n            (onValueChanged)=\"onCaptionValueChanged($event)\" maxLength=\"40\">\r\n            <dx-validator>\r\n              <dxi-validation-rule type=\"pattern\" [pattern]=\"captionValidationPattern\"\r\n                message=\"Caption will contain only alphabets(a-z,A-Z)/numbers(0-9)/underscore(_)/space.\">\r\n              </dxi-validation-rule>\r\n            </dx-validator>\r\n          </dx-text-box>\r\n        </div>\r\n      </div>\r\n      <div class=\"format-alignments-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enableFormat\">\r\n          <div>Format:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.format\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableAlignment\">\r\n          <div>Alignment:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.alignment\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"pin-font-style-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enablePin\">\r\n          <div>Pin:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.pin\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableFontStyle\">\r\n          <div>Font Style:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.fontStyle\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"text-font-style-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enableTextColor\">\r\n          <div>Text Color:</div>\r\n          <div class=\"text-margin-style\">\r\n            <dx-color-box [(value)]=\"textColor\" [readOnly]=\"isColorReadOnly\" (onFocusIn)=\"onColorFocusIn()\"\r\n              (onValueChanged)=\"onChangeOfTextColor()\">\r\n            </dx-color-box>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableBackgroundColor\">\r\n          <div>Background Color:</div>\r\n          <div class=\"background-margin-style\">\r\n            <dx-color-box [(value)]=\"backgroundColor\" [readOnly]=\"isColorReadOnly\" (onFocusIn)=\"onColorFocusIn()\"\r\n              (onValueChanged)=\"onChangeOfBackgroundColor()\">\r\n            </dx-color-box>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n",
        styles: [""]
    })
], ColorFormatComponent);

let ColumnChooserComponent = class ColumnChooserComponent {
    constructor() {
        this.refreshOnColumnUpdate = true;
        //#endregion Input Properties
        //#region Output Events
        this.applyButtonClick = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.selectedColumnData = {
            caption: '',
            columnId: -1,
            dataField: '',
            dataType: null,
            description: '',
            format: '',
            groupName: '',
            visible: true
        };
        this.bulkApplyColumnsLookup = {};
        this.columnFieldTitle = 'COLUMNS';
        this.columnSearchValue = '';
        this.columnsData = [];
        this.columnsSubject = new Subject();
        this.copyOfColumnList = [];
        this.isMasterGridSelected = true;
        this.selectAllChecked = false;
        this.selectedCols = [];
        this.selectedGridInstanceList = [];
        this.selectedGroupNames = [];
        this.showBasicFormat = JSON.parse(sessionStorage.getItem('basicFormatVisibility')) || false;
        this.showColumnList = true;
    }
    //#region Input Properties
    set columnsList(columnsList) {
        this.columnsData = [];
        this.selectedGroupNames = [];
        this.copyOfColumnList = [];
        columnsList.forEach(item => {
            this.columnsData.push(item);
            if (item.groupName && this.selectedGroupNames.indexOf(item.groupName) === -1) {
                this.selectedGroupNames.push(item.groupName);
            }
            this.copyOfColumnList.push(item);
        });
    }
    get columnsList() { return this.columnsData; }
    set selectedColumnInfo(columnItem) {
        this.selectedColumnData = columnItem;
        if (columnItem === null || columnItem === void 0 ? void 0 : columnItem.dataField) {
            this.onClickColumnItem(columnItem.dataField);
        }
        this.bringElementToViewArea((columnItem === null || columnItem === void 0 ? void 0 : columnItem.dataField) || '');
    }
    get selectedColumnInfo() { return this.selectedColumnData; }
    set gridInstanceList(gridInstanceList) {
        var _a;
        const gridInstances = gridInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length);
        this.selectedGridInstanceList = gridInstances.map(item => item.gridComponentInstance);
        if (gridInstanceList.length > 1 && ((_a = gridInstances[0]) === null || _a === void 0 ? void 0 : _a.gridName)) {
            this.isMasterGridSelected = gridInstances[0].isMasterGrid;
            this.columnFieldTitle = `COLUMNS${this.isMasterGridSelected ? '' : ' - Child View'}`.toUpperCase();
        }
    }
    get gridInstanceList() { return this.selectedGridInstanceList; }
    set debounce(num) {
        this.subscribeToColumnChanges(num);
    }
    ;
    //#endregion Output Events
    get checkMasterCheckBox() {
        const anyOneColumnHidden = this.columnsData.find(colItem => {
            return colItem.visible === false && colItem.dataField !== customActionColumnInfo.dataField;
        });
        return anyOneColumnHidden ? false : true;
    }
    set checkMasterCheckBox(args) { this.onClickMasterCheckBox(args); }
    //#region Angular Lifecycle Events
    ngOnInit() {
        if (this.selectedGridInstanceList.length) {
            setTimeout(() => {
                this.selectedGridInstanceList.forEach(grid => {
                    grid.columnOption(customActionColumnInfo.dataField, { visible: false });
                    sessionStorage.setItem('currentAppliedView', JSON.stringify(grid.state())); // holding state to make sure group-by works when we toggle selectAll
                });
            }, 0);
        }
    }
    ngOnDestroy() {
        this.selectedCols.forEach(col => {
            this.columnsData.filter(column => column.dataField === col.dataField)[0].visible = !col.visible;
        });
        const anyColumnVisible = this.columnsData.find(item => item.visible);
        if (this.selectedGridInstanceList.length) {
            this.selectedGridInstanceList.forEach(item => {
                item.columnOption(customActionColumnInfo.dataField, { visible: anyColumnVisible ? true : false });
            });
        }
        sessionStorage.removeItem('currentAppliedView');
        // Lets columns finish up rendering if any outstanding. Will unsubcribe itself when
        // destroyed parameter is set to true.
        this.destroyed = true;
        this.columnsSubject.next(null);
    }
    //#endregion Angular Lifecycle Events
    //#region Public Methods
    toggleColumnVisibility(dataField, visible) {
        var _a;
        if (this.isCached) {
            const colIndex = this.selectedCols.findIndex(data => data.dataField === dataField);
            if (colIndex !== -1) {
                if (visible) {
                    visible = !((_a = this.selectedGridInstanceList[0].getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.filter(col => col.hasOwnProperty('dataField') && col.dataField === dataField).length);
                }
                if (!visible) {
                    this.selectedCols.splice(colIndex, 1);
                }
            }
            else if (colIndex === -1) {
                this.selectedCols.push({ dataField, visible });
            }
        }
        else if (this.useBulkApply) {
            this.bulkApplyColumnsLookup[dataField.toLowerCase()] = { dataField, visible };
        }
        else {
            for (let index = 0; index < this.columnsData.length; index++) {
                if (dataField === this.columnsData[index].dataField) {
                    this.columnsData[index].visible = visible;
                    this.columnsSubject.next({ visible, dataField });
                    break;
                }
            }
        }
    }
    toggleGroupVisibility(event, groupName) {
        if (this.selectedGroupNames.indexOf(groupName) > -1) {
            this.selectedGroupNames = this.selectedGroupNames.filter(arr => arr !== groupName);
        }
        else {
            this.selectedGroupNames.push(groupName);
        }
    }
    onSearchColumnValueChange(args) {
        this.copyOfColumnList = JSON.parse(JSON.stringify(this.columnsData.filter(data => data.caption.toLowerCase().indexOf(args.value.toLowerCase()) > -1)));
        const visibleCol = this.selectedCols.filter(data => data.visible);
        if (visibleCol.length) {
            visibleCol.forEach(item => {
                this.copyOfColumnList.filter(data => data.dataField === item.dataField).map((col) => col.visible = true);
            });
        }
    }
    columnChooserClick(event) {
        const target = event.target;
        if (target && target.dataset) {
            switch (target.tagName.toLowerCase()) {
                case 'label':
                    this.onClickColumnItem(target.dataset.item);
                    break;
            }
        }
        event.stopPropagation();
    }
    onClickMasterCheckBox(val) {
        this.selectAllChecked = val;
        this.copyOfColumnList.forEach(item => {
            item.visible = val;
        });
        if (this.isCached) {
            this.columnsData.forEach(item => {
                if (item.dataField !== customActionColumnInfo.dataField) {
                    this.toggleColumnVisibility(item.dataField, val);
                }
            });
        }
        if (this.useBulkApply) {
            this.copyOfColumnList.forEach(item => {
                this.bulkApplyColumnsLookup[item.dataField.toLowerCase()] = { dataField: item.dataField, visible: item.visible };
            });
        }
        else {
            setTimeout(() => {
                this.copyOfColumnList.forEach(item => { item.visible = val; });
                this.selectedGridInstanceList.forEach(item => {
                    item.option('columns', []);
                });
                this.columnsData.forEach(item => {
                    if (item.dataField !== customActionColumnInfo.dataField) {
                        item.visible = val;
                    }
                });
                this.selectedGridInstanceList.forEach(item => {
                    item.option('columns', this.columnsData);
                });
                if (this.selectAllChecked) {
                    this.selectedGridInstanceList.forEach(item => {
                        item.state(JSON.parse(sessionStorage.getItem('currentAppliedView')));
                    });
                }
            }, 0);
        }
    }
    fetchCachedColumnsData() {
        if (this.selectedCols.length) {
            const actionColumn = this.columnsData.filter(data => data.dataField === customActionColumnInfo.dataField && data.groupName === customActionColumnInfo.groupName)[0];
            const visibleColumns = this.selectedCols.filter(data => data.visible).map(item => item.dataField);
            if (actionColumn) {
                this.selectedGridInstanceList.forEach(item => {
                    item.columnOption(actionColumn.dataField, { visible: visibleColumns.length ? true : false, fixed: true, fixedPosition: 'right' });
                });
            }
        }
        else {
            this.selectedGridInstanceList.forEach(gridInst => {
                gridInst.beginUpdate();
                this.columnsList.forEach(item => {
                    gridInst.columnOption(item.dataField, 'visible', false);
                });
                gridInst.option('dataSource', []);
                gridInst.endUpdate();
            });
        }
        // TODO: check enable and disable grid export. If not then remove below code.
        if ((this.selectedGridInstanceList[0].getVisibleColumns().filter(data => data.command !== 'empty').length) > 0) {
            this.selectedGridInstanceList[0].option('export.enabled', false);
        }
        this.applyButtonClick.emit(this.selectedCols);
        this.selectedCols = [];
    }
    formatOptionsChanged(args) {
        this.columnsData.forEach(col => {
            var _a;
            if (col.dataField === ((_a = this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField)) {
                col.caption = args.caption;
                this.selectedGridInstanceList.forEach(item => {
                    item.columnOption(col.dataField, { caption: col.caption });
                });
            }
        });
        this.copyOfColumnList.forEach(item => {
            var _a;
            if (item.dataField === ((_a = this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField)) {
                item.caption = args.caption;
            }
        });
    }
    onFormatToggled(val) {
        this.showBasicFormat = val;
        sessionStorage.setItem('basicFormatVisibility', JSON.stringify(val));
    }
    onBulkApplyClick() {
        this.bulkApplyColumns(Object.values(this.bulkApplyColumnsLookup));
        this.bulkApplyColumnsLookup = {};
        this.closeFlyOut();
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
    //#endregion Public Methods
    //#region Private Methods
    onClickColumnItem(dataField) {
        this.selectedColumnData = this.columnsData.filter(element => element.dataField.toLowerCase() === dataField.toLowerCase())[0];
    }
    bringElementToViewArea(dataField) {
        const dataFieldElem = window.document.querySelectorAll(`input[data-item='${dataField}']`);
        const container = window.document.getElementsByClassName('columnChooser-group-columns');
        if (container.length && dataFieldElem.length) {
            const topPos = dataFieldElem[0].offsetTop;
            container[0].scrollTop = topPos - (container[0].offsetTop + 10); // 10 is offset to enhance UX.
        }
    }
    subscribeToColumnChanges(debounce) {
        if (this.columnSubjectSubscription) {
            this.columnSubjectSubscription.unsubscribe();
        }
        this.columnSubjectSubscription = this.columnsSubject.pipe(buffer(this.columnsSubject.pipe(debounceTime(debounce))))
            .subscribe(fields => {
            fields = fields.filter(f => f);
            this.bulkApplyColumns(fields);
            if (this.destroyed) {
                this.columnSubjectSubscription.unsubscribe();
            }
        });
    }
    bulkApplyColumns(fields) {
        if (fields.length > 0) {
            this.selectedGridInstanceList.forEach(item => { item.beginCustomLoading('Loading'); item.beginUpdate(); });
            fields.forEach(f => {
                this.selectedGridInstanceList.forEach(item => item.columnOption(f.dataField, { visible: f.visible }));
            });
            if (this.refreshOnColumnUpdate) {
                this.selectedGridInstanceList.forEach(item => item.refresh(true));
            }
            else {
                this.selectedGridInstanceList.forEach(item => item.repaint());
            }
            this.selectedGridInstanceList.forEach(item => { item.endCustomLoading(); item.endUpdate(); });
        }
    }
};
__decorate([
    Input()
], ColumnChooserComponent.prototype, "columnsList", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "isCached", void 0);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "selectedColumnInfo", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "gridInstanceList", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "debounce", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "useBulkApply", void 0);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "refreshOnColumnUpdate", void 0);
__decorate([
    Output()
], ColumnChooserComponent.prototype, "applyButtonClick", void 0);
__decorate([
    Output()
], ColumnChooserComponent.prototype, "closeCurrentFlyOut", void 0);
ColumnChooserComponent = __decorate([
    Component({
        selector: 'pbi-column-chooser',
        template: "<div class=\"column-chooser-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-cog title-icon\"></i>\r\n            <span class=\"section-title\">FIELDS</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"property-tab-in-column-chooser\" (click)=\"showColumnList = !showColumnList\">\r\n                <span>{{columnFieldTitle}}</span>\r\n                <i [class.fa-angle-down]=\"!showColumnList\" [class.fa-angle-up]=\"showColumnList\"\r\n                    class=\"fa basic-format-toggle-icon\"></i>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n\r\n    <div class=\"accordion-content columns-search-container\" *ngIf=\"showColumnList\">\r\n        <div class=\"form-group columns-search\">\r\n            <div class=\"text-container\">\r\n                <dx-text-box mode=\"search\" [(value)]=\"columnSearchValue\" valueChangeEvent=\"keyup\"\r\n                    (onValueChanged)=\"onSearchColumnValueChange($event)\" placeholder=\"Enter search text\"\r\n                    [showClearButton]=\"true\">\r\n                </dx-text-box>\r\n            </div>\r\n        </div>\r\n        <div class=\"column-list-container\">\r\n            <div class=\"columnChooser-group-columns\"\r\n                [class.columnChooser-group-columns-without-format]=\"!showBasicFormat\"\r\n                [class.columnChooser-group-columns-with-format]=\"showBasicFormat\">\r\n                <span class=\"column-visible-label\">Visible</span>\r\n                <span class=\"column-caption-label\">Caption</span>\r\n                <div class=\"column-header\">\r\n                    <input type=\"checkbox\" [(ngModel)]=\"checkMasterCheckBox\" />\r\n                    <label class=\"column-list-label\">Select All</label>\r\n                </div>\r\n                <div *ngFor=\"let groupItem of copyOfColumnList;let i = index;\">\r\n                    <div class=\"groupContainer\"\r\n                        *ngIf=\" ((i===0) || (groupItem.groupName != copyOfColumnList[i-1].groupName)) && groupItem.groupName !== '_row_actions_group_' \"\r\n                        (click)=\"toggleGroupVisibility($event, groupItem.groupName)\">\r\n                        <i class=\"dx-icon-spindown\"\r\n                            [class.dx-icon-spindown]=\"selectedGroupNames.indexOf(groupItem.groupName)  > -1 \"\r\n                            [class.dx-icon-spinright]=\"selectedGroupNames.indexOf(groupItem.groupName) === -1\"></i>\r\n                        <span>{{groupItem.groupName || 'Columns'}}</span>\r\n                    </div>\r\n                    <span *ngIf=\"selectedGroupNames.indexOf(groupItem.groupName) > -1\">\r\n                        <div class=\"cgf-columns-list\"\r\n                            [class.selectedColumn]=\"selectedColumnInfo && groupItem.dataField === selectedColumnInfo.dataField\">\r\n                            <input type=\"checkbox\" attr.data-item=\"{{groupItem.dataField}}\"\r\n                                [(ngModel)]=\"groupItem.visible\"\r\n                                (change)=\"toggleColumnVisibility(groupItem.dataField, groupItem.visible)\" />\r\n                            <label class=\"column-list-label\" (click)=\"columnChooserClick($event)\"\r\n                                attr.data-item=\"{{groupItem.dataField}}\">{{groupItem.caption}}</label>\r\n                        </div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"accordion-content\">\r\n        <pbi-color-format [gridInstanceList]=\"selectedGridInstanceList\" [isMasterGridSelected]=\"isMasterGridSelected\"\r\n            [selectedColumnInfo]=\"selectedColumnData\" [enableInputDataFieldCheck]=\"true\"\r\n            [showBasicFormat]=\"showBasicFormat\" (formatOptionsChanged)=\"formatOptionsChanged($event)\"\r\n            (formatToggled)=\"onFormatToggled($event)\">\r\n        </pbi-color-format>\r\n    </div>\r\n    <span class=\"button-container\" *ngIf=\"isCached\">\r\n        <dx-button text=\"Apply\" class=\"cacheButton\" type=\"default\" (onClick)=\"fetchCachedColumnsData()\">\r\n        </dx-button>\r\n    </span>\r\n    <span class=\"button-container\" *ngIf=\"useBulkApply\">\r\n        <dx-button text=\"Apply\" class=\"cacheButton\" type=\"default\" (onClick)=\"onBulkApplyClick()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    })
], ColumnChooserComponent);

let FlyOutActionIconContainerComponent = class FlyOutActionIconContainerComponent {
    constructor() {
        this.position = 'right';
        this.showLoader = false;
        this.showParametersControls = false;
        this.visibleFlyOuts = [];
        this.flyOutSelectionClick = new EventEmitter();
        this.refreshEntityData = new EventEmitter();
        this.activeFlyOut = { id: -1 };
        this.cgfLeftMenuDropDownListItems = [];
        this.cgfLeftMenus = [];
        this.currentFlyOutSection = { id: -1 };
        this.flyOutSections = [];
        this.leftMenuDropDownList = [];
        this.leftMenuList = [];
    }
    set resetContainerSettings(val) {
        if (val) {
            this.currentFlyOutSection = { id: -1 };
        }
    }
    set selectedFlyOut(val) {
        if (val) {
            this.activeFlyOut = val;
        }
        else {
            this.activeFlyOut = { id: -1 };
        }
    }
    get selectedFlyOut() { return this.activeFlyOut; }
    set cgfLeftMenuList(data) {
        this.cgfLeftMenus = data;
        if (data) {
            this.prepareExtraActions();
        }
    }
    get cgfLeftMenuList() { return this.cgfLeftMenus; }
    ngOnInit() {
        this.flyOutSections = cgfFlyOutList.filter(item => this.visibleFlyOuts.indexOf(item.id) > -1);
    }
    onFlyOutIconClick(evt, item) {
        if (item.id === CGFFlyOutEnum.newTab) {
            open(location.href, '_blank');
            return;
        }
        if (item.id === CGFFlyOutEnum.refreshData) {
            this.refreshData();
            return;
        }
        if (item.id === this.activeFlyOut.id) {
            this.activeFlyOut = { id: -1 };
        }
        else {
            this.activeFlyOut = item;
        }
        this.showParametersControls = false;
        this.flyOutSelectionClick.emit({ evt, item });
    }
    showParametersControlsContainer() {
        this.currentFlyOutSection = { id: -1 };
        this.showParametersControls = this.showParametersControls ? false : true;
        this.flyOutSelectionClick.emit({ showEntityParameter: this.showParametersControls });
    }
    refreshData() {
        if (this.disableRefreshIcon) {
            appToast({ type: 'error', message: 'Parameters are required.' });
            return;
        }
        this.showLoader = true;
        this.refreshEntityData.emit();
    }
    cgfLeftMenuClick(data) {
        openDestination(data, filterSelectedRowData(this.gridInstance), this.dataBrowserEntityParameters, null); // this.appService
    }
    onMoreItemClick(args) {
        if (!args.itemData.items) {
            openDestination(args.itemData, filterSelectedRowData(this.gridInstance), this.dataBrowserEntityParameters);
        }
    }
    onFlyOutSelectionClick(event, item) {
        if (item.id === CGFFlyOutEnum.newTab) {
            open(location.href, '_blank');
            return;
        }
        if (item.id === CGFFlyOutEnum.refreshData) {
            this.refreshData();
            return;
        }
        if (item.id === this.currentFlyOutSection.id) {
            this.currentFlyOutSection = { id: -1 };
        }
        else {
            this.currentFlyOutSection = item;
        }
        this.showParametersControls = false;
        this.flyOutSelectionClick.emit({ event, item });
    }
    prepareExtraActions() {
        this.leftMenuList = this.cgfLeftMenus.filter(x => x.IsFrequentlyUsed === true && x.ShowAsContextMenu === false
            && x.DestinationType !== actionTypes.childEntity);
        this.cgfLeftMenuDropDownListItems = this.cgfLeftMenus.filter(x => x.IsFrequentlyUsed === false && x.ShowAsContextMenu === false
            && x.DestinationType !== actionTypes.childEntity);
        this.leftMenuDropDownList = [{ text: 'More', icon: 'overflow', items: this.cgfLeftMenuDropDownListItems }];
        setTimeout(() => {
            const elm = document.querySelector('.optionsActions .dx-menu-item-popout-container');
            if (elm !== null) {
                elm.remove();
            }
        }, 100);
    }
};
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "dataBrowserEntityParameters", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "disableRefreshIcon", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "gridInstance", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "position", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "showLoader", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "showParametersControls", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "visibleFlyOuts", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "resetContainerSettings", null);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "selectedFlyOut", null);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "cgfLeftMenuList", null);
__decorate([
    Output()
], FlyOutActionIconContainerComponent.prototype, "flyOutSelectionClick", void 0);
__decorate([
    Output()
], FlyOutActionIconContainerComponent.prototype, "refreshEntityData", void 0);
FlyOutActionIconContainerComponent = __decorate([
    Component({
        selector: 'pbi-fly-out-action-icon-container',
        template: "<div [ngClass]=\"{'flex-end': position === 'right', 'flex-start': position === 'left'}\">\r\n    <ul class=\"no-list-style filter-list\">\r\n        <li *ngFor=\"let item of leftMenuList\">\r\n            <span (click)=\"cgfLeftMenuClick(item)\">\r\n                <i *ngIf=\"item.IconClassName\" class=\"{{item.IconClassName}} pointer dynamic-tooltip flyOutSelectionIcon\"\r\n                    attr.title=\"{{item.DisplayName}} \"></i>\r\n                <button *ngIf=\"!item.IconClassName\">{{item.DisplayName}}</button>\r\n            </span>\r\n        </li>\r\n    </ul>\r\n    <dx-menu cssClass=\"optionsActions\" #menu [dataSource]=\"leftMenuDropDownList\" displayExpr=\"DisplayName\"\r\n        (onItemClick)=\"onMoreItemClick($event)\" title=\"More\" class=\"left-menu-item-list-control\"\r\n        *ngIf=\"cgfLeftMenuDropDownListItems.length > 0\">\r\n    </dx-menu>\r\n    <div>\r\n        <ul class=\"icon-list\">\r\n            <li *ngFor=\"let sectionItem of flyOutSections\">\r\n                <i class=\"{{sectionItem.src}}\" [class.pointer-none-events]=\"\"\r\n                    [class.active-flyOut]=\"activeFlyOut.id === sectionItem.id\" [class.loading]=\"\"\r\n                    title=\"{{sectionItem.title}}\" (click)=\"onFlyOutIconClick($event, sectionItem)\">\r\n                </i>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n",
        styles: [""]
    })
], FlyOutActionIconContainerComponent);

let CommonGridFrameworkComponent = class CommonGridFrameworkComponent {
    //#endregion Data Members
    constructor(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.securityIdColumnName = '';
        this.isEditSecurity = false;
        this.showEntityParameter = false;
        this.isCached = false;
        this.cgfEvent = new EventEmitter();
        this.loadChildEntityData = new EventEmitter();
        this.selectionChanged = new EventEmitter();
        //#region Data Members
        this.cachedColumns = [];
        this.cachingEnabled = false;
        this.conditionalFormattingStorageKey = CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
        this.currentSelectedFlyOut = null;
        this.disableRefresh = true;
        this.flyOutEnums = CGFFlyOutEnum; // this variable is for HTML mapping
        this.flyOutTitle = '';
        this.gridComponentInstanceList = [];
        this.resetSettings = false;
        this.selectedColumn = null;
        this.selectedEntityInfo = { columnList: [], gridName: '' };
        this.selectedGridInstance = new Object();
        this.selectedGridOptions = new Object();
        this.CGFStorageKeyCollection = [
            CGFStorageKeys[CGFStorageKeys.conditionalFormatting],
            CGFStorageKeys[CGFStorageKeys.dictionaryFormatData],
            CGFStorageKeys[CGFStorageKeys.formatData],
            CGFStorageKeys[CGFStorageKeys.childGridView]
        ];
    }
    set currentFlyOut(flyOut) {
        if (flyOut) {
            const item = cgfFlyOutList.find(f => f.id === flyOut);
            this.onFlyOutSelectionChange({ item });
        }
    }
    set cgfFeatureUpdate(option) {
        switch (option) {
            case CGFFeatureUpdateEnum.customColumnUpdated:
                this.handleCellClick(this.selectedGridOptions, null);
                break;
            case CGFFeatureUpdateEnum.cachedData:
                this.updateCachedDataSource();
                break;
        }
    }
    get containsVisibleParameters() {
        const visibleParams = this.options.dataBrowserEntityParameters.filter(p => !p.HideInDataBrowser);
        return visibleParams.length > 0;
    }
    ngOnInit() {
        var _a;
        if ((_a = this.options) === null || _a === void 0 ? void 0 : _a.dataBrowserEntityParameters.length) {
            this.options.dataBrowserEntityParameters.map(val => {
                if (val.DefaultSingleValue) {
                    if (val.DataType.toLowerCase() === 'boolean') {
                        val.Value = isNaN(val.DefaultSingleValue) ? val.DefaultSingleValue.toLowerCase() === 'true' : Number(val.DefaultSingleValue) === 1;
                    }
                    if (val.DataType.toLowerCase() === 'datetime') {
                        val.Value = val.DefaultSingleValue ? convertDateToUsFormat(val.DefaultSingleValue) : '';
                    }
                    else {
                        val.Value = val.DefaultSingleValue;
                    }
                }
                else if (val.DataType.toLowerCase() === 'boolean' && !val.Value) {
                    val.Value = false;
                }
                else if (val.Type.toLowerCase() === 'sqllist' || val.Type.toLowerCase() === 'list') {
                    val.Value = val.DefaultValue;
                }
            });
        }
    }
    onGridInitialized() {
        this.tryToApplyInitialViews();
    }
    onFlyOutSelectionChange(args) {
        var _a, _b;
        if (!this.selectedGridInstance || !Object.keys(this.selectedGridInstance).length) {
            this.selectedGridInstance = this.options.gridOptions.gridComponentInstance;
            this.selectedGridOptions = this.options.gridOptions;
        }
        if (typeof args.item === 'undefined' && args.showEntityParameter) {
            this.showEntityParameter = args.showEntityParameter;
            this.closeCurrentFlyOut();
        }
        else {
            this.showEntityParameter = false;
            if (((_a = args.item) === null || _a === void 0 ? void 0 : _a.id) === ((_b = this.currentSelectedFlyOut) === null || _b === void 0 ? void 0 : _b.id)) {
                this.closeCurrentFlyOut();
            }
            else {
                this.currentSelectedFlyOut = args.item || null;
            }
        }
        this.setGridInstanceList(this.selectedGridOptions);
    }
    viewEvents(args) {
        if (this.options.viewDataSource) {
            switch (args.event) {
                case CGFEventsEnum.applyView:
                    this.options.gridOptions.applyViewToGrid(args.data);
                    break;
                case CGFEventsEnum.updateLayouts:
                    this.options.viewList = args.data;
                default:
                    break;
            }
        }
        else {
            this.cgfEvent.emit(args);
        }
    }
    customColumnEvent(args) {
        this.cgfEvent.emit(args);
    }
    entityParamEvent(args) {
        this.cgfEvent.emit(args);
    }
    onFocusoutEntityParam(args) {
        this.disableRefresh = args;
    }
    cellClickEvent(cellClickObject) {
        this.handleCellClick(this.options.gridOptions, cellClickObject);
        this.selectionChanged.emit(cellClickObject);
    }
    gridChanged(event) {
        if (event) {
            const currentFlyOutId = this.currentSelectedFlyOut.id;
            this.currentSelectedFlyOut = { id: CGFFlyOutEnum.newTab };
            setTimeout(() => { this.currentSelectedFlyOut.id = currentFlyOutId; }, 0);
            this.selectedColumn = null;
        }
        else {
            if (this.selectedGridOptions && Object.keys(this.selectedGridOptions).length) {
                this.setGridInstanceList(this.selectedGridOptions);
            }
        }
    }
    onGridOptionChange(args) {
        var _a, _b;
        switch (args.key) {
            case 'gridLines':
                this.options.gridOptions.showColumnLines = args.value;
                if ((_a = this.options.gridOptions.childEntityList[0]) === null || _a === void 0 ? void 0 : _a.gridOptions) {
                    this.options.gridOptions.childEntityList[0].gridOptions.showColumnLines = args.value;
                }
                break;
            case 'theme':
                this.options.gridOptions.selectedTheme = args.value;
                // this.options.gridOptions.selectedThemeClass = getClassNameByThemeName(this.options.gridOptions.selectedTheme);
                if ((_b = this.options.gridOptions.childEntityList[0]) === null || _b === void 0 ? void 0 : _b.gridOptions) {
                    this.options.gridOptions.childEntityList[0].gridOptions.selectedTheme = args.value;
                }
                break;
        }
    }
    customButtonClick(args) {
        switch (args.btnType) {
            case 'edit':
                const visibleCols = this.options.gridOptions.gridComponentInstance.getVisibleColumns();
                if (visibleCols && visibleCols.length) {
                    const cols = visibleCols.map(data => data.dataField).filter(col => {
                        return col && (col.toLowerCase() === 'securityid' || col.toLowerCase() === 'security id');
                    });
                    if (cols && cols.length) {
                        this.cgfEvent.emit({ event: CGFEventsEnum.editSecurity, data: args.rowData });
                    }
                    else {
                        appToast({ type: 'error', message: 'SecurityId is a required field. Please select from column list.' });
                    }
                    break;
                }
                break;
            case 'add':
                this.cgfEvent.emit({ event: CGFEventsEnum.editSecurity, data: {} });
                break;
        }
    }
    applyButtonClick(args) {
        this.closeCurrentFlyOut();
        this.cachedColumns = args;
        this.cgfEvent.emit({ event: CGFEventsEnum.getCachedData, data: args });
    }
    onRefreshEntityData() {
        this.closeCurrentFlyOut();
        this.cgfEvent.emit({ event: CGFEventsEnum.refreshEntityData });
    }
    saveOrderOfParameters() {
        this.cgfEvent.emit({ event: CGFEventsEnum.saveParameterOrder });
    }
    changeInValueOfConditionalFormatting() {
        this.options.gridOptions.gridComponentInstance.repaint();
    }
    getChildEntityData(event) {
        var _a;
        if (this.isCached) {
            const primaryKeyColName = this.options.gridOptions.childEntityList[0].primaryKeyColumn;
            const isPrimaryColumnExists = (_a = this.options.gridOptions.gridComponentInstance.getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.filter(col => col.dataField === primaryKeyColName);
            if (!isPrimaryColumnExists.length) {
                appToast({
                    type: 'error', message: `Following column(s) ${primaryKeyColName} is required. Please make sure same exists with value.`,
                    displayTime: 4000
                });
                return;
            }
        }
        this.loadChildEntityData.emit(event);
    }
    childEntityCellClick(args) {
        this.options.gridOptions.childEntityList.forEach(grid => {
            if (grid.gridOptions.gridName === args.childGridOptions.gridName) {
                this.selectedGridOptions = grid.gridOptions;
            }
        });
        this.handleCellClick(this.selectedGridOptions, args.cellObject);
    }
    resetGridFormattingInfo() {
        let sessionCount = sessionStorage.length;
        while (sessionCount--) {
            const key = sessionStorage.key(sessionCount);
            if (this.CGFStorageKeyCollection.findIndex(item => key.indexOf(item) > -1 ? true : false) > -1) {
                sessionStorage.removeItem(key);
            }
        }
        sessionStorage.removeItem('theme');
        this.gridComponentInstanceList.forEach(item => item.gridComponentInstance.state({}));
    }
    closeCurrentFlyOut() {
        this.currentSelectedFlyOut = null;
        // this.resetSettings = true;
    }
    updateParameterSectionVisibility() {
        var _a;
        if (this.appCGFContainer) {
            (_a = this.appCGFContainer) === null || _a === void 0 ? void 0 : _a.showParametersControlsContainer();
        }
        else {
            this.onFlyOutSelectionClick({ showEntityParameter: !this.showEntityParameter });
        }
    }
    onFlyOutSelectionClick(args) {
        this.resetSettings = false;
        if (!this.selectedGridInstance || !Object.keys(this.selectedGridInstance).length) {
            this.selectedGridInstance = this.options.gridOptions.gridComponentInstance;
            this.selectedGridOptions = this.options.gridOptions;
        }
        if (typeof args.item === 'undefined' && args.showEntityParameter) {
            this.showEntityParameter = args.showEntityParameter;
            this.currentSelectedFlyOut = new Object();
        }
        else {
            this.showEntityParameter = false;
            if (args.item && args.item.id === this.currentSelectedFlyOut.id) {
                this.currentSelectedFlyOut = new Object();
            }
            else {
                this.currentSelectedFlyOut = args.item || {};
            }
        }
        this.setGridInstanceList(this.selectedGridOptions);
        this.setFlyOutTitle();
    }
    handleCellClick(dataGridOptions, cellObject) {
        var _a;
        this.selectedGridInstance = dataGridOptions.gridComponentInstance;
        this.selectedGridOptions = dataGridOptions;
        this.selectedEntityInfo.gridName = dataGridOptions.gridName;
        this.selectedEntityInfo.columnList = dataGridOptions.columns;
        this.setGridInstanceList(this.selectedGridOptions);
        if ((_a = cellObject.column) === null || _a === void 0 ? void 0 : _a.dataField) {
            this.selectedColumn = Object.assign({}, dataGridOptions.columns.filter((item) => item.dataField === cellObject.column.dataField)[0]);
        }
        else {
            this.selectedColumn = null;
        }
    }
    setGridInstanceList(currentGridOptions) {
        var _a;
        this.gridComponentInstanceList = [];
        const gridName = currentGridOptions.gridName || this.options.gridOptions.gridName;
        const isMasterGridSelected = this.options.gridOptions.gridName === gridName && currentGridOptions.isMasterGrid;
        // this.conditionalFormattingTitle = isMasterGridSelected ? 'CONDITIONAL FORMATTING' : 'CONDITIONAL FORMATTING - CHILD VIEW';
        // Adding Master grid instance
        this.gridComponentInstanceList.push({
            isMasterGrid: true,
            gridName: this.options.gridOptions.gridName,
            gridComponentInstance: this.options.gridOptions.gridComponentInstance,
            isSelected: isMasterGridSelected
        });
        // Adding child grid instances
        if (this.options.gridOptions.childEntityList.length) {
            this.options.gridOptions.childEntityList[0].childGridInstanceList.forEach(childGridInstance => {
                this.gridComponentInstanceList.push({
                    isMasterGrid: false,
                    gridName: this.options.gridOptions.childEntityList[0].gridOptions.gridName,
                    gridComponentInstance: childGridInstance,
                    isSelected: !isMasterGridSelected,
                });
            });
        }
        const selectedGridInstance = this.gridComponentInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length);
        if (selectedGridInstance.length) {
            this.conditionalFormattingStorageKey = getStorageKey(selectedGridInstance[0].gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], selectedGridInstance[0].isMasterGrid);
            if (CGFFlyOutEnum.conditionFormatting === ((_a = this.currentSelectedFlyOut) === null || _a === void 0 ? void 0 : _a.id)) {
                this.selectedEntityInfo.columnList = this.prepareLookUpDataForConditonalFormatting(this.selectedEntityInfo.columnList);
            }
        }
        this.cachingEnabled = isMasterGridSelected ? this.isCached : false;
    }
    prepareLookUpDataForConditonalFormatting(columnList) {
        const arr = [];
        for (let index = 0; index < columnList.length; index++) {
            columnList[index].lookup = { dataSource: this.getDataLookDataSource(columnList[index].dataField) };
            // columnList[index].filterOperations = getFilterOperations(columnList[index].dataType);
        }
        return columnList;
    }
    getDataLookDataSource(dataField) {
        const arr = [];
        this.options.gridOptions.dataSource.forEach(item => {
            if (arr.indexOf(item[dataField]) === -1) {
                arr.push(item[dataField]);
            }
        });
        return [...new Set(arr)];
    }
    updateCachedDataSource() {
        this.options.gridOptions.gridComponentInstance.beginUpdate();
        this.cachedColumns.forEach(item => {
            this.options.gridOptions.gridComponentInstance.columnOption(item.dataField, 'visible', item.visible);
        });
        this.options.gridOptions.gridComponentInstance.option('dataSource', this.options.gridOptions.dataSource);
        this.options.gridOptions.gridComponentInstance.endUpdate();
    }
    tryToApplyInitialViews() {
        if (this.options.viewDataSource) {
            this.options.viewDataSource.getViews(this.options.viewDataSource.key).then(layouts => {
                this.options.viewList = layouts;
                if (this.isViewDefinedInQueryParams()) {
                    this.tryToApplyQueryParamLayout(layouts);
                }
                else {
                    this.tryToApplyDefaultLayout(layouts);
                }
            }).catch(() => {
                appToast({ type: 'error', message: 'Unable to retrieve the layouts for this grid' });
            });
        }
    }
    tryToApplyQueryParamLayout(views) {
        const queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        if (this.isViewDefinedInQueryParams()) {
            const queryView = views.find(f => f.id === Number(queryParams.layout));
            this.options.viewDataSource.applyView(queryView).then(gridView => {
                this.viewEvents({ event: CGFEventsEnum.applyView, data: gridView });
            }).catch(() => {
                appToast({ type: 'error', message: `The layout with ID: ${queryParams.layout} was not applied successfully` });
            });
        }
    }
    isViewDefinedInQueryParams() {
        const queryParams = this.activatedRoute.snapshot.queryParams;
        return queryParams.layout && queryParams.layout > 0;
    }
    tryToApplyDefaultLayout(views) {
        const defaultView = this.getDefaultView(views);
        if (defaultView) {
            this.options.viewDataSource.applyView(defaultView).then(gridView => {
                const queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
                queryParams.layout = gridView.id;
                this.router.navigate([], { queryParams });
                this.viewEvents({ event: CGFEventsEnum.applyView, data: gridView });
            }).catch(() => {
                appToast({ type: 'error', message: 'The default layout was not applied successfully' });
            });
        }
    }
    getDefaultView(views) {
        return views.filter(view => view.isDefault || view.isGlobalDefault)[0] || views[0] || null;
    }
    setFlyOutTitle() {
        if (this.currentSelectedFlyOut) {
            const isMasterSelected = this.selectedGridOptions.isMasterGrid;
            switch (this.currentSelectedFlyOut.id) {
                case CGFFlyOutEnum.conditionFormatting:
                    this.flyOutTitle = isMasterSelected ? 'CONDITIONAL FORMATTING' : 'CONDITIONAL FORMATTING - CHILD VIEW';
                    break;
                case CGFFlyOutEnum.customColumns:
                    this.flyOutTitle = isMasterSelected ? 'CUSTOM COLUMN' : 'CUSTOM COLUMN - CHILD VIEW';
                    break;
            }
        }
        else {
            this.flyOutTitle = '';
        }
    }
};
CommonGridFrameworkComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router }
];
__decorate([
    ViewChild(FlyOutActionIconContainerComponent)
], CommonGridFrameworkComponent.prototype, "appCGFContainer", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "options", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "securityIdColumnName", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "isEditSecurity", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "showEntityParameter", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "isCached", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "currentFlyOut", null);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "cgfFeatureUpdate", null);
__decorate([
    Output()
], CommonGridFrameworkComponent.prototype, "cgfEvent", void 0);
__decorate([
    Output()
], CommonGridFrameworkComponent.prototype, "loadChildEntityData", void 0);
__decorate([
    Output()
], CommonGridFrameworkComponent.prototype, "selectionChanged", void 0);
CommonGridFrameworkComponent = __decorate([
    Component({
        selector: 'pbi-common-grid-framework',
        template: "<ng-template [ngIf]=\"options.showCGF\">\r\n    <div class=\"action-container\">\r\n        <div *ngIf=\"containsVisibleParameters\" class=\"entity-param-with-edit-button-container\">\r\n            <div>\r\n                <div>\r\n                    <div class=\"data-entity-parameter-container\">\r\n                        <span class=\"ellipsis data-entity-parameter-edit-label\"\r\n                            title=\"To Edit/View Parameters click on edit icon.\">\r\n                            <span *ngFor=\"let arrItem of options.dataBrowserEntityParameters;let i = index;\">\r\n                                <ng-template *ngIf=\"!arrItem.HideInDataBrowser\">\r\n                                    <span> {{arrItem.Name}} : {{arrItem.Value}} </span>\r\n                                </ng-template>\r\n                                <ng-template\r\n                                    *ngIf=\"i < options.dataBrowserEntityParameters.length - 1 && !arrItem.HideInDataBrowser\">\r\n                                    <span>|</span>\r\n                                </ng-template>\r\n                            </span>\r\n                        </span>\r\n                        <span class=\"parameter-edit-icon-container\" (click)=\"updateParameterSectionVisibility()\">\r\n                            <i class=\"fas fa-pen pointer parameter-edit-icon\" title=\"Edit/View Parameters\"></i>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ng-template [ngIf]=\"options.showFlyoutButtonContainer\">\r\n            <pbi-fly-out-action-icon-container [position]=\"options.flyOutPosition\"\r\n                [visibleFlyOuts]=\"options.visibleFlyOuts\" [selectedFlyOut]=\"currentSelectedFlyOut\"\r\n                [resetContainerSettings]=\"resetSettings\" [showParametersControls]=\"showEntityParameter\"\r\n                [cgfLeftMenuList]=\"options.gridOptions.contextMenuMappingList\" [showLoader]=\"options.showLoader\"\r\n                [dataBrowserEntityParameters]=\"options.dataBrowserEntityParameters\"\r\n                [disableRefreshIcon]=\"disableRefresh\" (flyOutSelectionClick)=\"onFlyOutSelectionChange($event)\"\r\n                (refreshEntityData)=\"onRefreshEntityData()\">\r\n            </pbi-fly-out-action-icon-container>\r\n        </ng-template>\r\n    </div>\r\n    <div class=\"parameter-controls-container\" *ngIf=\"options.dataBrowserEntityParameters.length && showEntityParameter\">\r\n        <div>\r\n            <div>\r\n                <label class=\"parameter-header-label\">Parameters</label>\r\n                <i class=\"far fa-save pointer\" style=\"margin-left: 10px; font-size: 14px; color: #9b9ea0;\"\r\n                    title=\"Save order of parameters.\" (click)=\"saveOrderOfParameters()\"></i>\r\n            </div>\r\n            <div class=\" text-align-right parameters-close-button\">\r\n                <i class=\"fas fa-times pointer\" title=\"Close\" (click)=\"updateParameterSectionVisibility()\"></i>\r\n            </div>\r\n        </div>\r\n        <div class=\"parameter-controls-data-container\">\r\n            <pbi-entity-parameters [(dataBrowserEntityParameters)]=\"options.dataBrowserEntityParameters\"\r\n                (entityParamEvent)=\"entityParamEvent($event)\" (flyOutSelectionClick)=\"onFlyOutSelectionClick($event)\"\r\n                (disableRefreshIcon)=\"onFocusoutEntityParam($event)\">\r\n            </pbi-entity-parameters>\r\n        </div>\r\n    </div>\r\n    <div class=\"grid-and-fly-out-container\">\r\n        <div\r\n            [ngClass]=\"{'fly-out-close-grid-width': !currentSelectedFlyOut, 'fly-out-open-grid-width': currentSelectedFlyOut}\">\r\n            <ng-template [ngIf]=\"options.showGrid\">\r\n                <pbi-grid (gridInitialized)=\"onGridInitialized()\" [gridOptions]=\"options.gridOptions\"\r\n                    (gridChanged)=\"gridChanged($event)\" (gridCellClick)=\"cellClickEvent($event)\"\r\n                    (customButtonClick)=\"customButtonClick($event)\" (childEntityData)=\"getChildEntityData($event)\"\r\n                    (childEntityCellClick)=\"childEntityCellClick($event)\">\r\n                </pbi-grid>\r\n            </ng-template>\r\n        </div>\r\n        <div class=\"fly-out-container\" *ngIf=\"currentSelectedFlyOut?.id > 0\">\r\n            <div [ngSwitch]=\"currentSelectedFlyOut.id\">\r\n                <pbi-filter *ngSwitchCase=\"flyOutEnums.filter\" [filterGridOptions]=\"selectedGridOptions\"\r\n                    [gridInstanceList]=\"gridComponentInstanceList\" (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-filter>\r\n                <pbi-column-chooser *ngSwitchCase=\"flyOutEnums.columnChooser\"\r\n                    [(columnsList)]=\"options.gridOptions.columns\" [gridInstanceList]=\"gridComponentInstanceList\"\r\n                    [(selectedColumnInfo)]=\"selectedColumn\" [isCached]=\"isCached\" [useBulkApply]=\"options.useBulkApply\"\r\n                    [refreshOnColumnUpdate]=\"options.refreshOnColumnUpdate\" [debounce]=\"options.columnDebounce\"\r\n                    (closeCurrentFlyOut)=\"closeCurrentFlyOut()\" (applyButtonClick)=\"applyButtonClick($event)\">\r\n                </pbi-column-chooser>\r\n                <pbi-view-selection *ngSwitchCase=\"flyOutEnums.viewSelection\"\r\n                    [gridInstanceList]=\"gridComponentInstanceList\" [(viewList)]=\"options.viewList\"\r\n                    [(selectedTheme)]=\"options.gridOptions.selectedTheme\" [viewDataSource]=\"options.viewDataSource\"\r\n                    [(isGridBorderVisible)]=\"options.gridOptions.showColumnLines\" (viewEvent)=\"viewEvents($event)\"\r\n                    (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-view-selection>\r\n                <pbi-settings *ngSwitchCase=\"flyOutEnums.gridSettings\"\r\n                    [cgfSettingsOptions]=\"options.settingFlyOutOptions\"\r\n                    [(selectedTheme)]=\"options.gridOptions.selectedTheme\"\r\n                    [(selectedBorderSwitch)]=\"options.gridOptions.showColumnLines\"\r\n                    [gridInstanceList]=\"gridComponentInstanceList\" (gridSwitchValueChange)=\"onGridOptionChange($event)\"\r\n                    (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-settings>\r\n                <pbi-conditional-formatting *ngSwitchCase=\"flyOutEnums.conditionFormatting\"\r\n                    [listOfColumns]=\"selectedEntityInfo.columnList\" [storageKey]=\"conditionalFormattingStorageKey\"\r\n                    (valueChange)=\"changeInValueOfConditionalFormatting()\" (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-conditional-formatting>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</ng-template>\r\n",
        styles: [""]
    })
], CommonGridFrameworkComponent);

let ConditionalFormattingComponent = class ConditionalFormattingComponent {
    constructor() {
        this.groupOperations = ['and', 'or'];
        this.title = 'CONDITIONAL FORMATTING';
        this.valueChange = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this._listOfColumns = [];
        this._storageKey = CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
        this.applyType = 'Column';
        this.conditionFormattingControlVisible = false;
        this.copyOfListItems = [];
        this.filterApplyTypeCollection = ['Column', 'Row'];
        this.filterValue = null;
        this.hideColumnDropdown = true;
        this.listOfConditionalFormatting = [];
        this.operationOnColumn = '';
        this.selectedColumnData = null;
        this.selectedCondition = [];
    }
    set listOfColumns(colInfo) {
        this._listOfColumns = [];
        this.copyOfListItems = colInfo;
        colInfo.forEach(item => { this._listOfColumns.push({ dataField: item.dataField, caption: item.caption, dataType: item.dataType }); });
    }
    get listOfColumns() { return this.copyOfListItems; }
    set storageKey(key) {
        this._storageKey = key || CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
        this.ngOnInit();
    }
    ngOnInit() {
        this.hideFormattingInfo();
        this.listOfConditionalFormatting = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
    }
    ngOnDestroy() {
        this.listOfColumns.forEach(item => item.lookup = null);
    }
    addConditionalFormatting() {
        this.resetInfo();
        this.showConditionalFormatting();
    }
    editConditionalFormatting() {
        var _a;
        if ((_a = this.selectedCondition) === null || _a === void 0 ? void 0 : _a.length) {
            this.showConditionalFormatting();
        }
    }
    hideFormattingInfo() {
        this.conditionFormattingControlVisible = false;
        this.resetInfo();
    }
    updateConditionalFormatting() {
        if (this.operationOnColumn.trim() === '' && this.applyType.toLowerCase() === 'column') {
            appToast({ type: 'warning', message: `Please select 'Apply To Column'.` });
            return;
        }
        // this._dataSourceFilterValue = this.formatValue(this.filterValue);
        const currentFormatting = this.columnFormatComponent.formatObject;
        const existingFormattingInfo = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
        if (existingFormattingInfo.length && this.selectedCondition.length) { // added this condition which informs whether adding or editing condition
            for (let index = 0; index < existingFormattingInfo.length; index++) {
                if (existingFormattingInfo[index].formatRuleName === this.selectedCondition[0].formatRuleName) {
                    existingFormattingInfo[index] = currentFormatting;
                    existingFormattingInfo[index].condition = this.filterValue;
                    existingFormattingInfo[index].dataField = this.applyType.toLowerCase() === 'column' ? this.operationOnColumn : '___dataFieldForRow___'; // Adding dummy dataField for rowType
                    existingFormattingInfo[index].applyType = this.applyType.toLowerCase();
                    existingFormattingInfo[index].formatRuleName = this.getFormatName(this.title, index + 1);
                    break;
                }
            }
        }
        else {
            currentFormatting.condition = this.filterValue;
            currentFormatting.dataField = this.applyType.toLowerCase() === 'column' ? this.operationOnColumn : '___dataFieldForRow___'; // Adding dummy dataField for rowType
            currentFormatting.formatRuleName = this.getFormatName(this.title);
            currentFormatting.applyType = this.applyType.toLowerCase();
            existingFormattingInfo.push(currentFormatting);
        }
        sessionStorage.setItem(this._storageKey, JSON.stringify(existingFormattingInfo));
        this.prepareListOfConditions();
        this.hideFormattingInfo();
        setTimeout(() => { this.forceToApplyFormats(); }, 100); // will update list on UI then forced refresh.
    }
    forceToApplyFormats() {
        this.valueChange.emit({ data: JSON.parse(sessionStorage.getItem(this._storageKey)) });
    }
    onConditionalItemDeleted(e) {
        const _existingFormats = JSON.parse(sessionStorage.getItem(this._storageKey));
        const deletedItemIndex = _existingFormats.findIndex(item => item.formatRuleName === e.itemData.formatRuleName && item.dataField === e.itemData.dataField);
        if (deletedItemIndex > -1) {
            if (deletedItemIndex + 1 < _existingFormats.length) {
                _existingFormats.forEach((arrItem, index) => {
                    if (index > deletedItemIndex) {
                        arrItem.formatRuleName = this.getFormatName(this.title, index, arrItem.applyType, arrItem.dataField);
                    }
                });
            }
            _existingFormats.splice(deletedItemIndex, 1);
            sessionStorage.setItem(this._storageKey, JSON.stringify(_existingFormats));
            this.prepareListOfConditions();
            this.hideFormattingInfo();
            setTimeout(() => { this.forceToApplyFormats(); }, 100); // will update list on UI then forced refresh.
        }
    }
    showConditionalFormatting() {
        var _a, _b, _c, _d;
        this.operationOnColumn = ((_a = this.selectedCondition[0]) === null || _a === void 0 ? void 0 : _a.dataField) || this.operationOnColumn;
        this.conditionFormattingControlVisible = true;
        this.filterValue = (_b = this.selectedCondition[0]) === null || _b === void 0 ? void 0 : _b.condition;
        this.selectedColumnData = this.selectedCondition[0] ? Object.assign({}, this.selectedCondition[0]) : Object.assign({
            dataField: '__null__',
            backgroundColor: '',
            condition: '',
            cssClass: ((_d = (_c = this.selectedColumnData) === null || _c === void 0 ? void 0 : _c.cssClass) === null || _d === void 0 ? void 0 : _d.replace('bold', '').replace('underline', '').replace('italic', '')) || '',
            formatRuleName: '',
            textColor: '',
        });
        if (this.selectedColumnData.applyType) {
            this.applyType = this.selectedColumnData.applyType.charAt(0).toUpperCase() + this.selectedColumnData.applyType.slice(1);
        }
        else {
            this.applyType = this.filterApplyTypeCollection[0]; // Setting column
        }
        this.onApplyTypeChange();
    }
    prepareListOfConditions() {
        const conditionalFormats = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
        this.listOfConditionalFormatting = conditionalFormats || [];
    }
    onFormatSelectionChanged() { this.editConditionalFormatting(); }
    resetInfo() {
        this.selectedCondition = [];
        this.selectedColumnData = null;
        this.operationOnColumn = '';
        this.hideColumnDropdown = true;
    }
    onApplyTypeChange() {
        this.hideColumnDropdown = this.applyType.toLowerCase() === 'column';
        if (!this.hideColumnDropdown) {
            this.operationOnColumn = '';
        }
    }
    getFormatName(entityName, counter = this.listOfConditionalFormatting.length + 1, selectedApplyType = this.applyType, selectedOperationOnColumn = this.operationOnColumn) {
        selectedApplyType = selectedApplyType.toLowerCase() === 'row' ? selectedApplyType : selectedOperationOnColumn;
        return `${entityName.length ? entityName + ': ' : ''}${selectedApplyType} Format Rule ${counter}`;
    }
    onEditorPreparing(e) {
        var _a;
        // Dynamic control change based on the operation to be done.
        const filterLookUp = ((_a = this.listOfColumns.filter(item => item.dataField === e.dataField)[0]) === null || _a === void 0 ? void 0 : _a.lookup) || null;
        if (e.filterOperation === '=' && filterLookUp) {
            e.editorName = 'dxSelectBox';
            e.editorOptions.searchEnabled = true;
            if (filterLookUp) {
                e.editorOptions.dataSource = new DataSource({ store: filterLookUp.dataSource });
            }
            e.editorOptions.onValueChanged = (event) => {
                e.setValue(event.value);
            };
        }
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
};
__decorate([
    ViewChild(ColorFormatComponent)
], ConditionalFormattingComponent.prototype, "columnFormatComponent", void 0);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "listOfColumns", null);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "groupOperations", void 0);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "storageKey", null);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "title", void 0);
__decorate([
    Output()
], ConditionalFormattingComponent.prototype, "valueChange", void 0);
__decorate([
    Output()
], ConditionalFormattingComponent.prototype, "closeCurrentFlyOut", void 0);
ConditionalFormattingComponent = __decorate([
    Component({
        selector: 'pbi-conditional-formatting',
        template: "<div class=\"conditional-formatting-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-tools title-icon\"></i>\r\n            <div class=\"section-title\">{{title}}</div>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"widget-container accordion-data-container\">\r\n        <span class=\"add-icon-container\">\r\n            <i class=\"fas fa-plus fly-out-round-add-icon pointer\" (click)=\"addConditionalFormatting()\"></i>\r\n            <div class=\"add-rule-text\">Add Rules</div>\r\n        </span>\r\n        <span class=\"rule-list-container\" [class.dynamic-rule-list]=\"conditionFormattingControlVisible\">\r\n            <dx-list [items]=\"listOfConditionalFormatting\" allowItemDeleting=\"true\" itemDeleteMode=\"toggle\"\r\n                selectionMode=\"single\" keyExpr=\"formatRuleName\" displayExpr=\"formatRuleName\" searchExpr=\"formatRuleName\"\r\n                searchMode=\"contains\" [(selectedItems)]=\"selectedCondition\" [searchEnabled]=\"false\"\r\n                (onItemDeleted)=\"onConditionalItemDeleted($event)\" (onSelectionChanged)=\"onFormatSelectionChanged()\">\r\n                <!-- <dxo-item-dragging [data]=\"listOfConditionalFormatting\" [allowReordering]=\"true\" [onDragStart]=\"onDragStart\"\r\n                    [onDragEnd]=\"onDragEnd\"></dxo-item-dragging> -->\r\n                <div *dxTemplate=\"let data of 'item'\">\r\n                    <div class=\"list-text-wrap\">{{data.formatRuleName}}</div>\r\n                </div>\r\n            </dx-list>\r\n        </span>\r\n        <div *ngIf=\"conditionFormattingControlVisible\">\r\n            <div class=\"apply-format\">\r\n                <span class=\"action-container\">\r\n                    <!-- <i class=\"pointer\" (click)=\"updateConditionalFormatting()\"> Save </i>\r\n                <i class=\"fas fa-times pointer\" flow=\"down\" data-tooltip=\"Close\" (click)=\"hideFormattingInfo()\"></i> -->\r\n                </span>\r\n                <div class=\"applyTypeContainer\">\r\n                    <span class=\"label-padding\">Apply Type(Row/Column)</span>\r\n                    <dx-radio-group class=\"radioCollection\" [items]=\"filterApplyTypeCollection\" [(value)]=\"applyType\"\r\n                        layout=\"horizontal\" (onValueChanged)=\"onApplyTypeChange()\">\r\n                    </dx-radio-group>\r\n                </div>\r\n                <span *ngIf=\"hideColumnDropdown\">\r\n                    <div class=\"label-padding\">Apply To Column </div>\r\n                    <div class=\"apply-column\">\r\n                        <dx-select-box class=\"input-element column-list\" [dataSource]=\"_listOfColumns\"\r\n                            valueExpr=\"dataField\" displayExpr=\"caption\" [(value)]=\"operationOnColumn\"\r\n                            searchMode=\"contains\" searchEnabled=\"true\">\r\n                        </dx-select-box>\r\n                    </div>\r\n                </span>\r\n                <div class=\"conditional-filter\">\r\n                    <dx-filter-builder class=\"filter-container\" [disabled]=\"!conditionFormattingControlVisible\"\r\n                        [fields]=\"_listOfColumns\" [(value)]=\"filterValue\" [groupOperations]=\"groupOperations\"\r\n                        (onEditorPreparing)=\"onEditorPreparing($event)\">\r\n                    </dx-filter-builder>\r\n                </div>\r\n            </div>\r\n            <pbi-color-format [storageKey]=\"_storageKey\" [enableInputDataFieldCheck]=\"true\" [showBasicFormat]=\"true\"\r\n                [enableInputCaptionField]=\"false\" [enableFormat]=\"false\" [enablePin]=\"false\" [enableAlignment]=\"false\"\r\n                [enableStorage]=\"false\" [selectedColumnInfo]=\"selectedColumnData\">\r\n            </pbi-color-format>\r\n        </div>\r\n    </div>\r\n    <span class=\"button-container\" *ngIf=\"conditionFormattingControlVisible\">\r\n        <dx-button class=\"save-button\" text=\"Save\" type=\"default\" (onClick)=\"updateConditionalFormatting()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    })
], ConditionalFormattingComponent);

let FilterComponent = class FilterComponent {
    constructor() {
        this.closeCurrentFlyOut = new EventEmitter();
        this.applyButtonOptions = {
            text: 'Apply',
            type: 'default'
        };
        this.gridOptions = new Object();
        this.listOfColumns = [];
        this.title = 'FILTERS';
        this.showFilterSection = true;
    }
    set filterGridOptions(filterGridOptions) {
        var _a, _b;
        this.gridOptions = filterGridOptions;
        this.filterValue = ((_b = (_a = filterGridOptions === null || filterGridOptions === void 0 ? void 0 : filterGridOptions.gridComponentInstance) === null || _a === void 0 ? void 0 : _a.option()) === null || _b === void 0 ? void 0 : _b.filterValue) || this.filterGridOptions.gridFilterValue;
        this.listOfColumns = filterGridOptions.columns.filter((c) => c.allowFiltering !== false) || [];
    }
    get filterGridOptions() { return this.gridOptions; }
    set gridInstanceList(gridInstanceList) {
        var _a;
        this.selectedGridInstanceList = gridInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length)
            .map(item => item.gridComponentInstance);
        this.title = ((_a = gridInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length)[0]) === null || _a === void 0 ? void 0 : _a.isMasterGrid) ? 'FILTERS' : 'FILTERS - CHILD VIEW';
    }
    get gridInstanceList() { return this.selectedGridInstanceList; }
    ;
    applyFilter() {
        this.filterGridOptions.gridFilterValue = this.filterValue;
        // change
        // if (this.gridInstance) {
        //   this.gridInstance.beginUpdate();
        //   this.gridInstance.option('filterValue', this.filterValue);
        //   applyFiltersToGrid(this.gridInstance, this.filterValue);
        //   this.gridInstance.endUpdate();
        //   setTimeout(() => {
        //     const currentGridState = this.gridInstance.state();
        //     currentGridState.gridFilterValue = this.filterValue;
        //     this.gridInstance.state(currentGridState);
        //   }, 100);
        // }
        if (this.selectedGridInstanceList.length) {
            this.selectedGridInstanceList.forEach(gridInstance => {
                gridInstance.beginUpdate();
                gridInstance.option('filterValue', this.filterValue);
                applyFiltersToGrid(gridInstance, this.filterValue);
                gridInstance.endUpdate();
                setTimeout(() => {
                    const currentGridState = gridInstance.state();
                    currentGridState.gridFilterValue = this.filterValue;
                    gridInstance.state(currentGridState);
                }, 100);
            });
        }
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
};
__decorate([
    Input()
], FilterComponent.prototype, "filterGridOptions", null);
__decorate([
    Input()
], FilterComponent.prototype, "gridInstanceList", null);
__decorate([
    Output()
], FilterComponent.prototype, "closeCurrentFlyOut", void 0);
FilterComponent = __decorate([
    Component({
        selector: 'pbi-filter',
        template: "<div class=\"filter-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-filter title-icon\"></i>\r\n            <span class=\"section-title\">{{title}}</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"pointer property-tab-in-view-selection\" (click)=\"showFilterSection = !showFilterSection\">\r\n                <span>VISUAL</span>\r\n                <span [class.fa-angle-down]=\"!showFilterSection\" [class.fa-angle-up]=\"showFilterSection\"\r\n                    class=\"fa basic-format-toggle-icon\"></span>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n    <div class=\"accordion-data-container\" *ngIf=\"showFilterSection\">\r\n        <div>\r\n            <div class=\"filter-options\">\r\n                <dx-filter-builder class=\"filter-builder\" [fields]=\"listOfColumns\" [(value)]=\"filterValue\"\r\n                    [allowHierarchicalFields]=\"true\">\r\n                </dx-filter-builder>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <span class=\"button-container\">\r\n        <dx-button class=\"apply-filter-button\" [text]=\"applyButtonOptions.text\" [type]=\"applyButtonOptions.type\"\r\n            (onClick)=\"applyFilter()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    })
], FilterComponent);

let GridComponent = class GridComponent {
    constructor() {
        this.gridOptions = null;
        /* These output events provide you flexibility to handle certain scenarios where parent need to perform
        certain action(s) based on grid events which is sometime need to be delegate to parent rather than using gridOptions*/
        this.gridCellClick = new EventEmitter();
        this.gridCellPrepared = new EventEmitter();
        this.gridContentReady = new EventEmitter();
        this.gridContextMenuPreparing = new EventEmitter();
        this.gridEditingStart = new EventEmitter();
        this.gridEditorPrepared = new EventEmitter();
        this.gridEditorPreparing = new EventEmitter();
        this.gridExporting = new EventEmitter();
        this.gridInitNewRow = new EventEmitter();
        this.gridInitialized = new EventEmitter();
        this.gridReorder = new EventEmitter();
        this.gridRowClick = new EventEmitter();
        this.gridRowPrepared = new EventEmitter();
        this.gridRowRemoved = new EventEmitter();
        this.gridRowValidating = new EventEmitter();
        this.gridSelectionChanged = new EventEmitter();
        //#region PBI Grid Event Custom One
        this.childEntityCellClick = new EventEmitter();
        this.childEntityData = new EventEmitter();
        this.customButtonClick = new EventEmitter();
        this.gridChanged = new EventEmitter();
        this.userDefinedCustomButtonClick = new EventEmitter();
        //#endregion
        this.dropDownOptions = {
            resizeEnabled: true,
            width: 'auto',
            onContentReady: (e) => {
                const DOM = e.component._$content;
                const instance = Resizable.getInstance(DOM);
                instance.option('handles', 'left right');
            }
        };
    }
    onCellClick(info) {
        if (this.gridOptions.onCellClick) {
            this.gridOptions.onCellClick(info);
        }
        this.gridCellClick.emit(info);
    }
    onCellPrepared(info) {
        if (this.gridOptions.onCellPrepared) {
            this.gridOptions.onCellPrepared(info);
        }
        this.gridCellPrepared.emit(info);
    }
    onContentReady(gridObject) {
        if (this.gridOptions.onContentReady) {
            this.gridOptions.onContentReady(gridObject);
        }
        applyFiltersToGrid(this.gridOptions.gridComponentInstance, this.gridOptions.gridComponentInstance.option().filterValue);
        this.gridContentReady.emit(gridObject);
    }
    onContextMenuPreparing(info) {
        if (this.gridOptions.onContextMenuPreparing) {
            this.gridOptions.onContextMenuPreparing(info);
        }
        this.gridContextMenuPreparing.emit(info);
    }
    onEditingStart(info) {
        if (this.gridOptions.onEditingStart) {
            this.gridOptions.onEditingStart(info);
        }
        this.gridEditingStart.emit(info);
    }
    onEditorPrepared(info) {
        if (this.gridOptions.onEditorPrepared) {
            this.gridOptions.onEditorPrepared(info);
        }
        this.gridEditorPrepared.emit(info);
    }
    onEditorPreparing(info) {
        if (this.gridOptions.onEditorPreparing) {
            this.gridOptions.onEditorPreparing(info);
        }
        this.gridEditorPreparing.emit(info);
    }
    onExporting(info) {
        if (this.gridOptions.onExporting) {
            this.gridOptions.onExporting(info);
        }
        this.gridExporting.emit(info);
    }
    onInitNewRow(info) {
        if (this.gridOptions.onInitNewRow) {
            this.gridOptions.onInitNewRow(info);
        }
        this.gridInitNewRow.emit(info);
    }
    onInitialized(gridObject) {
        this.gridOptions.gridComponentInstance = gridObject.component;
        if (this.gridOptions.onInitialized) {
            this.gridOptions.onInitialized(gridObject);
        }
        this.gridInitialized.emit(gridObject);
    }
    onReorder(e) {
        if (this.gridOptions.onReorder) {
            this.gridOptions.onReorder(e);
        }
        this.gridReorder.emit(e);
    }
    onRowClick(info) {
        if (this.gridOptions.onRowClick) {
            this.gridOptions.onRowClick(info);
        }
    }
    onRowPrepared(info) {
        if (this.gridOptions.onRowPrepared) {
            this.gridOptions.onRowPrepared(info);
        }
    }
    onRowRemoved(info) {
        if (this.gridOptions.onRowRemoved) {
            this.gridOptions.onRowRemoved(info);
        }
    }
    onRowUpdating(info) {
        if (this.gridOptions.onRowUpdating) {
            this.gridOptions.onRowUpdating(info);
        }
        this.gridReorder.emit(info);
    }
    onRowValidating(info) {
        if (this.gridOptions.onRowValidating) {
            this.gridOptions.onRowValidating(info);
        }
    }
    onSelectionChanged(info) {
        if (this.gridOptions.onSelectionChanged) {
            this.gridOptions.onSelectionChanged(info);
        }
    }
    //#region Template related functions
    customActionButtonClick(evt, rowData, btnType) {
        if (this.gridOptions.onCustomButtonClick) {
            this.gridOptions.onCustomButtonClick({ evt, rowData, btnType });
        }
        this.customButtonClick.emit({ evt, rowData, btnType });
    }
    customActionButtonTemplateClick(evt, rowData, actionItem) {
        if (this.gridOptions.onUserDefinedCustomButtonClick) {
            this.gridOptions.onUserDefinedCustomButtonClick({ evt, rowData, actionItem });
        }
        this.userDefinedCustomButtonClick.emit({ evt, rowData, actionItem });
    }
    onLookupValueChanged(args, cellData) {
        cellData.setValue(cellData.value);
    }
    onLookupGridContentReady(args, cellData) {
        args.component.columnOption('DisplayValue', 'visible', false);
        if (args.component.option('selection.mode') === 'multiple' && cellData.value && !(cellData.value instanceof Array)) {
            const arr = [];
            arr.push(cellData.value);
            cellData.value = arr;
        }
    }
};
__decorate([
    Input()
], GridComponent.prototype, "gridOptions", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridCellClick", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridCellPrepared", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridContentReady", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridContextMenuPreparing", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridEditingStart", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridEditorPrepared", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridEditorPreparing", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridExporting", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridInitNewRow", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridInitialized", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridReorder", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridRowClick", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridRowPrepared", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridRowRemoved", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridRowValidating", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridSelectionChanged", void 0);
__decorate([
    Output()
], GridComponent.prototype, "childEntityCellClick", void 0);
__decorate([
    Output()
], GridComponent.prototype, "childEntityData", void 0);
__decorate([
    Output()
], GridComponent.prototype, "customButtonClick", void 0);
__decorate([
    Output()
], GridComponent.prototype, "gridChanged", void 0);
__decorate([
    Output()
], GridComponent.prototype, "userDefinedCustomButtonClick", void 0);
GridComponent = __decorate([
    Component({
        selector: 'pbi-grid',
        template: "<dx-data-grid id=\"gridContainer\" class=\"{{gridOptions.selectedThemeClass}}\" [dataSource]=\"gridOptions.dataSource\"\r\n    [height]=\"gridOptions.height\" [accessKey]=\"gridOptions.accessKey\"\r\n    [activeStateEnabled]=\"gridOptions.enableActiveState\" [allowColumnReordering]=\"gridOptions.allowColumnReordering\"\r\n    [allowColumnResizing]=\"gridOptions.allowColumnResizing\"\r\n    [autoNavigateToFocusedRow]=\"gridOptions.autoNavigateToFocusedRow\" [columnAutoWidth]=\"gridOptions.columnAutoWidth\"\r\n    [cacheEnabled]=\"gridOptions.enableCache\" [cellHintEnabled]=\"gridOptions.enableCellHint\"\r\n    [columnHidingEnabled]=\"gridOptions.enableColumnHiding\" [columnMinWidth]=\"gridOptions.columnMinWidth\"\r\n    [columnResizingMode]=\"gridOptions.columnResizingMode\" [columns]=\"gridOptions.columns\"\r\n    [customizeColumns]=\"gridOptions.customizeColumns\" [dataSource]=\"gridOptions.dataSource\"\r\n    [dateSerializationFormat]=\"gridOptions.dateSerializationFormat\" [disabled]=\"gridOptions.disabled\"\r\n    [errorRowEnabled]=\"gridOptions.enableErrorRow\" [filterSyncEnabled]=\"gridOptions.filterSyncEnabled\"\r\n    [filterValue]=\"gridOptions.filterValue\" [height]=\"gridOptions.height\"\r\n    [highlightChanges]=\"gridOptions.highlightChanges\" [hoverStateEnabled]=\"gridOptions.hoverStateEnabled\"\r\n    [noDataText]=\"gridOptions.noDataText\" [repaintChangesOnly]=\"gridOptions.repaintChangesOnly\"\r\n    [remoteOperations]=\"gridOptions.remoteOperationsEnabled\" [rowAlternationEnabled]=\"gridOptions.rowAlternationEnabled\"\r\n    [showBorders]=\"gridOptions.showBorders\" [showColumnHeaders]=\"gridOptions.showColumnHeaders\"\r\n    [showColumnLines]=\"gridOptions.showColumnLines\" [showRowLines]=\"gridOptions.showRowLines\"\r\n    (onCellClick)=\"onCellClick($event)\" (onCellPrepared)=\"onCellPrepared($event)\"\r\n    (onContentReady)=\"onContentReady($event)\" (onContextMenuPreparing)=\"onContextMenuPreparing($event)\"\r\n    (onEditingStart)=\"onEditingStart($event)\" (onEditorPrepared)=\"onEditorPrepared($event)\"\r\n    (onEditorPreparing)=\"onEditorPreparing($event)\" (onExporting)=\"onExporting($event)\"\r\n    (onInitNewRow)=\"onInitNewRow($event)\" (onInitialized)=\"onInitialized($event)\" (onRowClick)=\"onRowClick($event)\"\r\n    (onRowPrepared)=\"onRowPrepared($event)\" (onRowRemoved)=\"onRowRemoved($event)\"\r\n    (onRowUpdating)=\"onRowUpdating($event)\" (onRowValidating)=\"onRowValidating($event)\"\r\n    (onSelectionChanged)=\"onSelectionChanged($event)\">\r\n\r\n    <!-- #region Start Grid Options which help to enable or disable grid features -->\r\n    <dxo-editing [mode]=\"gridOptions.editingMode\" [allowUpdating]=\"gridOptions.allowUpdating\"\r\n        [allowAdding]=\"gridOptions.allowAdding\" [allowDeleting]=\"gridOptions.allowDeleting\"\r\n        [useIcons]=\"gridOptions.useIcons\" [refreshMode]=\"gridOptions.refreshMode\"></dxo-editing>\r\n    <dxo-column-chooser [enabled]=\"gridOptions.enableColumnChooser\"></dxo-column-chooser>\r\n    <dxo-column-fixing [enabled]=\"gridOptions.enableColumnFixing\"></dxo-column-fixing>\r\n    <dxo-export [enabled]=\"gridOptions.allowDataExport\" [fileName]=\"gridOptions.gridName\"\r\n        [allowExportSelectedData]=\"gridOptions.allowSelectedDataExport\"></dxo-export>\r\n    <dxo-filter-panel [visible]=\"gridOptions.showFilterPanel\"></dxo-filter-panel>\r\n    <dxo-filter-row [visible]=\"gridOptions.showFilterRow\"></dxo-filter-row>\r\n    <dxo-group-panel [visible]=\"gridOptions.showGroupPanel\"></dxo-group-panel>\r\n    <dxo-grouping #expand [autoExpandAll]=\"gridOptions.autoExpandAll\"\r\n        [contextMenuEnabled]=\"gridOptions.enableContextGrpMenu\">\r\n    </dxo-grouping>\r\n    <dxo-load-panel [enabled]=\"gridOptions.enableLoadPanel\"></dxo-load-panel>\r\n    <dxo-header-filter [visible]=\"gridOptions.showHeaderFilter\"></dxo-header-filter>\r\n    <dxo-pager [showPageSizeSelector]=\"gridOptions.showPageSizeSelector\"\r\n        [allowedPageSizes]=\"gridOptions.allowedPageSizes\" [showInfo]=\"gridOptions.showPageInfo\"\r\n        [visible]=\"gridOptions.showPager\"></dxo-pager>\r\n    <dxo-paging [pageSize]=\"gridOptions.pageSize\" [enabled]=\"gridOptions.pagingEnabled\"></dxo-paging>\r\n    <dxo-row-dragging [allowReordering]=\"gridOptions.allowReordering\" [showDragIcons]=\"gridOptions.showDragIcons\"\r\n        [onReorder]=\"onReorder\">\r\n    </dxo-row-dragging>\r\n    <dx-scroll-view #scrollView id=\"scrollView\" [showScrollbar]=\"gridOptions.showScrollbars\"></dx-scroll-view>\r\n    <dxo-scrolling [useNative]=\"false\" [mode]=\"gridOptions.scrollMode\"\r\n        columnRenderingMode=\"gridOptions.columnRenderingMode\"></dxo-scrolling>\r\n    <dxo-sorting [mode]=\"gridOptions.sortingType\"></dxo-sorting>\r\n    <dxo-selection [mode]=\"gridOptions.selectionMode\"></dxo-selection>\r\n    <dxo-search-panel [visible]=\"gridOptions.showSearchPanel\" placeholder=\"Search...\"></dxo-search-panel>\r\n    <dxo-state-storing [enabled]=\"gridOptions.enableStateStoring\" [type]=\"gridOptions.stateStorageType\"\r\n        [storageKey]=\"gridOptions.gridName\">\r\n    </dxo-state-storing>\r\n    <!-- #endregion End Grid Options which help to enable or disable grid features -->\r\n\r\n    <!-- #region Templates for Action Columns -->\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'actionButtonTemplate'\">\r\n        <i class=\"far fa-edit\" title=\"Edit\" (click)=\"customActionButtonClick($event, rowData, 'edit')\"></i>\r\n        <i class=\"far fa-trash-alt\" title=\"Delete\" (click)=\"customActionButtonClick($event, rowData, 'delete')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'actionEditButtonTemplate'\">\r\n        <i class=\"far fa-edit\" title=\"Edit\" (click)=\"customActionButtonClick($event, rowData, 'edit')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'viewRunButtonTemplate'\">\r\n        <i class=\"far fa-eye\" title=\"View Job\" (click)=\"customActionButtonClick($event, rowData, 'view')\"></i>\r\n        <i class=\"fas fa-play\" title=\"Run Job\" (click)=\"customActionButtonClick($event, rowData, 'run')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'viewEditDeleteButtonTemplate'\">\r\n        <i class=\"far fa-eye\" title=\"View\" (click)=\"customActionButtonClick($event, rowData, 'view')\"></i>\r\n        <i class=\"far fa-edit\" title=\"Edit\" (click)=\"customActionButtonClick($event, rowData, 'edit')\"></i>\r\n        <i class=\"far fa-trash-alt\" title=\"Delete\" (click)=\"customActionButtonClick($event, rowData, 'delete')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'customActionButtonTemplate'\">\r\n        <ng-template *ngFor=\"let item of gridOptions.contextMenuMappingList\">\r\n            <i *ngIf=\"item.ShowAsContextMenu && item.IconClassName\" attr.title=\"{{item.DisplayName}}\"\r\n                class=\"{{item.IconClassName}}\" (click)=\"customActionButtonTemplateClick($event, rowData, item)\"></i>\r\n            <span *ngIf=\"item.ShowAsContextMenu && !item.IconClassName\" class=\"customActionIconText\"\r\n                (click)=\"customActionButtonTemplateClick($event, rowData, item )\">{{item.DisplayName}}</span>\r\n        </ng-template>\r\n    </div>\r\n\r\n    <!-- #endregion Templates for Action Columns -->\r\n\r\n    <!-- #region Templates for Ref Data Controls -->\r\n    <div *dxTemplate=\"let cellData of 'ddBoxCellTemplate'\">\r\n        <dx-drop-down-box [disabled]=\"true\" [(value)]=\"cellData.value\" [dataSource]=\"cellData.column.lookup.dataSource\"\r\n            [valueExpr]=\"cellData.column.lookup.valueExpr\" [displayExpr]=\"cellData.column.lookup.displayExpr\">\r\n            <dxo-drop-down-options [height]=\"500\"></dxo-drop-down-options>\r\n        </dx-drop-down-box>\r\n    </div>\r\n\r\n    <div *dxTemplate=\"let cellData of 'ddBoxEditMultiTemplate'\">\r\n        <dx-drop-down-box [(value)]=\"cellData.value\" [dataSource]=\"cellData.column.lookup.dataSource\"\r\n            [valueExpr]=\"cellData.column.lookup.valueExpr\" [displayExpr]=\"cellData.column.lookup.displayExpr\"\r\n            (onValueChanged)=\"onLookupValueChanged($event, cellData)\" [dropDownOptions]=\"dropDownOptions\">\r\n            <dxo-drop-down-options [height]=\"500\"></dxo-drop-down-options>\r\n            <div *dxTemplate=\"let data of 'content'\">\r\n                <dx-data-grid id=\"lookupGrid\" [keyExpr]=\"cellData.column.lookup.valueExpr\" class=\"lookup-grid-height\"\r\n                    [dataSource]=\"cellData.column.lookup.dataSource\" [(selectedRowKeys)]=\"cellData.value\"\r\n                    (onContentReady)=\"onLookupGridContentReady($event, cellData)\" [allowColumnResizing]=\"true\"\r\n                    columnResizingMode=\"widget\">\r\n                    <dxo-filter-row visible=\" true\"></dxo-filter-row>\r\n                    <dxo-scrolling mode=\"infinite\">\r\n                    </dxo-scrolling>\r\n                    <dxo-selection [allowSelectAll]=\"false\" showCheckBoxesMode=\"always\" mode='multiple'>\r\n                    </dxo-selection>\r\n                    <dxo-paging [enabled]=\"false\"></dxo-paging>\r\n                </dx-data-grid>\r\n            </div>\r\n        </dx-drop-down-box>\r\n    </div>\r\n\r\n    <div *dxTemplate=\"let cellData of 'ddBoxEditSingleTemplate'\">\r\n        <dx-drop-down-box [(value)]=\"cellData.value\" [dataSource]=\"cellData.column.lookup.dataSource\"\r\n            [valueExpr]=\"cellData.column.lookup.valueExpr\" [displayExpr]=\"cellData.column.lookup.displayExpr\"\r\n            (onValueChanged)=\"onLookupValueChanged($event, cellData)\" [dropDownOptions]=\"dropDownOptions\">\r\n            <dxo-drop-down-options [height]=\"500\"></dxo-drop-down-options>\r\n            <div *dxTemplate=\"let data of 'content'\">\r\n                <dx-data-grid id=\"lookupGrid\" [keyExpr]=\"cellData.column.lookup.valueExpr\" class=\"lookup-grid-height\"\r\n                    [dataSource]=\"cellData.column.lookup.dataSource\" [(selectedRowKeys)]=\"cellData.value\"\r\n                    (onContentReady)=\"onLookupGridContentReady($event, cellData)\" [allowColumnResizing]=\"true\"\r\n                    columnResizingMode=\"widget\" [width]='cellData.column.lookup.width'>\r\n                    <dxo-filter-row visible=\" true\"></dxo-filter-row>\r\n                    <dxo-scrolling mode=\"infinite\"></dxo-scrolling>\r\n                    <dxo-selection [allowSelectAll]=\"false\" showCheckBoxesMode=\"always\" mode='single'>\r\n                    </dxo-selection>\r\n                    <dxo-paging [enabled]=\"false\"></dxo-paging>\r\n                </dx-data-grid>\r\n            </div>\r\n        </dx-drop-down-box>\r\n    </div>\r\n    <!-- #endregion Templates for Ref data Controls -->\r\n\r\n</dx-data-grid>\r\n",
        styles: [""]
    })
], GridComponent);

let SettingsComponent = class SettingsComponent {
    constructor(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.cgfSettingsOptions = [];
        this.gridSwitchValueChange = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.hideGridBorder = false;
        this.radioGroup = ['Auto Fit'];
        this.radioGroupValue = null;
        this.showFormattingSection = true;
        this.themeList = [
            { key: 'light.regular', value: 'Cozy' },
            { key: 'light.compact', value: 'Regular' },
            { key: 'np.compact', value: 'Default' }
        ];
        this.exportActionList = [];
        this.cgfSettingsEnum = CGFSettingsEnum;
        this.currentAppliedTheme = 'np.compact';
    }
    set selectedTheme(themeName) {
        if (sessionStorage.getItem('theme')) {
            this.currentAppliedTheme = sessionStorage.getItem('theme');
        }
        else {
            this.currentAppliedTheme = themeName;
        }
    }
    get selectedTheme() { return this.currentAppliedTheme; }
    set selectedBorderSwitch(val) {
        this.hideGridBorder = val;
    }
    get selectedBorderSwitch() { return this.hideGridBorder; }
    ngOnInit() {
        this.exportActionList = SettingExportOptions.filter(option => this.cgfSettingsOptions.indexOf(option.id) > -1);
    }
    onThemeChanged() {
        sessionStorage.setItem('theme', this.currentAppliedTheme);
        const appliedThemeName = getClassNameByThemeName(this.currentAppliedTheme);
        this.gridInstanceList.forEach(item => {
            let existingClasses = item.gridComponentInstance._$element[0].className;
            if (existingClasses.indexOf('dx-swatch-default') > -1) {
                existingClasses = existingClasses.replace(new RegExp('dx-swatch-default', 'g'), '');
            }
            else if (existingClasses.indexOf('dx-swatch-regular') > -1) {
                existingClasses = existingClasses.replace(new RegExp('dx-swatch-regular', 'g'), '');
            }
            item.gridComponentInstance._$element[0].className = `${existingClasses} ${appliedThemeName}`;
        });
        this.emitToggleValue('theme', this.currentAppliedTheme);
    }
    onHideGridChange() {
        this.emitToggleValue('gridLines', this.hideGridBorder);
    }
    onClickOfDEMButton() {
        // const params = {
        //   webMenuId: getWebMenuIdByKey('DataEntityManagement'),
        //   entityId: this.activatedRoute.snapshot.queryParams.entity,
        //   activeTab: 'databrowserentity'
        // };
        // this.router.navigate(['/dataEntityManagement'], { queryParams: params });
    }
    onAutoFitChange() {
        let gridInstanceMaxCol = {
            gridComponentInstance: new Object(),
            gridName: '',
            isMasterGrid: false,
            isSelected: false
        };
        let existColCount = 0;
        this.gridInstanceList.forEach(gridInstance => {
            var _a;
            const visibleColCount = ((_a = gridInstance.gridComponentInstance.getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.length) || 0;
            if (existColCount < visibleColCount) {
                gridInstanceMaxCol = gridInstance;
                existColCount = visibleColCount;
            }
        });
        gridInstanceMaxCol.gridComponentInstance.beginUpdate();
        const colCount = gridInstanceMaxCol.gridComponentInstance.columnCount();
        for (let i = 0; i < colCount; i++) {
            if (gridInstanceMaxCol.gridComponentInstance.columnOption(i, 'visible')) {
                gridInstanceMaxCol.gridComponentInstance.columnOption(i, 'width', 'auto');
            }
        }
        gridInstanceMaxCol.gridComponentInstance.endUpdate();
    }
    emitToggleValue(key, value) {
        const _data = { key, value };
        this.gridSwitchValueChange.emit(_data);
    }
    onWorkFlowEmailClick() {
        // const redirectURL =
        //   this.configService.config.WorkFlowMasterBaseURL + '/#/creator/exportreportingplatform?url=' + encodeURIComponent(location.href);
        // open(redirectURL, '_blank');
    }
    getKey() {
        // this.cgfService.getApiKey().subscribe(response => { this.generateUrlSuccess(response); }, () => {
        //   apiCallError('Error while generating key. Please try again.');
        // });
    }
    generateUrlSuccess(response) {
        // if (response.ToasterType.toLowerCase().trim() === 'success') {
        //   const urlParams = location.hash.split('?')[1];
        //   const url = `${APIEndPoints.dataBrowser.excelData}?${urlParams}&key=${response.Message}`;
        //   copyTextToClipBoard(url);
        //   appToastr({ type: 'success', message: 'Link generated and copied successfully.' });
        // } else if (response.ToasterType.toLowerCase().trim() === 'error') {
        //   appToastr({ type: 'error', message: response.Message || 'Error while generating key. Please try again.' });
        // }
    }
    exportSectionButtonClick(data) {
        switch (data.key) {
            case 'exportExcel':
                const selectedInstance = this.gridInstanceList.filter(item => item.isSelected)[0];
                if (selectedInstance) {
                    // When the selectionOnly parameter is false - the method exports all rows, when true - only the selected ones.
                    selectedInstance.gridComponentInstance.exportToExcel(false);
                }
                break;
            case 'excelLink':
                this.getKey();
                break;
            case 'emailReport':
                this.onWorkFlowEmailClick();
                break;
        }
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
};
SettingsComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router }
];
__decorate([
    Input()
], SettingsComponent.prototype, "cgfSettingsOptions", void 0);
__decorate([
    Input()
], SettingsComponent.prototype, "selectedTheme", null);
__decorate([
    Input()
], SettingsComponent.prototype, "selectedBorderSwitch", null);
__decorate([
    Input()
], SettingsComponent.prototype, "gridInstanceList", void 0);
__decorate([
    Output()
], SettingsComponent.prototype, "gridSwitchValueChange", void 0);
__decorate([
    Output()
], SettingsComponent.prototype, "closeCurrentFlyOut", void 0);
SettingsComponent = __decorate([
    Component({
        selector: 'pbi-settings',
        template: "<div class=\"settings-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-cog title-icon\"></i>\r\n            <span class=\"section-title\">Settings</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"pointer property-tab-in-view-selection\" (click)=\"showFormattingSection = !showFormattingSection\">\r\n                <span>FORMATTING</span>\r\n                <span [class.fa-angle-down]=\"!showFormattingSection\" [class.fa-angle-up]=\"showFormattingSection\"\r\n                    class=\"fa basic-format-toggle-icon\"></span>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n    <div class=\"accordion-data-container\" *ngIf=\"showFormattingSection\">\r\n        <div class=\"setting-content\">\r\n            <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.theme) > -1\">\r\n                <label>THEME</label>\r\n                <div>\r\n                    <dx-select-box class=\"theme-dropdown\" [items]=\"themeList\" valueExpr=\"key\" displayExpr=\"value\"\r\n                        placeholder=\"Select Theme\" [(value)]=\"currentAppliedTheme\" (onValueChanged)=\"onThemeChanged()\">\r\n                        <div [class.selected-theme]=\"currentAppliedTheme === data.key\" *dxTemplate=\"let data of 'item'\">\r\n                            <div class=\"theme-item\">\r\n                                <div class=\"theme-name\">\r\n                                    {{data.value}}\r\n                                </div>\r\n                                <i class=\"fa fa-check theme-icon\" *ngIf=\"currentAppliedTheme === data.key\"></i>\r\n                            </div>\r\n                        </div>\r\n                    </dx-select-box>\r\n                </div>\r\n            </div>\r\n            <div class=\"grid-control-container\">\r\n                <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.gridLines) > -1\">\r\n                    <label>GRIDLINES</label>\r\n                    <div>\r\n                        <!-- <app-bool-input [(State)]=\"hideGridBorder\" (StateChange)=\"onHideGridChange($event)\">\r\n                        </app-bool-input> -->\r\n                        <label class=\"switch\">\r\n                            <!-- <app-bool-input [(State)]=\"isPinned\"></app-bool-input> -->\r\n                            <span>\r\n                                <label class=\"switch\">\r\n                                    <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"hideGridBorder\"\r\n                                        (ngModelChange)=\"onHideGridChange()\">\r\n                                    <span class=\"slider round\"></span>\r\n                                </label>\r\n                            </span>\r\n                        </label>\r\n\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.autoFit) > -1\">\r\n                    <dx-button class=\"settings-custom-btn-dem auto-fit\" (onClick)=\"onAutoFitChange()\"\r\n                        template=\"DEMButtonTemplate\">\r\n                        <div class=\"DEMButtonTemplate\" *dxTemplate=\"let buttonData of 'DEMButtonTemplate'\">\r\n                            <i class=\"fas fa-arrows-alt-h\"></i>\r\n                            <div class=\"button-text\">Auto Fit</div>\r\n                        </div>\r\n                    </dx-button>\r\n                </div>\r\n            </div>\r\n            <div class=\"section\" *ngIf=\"exportActionList.length > 0\">\r\n                <label>EXPORTS</label>\r\n                <div class=\"export-button-containers\">\r\n                    <div *ngFor=\"let item of exportActionList\">\r\n                        <dx-button class=\"settings-custom-btn\" template=\"buttonTemplate\"\r\n                            (click)=\"exportSectionButtonClick(item)\">\r\n                            <div class=\"button-template\" *dxTemplate=\"let buttonData of 'buttonTemplate'\">\r\n                                <div>\r\n                                    <i class=\"{{item.icon}}\"></i>\r\n                                </div>\r\n                                <div class=\"button-text\">{{item.title}}</div>\r\n                            </div>\r\n                        </dx-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"section\" *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.advDem) > -1\">\r\n                <label>ADVANCED MANAGEMENT</label>\r\n                <div>\r\n                    <div>\r\n                        <dx-button class=\"settings-custom-btn-dem\" (onClick)=\"onClickOfDEMButton()\"\r\n                            template=\"DEMButtonTemplate\">\r\n                            <div class=\"DEMButtonTemplate\" *dxTemplate=\"let buttonData of 'DEMButtonTemplate'\">\r\n                                <i class=\"fas fa-wrench\"></i>\r\n                                <div class=\"button-text\">Data Entity Management</div>\r\n                            </div>\r\n                        </dx-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
        styles: [""]
    })
], SettingsComponent);

class ViewSelectionModel {
    constructor(args) {
        this.canDefault = false;
        this.canDelete = false;
        this.canGlobalDefault = false;
        this.canSave = false;
        this.canSaveNew = false;
        this.id = -1;
        this.isDefault = false;
        this.isGlobalDefault = false;
        this.isPinned = false;
        this.isPublic = false;
        this.name = '';
        if (args) {
            this.canDefault = args.CanDefault ? args.CanDefault : false;
            this.canDelete = args.CanDelete ? args.CanDelete : false;
            this.canGlobalDefault = args.CanGlobalDefault ? args.CanGlobalDefault : false;
            this.canSave = args.CanSave ? args.CanSave : false;
            this.canSaveNew = args.CanSaveNew ? args.CanSaveNew : false;
            this.id = args.Id ? args.Id : -1;
            this.isDefault = args.IsDefault ? args.IsDefault : false;
            this.isPublic = args.IsPublic ? args.IsPublic : false;
            this.name = args.Name ? args.Name : '';
            this.isGlobalDefault = args.IsGlobalDefault ? args.IsGlobalDefault : false;
            this.isPinned = args.IsPinned ? args.IsPinned : false;
        }
    }
}

let ViewSelectionComponent = class ViewSelectionComponent {
    constructor(activatedRoute, router, utilityService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.utilityService = utilityService;
        this.isGridBorderVisible = false;
        this.viewDataSource = new Object();
        this.viewEvent = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.defaults = [];
        this.currentSelectLayoutInfo = new Object();
        this.dropDownOptions = [{ value: 1, name: 'Save As' }];
        this.defaultTheme = 'np.compact';
        this.gridLayoutSettings = {
            colDef: [],
            filters: [],
            getCombinedFilter: [],
            summary: [],
            childGridLayout: [],
            gridName: ''
        };
        this.filterLayoutName = '';
        this.filterValue = null;
        this.groupLayoutList = [];
        this.isLayoutNone = false;
        this.isPinned = false;
        this.layoutDefaultData = [{ key: 1, value: 'User Default' }, { key: 2, value: 'Global Default' }];
        this.layoutList = [];
        this.routerSubscribe = null;
        this.selectedLayoutDefaultIds = [];
        this.selectedViewVisibility = 'Private';
        this.showFilterControlInput = false;
        this.showModalPopUp = false;
        this.showSaveButtonMenuList = false;
        this.themeList = [{ key: 'light.regular', value: 'Light' }, { key: 'light.compact', value: 'Light Compact' }];
        this.uniqueGridInstanceList = [];
        this.viewNameValidationPattern = /^[a-zA-Z0-9-_\s]+$/;
        this.viewTitle = 'Add New View';
        this.viewVisibilityTypes = ['Private', 'Public'];
    }
    set viewList(layoutList) {
        this.layoutList = layoutList;
        this.prepareGroupLayoutList();
    }
    get viewList() { return this.layoutList; }
    set selectedTheme(themeName) {
        if (sessionStorage.getItem('theme')) {
            this.defaultTheme = sessionStorage.getItem('theme');
        }
        else {
            this.defaultTheme = themeName;
        }
    }
    get selectedTheme() { return this.defaultTheme; }
    set gridInstanceList(gridInstanceList) {
        this.uniqueGridInstanceList = [];
        gridInstanceList.forEach(grid => {
            if (!this.uniqueGridInstanceList.filter(item => item.gridName === grid.gridName && item.isMasterGrid === grid.isMasterGrid).length) {
                this.uniqueGridInstanceList.push(grid);
            }
        });
    }
    get gridInstanceList() { return this.uniqueGridInstanceList; }
    //#region Angular LifeCycle event
    ngOnInit() {
        const layoutId = Number(this.activatedRoute.snapshot.queryParams['layout']);
        this.currentSelectLayoutInfo = this.layoutList.filter((arrItem) => {
            return arrItem.id === layoutId;
        })[0];
        if (this.currentSelectLayoutInfo) {
            this.layoutName = this.currentSelectLayoutInfo.name;
        }
        else if (!this.currentSelectLayoutInfo) {
            this.resetLayoutInfo();
        }
        this.setSelectedLayout();
        this.updateThemeSelection();
    }
    //#endregion Angular LifeCycle event
    //#region Public Methods
    onViewSelectionClick(layoutItem) {
        this.currentSelectLayoutInfo = layoutItem;
        this.setSelectedLayout(true);
    }
    deleteSelectedLayout(layoutItem) {
        const dataToDelete = {
            layoutId: layoutItem.id ? (layoutItem.id !== -1 ? layoutItem.id : 0) : 0,
            layoutName: layoutItem.name,
            isPublic: layoutItem.isPublic,
            isGlobalLayout: layoutItem.isGlobalDefault
        };
        const confirmDialog = confirm(`Are you sure you want to delete view (${layoutItem.name})?`, 'Delete View');
        confirmDialog.done((dialogResult) => {
            if (dialogResult) {
                this.deleteLayout(layoutItem);
            }
            else {
                return;
            }
        });
        // this.overrideConfirmPopupTheme();
    }
    updateViewVisibility(layoutItem) {
        if (this.actionOnSelectedView(layoutItem.id)) {
            const layoutAccessabilityGroup = (layoutItem ? !layoutItem.isPublic : this.currentSelectLayoutInfo.isPublic) ? 'Public' : 'Private';
            const confirmDialog = confirm(`Are you sure you want to change view to ${layoutAccessabilityGroup} ?`, 'Save View');
            confirmDialog.done((result) => {
                if (result) {
                    layoutItem.isPublic = !layoutItem.isPublic;
                    if (this.viewDataSource) {
                        if (layoutItem.isGlobalDefault) {
                            appToast({ message: 'Global default can\'t set as private view.', type: 'error' });
                            return;
                        }
                        this.viewDataSource.updateViewVisibility(layoutItem.id, layoutItem.isPublic)
                            .then((response) => {
                            this.selectedViewVisibility = layoutItem.isPublic ?
                                this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'public')[0]
                                : this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'private')[0];
                            appToast({ type: 'success', message: (response === null || response === void 0 ? void 0 : response.message) || 'View visibility updated.' });
                        }).catch((err) => {
                            appToast({ type: 'success', message: (err === null || err === void 0 ? void 0 : err.Message) || 'Unable to update view visibility.' });
                        });
                    }
                    else {
                        this.viewEvent.emit({ event: CGFEventsEnum.updateViewVisibility, data: layoutItem });
                    }
                }
            });
        }
    }
    addNewView() {
        this.gridInstanceList.forEach(grid => {
            grid.gridComponentInstance.beginUpdate();
            grid.gridComponentInstance.collapseAll(-1);
            grid.gridComponentInstance.clearGrouping();
            grid.gridComponentInstance.getVisibleColumns().forEach((colItem) => {
                grid.gridComponentInstance.columnOption(colItem.dataField, { visible: false });
            });
            grid.gridComponentInstance.endUpdate();
        });
        this.viewTitle = 'Add New View';
        this.selectedCrudOperation = crudOperation.add;
        this.layoutId = 0;
        this.showModalPopUp = true;
        this.layoutName = '';
        this.isPinned = false;
        this.selectedLayoutDefaultIds = [];
        this.selectedViewVisibility = this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'private')[0];
    }
    closePopupWindow() {
        this.selectedCrudOperation = '';
        if (this.activatedRoute.snapshot.queryParams.layout === '0') {
            const queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
            queryParams.layout = this.layoutId.toString();
            this.router.navigate([], { queryParams });
        }
        else {
            this.setSelectedLayout();
        }
        this.showModalPopUp = false;
    }
    searchBoxInitialized(e) { setTimeout(() => { e.component.focus(); }, 0); }
    saveAsLayout() {
        this.saveViewInformation(CGFEventsEnum.saveAsView);
    }
    saveViewInformation(enumVal) {
        if (!this.isFormValid()) {
            return;
        }
        if (!this.defaultOptionValidation()) {
            appToast({ type: 'warning', message: `Private View can't be made Global Default` });
            return;
        }
        if (this.viewDataSource) {
            this.viewDataSource.addView(this.prepareNewLayoutInfoBeforeEmitting())
                .then((response) => {
                this.viewEvent.emit({ event: CGFEventsEnum.updateLayouts, data: response.map(v => new ViewSelectionModel(v)) });
                appToast({ type: 'success', message: 'View has been saved successfully' });
            }).catch((err) => {
                appToast({ type: 'success', message: err.message || 'Unable to save view.' });
            });
        }
        else {
            this.viewEvent.emit({ event: enumVal || CGFEventsEnum.addNewView, data: this.prepareNewLayoutInfoBeforeEmitting() });
        }
        this.showModalPopUp = false;
    }
    // TODO without using setTimeout
    onLayoutNameContentReady(args) { setTimeout(() => { args.component.focus(); }, 300); }
    saveCurrentSelectedView() {
        if (this.viewDataSource) {
            this.viewDataSource.saveView(this.prepareCurrentLayoutInfoBeforeEmitting())
                .then((response) => {
                this.viewEvent.emit({ event: CGFEventsEnum.updateLayouts, data: response.map(v => new ViewSelectionModel(v)) });
                appToast({ type: 'success', message: 'Selected View has been updated successfully.' });
            }).catch((err) => {
                appToast({ type: 'error', message: err.message || 'Unable to save selected view.' });
            });
        }
        else {
            this.viewEvent.emit({ event: CGFEventsEnum.saveSelectedView, data: this.prepareCurrentLayoutInfoBeforeEmitting() });
        }
        this.showModalPopUp = false;
    }
    syncTreeViewSelection(e) {
        var _a, _b;
        const component = (e && e.component) || ((_a = this.treeView) === null || _a === void 0 ? void 0 : _a.instance);
        if (!component) {
            return;
        }
        if (!((_b = this.selectedLayoutDefaultIds) === null || _b === void 0 ? void 0 : _b.length)) {
            component.unselectAll();
        }
        else {
            this.selectedLayoutDefaultIds.forEach(((value) => { component.selectItem(value); }).bind(this));
        }
    }
    onDefaultVisibilityChange(e) {
        if (e.node.itemData) {
            this.selectedLayoutDefaultIds = e.component.getSelectedNodeKeys();
        }
    }
    filterListOfViews(args) {
        var _a, _b;
        this.filterValue = (_a = this.filterLayoutName) === null || _a === void 0 ? void 0 : _a.toLowerCase();
        if (!((_b = this.filterValue) === null || _b === void 0 ? void 0 : _b.trim()) && !(args.event.originalEvent instanceof KeyboardEvent)) {
            this.clearFilteredViews();
        }
    }
    cloneSelectedLayout(layoutItem) {
        if (this.actionOnSelectedView(layoutItem.id)) {
            this.showModalPopUp = true;
            this.viewTitle = 'Clone View';
            this.selectedCrudOperation = crudOperation.clone;
            let counter = 1;
            const cloneName = 'Clone';
            this.layoutName = `${layoutItem.name} ${cloneName}`;
            this.isPinned = layoutItem.isPinned;
            this.layoutId = 0;
            this.selectedViewVisibility = layoutItem.isPublic ? this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'public')[0]
                : this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'private')[0];
            this.setLayoutDefaultOptions(layoutItem);
            while (!this.isViewNameUnique(this.layoutName)) {
                this.layoutName = `${layoutItem.name} ${cloneName} ${counter}`;
                counter += 1;
            }
        }
    }
    editSelectedLayout(layoutItem) {
        if (this.actionOnSelectedView(layoutItem.id)) {
            this.selectedCrudOperation = crudOperation.edit;
            this.viewTitle = 'Edit View';
            this.showModalPopUp = true;
            this.layoutId = layoutItem.id;
            this.layoutName = layoutItem.name;
            this.isPinned = layoutItem.isPinned;
            this.selectedViewVisibility = layoutItem.isPublic ? this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'public')[0]
                : this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'private')[0];
            this.setLayoutDefaultOptions(layoutItem);
            this.setSelectedLayout();
        }
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
    //#endregion Public Methods
    //#region Private Methods
    setSelectedLayout(applyLayout = false) {
        this.populateDefaults();
        this.layoutName = this.currentSelectLayoutInfo.name;
        this.selectedViewVisibility = this.currentSelectLayoutInfo.isPublic ? this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'public')[0]
            : this.viewVisibilityTypes.filter(item => item.toLowerCase() === 'private')[0];
        this.isPinned = this.currentSelectLayoutInfo.isPinned;
        this.setLayoutDefaultOptions(this.currentSelectLayoutInfo);
        if (applyLayout) {
            this.applySelectedLayout();
        }
    }
    applySelectedLayout() {
        const queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        if (this.currentSelectLayoutInfo && this.currentSelectLayoutInfo.id !== -1) {
            if (Number(queryParams.layout) === this.currentSelectLayoutInfo.id) {
                return;
            }
            queryParams.layout = this.currentSelectLayoutInfo.id;
            this.router.navigate([], { queryParams });
        }
        else {
            this.resetLayoutInfo();
            queryParams.layout = 0;
            this.router.navigate([], { queryParams });
        }
        if (this.viewDataSource) {
            this.viewDataSource.applyView(this.currentSelectLayoutInfo)
                .then((updatedViewModel) => {
                this.viewEvent.emit({ event: CGFEventsEnum.applyView, data: updatedViewModel });
                appToast({ type: 'success', message: 'Selected view has been applied successfully' });
            }).catch(() => {
                appToast({ type: 'error', message: 'Unable to apply selected view.' });
            });
        }
        else {
            this.viewEvent.emit({ event: CGFEventsEnum.applyView, data: this.currentSelectLayoutInfo });
        }
    }
    updateGridLayoutSettings() {
        const childGridLayout = [];
        this.uniqueGridInstanceList.forEach(gridInstance => {
            const gridSettings = this.setLayoutSettings(gridInstance.gridComponentInstance, gridInstance.isMasterGrid);
            gridSettings.gridName = gridInstance.gridName;
            if (gridInstance.isMasterGrid) {
                this.gridLayoutSettings = gridSettings;
            }
            else {
                childGridLayout.push(gridSettings);
            }
        });
        this.gridLayoutSettings.childGridLayout = childGridLayout.length ? childGridLayout
            : JSON.parse(sessionStorage.getItem('childGridLayout')) || [];
    }
    setLayoutSettings(gridInstance, isMasterGrid = true) {
        // const gridLayoutSetting = gridInstance.state();
        // gridLayoutSetting.colDef = gridInstance.getVisibleColumns().filter((item) => {
        //   return item.dataField !== customActionColumnInfo.dataField || item.command !== 'empty';
        // });
        // gridLayoutSetting.formatData = JSON.parse(sessionStorage.getItem(getStorageKey(gridInstance, CGFStorageKeys[CGFStorageKeys.formatData], isMasterGrid)));
        // gridLayoutSetting.summary = gridInstance.option('summary');
        // gridLayoutSetting.selectedTheme = this.appliedTheme;
        // gridLayoutSetting.isGridBorderVisible = this.isGridBorderVisible;
        // gridLayoutSetting.conditionalFormatting = JSON.parse(sessionStorage.getItem(getStorageKey(gridInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], isMasterGrid)));
        // return gridLayoutSetting;
    }
    populateDefaults() {
        this.defaults = [];
        if (this.currentSelectLayoutInfo.id > 0) {
            if (this.currentSelectLayoutInfo.canDefault) {
                this.defaults.push({ text: this.currentSelectLayoutInfo.isDefault ? 'Clear Default' : 'Set As Default', value: 'Default', icon: 'fas fa-star default-icon-color' });
            }
            if (this.currentSelectLayoutInfo.canGlobalDefault && this.currentSelectLayoutInfo.isPublic) {
                this.defaults.push({
                    text: this.currentSelectLayoutInfo.isGlobalDefault ?
                        'Clear Global Default' : 'Set As Global Default', value: 'GlobalDefault', icon: 'fas fa-star global-default-icon-color'
                });
            }
        }
    }
    deleteLayout(viewToDelete) {
        if (this.viewDataSource) {
            this.viewDataSource.deleteView(viewToDelete)
                .then(() => {
                appToast({ type: 'success', message: 'View has been deleted successfully.' });
                this.viewEvent.emit({ event: CGFEventsEnum.updateLayouts, data: [...this.viewList.filter(item => item.id !== viewToDelete.id)] });
                this.prepareGroupLayoutList();
            }).catch(() => {
                appToast({ type: 'error', message: 'View has been was not deleted successfully.' });
            });
        }
        else {
            this.viewEvent.emit({ event: CGFEventsEnum.deleteView, data: viewToDelete });
            // TODO: refactor below duplicate code.
            this.viewList = [...this.viewList.filter(item => item.id !== viewToDelete.id)];
            this.prepareGroupLayoutList();
        }
    }
    resetLayoutInfo() { this.currentSelectLayoutInfo = new Object(); }
    updateThemeSelection() { this.defaultTheme = this.defaultTheme || 'dx-swatch-default'; }
    clearFilteredViews() {
        this.filterValue = null;
        this.filterLayoutName = '';
        this.showFilterControlInput = false;
    }
    prepareGroupLayoutList() {
        const groups = new Set(this.layoutList.map(item => item.isPinned));
        this.groupLayoutList = [];
        groups.forEach(groupValue => this.groupLayoutList.push({
            groupName: groupValue ? 'PINNED' : 'VIEWS',
            isVisible: true,
            values: this.layoutList.filter(i => i.isPinned === groupValue)
        }));
        // Sorting to make sure that Pinned is always on top.
        this.groupLayoutList = this.groupLayoutList.sort((a, b) => {
            if (a.groupName < b.groupName) {
                return -1;
            }
            if (a.groupName > b.groupName) {
                return 1;
            }
            return 0;
        });
    }
    setLayoutDefaultOptions(layoutItem) {
        this.selectedLayoutDefaultIds = [];
        if (layoutItem.isGlobalDefault) {
            this.selectedLayoutDefaultIds.push(this.layoutDefaultData.filter(item => item.value.toLowerCase() === 'global default')[0].key);
        }
        if (layoutItem.isDefault) {
            this.selectedLayoutDefaultIds.push(this.layoutDefaultData.filter(item => item.value.toLowerCase() === 'user default')[0].key);
        }
    }
    isViewNameUnique(viewName) {
        const filterItem = this.layoutList.filter(item => item.name === viewName) || [];
        return filterItem.length ? false : true;
    }
    isFormValid() {
        return validationEngine.validateGroup('viewSave').isValid;
    }
    actionOnSelectedView(layoutId) {
        var _a;
        if (((_a = this.currentSelectLayoutInfo) === null || _a === void 0 ? void 0 : _a.id) !== layoutId) {
            appToast({ type: 'warning', message: `Please apply the layout before performing any action on it.` });
            return false;
        }
        return true;
    }
    prepareNewLayoutInfoBeforeEmitting() {
        return {
            id: this.layoutId,
            name: this.layoutName,
            isGlobalDefault: this.checkIfGlobalDefault(this.selectedLayoutDefaultIds),
            isUserDefault: this.checkIfUserDefault(this.selectedLayoutDefaultIds),
            isPinned: this.isPinned,
            defaultOptions: this.layoutDefaultData.filter(item => item.selected).length > 0 ? this.layoutDefaultData : [],
            visibility: this.selectedViewVisibility.toLowerCase() === 'public',
            state: this.getGridStateOfSelectedGrid()
        };
    }
    prepareCurrentLayoutInfoBeforeEmitting() {
        return {
            id: this.currentSelectLayoutInfo.id,
            name: this.currentSelectLayoutInfo.name,
            isPinned: this.currentSelectLayoutInfo.isPinned,
            isGlobalDefault: this.currentSelectLayoutInfo.isGlobalDefault,
            isUserDefault: this.currentSelectLayoutInfo.isDefault,
            defaultOptions: this.layoutDefaultData.filter(item => item.selected).length > 0 ? this.layoutDefaultData : [],
            visibility: this.currentSelectLayoutInfo.isPublic,
            state: this.getGridStateOfSelectedGrid()
        };
    }
    getGridStateOfSelectedGrid() {
        const currentSelectedGridInstance = this.gridInstanceList.filter(item => item.isSelected)[0];
        const key = this.utilityService.getStorageKey(currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], true);
        const currentGridState = currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance.state();
        currentGridState.visibleColumns = currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance.getVisibleColumns().filter((item) => {
            return item.dataField !== customActionColumnInfo.dataField || item.command !== 'empty';
        });
        currentGridState.columnFormattingInfo = JSON.parse(sessionStorage.getItem(key)) || [];
        currentGridState.summary = currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance.option('summary');
        currentGridState.selectedTheme = this.defaultTheme;
        currentGridState.isGridBorderVisible = this.isGridBorderVisible;
        currentGridState.conditionalFormattingInfo = JSON.parse(sessionStorage.getItem(key)) || [];
        return {
            gridState: currentGridState,
            columnFormattingInfo: JSON.parse(sessionStorage.getItem(key)) || [],
            conditionFormattingInfo: []
        };
    }
    defaultOptionValidation() {
        var _a;
        const isGlobalDefault = this.selectedLayoutDefaultIds.indexOf((_a = this.layoutDefaultData.filter((item) => item.value.toLowerCase() === 'global default')[0]) === null || _a === void 0 ? void 0 : _a.key) > -1;
        const isPublic = this.selectedViewVisibility.toLowerCase() === 'public';
        return isGlobalDefault ? isPublic : true;
    }
    checkIfGlobalDefault(ids) {
        const globalKey = this.layoutDefaultData.find(a => a.value === 'Global Default');
        return ids.some(id => globalKey.key === id);
    }
    checkIfUserDefault(ids) {
        const userKey = this.layoutDefaultData.find(a => a.value === 'User Default');
        return ids.some(id => userKey.key === id);
    }
};
ViewSelectionComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router },
    { type: CGFUtilityService }
];
__decorate([
    ViewChild(DxTreeViewComponent)
], ViewSelectionComponent.prototype, "treeView", void 0);
__decorate([
    Input()
], ViewSelectionComponent.prototype, "viewList", null);
__decorate([
    Input()
], ViewSelectionComponent.prototype, "selectedTheme", null);
__decorate([
    Input()
], ViewSelectionComponent.prototype, "gridInstanceList", null);
__decorate([
    Input()
], ViewSelectionComponent.prototype, "isGridBorderVisible", void 0);
__decorate([
    Input()
], ViewSelectionComponent.prototype, "viewDataSource", void 0);
__decorate([
    Output()
], ViewSelectionComponent.prototype, "viewEvent", void 0);
__decorate([
    Output()
], ViewSelectionComponent.prototype, "closeCurrentFlyOut", void 0);
ViewSelectionComponent = __decorate([
    Component({
        selector: 'pbi-view-selection',
        template: "<div class=\"viewSelectionContainer\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-list title-icon\"></i>\r\n            <span class=\"section-title\">View Selection</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div>\r\n        <div class=\"add-search-container\">\r\n            <div class=\"text-search\">\r\n                <dx-text-box class=\"search-box\" mode=\"search\" placeholder=\"Filter by keyword\"\r\n                    [(value)]=\"filterLayoutName\" maxLength=\"40\" (onInitialized)=\"searchBoxInitialized($event)\"\r\n                    (onValueChanged)=\"filterListOfViews($event)\" valueChangeEvent=\"keyup\">\r\n                </dx-text-box>\r\n            </div>\r\n            <div class=\"add-view\">\r\n                <i class=\"fas fa-plus fly-out-round-add-icon pointer\" title=\"Add\" (click)=\"addNewView()\"></i>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngFor=\"let obj of groupLayoutList\">\r\n        <div class=\"active-properties-tab\">\r\n            <ul>\r\n                <li class=\"pointer property-tab-in-view-selection\" (click)=\"obj.isVisible = !obj.isVisible\">\r\n                    <span>{{obj.groupName}}</span>\r\n                    <i [class.fa-angle-down]=\"!obj.isVisible\" [class.fa-angle-up]=\"obj.isVisible\"\r\n                        class=\"fa basic-format-toggle-icon\"></i>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <div [class.dynamic-height-container]=\"groupLayoutList.length === 1\" class=\"views-list-detail-container\"\r\n            *ngIf=\"obj.isVisible\">\r\n            <div *ngFor=\"let item of obj.values\">\r\n                <span class=\"layout-info-container\"\r\n                    *ngIf=\"filterValue ? item.name.toLowerCase().indexOf(filterValue) > -1 : true\">\r\n                    <span [class.selectedLayout]=\"currentSelectLayoutInfo.id === item.id\">\r\n                        <span class=\"applied-layout dynamic-tooltip\" flow=\"down\"\r\n                            attr.title=\"{{item.isPublic ? 'Public View' : 'Private View'}}\">\r\n                            <i class=\"far fa-globe-americas\" (click)=\"updateViewVisibility(item)\"\r\n                                *ngIf=\"item.isPublic\"></i>\r\n                            <i class=\"fas fa-eye-slash\" (click)=\"updateViewVisibility(item)\" *ngIf=\"!item.isPublic\"></i>\r\n                        </span>\r\n                        <span class=\"layoutName\" (click)=\"onViewSelectionClick(item)\">\r\n                            {{item.name}}\r\n                            <i class=\"fas fa-star default-icon-color\" *ngIf=\"item.isDefault\"></i>\r\n                            <i class=\"fas fa-star global-default-icon-color\" *ngIf=\"item.isGlobalDefault\"></i>\r\n                        </span>\r\n                        <i class=\"far fa-clone layoutDeleteIcon dynamic-tooltip\" title=\"Clone Layout\"\r\n                            [class.deleteIconWithSelectedLayout]=\"currentSelectLayoutInfo.id === item.id\"\r\n                            (click)=\"cloneSelectedLayout(item)\"></i>\r\n                        <i class=\"fas fa-pencil layoutDeleteIcon dynamic-tooltip\" title=\"Edit Layout\"\r\n                            [class.deleteIconWithSelectedLayout]=\"currentSelectLayoutInfo.id === item.id\"\r\n                            (click)=\"editSelectedLayout(item)\"></i>\r\n                        <i class=\"far fa-trash-alt layoutDeleteIcon dynamic-tooltip\" title=\"Delete Layout\"\r\n                            [class.deleteIconWithSelectedLayout]=\"currentSelectLayoutInfo.id === item.id\"\r\n                            (click)=\"deleteSelectedLayout(item)\"></i>\r\n                    </span>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"save-button-container\" *ngIf=\"layoutList.length\">\r\n        <dx-button class=\"view-operation-buttons\" text=\"Save\" type=\"default\" (click)=\"saveCurrentSelectedView()\">\r\n        </dx-button>\r\n    </div>\r\n    <div class=\"empty-message\" *ngIf=\"!layoutList.length\">\r\n        <div>View Not Found</div>\r\n    </div>\r\n</div>\r\n\r\n<dx-popup class=\"pbi-modal-popup\" width=\"40%\" height=\"auto\" [showTitle]=\"true\" [title]=\"viewTitle\" [dragEnabled]=\"false\"\r\n    [(visible)]=\"showModalPopUp\">\r\n    <div *dxTemplate=\"let data of 'content'\">\r\n        <dx-validation-group id='viewSave'>\r\n            <form>\r\n                <div class=\"view-flex-container\">\r\n                    <div class=\"view-name-container\">\r\n                        <label class=\"required-field\">Name</label>\r\n                        <div>\r\n                            <div>\r\n                                <dx-text-box (onContentReady)=\"onLayoutNameContentReady($event)\"\r\n                                    class=\"pbi-text-editor view-name-textbox\" [(value)]=\"layoutName\" maxLength=\"40\">\r\n                                    <dx-validator validationGroup=\"viewSave\">\r\n                                        <dxi-validation-rule type=\"required\" message=\"Name is required.\">\r\n                                        </dxi-validation-rule>\r\n                                        <dxi-validation-rule type=\"pattern\" [pattern]=\"viewNameValidationPattern\"\r\n                                            message=\"Name will contain only alphabets(a-z,A-Z)/numbers(0-9)/special characters(-,_,space).\">\r\n                                        </dxi-validation-rule>\r\n                                    </dx-validator>\r\n                                </dx-text-box>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"view-label-container\">\r\n                        <label class=\"required-field\">Default Options</label>\r\n                        <div>\r\n                            <dx-drop-down-box class=\"pbi-text-editor\" [(value)]=\"selectedLayoutDefaultIds\"\r\n                                valueExpr=\"key\" displayExpr=\"value\" [dataSource]=\"layoutDefaultData\"\r\n                                (onValueChanged)=\"syncTreeViewSelection()\">\r\n                                <div *dxTemplate=\"let data of 'content'\">\r\n                                    <dx-tree-view [dataSource]=\"layoutDefaultData\" dataStructure=\"plain\" keyExpr=\"key\"\r\n                                        selectionMode=\"multiple\" showCheckBoxesMode=\"normal\" displayExpr=\"value\"\r\n                                        [selectByClick]=\"true\" (onContentReady)=\"syncTreeViewSelection($event)\"\r\n                                        (onItemSelectionChanged)=\"onDefaultVisibilityChange($event)\">\r\n                                    </dx-tree-view>\r\n                                </div>\r\n                            </dx-drop-down-box>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"view-flex-container\">\r\n                    <div class=\"view-name-container\">\r\n                        <label class=\"required-field\">Pinned</label>\r\n                        <div>\r\n                            <div>\r\n                                <label class=\"switch\">\r\n                                    <!-- <app-bool-input [(State)]=\"isPinned\"></app-bool-input> -->\r\n                                    <span>\r\n                                        <label class=\"switch\">\r\n                                            <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"isPinned\">\r\n                                            <span class=\"slider round\"></span>\r\n                                        </label>\r\n                                    </span>\r\n                                </label>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"view-label-container\">\r\n                        <label class=\"required-field\">View Visibility</label>\r\n                        <div>\r\n                            <div>\r\n                                <dx-radio-group [items]=\"viewVisibilityTypes\" [(value)]=\"selectedViewVisibility\"\r\n                                    layout=\"horizontal\">\r\n                                </dx-radio-group>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"pop-up-footer\">\r\n                    <div class=\"view-action-button\">\r\n                        <dx-button class=\"cancel-button\" text=\"Cancel\" type=\"default\" (click)=\"closePopupWindow()\">\r\n                        </dx-button>\r\n                        <dx-drop-down-button class=\"pbi-split-button\" [splitButton]=\"true\" [useSelectMode]=\"false\"\r\n                            text=\"Save\" [items]=\"dropDownOptions\" displayExpr=\"name\" keyExpr=\"id\"\r\n                            (onButtonClick)=\"saveViewInformation()\" (onItemClick)=\"saveAsLayout()\">\r\n                        </dx-drop-down-button>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </dx-validation-group>\r\n    </div>\r\n</dx-popup>\r\n",
        styles: [""]
    })
], ViewSelectionComponent);

let EntityParametersComponent = class EntityParametersComponent {
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.dataBrowserEntityParameters = [];
        this.disableRefreshIcon = new EventEmitter();
        this.entityParamEvent = new EventEmitter();
        this.flyOutSelectionClick = new EventEmitter();
        this.dateShortcutsItems = dateShortcuts;
        this.disableParameterApply = true;
        this.dxDateBox = {
            dateParam: {
                type: 'date',
                onFocusOut: (e) => {
                    if (!e.component.option('isValid')) {
                        e.component.option('value', '');
                    }
                }
            }
        };
        this.gridInstances = [];
        this.sortable = null;
        this.dropDownOptions = {
            resizeEnabled: true,
            onContentReady: (e) => {
                const DOM = e.component._$content;
                const instance = Resizable.getInstance(DOM);
                instance.option('handles', 'left right');
            }
        };
    }
    ngDoCheck() {
        let formComplete = true;
        if (this.dataBrowserEntityParameters.length === 1
            && this.dataBrowserEntityParameters[0].Type.toLowerCase() === 'userid') {
            formComplete = false;
        }
        else {
            for (let i = 0; i < this.dataBrowserEntityParameters.length; i++) {
                const param = this.dataBrowserEntityParameters[i];
                if ((param.Value === null || param.Value === undefined || param.Value === '') && !param.IsOptional && param.DataType.toLowerCase() !== 'boolean') {
                    formComplete = false;
                    break;
                }
            }
        }
        if (this.disableParameterApply === formComplete) {
            this.disableParameterApply = !formComplete;
            this.disableRefreshIcon.emit(this.disableParameterApply);
        }
    }
    ngAfterViewInit() {
        let allDefaultArePresent = true;
        this.dataBrowserEntityParameters.forEach((item) => {
            const isList = item.Type.toLowerCase() === 'sqllist' || item.Type.toLowerCase() === 'list';
            if (((!item.DefaultValue && isList) || (!isList && !item.DefaultSingleValue)) && !item.IsOptional) {
                allDefaultArePresent = false;
            }
            if (item.DataType.toLowerCase() === 'boolean') {
                item.Value = (item.Value === false || (item.Value && (item.Value === '0' || item.Value === 'false'))) ? false : true;
            }
        });
        if (allDefaultArePresent) {
            this.applyParameters();
        }
        this.sortable = new window['Sortable'](document.getElementById('parameter-list-container'), {
            animation: 150,
            handle: '.drag-icon',
            onEnd: (args) => {
                if (this.dataBrowserEntityParameters[args.oldIndex].Type.toLowerCase() === 'list' || this.dataBrowserEntityParameters[args.oldIndex].Type.toLowerCase() === 'sqllist') {
                    this.gridInstances.forEach((item) => {
                        if (item.order === args.oldIndex) {
                            item.order = args.newIndex;
                        }
                    });
                }
                this.dataBrowserEntityParameters.splice(args.newIndex, 0, this.dataBrowserEntityParameters.splice(args.oldIndex, 1)[0]);
                this.dataBrowserEntityParameters.forEach((item, index) => {
                    item.Order = index;
                });
            }
        });
    }
    templateFunction(data) {
        return '<div class=\'custom-option-item\' title=\'' + data + '\'>' + data + '</div>';
    }
    applyParameters() {
        const paramterMapping = this.dataBrowserEntityParameters;
        const windowObj = window;
        const params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        let queryParamsUpdated = false;
        paramterMapping.forEach((item) => {
            const name = item.OriginalName || item.Name;
            let paramValue = null;
            if (item.DataType.toLowerCase() === 'datetime') {
                paramValue = item.dateShortcutId || windowObj.moment(item.Value).format('M/D/YYYY');
                if (paramValue.toString() !== params[name]) {
                    params[name] = paramValue;
                    queryParamsUpdated = true;
                }
            }
            else if (item.DataType.toLowerCase() === 'boolean' && !item.Value && params[name] !== 'false') {
                params[name] = false;
                queryParamsUpdated = true;
                // tslint:disable-next-line:triple-equals
            }
            else if ((item.Value == 0 || item.Value) && params[name] !== item.Value.toString()) {
                params[name] = item.Value;
                queryParamsUpdated = true;
            }
            else if (((this.IsNullEmptyOrWhiteSpace(item.Value) && !this.IsNullEmptyOrWhiteSpace(params[name]))
                || (!this.IsNullEmptyOrWhiteSpace(item.Value) && this.IsNullEmptyOrWhiteSpace(params[name]))) && item.IsOptional) {
                params[name] = !this.IsNullEmptyOrWhiteSpace(item.Value) ? item.Value : null;
                if (!this.IsNullEmptyOrWhiteSpace(params[name])) {
                    item.Value = params[name];
                }
                queryParamsUpdated = true;
            }
        });
        if (!queryParamsUpdated) {
            return;
        }
        this.router.navigate(['/dataBrowser'], { queryParams: params });
        this.entityParamEvent.emit({ event: CGFEventsEnum.applyEntityParameters, data: paramterMapping });
        this.flyOutSelectionClick.emit({ showEntityParameter: false });
    }
    // on selection change of grid records
    selectionChangedHandler(selectedRowEvent, order) {
        const result = [];
        if (selectedRowEvent.selectedRowsData.length > 100) { // 100 is the to restrict and prevent DB parameters issue.
            appToast({ type: 'error', message: 'Only 100 items can be selected.' });
            selectedRowEvent.component.deselectRows(selectedRowEvent.currentSelectedRowKeys);
            return;
        }
        selectedRowEvent.selectedRowsData.forEach((item) => { result.push(item.data); });
        this.dataBrowserEntityParameters[order].Value = result.join(',');
    }
    // when grid is loaded with content
    onContentReadyHandler(e, order) {
        const lastPage = document.querySelector('.dx-page-sizes .dx-selection');
        if (lastPage !== null && lastPage.textContent === '0') {
            lastPage.textContent = 'ALL';
        }
        if (this.dataBrowserEntityParameters[order].Value) {
            const selectedIds = [];
            const arraySelectedValues = this.dataBrowserEntityParameters[order].Value.split(',');
            arraySelectedValues.forEach(r => {
                for (let i = 0; i < this.dataBrowserEntityParameters[order].DefaultListValue.length; i++) {
                    if (r.trim() === this.dataBrowserEntityParameters[order].DefaultListValue[i].trim()) {
                        selectedIds.push(i);
                    }
                }
            });
            e.component.selectRows(selectedIds);
        }
        // overwrite text of page size 0 to All.
        setTimeout(() => {
            const elm = document.querySelector('.dx-selectbox');
            if (elm != null) {
                document.querySelector('.dx-selectbox').addEventListener('click', function () {
                    setTimeout(() => {
                        document.querySelector('.dx-scrollview-content').lastElementChild.textContent = 'ALL';
                    }, 0);
                });
            }
        }, 0);
        this.onParameterDropDownOpened();
        // when page sizes are not showing by dropdown
        const el = document.querySelector('.dx-page-size');
        if (el !== null) {
            if (el.textContent === 'ALL') {
                const elm = document.querySelector('.dx-page-sizes .dx-selection');
                elm.setAttribute('style', 'pointer-events: none');
            }
            else {
                const elm = document.querySelector('.dx-page-sizes .dx-selection');
                elm.setAttribute('style', 'pointer-events: none');
            }
        }
    }
    // to clear dropdown selection
    onValueChangedDropDownBox(e, order) {
        if (e.value === null) {
            // clear grid selection
            const gridInstance = this.getGridInstance(order);
            if (gridInstance) {
                gridInstance.clearSelection();
            }
            this.dataBrowserEntityParameters[order].Value = '';
        }
    }
    // when grid is initialized
    onInitializedHandler(e, order) {
        const object = {
            order: order,
            gridInstance: e.component
        };
        if (!this.gridInstances.filter((r) => r.order === order).length) {
            this.gridInstances.push(object);
        }
        const gridOptions = this.prepareGridOptions(this.dataBrowserEntityParameters[order]);
        setTimeout(() => {
            e.component.beginUpdate();
            if (gridOptions.DataSource.store.data.length > 100) { // 100 is the to restrict and prevent DB parameters issue..
                e.component.option('showColumnHeaders', false);
            }
            e.component.option('dataSource', gridOptions.DataSource);
            e.component.option('pager', gridOptions.Pager);
            e.component.option('paging', gridOptions.Paging);
            e.component.endUpdate();
        }, 0);
    }
    // prepare grid data source with Id as key
    prepareGridOptions(item) {
        const jsonDefaultValues = [];
        item.DefaultListValue.forEach((r, index) => {
            const object = {};
            object['Id'] = index;
            object['data'] = r;
            jsonDefaultValues.push(object);
        });
        const gridOptions = {
            DataSource: {
                store: {
                    type: 'array',
                    key: 'Id',
                    data: jsonDefaultValues
                }
            },
            Pager: { showPageSizeSelector: true, allowedPageSizes: [30, 0], showInfo: true, visible: true },
            Paging: { enabled: true, pageSize: 30 }
        };
        if (jsonDefaultValues.length <= 30) {
            gridOptions.Paging.enabled = false;
            gridOptions.Pager.visible = false;
        }
        return gridOptions;
    }
    getGridInstance(order) {
        const filterInstance = this.gridInstances.filter((r) => r.order === order)[0];
        return filterInstance ? filterInstance.gridInstance : null;
    }
    onDateValueChanged(e, item) {
        if (e.value) {
            item.Value = convertDateToUsFormat(e.value);
        }
        this.dateShortcutContentReady(item);
    }
    /**
    * Method will be called on content ready of datebox.
    * @returns void
    */
    dateShortcutContentReady(item) {
        item.dateShortcutIcon = 'fal fa-calendar-alt';
        item.dateShortcutId = '';
        if (item.Value) {
            for (let index = 0; index < this.dateShortcutsItems.length; index++) {
                if (this.dateShortcutsItems[index].Value === item.Value) {
                    item.dateShortcutIcon = 'fas fa-calendar-alt';
                    item.dateShortcutId = this.dateShortcutsItems[index].Id;
                }
            }
        }
    }
    dateShortcutOpen(item) {
        this.dateShortcutsItems.forEach(data => {
            if (data.Id === item.dateShortcutId) {
                data.IsSelected = true;
            }
            else {
                data.IsSelected = false;
            }
        });
    }
    /**
     * Method will be called on date short cut click.
     * @returns void
     */
    dateShortcutItemClick(selectedData, globalItem) {
        const item = selectedData.itemData;
        item.IsSelected = !item.IsSelected;
        if (item.IsSelected) {
            const prevSelected = this.dateShortcutsItems.filter(data => data.IsSelected);
            if (prevSelected && prevSelected.length && prevSelected[0].Id !== item.Id) {
                prevSelected[0].IsSelected = false;
            }
            globalItem.dateShortcutId = item.Id;
            globalItem.dateShortcutIcon = 'fas fa-calendar-alt';
            globalItem.Value = item.Value;
        }
        else {
            globalItem.dateShortcutIcon = 'fal fa-calendar-alt';
            globalItem.dateShortcutId = '';
        }
    }
    IsNullEmptyOrWhiteSpace(value) {
        const v = value ? `${value}`.trim() : '';
        return (v === null || v === undefined || v === '');
    }
    onParameterDropDownOpened(args) {
        if (args) {
            // resetting the width to parent control
            args.component.option('dropDownOptions.width', 254);
        }
        const colHtmlCollection = document.getElementsByClassName('entity-param-grid col');
        if (colHtmlCollection.length) {
            setTimeout(() => {
                for (let index = 0; index < colHtmlCollection.length; index++) {
                    if (index % 2 === 0) {
                        colHtmlCollection[index].setAttribute('style', 'width: 25px');
                    }
                }
            }, 0);
        }
    }
    onKeyDown_Grid(evt) {
        if (evt.component.option('dataSource').store.data.length > 100) { // 100 is the to restrict and prevent DB parameters issue.
            if (evt.event.ctrlKey && evt.event.keyCode === 65) { // ctrl+a
                appToast({ type: 'error', message: 'Only 100 items can be selected.' });
                evt.event.preventDefault();
            }
        }
    }
    onMouseOver(item) {
        item.showBorder = true;
    }
    onMouseOut(item) {
        item.showBorder = false;
    }
};
EntityParametersComponent.ctorParameters = () => [
    { type: Router },
    { type: ActivatedRoute }
];
__decorate([
    ViewChild(DxTreeViewComponent)
], EntityParametersComponent.prototype, "treeView", void 0);
__decorate([
    Input()
], EntityParametersComponent.prototype, "dataBrowserEntityParameters", void 0);
__decorate([
    Output()
], EntityParametersComponent.prototype, "disableRefreshIcon", void 0);
__decorate([
    Output()
], EntityParametersComponent.prototype, "entityParamEvent", void 0);
__decorate([
    Output()
], EntityParametersComponent.prototype, "flyOutSelectionClick", void 0);
EntityParametersComponent = __decorate([
    Component({
        selector: 'pbi-entity-parameters',
        template: "<div>\r\n    <div class=\"row no-margin\">\r\n        <div id=\"parameter-list-container\">\r\n            <div [class.parameter-item-container]=\"!item.HideInDataBrowser\"\r\n                *ngFor=\"let item of dataBrowserEntityParameters;\">\r\n                <div *ngIf=\"!item.HideInDataBrowser\" class=\"display-inline-block hideInBrowser\">\r\n                    <div class=\"display-inline-block\"\r\n                        *ngIf=\"item.DataType.toLowerCase() === 'datetime' &&  (item.Type.toLowerCase()=== 'single'|| item.Type.toLowerCase()==='sqlsingle' || item.Type.toLowerCase()==='static')\">\r\n                        <div class=\"form-group display-inline-block\">\r\n                            <label class=\"input-label label-padding-top\">{{item.Name}}\r\n                                <i class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                    (mouseout)=\"onMouseOut(item)\"></i>\r\n                            </label>\r\n                            <dx-date-box width=\"219px\" (focusOut)=\"dxDateBox.onFocusOut($event)\" [(value)]=\"item.Value\"\r\n                                (onContentReady)=\"dateShortcutContentReady(item)\"\r\n                                (onValueChanged)=\"onDateValueChanged($event, item)\"\r\n                                class=\"input-element single-date-picker\"\r\n                                invalidDateMessage='Value must be a valid date'>\r\n                                <dx-validator>\r\n                                    <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                        message=\"{{item.Name + ' is required. '}}\">\r\n                                    </dxi-validation-rule>\r\n                                </dx-validator>\r\n                            </dx-date-box>\r\n                        </div>\r\n                        <div class=\"form-group display-inline-block\" style=\"vertical-align: bottom; margin-left: 10px;\">\r\n                            <label class=\"input-label\">&nbsp;</label>\r\n                            <dx-drop-down-button\r\n                                [icon]=\"item.dateShortcutIcon ? item.dateShortcutIcon :'fal fa-calendar-alt'\"\r\n                                displayExpr=\"Name\" [items]=\"dateShortcutsItems\" valueExpr=\"Id\"\r\n                                class=\"input-dropdown-button\" itemTemplate=\"listItem\"\r\n                                (onButtonClick)=\"dateShortcutOpen(item)\"\r\n                                (onItemClick)=\"dateShortcutItemClick($event,item)\">\r\n                                <div *dxTemplate=\"let data of 'listItem'\"\r\n                                    [class.active-item]=\"data.IsSelected && (item.dateShortcutId === data.Id)\">\r\n                                    <label class=\"date-short-cut-item-label\">{{data.Name}}</label>\r\n                                </div>\r\n                                <dxo-drop-down-options width=\"300px\">\r\n                                </dxo-drop-down-options>\r\n                            </dx-drop-down-button>\r\n                        </div>\r\n                    </div>\r\n                    <ng-template [ngIf]=\"item.DataType.toLowerCase() === 'boolean'\">\r\n                        <div class=\"form-group display-inline-block\">\r\n                            <label class=\"input-label display-block\">{{item.Name}}\r\n                                <i class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                    (mouseout)=\"onMouseOut(item)\"></i>\r\n                            </label>\r\n                            <div>\r\n                                <!-- <app-bool-input [(State)]=\"item.Value\"></app-bool-input> -->\r\n                                <!-- <label class=\"switch\">\r\n                                    <span>\r\n                                        <label class=\"switch\">\r\n                                            <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"item.Value\">\r\n                                            <span class=\"slider round\"></span>\r\n                                        </label>\r\n                                    </span>\r\n                                </label> -->\r\n                            </div>\r\n                        </div>\r\n                    </ng-template>\r\n                    <div *ngIf=\"(item.Type.toLowerCase()=== 'userid')\" class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-text-box width=\"219px\" [disabled]=\"true\" [(value)]=\"item.Value\" class=\"input-element\">\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-text-box>\r\n                    </div>\r\n                    <div *ngIf=\"item.DataType.toLowerCase() === 'string' && (item.Type.toLowerCase()=== 'single'|| item.Type.toLowerCase()==='sqlsingle')\"\r\n                        class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-text-box width=\"219px\" [(value)]=\"item.Value\" class=\"input-element\">\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-text-box>\r\n                    </div>\r\n                    <div *ngIf=\"item.DataType.toLowerCase()==='integer' && (item.Type.toLowerCase()==='single' || item.Type.toLowerCase()==='sqlsingle')\"\r\n                        class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-number-box width=\"219px\" [(value)]=\"item.Value\" class=\"input-element \">\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-number-box>\r\n                    </div>\r\n                    <div *ngIf=\"(item.DataType.toLowerCase()==='datetime' || item.DataType.toLowerCase()==='integer' || item.DataType.toLowerCase()==='string') && (item.Type.toLowerCase()==='list' || item.Type.toLowerCase()==='sqllist')\"\r\n                        class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-drop-down-box width=\"254px\" class=\"input-element\" [(value)]=\"item.Value\"\r\n                            placeholder=\"Select...\" [dataSource]=\"item.DefaultListValue\" valueExpr=\"data\"\r\n                            displayExpr=\"data\" [showClearButton]=\"true\"\r\n                            (onValueChanged)=\"onValueChangedDropDownBox($event,item.Order)\"\r\n                            (onOpened)=\"onParameterDropDownOpened($event)\" [dropDownOptions]=\"dropDownOptions\">\r\n                            <div *dxTemplate=\"let data of 'content'\">\r\n                                <dx-data-grid class=\"entity-param-grid\"\r\n                                    [columns]=\"[{dataField:'data', caption:'Select All'}]\" [keyExpr]=\"data\"\r\n                                    [selection]=\"{ mode: 'multiple', showCheckBoxesMode:'always' }\"\r\n                                    [hoverStateEnabled]=\"true\" [filterRow]=\"{ visible: true }\" [height]=\"345\"\r\n                                    width=\"auto\" [showColumnHeaders]=\"true\" [wordWrapEnabled]=\"true\"\r\n                                    (onInitialized)=\"onInitializedHandler($event, item.Order)\"\r\n                                    (onSelectionChanged)=\"selectionChangedHandler($event,item.Order)\"\r\n                                    (onContentReady)=\"onContentReadyHandler($event,item.Order)\"\r\n                                    (onKeyDown)=\"onKeyDown_Grid($event)\">\r\n                                    <dxo-load-panel [enabled]=\"true\" [shading]=\"true\"\r\n                                        [indicatorSrc]=\"'assets/img/loader/loader-indicator.gif'\">\r\n                                    </dxo-load-panel>\r\n                                </dx-data-grid>\r\n                            </div>\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-drop-down-box>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row no-margin text-align-right input-form-footer-button-container \">\r\n            <dx-button class=\"btn-with-blue-bg-color\" text=\"LOAD\" (click)=\"applyParameters()\"\r\n                [disabled]=\"disableParameterApply\"></dx-button>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
        styles: [""]
    })
], EntityParametersComponent);

//#endregion Components of Common Grid Framework
const components$1 = [
    ColorFormatComponent,
    ColumnChooserComponent,
    CommonGridFrameworkComponent,
    ConditionalFormattingComponent,
    EntityParametersComponent,
    FilterComponent,
    FlyOutActionIconContainerComponent,
    GridComponent,
    SettingsComponent,
    ViewSelectionComponent,
];
let PBICommonGridFrameworkModule = class PBICommonGridFrameworkModule {
};
PBICommonGridFrameworkModule = __decorate([
    NgModule({
        declarations: [components$1],
        imports: [
            CommonModule,
            DxButtonModule,
            DxColorBoxModule,
            DxDataGridModule,
            DxDateBoxModule,
            DxDropDownBoxModule,
            DxDropDownButtonModule,
            DxFilterBuilderModule,
            DxListModule,
            DxMenuModule,
            DxNumberBoxModule,
            DxPopupModule,
            DxRadioGroupModule,
            DxScrollViewModule,
            DxSelectBoxModule,
            DxTemplateModule,
            DxTextBoxModule,
            DxTreeViewModule,
            DxValidationGroupModule,
            DxValidatorModule,
            FormsModule,
        ],
        exports: components$1
    })
], PBICommonGridFrameworkModule);

class PBIGridOptionsModel {
    // TODO: Parameterized constructor for assigning values
    constructor() {
        /* https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxDataGrid/Configuration/ for configuration of the dxGrid */
        //#region Grid Configuration Members
        // baseColumnsList: Array<IMS_Web.IGridColumn> = [];
        // columns: Array<IMS_Web.IGridColumn> = [];
        /* Specifies the shortcut key that sets focus on the widget.
            IE, Chrome, Safari, Opera 15+: [ALT] + accesskey
            Opera prior version 15: [SHIFT][ESC] + accesskey
            Firefox: [ALT][SHIFT] + accesskey */
        this.accessKey = 'g';
        this.allowAdding = false;
        this.allowColumnReordering = true;
        this.allowColumnResizing = true;
        this.allowDataExport = false;
        this.allowDeleting = false;
        this.allowReordering = false; // make sure rows can be re-ordered
        this.allowSelectedDataExport = false;
        this.allowUpdating = false;
        this.allowedPageSizes = [50, 150, 300];
        this.autoExpandAll = false;
        this.autoNavigateToFocusedRow = true;
        this.columnAutoWidth = false;
        this.columnMinWidth = 50;
        this.columnRenderingMode = 'virtual';
        this.columnResizingMode = 'widget';
        this.columns = [];
        this.contextMenuMappingList = [];
        this.dataSource = [];
        this.dateSerializationFormat = 'yyyy-MM-ddtHH:mm:ss';
        this.disabled = false;
        this.editingMode = 'row';
        this.enableActiveState = false;
        this.enableCache = true;
        this.enableCellHint = true;
        this.enableColumnChooser = false;
        this.enableColumnFixing = true;
        this.enableColumnHiding = true;
        this.enableContextGrpMenu = true;
        this.enableContextMenu = true;
        this.enableErrorRow = true;
        this.enableGridFormatting = false;
        this.enableLoadPanel = false;
        this.enableStateStoring = false;
        this.filterSyncEnabled = true;
        this.filterValue = null;
        this.gridComponentInstance = null;
        this.gridFilterValue = null;
        this.gridName = 'PortfolioBI Data Grid';
        this.highlightChanges = false;
        this.hoverStateEnabled = true;
        this.isMasterGrid = true;
        this.listGroupedColumns = [];
        this.noDataText = 'No Data';
        this.pageSize = 50;
        this.pagingEnabled = true;
        this.refreshMode = 'full';
        this.remoteOperationsEnabled = false;
        this.repaintChangesOnly = false;
        this.rowAlternationEnabled = false;
        this.scrollMode = 'standard';
        this.selectedTheme = '';
        this.selectedThemeClass = ''; // Empty means regular theme will be applied.
        this.selectionMode = 'single';
        this.showBorders = true;
        this.showColumnHeaders = true;
        this.showColumnLines = true;
        this.showDragIcons = false;
        this.showFilterPanel = true; // flag to show/hide filter info at bottom of grid.
        this.showFilterRow = true;
        this.showGroupPanel = true;
        this.showHeaderFilter = true;
        this.showPageInfo = true;
        this.showPageSizeSelector = false;
        this.showPager = true;
        this.showRowLines = true;
        this.showScrollbars = true;
        this.stateStorageType = null;
        this.showSearchPanel = false;
        this.sortingType = 'multiple';
        this.useIcons = false;
        this.stringSpecificOperators = ['contains', 'notcontains', 'startswith', 'endswith'];
        this.childEntityList = [];
        this.isMasterDetailEnabled = false;
    }
    applyViewToGrid(viewJson) {
        this.applyStateProperties(viewJson === null || viewJson === void 0 ? void 0 : viewJson.state.gridState);
    }
    onContextMenuPreparing(cellObject) {
        var _a;
        if (!this.enableContextMenu) {
            return;
        }
        // Added code to disable ungroupAll, when no column is grouped.
        if (cellObject.target === 'header' || cellObject.target === 'headerPanel') {
            const groupCount = cellObject.component.columnOption('groupIndex:0');
            if (!groupCount) {
                if (cellObject.items) {
                    cellObject.items.forEach((item) => {
                        if (item.value === 'ungroupAll') {
                            item.disabled = true;
                        }
                    });
                }
            }
            (_a = cellObject.items) === null || _a === void 0 ? void 0 : _a.push({
                disabled: false,
                icon: '',
                onItemClick: () => {
                    this.gridComponentInstance.beginUpdate();
                    const colCount = this.gridComponentInstance.columnCount();
                    for (let i = 0; i < colCount; i++) {
                        if (this.gridComponentInstance.columnOption(i, 'visible')) {
                            this.gridComponentInstance.columnOption(i, 'width', 'auto');
                        }
                    }
                    this.gridComponentInstance.endUpdate();
                },
                text: 'Auto Fit',
                value: 'autoFit',
            });
        }
        if (cellObject.row && cellObject.row.rowType === 'group') {
            cellObject.items.push({
                text: 'Expand All',
                onItemClick: () => { cellObject.component.expandAll(void 0); }
            });
            cellObject.items.push({
                text: 'Collapse All',
                onItemClick: () => { cellObject.component.collapseAll(void 0); }
            });
        }
        if (cellObject.row && cellObject.row.rowType === 'data') {
            const isNumberType = cellObject.column.dataType === 'number' ? true : false;
            cellObject.items = [{
                    visible: isNumberType,
                    text: 'Sum',
                    onItemClick: () => {
                        this.addOrRemoveAggregationSummary(cellObject, 'sum');
                    }
                },
                {
                    visible: isNumberType,
                    text: 'Avg',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'avg'); }
                },
                {
                    text: 'Max',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'max'); }
                },
                {
                    text: 'Min',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'min'); }
                },
                {
                    text: 'Count',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'count'); }
                },
                {
                    text: 'Reset',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'reset'); }
                }];
        }
    }
    ;
    onCellPrepared(cellInfo) {
        if (cellInfo.column && cellInfo.column.hasOwnProperty('dataField')) {
            // Added try catch so that in case formatting logic fails, user can still use CGF entity.
            try {
                this.cellFormat(cellInfo, true);
            }
            catch (error) {
                console.error(error);
                console.log('Cell formatting broke while exporting.', cellInfo);
            }
        }
    }
    onRowPrepared(args) {
        if (args.rowType === rowTypeInfo.data) {
            this.setCustomColumnData(args);
        }
        this.formatRows(args.rowType, args.data, args.rowElement, null);
    }
    cellFormat(cellInfo, isCellPrepared, excelGridCellInfo = null) {
        var _a, _b, _c;
        if (cellInfo) {
            let dataValue = null;
            switch (cellInfo.rowType) {
                case rowTypeInfo.totalFooter:
                    if (((_a = cellInfo.summaryItems) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                        dataValue = (_b = cellInfo.summaryItems[0]) === null || _b === void 0 ? void 0 : _b.value;
                    }
                    break;
                case rowTypeInfo.group:
                    if (!cellInfo.column.hasOwnProperty('command')
                        || (cellInfo.column.hasOwnProperty('command')
                            && cellInfo.column.command !== 'expand')) {
                        if ((_c = cellInfo.totalItem) === null || _c === void 0 ? void 0 : _c.summaryCells) {
                            const filterItem = cellInfo.totalItem.summaryCells.filter(item => { var _a; return cellInfo.column.dataField === ((_a = item[0]) === null || _a === void 0 ? void 0 : _a.column); })[0];
                            if (filterItem === null || filterItem === void 0 ? void 0 : filterItem.length) {
                                dataValue = filterItem[0].value;
                            }
                            else {
                                dataValue = cellInfo.value;
                            }
                        }
                    }
                    break;
                case rowTypeInfo.data:
                    dataValue = cellInfo.value;
                    break;
            }
            if (this.allowedRowTypes(cellInfo.rowType)) {
                if (!this.listGroupedColumns.length && cellInfo.component) {
                    this.listGroupedColumns = this.getGroupedColumnList(cellInfo.component);
                }
                this.prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue);
            }
            // else if (typeof (dataValue) === 'boolean' && cellInfo.rowType === rowTypeInfo.data) {
            //     this.prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue);
            // }
        }
    }
    allowedRowTypes(type) {
        switch (type) {
            case rowTypeInfo.data:
            case rowTypeInfo.group:
            case rowTypeInfo.totalFooter:
                return true;
        }
        return false;
    }
    prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue) {
        const rowTypeFilterForFormatting = 'row';
        const columnFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid))) || [];
        const baseFormatCollection = this.isMasterGrid ? JSON.parse(sessionStorage.getItem(CGFStorageKeys[CGFStorageKeys.dictionaryFormatData])) || [] : [];
        const conditionalFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid))) || [];
        const conditionFilterItemsForColumns = conditionalFormatCollection.filter(arrItem => arrItem.dataField === cellInfo.column.dataField && arrItem.applyType !== rowTypeFilterForFormatting);
        let conditionalFormattingRequired = false;
        const formatData = columnFormatCollection.find(arrItem => arrItem.dataField === cellInfo.column.dataField) ||
            baseFormatCollection.find(arrItem => arrItem.dataField === cellInfo.column.dataField);
        let applyBasicFormatting = true;
        conditionFilterItemsForColumns.forEach(filterItem => {
            var _a, _b, _c, _d, _e;
            if (filterItem.condition && cellInfo.rowType === 'data') {
                conditionalFormattingRequired = this.checkConditionSatisfied(filterItem, cellInfo.data);
            }
            if (conditionalFormattingRequired) {
                let _formattingData = Object.assign({}, formatData);
                if ((_formattingData === null || _formattingData === void 0 ? void 0 : _formattingData.dataField) === filterItem.dataField) {
                    for (const key in _formattingData) {
                        if (_formattingData.hasOwnProperty(key)) {
                            _formattingData.textColor = filterItem.textColor || _formattingData.textColor;
                            _formattingData.backgroundColor = filterItem.backgroundColor || _formattingData.backgroundColor;
                            // reset then apply
                            _formattingData.cssClass = (_c = (_b = (_a = _formattingData.cssClass) === null || _a === void 0 ? void 0 : _a.replace('bold', '')) === null || _b === void 0 ? void 0 : _b.replace('underline', '')) === null || _c === void 0 ? void 0 : _c.replace('italic', '');
                            _formattingData.cssClass = (_e = (_d = filterItem.cssClass) === null || _d === void 0 ? void 0 : _d.concat(' ')) === null || _e === void 0 ? void 0 : _e.concat(_formattingData.cssClass);
                        }
                    }
                }
                else {
                    _formattingData = Object.assign({}, filterItem);
                }
                this.applyFormatting(dataValue, _formattingData, isCellPrepared, cellInfo, excelGridCellInfo);
                applyBasicFormatting = false;
            }
        });
        if (formatData && applyBasicFormatting) {
            this.applyFormatting(dataValue, formatData, isCellPrepared, cellInfo, excelGridCellInfo);
        }
        else if (cellInfo.value !== cellInfo.text) { // !IMPORTANT Check why we need to use this condition. Since it worked in previous versions.
            cellInfo.text = cellInfo.value;
        }
    }
    checkConditionSatisfied(conditionInfo, rowData) {
        let conditionPassed = false;
        const updatedValue = this.startValidation(JSON.parse(JSON.stringify(conditionInfo.condition)), rowData);
        if (JSON.stringify(conditionInfo.condition) !== JSON.stringify(updatedValue)) {
            conditionPassed = this.transformAndRunValidation(updatedValue);
        }
        return conditionPassed;
    }
    startValidation(value, rowData) {
        if (value && Array.isArray(value[0])) {
            return value.map((item) => {
                return Array.isArray(item[0]) ? this.startValidation(item, rowData) : this.validate(item, rowData);
            });
        }
        return this.validate(value, rowData);
    }
    validate(item, rowData) {
        if (typeof item === 'string' && this.isReservedKeyWord(item)) {
            return item;
        }
        let _dataType = item[2] ? typeof item[2] : 'string';
        if (item[2] === 0 || item[2] === false) {
            _dataType = typeof item[2];
        }
        switch (_dataType) {
            case 'number':
                item = this.validateNumberType(item, rowData);
                break;
            case 'boolean':
                item = this.validateBooleanType(item, rowData);
                break;
            case 'string':
                if (item[2] && !isNaN(new Date(item[2]).getTime())) { // condition to check whether string is date type
                    if (this.stringSpecificOperators.indexOf(item[1]) > -1) { // condition since new date will return values for strings like '99'
                        item = this.validateStringType(item, rowData);
                    }
                    else {
                        item = this.validateDateType(item, rowData);
                    }
                }
                else {
                    item = this.validateStringType(item, rowData);
                }
                break;
            case 'object':
                if (Array.isArray(item[2])) {
                    if (typeof item[2][0] === 'string') {
                        item = this.validateDateType(item, rowData);
                    }
                    else if (typeof item[2][0] === 'number') {
                        item = this.validateNumberType(item, rowData);
                    }
                }
                break;
        }
        return item;
    }
    isReservedKeyWord(value) {
        switch (value) {
            case 'or':
            case 'and':
                return true;
        }
    }
    validateNumberType(item, rowData) {
        const filterVal1 = item[2][0] || item[2];
        const filterVal2 = item[2][1] || item[2];
        const isPassed = this.validateOperatorAndOperands(item[1], rowData[item[0]], filterVal1, filterVal2);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    }
    validateBooleanType(item, rowData) {
        const isPassed = this.validateOperatorAndOperands(item[1], rowData[item[0]], item[2]);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed.toString(); // this conversion(only for boolean) will be used in parent functions.
        }
        return item;
    }
    validateStringType(item, rowData) {
        var _a, _b;
        if (item[2] !== null) {
            item[2] = (_a = item[2]) === null || _a === void 0 ? void 0 : _a.toLowerCase();
        }
        let dataFieldValueInRow = rowData[item[0]];
        if (typeof rowData[item[0]] === 'string') {
            dataFieldValueInRow = ((_b = rowData[item[0]]) === null || _b === void 0 ? void 0 : _b.toLowerCase()) || null;
        }
        const isPassed = this.validateOperatorAndOperands(item[1], dataFieldValueInRow, item[2] === undefined ? '' : item[2]);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    }
    validateDateType(item, rowData) {
        let filterVal1 = item[2][0].length > 6 ? item[2][0] : item[2]; // conditions to check string is date only and 6 is since we have mm/dd/yyyy format
        let filterVal2 = item[2][1].length > 6 ? item[2][1] : item[2];
        filterVal1 = this.convertDateForComparison(filterVal1).getTime();
        filterVal2 = this.convertDateForComparison(filterVal2).getTime();
        let rowColData = this.convertDateForComparison(rowData[item[0]]).getTime();
        rowColData = isNaN(rowColData) ? '' : rowColData;
        const isPassed = this.validateOperatorAndOperands(item[1], rowColData, filterVal1, filterVal2);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    }
    convertDateForComparison(dateValue) {
        const dateTime = new Date(dateValue);
        const formattedDate = (dateTime.getMonth() + 1) + '-' + dateTime.getDate() + '-' + dateTime.getFullYear();
        return new Date(formattedDate);
    }
    validateOperatorAndOperands(operator, rowColumnData, filterValue, filterValue2) {
        if (typeof (rowColumnData) !== 'undefined') {
            switch (operator) {
                case 'contains':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.indexOf(filterValue)) > -1 ? true : false;
                case 'notcontains':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.indexOf(filterValue)) === -1 ? false : true;
                case 'startswith':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.startsWith(filterValue)) ? true : false;
                case 'endswith':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.endsWith(filterValue)) ? true : false;
                case 'isblank':
                    return rowColumnData === null || rowColumnData === '' ? true : false;
                case 'isnotblank':
                    return rowColumnData !== null && rowColumnData !== '' ? true : false;
                case 'between':
                    // we have assumption that fist value is min and second is max
                    return rowColumnData > filterValue && rowColumnData < filterValue2 ? true : false;
                case '=':
                    return rowColumnData === filterValue ? true : false;
                case '<>':
                    return rowColumnData !== filterValue ? true : false;
                case '<':
                    return rowColumnData < filterValue ? true : false;
                case '>':
                    return rowColumnData > filterValue ? true : false;
                case '>=':
                    return rowColumnData >= filterValue ? true : false;
                case '<=':
                    return rowColumnData <= filterValue ? true : false;
            }
        }
        return void 0;
    }
    transformAndRunValidation(value) {
        let evalString = '';
        value.forEach(item => {
            if (Array.isArray(item) && !Array.isArray(item[2])) {
                evalString += item[2];
            }
            else if (typeof item === 'string' || typeof item === 'boolean') {
                switch (item) {
                    case 'and':
                        evalString += ' && ';
                        break;
                    case 'or':
                        evalString += ' || ';
                        break;
                    case true:
                    case false:
                        evalString += ` ${item} `;
                        break;
                    case 'true':
                    case 'false':
                        evalString += ` ${JSON.parse(item)} `;
                        break;
                }
            }
            else if (Array.isArray(item)) {
                evalString = this.transformAndRunValidation(item);
            }
        });
        try {
            // tslint:disable-next-line: no-eval
            return eval(evalString);
        }
        catch (e) {
            console.error(e);
            return false;
        }
    }
    getGroupedColumnList(gridComponent) {
        const listGroupedColumns = [];
        const count = gridComponent.option('columns').length;
        for (let i = 0; i < count; i++) {
            const groupedColum = gridComponent.columnOption('groupIndex:' + i.toString());
            if (groupedColum) {
                listGroupedColumns.push(groupedColum.dataField.toLowerCase());
            }
        }
        return listGroupedColumns;
    }
    applyFormatting(dataValue, formatData, isCellPrepared, cellInfo, excelGridCellInfo) {
        var _a, _b;
        for (const _key in formatData) {
            if (formatData.hasOwnProperty(_key)) {
                const keyValue = formatData[_key] || '';
                switch (_key) {
                    case 'cssClass':
                        if (isCellPrepared) {
                            const classArray = `${keyValue}`.split(' ');
                            classArray.forEach(item => { if (item.trim()) {
                                cellInfo.cellElement.classList.add(item.trim());
                            } });
                        }
                        else if ((keyValue === null || keyValue === void 0 ? void 0 : keyValue.length) > 0) {
                            this.excelCellFormat(`${keyValue}`, excelGridCellInfo, dataValue);
                        }
                        break;
                    case 'format':
                        let assigneeObject = '';
                        if (isCellPrepared && formatData.dataType !== 'boolean') {
                            assigneeObject = dataValue; // first reset than apply format.
                        }
                        if (typeof keyValue !== 'string') {
                            switch (keyValue.type) {
                                case 'currency':
                                    assigneeObject = this.prepareUSDFormat(keyValue.precision, dataValue);
                                    break;
                                case 'percent':
                                    assigneeObject = this.preparePercentFormat(keyValue.precision, dataValue);
                                    break;
                                case 'comma':
                                    assigneeObject = this.prepareCommaFormat(keyValue.precision, dataValue);
                                    break;
                                case 'fixedPoint':
                                    assigneeObject = dataValue.toFixed(keyValue.precision);
                                    break;
                            }
                        }
                        else if (formatData[_key] === 'shortDate') {
                            const date = new Date(dataValue);
                            assigneeObject = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                        }
                        if (isCellPrepared && assigneeObject) { // added "AND" condition to prevent overriding value for boolean column
                            cellInfo.cellElement.innerText = assigneeObject;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.value = assigneeObject || dataValue;
                        }
                        break;
                    case 'alignment':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.textAlign = keyValue;
                        }
                        else {
                            excelGridCellInfo.alignment = { horizontal: keyValue };
                        }
                        if (cellInfo.rowType === 'totalFooter' && ((_b = (_a = cellInfo.cellElement) === null || _a === void 0 ? void 0 : _a.childNodes) === null || _b === void 0 ? void 0 : _b.length)) {
                            cellInfo.cellElement.childNodes[0].style.textAlign = keyValue;
                        }
                        break;
                    case 'textColor':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.color = keyValue;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { color: { argb: keyValue === null || keyValue === void 0 ? void 0 : keyValue.replace('#', 'ff') } });
                        }
                        break;
                    case 'backgroundColor':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.backgroundColor = keyValue;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: keyValue === null || keyValue === void 0 ? void 0 : keyValue.replace('#', 'ff') } };
                        }
                        break;
                    // case 'fixed': // !Important this case has been handled in databrowser.component.ts
                }
            }
        }
    }
    excelCellFormat(classList, excelGridCellInfo, val) {
        let classListData = classList.split(' ');
        let decimalPlacesData;
        classListData = classListData.filter((item) => {
            if (!item.toLowerCase().includes('decimaladded')) {
                return item;
            }
            else {
                decimalPlacesData = item;
            }
        });
        if (decimalPlacesData) {
            classListData.unshift(decimalPlacesData);
        }
        for (const formatClass of classListData) {
            switch (formatClass.toLowerCase()) {
                case 'bold':
                    excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { bold: true });
                    break;
                case 'underline':
                    excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { underline: true });
                    break;
                case 'italic':
                    excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { italic: true });
                    break;
            }
            if (formatClass.toLowerCase().includes('decimaladded')) {
                const precisionData = formatClass.split('-');
                if (precisionData.length > 0) {
                    excelGridCellInfo.value = val === null || val === void 0 ? void 0 : val.toFixed(Number(precisionData[1]));
                }
            }
        }
    }
    prepareUSDFormat(precision, value) {
        precision = precision === 0 ? 0 : precision || 2; // Default to 2 Decimal Places;
        return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    }
    preparePercentFormat(precision, value) {
        precision = precision === 0 ? 0 : precision || 3; // Default to 3 Decimal Places;
        return new Intl.NumberFormat('en-US', { style: 'percent', minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    }
    prepareCommaFormat(precision, value) {
        precision = precision === 0 ? 0 : precision || 0; // Default to 2 Decimal Places;
        return new Intl.NumberFormat('en-US', { useGrouping: true, minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    }
    addOrRemoveAggregationSummary(argsEvt, typeOfAggregation) {
        this.gridComponentInstance.beginUpdate();
        const totalSummaryItems = argsEvt.component.option('summary.totalItems') || [], groupSummaryItems = argsEvt.component.option('summary.groupItems') || [], indexOfTotalSummary = this.checkForExistingSummaryType(totalSummaryItems, argsEvt.column.dataField), indexOfGroupSummary = this.checkForExistingSummaryType(groupSummaryItems, argsEvt.column.dataField);
        const isNotTypeCountWithFormat = argsEvt.column.format && typeOfAggregation !== 'count';
        const summaryObj = {
            column: argsEvt.column.dataField,
            summaryType: typeOfAggregation,
            alignByColumn: true,
            displayFormat: '{0}',
            valueFormat: isNotTypeCountWithFormat ? argsEvt.column.format : { type: 'fixedPoint', precision: 0 }
            // showInGroupFooter: true
        };
        if (typeOfAggregation === 'reset') {
            if (indexOfTotalSummary !== -1 || indexOfGroupSummary !== -1) {
                totalSummaryItems.splice(indexOfTotalSummary, 1);
                groupSummaryItems.splice(indexOfGroupSummary, 1);
                this.gridComponentInstance.option('summary.totalItems', totalSummaryItems);
                this.gridComponentInstance.option('summary.groupItems', groupSummaryItems);
                // Making sure filters are reapplied
                if (argsEvt.component.state().filters) {
                    argsEvt.component.filter(argsEvt.component.state().filters);
                }
            }
            this.gridComponentInstance.endUpdate();
            return;
        }
        if (indexOfTotalSummary === -1 || indexOfGroupSummary === -1) {
            totalSummaryItems.push(summaryObj);
            groupSummaryItems.push(summaryObj);
        }
        else {
            totalSummaryItems.splice(indexOfTotalSummary, 1);
            groupSummaryItems.splice(indexOfGroupSummary, 1);
            totalSummaryItems.push(summaryObj);
            groupSummaryItems.push(summaryObj);
        }
        argsEvt.component.option('summary.totalItems', totalSummaryItems);
        argsEvt.component.option('summary.groupItems', groupSummaryItems);
        // Making sure filters are reapplied
        if (argsEvt.component.state().filters) {
            argsEvt.component.filter(argsEvt.component.state().filters);
        }
        this.gridComponentInstance.endUpdate();
    }
    checkForExistingSummaryType(summaryItemsCollection, columnName) {
        for (let i = 0; i < summaryItemsCollection.length; i++) {
            if (summaryItemsCollection[i].column === columnName) {
                return i;
            }
        }
        return -1;
    }
    applyStateProperties(viewJson) {
        var _a, _b, _c, _d, _e;
        if (!this.gridComponentInstance || !this.gridComponentInstance.NAME) {
            return;
        }
        this.selectedTheme = '';
        this.gridComponentInstance.beginCustomLoading('Refreshing Grid');
        this.gridComponentInstance.beginUpdate();
        if (viewJson) {
            if (viewJson.columns) {
                const viewColumnsLookup = ToDictionary((viewJson === null || viewJson === void 0 ? void 0 : viewJson.columns) || [], a => a.dataField.toLowerCase());
                for (let i = 0; i < this.columns.length; i++) {
                    const key = this.columns[i].dataField.toLowerCase();
                    if (viewColumnsLookup.hasOwnProperty(key)) {
                        this.columns[i].visible = viewColumnsLookup[key].visible;
                    }
                    else {
                        this.columns[i].visible = false;
                    }
                }
            }
            // Below loop is to OVERRIDE CAPTION OF COLUMN
            if (viewJson.visibleColumns && viewJson.visibleColumns.length) {
                for (let index = 0; index < viewJson.visibleColumns.length; index++) {
                    for (let _index = 0; _index < this.columns.length; _index++) {
                        const dataField = viewJson.visibleColumns[index].dataField;
                        if (dataField && dataField !== customActionColumnInfo.dataField &&
                            this.columns[_index].dataField.toLowerCase() === dataField.toLowerCase()) {
                            this.columns[_index].caption = viewJson.visibleColumns[index].caption;
                            // resetting already applied filter class.
                            this.columns[_index].cssClass = (_a = this.columns[_index].cssClass) === null || _a === void 0 ? void 0 : _a.replace(/filterApplied/g, '');
                            break;
                        }
                    }
                }
            }
            if (((_c = (_b = viewJson.summary) === null || _b === void 0 ? void 0 : _b.groupItems) === null || _c === void 0 ? void 0 : _c.length) || ((_e = (_d = viewJson.summary) === null || _d === void 0 ? void 0 : _d.totalItems) === null || _e === void 0 ? void 0 : _e.length)) {
                this.gridComponentInstance.option('summary', viewJson.summary);
            }
            else {
                viewJson.summary.groupItems = [];
                viewJson.summary.totalItems = [];
                this.gridComponentInstance.option('summary', viewJson.summary);
            }
            const formatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid)) || '""');
            if (formatCollection) {
                viewJson.columnFormattingInfo = Object.assign([], true, formatCollection, viewJson.columnFormattingInfo);
            }
            sessionStorage.setItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid), JSON.stringify(viewJson.columnFormattingInfo ? viewJson.columnFormattingInfo : ''));
            sessionStorage.setItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid), JSON.stringify(viewJson.conditionalFormattingInfo ? viewJson.conditionalFormattingInfo : null));
            this.gridFilterValue = viewJson ? viewJson.filterValue : null;
            if (this.gridFilterValue) {
                this.gridComponentInstance.option('filterValue', this.gridFilterValue);
                setTimeout(() => {
                    applyFilterCssClass(this.gridFilterValue, this.gridComponentInstance);
                }, 10);
            }
            if (viewJson.hasOwnProperty(CGFStorageKeys[CGFStorageKeys.childGridView])) {
                // this.setChildGridLayoutInfo(layoutJson);
            }
            appToast({ message: 'Selected view settings has been applied.', type: 'success' });
        }
        else {
            sessionStorage.removeItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid));
            sessionStorage.removeItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid));
            this.columns.forEach(column => {
                if (column.cssClass && column.cssClass.toLowerCase().trim() === 'filterapplied') {
                    column.cssClass = '';
                }
            });
        }
        this.selectedTheme = viewJson ? viewJson.selectedTheme || 'np.compact' : 'np.compact';
        sessionStorage.setItem('theme', this.selectedTheme);
        this.showColumnLines = viewJson ? viewJson.isGridBorderVisible || false : false;
        this.selectedThemeClass = getClassNameByThemeName(this.selectedTheme);
        this.gridComponentInstance.endUpdate();
        this.gridComponentInstance.state(viewJson);
        this.gridComponentInstance.endCustomLoading();
    }
    setCustomColumnData(args) {
        const customCols = args.columns.filter(col => col.expression && col.groupName === 'Custom Columns');
        customCols.forEach(col => {
            args.data[col.dataField] = args.values[col.index];
            this.columns.filter(column => column.dataField === col.dataField)[0].dataType = col.dataType;
        });
    }
    formatRows(dataType, rowData, rowElement, excelCell) {
        if (dataType === rowTypeInfo.data) {
            const conditionalFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid))) || [];
            const conditionFilterItems = conditionalFormatCollection.filter(arrItem => arrItem.applyType === 'row');
            let conditionalFormattingRequired = false;
            conditionFilterItems.forEach(filterItem => {
                var _a;
                if (filterItem.condition) {
                    conditionalFormattingRequired = this.checkConditionSatisfied(filterItem, rowData);
                    if (conditionalFormattingRequired) {
                        if (rowElement) {
                            for (let index = 0; index < rowElement.children.length; index++) {
                                const existingClasses = ((_a = rowElement.children[index].classList) === null || _a === void 0 ? void 0 : _a.toString().replace('bold', '').replace('underline', '').replace('italic', '')) || '';
                                rowElement.children[index].style.backgroundColor = filterItem.backgroundColor || rowElement.children[index].style.backgroundColor;
                                rowElement.children[index].style.color = filterItem.textColor || rowElement.children[index].style.color;
                                rowElement.children[index].classList = existingClasses.concat(' ').concat(filterItem.cssClass);
                            }
                        }
                        else if (excelCell) {
                            if (filterItem.backgroundColor) {
                                excelCell.fill = {
                                    type: 'pattern', pattern: 'solid',
                                    fgColor: { argb: filterItem.backgroundColor.replace('#', 'ff') }
                                };
                            }
                            if (filterItem.textColor) {
                                excelCell.font = {
                                    color: { argb: filterItem.textColor.replace('#', 'ff') }
                                };
                            }
                            if (filterItem.cssClass) {
                                this.excelCellFormat(filterItem.cssClass, excelCell, null);
                            }
                        }
                    }
                }
            });
        }
    }
}

class PBICommonGridFrameworkOptionsModel {
    constructor() {
        this.columnDebounce = 100;
        this.flyOutPosition = 'right';
        this.gridOptions = new PBIGridOptionsModel();
        this.viewList = [];
        this.refreshOnColumnUpdate = true;
        this.settingFlyOutOptions = [];
        this.showCGF = false;
        this.showFlyoutButtonContainer = true;
        this.showGrid = true;
        this.showLoader = false;
        this.useBulkApply = false;
        this.visibleFlyOuts = [];
        this.viewDataSource = null;
        this.dataBrowserEntityParameters = [];
    }
}

class DataBrowserViewSource {
    constructor(baseUrl, key, httpClient) {
        this.baseUrl = baseUrl;
        this.key = key;
        this.httpClient = httpClient;
        this.getViews = (key) => __awaiter(this, void 0, void 0, function* () {
            const views = yield this.httpClient.get(this.getUrl(`/layout/view-list/${key}`)).toPromise();
            return (views === null || views === void 0 ? void 0 : views.map(v => new ViewSelectionModel(v))) || [];
        });
        this.updateViewVisibility = (viewId, isPublic) => {
            return this.httpClient.get(this.getUrl(`/layout/layout-visibility/${viewId}/${isPublic}`)).toPromise();
        };
        this.saveView = (gridView) => __awaiter(this, void 0, void 0, function* () {
            const layout = this.transformToDataBrowserObject(gridView, this.key);
            const isValid = yield this.httpClient.post(this.getUrl('/layout/layout-validation'), layout).toPromise();
            if (isValid.IsValidationFailed) {
                throw this.error(isValid.ValidationMessage.join(', '));
            }
            return yield this.httpClient.post(this.getUrl(`/layout/layout`), layout).toPromise();
        });
        this.addView = (gridView) => __awaiter(this, void 0, void 0, function* () {
            const layout = this.transformToDataBrowserObject(gridView, this.key);
            const isValid = yield this.httpClient.post(this.getUrl('/layout/layout-validation'), layout).toPromise();
            if (isValid.IsValidationFailed) {
                throw this.error(isValid.ValidationMessage.join(', '));
            }
            return yield this.httpClient.post(this.getUrl(`/layout/layout`), layout).toPromise();
        });
        this.deleteView = (view) => __awaiter(this, void 0, void 0, function* () {
            const canDelete = yield this.httpClient.get(this.getUrl(`/layout/delete-layout-validation/${view.id}`)).toPromise();
            if (canDelete.IsValidationFailed) {
                throw this.error(canDelete.ValidationMessage.join(', '));
            }
            return yield this.httpClient.get(this.getUrl(`/layout/delete-layout?id=${view.id}&gridName=${this.key}&isGlobal=${view.isGlobalDefault}`)).toPromise();
        });
        this.applyView = (view) => __awaiter(this, void 0, void 0, function* () {
            const layout = yield this.httpClient.get(this.getUrl(`/layout/layout/${view.id}`)).toPromise();
            return this.transformToCGFObject(layout);
        });
        if (baseUrl[baseUrl.length - 1] === "/") {
            this.baseUrl = baseUrl.slice(0, baseUrl.length - 1);
        }
    }
    getUrl(endPoint) {
        return new URL(this.baseUrl + endPoint).toString();
    }
    error(message) {
        return { message: message, isError: true, exceptionMessage: '', toasterType: 'error' };
    }
    transformToDataBrowserObject(grid, key) {
        return {
            GridName: key,
            Name: grid.name,
            Id: grid.id,
            OverrideId: grid.id,
            IsPinned: grid.isPinned,
            IsPublic: grid.visibility,
            JsonLayout: JSON.stringify(grid.state),
            IsUserDefault: grid.isUserDefault,
            IsGlobalDefault: grid.isGlobalDefault,
            DataLoadOptions: null
        };
    }
    transformToCGFObject(view) {
        return {
            name: view.Name,
            id: view.Id,
            defaultOptions: null,
            isGlobalDefault: view.IsGlobalDefault,
            isUserDefault: view.IsUserDefault,
            state: JSON.parse(view.JsonLayout),
            visibility: view.IsPublic,
            isPinned: view.IsPinned,
        };
    }
}

//#endregion CGF

/**
 * Generated bundle index. Do not edit.
 */

export { CGFEventsEnum, CGFFeatureUpdateEnum, CGFFlyOutEnum, CGFSettingsEnum, CGFStorageKeys, CommonGridFrameworkComponent, DataBrowserViewSource, GridComponent, NavigationContentComponent, NavigationMenuComponent, NavigationSectionComponent, PBICommonGridFrameworkModule, PBICommonGridFrameworkOptionsModel, PBIGridOptionsModel, PortfolioBICoreModule, SecurityMasterAction, ViewSelectionModel, SearchService as ɵa, ColorFormatComponent as ɵb, CGFUtilityService as ɵc, ColumnChooserComponent as ɵd, FlyOutActionIconContainerComponent as ɵe, ConditionalFormattingComponent as ɵf, EntityParametersComponent as ɵg, FilterComponent as ɵh, SettingsComponent as ɵi, ViewSelectionComponent as ɵj };
//# sourceMappingURL=pbi-ng.js.map
