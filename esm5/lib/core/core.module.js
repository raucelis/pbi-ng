import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NavigationMenuComponent } from './components/navigation/navigation-menu.component';
import { NavigationSectionComponent } from './components/navigation/navigation-section.component';
import { NavigationContentComponent } from './components/navigation/navigation-content.component';
import { SearchService } from './services/search.service';
var components = [
    NavigationMenuComponent,
    NavigationSectionComponent,
    NavigationContentComponent
];
var PortfolioBICoreModule = /** @class */ (function () {
    function PortfolioBICoreModule() {
    }
    PortfolioBICoreModule = __decorate([
        NgModule({
            declarations: components,
            imports: [
                CommonModule,
                FormsModule
            ],
            exports: components,
            providers: [SearchService]
        })
    ], PortfolioBICoreModule);
    return PortfolioBICoreModule;
}());
export { PortfolioBICoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29yZS9jb3JlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTdDLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBQzVGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUcxRCxJQUFNLFVBQVUsR0FBVTtJQUN6Qix1QkFBdUI7SUFDdkIsMEJBQTBCO0lBQzFCLDBCQUEwQjtDQUMxQixDQUFDO0FBV0Y7SUFBQTtJQUFxQyxDQUFDO0lBQXpCLHFCQUFxQjtRQVRqQyxRQUFRLENBQUM7WUFDVCxZQUFZLEVBQUUsVUFBVTtZQUN4QixPQUFPLEVBQUU7Z0JBQ1IsWUFBWTtnQkFDWixXQUFXO2FBQ1g7WUFDRCxPQUFPLEVBQUUsVUFBVTtZQUNuQixTQUFTLEVBQUUsQ0FBQyxhQUFhLENBQUM7U0FDMUIsQ0FBQztPQUNXLHFCQUFxQixDQUFJO0lBQUQsNEJBQUM7Q0FBQSxBQUF0QyxJQUFzQztTQUF6QixxQkFBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCB7IE5hdmlnYXRpb25NZW51Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi1tZW51LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25TZWN0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi1zZWN0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25Db250ZW50Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi1jb250ZW50LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNlYXJjaFNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3NlYXJjaC5zZXJ2aWNlJztcclxuXHJcblxyXG5jb25zdCBjb21wb25lbnRzOiBhbnlbXSA9IFtcclxuXHROYXZpZ2F0aW9uTWVudUNvbXBvbmVudCxcclxuXHROYXZpZ2F0aW9uU2VjdGlvbkNvbXBvbmVudCxcclxuXHROYXZpZ2F0aW9uQ29udGVudENvbXBvbmVudFxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuXHRkZWNsYXJhdGlvbnM6IGNvbXBvbmVudHMsXHJcblx0aW1wb3J0czogW1xyXG5cdFx0Q29tbW9uTW9kdWxlLFxyXG5cdFx0Rm9ybXNNb2R1bGVcclxuXHRdLFxyXG5cdGV4cG9ydHM6IGNvbXBvbmVudHMsXHJcblx0cHJvdmlkZXJzOiBbU2VhcmNoU2VydmljZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBvcnRmb2xpb0JJQ29yZU1vZHVsZSB7IH1cclxuIl19