import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, OnInit, ElementRef, Renderer2, HostBinding, ViewChild } from '@angular/core';
var NavigationContentComponent = /** @class */ (function () {
    function NavigationContentComponent(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.navigateTo = new EventEmitter();
        this.navigateToNewTab = new EventEmitter();
        this.activeItemConfig = null;
    }
    Object.defineProperty(NavigationContentComponent.prototype, "config", {
        get: function () {
            return this._config;
        },
        set: function (v) {
            this._config = v;
            this.computeContentMaxHeight();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationContentComponent.prototype, "top", {
        set: function (v) {
            this.renderer.setStyle(this.el.nativeElement, 'top', v + "px");
            this.computeContentMaxHeight();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationContentComponent.prototype, "browsing", {
        get: function () {
            return !!this.activeItemConfig;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationContentComponent.prototype, "contentTop", {
        get: function () {
            return this._contentTop;
        },
        enumerable: true,
        configurable: true
    });
    NavigationContentComponent.prototype.ngOnInit = function () { };
    NavigationContentComponent.prototype.ngAfterViewInit = function () {
        this.computeContentMaxHeight();
    };
    NavigationContentComponent.prototype.browse = function (config, event) {
        event.stopImmediatePropagation();
        if (this.isBrowsing(config)) {
            this.activeItemConfig = null;
            return;
        }
        if (config.content) {
            var target = event.target;
            var p = target.closest('.navigation-item');
            this._contentTop = p.offsetTop - this.items.nativeElement.scrollTop;
            this.activeItemConfig = config;
        }
        else {
            this.onNavigateTo(config);
        }
    };
    NavigationContentComponent.prototype.browseOrNavigate = function (config, event) {
        config.link ? this.onNavigateTo(config) : this.browse(config, event);
    };
    NavigationContentComponent.prototype.isBrowsing = function (config) {
        return this.activeItemConfig === config;
    };
    NavigationContentComponent.prototype.onNavigateTo = function (config, newTab) {
        this.activeItemConfig = null;
        newTab ? this.navigateToNewTab.emit(config) : this.navigateTo.emit(config);
    };
    NavigationContentComponent.prototype.computeContentMaxHeight = function () {
        if (!this.items) {
            return;
        }
        this.activeItemConfig = null;
        var top = this.el.nativeElement.offsetTop;
        var parent = this.el.nativeElement.offsetParent;
        while (parent) {
            top += parent.offsetTop;
            parent = parent.offsetParent;
        }
        var maxHeight = window.innerHeight - top;
        this.renderer.setStyle(this.items.nativeElement, 'max-height', maxHeight + "px");
    };
    NavigationContentComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    __decorate([
        Input()
    ], NavigationContentComponent.prototype, "config", null);
    __decorate([
        Input()
    ], NavigationContentComponent.prototype, "top", null);
    __decorate([
        HostBinding('class.browsing')
    ], NavigationContentComponent.prototype, "browsing", null);
    __decorate([
        Output()
    ], NavigationContentComponent.prototype, "navigateTo", void 0);
    __decorate([
        Output()
    ], NavigationContentComponent.prototype, "navigateToNewTab", void 0);
    __decorate([
        ViewChild('items')
    ], NavigationContentComponent.prototype, "items", void 0);
    NavigationContentComponent = __decorate([
        Component({
            selector: 'pbi-navigation-content',
            template: "<div #items class=\"items\">\r\n    <div *ngFor=\"let groupConfig of config.groups\" class=\"navigation-group\">\r\n        <span *ngIf=\"groupConfig.title\">{{groupConfig.title}}</span>\r\n        <div *ngFor=\"let itemConfig of groupConfig.items\"  class=\"navigation-item\">\r\n            <div [title]=\"itemConfig.title\">\r\n                <div (click)=\"browseOrNavigate(itemConfig, $event)\">\r\n                    <i class=\"{{itemConfig.icon}} icon\"></i>\r\n                    <span class=\"text\">{{itemConfig.title}}</span>\r\n                </div>\r\n                <div>\r\n                    <div *ngIf=\"itemConfig.openNewTab\" (click)=\"onNavigateTo(itemConfig, true)\">\r\n                        <i class=\"fa fa-external-link icon\"></i>\r\n                    </div>\r\n                </div>\r\n                <div>\r\n                    <div *ngIf=\"itemConfig.content\" (click)=\"browse(itemConfig, $event)\">\r\n                        <i *ngIf=\"isBrowsing(itemConfig); else notBrowsing\" class=\"fa fa-angle-down icon\"></i>\r\n                        <ng-template #notBrowsing>\r\n                            <i class=\"fa fa-angle-right icon\"></i>\r\n                        </ng-template>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <ng-template [ngIf]=\"isBrowsing(itemConfig)\">\r\n                <pbi-navigation-content class=\"child\" \r\n                    [config]=\"itemConfig.content\" \r\n                    (navigateTo)=\"onNavigateTo($event)\" \r\n                    (navigateToNewTab)=\"onNavigateTo($event, true)\">\r\n                </pbi-navigation-content>\r\n            </ng-template>\r\n        </div>\r\n    </div>\r\n</div>\r\n<ng-template [ngIf]=\"activeItemConfig\">\r\n    <pbi-navigation-content \r\n        class=\"flyout\"\r\n        [config]=\"activeItemConfig.content\"\r\n        [top]=\"contentTop\" \r\n        (navigateTo)=\"onNavigateTo($event)\" \r\n        (navigateToNewTab)=\"onNavigateTo($event, true)\">\r\n    </pbi-navigation-content>\r\n</ng-template> "
        })
    ], NavigationContentComponent);
    return NavigationContentComponent;
}());
export { NavigationContentComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1jb250ZW50LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLWNvbnRlbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFROUg7SUEwQkksb0NBQ1ksRUFBYyxFQUNMLFFBQW1CO1FBRDVCLE9BQUUsR0FBRixFQUFFLENBQVk7UUFDTCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBWDlCLGVBQVUsR0FBc0MsSUFBSSxZQUFZLEVBQXVCLENBQUM7UUFDeEYscUJBQWdCLEdBQXNDLElBQUksWUFBWSxFQUF1QixDQUFDO1FBRWpHLHFCQUFnQixHQUF5QixJQUFJLENBQUM7SUFRVCxDQUFDO0lBekJwQyxzQkFBSSw4Q0FBTTthQUluQjtZQUNJLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQTtRQUN2QixDQUFDO2FBTlEsVUFBVyxDQUEwQjtZQUMxQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztZQUNqQixJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUNuQyxDQUFDOzs7T0FBQTtJQUlRLHNCQUFJLDJDQUFHO2FBQVAsVUFBUSxDQUFTO1lBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLEtBQUssRUFBSyxDQUFDLE9BQUksQ0FBQyxDQUFDO1lBQy9ELElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBQ25DLENBQUM7OztPQUFBO0lBQzhCLHNCQUFJLGdEQUFRO2FBQVo7WUFDM0IsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQ25DLENBQUM7OztPQUFBO0lBTUQsc0JBQUksa0RBQVU7YUFBZDtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUM1QixDQUFDOzs7T0FBQTtJQU1NLDZDQUFRLEdBQWYsY0FBMEIsQ0FBQztJQUNwQixvREFBZSxHQUF0QjtRQUNJLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFFTSwyQ0FBTSxHQUFiLFVBQWMsTUFBNEIsRUFBRSxLQUFpQjtRQUN6RCxLQUFLLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNqQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDekIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUM3QixPQUFPO1NBQ1Y7UUFDRCxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQXFCLENBQUM7WUFDM0MsSUFBTSxDQUFDLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBZ0IsQ0FBQztZQUM1RCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO1lBQ3BFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7U0FDbEM7YUFBTTtZQUNILElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDN0I7SUFDTCxDQUFDO0lBRU0scURBQWdCLEdBQXZCLFVBQXdCLE1BQTRCLEVBQUUsS0FBaUI7UUFDbkUsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDekUsQ0FBQztJQUVNLCtDQUFVLEdBQWpCLFVBQWtCLE1BQTRCO1FBQzFDLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQztJQUM1QyxDQUFDO0lBRU0saURBQVksR0FBbkIsVUFBb0IsTUFBMkIsRUFBRSxNQUFnQjtRQUM3RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVPLDREQUF1QixHQUEvQjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQUUsT0FBTztTQUFFO1FBRTVCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDN0IsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO1FBQzFDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztRQUNoRCxPQUFPLE1BQU0sRUFBRTtZQUNYLEdBQUcsSUFBSSxNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ3hCLE1BQU0sR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO1NBQ2hDO1FBQ0QsSUFBTSxTQUFTLEdBQUcsTUFBTSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7UUFDM0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsWUFBWSxFQUFLLFNBQVMsT0FBSSxDQUFDLENBQUM7SUFDckYsQ0FBQzs7Z0JBakRlLFVBQVU7Z0JBQ0ssU0FBUzs7SUF6Qi9CO1FBQVIsS0FBSyxFQUFFOzREQUdQO0lBSVE7UUFBUixLQUFLLEVBQUU7eURBR1A7SUFDOEI7UUFBOUIsV0FBVyxDQUFDLGdCQUFnQixDQUFDOzhEQUU3QjtJQUNTO1FBQVQsTUFBTSxFQUFFO2tFQUF5RjtJQUN4RjtRQUFULE1BQU0sRUFBRTt3RUFBK0Y7SUFDcEY7UUFBbkIsU0FBUyxDQUFDLE9BQU8sQ0FBQzs2REFBMkI7SUFuQnJDLDBCQUEwQjtRQUp0QyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsd0JBQXdCO1lBQ2xDLHFqRUFBa0Q7U0FDckQsQ0FBQztPQUNXLDBCQUEwQixDQTZFdEM7SUFBRCxpQ0FBQztDQUFBLEFBN0VELElBNkVDO1NBN0VZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIEVsZW1lbnRSZWYsIFJlbmRlcmVyMiwgSG9zdEJpbmRpbmcsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmF2aWdhdGlvbkNvbnRlbnRDb25maWcsIE5hdmlnYXRpb25JdGVtQ29uZmlnLCBOYXZpZ2FibGVJdGVtQ29uZmlnIH0gZnJvbSAnLi4vY29udHJhY3RzL25hdmlnYXRpb24nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3BiaS1uYXZpZ2F0aW9uLWNvbnRlbnQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL25hdmlnYXRpb24tY29udGVudC5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5hdmlnYXRpb25Db250ZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBwcml2YXRlIF9jb25maWc6IE5hdmlnYXRpb25Db250ZW50Q29uZmlnO1xyXG4gICAgQElucHV0KCkgc2V0IGNvbmZpZyh2OiBOYXZpZ2F0aW9uQ29udGVudENvbmZpZykge1xyXG4gICAgICAgIHRoaXMuX2NvbmZpZyA9IHY7XHJcbiAgICAgICAgdGhpcy5jb21wdXRlQ29udGVudE1heEhlaWdodCgpO1xyXG4gICAgfVxyXG4gICAgZ2V0IGNvbmZpZygpOiBOYXZpZ2F0aW9uQ29udGVudENvbmZpZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbmZpZ1xyXG4gICAgfVxyXG4gICAgQElucHV0KCkgc2V0IHRvcCh2OiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuZWwubmF0aXZlRWxlbWVudCwgJ3RvcCcsIGAke3Z9cHhgKTtcclxuICAgICAgICB0aGlzLmNvbXB1dGVDb250ZW50TWF4SGVpZ2h0KCk7XHJcbiAgICB9XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmJyb3dzaW5nJykgZ2V0IGJyb3dzaW5nKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAhIXRoaXMuYWN0aXZlSXRlbUNvbmZpZztcclxuICAgIH1cclxuICAgIEBPdXRwdXQoKSBuYXZpZ2F0ZVRvOiBFdmVudEVtaXR0ZXI8TmF2aWdhYmxlSXRlbUNvbmZpZz4gPSBuZXcgRXZlbnRFbWl0dGVyPE5hdmlnYWJsZUl0ZW1Db25maWc+KCk7XHJcbiAgICBAT3V0cHV0KCkgbmF2aWdhdGVUb05ld1RhYjogRXZlbnRFbWl0dGVyPE5hdmlnYWJsZUl0ZW1Db25maWc+ID0gbmV3IEV2ZW50RW1pdHRlcjxOYXZpZ2FibGVJdGVtQ29uZmlnPigpO1xyXG4gICAgQFZpZXdDaGlsZCgnaXRlbXMnKSBwcml2YXRlIGl0ZW1zOiBFbGVtZW50UmVmO1xyXG4gICAgcHVibGljIGFjdGl2ZUl0ZW1Db25maWc6IE5hdmlnYXRpb25JdGVtQ29uZmlnID0gbnVsbDtcclxuICAgIHByaXZhdGUgX2NvbnRlbnRUb3A6IG51bWJlcjtcclxuICAgIGdldCBjb250ZW50VG9wKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbnRlbnRUb3A7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZixcclxuICAgICAgICBwcml2YXRlIHJlYWRvbmx5IHJlbmRlcmVyOiBSZW5kZXJlcjIpIHsgfVxyXG5cclxuICAgIHB1YmxpYyBuZ09uSW5pdCgpOiB2b2lkIHsgfVxyXG4gICAgcHVibGljIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNvbXB1dGVDb250ZW50TWF4SGVpZ2h0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGJyb3dzZShjb25maWc6IE5hdmlnYXRpb25JdGVtQ29uZmlnLCBldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGlmICh0aGlzLmlzQnJvd3NpbmcoY29uZmlnKSkge1xyXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZUl0ZW1Db25maWcgPSBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjb25maWcuY29udGVudCkge1xyXG4gICAgICAgICAgICBjb25zdCB0YXJnZXQgPSBldmVudC50YXJnZXQgYXMgSFRNTEVsZW1lbnQ7XHJcbiAgICAgICAgICAgIGNvbnN0IHAgPSB0YXJnZXQuY2xvc2VzdCgnLm5hdmlnYXRpb24taXRlbScpIGFzIEhUTUxFbGVtZW50O1xyXG4gICAgICAgICAgICB0aGlzLl9jb250ZW50VG9wID0gcC5vZmZzZXRUb3AgLSB0aGlzLml0ZW1zLm5hdGl2ZUVsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZUl0ZW1Db25maWcgPSBjb25maWc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5vbk5hdmlnYXRlVG8oY29uZmlnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGJyb3dzZU9yTmF2aWdhdGUoY29uZmlnOiBOYXZpZ2F0aW9uSXRlbUNvbmZpZywgZXZlbnQ6IE1vdXNlRXZlbnQpOiB2b2lkIHtcclxuICAgICAgICBjb25maWcubGluayA/IHRoaXMub25OYXZpZ2F0ZVRvKGNvbmZpZykgOiB0aGlzLmJyb3dzZShjb25maWcsIGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaXNCcm93c2luZyhjb25maWc6IE5hdmlnYXRpb25JdGVtQ29uZmlnKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWN0aXZlSXRlbUNvbmZpZyA9PT0gY29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbk5hdmlnYXRlVG8oY29uZmlnOiBOYXZpZ2FibGVJdGVtQ29uZmlnLCBuZXdUYWI/OiBib29sZWFuKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVJdGVtQ29uZmlnID0gbnVsbDtcclxuICAgICAgICBuZXdUYWIgPyB0aGlzLm5hdmlnYXRlVG9OZXdUYWIuZW1pdChjb25maWcpIDogdGhpcy5uYXZpZ2F0ZVRvLmVtaXQoY29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNvbXB1dGVDb250ZW50TWF4SGVpZ2h0KCk6IHZvaWQge1xyXG4gICAgICAgIGlmICghdGhpcy5pdGVtcykgeyByZXR1cm47IH1cclxuICAgICAgICBcclxuICAgICAgICB0aGlzLmFjdGl2ZUl0ZW1Db25maWcgPSBudWxsO1xyXG4gICAgICAgIGxldCB0b3AgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQub2Zmc2V0VG9wO1xyXG4gICAgICAgIGxldCBwYXJlbnQgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQub2Zmc2V0UGFyZW50O1xyXG4gICAgICAgIHdoaWxlIChwYXJlbnQpIHtcclxuICAgICAgICAgICAgdG9wICs9IHBhcmVudC5vZmZzZXRUb3A7XHJcbiAgICAgICAgICAgIHBhcmVudCA9IHBhcmVudC5vZmZzZXRQYXJlbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IG1heEhlaWdodCA9IHdpbmRvdy5pbm5lckhlaWdodCAtIHRvcDtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLnNldFN0eWxlKHRoaXMuaXRlbXMubmF0aXZlRWxlbWVudCwgJ21heC1oZWlnaHQnLCBgJHttYXhIZWlnaHR9cHhgKTtcclxuICAgIH1cclxufSJdfQ==