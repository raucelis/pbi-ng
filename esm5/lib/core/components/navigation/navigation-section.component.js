import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SearchService } from '../../services/search.service';
var NavigationSectionComponent = /** @class */ (function () {
    function NavigationSectionComponent(searchService) {
        this.searchService = searchService;
        this.action = new EventEmitter();
        this.navigateTo = new EventEmitter();
        this.navigateToNewTab = new EventEmitter();
    }
    ;
    Object.defineProperty(NavigationSectionComponent.prototype, "contentConfig", {
        get: function () {
            return this.config ?
                this.searchQuery ? this._filteredContentConfig : this.config.content :
                null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationSectionComponent.prototype, "infoItems", {
        get: function () {
            var items = [];
            if (this.config.informational && this.config.content.groups) {
                items = this.config.content.groups[0].items;
            }
            return items;
        },
        enumerable: true,
        configurable: true
    });
    NavigationSectionComponent.prototype.ngOnInit = function () { };
    NavigationSectionComponent.prototype.search = function () {
        if (!this.searchQuery) {
            return;
        }
        var items = [];
        this.flattenTree(this.config.content, items);
        this._filteredContentConfig = {
            groups: [{
                    title: items.length > 0 ? 'results' : 'no matches',
                    items: items
                }]
        };
    };
    NavigationSectionComponent.prototype.onActionClick = function (action) {
        this.action.emit(action.id);
    };
    NavigationSectionComponent.prototype.onNavigateTo = function (config, newTab) {
        newTab ? this.navigateToNewTab.emit(config) : this.navigateTo.emit(config);
    };
    NavigationSectionComponent.prototype.flattenTree = function (config, items) {
        var _this = this;
        config.groups.forEach(function (g) {
            g.items.forEach(function (i) {
                if (i.content) {
                    _this.flattenTree(i.content, items);
                }
                else if (_this.searchService.deepContains(i, _this.searchQuery)) {
                    items.push(i);
                }
            });
        });
    };
    NavigationSectionComponent.ctorParameters = function () { return [
        { type: SearchService }
    ]; };
    __decorate([
        Input()
    ], NavigationSectionComponent.prototype, "config", void 0);
    __decorate([
        Output()
    ], NavigationSectionComponent.prototype, "action", void 0);
    __decorate([
        Output()
    ], NavigationSectionComponent.prototype, "navigateTo", void 0);
    __decorate([
        Output()
    ], NavigationSectionComponent.prototype, "navigateToNewTab", void 0);
    NavigationSectionComponent = __decorate([
        Component({
            selector: 'pbi-navigation-section',
            template: "<ng-template [ngIf]=\"config\">\r\n    <ng-template [ngIf]=\"config.searchable\">\r\n        <div class=\"search-box dark\">\r\n            <div class=\"form-group\" role=\"search\">\r\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"searchQuery\" name=\"search\" (keyup)=\"search()\" autocomplete=\"off\" />\r\n            </div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template [ngIf]=\"config.actions\">\r\n        <div class=\"actions\">\r\n            <div>\r\n                <div *ngFor=\"let action of config.actions\" class=\"navigation-action\" (click)=\"onActionClick(action)\">\r\n                    <i [class]=\"action.icon\" [title]=\"action.title\"></i>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template [ngIf]=\"config.informational\" [ngIfElse]=\"navigationContent\">\r\n        <div class=\"info\">\r\n            <div *ngFor=\"let item of infoItems\">{{item.title}}</div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template #navigationContent>\r\n        <pbi-navigation-content [config]=\"contentConfig\" (navigateTo)=\"onNavigateTo($event)\" (navigateToNewTab)=\"onNavigateTo($event, true)\"></pbi-navigation-content>\r\n    </ng-template>\r\n</ng-template>"
        })
    ], NavigationSectionComponent);
    return NavigationSectionComponent;
}());
export { NavigationSectionComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1zZWN0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLXNlY3Rpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQU05RDtJQXlCSSxvQ0FDcUIsYUFBNEI7UUFBNUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUF2QnZDLFdBQU0sR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUMxRCxlQUFVLEdBQXNDLElBQUksWUFBWSxFQUF1QixDQUFDO1FBQ3hGLHFCQUFnQixHQUFzQyxJQUFJLFlBQVksRUFBdUIsQ0FBQztJQXNCeEcsQ0FBQztJQXhCbUUsQ0FBQztJQU1yRSxzQkFBSSxxREFBYTthQUFqQjtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNoQixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3RFLElBQUksQ0FBQztRQUNiLENBQUM7OztPQUFBO0lBRUQsc0JBQUksaURBQVM7YUFBYjtZQUNJLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUNmLElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFDO2dCQUN2RCxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTthQUM5QztZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUM7OztPQUFBO0lBUU0sNkNBQVEsR0FBZixjQUF5QixDQUFDO0lBRW5CLDJDQUFNLEdBQWI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUFFLE9BQU87U0FBRTtRQUNsQyxJQUFJLEtBQUssR0FBMkIsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLHNCQUFzQixHQUFHO1lBQzFCLE1BQU0sRUFBRSxDQUFDO29CQUNMLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZO29CQUNsRCxLQUFLLEVBQUUsS0FBSztpQkFDZixDQUFDO1NBQ0wsQ0FBQztJQUNOLENBQUM7SUFFTSxrREFBYSxHQUFwQixVQUFxQixNQUE4QjtRQUMvQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVNLGlEQUFZLEdBQW5CLFVBQW9CLE1BQTJCLEVBQUUsTUFBZ0I7UUFDN0QsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMvRSxDQUFDO0lBRU8sZ0RBQVcsR0FBbkIsVUFBb0IsTUFBK0IsRUFBRSxLQUE2QjtRQUFsRixpQkFVQztRQVRHLE1BQU0sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQztZQUNuQixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFBLENBQUM7Z0JBQ2IsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFO29CQUNYLEtBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDdEM7cUJBQU0sSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxFQUFDO29CQUMzRCxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkFuQ21DLGFBQWE7O0lBeEJ4QztRQUFSLEtBQUssRUFBRTs4REFBaUM7SUFDL0I7UUFBVCxNQUFNLEVBQUU7OERBQTJEO0lBQzFEO1FBQVQsTUFBTSxFQUFFO2tFQUF5RjtJQUN4RjtRQUFULE1BQU0sRUFBRTt3RUFBK0Y7SUFML0YsMEJBQTBCO1FBSnRDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSx3QkFBd0I7WUFDbEMsOHdDQUFrRDtTQUNyRCxDQUFDO09BQ1csMEJBQTBCLENBK0R0QztJQUFELGlDQUFDO0NBQUEsQUEvREQsSUErREM7U0EvRFksMEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uU2VjdGlvbkNvbmZpZywgTmF2aWdhdGlvbkNvbnRlbnRDb25maWcsIE5hdmlnYXRpb25BY3Rpb25Db25maWcsIE5hdmlnYWJsZUl0ZW1Db25maWcsIE5hdmlnYXRpb25JdGVtQ29uZmlnIH0gZnJvbSAnLi4vY29udHJhY3RzL25hdmlnYXRpb24nO1xyXG5pbXBvcnQgeyBTZWFyY2hTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvc2VhcmNoLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3BiaS1uYXZpZ2F0aW9uLXNlY3Rpb24nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL25hdmlnYXRpb24tc2VjdGlvbi5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5hdmlnYXRpb25TZWN0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKSBjb25maWc6IE5hdmlnYXRpb25TZWN0aW9uQ29uZmlnO1xyXG4gICAgQE91dHB1dCgpIGFjdGlvbjogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTs7XHJcbiAgICBAT3V0cHV0KCkgbmF2aWdhdGVUbzogRXZlbnRFbWl0dGVyPE5hdmlnYWJsZUl0ZW1Db25maWc+ID0gbmV3IEV2ZW50RW1pdHRlcjxOYXZpZ2FibGVJdGVtQ29uZmlnPigpO1xyXG4gICAgQE91dHB1dCgpIG5hdmlnYXRlVG9OZXdUYWI6IEV2ZW50RW1pdHRlcjxOYXZpZ2FibGVJdGVtQ29uZmlnPiA9IG5ldyBFdmVudEVtaXR0ZXI8TmF2aWdhYmxlSXRlbUNvbmZpZz4oKTtcclxuXHJcbiAgICBwcml2YXRlIF9maWx0ZXJlZENvbnRlbnRDb25maWc6IE5hdmlnYXRpb25Db250ZW50Q29uZmlnO1xyXG5cclxuICAgIGdldCBjb250ZW50Q29uZmlnKCk6IE5hdmlnYXRpb25Db250ZW50Q29uZmlnIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb25maWcgP1xyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaFF1ZXJ5ID8gdGhpcy5fZmlsdGVyZWRDb250ZW50Q29uZmlnIDogdGhpcy5jb25maWcuY29udGVudCA6XHJcbiAgICAgICAgICAgIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGluZm9JdGVtcygpOiBOYXZpZ2F0aW9uSXRlbUNvbmZpZ1tde1xyXG4gICAgICAgIGxldCBpdGVtcyA9IFtdO1xyXG4gICAgICAgIGlmKHRoaXMuY29uZmlnLmluZm9ybWF0aW9uYWwgJiYgdGhpcy5jb25maWcuY29udGVudC5ncm91cHMpe1xyXG4gICAgICAgICAgICBpdGVtcyA9IHRoaXMuY29uZmlnLmNvbnRlbnQuZ3JvdXBzWzBdLml0ZW1zXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpdGVtcztcclxuICAgIH1cclxuXHJcbiAgICBzZWFyY2hRdWVyeTogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgcmVhZG9ubHkgc2VhcmNoU2VydmljZTogU2VhcmNoU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBuZ09uSW5pdCgpOiB2b2lkIHt9XHJcblxyXG4gICAgcHVibGljIHNlYXJjaCgpOiB2b2lkIHtcclxuICAgICAgICBpZiAoIXRoaXMuc2VhcmNoUXVlcnkpIHsgcmV0dXJuOyB9XHJcbiAgICAgICAgbGV0IGl0ZW1zOiBOYXZpZ2F0aW9uSXRlbUNvbmZpZ1tdID0gW107XHJcbiAgICAgICAgdGhpcy5mbGF0dGVuVHJlZSh0aGlzLmNvbmZpZy5jb250ZW50LCBpdGVtcyk7XHJcbiAgICAgICAgdGhpcy5fZmlsdGVyZWRDb250ZW50Q29uZmlnID0ge1xyXG4gICAgICAgICAgICBncm91cHM6IFt7XHJcbiAgICAgICAgICAgICAgICB0aXRsZTogaXRlbXMubGVuZ3RoID4gMCA/ICdyZXN1bHRzJyA6ICdubyBtYXRjaGVzJyxcclxuICAgICAgICAgICAgICAgIGl0ZW1zOiBpdGVtc1xyXG4gICAgICAgICAgICB9XVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uQWN0aW9uQ2xpY2soYWN0aW9uOiBOYXZpZ2F0aW9uQWN0aW9uQ29uZmlnKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hY3Rpb24uZW1pdChhY3Rpb24uaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBvbk5hdmlnYXRlVG8oY29uZmlnOiBOYXZpZ2FibGVJdGVtQ29uZmlnLCBuZXdUYWI/OiBib29sZWFuKSB7XHJcbiAgICAgICAgbmV3VGFiID8gdGhpcy5uYXZpZ2F0ZVRvTmV3VGFiLmVtaXQoY29uZmlnKSA6IHRoaXMubmF2aWdhdGVUby5lbWl0KGNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBmbGF0dGVuVHJlZShjb25maWc6IE5hdmlnYXRpb25Db250ZW50Q29uZmlnLCBpdGVtczogTmF2aWdhdGlvbkl0ZW1Db25maWdbXSk6IHZvaWQge1xyXG4gICAgICAgIGNvbmZpZy5ncm91cHMuZm9yRWFjaChnID0+IHtcclxuICAgICAgICAgICAgZy5pdGVtcy5mb3JFYWNoKGkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGkuY29udGVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmxhdHRlblRyZWUoaS5jb250ZW50LCBpdGVtcyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc2VhcmNoU2VydmljZS5kZWVwQ29udGFpbnMoaSwgdGhpcy5zZWFyY2hRdWVyeSkpe1xyXG4gICAgICAgICAgICAgICAgICAgICBpdGVtcy5wdXNoKGkpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbn0iXX0=