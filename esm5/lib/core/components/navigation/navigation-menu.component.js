import { __decorate } from "tslib";
import { Component, EventEmitter, HostBinding, Input, Output, HostListener } from '@angular/core';
import { Router } from '@angular/router';
var NavigationMenuComponent = /** @class */ (function () {
    function NavigationMenuComponent(router) {
        this.router = router;
        this._config = {
            showToggleButton: true,
            collapsed: true,
            sections: []
        };
        this.action = new EventEmitter();
    }
    Object.defineProperty(NavigationMenuComponent.prototype, "collapsed", {
        get: function () {
            return this.config ? this.config.collapsed : true;
        },
        enumerable: true,
        configurable: true
    });
    NavigationMenuComponent.prototype.onDocumentClick = function (target) {
        var el = target.closest('pbi-navigation-menu');
        if (el === null)
            this.dismiss();
    };
    NavigationMenuComponent.prototype.onKeyDown = function (event) {
        this.keyboardEvent = event;
    };
    NavigationMenuComponent.prototype.onKeyUp = function (event) {
        this.keyboardEvent = null;
    };
    Object.defineProperty(NavigationMenuComponent.prototype, "config", {
        get: function () {
            return this._config;
        },
        set: function (v) {
            if (v) {
                this._config = v;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuComponent.prototype, "shiftKey", {
        get: function () {
            return this.keyboardEvent && this.keyboardEvent.shiftKey;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuComponent.prototype, "ctrlKey", {
        get: function () {
            return this.keyboardEvent && this.keyboardEvent.ctrlKey;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NavigationMenuComponent.prototype, "newTab", {
        get: function () {
            return this.ctrlKey || this.shiftKey;
        },
        enumerable: true,
        configurable: true
    });
    NavigationMenuComponent.prototype.ngOnInit = function () { };
    NavigationMenuComponent.prototype.toggle = function () {
        if (!this.config) {
            return;
        }
        this.config.collapsed = !this.config.collapsed;
    };
    NavigationMenuComponent.prototype.dismiss = function () {
        this.activeSectionConfig = null;
        this.config.collapsed = true;
    };
    NavigationMenuComponent.prototype.browse = function (config, event) {
        event.stopImmediatePropagation();
        if (this.isBrowsing(config)) {
            this.activeSectionConfig = null;
            return;
        }
        if (config.content) {
            this.activeSectionConfig = config;
        }
        else {
            this.navigateTo(config, this.newTab);
        }
    };
    NavigationMenuComponent.prototype.isBrowsing = function (config) {
        return this.activeSectionConfig === config;
    };
    NavigationMenuComponent.prototype.navigateTo = function (config, newTab) {
        this.dismiss();
        newTab ? this.windowNavigateTo(config) : this.ngNavigateTo(config);
        this.keyboardEvent = null;
    };
    NavigationMenuComponent.prototype.handleAction = function (actionId) {
        this.dismiss();
        this.action.emit(actionId);
    };
    NavigationMenuComponent.prototype.windowNavigateTo = function (config) {
        var link = config.link;
        var queryParams = config.queryParams;
        if (this.isExternalLink(link))
            this.openNewBrowserTab(link, queryParams);
        else
            this.openNewBrowserTab(this.config.useHash ? "#" + link : link, queryParams);
    };
    NavigationMenuComponent.prototype.ngNavigateTo = function (config) {
        var link = config.link;
        var queryParams = config.queryParams;
        if (this.isExternalLink(link))
            this.openNewBrowserTab(link, queryParams);
        else {
            //TODO: run validation checks if ever needed.
            queryParams ? this.router.navigate([link], { queryParams: queryParams }) : this.router.navigate([link]);
        }
    };
    NavigationMenuComponent.prototype.isExternalLink = function (link) {
        return link.includes('http://') || link.includes('https://');
    };
    NavigationMenuComponent.prototype.openNewBrowserTab = function (link, queryParams) {
        var l = this.config.href ? "" + this.config.href + link : "" + link;
        if (queryParams) {
            var params = [];
            for (var property in queryParams) {
                if (!queryParams.hasOwnProperty(property)) {
                    continue;
                }
                params.push(property + "=" + queryParams[property]);
            }
            l += "?" + params.join('&');
        }
        this.shiftKey ? window.open(l) : window.open(l, '_blank');
    };
    NavigationMenuComponent.ctorParameters = function () { return [
        { type: Router }
    ]; };
    __decorate([
        HostBinding('class.collapsed')
    ], NavigationMenuComponent.prototype, "collapsed", null);
    __decorate([
        HostListener('document:click', ['$event.target'])
    ], NavigationMenuComponent.prototype, "onDocumentClick", null);
    __decorate([
        HostListener('document:keydown', ['$event'])
    ], NavigationMenuComponent.prototype, "onKeyDown", null);
    __decorate([
        HostListener('document:keyup', ['$event'])
    ], NavigationMenuComponent.prototype, "onKeyUp", null);
    __decorate([
        Input()
    ], NavigationMenuComponent.prototype, "config", null);
    __decorate([
        Output()
    ], NavigationMenuComponent.prototype, "action", void 0);
    NavigationMenuComponent = __decorate([
        Component({
            selector: 'pbi-navigation-menu',
            template: "<ng-template [ngIf]=\"config.showToggleButton\">\r\n    <div (click)=\"toggle()\" class=\"navigation-item toggle\">\r\n        <div>\r\n            <div>\r\n                <i class=\"fa fa-bars icon\"></i><span></span>\r\n            </div>\r\n            <div></div>\r\n        </div>\r\n    </div>\r\n</ng-template>\r\n<ng-template [ngIf]=\"config\">\r\n    <div *ngFor=\"let sectionConfig of config.sections\" class=\"navigation-item\">\r\n        <div [title]=\"sectionConfig.title\">\r\n            <div (click)=\"browse(sectionConfig, $event)\">\r\n                <i class=\"{{sectionConfig.icon}} icon\"></i><span class=\"text\">{{sectionConfig.title}}</span>\r\n            </div>\r\n            <div>\r\n                <div *ngIf=\"sectionConfig.openNewTab\" (click)=\"navigateTo(sectionConfig, true)\">\r\n                    <i class=\"fa fa-external-link icon\"></i>\r\n                </div>\r\n            </div>\r\n            <div>\r\n                <div *ngIf=\"sectionConfig.content\" (click)=\"browse(sectionConfig, $event)\">\r\n                    <i *ngIf=\"isBrowsing(sectionConfig); else notBrowsing\" class=\"fa fa-angle-down icon\"></i>\r\n                    <ng-template #notBrowsing>\r\n                        <i class=\"fa fa-angle-right icon\"></i>\r\n                    </ng-template>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ng-template [ngIf]=\"isBrowsing(sectionConfig)\">\r\n            <pbi-navigation-section [config]=\"sectionConfig\" (action)=\"handleAction($event)\" (navigateTo)=\"navigateTo($event, newTab)\" (navigateToNewTab)=\"navigateTo($event, true)\"></pbi-navigation-section>\r\n        </ng-template>\r\n    </div>\r\n</ng-template>"
        })
    ], NavigationMenuComponent);
    return NavigationMenuComponent;
}());
export { NavigationMenuComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1tZW51LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBVSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUcsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBUXpDO0lBK0NJLGlDQUE2QixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQTVCbkMsWUFBTyxHQUF5QjtZQUNwQyxnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLFNBQVMsRUFBRSxJQUFJO1lBQ2YsUUFBUSxFQUFFLEVBQUU7U0FDZixDQUFDO1FBU1EsV0FBTSxHQUF5QixJQUFJLFlBQVksRUFBVSxDQUFDO0lBZXJCLENBQUM7SUE1Q2hELHNCQUFJLDhDQUFTO2FBQWI7WUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDdEQsQ0FBQzs7O09BQUE7SUFFRCxpREFBZSxHQUFmLFVBQWdCLE1BQW1CO1FBQy9CLElBQUksRUFBRSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUMvQyxJQUFJLEVBQUUsS0FBSyxJQUFJO1lBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRCwyQ0FBUyxHQUFULFVBQVUsS0FBb0I7UUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVELHlDQUFPLEdBQVAsVUFBUSxLQUFvQjtRQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO0lBTUQsc0JBQUksMkNBQU07YUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDO2FBQ1EsVUFBVyxDQUF1QjtZQUN2QyxJQUFHLENBQUMsRUFBQztnQkFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQzthQUNwQjtRQUNMLENBQUM7OztPQUxBO0lBVUQsc0JBQVksNkNBQVE7YUFBcEI7WUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDN0QsQ0FBQzs7O09BQUE7SUFDRCxzQkFBWSw0Q0FBTzthQUFuQjtZQUNJLE9BQU8sSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQztRQUM1RCxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDJDQUFNO2FBQVY7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QyxDQUFDOzs7T0FBQTtJQUlNLDBDQUFRLEdBQWYsY0FBMEIsQ0FBQztJQUVwQix3Q0FBTSxHQUFiO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFBRSxPQUFPO1NBQUU7UUFDN0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztJQUNuRCxDQUFDO0lBRU0seUNBQU8sR0FBZDtRQUNJLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7UUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFFTSx3Q0FBTSxHQUFiLFVBQWMsTUFBK0IsRUFBRSxLQUFpQjtRQUM1RCxLQUFLLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNqQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDekIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztZQUNoQyxPQUFPO1NBQ1Y7UUFDRCxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7WUFDaEIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLE1BQU0sQ0FBQztTQUNyQzthQUFNO1lBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3hDO0lBQ0wsQ0FBQztJQUVNLDRDQUFVLEdBQWpCLFVBQWtCLE1BQStCO1FBQzdDLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixLQUFLLE1BQU0sQ0FBQztJQUMvQyxDQUFDO0lBRU0sNENBQVUsR0FBakIsVUFBa0IsTUFBMkIsRUFBRSxNQUFnQjtRQUMzRCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDZixNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO0lBRU0sOENBQVksR0FBbkIsVUFBb0IsUUFBZ0I7UUFDaEMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUdPLGtEQUFnQixHQUF4QixVQUF5QixNQUEyQjtRQUNoRCxJQUFNLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQU0sV0FBVyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDdkMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztZQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7O1lBQ3BFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsTUFBSSxJQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztJQUN0RixDQUFDO0lBRU8sOENBQVksR0FBcEIsVUFBcUIsTUFBMkI7UUFDNUMsSUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUN6QixJQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7WUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO2FBQ3BFO1lBQ0QsNkNBQTZDO1lBQzdDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDM0c7SUFDTCxDQUFDO0lBRU8sZ0RBQWMsR0FBdEIsVUFBdUIsSUFBWTtRQUMvQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRU8sbURBQWlCLEdBQXpCLFVBQTBCLElBQVksRUFBRSxXQUFpQjtRQUNyRCxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUcsSUFBTSxDQUFDO1FBQ3BFLElBQUksV0FBVyxFQUFFO1lBQ2IsSUFBSSxNQUFNLEdBQWEsRUFBRSxDQUFDO1lBQzFCLEtBQUssSUFBSSxRQUFRLElBQUksV0FBVyxFQUFFO2dCQUM5QixJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFBRSxTQUFTO2lCQUFFO2dCQUN4RCxNQUFNLENBQUMsSUFBSSxDQUFJLFFBQVEsU0FBSSxXQUFXLENBQUMsUUFBUSxDQUFHLENBQUMsQ0FBQzthQUN2RDtZQUNELENBQUMsSUFBSSxNQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFHLENBQUM7U0FDL0I7UUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUM5RCxDQUFDOztnQkEzRW9DLE1BQU07O0lBNUMzQztRQURDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQzs0REFHOUI7SUFFRDtRQURDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2tFQUlqRDtJQUVEO1FBREMsWUFBWSxDQUFDLGtCQUFrQixFQUFFLENBQUMsUUFBUSxDQUFDLENBQUM7NERBRzVDO0lBRUQ7UUFEQyxZQUFZLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzswREFHMUM7SUFTUTtRQUFSLEtBQUssRUFBRTt5REFJUDtJQUNTO1FBQVQsTUFBTSxFQUFFOzJEQUEyRDtJQWhDM0QsdUJBQXVCO1FBSm5DLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0IsMHNEQUErQztTQUNsRCxDQUFDO09BQ1csdUJBQXVCLENBMkhuQztJQUFELDhCQUFDO0NBQUEsQUEzSEQsSUEySEM7U0EzSFksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT3V0cHV0LCBPbkluaXQsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgTmF2aWdhdGlvbk1lbnVDb25maWcsIE5hdmlnYXRpb25TZWN0aW9uQ29uZmlnLCBOYXZpZ2FibGVJdGVtQ29uZmlnIH0gZnJvbSAnLi4vY29udHJhY3RzL25hdmlnYXRpb24nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3BiaS1uYXZpZ2F0aW9uLW1lbnUnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL25hdmlnYXRpb24tbWVudS5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5hdmlnYXRpb25NZW51Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmNvbGxhcHNlZCcpXHJcbiAgICBnZXQgY29sbGFwc2VkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNvbmZpZyA/IHRoaXMuY29uZmlnLmNvbGxhcHNlZCA6IHRydWU7XHJcbiAgICB9XHJcbiAgICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDpjbGljaycsIFsnJGV2ZW50LnRhcmdldCddKVxyXG4gICAgb25Eb2N1bWVudENsaWNrKHRhcmdldDogSFRNTEVsZW1lbnQpIHtcclxuICAgICAgICBsZXQgZWwgPSB0YXJnZXQuY2xvc2VzdCgncGJpLW5hdmlnYXRpb24tbWVudScpO1xyXG4gICAgICAgIGlmIChlbCA9PT0gbnVsbCkgdGhpcy5kaXNtaXNzKCk7XHJcbiAgICB9XHJcbiAgICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDprZXlkb3duJywgWyckZXZlbnQnXSlcclxuICAgIG9uS2V5RG93bihldmVudDogS2V5Ym9hcmRFdmVudCkge1xyXG4gICAgICAgIHRoaXMua2V5Ym9hcmRFdmVudCA9IGV2ZW50O1xyXG4gICAgfVxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5dXAnLCBbJyRldmVudCddKVxyXG4gICAgb25LZXlVcChldmVudDogS2V5Ym9hcmRFdmVudCkge1xyXG4gICAgICAgIHRoaXMua2V5Ym9hcmRFdmVudCA9IG51bGw7XHJcbiAgICB9XHJcbiAgICBwcml2YXRlIF9jb25maWc6IE5hdmlnYXRpb25NZW51Q29uZmlnID0ge1xyXG4gICAgICAgIHNob3dUb2dnbGVCdXR0b246IHRydWUsXHJcbiAgICAgICAgY29sbGFwc2VkOiB0cnVlLFxyXG4gICAgICAgIHNlY3Rpb25zOiBbXVxyXG4gICAgfTtcclxuICAgIGdldCBjb25maWcoKTogTmF2aWdhdGlvbk1lbnVDb25maWd7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbmZpZztcclxuICAgIH1cclxuICAgIEBJbnB1dCgpIHNldCBjb25maWcodjogTmF2aWdhdGlvbk1lbnVDb25maWcpe1xyXG4gICAgICAgIGlmKHYpe1xyXG4gICAgICAgICAgICB0aGlzLl9jb25maWcgPSB2O1xyXG4gICAgICAgIH1cclxuICAgIH0gXHJcbiAgICBAT3V0cHV0KCkgYWN0aW9uOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG4gICAgcHJpdmF0ZSBhY3RpdmVTZWN0aW9uQ29uZmlnOiBOYXZpZ2F0aW9uU2VjdGlvbkNvbmZpZztcclxuICAgIHByaXZhdGUga2V5Ym9hcmRFdmVudDogS2V5Ym9hcmRFdmVudDtcclxuXHJcbiAgICBwcml2YXRlIGdldCBzaGlmdEtleSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5rZXlib2FyZEV2ZW50ICYmIHRoaXMua2V5Ym9hcmRFdmVudC5zaGlmdEtleTtcclxuICAgIH1cclxuICAgIHByaXZhdGUgZ2V0IGN0cmxLZXkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMua2V5Ym9hcmRFdmVudCAmJiB0aGlzLmtleWJvYXJkRXZlbnQuY3RybEtleTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbmV3VGFiKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN0cmxLZXkgfHwgdGhpcy5zaGlmdEtleTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlYWRvbmx5IHJvdXRlcjogUm91dGVyKSB7IH1cclxuXHJcbiAgICBwdWJsaWMgbmdPbkluaXQoKTogdm9pZCB7IH1cclxuXHJcbiAgICBwdWJsaWMgdG9nZ2xlKCk6IHZvaWQge1xyXG4gICAgICAgIGlmICghdGhpcy5jb25maWcpIHsgcmV0dXJuOyB9XHJcbiAgICAgICAgdGhpcy5jb25maWcuY29sbGFwc2VkID0gIXRoaXMuY29uZmlnLmNvbGxhcHNlZDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZGlzbWlzcygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmFjdGl2ZVNlY3Rpb25Db25maWcgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuY29uZmlnLmNvbGxhcHNlZCA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGJyb3dzZShjb25maWc6IE5hdmlnYXRpb25TZWN0aW9uQ29uZmlnLCBldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIGlmICh0aGlzLmlzQnJvd3NpbmcoY29uZmlnKSkge1xyXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZVNlY3Rpb25Db25maWcgPSBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjb25maWcuY29udGVudCkge1xyXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZVNlY3Rpb25Db25maWcgPSBjb25maWc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5uYXZpZ2F0ZVRvKGNvbmZpZywgdGhpcy5uZXdUYWIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaXNCcm93c2luZyhjb25maWc6IE5hdmlnYXRpb25TZWN0aW9uQ29uZmlnKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWN0aXZlU2VjdGlvbkNvbmZpZyA9PT0gY29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBuYXZpZ2F0ZVRvKGNvbmZpZzogTmF2aWdhYmxlSXRlbUNvbmZpZywgbmV3VGFiPzogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZGlzbWlzcygpO1xyXG4gICAgICAgIG5ld1RhYiA/IHRoaXMud2luZG93TmF2aWdhdGVUbyhjb25maWcpIDogdGhpcy5uZ05hdmlnYXRlVG8oY29uZmlnKTtcclxuICAgICAgICB0aGlzLmtleWJvYXJkRXZlbnQgPSBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBoYW5kbGVBY3Rpb24oYWN0aW9uSWQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZGlzbWlzcygpO1xyXG4gICAgICAgIHRoaXMuYWN0aW9uLmVtaXQoYWN0aW9uSWQpO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBwcml2YXRlIHdpbmRvd05hdmlnYXRlVG8oY29uZmlnOiBOYXZpZ2FibGVJdGVtQ29uZmlnKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgbGluayA9IGNvbmZpZy5saW5rO1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5UGFyYW1zID0gY29uZmlnLnF1ZXJ5UGFyYW1zO1xyXG4gICAgICAgIGlmICh0aGlzLmlzRXh0ZXJuYWxMaW5rKGxpbmspKSB0aGlzLm9wZW5OZXdCcm93c2VyVGFiKGxpbmssIHF1ZXJ5UGFyYW1zKTtcclxuICAgICAgICBlbHNlIHRoaXMub3Blbk5ld0Jyb3dzZXJUYWIodGhpcy5jb25maWcudXNlSGFzaCA/IGAjJHtsaW5rfWAgOiBsaW5rLCBxdWVyeVBhcmFtcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBuZ05hdmlnYXRlVG8oY29uZmlnOiBOYXZpZ2FibGVJdGVtQ29uZmlnKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgbGluayA9IGNvbmZpZy5saW5rO1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5UGFyYW1zID0gY29uZmlnLnF1ZXJ5UGFyYW1zO1xyXG4gICAgICAgIGlmICh0aGlzLmlzRXh0ZXJuYWxMaW5rKGxpbmspKSB0aGlzLm9wZW5OZXdCcm93c2VyVGFiKGxpbmssIHF1ZXJ5UGFyYW1zKTtcclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgLy9UT0RPOiBydW4gdmFsaWRhdGlvbiBjaGVja3MgaWYgZXZlciBuZWVkZWQuXHJcbiAgICAgICAgICAgIHF1ZXJ5UGFyYW1zID8gdGhpcy5yb3V0ZXIubmF2aWdhdGUoW2xpbmtdLCB7IHF1ZXJ5UGFyYW1zOiBxdWVyeVBhcmFtcyB9KSA6IHRoaXMucm91dGVyLm5hdmlnYXRlKFtsaW5rXSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNFeHRlcm5hbExpbmsobGluazogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGxpbmsuaW5jbHVkZXMoJ2h0dHA6Ly8nKSB8fCBsaW5rLmluY2x1ZGVzKCdodHRwczovLycpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb3Blbk5ld0Jyb3dzZXJUYWIobGluazogc3RyaW5nLCBxdWVyeVBhcmFtcz86IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGxldCBsID0gdGhpcy5jb25maWcuaHJlZiA/IGAke3RoaXMuY29uZmlnLmhyZWZ9JHtsaW5rfWAgOiBgJHtsaW5rfWA7XHJcbiAgICAgICAgaWYgKHF1ZXJ5UGFyYW1zKSB7XHJcbiAgICAgICAgICAgIGxldCBwYXJhbXM6IHN0cmluZ1tdID0gW107XHJcbiAgICAgICAgICAgIGZvciAobGV0IHByb3BlcnR5IGluIHF1ZXJ5UGFyYW1zKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXF1ZXJ5UGFyYW1zLmhhc093blByb3BlcnR5KHByb3BlcnR5KSkgeyBjb250aW51ZTsgfVxyXG4gICAgICAgICAgICAgICAgcGFyYW1zLnB1c2goYCR7cHJvcGVydHl9PSR7cXVlcnlQYXJhbXNbcHJvcGVydHldfWApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGwgKz0gYD8ke3BhcmFtcy5qb2luKCcmJyl9YDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zaGlmdEtleSA/IHdpbmRvdy5vcGVuKGwpIDogd2luZG93Lm9wZW4obCwgJ19ibGFuaycpO1xyXG4gICAgfVxyXG59Il19