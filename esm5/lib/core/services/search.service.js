import { __decorate, __generator, __values } from "tslib";
import { Injectable } from '@angular/core';
var SearchService = /** @class */ (function () {
    function SearchService() {
    }
    /**
     * Returns elements of the array that contain the given string, case insensitive, anywhere in their object hierarchy.
     * @param input The array to search.
     * @param text The string to match, case insensitive.
     * @param exclude The list of properties to exclude from the search.
     */
    SearchService.prototype.deepArrayContains = function (input, text, exclude) {
        if (!text) {
            return input;
        }
        var textLower = text.toLowerCase();
        var filtered = this.deepArrayFilter(input, function (val) { return val !== null && val !== undefined && val.toString().toLowerCase().indexOf(textLower) > -1; }, exclude);
        return Array.from(filtered);
    };
    /**
     * Returns elements of the array that match the provided function.
     * @param input The array to search.
     * @param filter The filter function to apply.
     */
    SearchService.prototype.deepArrayFilter = function (input, filter, exclude) {
        var input_1, input_1_1, item, e_1_1;
        var e_1, _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 5, 6, 7]);
                    input_1 = __values(input), input_1_1 = input_1.next();
                    _b.label = 1;
                case 1:
                    if (!!input_1_1.done) return [3 /*break*/, 4];
                    item = input_1_1.value;
                    if (!this.deepMatch(item, filter, exclude)) return [3 /*break*/, 3];
                    return [4 /*yield*/, item];
                case 2:
                    _b.sent();
                    _b.label = 3;
                case 3:
                    input_1_1 = input_1.next();
                    return [3 /*break*/, 1];
                case 4: return [3 /*break*/, 7];
                case 5:
                    e_1_1 = _b.sent();
                    e_1 = { error: e_1_1 };
                    return [3 /*break*/, 7];
                case 6:
                    try {
                        if (input_1_1 && !input_1_1.done && (_a = input_1.return)) _a.call(input_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                    return [7 /*endfinally*/];
                case 7: return [2 /*return*/];
            }
        });
    };
    /**
     * Finds if any properties of the given object, or their children, match the given string, case insensitive.
     * @param obj The object to search.
     * @param text The string to match, case insensitive.
     */
    SearchService.prototype.deepContains = function (obj, text) {
        if (!text) {
            return true;
        }
        var textLower = text.toLowerCase();
        return this.deepMatch(obj, function (val) { return val !== null && val !== undefined && val.toString().toLowerCase().indexOf(textLower) > -1; });
    };
    /**
     * Finds if any properties of the given object, or their children, match the given filter.
     * @param obj The object to search.
     * @param filter The filter function to apply.
     */
    SearchService.prototype.deepMatch = function (obj, filter, exclude) {
        var e_2, _a;
        for (var prop in obj) {
            if (!obj.hasOwnProperty(prop))
                continue;
            if (exclude && exclude.includes(prop))
                continue;
            var propVal = obj[prop];
            if (Array.isArray(propVal)) {
                try {
                    for (var propVal_1 = (e_2 = void 0, __values(propVal)), propVal_1_1 = propVal_1.next(); !propVal_1_1.done; propVal_1_1 = propVal_1.next()) {
                        var item = propVal_1_1.value;
                        if (this.deepMatch(item, filter, exclude)) {
                            return true;
                        }
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (propVal_1_1 && !propVal_1_1.done && (_a = propVal_1.return)) _a.call(propVal_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            else {
                if (propVal instanceof Date) {
                    var a = propVal.toLocaleDateString();
                    var b = a.split('/').join('-');
                    if (filter(a) || filter(b)) {
                        return true;
                    }
                }
                else if (filter(propVal)) {
                    return true;
                }
            }
        }
        return false;
    };
    SearchService.prototype.treeWalk = function (arr, childPath, callback) {
        var e_3, _a;
        if (!arr) {
            return undefined;
        }
        try {
            for (var arr_1 = __values(arr), arr_1_1 = arr_1.next(); !arr_1_1.done; arr_1_1 = arr_1.next()) {
                var item = arr_1_1.value;
                callback(item);
                var children = item[childPath];
                this.treeWalk(children, childPath, callback);
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (arr_1_1 && !arr_1_1.done && (_a = arr_1.return)) _a.call(arr_1);
            }
            finally { if (e_3) throw e_3.error; }
        }
    };
    SearchService = __decorate([
        Injectable()
    ], SearchService);
    return SearchService;
}());
export { SearchService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29yZS9zZXJ2aWNlcy9zZWFyY2guc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQztJQUFBO0lBbUZBLENBQUM7SUFsRkE7Ozs7O09BS0c7SUFDSCx5Q0FBaUIsR0FBakIsVUFBcUIsS0FBVSxFQUFFLElBQVksRUFBRSxPQUFrQjtRQUNoRSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQUUsT0FBTyxLQUFLLENBQUM7U0FBRTtRQUM1QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEtBQUssSUFBSSxJQUFJLEdBQUcsS0FBSyxTQUFTLElBQUksR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBekYsQ0FBeUYsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN0SixPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7O09BSUc7SUFDRix1Q0FBZSxHQUFoQixVQUFvQixLQUFVLEVBQUUsTUFBa0MsRUFBRSxPQUFrQjs7Ozs7OztvQkFDbEUsVUFBQSxTQUFBLEtBQUssQ0FBQTs7OztvQkFBYixJQUFJO3lCQUNWLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsRUFBckMsd0JBQXFDO29CQUN4QyxxQkFBTSxJQUFJLEVBQUE7O29CQUFWLFNBQVUsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQUdiO0lBRUQ7Ozs7T0FJRztJQUNILG9DQUFZLEdBQVosVUFBZ0IsR0FBTSxFQUFFLElBQVk7UUFDbkMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1NBQUU7UUFDM0IsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLEtBQUssSUFBSSxJQUFJLEdBQUcsS0FBSyxTQUFTLElBQUksR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBekYsQ0FBeUYsQ0FBQyxDQUFDO0lBQzlILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsaUNBQVMsR0FBVCxVQUFhLEdBQU0sRUFBRSxNQUFrQyxFQUFFLE9BQWtCOztRQUMxRSxLQUFLLElBQU0sSUFBSSxJQUFJLEdBQUcsRUFBRTtZQUN2QixJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7Z0JBQUUsU0FBUztZQUN4QyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFBRSxTQUFTO1lBRWhELElBQU0sT0FBTyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMxQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7O29CQUMzQixLQUFtQixJQUFBLDJCQUFBLFNBQUEsT0FBTyxDQUFBLENBQUEsZ0NBQUEscURBQUU7d0JBQXZCLElBQU0sSUFBSSxvQkFBQTt3QkFDZCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsRUFBRTs0QkFDMUMsT0FBTyxJQUFJLENBQUM7eUJBQ1o7cUJBQ0Q7Ozs7Ozs7OzthQUNEO2lCQUNJO2dCQUVKLElBQUksT0FBTyxZQUFZLElBQUksRUFBRTtvQkFDNUIsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLGtCQUFrQixFQUFFLENBQUM7b0JBQ3JDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMvQixJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQzNCLE9BQU8sSUFBSSxDQUFDO3FCQUNaO2lCQUNEO3FCQUFNLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUMzQixPQUFPLElBQUksQ0FBQztpQkFDWjthQUNEO1NBQ0Q7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNkLENBQUM7SUFFRCxnQ0FBUSxHQUFSLFVBQVksR0FBUSxFQUFFLFNBQWtCLEVBQUUsUUFBMkI7O1FBQ3BFLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFBRSxPQUFPLFNBQVMsQ0FBQztTQUFFOztZQUUvQixLQUFpQixJQUFBLFFBQUEsU0FBQSxHQUFHLENBQUEsd0JBQUEseUNBQUU7Z0JBQWpCLElBQUksSUFBSSxnQkFBQTtnQkFDWixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRWYsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBZSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7YUFDN0M7Ozs7Ozs7OztJQUNGLENBQUM7SUFsRlcsYUFBYTtRQUR6QixVQUFVLEVBQUU7T0FDQSxhQUFhLENBbUZ6QjtJQUFELG9CQUFDO0NBQUEsQUFuRkQsSUFtRkM7U0FuRlksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFNlYXJjaFNlcnZpY2Uge1xyXG5cdC8qKlxyXG5cdCAqIFJldHVybnMgZWxlbWVudHMgb2YgdGhlIGFycmF5IHRoYXQgY29udGFpbiB0aGUgZ2l2ZW4gc3RyaW5nLCBjYXNlIGluc2Vuc2l0aXZlLCBhbnl3aGVyZSBpbiB0aGVpciBvYmplY3QgaGllcmFyY2h5LlxyXG5cdCAqIEBwYXJhbSBpbnB1dCBUaGUgYXJyYXkgdG8gc2VhcmNoLlxyXG5cdCAqIEBwYXJhbSB0ZXh0IFRoZSBzdHJpbmcgdG8gbWF0Y2gsIGNhc2UgaW5zZW5zaXRpdmUuXHJcblx0ICogQHBhcmFtIGV4Y2x1ZGUgVGhlIGxpc3Qgb2YgcHJvcGVydGllcyB0byBleGNsdWRlIGZyb20gdGhlIHNlYXJjaC5cclxuXHQgKi9cclxuXHRkZWVwQXJyYXlDb250YWluczxUPihpbnB1dDogVFtdLCB0ZXh0OiBzdHJpbmcsIGV4Y2x1ZGU/OiBzdHJpbmdbXSk6IFRbXSB7XHJcblx0XHRpZiAoIXRleHQpIHsgcmV0dXJuIGlucHV0OyB9XHJcblx0XHRsZXQgdGV4dExvd2VyID0gdGV4dC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0bGV0IGZpbHRlcmVkID0gdGhpcy5kZWVwQXJyYXlGaWx0ZXIoaW5wdXQsIHZhbCA9PiB2YWwgIT09IG51bGwgJiYgdmFsICE9PSB1bmRlZmluZWQgJiYgdmFsLnRvU3RyaW5nKCkudG9Mb3dlckNhc2UoKS5pbmRleE9mKHRleHRMb3dlcikgPiAtMSwgZXhjbHVkZSk7XHJcblx0XHRyZXR1cm4gQXJyYXkuZnJvbShmaWx0ZXJlZCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBSZXR1cm5zIGVsZW1lbnRzIG9mIHRoZSBhcnJheSB0aGF0IG1hdGNoIHRoZSBwcm92aWRlZCBmdW5jdGlvbi5cclxuXHQgKiBAcGFyYW0gaW5wdXQgVGhlIGFycmF5IHRvIHNlYXJjaC5cclxuXHQgKiBAcGFyYW0gZmlsdGVyIFRoZSBmaWx0ZXIgZnVuY3Rpb24gdG8gYXBwbHkuXHJcblx0ICovXHJcblx0KmRlZXBBcnJheUZpbHRlcjxUPihpbnB1dDogVFtdLCBmaWx0ZXI6IChwcm9wZXJ0eTogYW55KSA9PiBib29sZWFuLCBleGNsdWRlPzogc3RyaW5nW10pIHtcclxuXHRcdGZvciAoY29uc3QgaXRlbSBvZiBpbnB1dCkge1xyXG5cdFx0XHRpZiAodGhpcy5kZWVwTWF0Y2goaXRlbSwgZmlsdGVyLCBleGNsdWRlKSkge1xyXG5cdFx0XHRcdHlpZWxkIGl0ZW07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIEZpbmRzIGlmIGFueSBwcm9wZXJ0aWVzIG9mIHRoZSBnaXZlbiBvYmplY3QsIG9yIHRoZWlyIGNoaWxkcmVuLCBtYXRjaCB0aGUgZ2l2ZW4gc3RyaW5nLCBjYXNlIGluc2Vuc2l0aXZlLlxyXG5cdCAqIEBwYXJhbSBvYmogVGhlIG9iamVjdCB0byBzZWFyY2guXHJcblx0ICogQHBhcmFtIHRleHQgVGhlIHN0cmluZyB0byBtYXRjaCwgY2FzZSBpbnNlbnNpdGl2ZS5cclxuXHQgKi9cclxuXHRkZWVwQ29udGFpbnM8VD4ob2JqOiBULCB0ZXh0OiBzdHJpbmcpIHtcclxuXHRcdGlmICghdGV4dCkgeyByZXR1cm4gdHJ1ZTsgfVxyXG5cdFx0Y29uc3QgdGV4dExvd2VyID0gdGV4dC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0cmV0dXJuIHRoaXMuZGVlcE1hdGNoKG9iaiwgdmFsID0+IHZhbCAhPT0gbnVsbCAmJiB2YWwgIT09IHVuZGVmaW5lZCAmJiB2YWwudG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpLmluZGV4T2YodGV4dExvd2VyKSA+IC0xKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIEZpbmRzIGlmIGFueSBwcm9wZXJ0aWVzIG9mIHRoZSBnaXZlbiBvYmplY3QsIG9yIHRoZWlyIGNoaWxkcmVuLCBtYXRjaCB0aGUgZ2l2ZW4gZmlsdGVyLlxyXG5cdCAqIEBwYXJhbSBvYmogVGhlIG9iamVjdCB0byBzZWFyY2guXHJcblx0ICogQHBhcmFtIGZpbHRlciBUaGUgZmlsdGVyIGZ1bmN0aW9uIHRvIGFwcGx5LlxyXG5cdCAqL1xyXG5cdGRlZXBNYXRjaDxUPihvYmo6IFQsIGZpbHRlcjogKHByb3BlcnR5OiBhbnkpID0+IGJvb2xlYW4sIGV4Y2x1ZGU/OiBzdHJpbmdbXSk6IGJvb2xlYW4ge1xyXG5cdFx0Zm9yIChjb25zdCBwcm9wIGluIG9iaikge1xyXG5cdFx0XHRpZiAoIW9iai5oYXNPd25Qcm9wZXJ0eShwcm9wKSkgY29udGludWU7XHJcblx0XHRcdGlmIChleGNsdWRlICYmIGV4Y2x1ZGUuaW5jbHVkZXMocHJvcCkpIGNvbnRpbnVlO1xyXG5cclxuXHRcdFx0Y29uc3QgcHJvcFZhbCA9IG9ialtwcm9wXTtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkocHJvcFZhbCkpIHtcclxuXHRcdFx0XHRmb3IgKGNvbnN0IGl0ZW0gb2YgcHJvcFZhbCkge1xyXG5cdFx0XHRcdFx0aWYgKHRoaXMuZGVlcE1hdGNoKGl0ZW0sIGZpbHRlciwgZXhjbHVkZSkpIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdGVsc2Uge1xyXG5cclxuXHRcdFx0XHRpZiAocHJvcFZhbCBpbnN0YW5jZW9mIERhdGUpIHtcclxuXHRcdFx0XHRcdGxldCBhID0gcHJvcFZhbC50b0xvY2FsZURhdGVTdHJpbmcoKTtcclxuXHRcdFx0XHRcdGxldCBiID0gYS5zcGxpdCgnLycpLmpvaW4oJy0nKTtcclxuXHRcdFx0XHRcdGlmIChmaWx0ZXIoYSkgfHwgZmlsdGVyKGIpKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSBpZiAoZmlsdGVyKHByb3BWYWwpKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHR0cmVlV2FsazxUPihhcnI6IFRbXSwgY2hpbGRQYXRoOiBrZXlvZiBULCBjYWxsYmFjazogKGl0ZW06IFQpID0+IHZvaWQpIHtcclxuXHRcdGlmICghYXJyKSB7IHJldHVybiB1bmRlZmluZWQ7IH1cclxuXHJcblx0XHRmb3IgKGxldCBpdGVtIG9mIGFycikge1xyXG5cdFx0XHRjYWxsYmFjayhpdGVtKTtcclxuXHJcblx0XHRcdGNvbnN0IGNoaWxkcmVuID0gaXRlbVtjaGlsZFBhdGhdIGFzIGFueSBhcyBUW107XHJcblx0XHRcdHRoaXMudHJlZVdhbGsoY2hpbGRyZW4sIGNoaWxkUGF0aCwgY2FsbGJhY2spO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0=