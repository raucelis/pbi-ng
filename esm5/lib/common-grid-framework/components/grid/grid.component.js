import { __decorate } from "tslib";
import Resizable from 'devextreme/ui/resizable';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { applyFiltersToGrid } from '../../utilities/utilityFunctions';
var GridComponent = /** @class */ (function () {
    function GridComponent() {
        this.gridOptions = null;
        /* These output events provide you flexibility to handle certain scenarios where parent need to perform
        certain action(s) based on grid events which is sometime need to be delegate to parent rather than using gridOptions*/
        this.gridCellClick = new EventEmitter();
        this.gridCellPrepared = new EventEmitter();
        this.gridContentReady = new EventEmitter();
        this.gridContextMenuPreparing = new EventEmitter();
        this.gridEditingStart = new EventEmitter();
        this.gridEditorPrepared = new EventEmitter();
        this.gridEditorPreparing = new EventEmitter();
        this.gridExporting = new EventEmitter();
        this.gridInitNewRow = new EventEmitter();
        this.gridInitialized = new EventEmitter();
        this.gridReorder = new EventEmitter();
        this.gridRowClick = new EventEmitter();
        this.gridRowPrepared = new EventEmitter();
        this.gridRowRemoved = new EventEmitter();
        this.gridRowValidating = new EventEmitter();
        this.gridSelectionChanged = new EventEmitter();
        //#region PBI Grid Event Custom One
        this.childEntityCellClick = new EventEmitter();
        this.childEntityData = new EventEmitter();
        this.customButtonClick = new EventEmitter();
        this.gridChanged = new EventEmitter();
        this.userDefinedCustomButtonClick = new EventEmitter();
        //#endregion
        this.dropDownOptions = {
            resizeEnabled: true,
            width: 'auto',
            onContentReady: function (e) {
                var DOM = e.component._$content;
                var instance = Resizable.getInstance(DOM);
                instance.option('handles', 'left right');
            }
        };
    }
    GridComponent.prototype.onCellClick = function (info) {
        if (this.gridOptions.onCellClick) {
            this.gridOptions.onCellClick(info);
        }
        this.gridCellClick.emit(info);
    };
    GridComponent.prototype.onCellPrepared = function (info) {
        if (this.gridOptions.onCellPrepared) {
            this.gridOptions.onCellPrepared(info);
        }
        this.gridCellPrepared.emit(info);
    };
    GridComponent.prototype.onContentReady = function (gridObject) {
        if (this.gridOptions.onContentReady) {
            this.gridOptions.onContentReady(gridObject);
        }
        applyFiltersToGrid(this.gridOptions.gridComponentInstance, this.gridOptions.gridComponentInstance.option().filterValue);
        this.gridContentReady.emit(gridObject);
    };
    GridComponent.prototype.onContextMenuPreparing = function (info) {
        if (this.gridOptions.onContextMenuPreparing) {
            this.gridOptions.onContextMenuPreparing(info);
        }
        this.gridContextMenuPreparing.emit(info);
    };
    GridComponent.prototype.onEditingStart = function (info) {
        if (this.gridOptions.onEditingStart) {
            this.gridOptions.onEditingStart(info);
        }
        this.gridEditingStart.emit(info);
    };
    GridComponent.prototype.onEditorPrepared = function (info) {
        if (this.gridOptions.onEditorPrepared) {
            this.gridOptions.onEditorPrepared(info);
        }
        this.gridEditorPrepared.emit(info);
    };
    GridComponent.prototype.onEditorPreparing = function (info) {
        if (this.gridOptions.onEditorPreparing) {
            this.gridOptions.onEditorPreparing(info);
        }
        this.gridEditorPreparing.emit(info);
    };
    GridComponent.prototype.onExporting = function (info) {
        if (this.gridOptions.onExporting) {
            this.gridOptions.onExporting(info);
        }
        this.gridExporting.emit(info);
    };
    GridComponent.prototype.onInitNewRow = function (info) {
        if (this.gridOptions.onInitNewRow) {
            this.gridOptions.onInitNewRow(info);
        }
        this.gridInitNewRow.emit(info);
    };
    GridComponent.prototype.onInitialized = function (gridObject) {
        this.gridOptions.gridComponentInstance = gridObject.component;
        if (this.gridOptions.onInitialized) {
            this.gridOptions.onInitialized(gridObject);
        }
        this.gridInitialized.emit(gridObject);
    };
    GridComponent.prototype.onReorder = function (e) {
        if (this.gridOptions.onReorder) {
            this.gridOptions.onReorder(e);
        }
        this.gridReorder.emit(e);
    };
    GridComponent.prototype.onRowClick = function (info) {
        if (this.gridOptions.onRowClick) {
            this.gridOptions.onRowClick(info);
        }
    };
    GridComponent.prototype.onRowPrepared = function (info) {
        if (this.gridOptions.onRowPrepared) {
            this.gridOptions.onRowPrepared(info);
        }
    };
    GridComponent.prototype.onRowRemoved = function (info) {
        if (this.gridOptions.onRowRemoved) {
            this.gridOptions.onRowRemoved(info);
        }
    };
    GridComponent.prototype.onRowUpdating = function (info) {
        if (this.gridOptions.onRowUpdating) {
            this.gridOptions.onRowUpdating(info);
        }
        this.gridReorder.emit(info);
    };
    GridComponent.prototype.onRowValidating = function (info) {
        if (this.gridOptions.onRowValidating) {
            this.gridOptions.onRowValidating(info);
        }
    };
    GridComponent.prototype.onSelectionChanged = function (info) {
        if (this.gridOptions.onSelectionChanged) {
            this.gridOptions.onSelectionChanged(info);
        }
    };
    //#region Template related functions
    GridComponent.prototype.customActionButtonClick = function (evt, rowData, btnType) {
        if (this.gridOptions.onCustomButtonClick) {
            this.gridOptions.onCustomButtonClick({ evt: evt, rowData: rowData, btnType: btnType });
        }
        this.customButtonClick.emit({ evt: evt, rowData: rowData, btnType: btnType });
    };
    GridComponent.prototype.customActionButtonTemplateClick = function (evt, rowData, actionItem) {
        if (this.gridOptions.onUserDefinedCustomButtonClick) {
            this.gridOptions.onUserDefinedCustomButtonClick({ evt: evt, rowData: rowData, actionItem: actionItem });
        }
        this.userDefinedCustomButtonClick.emit({ evt: evt, rowData: rowData, actionItem: actionItem });
    };
    GridComponent.prototype.onLookupValueChanged = function (args, cellData) {
        cellData.setValue(cellData.value);
    };
    GridComponent.prototype.onLookupGridContentReady = function (args, cellData) {
        args.component.columnOption('DisplayValue', 'visible', false);
        if (args.component.option('selection.mode') === 'multiple' && cellData.value && !(cellData.value instanceof Array)) {
            var arr = [];
            arr.push(cellData.value);
            cellData.value = arr;
        }
    };
    __decorate([
        Input()
    ], GridComponent.prototype, "gridOptions", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridCellClick", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridCellPrepared", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridContentReady", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridContextMenuPreparing", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridEditingStart", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridEditorPrepared", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridEditorPreparing", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridExporting", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridInitNewRow", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridInitialized", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridReorder", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridRowClick", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridRowPrepared", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridRowRemoved", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridRowValidating", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridSelectionChanged", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "childEntityCellClick", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "childEntityData", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "customButtonClick", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "gridChanged", void 0);
    __decorate([
        Output()
    ], GridComponent.prototype, "userDefinedCustomButtonClick", void 0);
    GridComponent = __decorate([
        Component({
            selector: 'pbi-grid',
            template: "<dx-data-grid id=\"gridContainer\" class=\"{{gridOptions.selectedThemeClass}}\" [dataSource]=\"gridOptions.dataSource\"\r\n    [height]=\"gridOptions.height\" [accessKey]=\"gridOptions.accessKey\"\r\n    [activeStateEnabled]=\"gridOptions.enableActiveState\" [allowColumnReordering]=\"gridOptions.allowColumnReordering\"\r\n    [allowColumnResizing]=\"gridOptions.allowColumnResizing\"\r\n    [autoNavigateToFocusedRow]=\"gridOptions.autoNavigateToFocusedRow\" [columnAutoWidth]=\"gridOptions.columnAutoWidth\"\r\n    [cacheEnabled]=\"gridOptions.enableCache\" [cellHintEnabled]=\"gridOptions.enableCellHint\"\r\n    [columnHidingEnabled]=\"gridOptions.enableColumnHiding\" [columnMinWidth]=\"gridOptions.columnMinWidth\"\r\n    [columnResizingMode]=\"gridOptions.columnResizingMode\" [columns]=\"gridOptions.columns\"\r\n    [customizeColumns]=\"gridOptions.customizeColumns\" [dataSource]=\"gridOptions.dataSource\"\r\n    [dateSerializationFormat]=\"gridOptions.dateSerializationFormat\" [disabled]=\"gridOptions.disabled\"\r\n    [errorRowEnabled]=\"gridOptions.enableErrorRow\" [filterSyncEnabled]=\"gridOptions.filterSyncEnabled\"\r\n    [filterValue]=\"gridOptions.filterValue\" [height]=\"gridOptions.height\"\r\n    [highlightChanges]=\"gridOptions.highlightChanges\" [hoverStateEnabled]=\"gridOptions.hoverStateEnabled\"\r\n    [noDataText]=\"gridOptions.noDataText\" [repaintChangesOnly]=\"gridOptions.repaintChangesOnly\"\r\n    [remoteOperations]=\"gridOptions.remoteOperationsEnabled\" [rowAlternationEnabled]=\"gridOptions.rowAlternationEnabled\"\r\n    [showBorders]=\"gridOptions.showBorders\" [showColumnHeaders]=\"gridOptions.showColumnHeaders\"\r\n    [showColumnLines]=\"gridOptions.showColumnLines\" [showRowLines]=\"gridOptions.showRowLines\"\r\n    (onCellClick)=\"onCellClick($event)\" (onCellPrepared)=\"onCellPrepared($event)\"\r\n    (onContentReady)=\"onContentReady($event)\" (onContextMenuPreparing)=\"onContextMenuPreparing($event)\"\r\n    (onEditingStart)=\"onEditingStart($event)\" (onEditorPrepared)=\"onEditorPrepared($event)\"\r\n    (onEditorPreparing)=\"onEditorPreparing($event)\" (onExporting)=\"onExporting($event)\"\r\n    (onInitNewRow)=\"onInitNewRow($event)\" (onInitialized)=\"onInitialized($event)\" (onRowClick)=\"onRowClick($event)\"\r\n    (onRowPrepared)=\"onRowPrepared($event)\" (onRowRemoved)=\"onRowRemoved($event)\"\r\n    (onRowUpdating)=\"onRowUpdating($event)\" (onRowValidating)=\"onRowValidating($event)\"\r\n    (onSelectionChanged)=\"onSelectionChanged($event)\">\r\n\r\n    <!-- #region Start Grid Options which help to enable or disable grid features -->\r\n    <dxo-editing [mode]=\"gridOptions.editingMode\" [allowUpdating]=\"gridOptions.allowUpdating\"\r\n        [allowAdding]=\"gridOptions.allowAdding\" [allowDeleting]=\"gridOptions.allowDeleting\"\r\n        [useIcons]=\"gridOptions.useIcons\" [refreshMode]=\"gridOptions.refreshMode\"></dxo-editing>\r\n    <dxo-column-chooser [enabled]=\"gridOptions.enableColumnChooser\"></dxo-column-chooser>\r\n    <dxo-column-fixing [enabled]=\"gridOptions.enableColumnFixing\"></dxo-column-fixing>\r\n    <dxo-export [enabled]=\"gridOptions.allowDataExport\" [fileName]=\"gridOptions.gridName\"\r\n        [allowExportSelectedData]=\"gridOptions.allowSelectedDataExport\"></dxo-export>\r\n    <dxo-filter-panel [visible]=\"gridOptions.showFilterPanel\"></dxo-filter-panel>\r\n    <dxo-filter-row [visible]=\"gridOptions.showFilterRow\"></dxo-filter-row>\r\n    <dxo-group-panel [visible]=\"gridOptions.showGroupPanel\"></dxo-group-panel>\r\n    <dxo-grouping #expand [autoExpandAll]=\"gridOptions.autoExpandAll\"\r\n        [contextMenuEnabled]=\"gridOptions.enableContextGrpMenu\">\r\n    </dxo-grouping>\r\n    <dxo-load-panel [enabled]=\"gridOptions.enableLoadPanel\"></dxo-load-panel>\r\n    <dxo-header-filter [visible]=\"gridOptions.showHeaderFilter\"></dxo-header-filter>\r\n    <dxo-pager [showPageSizeSelector]=\"gridOptions.showPageSizeSelector\"\r\n        [allowedPageSizes]=\"gridOptions.allowedPageSizes\" [showInfo]=\"gridOptions.showPageInfo\"\r\n        [visible]=\"gridOptions.showPager\"></dxo-pager>\r\n    <dxo-paging [pageSize]=\"gridOptions.pageSize\" [enabled]=\"gridOptions.pagingEnabled\"></dxo-paging>\r\n    <dxo-row-dragging [allowReordering]=\"gridOptions.allowReordering\" [showDragIcons]=\"gridOptions.showDragIcons\"\r\n        [onReorder]=\"onReorder\">\r\n    </dxo-row-dragging>\r\n    <dx-scroll-view #scrollView id=\"scrollView\" [showScrollbar]=\"gridOptions.showScrollbars\"></dx-scroll-view>\r\n    <dxo-scrolling [useNative]=\"false\" [mode]=\"gridOptions.scrollMode\"\r\n        columnRenderingMode=\"gridOptions.columnRenderingMode\"></dxo-scrolling>\r\n    <dxo-sorting [mode]=\"gridOptions.sortingType\"></dxo-sorting>\r\n    <dxo-selection [mode]=\"gridOptions.selectionMode\"></dxo-selection>\r\n    <dxo-search-panel [visible]=\"gridOptions.showSearchPanel\" placeholder=\"Search...\"></dxo-search-panel>\r\n    <dxo-state-storing [enabled]=\"gridOptions.enableStateStoring\" [type]=\"gridOptions.stateStorageType\"\r\n        [storageKey]=\"gridOptions.gridName\">\r\n    </dxo-state-storing>\r\n    <!-- #endregion End Grid Options which help to enable or disable grid features -->\r\n\r\n    <!-- #region Templates for Action Columns -->\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'actionButtonTemplate'\">\r\n        <i class=\"far fa-edit\" title=\"Edit\" (click)=\"customActionButtonClick($event, rowData, 'edit')\"></i>\r\n        <i class=\"far fa-trash-alt\" title=\"Delete\" (click)=\"customActionButtonClick($event, rowData, 'delete')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'actionEditButtonTemplate'\">\r\n        <i class=\"far fa-edit\" title=\"Edit\" (click)=\"customActionButtonClick($event, rowData, 'edit')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'viewRunButtonTemplate'\">\r\n        <i class=\"far fa-eye\" title=\"View Job\" (click)=\"customActionButtonClick($event, rowData, 'view')\"></i>\r\n        <i class=\"fas fa-play\" title=\"Run Job\" (click)=\"customActionButtonClick($event, rowData, 'run')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'viewEditDeleteButtonTemplate'\">\r\n        <i class=\"far fa-eye\" title=\"View\" (click)=\"customActionButtonClick($event, rowData, 'view')\"></i>\r\n        <i class=\"far fa-edit\" title=\"Edit\" (click)=\"customActionButtonClick($event, rowData, 'edit')\"></i>\r\n        <i class=\"far fa-trash-alt\" title=\"Delete\" (click)=\"customActionButtonClick($event, rowData, 'delete')\"></i>\r\n    </div>\r\n\r\n    <div class=\"actionColumnContainer\" *dxTemplate=\"let rowData of 'customActionButtonTemplate'\">\r\n        <ng-template *ngFor=\"let item of gridOptions.contextMenuMappingList\">\r\n            <i *ngIf=\"item.ShowAsContextMenu && item.IconClassName\" attr.title=\"{{item.DisplayName}}\"\r\n                class=\"{{item.IconClassName}}\" (click)=\"customActionButtonTemplateClick($event, rowData, item)\"></i>\r\n            <span *ngIf=\"item.ShowAsContextMenu && !item.IconClassName\" class=\"customActionIconText\"\r\n                (click)=\"customActionButtonTemplateClick($event, rowData, item )\">{{item.DisplayName}}</span>\r\n        </ng-template>\r\n    </div>\r\n\r\n    <!-- #endregion Templates for Action Columns -->\r\n\r\n    <!-- #region Templates for Ref Data Controls -->\r\n    <div *dxTemplate=\"let cellData of 'ddBoxCellTemplate'\">\r\n        <dx-drop-down-box [disabled]=\"true\" [(value)]=\"cellData.value\" [dataSource]=\"cellData.column.lookup.dataSource\"\r\n            [valueExpr]=\"cellData.column.lookup.valueExpr\" [displayExpr]=\"cellData.column.lookup.displayExpr\">\r\n            <dxo-drop-down-options [height]=\"500\"></dxo-drop-down-options>\r\n        </dx-drop-down-box>\r\n    </div>\r\n\r\n    <div *dxTemplate=\"let cellData of 'ddBoxEditMultiTemplate'\">\r\n        <dx-drop-down-box [(value)]=\"cellData.value\" [dataSource]=\"cellData.column.lookup.dataSource\"\r\n            [valueExpr]=\"cellData.column.lookup.valueExpr\" [displayExpr]=\"cellData.column.lookup.displayExpr\"\r\n            (onValueChanged)=\"onLookupValueChanged($event, cellData)\" [dropDownOptions]=\"dropDownOptions\">\r\n            <dxo-drop-down-options [height]=\"500\"></dxo-drop-down-options>\r\n            <div *dxTemplate=\"let data of 'content'\">\r\n                <dx-data-grid id=\"lookupGrid\" [keyExpr]=\"cellData.column.lookup.valueExpr\" class=\"lookup-grid-height\"\r\n                    [dataSource]=\"cellData.column.lookup.dataSource\" [(selectedRowKeys)]=\"cellData.value\"\r\n                    (onContentReady)=\"onLookupGridContentReady($event, cellData)\" [allowColumnResizing]=\"true\"\r\n                    columnResizingMode=\"widget\">\r\n                    <dxo-filter-row visible=\" true\"></dxo-filter-row>\r\n                    <dxo-scrolling mode=\"infinite\">\r\n                    </dxo-scrolling>\r\n                    <dxo-selection [allowSelectAll]=\"false\" showCheckBoxesMode=\"always\" mode='multiple'>\r\n                    </dxo-selection>\r\n                    <dxo-paging [enabled]=\"false\"></dxo-paging>\r\n                </dx-data-grid>\r\n            </div>\r\n        </dx-drop-down-box>\r\n    </div>\r\n\r\n    <div *dxTemplate=\"let cellData of 'ddBoxEditSingleTemplate'\">\r\n        <dx-drop-down-box [(value)]=\"cellData.value\" [dataSource]=\"cellData.column.lookup.dataSource\"\r\n            [valueExpr]=\"cellData.column.lookup.valueExpr\" [displayExpr]=\"cellData.column.lookup.displayExpr\"\r\n            (onValueChanged)=\"onLookupValueChanged($event, cellData)\" [dropDownOptions]=\"dropDownOptions\">\r\n            <dxo-drop-down-options [height]=\"500\"></dxo-drop-down-options>\r\n            <div *dxTemplate=\"let data of 'content'\">\r\n                <dx-data-grid id=\"lookupGrid\" [keyExpr]=\"cellData.column.lookup.valueExpr\" class=\"lookup-grid-height\"\r\n                    [dataSource]=\"cellData.column.lookup.dataSource\" [(selectedRowKeys)]=\"cellData.value\"\r\n                    (onContentReady)=\"onLookupGridContentReady($event, cellData)\" [allowColumnResizing]=\"true\"\r\n                    columnResizingMode=\"widget\" [width]='cellData.column.lookup.width'>\r\n                    <dxo-filter-row visible=\" true\"></dxo-filter-row>\r\n                    <dxo-scrolling mode=\"infinite\"></dxo-scrolling>\r\n                    <dxo-selection [allowSelectAll]=\"false\" showCheckBoxesMode=\"always\" mode='single'>\r\n                    </dxo-selection>\r\n                    <dxo-paging [enabled]=\"false\"></dxo-paging>\r\n                </dx-data-grid>\r\n            </div>\r\n        </dx-drop-down-box>\r\n    </div>\r\n    <!-- #endregion Templates for Ref data Controls -->\r\n\r\n</dx-data-grid>\r\n",
            styles: [""]
        })
    ], GridComponent);
    return GridComponent;
}());
export { GridComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29tbW9uLWdyaWQtZnJhbWV3b3JrL2NvbXBvbmVudHMvZ3JpZC9ncmlkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxTQUFTLE1BQU0seUJBQXlCLENBQUM7QUFDaEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQXVCdkUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFPdEU7SUF5Q0U7UUF2Q2dCLGdCQUFXLEdBQXdCLElBQUksQ0FBQztRQUV4RDs4SEFDc0g7UUFDckcsa0JBQWEsR0FBeUMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RSxxQkFBZ0IsR0FBNEMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMvRSxxQkFBZ0IsR0FBb0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN2RSw2QkFBd0IsR0FBb0QsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMvRixxQkFBZ0IsR0FBNEMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUMvRSx1QkFBa0IsR0FBOEMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNuRix3QkFBbUIsR0FBK0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNyRixrQkFBYSxHQUF5QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3pFLG1CQUFjLEdBQTBDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0Usb0JBQWUsR0FBMkMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3RSxnQkFBVyxHQUF1QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3JFLGlCQUFZLEdBQXdDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkUsb0JBQWUsR0FBMkMsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM3RSxtQkFBYyxHQUEwQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzNFLHNCQUFpQixHQUE2QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pGLHlCQUFvQixHQUFnRCxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXhHLG1DQUFtQztRQUNsQix5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzFDLG9CQUFlLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNyQyxzQkFBaUIsR0FBcUUsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDakMsaUNBQTRCLEdBQXFFLElBQUksWUFBWSxFQUFFLENBQUM7UUFDckksWUFBWTtRQUVaLG9CQUFlLEdBQUc7WUFDaEIsYUFBYSxFQUFFLElBQUk7WUFDbkIsS0FBSyxFQUFFLE1BQU07WUFDYixjQUFjLEVBQUUsVUFBQyxDQUFDO2dCQUNoQixJQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQW9CLENBQUM7Z0JBQzdDLElBQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFjLENBQUM7Z0JBQ3pELFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFDO1lBQzNDLENBQUM7U0FDRixDQUFDO0lBRWMsQ0FBQztJQUVWLG1DQUFXLEdBQWxCLFVBQW1CLElBQTRCO1FBQzdDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEM7UUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRU0sc0NBQWMsR0FBckIsVUFBc0IsSUFBK0I7UUFDbkQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRTtZQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVNLHNDQUFjLEdBQXJCLFVBQXNCLFVBQTZCO1FBQ2pELElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUU7WUFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDN0M7UUFDRCxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsTUFBTSxFQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDeEgsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRU0sOENBQXNCLEdBQTdCLFVBQThCLElBQXVDO1FBQ25FLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsRUFBRTtZQUMzQyxJQUFJLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRU0sc0NBQWMsR0FBckIsVUFBc0IsSUFBK0I7UUFDbkQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsRUFBRTtZQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVNLHdDQUFnQixHQUF2QixVQUF3QixJQUFpQztRQUN2RCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUU7WUFDckMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6QztRQUNELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckMsQ0FBQztJQUVNLHlDQUFpQixHQUF4QixVQUF5QixJQUFrQztRQUN6RCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEVBQUU7WUFDdEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMxQztRQUNELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVNLG1DQUFXLEdBQWxCLFVBQW1CLElBQTRCO1FBQzdDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7WUFDaEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEM7UUFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNoQyxDQUFDO0lBRU0sb0NBQVksR0FBbkIsVUFBb0IsSUFBNkI7UUFDL0MsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRTtZQUNqQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNyQztRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFTSxxQ0FBYSxHQUFwQixVQUFxQixVQUFvQztRQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUM7UUFDOUQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRTtZQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUM1QztRQUNELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFTSxpQ0FBUyxHQUFoQixVQUFpQixDQUFNO1FBQ3JCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDL0I7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRU0sa0NBQVUsR0FBakIsVUFBa0IsSUFBMkI7UUFDM0MsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRTtZQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuQztJQUNILENBQUM7SUFFTSxxQ0FBYSxHQUFwQixVQUFxQixJQUE4QjtRQUNqRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQztJQUVNLG9DQUFZLEdBQW5CLFVBQW9CLElBQTZCO1FBQy9DLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUU7WUFDakMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckM7SUFDSCxDQUFDO0lBRU0scUNBQWEsR0FBcEIsVUFBcUIsSUFBMEI7UUFDN0MsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRTtZQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN0QztRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlCLENBQUM7SUFFTSx1Q0FBZSxHQUF0QixVQUF1QixJQUFnQztRQUNyRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3hDO0lBQ0gsQ0FBQztJQUVNLDBDQUFrQixHQUF6QixVQUEwQixJQUFtQztRQUMzRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUMzQztJQUNILENBQUM7SUFFRCxvQ0FBb0M7SUFDN0IsK0NBQXVCLEdBQTlCLFVBQStCLEdBQWUsRUFBRSxPQUFZLEVBQUUsT0FBZTtRQUMzRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEdBQUcsS0FBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQztTQUNqRTtRQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVNLHVEQUErQixHQUF0QyxVQUF1QyxHQUFlLEVBQUUsT0FBWSxFQUFFLFVBQWU7UUFDbkYsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLDhCQUE4QixFQUFFO1lBQ25ELElBQUksQ0FBQyxXQUFXLENBQUMsOEJBQThCLENBQUMsRUFBRSxHQUFHLEtBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxVQUFVLFlBQUEsRUFBRSxDQUFDLENBQUM7U0FDL0U7UUFDRCxJQUFJLENBQUMsNEJBQTRCLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxLQUFBLEVBQUUsT0FBTyxTQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFTSw0Q0FBb0IsR0FBM0IsVUFBNEIsSUFBcUMsRUFBRSxRQUFhO1FBQzlFLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFTSxnREFBd0IsR0FBL0IsVUFBZ0MsSUFBdUIsRUFBRSxRQUFhO1FBQ3BFLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLGNBQWMsRUFBRSxTQUFTLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDOUQsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLFVBQVUsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxZQUFZLEtBQUssQ0FBQyxFQUFFO1lBQ2xILElBQU0sR0FBRyxHQUFHLEVBQUUsQ0FBQztZQUNmLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3pCLFFBQVEsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDO1NBQ3RCO0lBQ0gsQ0FBQztJQXZMUTtRQUFSLEtBQUssRUFBRTtzREFBZ0Q7SUFJOUM7UUFBVCxNQUFNLEVBQUU7d0RBQWlGO0lBQ2hGO1FBQVQsTUFBTSxFQUFFOzJEQUF1RjtJQUN0RjtRQUFULE1BQU0sRUFBRTsyREFBK0U7SUFDOUU7UUFBVCxNQUFNLEVBQUU7bUVBQXVHO0lBQ3RHO1FBQVQsTUFBTSxFQUFFOzJEQUF1RjtJQUN0RjtRQUFULE1BQU0sRUFBRTs2REFBMkY7SUFDMUY7UUFBVCxNQUFNLEVBQUU7OERBQTZGO0lBQzVGO1FBQVQsTUFBTSxFQUFFO3dEQUFpRjtJQUNoRjtRQUFULE1BQU0sRUFBRTt5REFBbUY7SUFDbEY7UUFBVCxNQUFNLEVBQUU7MERBQXFGO0lBQ3BGO1FBQVQsTUFBTSxFQUFFO3NEQUE2RTtJQUM1RTtRQUFULE1BQU0sRUFBRTt1REFBK0U7SUFDOUU7UUFBVCxNQUFNLEVBQUU7MERBQXFGO0lBQ3BGO1FBQVQsTUFBTSxFQUFFO3lEQUFtRjtJQUNsRjtRQUFULE1BQU0sRUFBRTs0REFBeUY7SUFDeEY7UUFBVCxNQUFNLEVBQUU7K0RBQStGO0lBRzlGO1FBQVQsTUFBTSxFQUFFOytEQUFrRDtJQUNqRDtRQUFULE1BQU0sRUFBRTswREFBNkM7SUFDNUM7UUFBVCxNQUFNLEVBQUU7NERBQWlIO0lBQ2hIO1FBQVQsTUFBTSxFQUFFO3NEQUF5QztJQUN4QztRQUFULE1BQU0sRUFBRTt1RUFBNEg7SUE1QjFILGFBQWE7UUFMekIsU0FBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFVBQVU7WUFDcEIsd3dWQUFvQzs7U0FFckMsQ0FBQztPQUNXLGFBQWEsQ0FpTXpCO0lBQUQsb0JBQUM7Q0FBQSxBQWpNRCxJQWlNQztTQWpNWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlc2l6YWJsZSBmcm9tICdkZXZleHRyZW1lL3VpL3Jlc2l6YWJsZSc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBCSUdyaWRPcHRpb25zTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvZ3JpZE9wdGlvbnMubW9kZWwnO1xyXG5pbXBvcnQge1xyXG4gIFBCSUdyaWRFdmVudHMsXHJcbiAgR3JpZEJhc2VBcmd1bWVudHMsXHJcbiAgR3JpZENlbGxDbGlja0FyZ3VtZW50cyxcclxuICBHcmlkQ2VsbFByZXBhcmVkQXJndW1lbnRzLFxyXG4gIEdyaWRDb250ZXh0TWVudVByZXBhcmluZ0FyZ3VtZW50cyxcclxuICBHcmlkRWRpdGluZ1N0YXJ0QXJndW1lbnRzLFxyXG4gIEdyaWRFZGl0b3JQcmVwYXJlZEFyZ3VtZW50cyxcclxuICBHcmlkRWRpdG9yUHJlcGFyaW5nQXJndW1lbnRzLFxyXG4gIEdyaWRFeHBvcnRpbmdBcmd1bWVudHMsXHJcbiAgR3JpZEluaXROZXdSb3dBcmd1bWVudHMsXHJcbiAgR3JpZEluaXRpYWxpemVkQXJndW1lbnRzLFxyXG4gIEdyaWRSZW9yZGVyQXJndW1lbnRzLFxyXG4gIEdyaWRSb3dDbGlja0FyZ3VtZW50cyxcclxuICBHcmlkUm93UHJlcGFyZWRBcmd1bWVudHMsXHJcbiAgR3JpZFJvd1JlbW92ZWRBcmd1bWVudHMsXHJcbiAgR3JpZFJvd1ZhbGlkYXRpbmdBcmd1bWVudHMsXHJcbiAgR3JpZFNlbGVjdGlvbkNoYW5nZWRBcmd1bWVudHMsXHJcbn0gZnJvbSAnLi4vY29udHJhY3RzL2dyaWQnO1xyXG5pbXBvcnQgZHhEYXRhR3JpZCBmcm9tICdkZXZleHRyZW1lL3VpL2RhdGFfZ3JpZCc7XHJcbmltcG9ydCB7IERyb3Bkb3duQm94VmFsdWVDaGFuZ2VBcmd1bWVudHMgfSBmcm9tICcuLi9jb250cmFjdHMvZGV2ZXh0cmVtZVdpZGdldHMnO1xyXG5pbXBvcnQgeyBhcHBseUZpbHRlcnNUb0dyaWQgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvdXRpbGl0eUZ1bmN0aW9ucyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS1ncmlkJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZ3JpZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZ3JpZC5jb21wb25lbnQuY3NzJ10sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBHcmlkQ29tcG9uZW50IGltcGxlbWVudHMgUEJJR3JpZEV2ZW50cyB7XHJcblxyXG4gIEBJbnB1dCgpIHB1YmxpYyBncmlkT3B0aW9uczogUEJJR3JpZE9wdGlvbnNNb2RlbCA9IG51bGw7XHJcblxyXG4gIC8qIFRoZXNlIG91dHB1dCBldmVudHMgcHJvdmlkZSB5b3UgZmxleGliaWxpdHkgdG8gaGFuZGxlIGNlcnRhaW4gc2NlbmFyaW9zIHdoZXJlIHBhcmVudCBuZWVkIHRvIHBlcmZvcm1cclxuICBjZXJ0YWluIGFjdGlvbihzKSBiYXNlZCBvbiBncmlkIGV2ZW50cyB3aGljaCBpcyBzb21ldGltZSBuZWVkIHRvIGJlIGRlbGVnYXRlIHRvIHBhcmVudCByYXRoZXIgdGhhbiB1c2luZyBncmlkT3B0aW9ucyovXHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkQ2VsbENsaWNrOiBFdmVudEVtaXR0ZXI8R3JpZENlbGxDbGlja0FyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkQ2VsbFByZXBhcmVkOiBFdmVudEVtaXR0ZXI8R3JpZENlbGxQcmVwYXJlZEFyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkQ29udGVudFJlYWR5OiBFdmVudEVtaXR0ZXI8R3JpZEJhc2VBcmd1bWVudHM+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgZ3JpZENvbnRleHRNZW51UHJlcGFyaW5nOiBFdmVudEVtaXR0ZXI8R3JpZENvbnRleHRNZW51UHJlcGFyaW5nQXJndW1lbnRzPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGdyaWRFZGl0aW5nU3RhcnQ6IEV2ZW50RW1pdHRlcjxHcmlkRWRpdGluZ1N0YXJ0QXJndW1lbnRzPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGdyaWRFZGl0b3JQcmVwYXJlZDogRXZlbnRFbWl0dGVyPEdyaWRFZGl0b3JQcmVwYXJlZEFyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkRWRpdG9yUHJlcGFyaW5nOiBFdmVudEVtaXR0ZXI8R3JpZEVkaXRvclByZXBhcmluZ0FyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkRXhwb3J0aW5nOiBFdmVudEVtaXR0ZXI8R3JpZEV4cG9ydGluZ0FyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkSW5pdE5ld1JvdzogRXZlbnRFbWl0dGVyPEdyaWRJbml0TmV3Um93QXJndW1lbnRzPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGdyaWRJbml0aWFsaXplZDogRXZlbnRFbWl0dGVyPEdyaWRJbml0aWFsaXplZEFyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkUmVvcmRlcjogRXZlbnRFbWl0dGVyPEdyaWRSZW9yZGVyQXJndW1lbnRzPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGdyaWRSb3dDbGljazogRXZlbnRFbWl0dGVyPEdyaWRSb3dDbGlja0FyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkUm93UHJlcGFyZWQ6IEV2ZW50RW1pdHRlcjxHcmlkUm93UHJlcGFyZWRBcmd1bWVudHM+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgZ3JpZFJvd1JlbW92ZWQ6IEV2ZW50RW1pdHRlcjxHcmlkUm93UmVtb3ZlZEFyZ3VtZW50cz4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBncmlkUm93VmFsaWRhdGluZzogRXZlbnRFbWl0dGVyPEdyaWRSb3dWYWxpZGF0aW5nQXJndW1lbnRzPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGdyaWRTZWxlY3Rpb25DaGFuZ2VkOiBFdmVudEVtaXR0ZXI8R3JpZFNlbGVjdGlvbkNoYW5nZWRBcmd1bWVudHM+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAvLyNyZWdpb24gUEJJIEdyaWQgRXZlbnQgQ3VzdG9tIE9uZVxyXG4gIEBPdXRwdXQoKSBwdWJsaWMgY2hpbGRFbnRpdHlDZWxsQ2xpY2sgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBjaGlsZEVudGl0eURhdGEgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBjdXN0b21CdXR0b25DbGljazogRXZlbnRFbWl0dGVyPHsgZXZ0OiBNb3VzZUV2ZW50LCByb3dEYXRhOiBhbnksIGJ0blR5cGU6IHN0cmluZyB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGdyaWRDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgdXNlckRlZmluZWRDdXN0b21CdXR0b25DbGljazogRXZlbnRFbWl0dGVyPHsgZXZ0OiBNb3VzZUV2ZW50LCByb3dEYXRhOiBhbnksIGFjdGlvbkl0ZW06IGFueSB9PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICAvLyNlbmRyZWdpb25cclxuXHJcbiAgZHJvcERvd25PcHRpb25zID0ge1xyXG4gICAgcmVzaXplRW5hYmxlZDogdHJ1ZSxcclxuICAgIHdpZHRoOiAnYXV0bycsXHJcbiAgICBvbkNvbnRlbnRSZWFkeTogKGUpID0+IHtcclxuICAgICAgY29uc3QgRE9NID0gZS5jb21wb25lbnQuXyRjb250ZW50IGFzIEVsZW1lbnQ7XHJcbiAgICAgIGNvbnN0IGluc3RhbmNlID0gUmVzaXphYmxlLmdldEluc3RhbmNlKERPTSkgYXMgUmVzaXphYmxlO1xyXG4gICAgICBpbnN0YW5jZS5vcHRpb24oJ2hhbmRsZXMnLCAnbGVmdCByaWdodCcpO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHB1YmxpYyBvbkNlbGxDbGljayhpbmZvOiBHcmlkQ2VsbENsaWNrQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vbkNlbGxDbGljaykge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uQ2VsbENsaWNrKGluZm8pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5ncmlkQ2VsbENsaWNrLmVtaXQoaW5mbyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25DZWxsUHJlcGFyZWQoaW5mbzogR3JpZENlbGxQcmVwYXJlZEFyZ3VtZW50cyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMub25DZWxsUHJlcGFyZWQpIHtcclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucy5vbkNlbGxQcmVwYXJlZChpbmZvKTtcclxuICAgIH1cclxuICAgIHRoaXMuZ3JpZENlbGxQcmVwYXJlZC5lbWl0KGluZm8pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uQ29udGVudFJlYWR5KGdyaWRPYmplY3Q6IEdyaWRCYXNlQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vbkNvbnRlbnRSZWFkeSkge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uQ29udGVudFJlYWR5KGdyaWRPYmplY3QpO1xyXG4gICAgfVxyXG4gICAgYXBwbHlGaWx0ZXJzVG9HcmlkKHRoaXMuZ3JpZE9wdGlvbnMuZ3JpZENvbXBvbmVudEluc3RhbmNlLCB0aGlzLmdyaWRPcHRpb25zLmdyaWRDb21wb25lbnRJbnN0YW5jZS5vcHRpb24oKS5maWx0ZXJWYWx1ZSk7XHJcbiAgICB0aGlzLmdyaWRDb250ZW50UmVhZHkuZW1pdChncmlkT2JqZWN0KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkNvbnRleHRNZW51UHJlcGFyaW5nKGluZm86IEdyaWRDb250ZXh0TWVudVByZXBhcmluZ0FyZ3VtZW50cyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMub25Db250ZXh0TWVudVByZXBhcmluZykge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uQ29udGV4dE1lbnVQcmVwYXJpbmcoaW5mbyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdyaWRDb250ZXh0TWVudVByZXBhcmluZy5lbWl0KGluZm8pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRWRpdGluZ1N0YXJ0KGluZm86IEdyaWRFZGl0aW5nU3RhcnRBcmd1bWVudHMpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLm9uRWRpdGluZ1N0YXJ0KSB7XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMub25FZGl0aW5nU3RhcnQoaW5mbyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdyaWRFZGl0aW5nU3RhcnQuZW1pdChpbmZvKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkVkaXRvclByZXBhcmVkKGluZm86IEdyaWRFZGl0b3JQcmVwYXJlZEFyZ3VtZW50cyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMub25FZGl0b3JQcmVwYXJlZCkge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uRWRpdG9yUHJlcGFyZWQoaW5mbyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdyaWRFZGl0b3JQcmVwYXJlZC5lbWl0KGluZm8pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRWRpdG9yUHJlcGFyaW5nKGluZm86IEdyaWRFZGl0b3JQcmVwYXJpbmdBcmd1bWVudHMpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmdyaWRPcHRpb25zLm9uRWRpdG9yUHJlcGFyaW5nKSB7XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMub25FZGl0b3JQcmVwYXJpbmcoaW5mbyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdyaWRFZGl0b3JQcmVwYXJpbmcuZW1pdChpbmZvKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkV4cG9ydGluZyhpbmZvOiBHcmlkRXhwb3J0aW5nQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vbkV4cG9ydGluZykge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uRXhwb3J0aW5nKGluZm8pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5ncmlkRXhwb3J0aW5nLmVtaXQoaW5mbyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Jbml0TmV3Um93KGluZm86IEdyaWRJbml0TmV3Um93QXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vbkluaXROZXdSb3cpIHtcclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucy5vbkluaXROZXdSb3coaW5mbyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdyaWRJbml0TmV3Um93LmVtaXQoaW5mbyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Jbml0aWFsaXplZChncmlkT2JqZWN0OiBHcmlkSW5pdGlhbGl6ZWRBcmd1bWVudHMpIHtcclxuICAgIHRoaXMuZ3JpZE9wdGlvbnMuZ3JpZENvbXBvbmVudEluc3RhbmNlID0gZ3JpZE9iamVjdC5jb21wb25lbnQ7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vbkluaXRpYWxpemVkKSB7XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMub25Jbml0aWFsaXplZChncmlkT2JqZWN0KTtcclxuICAgIH1cclxuICAgIHRoaXMuZ3JpZEluaXRpYWxpemVkLmVtaXQoZ3JpZE9iamVjdCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25SZW9yZGVyKGU6IGFueSk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMub25SZW9yZGVyKSB7XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMub25SZW9yZGVyKGUpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5ncmlkUmVvcmRlci5lbWl0KGUpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uUm93Q2xpY2soaW5mbzogR3JpZFJvd0NsaWNrQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vblJvd0NsaWNrKSB7XHJcbiAgICAgIHRoaXMuZ3JpZE9wdGlvbnMub25Sb3dDbGljayhpbmZvKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvblJvd1ByZXBhcmVkKGluZm86IEdyaWRSb3dQcmVwYXJlZEFyZ3VtZW50cyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMub25Sb3dQcmVwYXJlZCkge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uUm93UHJlcGFyZWQoaW5mbyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Sb3dSZW1vdmVkKGluZm86IEdyaWRSb3dSZW1vdmVkQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vblJvd1JlbW92ZWQpIHtcclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucy5vblJvd1JlbW92ZWQoaW5mbyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Sb3dVcGRhdGluZyhpbmZvOiBHcmlkUmVvcmRlckFyZ3VtZW50cyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMub25Sb3dVcGRhdGluZykge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uUm93VXBkYXRpbmcoaW5mbyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmdyaWRSZW9yZGVyLmVtaXQoaW5mbyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Sb3dWYWxpZGF0aW5nKGluZm86IEdyaWRSb3dWYWxpZGF0aW5nQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vblJvd1ZhbGlkYXRpbmcpIHtcclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucy5vblJvd1ZhbGlkYXRpbmcoaW5mbyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25TZWxlY3Rpb25DaGFuZ2VkKGluZm86IEdyaWRTZWxlY3Rpb25DaGFuZ2VkQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vblNlbGVjdGlvbkNoYW5nZWQpIHtcclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucy5vblNlbGVjdGlvbkNoYW5nZWQoaW5mbyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyNyZWdpb24gVGVtcGxhdGUgcmVsYXRlZCBmdW5jdGlvbnNcclxuICBwdWJsaWMgY3VzdG9tQWN0aW9uQnV0dG9uQ2xpY2soZXZ0OiBNb3VzZUV2ZW50LCByb3dEYXRhOiBhbnksIGJ0blR5cGU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZ3JpZE9wdGlvbnMub25DdXN0b21CdXR0b25DbGljaykge1xyXG4gICAgICB0aGlzLmdyaWRPcHRpb25zLm9uQ3VzdG9tQnV0dG9uQ2xpY2soeyBldnQsIHJvd0RhdGEsIGJ0blR5cGUgfSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmN1c3RvbUJ1dHRvbkNsaWNrLmVtaXQoeyBldnQsIHJvd0RhdGEsIGJ0blR5cGUgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY3VzdG9tQWN0aW9uQnV0dG9uVGVtcGxhdGVDbGljayhldnQ6IE1vdXNlRXZlbnQsIHJvd0RhdGE6IGFueSwgYWN0aW9uSXRlbTogYW55KTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5ncmlkT3B0aW9ucy5vblVzZXJEZWZpbmVkQ3VzdG9tQnV0dG9uQ2xpY2spIHtcclxuICAgICAgdGhpcy5ncmlkT3B0aW9ucy5vblVzZXJEZWZpbmVkQ3VzdG9tQnV0dG9uQ2xpY2soeyBldnQsIHJvd0RhdGEsIGFjdGlvbkl0ZW0gfSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnVzZXJEZWZpbmVkQ3VzdG9tQnV0dG9uQ2xpY2suZW1pdCh7IGV2dCwgcm93RGF0YSwgYWN0aW9uSXRlbSB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkxvb2t1cFZhbHVlQ2hhbmdlZChhcmdzOiBEcm9wZG93bkJveFZhbHVlQ2hhbmdlQXJndW1lbnRzLCBjZWxsRGF0YTogYW55KTogdm9pZCB7XHJcbiAgICBjZWxsRGF0YS5zZXRWYWx1ZShjZWxsRGF0YS52YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Mb29rdXBHcmlkQ29udGVudFJlYWR5KGFyZ3M6IEdyaWRCYXNlQXJndW1lbnRzLCBjZWxsRGF0YTogYW55KTogdm9pZCB7XHJcbiAgICBhcmdzLmNvbXBvbmVudC5jb2x1bW5PcHRpb24oJ0Rpc3BsYXlWYWx1ZScsICd2aXNpYmxlJywgZmFsc2UpO1xyXG4gICAgaWYgKGFyZ3MuY29tcG9uZW50Lm9wdGlvbignc2VsZWN0aW9uLm1vZGUnKSA9PT0gJ211bHRpcGxlJyAmJiBjZWxsRGF0YS52YWx1ZSAmJiAhKGNlbGxEYXRhLnZhbHVlIGluc3RhbmNlb2YgQXJyYXkpKSB7XHJcbiAgICAgIGNvbnN0IGFyciA9IFtdO1xyXG4gICAgICBhcnIucHVzaChjZWxsRGF0YS52YWx1ZSk7XHJcbiAgICAgIGNlbGxEYXRhLnZhbHVlID0gYXJyO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8jZW5kcmVnaW9uXHJcblxyXG4gIC8vI3JlZ2lvbiAgUHJpdmF0ZSBGdW5jdGlvbnNcclxuXHJcbiAgLy8jZW5kcmVnaW9uICBQcml2YXRlIEZ1bmN0aW9uc1xyXG5cclxufVxyXG4iXX0=