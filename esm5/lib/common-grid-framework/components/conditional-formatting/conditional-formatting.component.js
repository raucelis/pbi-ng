import { __assign, __decorate } from "tslib";
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { CGFStorageKeys } from '../../utilities/enums';
import DataSource from 'devextreme/data/data_source';
import { ColorFormatComponent } from '../color-format/color-format.component';
import { appToast } from '../../utilities/utilityFunctions';
var ConditionalFormattingComponent = /** @class */ (function () {
    function ConditionalFormattingComponent() {
        this.groupOperations = ['and', 'or'];
        this.title = 'CONDITIONAL FORMATTING';
        this.valueChange = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this._listOfColumns = [];
        this._storageKey = CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
        this.applyType = 'Column';
        this.conditionFormattingControlVisible = false;
        this.copyOfListItems = [];
        this.filterApplyTypeCollection = ['Column', 'Row'];
        this.filterValue = null;
        this.hideColumnDropdown = true;
        this.listOfConditionalFormatting = [];
        this.operationOnColumn = '';
        this.selectedColumnData = null;
        this.selectedCondition = [];
    }
    Object.defineProperty(ConditionalFormattingComponent.prototype, "listOfColumns", {
        get: function () { return this.copyOfListItems; },
        set: function (colInfo) {
            var _this = this;
            this._listOfColumns = [];
            this.copyOfListItems = colInfo;
            colInfo.forEach(function (item) { _this._listOfColumns.push({ dataField: item.dataField, caption: item.caption, dataType: item.dataType }); });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConditionalFormattingComponent.prototype, "storageKey", {
        set: function (key) {
            this._storageKey = key || CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
            this.ngOnInit();
        },
        enumerable: true,
        configurable: true
    });
    ConditionalFormattingComponent.prototype.ngOnInit = function () {
        this.hideFormattingInfo();
        this.listOfConditionalFormatting = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
    };
    ConditionalFormattingComponent.prototype.ngOnDestroy = function () {
        this.listOfColumns.forEach(function (item) { return item.lookup = null; });
    };
    ConditionalFormattingComponent.prototype.addConditionalFormatting = function () {
        this.resetInfo();
        this.showConditionalFormatting();
    };
    ConditionalFormattingComponent.prototype.editConditionalFormatting = function () {
        var _a;
        if ((_a = this.selectedCondition) === null || _a === void 0 ? void 0 : _a.length) {
            this.showConditionalFormatting();
        }
    };
    ConditionalFormattingComponent.prototype.hideFormattingInfo = function () {
        this.conditionFormattingControlVisible = false;
        this.resetInfo();
    };
    ConditionalFormattingComponent.prototype.updateConditionalFormatting = function () {
        var _this = this;
        if (this.operationOnColumn.trim() === '' && this.applyType.toLowerCase() === 'column') {
            appToast({ type: 'warning', message: "Please select 'Apply To Column'." });
            return;
        }
        // this._dataSourceFilterValue = this.formatValue(this.filterValue);
        var currentFormatting = this.columnFormatComponent.formatObject;
        var existingFormattingInfo = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
        if (existingFormattingInfo.length && this.selectedCondition.length) { // added this condition which informs whether adding or editing condition
            for (var index = 0; index < existingFormattingInfo.length; index++) {
                if (existingFormattingInfo[index].formatRuleName === this.selectedCondition[0].formatRuleName) {
                    existingFormattingInfo[index] = currentFormatting;
                    existingFormattingInfo[index].condition = this.filterValue;
                    existingFormattingInfo[index].dataField = this.applyType.toLowerCase() === 'column' ? this.operationOnColumn : '___dataFieldForRow___'; // Adding dummy dataField for rowType
                    existingFormattingInfo[index].applyType = this.applyType.toLowerCase();
                    existingFormattingInfo[index].formatRuleName = this.getFormatName(this.title, index + 1);
                    break;
                }
            }
        }
        else {
            currentFormatting.condition = this.filterValue;
            currentFormatting.dataField = this.applyType.toLowerCase() === 'column' ? this.operationOnColumn : '___dataFieldForRow___'; // Adding dummy dataField for rowType
            currentFormatting.formatRuleName = this.getFormatName(this.title);
            currentFormatting.applyType = this.applyType.toLowerCase();
            existingFormattingInfo.push(currentFormatting);
        }
        sessionStorage.setItem(this._storageKey, JSON.stringify(existingFormattingInfo));
        this.prepareListOfConditions();
        this.hideFormattingInfo();
        setTimeout(function () { _this.forceToApplyFormats(); }, 100); // will update list on UI then forced refresh.
    };
    ConditionalFormattingComponent.prototype.forceToApplyFormats = function () {
        this.valueChange.emit({ data: JSON.parse(sessionStorage.getItem(this._storageKey)) });
    };
    ConditionalFormattingComponent.prototype.onConditionalItemDeleted = function (e) {
        var _this = this;
        var _existingFormats = JSON.parse(sessionStorage.getItem(this._storageKey));
        var deletedItemIndex = _existingFormats.findIndex(function (item) { return item.formatRuleName === e.itemData.formatRuleName && item.dataField === e.itemData.dataField; });
        if (deletedItemIndex > -1) {
            if (deletedItemIndex + 1 < _existingFormats.length) {
                _existingFormats.forEach(function (arrItem, index) {
                    if (index > deletedItemIndex) {
                        arrItem.formatRuleName = _this.getFormatName(_this.title, index, arrItem.applyType, arrItem.dataField);
                    }
                });
            }
            _existingFormats.splice(deletedItemIndex, 1);
            sessionStorage.setItem(this._storageKey, JSON.stringify(_existingFormats));
            this.prepareListOfConditions();
            this.hideFormattingInfo();
            setTimeout(function () { _this.forceToApplyFormats(); }, 100); // will update list on UI then forced refresh.
        }
    };
    ConditionalFormattingComponent.prototype.showConditionalFormatting = function () {
        var _a, _b, _c, _d;
        this.operationOnColumn = ((_a = this.selectedCondition[0]) === null || _a === void 0 ? void 0 : _a.dataField) || this.operationOnColumn;
        this.conditionFormattingControlVisible = true;
        this.filterValue = (_b = this.selectedCondition[0]) === null || _b === void 0 ? void 0 : _b.condition;
        this.selectedColumnData = this.selectedCondition[0] ? __assign({}, this.selectedCondition[0]) : __assign({
            dataField: '__null__',
            backgroundColor: '',
            condition: '',
            cssClass: ((_d = (_c = this.selectedColumnData) === null || _c === void 0 ? void 0 : _c.cssClass) === null || _d === void 0 ? void 0 : _d.replace('bold', '').replace('underline', '').replace('italic', '')) || '',
            formatRuleName: '',
            textColor: '',
        });
        if (this.selectedColumnData.applyType) {
            this.applyType = this.selectedColumnData.applyType.charAt(0).toUpperCase() + this.selectedColumnData.applyType.slice(1);
        }
        else {
            this.applyType = this.filterApplyTypeCollection[0]; // Setting column
        }
        this.onApplyTypeChange();
    };
    ConditionalFormattingComponent.prototype.prepareListOfConditions = function () {
        var conditionalFormats = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
        this.listOfConditionalFormatting = conditionalFormats || [];
    };
    ConditionalFormattingComponent.prototype.onFormatSelectionChanged = function () { this.editConditionalFormatting(); };
    ConditionalFormattingComponent.prototype.resetInfo = function () {
        this.selectedCondition = [];
        this.selectedColumnData = null;
        this.operationOnColumn = '';
        this.hideColumnDropdown = true;
    };
    ConditionalFormattingComponent.prototype.onApplyTypeChange = function () {
        this.hideColumnDropdown = this.applyType.toLowerCase() === 'column';
        if (!this.hideColumnDropdown) {
            this.operationOnColumn = '';
        }
    };
    ConditionalFormattingComponent.prototype.getFormatName = function (entityName, counter, selectedApplyType, selectedOperationOnColumn) {
        if (counter === void 0) { counter = this.listOfConditionalFormatting.length + 1; }
        if (selectedApplyType === void 0) { selectedApplyType = this.applyType; }
        if (selectedOperationOnColumn === void 0) { selectedOperationOnColumn = this.operationOnColumn; }
        selectedApplyType = selectedApplyType.toLowerCase() === 'row' ? selectedApplyType : selectedOperationOnColumn;
        return "" + (entityName.length ? entityName + ': ' : '') + selectedApplyType + " Format Rule " + counter;
    };
    ConditionalFormattingComponent.prototype.onEditorPreparing = function (e) {
        var _a;
        // Dynamic control change based on the operation to be done.
        var filterLookUp = ((_a = this.listOfColumns.filter(function (item) { return item.dataField === e.dataField; })[0]) === null || _a === void 0 ? void 0 : _a.lookup) || null;
        if (e.filterOperation === '=' && filterLookUp) {
            e.editorName = 'dxSelectBox';
            e.editorOptions.searchEnabled = true;
            if (filterLookUp) {
                e.editorOptions.dataSource = new DataSource({ store: filterLookUp.dataSource });
            }
            e.editorOptions.onValueChanged = function (event) {
                e.setValue(event.value);
            };
        }
    };
    ConditionalFormattingComponent.prototype.closeFlyOut = function () {
        this.closeCurrentFlyOut.emit();
    };
    __decorate([
        ViewChild(ColorFormatComponent)
    ], ConditionalFormattingComponent.prototype, "columnFormatComponent", void 0);
    __decorate([
        Input()
    ], ConditionalFormattingComponent.prototype, "listOfColumns", null);
    __decorate([
        Input()
    ], ConditionalFormattingComponent.prototype, "groupOperations", void 0);
    __decorate([
        Input()
    ], ConditionalFormattingComponent.prototype, "storageKey", null);
    __decorate([
        Input()
    ], ConditionalFormattingComponent.prototype, "title", void 0);
    __decorate([
        Output()
    ], ConditionalFormattingComponent.prototype, "valueChange", void 0);
    __decorate([
        Output()
    ], ConditionalFormattingComponent.prototype, "closeCurrentFlyOut", void 0);
    ConditionalFormattingComponent = __decorate([
        Component({
            selector: 'pbi-conditional-formatting',
            template: "<div class=\"conditional-formatting-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-tools title-icon\"></i>\r\n            <div class=\"section-title\">{{title}}</div>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"widget-container accordion-data-container\">\r\n        <span class=\"add-icon-container\">\r\n            <i class=\"fas fa-plus fly-out-round-add-icon pointer\" (click)=\"addConditionalFormatting()\"></i>\r\n            <div class=\"add-rule-text\">Add Rules</div>\r\n        </span>\r\n        <span class=\"rule-list-container\" [class.dynamic-rule-list]=\"conditionFormattingControlVisible\">\r\n            <dx-list [items]=\"listOfConditionalFormatting\" allowItemDeleting=\"true\" itemDeleteMode=\"toggle\"\r\n                selectionMode=\"single\" keyExpr=\"formatRuleName\" displayExpr=\"formatRuleName\" searchExpr=\"formatRuleName\"\r\n                searchMode=\"contains\" [(selectedItems)]=\"selectedCondition\" [searchEnabled]=\"false\"\r\n                (onItemDeleted)=\"onConditionalItemDeleted($event)\" (onSelectionChanged)=\"onFormatSelectionChanged()\">\r\n                <!-- <dxo-item-dragging [data]=\"listOfConditionalFormatting\" [allowReordering]=\"true\" [onDragStart]=\"onDragStart\"\r\n                    [onDragEnd]=\"onDragEnd\"></dxo-item-dragging> -->\r\n                <div *dxTemplate=\"let data of 'item'\">\r\n                    <div class=\"list-text-wrap\">{{data.formatRuleName}}</div>\r\n                </div>\r\n            </dx-list>\r\n        </span>\r\n        <div *ngIf=\"conditionFormattingControlVisible\">\r\n            <div class=\"apply-format\">\r\n                <span class=\"action-container\">\r\n                    <!-- <i class=\"pointer\" (click)=\"updateConditionalFormatting()\"> Save </i>\r\n                <i class=\"fas fa-times pointer\" flow=\"down\" data-tooltip=\"Close\" (click)=\"hideFormattingInfo()\"></i> -->\r\n                </span>\r\n                <div class=\"applyTypeContainer\">\r\n                    <span class=\"label-padding\">Apply Type(Row/Column)</span>\r\n                    <dx-radio-group class=\"radioCollection\" [items]=\"filterApplyTypeCollection\" [(value)]=\"applyType\"\r\n                        layout=\"horizontal\" (onValueChanged)=\"onApplyTypeChange()\">\r\n                    </dx-radio-group>\r\n                </div>\r\n                <span *ngIf=\"hideColumnDropdown\">\r\n                    <div class=\"label-padding\">Apply To Column </div>\r\n                    <div class=\"apply-column\">\r\n                        <dx-select-box class=\"input-element column-list\" [dataSource]=\"_listOfColumns\"\r\n                            valueExpr=\"dataField\" displayExpr=\"caption\" [(value)]=\"operationOnColumn\"\r\n                            searchMode=\"contains\" searchEnabled=\"true\">\r\n                        </dx-select-box>\r\n                    </div>\r\n                </span>\r\n                <div class=\"conditional-filter\">\r\n                    <dx-filter-builder class=\"filter-container\" [disabled]=\"!conditionFormattingControlVisible\"\r\n                        [fields]=\"_listOfColumns\" [(value)]=\"filterValue\" [groupOperations]=\"groupOperations\"\r\n                        (onEditorPreparing)=\"onEditorPreparing($event)\">\r\n                    </dx-filter-builder>\r\n                </div>\r\n            </div>\r\n            <pbi-color-format [storageKey]=\"_storageKey\" [enableInputDataFieldCheck]=\"true\" [showBasicFormat]=\"true\"\r\n                [enableInputCaptionField]=\"false\" [enableFormat]=\"false\" [enablePin]=\"false\" [enableAlignment]=\"false\"\r\n                [enableStorage]=\"false\" [selectedColumnInfo]=\"selectedColumnData\">\r\n            </pbi-color-format>\r\n        </div>\r\n    </div>\r\n    <span class=\"button-container\" *ngIf=\"conditionFormattingControlVisible\">\r\n        <dx-button class=\"save-button\" text=\"Save\" type=\"default\" (onClick)=\"updateConditionalFormatting()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
            styles: [""]
        })
    ], ConditionalFormattingComponent);
    return ConditionalFormattingComponent;
}());
export { ConditionalFormattingComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZGl0aW9uYWwtZm9ybWF0dGluZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29tbW9uLWdyaWQtZnJhbWV3b3JrL2NvbXBvbmVudHMvY29uZGl0aW9uYWwtZm9ybWF0dGluZy9jb25kaXRpb25hbC1mb3JtYXR0aW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3ZELE9BQU8sVUFBVSxNQUFNLDZCQUE2QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRzlFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQU81RDtJQStCRTtRQXJCZ0Isb0JBQWUsR0FBYSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztRQUsxQyxVQUFLLEdBQUcsd0JBQXdCLENBQUM7UUFDaEMsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pDLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDekQsbUJBQWMsR0FBb0IsRUFBRSxDQUFDO1FBQ3JDLGdCQUFXLEdBQUcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQ25FLGNBQVMsR0FBRyxRQUFRLENBQUM7UUFDckIsc0NBQWlDLEdBQUcsS0FBSyxDQUFDO1FBQzFDLG9CQUFlLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLDhCQUF5QixHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzlDLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ25CLHVCQUFrQixHQUFHLElBQUksQ0FBQztRQUMxQixnQ0FBMkIsR0FBRyxFQUFFLENBQUM7UUFDakMsc0JBQWlCLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLHVCQUFrQixHQUFpQixJQUFJLENBQUM7UUFDeEMsc0JBQWlCLEdBQW1CLEVBQUUsQ0FBQztJQUV2QixDQUFDO0lBNUJSLHNCQUFJLHlEQUFhO2FBSzFCLGNBQXNCLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7YUFMM0MsVUFBa0IsT0FBd0I7WUFBbkQsaUJBSUM7WUFIQyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQztZQUMvQixPQUFPLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFNLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDeEksQ0FBQzs7O09BQUE7SUFJUSxzQkFBSSxzREFBVTthQUFkLFVBQWUsR0FBVztZQUNqQyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsSUFBSSxjQUFjLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDL0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUM7OztPQUFBO0lBbUJELGlEQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoRyxDQUFDO0lBRUQsb0RBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEVBQWxCLENBQWtCLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBR00saUVBQXdCLEdBQS9CO1FBQ0UsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFFTSxrRUFBeUIsR0FBaEM7O1FBQ0UsVUFBSSxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLE1BQU0sRUFBRTtZQUNsQyxJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztTQUNsQztJQUNILENBQUM7SUFFTSwyREFBa0IsR0FBekI7UUFDRSxJQUFJLENBQUMsaUNBQWlDLEdBQUcsS0FBSyxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRU0sb0VBQTJCLEdBQWxDO1FBQUEsaUJBOEJDO1FBN0JDLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsRUFBRTtZQUNyRixRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRSxDQUFDLENBQUM7WUFDM0UsT0FBTztTQUNSO1FBQ0Qsb0VBQW9FO1FBQ3BFLElBQU0saUJBQWlCLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FBQztRQUNsRSxJQUFNLHNCQUFzQixHQUFlLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEcsSUFBSSxzQkFBc0IsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxFQUFFLHlFQUF5RTtZQUM3SSxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsc0JBQXNCLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUNsRSxJQUFJLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLGNBQWMsS0FBSyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxFQUFFO29CQUM3RixzQkFBc0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxpQkFBaUIsQ0FBQztvQkFDbEQsc0JBQXNCLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7b0JBQzNELHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLHFDQUFxQztvQkFDN0ssc0JBQXNCLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3ZFLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUN6RixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjthQUFNO1lBQ0wsaUJBQWlCLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDL0MsaUJBQWlCLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLHVCQUF1QixDQUFDLENBQUMscUNBQXFDO1lBQ2pLLGlCQUFpQixDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsRSxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUMzRCxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUNoRDtRQUNELGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNqRixJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixVQUFVLENBQUMsY0FBUSxLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLDhDQUE4QztJQUN4RyxDQUFDO0lBRU0sNERBQW1CLEdBQTFCO1FBQ0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBRU0saUVBQXdCLEdBQS9CLFVBQWdDLENBQUM7UUFBakMsaUJBaUJDO1FBaEJDLElBQU0sZ0JBQWdCLEdBQWUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzFGLElBQU0sZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLGNBQWMsS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUE1RixDQUE0RixDQUFDLENBQUM7UUFDMUosSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUN6QixJQUFJLGdCQUFnQixHQUFHLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2xELGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxLQUFLO29CQUN0QyxJQUFJLEtBQUssR0FBRyxnQkFBZ0IsRUFBRTt3QkFDNUIsT0FBTyxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLEtBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO3FCQUN0RztnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0QsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUMzRSxJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztZQUMvQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztZQUMxQixVQUFVLENBQUMsY0FBUSxLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLDhDQUE4QztTQUN2RztJQUNILENBQUM7SUFFTyxrRUFBeUIsR0FBakM7O1FBQ0UsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE9BQUEsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQywwQ0FBRSxTQUFTLEtBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ3hGLElBQUksQ0FBQyxpQ0FBaUMsR0FBRyxJQUFJLENBQUM7UUFDOUMsSUFBSSxDQUFDLFdBQVcsU0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLDBDQUFFLFNBQVMsQ0FBQztRQUN4RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEVBQUcsQ0FBQyxVQUVqRjtZQUNELFNBQVMsRUFBRSxVQUFVO1lBQ3JCLGVBQWUsRUFBRSxFQUFFO1lBQ25CLFNBQVMsRUFBRSxFQUFFO1lBQ2IsUUFBUSxFQUFFLGFBQUEsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxRQUFRLDBDQUFFLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxFQUFFLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxNQUFLLEVBQUU7WUFDckgsY0FBYyxFQUFFLEVBQUU7WUFDbEIsU0FBUyxFQUFFLEVBQUU7U0FDZCxDQUNGLENBQUM7UUFDSixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQUU7WUFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN6SDthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUI7U0FDdEU7UUFDRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRU8sZ0VBQXVCLEdBQS9CO1FBQ0UsSUFBTSxrQkFBa0IsR0FBZSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2xHLElBQUksQ0FBQywyQkFBMkIsR0FBRyxrQkFBa0IsSUFBSSxFQUFFLENBQUM7SUFDOUQsQ0FBQztJQUVNLGlFQUF3QixHQUEvQixjQUEwQyxJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFFckUsa0RBQVMsR0FBakI7UUFDRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDL0IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0lBQ2pDLENBQUM7SUFFTSwwREFBaUIsR0FBeEI7UUFDRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLENBQUM7UUFDcEUsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1NBQzdCO0lBQ0gsQ0FBQztJQUVPLHNEQUFhLEdBQXJCLFVBQXNCLFVBQWtCLEVBQUUsT0FBNkQsRUFDckcsaUJBQWtDLEVBQUUseUJBQWtEO1FBRDlDLHdCQUFBLEVBQUEsVUFBa0IsSUFBSSxDQUFDLDJCQUEyQixDQUFDLE1BQU0sR0FBRyxDQUFDO1FBQ3JHLGtDQUFBLEVBQUEsb0JBQW9CLElBQUksQ0FBQyxTQUFTO1FBQUUsMENBQUEsRUFBQSw0QkFBNEIsSUFBSSxDQUFDLGlCQUFpQjtRQUN0RixpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQztRQUM5RyxPQUFPLE1BQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFHLGlCQUFpQixxQkFBZ0IsT0FBUyxDQUFDO0lBQ3BHLENBQUM7SUFFTSwwREFBaUIsR0FBeEIsVUFBeUIsQ0FBQzs7UUFDeEIsNERBQTREO1FBQzVELElBQU0sWUFBWSxHQUFHLE9BQUEsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsU0FBUyxLQUFLLENBQUMsQ0FBQyxTQUFTLEVBQTlCLENBQThCLENBQUMsQ0FBQyxDQUFDLENBQUMsMENBQUUsTUFBTSxLQUFJLElBQUksQ0FBQztRQUMxRyxJQUFJLENBQUMsQ0FBQyxlQUFlLEtBQUssR0FBRyxJQUFJLFlBQVksRUFBRTtZQUM3QyxDQUFDLENBQUMsVUFBVSxHQUFHLGFBQWEsQ0FBQztZQUM3QixDQUFDLENBQUMsYUFBYSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDckMsSUFBSSxZQUFZLEVBQUU7Z0JBQ2hCLENBQUMsQ0FBQyxhQUFhLENBQUMsVUFBVSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFFLFlBQVksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO2FBQ2pGO1lBQ0QsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxjQUFjLEdBQUcsVUFBQyxLQUFLO2dCQUNyQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUM7U0FDSDtJQUNILENBQUM7SUFFTSxvREFBVyxHQUFsQjtRQUNFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBcExnQztRQUFoQyxTQUFTLENBQUMsb0JBQW9CLENBQUM7aUZBQTZDO0lBRXBFO1FBQVIsS0FBSyxFQUFFO3VFQUlQO0lBR1E7UUFBUixLQUFLLEVBQUU7MkVBQWtEO0lBQ2pEO1FBQVIsS0FBSyxFQUFFO29FQUdQO0lBQ1E7UUFBUixLQUFLLEVBQUU7aUVBQXlDO0lBQ3ZDO1FBQVQsTUFBTSxFQUFFO3VFQUF5QztJQUN4QztRQUFULE1BQU0sRUFBRTs4RUFBZ0Q7SUFqQjlDLDhCQUE4QjtRQUwxQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsNEJBQTRCO1lBQ3RDLDBwSUFBc0Q7O1NBRXZELENBQUM7T0FDVyw4QkFBOEIsQ0FzTDFDO0lBQUQscUNBQUM7Q0FBQSxBQXRMRCxJQXNMQztTQXRMWSw4QkFBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ0dGU3RvcmFnZUtleXMgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvZW51bXMnO1xyXG5pbXBvcnQgRGF0YVNvdXJjZSBmcm9tICdkZXZleHRyZW1lL2RhdGEvZGF0YV9zb3VyY2UnO1xyXG5pbXBvcnQgeyBDb2xvckZvcm1hdENvbXBvbmVudCB9IGZyb20gJy4uL2NvbG9yLWZvcm1hdC9jb2xvci1mb3JtYXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29sdW1uRm9ybWF0IH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbHVtbic7XHJcbmltcG9ydCB7IFBCSUdyaWRDb2x1bW4gfSBmcm9tICcuLi9jb250cmFjdHMvZ3JpZCc7XHJcbmltcG9ydCB7IGFwcFRvYXN0IH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdwYmktY29uZGl0aW9uYWwtZm9ybWF0dGluZycsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbmRpdGlvbmFsLWZvcm1hdHRpbmcuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbmRpdGlvbmFsLWZvcm1hdHRpbmcuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb25kaXRpb25hbEZvcm1hdHRpbmdDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBWaWV3Q2hpbGQoQ29sb3JGb3JtYXRDb21wb25lbnQpIGNvbHVtbkZvcm1hdENvbXBvbmVudDogQ29sb3JGb3JtYXRDb21wb25lbnQ7XHJcblxyXG4gIEBJbnB1dCgpIHNldCBsaXN0T2ZDb2x1bW5zKGNvbEluZm86IFBCSUdyaWRDb2x1bW5bXSkge1xyXG4gICAgdGhpcy5fbGlzdE9mQ29sdW1ucyA9IFtdO1xyXG4gICAgdGhpcy5jb3B5T2ZMaXN0SXRlbXMgPSBjb2xJbmZvO1xyXG4gICAgY29sSW5mby5mb3JFYWNoKGl0ZW0gPT4geyB0aGlzLl9saXN0T2ZDb2x1bW5zLnB1c2goeyBkYXRhRmllbGQ6IGl0ZW0uZGF0YUZpZWxkLCBjYXB0aW9uOiBpdGVtLmNhcHRpb24sIGRhdGFUeXBlOiBpdGVtLmRhdGFUeXBlIH0pOyB9KTtcclxuICB9XHJcbiAgZ2V0IGxpc3RPZkNvbHVtbnMoKSB7IHJldHVybiB0aGlzLmNvcHlPZkxpc3RJdGVtczsgfVxyXG5cclxuICBASW5wdXQoKSBwdWJsaWMgZ3JvdXBPcGVyYXRpb25zOiBzdHJpbmdbXSA9IFsnYW5kJywgJ29yJ107XHJcbiAgQElucHV0KCkgc2V0IHN0b3JhZ2VLZXkoa2V5OiBzdHJpbmcpIHtcclxuICAgIHRoaXMuX3N0b3JhZ2VLZXkgPSBrZXkgfHwgQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuY29uZGl0aW9uYWxGb3JtYXR0aW5nXTtcclxuICAgIHRoaXMubmdPbkluaXQoKTtcclxuICB9XHJcbiAgQElucHV0KCkgcHVibGljIHRpdGxlID0gJ0NPTkRJVElPTkFMIEZPUk1BVFRJTkcnO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgdmFsdWVDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBjbG9zZUN1cnJlbnRGbHlPdXQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgX2xpc3RPZkNvbHVtbnM6IFBCSUdyaWRDb2x1bW5bXSA9IFtdO1xyXG4gIF9zdG9yYWdlS2V5ID0gQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuY29uZGl0aW9uYWxGb3JtYXR0aW5nXTtcclxuICBhcHBseVR5cGUgPSAnQ29sdW1uJztcclxuICBjb25kaXRpb25Gb3JtYXR0aW5nQ29udHJvbFZpc2libGUgPSBmYWxzZTtcclxuICBjb3B5T2ZMaXN0SXRlbXMgPSBbXTtcclxuICBmaWx0ZXJBcHBseVR5cGVDb2xsZWN0aW9uID0gWydDb2x1bW4nLCAnUm93J107XHJcbiAgZmlsdGVyVmFsdWUgPSBudWxsO1xyXG4gIGhpZGVDb2x1bW5Ecm9wZG93biA9IHRydWU7XHJcbiAgbGlzdE9mQ29uZGl0aW9uYWxGb3JtYXR0aW5nID0gW107XHJcbiAgb3BlcmF0aW9uT25Db2x1bW4gPSAnJztcclxuICBzZWxlY3RlZENvbHVtbkRhdGE6IENvbHVtbkZvcm1hdCA9IG51bGw7XHJcbiAgc2VsZWN0ZWRDb25kaXRpb246IENvbHVtbkZvcm1hdFtdID0gW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5oaWRlRm9ybWF0dGluZ0luZm8oKTtcclxuICAgIHRoaXMubGlzdE9mQ29uZGl0aW9uYWxGb3JtYXR0aW5nID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKHRoaXMuX3N0b3JhZ2VLZXkpKSB8fCBbXTtcclxuICB9XHJcblxyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5saXN0T2ZDb2x1bW5zLmZvckVhY2goaXRlbSA9PiBpdGVtLmxvb2t1cCA9IG51bGwpO1xyXG4gIH1cclxuXHJcblxyXG4gIHB1YmxpYyBhZGRDb25kaXRpb25hbEZvcm1hdHRpbmcoKTogdm9pZCB7XHJcbiAgICB0aGlzLnJlc2V0SW5mbygpO1xyXG4gICAgdGhpcy5zaG93Q29uZGl0aW9uYWxGb3JtYXR0aW5nKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZWRpdENvbmRpdGlvbmFsRm9ybWF0dGluZygpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkQ29uZGl0aW9uPy5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5zaG93Q29uZGl0aW9uYWxGb3JtYXR0aW5nKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgaGlkZUZvcm1hdHRpbmdJbmZvKCk6IHZvaWQge1xyXG4gICAgdGhpcy5jb25kaXRpb25Gb3JtYXR0aW5nQ29udHJvbFZpc2libGUgPSBmYWxzZTtcclxuICAgIHRoaXMucmVzZXRJbmZvKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdXBkYXRlQ29uZGl0aW9uYWxGb3JtYXR0aW5nKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMub3BlcmF0aW9uT25Db2x1bW4udHJpbSgpID09PSAnJyAmJiB0aGlzLmFwcGx5VHlwZS50b0xvd2VyQ2FzZSgpID09PSAnY29sdW1uJykge1xyXG4gICAgICBhcHBUb2FzdCh7IHR5cGU6ICd3YXJuaW5nJywgbWVzc2FnZTogYFBsZWFzZSBzZWxlY3QgJ0FwcGx5IFRvIENvbHVtbicuYCB9KTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgLy8gdGhpcy5fZGF0YVNvdXJjZUZpbHRlclZhbHVlID0gdGhpcy5mb3JtYXRWYWx1ZSh0aGlzLmZpbHRlclZhbHVlKTtcclxuICAgIGNvbnN0IGN1cnJlbnRGb3JtYXR0aW5nID0gdGhpcy5jb2x1bW5Gb3JtYXRDb21wb25lbnQuZm9ybWF0T2JqZWN0O1xyXG4gICAgY29uc3QgZXhpc3RpbmdGb3JtYXR0aW5nSW5mbzogQXJyYXk8YW55PiA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSh0aGlzLl9zdG9yYWdlS2V5KSkgfHwgW107XHJcbiAgICBpZiAoZXhpc3RpbmdGb3JtYXR0aW5nSW5mby5sZW5ndGggJiYgdGhpcy5zZWxlY3RlZENvbmRpdGlvbi5sZW5ndGgpIHsgLy8gYWRkZWQgdGhpcyBjb25kaXRpb24gd2hpY2ggaW5mb3JtcyB3aGV0aGVyIGFkZGluZyBvciBlZGl0aW5nIGNvbmRpdGlvblxyXG4gICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgZXhpc3RpbmdGb3JtYXR0aW5nSW5mby5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICBpZiAoZXhpc3RpbmdGb3JtYXR0aW5nSW5mb1tpbmRleF0uZm9ybWF0UnVsZU5hbWUgPT09IHRoaXMuc2VsZWN0ZWRDb25kaXRpb25bMF0uZm9ybWF0UnVsZU5hbWUpIHtcclxuICAgICAgICAgIGV4aXN0aW5nRm9ybWF0dGluZ0luZm9baW5kZXhdID0gY3VycmVudEZvcm1hdHRpbmc7XHJcbiAgICAgICAgICBleGlzdGluZ0Zvcm1hdHRpbmdJbmZvW2luZGV4XS5jb25kaXRpb24gPSB0aGlzLmZpbHRlclZhbHVlO1xyXG4gICAgICAgICAgZXhpc3RpbmdGb3JtYXR0aW5nSW5mb1tpbmRleF0uZGF0YUZpZWxkID0gdGhpcy5hcHBseVR5cGUudG9Mb3dlckNhc2UoKSA9PT0gJ2NvbHVtbicgPyB0aGlzLm9wZXJhdGlvbk9uQ29sdW1uIDogJ19fX2RhdGFGaWVsZEZvclJvd19fXyc7IC8vIEFkZGluZyBkdW1teSBkYXRhRmllbGQgZm9yIHJvd1R5cGVcclxuICAgICAgICAgIGV4aXN0aW5nRm9ybWF0dGluZ0luZm9baW5kZXhdLmFwcGx5VHlwZSA9IHRoaXMuYXBwbHlUeXBlLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICBleGlzdGluZ0Zvcm1hdHRpbmdJbmZvW2luZGV4XS5mb3JtYXRSdWxlTmFtZSA9IHRoaXMuZ2V0Rm9ybWF0TmFtZSh0aGlzLnRpdGxlLCBpbmRleCArIDEpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjdXJyZW50Rm9ybWF0dGluZy5jb25kaXRpb24gPSB0aGlzLmZpbHRlclZhbHVlO1xyXG4gICAgICBjdXJyZW50Rm9ybWF0dGluZy5kYXRhRmllbGQgPSB0aGlzLmFwcGx5VHlwZS50b0xvd2VyQ2FzZSgpID09PSAnY29sdW1uJyA/IHRoaXMub3BlcmF0aW9uT25Db2x1bW4gOiAnX19fZGF0YUZpZWxkRm9yUm93X19fJzsgLy8gQWRkaW5nIGR1bW15IGRhdGFGaWVsZCBmb3Igcm93VHlwZVxyXG4gICAgICBjdXJyZW50Rm9ybWF0dGluZy5mb3JtYXRSdWxlTmFtZSA9IHRoaXMuZ2V0Rm9ybWF0TmFtZSh0aGlzLnRpdGxlKTtcclxuICAgICAgY3VycmVudEZvcm1hdHRpbmcuYXBwbHlUeXBlID0gdGhpcy5hcHBseVR5cGUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgZXhpc3RpbmdGb3JtYXR0aW5nSW5mby5wdXNoKGN1cnJlbnRGb3JtYXR0aW5nKTtcclxuICAgIH1cclxuICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0odGhpcy5fc3RvcmFnZUtleSwgSlNPTi5zdHJpbmdpZnkoZXhpc3RpbmdGb3JtYXR0aW5nSW5mbykpO1xyXG4gICAgdGhpcy5wcmVwYXJlTGlzdE9mQ29uZGl0aW9ucygpO1xyXG4gICAgdGhpcy5oaWRlRm9ybWF0dGluZ0luZm8oKTtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4geyB0aGlzLmZvcmNlVG9BcHBseUZvcm1hdHMoKTsgfSwgMTAwKTsgLy8gd2lsbCB1cGRhdGUgbGlzdCBvbiBVSSB0aGVuIGZvcmNlZCByZWZyZXNoLlxyXG4gIH1cclxuXHJcbiAgcHVibGljIGZvcmNlVG9BcHBseUZvcm1hdHMoKTogdm9pZCB7XHJcbiAgICB0aGlzLnZhbHVlQ2hhbmdlLmVtaXQoeyBkYXRhOiBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0odGhpcy5fc3RvcmFnZUtleSkpIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uQ29uZGl0aW9uYWxJdGVtRGVsZXRlZChlKTogdm9pZCB7XHJcbiAgICBjb25zdCBfZXhpc3RpbmdGb3JtYXRzOiBBcnJheTxhbnk+ID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKHRoaXMuX3N0b3JhZ2VLZXkpKTtcclxuICAgIGNvbnN0IGRlbGV0ZWRJdGVtSW5kZXggPSBfZXhpc3RpbmdGb3JtYXRzLmZpbmRJbmRleChpdGVtID0+IGl0ZW0uZm9ybWF0UnVsZU5hbWUgPT09IGUuaXRlbURhdGEuZm9ybWF0UnVsZU5hbWUgJiYgaXRlbS5kYXRhRmllbGQgPT09IGUuaXRlbURhdGEuZGF0YUZpZWxkKTtcclxuICAgIGlmIChkZWxldGVkSXRlbUluZGV4ID4gLTEpIHtcclxuICAgICAgaWYgKGRlbGV0ZWRJdGVtSW5kZXggKyAxIDwgX2V4aXN0aW5nRm9ybWF0cy5sZW5ndGgpIHtcclxuICAgICAgICBfZXhpc3RpbmdGb3JtYXRzLmZvckVhY2goKGFyckl0ZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICBpZiAoaW5kZXggPiBkZWxldGVkSXRlbUluZGV4KSB7XHJcbiAgICAgICAgICAgIGFyckl0ZW0uZm9ybWF0UnVsZU5hbWUgPSB0aGlzLmdldEZvcm1hdE5hbWUodGhpcy50aXRsZSwgaW5kZXgsIGFyckl0ZW0uYXBwbHlUeXBlLCBhcnJJdGVtLmRhdGFGaWVsZCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgICAgX2V4aXN0aW5nRm9ybWF0cy5zcGxpY2UoZGVsZXRlZEl0ZW1JbmRleCwgMSk7XHJcbiAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0odGhpcy5fc3RvcmFnZUtleSwgSlNPTi5zdHJpbmdpZnkoX2V4aXN0aW5nRm9ybWF0cykpO1xyXG4gICAgICB0aGlzLnByZXBhcmVMaXN0T2ZDb25kaXRpb25zKCk7XHJcbiAgICAgIHRoaXMuaGlkZUZvcm1hdHRpbmdJbmZvKCk7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4geyB0aGlzLmZvcmNlVG9BcHBseUZvcm1hdHMoKTsgfSwgMTAwKTsgLy8gd2lsbCB1cGRhdGUgbGlzdCBvbiBVSSB0aGVuIGZvcmNlZCByZWZyZXNoLlxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzaG93Q29uZGl0aW9uYWxGb3JtYXR0aW5nKCk6IHZvaWQge1xyXG4gICAgdGhpcy5vcGVyYXRpb25PbkNvbHVtbiA9IHRoaXMuc2VsZWN0ZWRDb25kaXRpb25bMF0/LmRhdGFGaWVsZCB8fCB0aGlzLm9wZXJhdGlvbk9uQ29sdW1uO1xyXG4gICAgdGhpcy5jb25kaXRpb25Gb3JtYXR0aW5nQ29udHJvbFZpc2libGUgPSB0cnVlO1xyXG4gICAgdGhpcy5maWx0ZXJWYWx1ZSA9IHRoaXMuc2VsZWN0ZWRDb25kaXRpb25bMF0/LmNvbmRpdGlvbjtcclxuICAgIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhID0gdGhpcy5zZWxlY3RlZENvbmRpdGlvblswXSA/IHsgLi4udGhpcy5zZWxlY3RlZENvbmRpdGlvblswXSB9IDpcclxuICAgICAge1xyXG4gICAgICAgIC4uLntcclxuICAgICAgICAgIGRhdGFGaWVsZDogJ19fbnVsbF9fJyxcclxuICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogJycsXHJcbiAgICAgICAgICBjb25kaXRpb246ICcnLFxyXG4gICAgICAgICAgY3NzQ2xhc3M6IHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhPy5jc3NDbGFzcz8ucmVwbGFjZSgnYm9sZCcsICcnKS5yZXBsYWNlKCd1bmRlcmxpbmUnLCAnJykucmVwbGFjZSgnaXRhbGljJywgJycpIHx8ICcnLFxyXG4gICAgICAgICAgZm9ybWF0UnVsZU5hbWU6ICcnLFxyXG4gICAgICAgICAgdGV4dENvbG9yOiAnJyxcclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuYXBwbHlUeXBlKSB7XHJcbiAgICAgIHRoaXMuYXBwbHlUeXBlID0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuYXBwbHlUeXBlLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuYXBwbHlUeXBlLnNsaWNlKDEpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hcHBseVR5cGUgPSB0aGlzLmZpbHRlckFwcGx5VHlwZUNvbGxlY3Rpb25bMF07IC8vIFNldHRpbmcgY29sdW1uXHJcbiAgICB9XHJcbiAgICB0aGlzLm9uQXBwbHlUeXBlQ2hhbmdlKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZXBhcmVMaXN0T2ZDb25kaXRpb25zKCk6IHZvaWQge1xyXG4gICAgY29uc3QgY29uZGl0aW9uYWxGb3JtYXRzOiBBcnJheTxhbnk+ID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKHRoaXMuX3N0b3JhZ2VLZXkpKSB8fCBbXTtcclxuICAgIHRoaXMubGlzdE9mQ29uZGl0aW9uYWxGb3JtYXR0aW5nID0gY29uZGl0aW9uYWxGb3JtYXRzIHx8IFtdO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRm9ybWF0U2VsZWN0aW9uQ2hhbmdlZCgpOiB2b2lkIHsgdGhpcy5lZGl0Q29uZGl0aW9uYWxGb3JtYXR0aW5nKCk7IH1cclxuXHJcbiAgcHJpdmF0ZSByZXNldEluZm8oKTogdm9pZCB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29uZGl0aW9uID0gW107XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YSA9IG51bGw7XHJcbiAgICB0aGlzLm9wZXJhdGlvbk9uQ29sdW1uID0gJyc7XHJcbiAgICB0aGlzLmhpZGVDb2x1bW5Ecm9wZG93biA9IHRydWU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25BcHBseVR5cGVDaGFuZ2UoKTogdm9pZCB7XHJcbiAgICB0aGlzLmhpZGVDb2x1bW5Ecm9wZG93biA9IHRoaXMuYXBwbHlUeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdjb2x1bW4nO1xyXG4gICAgaWYgKCF0aGlzLmhpZGVDb2x1bW5Ecm9wZG93bikge1xyXG4gICAgICB0aGlzLm9wZXJhdGlvbk9uQ29sdW1uID0gJyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldEZvcm1hdE5hbWUoZW50aXR5TmFtZTogc3RyaW5nLCBjb3VudGVyOiBudW1iZXIgPSB0aGlzLmxpc3RPZkNvbmRpdGlvbmFsRm9ybWF0dGluZy5sZW5ndGggKyAxLFxyXG4gICAgc2VsZWN0ZWRBcHBseVR5cGUgPSB0aGlzLmFwcGx5VHlwZSwgc2VsZWN0ZWRPcGVyYXRpb25PbkNvbHVtbiA9IHRoaXMub3BlcmF0aW9uT25Db2x1bW4pOiBzdHJpbmcge1xyXG4gICAgc2VsZWN0ZWRBcHBseVR5cGUgPSBzZWxlY3RlZEFwcGx5VHlwZS50b0xvd2VyQ2FzZSgpID09PSAncm93JyA/IHNlbGVjdGVkQXBwbHlUeXBlIDogc2VsZWN0ZWRPcGVyYXRpb25PbkNvbHVtbjtcclxuICAgIHJldHVybiBgJHtlbnRpdHlOYW1lLmxlbmd0aCA/IGVudGl0eU5hbWUgKyAnOiAnIDogJyd9JHtzZWxlY3RlZEFwcGx5VHlwZX0gRm9ybWF0IFJ1bGUgJHtjb3VudGVyfWA7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25FZGl0b3JQcmVwYXJpbmcoZSk6IHZvaWQge1xyXG4gICAgLy8gRHluYW1pYyBjb250cm9sIGNoYW5nZSBiYXNlZCBvbiB0aGUgb3BlcmF0aW9uIHRvIGJlIGRvbmUuXHJcbiAgICBjb25zdCBmaWx0ZXJMb29rVXAgPSB0aGlzLmxpc3RPZkNvbHVtbnMuZmlsdGVyKGl0ZW0gPT4gaXRlbS5kYXRhRmllbGQgPT09IGUuZGF0YUZpZWxkKVswXT8ubG9va3VwIHx8IG51bGw7XHJcbiAgICBpZiAoZS5maWx0ZXJPcGVyYXRpb24gPT09ICc9JyAmJiBmaWx0ZXJMb29rVXApIHtcclxuICAgICAgZS5lZGl0b3JOYW1lID0gJ2R4U2VsZWN0Qm94JztcclxuICAgICAgZS5lZGl0b3JPcHRpb25zLnNlYXJjaEVuYWJsZWQgPSB0cnVlO1xyXG4gICAgICBpZiAoZmlsdGVyTG9va1VwKSB7XHJcbiAgICAgICAgZS5lZGl0b3JPcHRpb25zLmRhdGFTb3VyY2UgPSBuZXcgRGF0YVNvdXJjZSh7IHN0b3JlOiBmaWx0ZXJMb29rVXAuZGF0YVNvdXJjZSB9KTtcclxuICAgICAgfVxyXG4gICAgICBlLmVkaXRvck9wdGlvbnMub25WYWx1ZUNoYW5nZWQgPSAoZXZlbnQpID0+IHtcclxuICAgICAgICBlLnNldFZhbHVlKGV2ZW50LnZhbHVlKTtcclxuICAgICAgfTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbG9zZUZseU91dCgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2xvc2VDdXJyZW50Rmx5T3V0LmVtaXQoKTtcclxuICB9XHJcbn1cclxuIl19