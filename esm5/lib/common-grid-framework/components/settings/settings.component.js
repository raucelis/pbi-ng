import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingExportOptions } from '../../utilities/constants';
import { CGFSettingsEnum } from '../../utilities/enums';
import { getClassNameByThemeName } from '../../utilities/utilityFunctions';
var SettingsComponent = /** @class */ (function () {
    function SettingsComponent(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.cgfSettingsOptions = [];
        this.gridSwitchValueChange = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.hideGridBorder = false;
        this.radioGroup = ['Auto Fit'];
        this.radioGroupValue = null;
        this.showFormattingSection = true;
        this.themeList = [
            { key: 'light.regular', value: 'Cozy' },
            { key: 'light.compact', value: 'Regular' },
            { key: 'np.compact', value: 'Default' }
        ];
        this.exportActionList = [];
        this.cgfSettingsEnum = CGFSettingsEnum;
        this.currentAppliedTheme = 'np.compact';
    }
    Object.defineProperty(SettingsComponent.prototype, "selectedTheme", {
        get: function () { return this.currentAppliedTheme; },
        set: function (themeName) {
            if (sessionStorage.getItem('theme')) {
                this.currentAppliedTheme = sessionStorage.getItem('theme');
            }
            else {
                this.currentAppliedTheme = themeName;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SettingsComponent.prototype, "selectedBorderSwitch", {
        get: function () { return this.hideGridBorder; },
        set: function (val) {
            this.hideGridBorder = val;
        },
        enumerable: true,
        configurable: true
    });
    SettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.exportActionList = SettingExportOptions.filter(function (option) { return _this.cgfSettingsOptions.indexOf(option.id) > -1; });
    };
    SettingsComponent.prototype.onThemeChanged = function () {
        sessionStorage.setItem('theme', this.currentAppliedTheme);
        var appliedThemeName = getClassNameByThemeName(this.currentAppliedTheme);
        this.gridInstanceList.forEach(function (item) {
            var existingClasses = item.gridComponentInstance._$element[0].className;
            if (existingClasses.indexOf('dx-swatch-default') > -1) {
                existingClasses = existingClasses.replace(new RegExp('dx-swatch-default', 'g'), '');
            }
            else if (existingClasses.indexOf('dx-swatch-regular') > -1) {
                existingClasses = existingClasses.replace(new RegExp('dx-swatch-regular', 'g'), '');
            }
            item.gridComponentInstance._$element[0].className = existingClasses + " " + appliedThemeName;
        });
        this.emitToggleValue('theme', this.currentAppliedTheme);
    };
    SettingsComponent.prototype.onHideGridChange = function () {
        this.emitToggleValue('gridLines', this.hideGridBorder);
    };
    SettingsComponent.prototype.onClickOfDEMButton = function () {
        // const params = {
        //   webMenuId: getWebMenuIdByKey('DataEntityManagement'),
        //   entityId: this.activatedRoute.snapshot.queryParams.entity,
        //   activeTab: 'databrowserentity'
        // };
        // this.router.navigate(['/dataEntityManagement'], { queryParams: params });
    };
    SettingsComponent.prototype.onAutoFitChange = function () {
        var gridInstanceMaxCol = {
            gridComponentInstance: new Object(),
            gridName: '',
            isMasterGrid: false,
            isSelected: false
        };
        var existColCount = 0;
        this.gridInstanceList.forEach(function (gridInstance) {
            var _a;
            var visibleColCount = ((_a = gridInstance.gridComponentInstance.getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.length) || 0;
            if (existColCount < visibleColCount) {
                gridInstanceMaxCol = gridInstance;
                existColCount = visibleColCount;
            }
        });
        gridInstanceMaxCol.gridComponentInstance.beginUpdate();
        var colCount = gridInstanceMaxCol.gridComponentInstance.columnCount();
        for (var i = 0; i < colCount; i++) {
            if (gridInstanceMaxCol.gridComponentInstance.columnOption(i, 'visible')) {
                gridInstanceMaxCol.gridComponentInstance.columnOption(i, 'width', 'auto');
            }
        }
        gridInstanceMaxCol.gridComponentInstance.endUpdate();
    };
    SettingsComponent.prototype.emitToggleValue = function (key, value) {
        var _data = { key: key, value: value };
        this.gridSwitchValueChange.emit(_data);
    };
    SettingsComponent.prototype.onWorkFlowEmailClick = function () {
        // const redirectURL =
        //   this.configService.config.WorkFlowMasterBaseURL + '/#/creator/exportreportingplatform?url=' + encodeURIComponent(location.href);
        // open(redirectURL, '_blank');
    };
    SettingsComponent.prototype.getKey = function () {
        // this.cgfService.getApiKey().subscribe(response => { this.generateUrlSuccess(response); }, () => {
        //   apiCallError('Error while generating key. Please try again.');
        // });
    };
    SettingsComponent.prototype.generateUrlSuccess = function (response) {
        // if (response.ToasterType.toLowerCase().trim() === 'success') {
        //   const urlParams = location.hash.split('?')[1];
        //   const url = `${APIEndPoints.dataBrowser.excelData}?${urlParams}&key=${response.Message}`;
        //   copyTextToClipBoard(url);
        //   appToastr({ type: 'success', message: 'Link generated and copied successfully.' });
        // } else if (response.ToasterType.toLowerCase().trim() === 'error') {
        //   appToastr({ type: 'error', message: response.Message || 'Error while generating key. Please try again.' });
        // }
    };
    SettingsComponent.prototype.exportSectionButtonClick = function (data) {
        switch (data.key) {
            case 'exportExcel':
                var selectedInstance = this.gridInstanceList.filter(function (item) { return item.isSelected; })[0];
                if (selectedInstance) {
                    // When the selectionOnly parameter is false - the method exports all rows, when true - only the selected ones.
                    selectedInstance.gridComponentInstance.exportToExcel(false);
                }
                break;
            case 'excelLink':
                this.getKey();
                break;
            case 'emailReport':
                this.onWorkFlowEmailClick();
                break;
        }
    };
    SettingsComponent.prototype.closeFlyOut = function () {
        this.closeCurrentFlyOut.emit();
    };
    SettingsComponent.ctorParameters = function () { return [
        { type: ActivatedRoute },
        { type: Router }
    ]; };
    __decorate([
        Input()
    ], SettingsComponent.prototype, "cgfSettingsOptions", void 0);
    __decorate([
        Input()
    ], SettingsComponent.prototype, "selectedTheme", null);
    __decorate([
        Input()
    ], SettingsComponent.prototype, "selectedBorderSwitch", null);
    __decorate([
        Input()
    ], SettingsComponent.prototype, "gridInstanceList", void 0);
    __decorate([
        Output()
    ], SettingsComponent.prototype, "gridSwitchValueChange", void 0);
    __decorate([
        Output()
    ], SettingsComponent.prototype, "closeCurrentFlyOut", void 0);
    SettingsComponent = __decorate([
        Component({
            selector: 'pbi-settings',
            template: "<div class=\"settings-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-cog title-icon\"></i>\r\n            <span class=\"section-title\">Settings</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"pointer property-tab-in-view-selection\" (click)=\"showFormattingSection = !showFormattingSection\">\r\n                <span>FORMATTING</span>\r\n                <span [class.fa-angle-down]=\"!showFormattingSection\" [class.fa-angle-up]=\"showFormattingSection\"\r\n                    class=\"fa basic-format-toggle-icon\"></span>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n    <div class=\"accordion-data-container\" *ngIf=\"showFormattingSection\">\r\n        <div class=\"setting-content\">\r\n            <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.theme) > -1\">\r\n                <label>THEME</label>\r\n                <div>\r\n                    <dx-select-box class=\"theme-dropdown\" [items]=\"themeList\" valueExpr=\"key\" displayExpr=\"value\"\r\n                        placeholder=\"Select Theme\" [(value)]=\"currentAppliedTheme\" (onValueChanged)=\"onThemeChanged()\">\r\n                        <div [class.selected-theme]=\"currentAppliedTheme === data.key\" *dxTemplate=\"let data of 'item'\">\r\n                            <div class=\"theme-item\">\r\n                                <div class=\"theme-name\">\r\n                                    {{data.value}}\r\n                                </div>\r\n                                <i class=\"fa fa-check theme-icon\" *ngIf=\"currentAppliedTheme === data.key\"></i>\r\n                            </div>\r\n                        </div>\r\n                    </dx-select-box>\r\n                </div>\r\n            </div>\r\n            <div class=\"grid-control-container\">\r\n                <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.gridLines) > -1\">\r\n                    <label>GRIDLINES</label>\r\n                    <div>\r\n                        <!-- <app-bool-input [(State)]=\"hideGridBorder\" (StateChange)=\"onHideGridChange($event)\">\r\n                        </app-bool-input> -->\r\n                        <label class=\"switch\">\r\n                            <!-- <app-bool-input [(State)]=\"isPinned\"></app-bool-input> -->\r\n                            <span>\r\n                                <label class=\"switch\">\r\n                                    <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"hideGridBorder\"\r\n                                        (ngModelChange)=\"onHideGridChange()\">\r\n                                    <span class=\"slider round\"></span>\r\n                                </label>\r\n                            </span>\r\n                        </label>\r\n\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.autoFit) > -1\">\r\n                    <dx-button class=\"settings-custom-btn-dem auto-fit\" (onClick)=\"onAutoFitChange()\"\r\n                        template=\"DEMButtonTemplate\">\r\n                        <div class=\"DEMButtonTemplate\" *dxTemplate=\"let buttonData of 'DEMButtonTemplate'\">\r\n                            <i class=\"fas fa-arrows-alt-h\"></i>\r\n                            <div class=\"button-text\">Auto Fit</div>\r\n                        </div>\r\n                    </dx-button>\r\n                </div>\r\n            </div>\r\n            <div class=\"section\" *ngIf=\"exportActionList.length > 0\">\r\n                <label>EXPORTS</label>\r\n                <div class=\"export-button-containers\">\r\n                    <div *ngFor=\"let item of exportActionList\">\r\n                        <dx-button class=\"settings-custom-btn\" template=\"buttonTemplate\"\r\n                            (click)=\"exportSectionButtonClick(item)\">\r\n                            <div class=\"button-template\" *dxTemplate=\"let buttonData of 'buttonTemplate'\">\r\n                                <div>\r\n                                    <i class=\"{{item.icon}}\"></i>\r\n                                </div>\r\n                                <div class=\"button-text\">{{item.title}}</div>\r\n                            </div>\r\n                        </dx-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"section\" *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.advDem) > -1\">\r\n                <label>ADVANCED MANAGEMENT</label>\r\n                <div>\r\n                    <div>\r\n                        <dx-button class=\"settings-custom-btn-dem\" (onClick)=\"onClickOfDEMButton()\"\r\n                            template=\"DEMButtonTemplate\">\r\n                            <div class=\"DEMButtonTemplate\" *dxTemplate=\"let buttonData of 'DEMButtonTemplate'\">\r\n                                <i class=\"fas fa-wrench\"></i>\r\n                                <div class=\"button-text\">Data Entity Management</div>\r\n                            </div>\r\n                        </dx-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
            styles: [""]
        })
    ], SettingsComponent);
    return SettingsComponent;
}());
export { SettingsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL3NldHRpbmdzL3NldHRpbmdzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXpELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQVUzRTtJQWlDRSwyQkFBb0IsY0FBOEIsRUFBVSxNQUFjO1FBQXRELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7UUFoQzFELHVCQUFrQixHQUEyQixFQUFFLENBQUM7UUFrQi9DLDBCQUFxQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0MsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RCxtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixlQUFVLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMxQixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QiwwQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDN0IsY0FBUyxHQUFvQjtZQUMzQixFQUFFLEdBQUcsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtZQUN2QyxFQUFFLEdBQUcsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRTtZQUMxQyxFQUFFLEdBQUcsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRTtTQUFDLENBQUM7UUFDM0MscUJBQWdCLEdBQWUsRUFBRSxDQUFDO1FBQ2xDLG9CQUFlLEdBQUcsZUFBZSxDQUFDO1FBQ2xDLHdCQUFtQixHQUFHLFlBQVksQ0FBQztJQUUyQyxDQUFDO0lBOUIvRSxzQkFBSSw0Q0FBYTthQU9qQixjQUFzQixPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUM7YUFQeEQsVUFBa0IsU0FBaUI7WUFDakMsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNuQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUM1RDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsU0FBUyxDQUFDO2FBQ3RDO1FBQ0gsQ0FBQzs7O09BQUE7SUFJRCxzQkFBSSxtREFBb0I7YUFHeEIsY0FBNkIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQzthQUgxRCxVQUF5QixHQUFZO1lBQ25DLElBQUksQ0FBQyxjQUFjLEdBQUcsR0FBRyxDQUFDO1FBQzVCLENBQUM7OztPQUFBO0lBb0JNLG9DQUFRLEdBQWY7UUFBQSxpQkFFQztRQURDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBL0MsQ0FBK0MsQ0FBQyxDQUFDO0lBQ2pILENBQUM7SUFFTSwwQ0FBYyxHQUFyQjtRQUNFLGNBQWMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQzFELElBQU0sZ0JBQWdCLEdBQUcsdUJBQXVCLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDM0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDaEMsSUFBSSxlQUFlLEdBQVksSUFBSSxDQUFDLHFCQUE2QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDekYsSUFBSSxlQUFlLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3JELGVBQWUsR0FBRyxlQUFlLENBQUMsT0FBTyxDQUFDLElBQUksTUFBTSxDQUFDLG1CQUFtQixFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ3JGO2lCQUFNLElBQUksZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUM1RCxlQUFlLEdBQUcsZUFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNyRjtZQUNBLElBQUksQ0FBQyxxQkFBNkIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFNLGVBQWUsU0FBSSxnQkFBa0IsQ0FBQztRQUN4RyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFTSw0Q0FBZ0IsR0FBdkI7UUFDRSxJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVNLDhDQUFrQixHQUF6QjtRQUNFLG1CQUFtQjtRQUNuQiwwREFBMEQ7UUFDMUQsK0RBQStEO1FBQy9ELG1DQUFtQztRQUNuQyxLQUFLO1FBQ0wsNEVBQTRFO0lBQzlFLENBQUM7SUFFTSwyQ0FBZSxHQUF0QjtRQUNFLElBQUksa0JBQWtCLEdBQTJCO1lBQy9DLHFCQUFxQixFQUFFLElBQUksTUFBTSxFQUFnQjtZQUNqRCxRQUFRLEVBQUUsRUFBRTtZQUNaLFlBQVksRUFBRSxLQUFLO1lBQ25CLFVBQVUsRUFBRSxLQUFLO1NBQ2xCLENBQUM7UUFDRixJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUM7UUFDdEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLFlBQVk7O1lBQ3hDLElBQU0sZUFBZSxHQUFHLE9BQUEsWUFBWSxDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixFQUFFLDBDQUFFLE1BQU0sS0FBSSxDQUFDLENBQUM7WUFDNUYsSUFBSSxhQUFhLEdBQUcsZUFBZSxFQUFFO2dCQUNuQyxrQkFBa0IsR0FBRyxZQUFZLENBQUM7Z0JBQ2xDLGFBQWEsR0FBRyxlQUFlLENBQUM7YUFDakM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZELElBQU0sUUFBUSxHQUFHLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3hFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDakMsSUFBSSxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxFQUFFO2dCQUN2RSxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQzthQUMzRTtTQUNGO1FBQ0Qsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDdkQsQ0FBQztJQUVPLDJDQUFlLEdBQXZCLFVBQXdCLEdBQVcsRUFBRSxLQUF1QjtRQUMxRCxJQUFNLEtBQUssR0FBYSxFQUFFLEdBQUcsS0FBQSxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRU0sZ0RBQW9CLEdBQTNCO1FBQ0Usc0JBQXNCO1FBQ3RCLHFJQUFxSTtRQUNySSwrQkFBK0I7SUFDakMsQ0FBQztJQUVNLGtDQUFNLEdBQWI7UUFDRSxvR0FBb0c7UUFDcEcsbUVBQW1FO1FBQ25FLE1BQU07SUFDUixDQUFDO0lBRU8sOENBQWtCLEdBQTFCLFVBQTJCLFFBQVE7UUFDakMsaUVBQWlFO1FBQ2pFLG1EQUFtRDtRQUNuRCw4RkFBOEY7UUFDOUYsOEJBQThCO1FBQzlCLHdGQUF3RjtRQUN4RixzRUFBc0U7UUFDdEUsZ0hBQWdIO1FBQ2hILElBQUk7SUFDTixDQUFDO0lBRU0sb0RBQXdCLEdBQS9CLFVBQWdDLElBQUk7UUFDbEMsUUFBUSxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ2hCLEtBQUssYUFBYTtnQkFDaEIsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFVBQVUsRUFBZixDQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEYsSUFBSSxnQkFBZ0IsRUFBRTtvQkFDcEIsK0dBQStHO29CQUMvRyxnQkFBZ0IsQ0FBQyxxQkFBcUIsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzdEO2dCQUNELE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNkLE1BQU07WUFDUixLQUFLLGFBQWE7Z0JBQ2hCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2dCQUM1QixNQUFNO1NBQ1Q7SUFDSCxDQUFDO0lBRU0sdUNBQVcsR0FBbEI7UUFDRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQzs7Z0JBNUdtQyxjQUFjO2dCQUFrQixNQUFNOztJQWhDakU7UUFBUixLQUFLLEVBQUU7aUVBQXdEO0lBRWhFO1FBREMsS0FBSyxFQUFFOzBEQU9QO0lBSUQ7UUFEQyxLQUFLLEVBQUU7aUVBR1A7SUFHUTtRQUFSLEtBQUssRUFBRTsrREFBd0Q7SUFDdEQ7UUFBVCxNQUFNLEVBQUU7b0VBQW1EO0lBQ2xEO1FBQVQsTUFBTSxFQUFFO2lFQUFnRDtJQXBCOUMsaUJBQWlCO1FBTjdCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxjQUFjO1lBQ3hCLGczS0FBd0M7O1NBRXpDLENBQUM7T0FFVyxpQkFBaUIsQ0ErSTdCO0lBQUQsd0JBQUM7Q0FBQSxBQS9JRCxJQStJQztTQS9JWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uSW5pdCwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgZHhEYXRhR3JpZCBmcm9tICdkZXZleHRyZW1lL3VpL2RhdGFfZ3JpZCc7XHJcbmltcG9ydCB7IFNldHRpbmdFeHBvcnRPcHRpb25zIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IENHRlNldHRpbmdzRW51bSB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy9lbnVtcyc7XHJcbmltcG9ydCB7IGdldENsYXNzTmFtZUJ5VGhlbWVOYW1lIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMnO1xyXG5pbXBvcnQgeyBHcmlkQ29tcG9uZW50SW5zdGFuY2VzIH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbW1vbi1ncmlkLWZyYW1ld29yayc7XHJcbmltcG9ydCB7IFZhbHVlU2V0IH0gZnJvbSAnLi4vY29udHJhY3RzL3ZpZXctc2VsZWN0aW9uJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAncGJpLXNldHRpbmdzJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vc2V0dGluZ3MuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL3NldHRpbmdzLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNldHRpbmdzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBwdWJsaWMgY2dmU2V0dGluZ3NPcHRpb25zOiBBcnJheTxDR0ZTZXR0aW5nc0VudW0+ID0gW107XHJcbiAgQElucHV0KClcclxuICBzZXQgc2VsZWN0ZWRUaGVtZSh0aGVtZU5hbWU6IHN0cmluZykge1xyXG4gICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oJ3RoZW1lJykpIHtcclxuICAgICAgdGhpcy5jdXJyZW50QXBwbGllZFRoZW1lID0gc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgndGhlbWUnKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY3VycmVudEFwcGxpZWRUaGVtZSA9IHRoZW1lTmFtZTtcclxuICAgIH1cclxuICB9XHJcbiAgZ2V0IHNlbGVjdGVkVGhlbWUoKSB7IHJldHVybiB0aGlzLmN1cnJlbnRBcHBsaWVkVGhlbWU7IH1cclxuXHJcbiAgQElucHV0KClcclxuICBzZXQgc2VsZWN0ZWRCb3JkZXJTd2l0Y2godmFsOiBib29sZWFuKSB7XHJcbiAgICB0aGlzLmhpZGVHcmlkQm9yZGVyID0gdmFsO1xyXG4gIH1cclxuICBnZXQgc2VsZWN0ZWRCb3JkZXJTd2l0Y2goKSB7IHJldHVybiB0aGlzLmhpZGVHcmlkQm9yZGVyOyB9XHJcblxyXG4gIEBJbnB1dCgpIHB1YmxpYyBncmlkSW5zdGFuY2VMaXN0OiBBcnJheTxHcmlkQ29tcG9uZW50SW5zdGFuY2VzPjtcclxuICBAT3V0cHV0KCkgcHVibGljIGdyaWRTd2l0Y2hWYWx1ZUNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGNsb3NlQ3VycmVudEZseU91dCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBoaWRlR3JpZEJvcmRlciA9IGZhbHNlO1xyXG4gIHJhZGlvR3JvdXAgPSBbJ0F1dG8gRml0J107XHJcbiAgcmFkaW9Hcm91cFZhbHVlID0gbnVsbDtcclxuICBzaG93Rm9ybWF0dGluZ1NlY3Rpb24gPSB0cnVlO1xyXG4gIHRoZW1lTGlzdDogQXJyYXk8VmFsdWVTZXQ+ID0gW1xyXG4gICAgeyBrZXk6ICdsaWdodC5yZWd1bGFyJywgdmFsdWU6ICdDb3p5JyB9LFxyXG4gICAgeyBrZXk6ICdsaWdodC5jb21wYWN0JywgdmFsdWU6ICdSZWd1bGFyJyB9LFxyXG4gICAgeyBrZXk6ICducC5jb21wYWN0JywgdmFsdWU6ICdEZWZhdWx0JyB9XTtcclxuICBleHBvcnRBY3Rpb25MaXN0OiBBcnJheTxhbnk+ID0gW107XHJcbiAgY2dmU2V0dGluZ3NFbnVtID0gQ0dGU2V0dGluZ3NFbnVtO1xyXG4gIGN1cnJlbnRBcHBsaWVkVGhlbWUgPSAnbnAuY29tcGFjdCc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLCBwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7IH1cclxuXHJcbiAgcHVibGljIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5leHBvcnRBY3Rpb25MaXN0ID0gU2V0dGluZ0V4cG9ydE9wdGlvbnMuZmlsdGVyKG9wdGlvbiA9PiB0aGlzLmNnZlNldHRpbmdzT3B0aW9ucy5pbmRleE9mKG9wdGlvbi5pZCkgPiAtMSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25UaGVtZUNoYW5nZWQoKTogdm9pZCB7XHJcbiAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCd0aGVtZScsIHRoaXMuY3VycmVudEFwcGxpZWRUaGVtZSk7XHJcbiAgICBjb25zdCBhcHBsaWVkVGhlbWVOYW1lID0gZ2V0Q2xhc3NOYW1lQnlUaGVtZU5hbWUodGhpcy5jdXJyZW50QXBwbGllZFRoZW1lKTtcclxuICAgIHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICBsZXQgZXhpc3RpbmdDbGFzc2VzOiBzdHJpbmcgPSAoaXRlbS5ncmlkQ29tcG9uZW50SW5zdGFuY2UgYXMgYW55KS5fJGVsZW1lbnRbMF0uY2xhc3NOYW1lO1xyXG4gICAgICBpZiAoZXhpc3RpbmdDbGFzc2VzLmluZGV4T2YoJ2R4LXN3YXRjaC1kZWZhdWx0JykgPiAtMSkge1xyXG4gICAgICAgIGV4aXN0aW5nQ2xhc3NlcyA9IGV4aXN0aW5nQ2xhc3Nlcy5yZXBsYWNlKG5ldyBSZWdFeHAoJ2R4LXN3YXRjaC1kZWZhdWx0JywgJ2cnKSwgJycpO1xyXG4gICAgICB9IGVsc2UgaWYgKGV4aXN0aW5nQ2xhc3Nlcy5pbmRleE9mKCdkeC1zd2F0Y2gtcmVndWxhcicpID4gLTEpIHtcclxuICAgICAgICBleGlzdGluZ0NsYXNzZXMgPSBleGlzdGluZ0NsYXNzZXMucmVwbGFjZShuZXcgUmVnRXhwKCdkeC1zd2F0Y2gtcmVndWxhcicsICdnJyksICcnKTtcclxuICAgICAgfVxyXG4gICAgICAoaXRlbS5ncmlkQ29tcG9uZW50SW5zdGFuY2UgYXMgYW55KS5fJGVsZW1lbnRbMF0uY2xhc3NOYW1lID0gYCR7ZXhpc3RpbmdDbGFzc2VzfSAke2FwcGxpZWRUaGVtZU5hbWV9YDtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5lbWl0VG9nZ2xlVmFsdWUoJ3RoZW1lJywgdGhpcy5jdXJyZW50QXBwbGllZFRoZW1lKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkhpZGVHcmlkQ2hhbmdlKCk6IHZvaWQge1xyXG4gICAgdGhpcy5lbWl0VG9nZ2xlVmFsdWUoJ2dyaWRMaW5lcycsIHRoaXMuaGlkZUdyaWRCb3JkZXIpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uQ2xpY2tPZkRFTUJ1dHRvbigpOiB2b2lkIHtcclxuICAgIC8vIGNvbnN0IHBhcmFtcyA9IHtcclxuICAgIC8vICAgd2ViTWVudUlkOiBnZXRXZWJNZW51SWRCeUtleSgnRGF0YUVudGl0eU1hbmFnZW1lbnQnKSxcclxuICAgIC8vICAgZW50aXR5SWQ6IHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucXVlcnlQYXJhbXMuZW50aXR5LFxyXG4gICAgLy8gICBhY3RpdmVUYWI6ICdkYXRhYnJvd3NlcmVudGl0eSdcclxuICAgIC8vIH07XHJcbiAgICAvLyB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9kYXRhRW50aXR5TWFuYWdlbWVudCddLCB7IHF1ZXJ5UGFyYW1zOiBwYXJhbXMgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25BdXRvRml0Q2hhbmdlKCk6IHZvaWQge1xyXG4gICAgbGV0IGdyaWRJbnN0YW5jZU1heENvbDogR3JpZENvbXBvbmVudEluc3RhbmNlcyA9IHtcclxuICAgICAgZ3JpZENvbXBvbmVudEluc3RhbmNlOiBuZXcgT2JqZWN0KCkgYXMgZHhEYXRhR3JpZCxcclxuICAgICAgZ3JpZE5hbWU6ICcnLFxyXG4gICAgICBpc01hc3RlckdyaWQ6IGZhbHNlLFxyXG4gICAgICBpc1NlbGVjdGVkOiBmYWxzZVxyXG4gICAgfTtcclxuICAgIGxldCBleGlzdENvbENvdW50ID0gMDtcclxuICAgIHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWRJbnN0YW5jZSA9PiB7XHJcbiAgICAgIGNvbnN0IHZpc2libGVDb2xDb3VudCA9IGdyaWRJbnN0YW5jZS5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZ2V0VmlzaWJsZUNvbHVtbnMoKT8ubGVuZ3RoIHx8IDA7XHJcbiAgICAgIGlmIChleGlzdENvbENvdW50IDwgdmlzaWJsZUNvbENvdW50KSB7XHJcbiAgICAgICAgZ3JpZEluc3RhbmNlTWF4Q29sID0gZ3JpZEluc3RhbmNlO1xyXG4gICAgICAgIGV4aXN0Q29sQ291bnQgPSB2aXNpYmxlQ29sQ291bnQ7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGdyaWRJbnN0YW5jZU1heENvbC5ncmlkQ29tcG9uZW50SW5zdGFuY2UuYmVnaW5VcGRhdGUoKTtcclxuICAgIGNvbnN0IGNvbENvdW50ID0gZ3JpZEluc3RhbmNlTWF4Q29sLmdyaWRDb21wb25lbnRJbnN0YW5jZS5jb2x1bW5Db3VudCgpO1xyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjb2xDb3VudDsgaSsrKSB7XHJcbiAgICAgIGlmIChncmlkSW5zdGFuY2VNYXhDb2wuZ3JpZENvbXBvbmVudEluc3RhbmNlLmNvbHVtbk9wdGlvbihpLCAndmlzaWJsZScpKSB7XHJcbiAgICAgICAgZ3JpZEluc3RhbmNlTWF4Q29sLmdyaWRDb21wb25lbnRJbnN0YW5jZS5jb2x1bW5PcHRpb24oaSwgJ3dpZHRoJywgJ2F1dG8nKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgZ3JpZEluc3RhbmNlTWF4Q29sLmdyaWRDb21wb25lbnRJbnN0YW5jZS5lbmRVcGRhdGUoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZW1pdFRvZ2dsZVZhbHVlKGtleTogc3RyaW5nLCB2YWx1ZTogYm9vbGVhbiB8IHN0cmluZykge1xyXG4gICAgY29uc3QgX2RhdGE6IFZhbHVlU2V0ID0geyBrZXksIHZhbHVlIH07XHJcbiAgICB0aGlzLmdyaWRTd2l0Y2hWYWx1ZUNoYW5nZS5lbWl0KF9kYXRhKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbldvcmtGbG93RW1haWxDbGljaygpOiB2b2lkIHtcclxuICAgIC8vIGNvbnN0IHJlZGlyZWN0VVJMID1cclxuICAgIC8vICAgdGhpcy5jb25maWdTZXJ2aWNlLmNvbmZpZy5Xb3JrRmxvd01hc3RlckJhc2VVUkwgKyAnLyMvY3JlYXRvci9leHBvcnRyZXBvcnRpbmdwbGF0Zm9ybT91cmw9JyArIGVuY29kZVVSSUNvbXBvbmVudChsb2NhdGlvbi5ocmVmKTtcclxuICAgIC8vIG9wZW4ocmVkaXJlY3RVUkwsICdfYmxhbmsnKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRLZXkoKTogdm9pZCB7XHJcbiAgICAvLyB0aGlzLmNnZlNlcnZpY2UuZ2V0QXBpS2V5KCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHsgdGhpcy5nZW5lcmF0ZVVybFN1Y2Nlc3MocmVzcG9uc2UpOyB9LCAoKSA9PiB7XHJcbiAgICAvLyAgIGFwaUNhbGxFcnJvcignRXJyb3Igd2hpbGUgZ2VuZXJhdGluZyBrZXkuIFBsZWFzZSB0cnkgYWdhaW4uJyk7XHJcbiAgICAvLyB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2VuZXJhdGVVcmxTdWNjZXNzKHJlc3BvbnNlKTogdm9pZCB7XHJcbiAgICAvLyBpZiAocmVzcG9uc2UuVG9hc3RlclR5cGUudG9Mb3dlckNhc2UoKS50cmltKCkgPT09ICdzdWNjZXNzJykge1xyXG4gICAgLy8gICBjb25zdCB1cmxQYXJhbXMgPSBsb2NhdGlvbi5oYXNoLnNwbGl0KCc/JylbMV07XHJcbiAgICAvLyAgIGNvbnN0IHVybCA9IGAke0FQSUVuZFBvaW50cy5kYXRhQnJvd3Nlci5leGNlbERhdGF9PyR7dXJsUGFyYW1zfSZrZXk9JHtyZXNwb25zZS5NZXNzYWdlfWA7XHJcbiAgICAvLyAgIGNvcHlUZXh0VG9DbGlwQm9hcmQodXJsKTtcclxuICAgIC8vICAgYXBwVG9hc3RyKHsgdHlwZTogJ3N1Y2Nlc3MnLCBtZXNzYWdlOiAnTGluayBnZW5lcmF0ZWQgYW5kIGNvcGllZCBzdWNjZXNzZnVsbHkuJyB9KTtcclxuICAgIC8vIH0gZWxzZSBpZiAocmVzcG9uc2UuVG9hc3RlclR5cGUudG9Mb3dlckNhc2UoKS50cmltKCkgPT09ICdlcnJvcicpIHtcclxuICAgIC8vICAgYXBwVG9hc3RyKHsgdHlwZTogJ2Vycm9yJywgbWVzc2FnZTogcmVzcG9uc2UuTWVzc2FnZSB8fCAnRXJyb3Igd2hpbGUgZ2VuZXJhdGluZyBrZXkuIFBsZWFzZSB0cnkgYWdhaW4uJyB9KTtcclxuICAgIC8vIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBleHBvcnRTZWN0aW9uQnV0dG9uQ2xpY2soZGF0YSk6IHZvaWQge1xyXG4gICAgc3dpdGNoIChkYXRhLmtleSkge1xyXG4gICAgICBjYXNlICdleHBvcnRFeGNlbCc6XHJcbiAgICAgICAgY29uc3Qgc2VsZWN0ZWRJbnN0YW5jZSA9IHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5maWx0ZXIoaXRlbSA9PiBpdGVtLmlzU2VsZWN0ZWQpWzBdO1xyXG4gICAgICAgIGlmIChzZWxlY3RlZEluc3RhbmNlKSB7XHJcbiAgICAgICAgICAvLyBXaGVuIHRoZSBzZWxlY3Rpb25Pbmx5IHBhcmFtZXRlciBpcyBmYWxzZSAtIHRoZSBtZXRob2QgZXhwb3J0cyBhbGwgcm93cywgd2hlbiB0cnVlIC0gb25seSB0aGUgc2VsZWN0ZWQgb25lcy5cclxuICAgICAgICAgIHNlbGVjdGVkSW5zdGFuY2UuZ3JpZENvbXBvbmVudEluc3RhbmNlLmV4cG9ydFRvRXhjZWwoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnZXhjZWxMaW5rJzpcclxuICAgICAgICB0aGlzLmdldEtleSgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdlbWFpbFJlcG9ydCc6XHJcbiAgICAgICAgdGhpcy5vbldvcmtGbG93RW1haWxDbGljaygpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb3NlRmx5T3V0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jbG9zZUN1cnJlbnRGbHlPdXQuZW1pdCgpO1xyXG4gIH1cclxuXHJcbn1cclxuIl19