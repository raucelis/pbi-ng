import { __decorate } from "tslib";
import { CGFStorageKeys } from '../../utilities/enums';
import { CGFUtilityService } from '../../services/cgf-utility.service';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { appToast } from '../../utilities/utilityFunctions';
import { columnFormattingOptions, customActionColumnInfo } from '../../utilities/constants';
var ColorFormatComponent = /** @class */ (function () {
    function ColorFormatComponent(utilityService) {
        this.utilityService = utilityService;
        this.enableAlignment = true;
        this.enableBackgroundColor = true;
        this.enableFontStyle = true;
        this.enableFormat = true;
        this.enableFormatToggle = true;
        this.enableInputCaptionField = true;
        this.enableInputDataFieldCheck = false;
        this.enablePin = true;
        this.enableStorage = true;
        this.enableTextColor = true;
        this.gridInstanceList = [];
        this.isMasterGridSelected = true;
        this.showBasicFormat = false;
        this.storageKey = CGFStorageKeys[CGFStorageKeys.formatData];
        this.formatOptionsChanged = new EventEmitter();
        this.formatToggled = new EventEmitter();
        this.backgroundColor = '';
        this.basicFormatIconsAndOptionsData = columnFormattingOptions;
        this.captionValidationPattern = /^[a-zA-Z0-9_\s]+$/;
        this.columnsActivePropertiesTabsList = [{ title: 'FORMATTING' }];
        this.decimalCounter = 0;
        this.decimalTypes = ['comma', 'percent', 'currency'];
        this.disableDecimalPrecision = false;
        this.formatObject = {};
        this.isColorReadOnly = true;
        this.textColor = '';
    }
    Object.defineProperty(ColorFormatComponent.prototype, "disableDecimalPrecisionInput", {
        get: function () { return this.disableDecimalPrecision; },
        set: function (data) {
            this.disableDecimalPrecision = data;
            this.basicFormatIconsAndOptionsData = this.disableDecimalPrecision ? this.removePrecisionControls(columnFormattingOptions) : columnFormattingOptions;
            this.isColorReadOnly = !this.disableDecimalPrecision;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColorFormatComponent.prototype, "selectedColumnInfo", {
        get: function () { return this.selectedColumnData; },
        set: function (columnItem) {
            var _a;
            this.canUpdateColumnFormatting = false;
            this.selectedColumnData = columnItem || { caption: '', dataField: '' };
            if (((_a = this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField.length) > 0) {
                this.prepareSelectedOption();
            }
            else {
                this.resetAllFormatOptions();
            }
        },
        enumerable: true,
        configurable: true
    });
    ColorFormatComponent.prototype.ngOnInit = function () {
        this.resetBasicSettings();
        if (this.selectedColumnData.dataField && this.selectedColumnData.dataField.length > 0) {
            this.prepareSelectedOption();
        }
    };
    //#region Public Methods
    ColorFormatComponent.prototype.onCaptionValueChanged = function (args) {
        if (args.event) {
            this.canUpdateColumnFormatting = true;
        }
        this.formatObject.caption = this.selectedColumnInfo.caption || '';
        var data = {
            caption: this.selectedColumnInfo.caption || ''
        };
        if (data.caption.match(this.captionValidationPattern)) {
            this.formatOptionsChanged.emit(data);
            this.onFormatOptionsChanged();
        }
        // else {
        //   this.selectedColumnInfo.caption = args && args.previousValue ? args.previousValue : '';
        // }
    };
    ColorFormatComponent.prototype.onChangeOfTextColor = function () {
        this.formatObject.textColor = this.textColor || '';
        this.onFormatOptionsChanged();
    };
    ColorFormatComponent.prototype.onChangeOfBackgroundColor = function () {
        this.formatObject.backgroundColor = this.backgroundColor || '';
        this.onFormatOptionsChanged();
    };
    ColorFormatComponent.prototype.onColorFocusIn = function () {
        if (this.isColorReadOnly) {
            appToast({ type: 'warning', message: 'Column Format: Please select a column.' });
        }
        this.canUpdateColumnFormatting = true;
    };
    ColorFormatComponent.prototype.formatColumn = function (args, event) {
        if (this.selectedColumnData.dataField === customActionColumnInfo.dataField ||
            ((!this.selectedColumnData.dataField) || (this.selectedColumnData.dataField && this.selectedColumnData.dataField.length === 0))
                && this.enableInputDataFieldCheck) {
            appToast({ type: 'warning', message: 'Column Format: Please select a column.' });
            return;
        }
        if (args.type === 'right') {
            if (this.gridInstanceList.length) {
                this.gridInstanceList.forEach(function (grid) {
                    grid.columnOption(customActionColumnInfo.dataField, { fixed: true, fixedPosition: 'right' });
                });
            }
        }
        this.prepareLoaderLocation(event);
        args.isSelected = !args.isSelected;
        if (event) {
            this.canUpdateColumnFormatting = true;
        }
        if (!this.selectFormat(args)) {
            args.isSelected = !args.isSelected;
        }
        if (args.key.toLowerCase().indexOf('decimal') > -1) {
            args.isSelected = false; // this case makes sure remove/add decimal are not highlighted.
        }
    };
    ColorFormatComponent.prototype.onFormatOptionsChanged = function () {
        var _this = this;
        if (!this.canUpdateColumnFormatting) {
            return;
        }
        if (this.selectedColumnData) {
            setTimeout(function () {
                if (_this.gridInstanceList.length) {
                    _this.gridInstanceList.forEach(function (item) { return item.repaint(); });
                }
                document.querySelector('.loader').classList.add('display-none');
            }, 0);
            var formatDataCollection = this.getOrSetSessionData(this.storageKey, 'get');
            var index = void 0, dataUpdated = void 0;
            dataUpdated = false;
            this.formatObject.dataField = this.selectedColumnData.dataField;
            for (index = 0; index < formatDataCollection.length; index++) {
                if (formatDataCollection[index].dataField === this.selectedColumnData.dataField) {
                    formatDataCollection[index] = this.formatObject;
                    dataUpdated = true;
                    break;
                }
            }
            if (!dataUpdated) {
                formatDataCollection.push(this.formatObject);
            }
            this.getOrSetSessionData(this.storageKey, 'set', formatDataCollection);
        }
    };
    ColorFormatComponent.prototype.toggleBasicFormat = function () {
        this.showBasicFormat = !this.showBasicFormat;
        this.formatToggled.emit(this.showBasicFormat);
    };
    //#endregion Public Methods
    //#region Private Methods
    ColorFormatComponent.prototype.selectFormat = function (args) {
        var formatNotAllowed = false;
        this.formatObject.dataType = this.formatObject.dataType || this.selectedColumnData.dataType;
        switch (args.key) {
            case 'leftAlignment':
                this.setAlignmentFormat(args, 'left');
                break;
            case 'rightAlignment':
                this.setAlignmentFormat(args, 'right');
                break;
            case 'centerAlignment':
                this.setAlignmentFormat(args, 'center');
                break;
            case 'leftPin':
                this.setPinPosition(args, 'left');
                break;
            case 'rightPin':
                this.setPinPosition(args, 'right');
                break;
            case 'bold':
                this.prepareColumnFormatUsingClassName('cssClass', 'bold', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'underline':
                this.prepareColumnFormatUsingClassName('cssClass', 'underline', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'italic':
                this.prepareColumnFormatUsingClassName('cssClass', 'italic', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'currency':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'currency', args.isSelected);
                    this.decimalCounter = 0;
                    this.formatObject.format = args.isSelected ? { type: 'currency', precision: 2 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'currency', 'currency', 2);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'addDecimal':
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    if (this.formatObject.format) {
                        this.decimalCounter = this.formatObject.format.precision || 0;
                    }
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    this.decimalCounter++;
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, true);
                    this.formatObject.format = {
                        type: this.getDecimalType(),
                        precision: this.decimalCounter
                    };
                    this.onFormatOptionsChanged();
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'removeDecimal':
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    if (this.formatObject.format) {
                        this.decimalCounter = this.formatObject.format.precision || 0;
                    }
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    if (this.decimalCounter > 0) {
                        this.decimalCounter--;
                        this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, true);
                        this.formatObject.format = {
                            type: this.getDecimalType(),
                            precision: this.decimalCounter
                        };
                    }
                    else if (this.decimalCounter === 0) {
                        this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    }
                    this.onFormatOptionsChanged();
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'percentage':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'percentage', args.isSelected);
                    this.formatObject.format = args.isSelected ? { type: 'percent', precision: 2 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'percent', 'percentage', 2);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'comma':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.decimalCounter = 0;
                    this.prepareColumnFormatUsingClassName('cssClass', 'comma', args.isSelected);
                    this.formatObject.format = args.isSelected ? { type: 'comma', precision: 0 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'comma', 'comma', 0);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'calendar':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType.indexOf('date') > -1 || !this.enableInputDataFieldCheck) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'calendar', args.isSelected);
                    this.formatObject.format = args.isSelected ? 'shortDate' : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.formatObject.format = args.isSelected ? 'shortDate' : '';
                    break;
                }
                formatNotAllowed = true;
                break;
        }
        if (!this.enableInputCaptionField) {
            setTimeout(function () {
                document.querySelector('.loader').classList.add('display-none');
            }, 0);
        }
        if (formatNotAllowed) {
            appToast({ type: 'warning', message: 'Format can\'t be applied on the selected column.' });
            document.querySelector('.loader').classList.add('display-none');
            return false;
        }
        return true;
    };
    ColorFormatComponent.prototype.setAlignmentFormat = function (args, alignmentType) {
        this.resetAlignmentExceptForCurrent(args.type);
        this.formatObject.alignment = args.isSelected ? alignmentType : '';
        if (this.enableInputCaptionField) {
            this.onFormatOptionsChanged();
        }
    };
    ColorFormatComponent.prototype.setPinPosition = function (args, position) {
        var _this = this;
        this.resetPinExceptForCurrent(args.type);
        this.formatObject.fixed = args.isSelected;
        this.formatObject.fixedPosition = args.isSelected ? position : '';
        if (this.gridInstanceList.length) {
            this.gridInstanceList.forEach(function (grid) {
                grid.columnOption(_this.selectedColumnData.dataField, { fixed: args.isSelected, fixedPosition: _this.formatObject.fixedPosition });
            });
        }
        if (this.enableInputCaptionField) {
            this.onFormatOptionsChanged();
        }
    };
    ColorFormatComponent.prototype.columnFormatChangeForDataDictionary = function (args, type, formatClass, precision) {
        this.prepareColumnFormatUsingClassName('cssClass', formatClass, args.isSelected);
        this.formatObject.format = args.isSelected ? { type: type, precision: precision } : '';
    };
    ColorFormatComponent.prototype.resetAlignmentExceptForCurrent = function (type) {
        if (type === void 0) { type = ''; }
        for (var i = 0; i < this.basicFormatIconsAndOptionsData.alignment.length; i++) {
            if (this.basicFormatIconsAndOptionsData.alignment[i].type !== type) {
                this.basicFormatIconsAndOptionsData.alignment[i].isSelected = false;
            }
        }
    };
    ColorFormatComponent.prototype.resetPinExceptForCurrent = function (type) {
        if (type === void 0) { type = ''; }
        this.basicFormatIconsAndOptionsData.pin.forEach(function (item) {
            if (item.type !== type) {
                item.isSelected = false;
            }
        });
    };
    ColorFormatComponent.prototype.prepareColumnFormatUsingClassName = function (formatType, formatValue, addFormat) {
        var listOfExistingFormats = this.formatObject[formatType] || '';
        if (addFormat) {
            if (listOfExistingFormats && listOfExistingFormats.length) {
                var formatGroupData = ['currency', 'percentage', 'comma', 'calendar'];
                if (formatGroupData.indexOf(formatValue) > -1) {
                    for (var formatIndex = 0; formatIndex < formatGroupData.length; formatIndex++) {
                        listOfExistingFormats = listOfExistingFormats.replace(formatGroupData[formatIndex], '');
                    }
                }
                listOfExistingFormats = listOfExistingFormats.replace(new RegExp(formatValue, 'g'), '').trim();
                listOfExistingFormats = listOfExistingFormats + ' ' + formatValue;
            }
            else {
                listOfExistingFormats = formatValue;
            }
        }
        else if (listOfExistingFormats && listOfExistingFormats.indexOf(formatValue) > -1) {
            listOfExistingFormats = listOfExistingFormats.replace(new RegExp(formatValue, 'g'), '').trim();
        }
        this.formatObject[formatType] = listOfExistingFormats;
        var _data = {};
        if (formatType === 'cssClass') {
            _data[formatType] = listOfExistingFormats;
        }
        else {
            _data[formatType] = addFormat ? formatValue : listOfExistingFormats;
        }
        return _data;
    };
    ColorFormatComponent.prototype.resetSelectedFormat = function (type) {
        if (type === void 0) { type = ''; }
        var listOfClasses = this.formatObject.cssClass;
        if (listOfClasses) {
            listOfClasses = listOfClasses.replace(/percentage/g, '').replace(/comma/g, '').replace(/currency/g, '');
            this.formatObject.cssClass = listOfClasses;
        }
        for (var i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
            if (this.basicFormatIconsAndOptionsData.format[i].type !== type) {
                this.basicFormatIconsAndOptionsData.format[i].isSelected = false;
            }
        }
    };
    ColorFormatComponent.prototype.resetBasicSettings = function () {
        var i;
        for (i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
            this.basicFormatIconsAndOptionsData.format[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.alignment.length; i++) {
            this.basicFormatIconsAndOptionsData.alignment[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.pin.length; i++) {
            this.basicFormatIconsAndOptionsData.pin[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.fontStyle.length; i++) {
            this.basicFormatIconsAndOptionsData.fontStyle[i].isSelected = false;
        }
        this.textColor = '';
        this.backgroundColor = '';
        this.formatObject = {};
        this.decimalCounter = 0;
    };
    ColorFormatComponent.prototype.prepareSelectedOption = function () {
        var _this = this;
        this.resetBasicSettings();
        var selectedColumnDef = new Object();
        if (this.enableStorage) {
            var columnFormatCollection = this.getOrSetSessionData(this.storageKey, 'get');
            var baseFormatCollection = this.isMasterGridSelected ?
                JSON.parse(sessionStorage.getItem(CGFStorageKeys[CGFStorageKeys.dictionaryFormatData])) || [] : [];
            selectedColumnDef = columnFormatCollection.find(function (arrItem) { return arrItem.dataField === _this.selectedColumnData.dataField; }) ||
                baseFormatCollection.find(function (arrItem) { return arrItem.dataField === _this.selectedColumnData.dataField; }) || {};
        }
        if (!Object.keys(selectedColumnDef).length && this.selectedColumnData) {
            selectedColumnDef = this.selectedColumnData;
        }
        if (selectedColumnDef) {
            if (selectedColumnDef.cssClass) {
                var classList = selectedColumnDef.cssClass.split(' ');
                var i_1;
                classList.forEach(function (cssClass) {
                    if (cssClass === 'bold' || cssClass === 'underline' || cssClass === 'italic') {
                        for (i_1 = 0; i_1 < _this.basicFormatIconsAndOptionsData.fontStyle.length; i_1++) {
                            if (_this.basicFormatIconsAndOptionsData.fontStyle[i_1].type === cssClass) {
                                _this.basicFormatIconsAndOptionsData.fontStyle[i_1].isSelected = true;
                                _this.formatObject.cssClass = _this.formatObject.cssClass ? _this.formatObject.cssClass + ' ' + cssClass : ' ' + cssClass;
                                break;
                            }
                        }
                    }
                    if (cssClass === 'currency' || cssClass === 'percentage' || cssClass === 'comma' || cssClass === 'calendar') {
                        for (i_1 = 0; i_1 < _this.basicFormatIconsAndOptionsData.format.length; i_1++) {
                            if (_this.basicFormatIconsAndOptionsData.format[i_1].type === cssClass) {
                                _this.basicFormatIconsAndOptionsData.format[i_1].isSelected = true;
                                _this.decimalCounter = selectedColumnDef.format ? selectedColumnDef.format.precision : 0;
                                _this.formatObject.cssClass = _this.formatObject.cssClass ? _this.formatObject.cssClass + ' ' + cssClass : ' ' + cssClass;
                                break;
                            }
                        }
                    }
                    if (cssClass.indexOf('decimal') > -1) {
                        _this.decimalCounter = selectedColumnDef.format.precision;
                    }
                });
            }
            if (selectedColumnDef.fixed) {
                var isPositionLeft = selectedColumnDef.fixedPosition === 'left' ? true : false;
                for (var ind = 0; ind < this.basicFormatIconsAndOptionsData.pin.length; ind++) {
                    if (isPositionLeft && this.basicFormatIconsAndOptionsData.pin[ind].key === 'leftPin') {
                        this.basicFormatIconsAndOptionsData.pin[ind].isSelected = true;
                        break;
                    }
                    else if (this.basicFormatIconsAndOptionsData.pin[ind].key === 'rightPin') {
                        this.basicFormatIconsAndOptionsData.pin[ind].isSelected = true;
                        break;
                    }
                }
            }
            var j = void 0;
            for (j = 0; j < this.basicFormatIconsAndOptionsData.alignment.length; j++) {
                if (this.basicFormatIconsAndOptionsData.alignment[j].type === selectedColumnDef.alignment) {
                    this.basicFormatIconsAndOptionsData.alignment[j].isSelected = true;
                }
            }
            for (j = 0; j < this.basicFormatIconsAndOptionsData.pin.length; j++) {
                if (this.basicFormatIconsAndOptionsData.pin[j].type === selectedColumnDef.fixedPosition) {
                    this.basicFormatIconsAndOptionsData.pin[j].isSelected = true;
                }
            }
            this.textColor = selectedColumnDef.textColor || '';
            this.backgroundColor = selectedColumnDef.backgroundColor || '';
            this.formatObject = selectedColumnDef;
        }
        this.isColorReadOnly = !this.selectedColumnData;
    };
    ColorFormatComponent.prototype.removePrecisionControls = function (data) {
        var columnFormatData = Object.assign({}, data);
        var formatData = columnFormatData.format.filter(function (item) {
            if (item.key !== 'removeDecimal' && item.key !== 'addDecimal') {
                return item;
            }
        });
        columnFormatData.format = formatData;
        return columnFormatData;
    };
    ColorFormatComponent.prototype.getDecimalType = function () {
        var _this = this;
        var decimalFormatType = 'fixedPoint';
        if (this.formatObject.format) {
            decimalFormatType = this.decimalTypes.some(function (t) { return t === _this.formatObject.format.type; })
                ? this.formatObject.format.type : 'fixedPoint';
        }
        return decimalFormatType;
    };
    ColorFormatComponent.prototype.getOrSetSessionData = function (key, type, data) {
        if (data === void 0) { data = null; }
        if (!this.enableStorage) {
            return [];
        }
        key = this.gridInstanceList.length ? this.utilityService.getStorageKey(this.gridInstanceList[0], key, this.isMasterGridSelected) : key;
        switch (type) {
            case 'get':
                return JSON.parse(sessionStorage.getItem(key)) || [];
            case 'set':
                sessionStorage.setItem(key, JSON.stringify(data));
                break;
        }
        return [];
    };
    ColorFormatComponent.prototype.resetAllFormatOptions = function () {
        this.resetBasicSettings();
        this.resetPinExceptForCurrent();
        this.resetSelectedFormat();
        this.resetAlignmentExceptForCurrent();
    };
    ColorFormatComponent.prototype.prepareLoaderLocation = function (event) {
        var cord = event.currentTarget.getBoundingClientRect();
        var leftCords = cord.left + 4 + 'px';
        var topCords = cord.top + 4 + 'px';
        var firstChild = document.querySelector('.loader');
        firstChild.classList.remove('display-none');
        firstChild.style.top = topCords;
        firstChild.style.left = leftCords;
    };
    ColorFormatComponent.ctorParameters = function () { return [
        { type: CGFUtilityService }
    ]; };
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableAlignment", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableBackgroundColor", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableFontStyle", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableFormat", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableFormatToggle", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableInputCaptionField", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableInputDataFieldCheck", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enablePin", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableStorage", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "enableTextColor", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "gridInstanceList", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "isMasterGridSelected", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "showBasicFormat", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "storageKey", void 0);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "disableDecimalPrecisionInput", null);
    __decorate([
        Input()
    ], ColorFormatComponent.prototype, "selectedColumnInfo", null);
    __decorate([
        Output()
    ], ColorFormatComponent.prototype, "formatOptionsChanged", void 0);
    __decorate([
        Output()
    ], ColorFormatComponent.prototype, "formatToggled", void 0);
    ColorFormatComponent = __decorate([
        Component({
            selector: 'pbi-color-format',
            template: "<div id=\"loader-container\">\r\n  <div class=\"loader display-none\"></div>\r\n</div>\r\n\r\n<div>\r\n  <div class=\"active-properties-tab\">\r\n    <ul>\r\n      <li *ngFor=\"let tabs of columnsActivePropertiesTabsList\"\r\n        [class.property-tab-in-column-chooser]=\"enableFormatToggle\"\r\n        [class.property-tab-in-data-dictionary]=\"!enableFormatToggle\"\r\n        (click)=\"enableFormatToggle && toggleBasicFormat()\">{{tabs.title}}\r\n        <span *ngIf=\"enableFormatToggle\" [class.fa-angle-down]=\"!showBasicFormat\" [class.fa-angle-up]=\"showBasicFormat\"\r\n          class=\"fa float-right basic-format-toggle-icon\"></span>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <div class=\"active-properties-basic-tab-content\" *ngIf=\"showBasicFormat || !enableFormatToggle\">\r\n    <div class=\"format-controls-container\">\r\n      <!-- <div *ngIf=\"enableInputDataFieldCheck\">\r\n                <div>Column ID: <span class=\"bold\">{{selectedColumnInfo.dataField}}</span></div>\r\n            </div> -->\r\n      <div *ngIf=\"enableInputCaptionField\">\r\n        <div>Caption:</div>\r\n        <div class=\"caption-font-style\">\r\n          <dx-text-box class=\"caption-input\" [(value)]=\"selectedColumnInfo.caption\"\r\n            (onValueChanged)=\"onCaptionValueChanged($event)\" maxLength=\"40\">\r\n            <dx-validator>\r\n              <dxi-validation-rule type=\"pattern\" [pattern]=\"captionValidationPattern\"\r\n                message=\"Caption will contain only alphabets(a-z,A-Z)/numbers(0-9)/underscore(_)/space.\">\r\n              </dxi-validation-rule>\r\n            </dx-validator>\r\n          </dx-text-box>\r\n        </div>\r\n      </div>\r\n      <div class=\"format-alignments-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enableFormat\">\r\n          <div>Format:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.format\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableAlignment\">\r\n          <div>Alignment:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.alignment\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"pin-font-style-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enablePin\">\r\n          <div>Pin:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.pin\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableFontStyle\">\r\n          <div>Font Style:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.fontStyle\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"text-font-style-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enableTextColor\">\r\n          <div>Text Color:</div>\r\n          <div class=\"text-margin-style\">\r\n            <dx-color-box [(value)]=\"textColor\" [readOnly]=\"isColorReadOnly\" (onFocusIn)=\"onColorFocusIn()\"\r\n              (onValueChanged)=\"onChangeOfTextColor()\">\r\n            </dx-color-box>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableBackgroundColor\">\r\n          <div>Background Color:</div>\r\n          <div class=\"background-margin-style\">\r\n            <dx-color-box [(value)]=\"backgroundColor\" [readOnly]=\"isColorReadOnly\" (onFocusIn)=\"onColorFocusIn()\"\r\n              (onValueChanged)=\"onChangeOfBackgroundColor()\">\r\n            </dx-color-box>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n",
            styles: [""]
        })
    ], ColorFormatComponent);
    return ColorFormatComponent;
}());
export { ColorFormatComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sb3ItZm9ybWF0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tcG9uZW50cy9jb2xvci1mb3JtYXQvY29sb3ItZm9ybWF0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBRXZFLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzVELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBTzVGO0lBc0RFLDhCQUE2QixjQUFpQztRQUFqQyxtQkFBYyxHQUFkLGNBQWMsQ0FBbUI7UUFwRDlDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLDBCQUFxQixHQUFHLElBQUksQ0FBQztRQUM3QixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQix1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDMUIsNEJBQXVCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLDhCQUF5QixHQUFHLEtBQUssQ0FBQztRQUNsQyxjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUN0Qix5QkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDNUIsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsZUFBVSxHQUFXLGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUF1QjlELHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDMUMsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBRTdELG9CQUFlLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLG1DQUE4QixHQUFHLHVCQUF1QixDQUFDO1FBRXpELDZCQUF3QixHQUFHLG1CQUFtQixDQUFDO1FBQy9DLG9DQUErQixHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztRQUM1RCxtQkFBYyxHQUFHLENBQUMsQ0FBQztRQUNuQixpQkFBWSxHQUFhLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUMxRCw0QkFBdUIsR0FBRyxLQUFLLENBQUM7UUFDaEMsaUJBQVksR0FBaUIsRUFBRSxDQUFDO1FBQ2hDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBRXZCLGNBQVMsR0FBRyxFQUFFLENBQUM7SUFFbUQsQ0FBQztJQXBDbkUsc0JBQUksOERBQTRCO2FBS2hDLGNBQThDLE9BQU8sSUFBSSxDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQzthQUxwRixVQUFpQyxJQUFhO1lBQzVDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7WUFDcEMsSUFBSSxDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLHVCQUF1QixDQUFDO1lBQ3JKLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUM7UUFDdkQsQ0FBQzs7O09BQUE7SUFJRCxzQkFBSSxvREFBa0I7YUFTdEIsY0FBMkIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO2FBVDVELFVBQXVCLFVBQXlCOztZQUM5QyxJQUFJLENBQUMseUJBQXlCLEdBQUcsS0FBSyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLElBQUksRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxFQUFFLEVBQUUsQ0FBQztZQUN2RSxJQUFJLE9BQUEsSUFBSSxDQUFDLGtCQUFrQiwwQ0FBRSxTQUFTLENBQUMsTUFBTSxJQUFHLENBQUMsRUFBRTtnQkFDakQsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7YUFDOUI7UUFDSCxDQUFDOzs7T0FBQTtJQXNCRCx1Q0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDMUIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNyRixJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztTQUM5QjtJQUNILENBQUM7SUFFRCx3QkFBd0I7SUFFakIsb0RBQXFCLEdBQTVCLFVBQTZCLElBQXlCO1FBQ3BELElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNkLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUNsRSxJQUFNLElBQUksR0FBRztZQUNYLE9BQU8sRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxJQUFJLEVBQUU7U0FDL0MsQ0FBQztRQUNGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEVBQUU7WUFDckQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUMvQjtRQUNELFNBQVM7UUFDVCw0RkFBNEY7UUFDNUYsSUFBSTtJQUVOLENBQUM7SUFFTSxrREFBbUIsR0FBMUI7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQztRQUNuRCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRU0sd0RBQXlCLEdBQWhDO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUM7UUFDL0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVNLDZDQUFjLEdBQXJCO1FBQ0UsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hCLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLENBQUMsQ0FBQztTQUNsRjtRQUNELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUM7SUFDeEMsQ0FBQztJQUVNLDJDQUFZLEdBQW5CLFVBQW9CLElBQTBCLEVBQUUsS0FBaUI7UUFDL0QsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxLQUFLLHNCQUFzQixDQUFDLFNBQVM7WUFDeEUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQzttQkFDNUgsSUFBSSxDQUFDLHlCQUF5QixFQUFFO1lBQ25DLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLENBQUMsQ0FBQztZQUNqRixPQUFPO1NBQ1I7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTyxFQUFFO1lBQ3pCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtnQkFDaEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQ2hDLElBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxhQUFhLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQztnQkFDL0YsQ0FBQyxDQUFDLENBQUM7YUFDSjtTQUNGO1FBQ0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBRW5DLElBQUksS0FBSyxFQUFFO1lBQ1QsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzVCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNsRCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxDQUFFLCtEQUErRDtTQUMxRjtJQUNILENBQUM7SUFFTSxxREFBc0IsR0FBN0I7UUFBQSxpQkE0QkM7UUEzQkMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtZQUNuQyxPQUFPO1NBQ1I7UUFDRCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUMzQixVQUFVLENBQUM7Z0JBQ1QsSUFBSSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO29CQUNoQyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFkLENBQWMsQ0FBQyxDQUFDO2lCQUN2RDtnQkFDRCxRQUFRLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDbEUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ04sSUFBTSxvQkFBb0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUM5RSxJQUFJLEtBQUssU0FBUSxFQUFFLFdBQVcsU0FBUyxDQUFDO1lBQ3hDLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFDcEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQztZQUNoRSxLQUFLLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDNUQsSUFBSSxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsRUFBRTtvQkFDL0Usb0JBQW9CLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDaEQsV0FBVyxHQUFHLElBQUksQ0FBQztvQkFDbkIsTUFBTTtpQkFDUDthQUNGO1lBQ0QsSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDaEIsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUM5QztZQUVELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEtBQUssRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1NBQ3hFO0lBQ0gsQ0FBQztJQUVNLGdEQUFpQixHQUF4QjtRQUNFLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsMkJBQTJCO0lBRTNCLHlCQUF5QjtJQUVqQiwyQ0FBWSxHQUFwQixVQUFxQixJQUEwQjtRQUM3QyxJQUFJLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDO1FBQzVGLFFBQVEsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNoQixLQUFLLGVBQWU7Z0JBQ2xCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ3RDLE1BQU07WUFDUixLQUFLLGdCQUFnQjtnQkFDbkIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDdkMsTUFBTTtZQUNSLEtBQUssaUJBQWlCO2dCQUNwQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUN4QyxNQUFNO1lBQ1IsS0FBSyxTQUFTO2dCQUNaLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNsQyxNQUFNO1lBRVIsS0FBSyxVQUFVO2dCQUNiLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUNuQyxNQUFNO1lBQ1IsS0FBSyxNQUFNO2dCQUNULElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxVQUFVLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDNUUsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQzlCLE1BQU07WUFDUixLQUFLLFdBQVc7Z0JBQ2QsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNqRixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUIsTUFBTTtZQUNSLEtBQUssUUFBUTtnQkFDWCxJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzlFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5QixNQUFNO1lBQ1IsS0FBSyxVQUFVO2dCQUNiLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDL0YsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNoRixJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztvQkFDeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUNyRixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsTUFBTTtpQkFDUDtxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFO29CQUN4QyxJQUFJLENBQUMsbUNBQW1DLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzFFLE1BQU07aUJBQ1A7Z0JBQ0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDL0YsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTt3QkFDNUIsSUFBSSxDQUFDLGNBQWMsR0FBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQW1DLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQztxQkFDN0Y7b0JBQ0QsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDakcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUN0QixJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLGVBQWUsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUNoRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRzt3QkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUU7d0JBQzNCLFNBQVMsRUFBRSxJQUFJLENBQUMsY0FBYztxQkFDL0IsQ0FBQztvQkFDRixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsTUFBTTtpQkFDUDtnQkFDRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLGVBQWU7Z0JBQ2xCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDL0YsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTt3QkFDNUIsSUFBSSxDQUFDLGNBQWMsR0FBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQW1DLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQztxQkFDN0Y7b0JBRUQsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDakcsSUFBSSxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsRUFBRTt3QkFDM0IsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO3dCQUN0QixJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLGVBQWUsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUNoRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRzs0QkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUU7NEJBQzNCLFNBQVMsRUFBRSxJQUFJLENBQUMsY0FBYzt5QkFDL0IsQ0FBQztxQkFDSDt5QkFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLEtBQUssQ0FBQyxFQUFFO3dCQUNwQyxJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLGVBQWUsR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFDO3FCQUNsRztvQkFDRCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsTUFBTTtpQkFDUDtnQkFDRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLFlBQVk7Z0JBQ2YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUMvRixJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLFlBQVksRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ2xGLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDcEYsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLE1BQU07aUJBQ1A7cUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtvQkFDeEMsSUFBSSxDQUFDLG1DQUFtQyxDQUFDLElBQUksRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUMzRSxNQUFNO2lCQUNQO2dCQUNELGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDeEIsTUFBTTtZQUNSLEtBQUssT0FBTztnQkFDVixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQy9GLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDO29CQUN4QixJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQzdFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDbEYsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLE1BQU07aUJBQ1A7cUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtvQkFDeEMsSUFBSSxDQUFDLG1DQUFtQyxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNwRSxNQUFNO2lCQUNQO2dCQUNELGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDeEIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFO29CQUM1RixJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ2hGLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUM5RCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztvQkFDOUIsTUFBTTtpQkFDUDtxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFO29CQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDOUQsTUFBTTtpQkFDUDtnQkFDRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLE1BQU07U0FDVDtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDakMsVUFBVSxDQUFDO2dCQUNULFFBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNsRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDUDtRQUNELElBQUksZ0JBQWdCLEVBQUU7WUFDcEIsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsa0RBQWtELEVBQUUsQ0FBQyxDQUFDO1lBQzNGLFFBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNoRSxPQUFPLEtBQUssQ0FBQztTQUNkO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRU8saURBQWtCLEdBQTFCLFVBQTJCLElBQTBCLEVBQUUsYUFBcUI7UUFDMUUsSUFBSSxDQUFDLDhCQUE4QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNuRSxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtZQUNoQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUMvQjtJQUNILENBQUM7SUFFTyw2Q0FBYyxHQUF0QixVQUF1QixJQUEwQixFQUFFLFFBQWdCO1FBQW5FLGlCQWFDO1FBWkMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzFDLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ2xFLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtZQUNoQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQkFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxFQUNqRCxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLGFBQWEsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7WUFDaEYsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBQ2hDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1NBQy9CO0lBQ0gsQ0FBQztJQUVPLGtFQUFtQyxHQUEzQyxVQUE0QyxJQUEwQixFQUFFLElBQVksRUFBRSxXQUFtQixFQUFFLFNBQWtCO1FBQzNILElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxVQUFVLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNqRixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksTUFBQSxFQUFFLFNBQVMsV0FBQSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN4RSxDQUFDO0lBRU8sNkRBQThCLEdBQXRDLFVBQXVDLElBQVM7UUFBVCxxQkFBQSxFQUFBLFNBQVM7UUFDOUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdFLElBQUksSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFFO2dCQUNsRSxJQUFJLENBQUMsOEJBQThCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7YUFDckU7U0FDRjtJQUNILENBQUM7SUFFTyx1REFBd0IsR0FBaEMsVUFBaUMsSUFBUztRQUFULHFCQUFBLEVBQUEsU0FBUztRQUN4QyxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDbEQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksRUFBRTtnQkFDdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7YUFDekI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFTyxnRUFBaUMsR0FBekMsVUFBMEMsVUFBa0IsRUFBRSxXQUFtQixFQUFFLFNBQWtCO1FBQ25HLElBQUkscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEUsSUFBSSxTQUFTLEVBQUU7WUFDYixJQUFJLHFCQUFxQixJQUFJLHFCQUFxQixDQUFDLE1BQU0sRUFBRTtnQkFDekQsSUFBTSxlQUFlLEdBQUcsQ0FBQyxVQUFVLEVBQUUsWUFBWSxFQUFFLE9BQU8sRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxlQUFlLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUM3QyxLQUFLLElBQUksV0FBVyxHQUFHLENBQUMsRUFBRSxXQUFXLEdBQUcsZUFBZSxDQUFDLE1BQU0sRUFBRSxXQUFXLEVBQUUsRUFBRTt3QkFDN0UscUJBQXFCLEdBQUcscUJBQXFCLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztxQkFDekY7aUJBQ0Y7Z0JBQ0QscUJBQXFCLEdBQUcscUJBQXFCLENBQUMsT0FBTyxDQUFDLElBQUksTUFBTSxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDL0YscUJBQXFCLEdBQUcscUJBQXFCLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQzthQUNuRTtpQkFBTTtnQkFDTCxxQkFBcUIsR0FBRyxXQUFXLENBQUM7YUFDckM7U0FDRjthQUFNLElBQUkscUJBQXFCLElBQUkscUJBQXFCLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ25GLHFCQUFxQixHQUFHLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDaEc7UUFFRCxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxHQUFHLHFCQUFxQixDQUFDO1FBQ3RELElBQU0sS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFJLFVBQVUsS0FBSyxVQUFVLEVBQUU7WUFDN0IsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLHFCQUFxQixDQUFDO1NBQzNDO2FBQU07WUFDTCxLQUFLLENBQUMsVUFBVSxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDO1NBQ3JFO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRU8sa0RBQW1CLEdBQTNCLFVBQTRCLElBQVM7UUFBVCxxQkFBQSxFQUFBLFNBQVM7UUFDbkMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7UUFDL0MsSUFBSSxhQUFhLEVBQUU7WUFDakIsYUFBYSxHQUFHLGFBQWEsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUN4RyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUM7U0FDNUM7UUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDMUUsSUFBSSxJQUFJLENBQUMsOEJBQThCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLEVBQUU7Z0JBQy9ELElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzthQUNsRTtTQUNGO0lBQ0gsQ0FBQztJQUVPLGlEQUFrQixHQUExQjtRQUNFLElBQUksQ0FBUyxDQUFDO1FBQ2QsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsOEJBQThCLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN0RSxJQUFJLENBQUMsOEJBQThCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDbEU7UUFFRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pFLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztTQUNyRTtRQUVELEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLDhCQUE4QixDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDbkUsSUFBSSxDQUFDLDhCQUE4QixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQy9EO1FBRUQsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsOEJBQThCLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN6RSxJQUFJLENBQUMsOEJBQThCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDckU7UUFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztJQUMxQixDQUFDO0lBRU8sb0RBQXFCLEdBQTdCO1FBQUEsaUJBNEVDO1FBM0VDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksaUJBQWlCLEdBQWlCLElBQUksTUFBTSxFQUFrQixDQUFDO1FBQ25FLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN0QixJQUFNLHNCQUFzQixHQUFtQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNoRyxJQUFNLG9CQUFvQixHQUFtQixJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDckcsaUJBQWlCLEdBQUcsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLFNBQVMsS0FBSyxLQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxFQUF2RCxDQUF1RCxDQUFDO2dCQUNqSCxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxPQUFPLENBQUMsU0FBUyxLQUFLLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQXZELENBQXVELENBQUMsSUFBSSxFQUFFLENBQUM7U0FDdkc7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDckUsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGtCQUF5QixDQUFDO1NBQ3BEO1FBQ0QsSUFBSSxpQkFBaUIsRUFBRTtZQUNyQixJQUFJLGlCQUFpQixDQUFDLFFBQVEsRUFBRTtnQkFDOUIsSUFBTSxTQUFTLEdBQUcsaUJBQWlCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxHQUFTLENBQUM7Z0JBQ2QsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQWdCO29CQUNqQyxJQUFJLFFBQVEsS0FBSyxNQUFNLElBQUksUUFBUSxLQUFLLFdBQVcsSUFBSSxRQUFRLEtBQUssUUFBUSxFQUFFO3dCQUM1RSxLQUFLLEdBQUMsR0FBRyxDQUFDLEVBQUUsR0FBQyxHQUFHLEtBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLEdBQUMsRUFBRSxFQUFFOzRCQUN6RSxJQUFJLEtBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsR0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtnQ0FDdEUsS0FBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dDQUNuRSxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztnQ0FDdkgsTUFBTTs2QkFDUDt5QkFDRjtxQkFDRjtvQkFFRCxJQUFJLFFBQVEsS0FBSyxVQUFVLElBQUksUUFBUSxLQUFLLFlBQVksSUFBSSxRQUFRLEtBQUssT0FBTyxJQUFJLFFBQVEsS0FBSyxVQUFVLEVBQUU7d0JBQzNHLEtBQUssR0FBQyxHQUFHLENBQUMsRUFBRSxHQUFDLEdBQUcsS0FBSSxDQUFDLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsR0FBQyxFQUFFLEVBQUU7NEJBQ3RFLElBQUksS0FBSSxDQUFDLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxHQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO2dDQUNuRSxLQUFJLENBQUMsOEJBQThCLENBQUMsTUFBTSxDQUFDLEdBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0NBQ2hFLEtBQUksQ0FBQyxjQUFjLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBRSxpQkFBaUIsQ0FBQyxNQUFtQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUN0SCxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztnQ0FDdkgsTUFBTTs2QkFDUDt5QkFDRjtxQkFDRjtvQkFFRCxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7d0JBQ3BDLEtBQUksQ0FBQyxjQUFjLEdBQUksaUJBQWlCLENBQUMsTUFBbUMsQ0FBQyxTQUFTLENBQUM7cUJBQ3hGO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFFRCxJQUFJLGlCQUFpQixDQUFDLEtBQUssRUFBRTtnQkFDM0IsSUFBTSxjQUFjLEdBQUcsaUJBQWlCLENBQUMsYUFBYSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ2pGLEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtvQkFDN0UsSUFBSSxjQUFjLElBQUksSUFBSSxDQUFDLDhCQUE4QixDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssU0FBUyxFQUFFO3dCQUNwRixJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7d0JBQy9ELE1BQU07cUJBQ1A7eUJBQU0sSUFBSSxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxVQUFVLEVBQUU7d0JBQzFFLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzt3QkFDL0QsTUFBTTtxQkFDUDtpQkFDRjthQUNGO1lBRUQsSUFBSSxDQUFDLFNBQVEsQ0FBQztZQUNkLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pFLElBQUksSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssaUJBQWlCLENBQUMsU0FBUyxFQUFFO29CQUN6RixJQUFJLENBQUMsOEJBQThCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7aUJBQ3BFO2FBQ0Y7WUFFRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNuRSxJQUFJLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGlCQUFpQixDQUFDLGFBQWEsRUFBRTtvQkFDdkYsSUFBSSxDQUFDLDhCQUE4QixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2lCQUM5RDthQUNGO1lBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1lBQ25ELElBQUksQ0FBQyxlQUFlLEdBQUcsaUJBQWlCLENBQUMsZUFBZSxJQUFJLEVBQUUsQ0FBQztZQUMvRCxJQUFJLENBQUMsWUFBWSxHQUFHLGlCQUFpQixDQUFDO1NBQ3ZDO1FBRUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztJQUNsRCxDQUFDO0lBRU8sc0RBQXVCLEdBQS9CLFVBQWdDLElBQUk7UUFDbEMsSUFBTSxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqRCxJQUFNLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFVBQUMsSUFBSTtZQUNyRCxJQUFJLElBQUksQ0FBQyxHQUFHLEtBQUssZUFBZSxJQUFJLElBQUksQ0FBQyxHQUFHLEtBQUssWUFBWSxFQUFFO2dCQUM3RCxPQUFPLElBQUksQ0FBQzthQUNiO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDO1FBQ3JDLE9BQU8sZ0JBQWdCLENBQUM7SUFDMUIsQ0FBQztJQUVPLDZDQUFjLEdBQXRCO1FBQUEsaUJBT0M7UUFOQyxJQUFJLGlCQUFpQixHQUFHLFlBQVksQ0FBQztRQUNyQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQzVCLGlCQUFpQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxLQUFNLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBbUMsQ0FBQyxJQUFJLEVBQWpFLENBQWlFLENBQUM7Z0JBQ2hILENBQUMsQ0FBRSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQW1DLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7U0FDaEY7UUFDRCxPQUFPLGlCQUFpQixDQUFDO0lBQzNCLENBQUM7SUFFTyxrREFBbUIsR0FBM0IsVUFBNEIsR0FBVyxFQUFFLElBQVksRUFBRSxJQUFXO1FBQVgscUJBQUEsRUFBQSxXQUFXO1FBQ2hFLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZCLE9BQU8sRUFBRSxDQUFDO1NBQ1g7UUFDRCxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1FBQ3ZJLFFBQVEsSUFBSSxFQUFFO1lBQ1osS0FBSyxLQUFLO2dCQUNSLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3ZELEtBQUssS0FBSztnQkFDUixjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELE1BQU07U0FDVDtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ1osQ0FBQztJQUVPLG9EQUFxQixHQUE3QjtRQUNFLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO0lBQ3hDLENBQUM7SUFFTyxvREFBcUIsR0FBN0IsVUFBOEIsS0FBaUI7UUFDN0MsSUFBTSxJQUFJLEdBQUksS0FBSyxDQUFDLGFBQTZCLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUMxRSxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDdkMsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDO1FBQ3JDLElBQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDckQsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDM0MsVUFBMEIsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztRQUNoRCxVQUEwQixDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDO0lBQ3JELENBQUM7O2dCQTFlNEMsaUJBQWlCOztJQXBEckQ7UUFBUixLQUFLLEVBQUU7aUVBQStCO0lBQzlCO1FBQVIsS0FBSyxFQUFFO3VFQUFxQztJQUNwQztRQUFSLEtBQUssRUFBRTtpRUFBK0I7SUFDOUI7UUFBUixLQUFLLEVBQUU7OERBQTRCO0lBQzNCO1FBQVIsS0FBSyxFQUFFO29FQUFrQztJQUNqQztRQUFSLEtBQUssRUFBRTt5RUFBdUM7SUFDdEM7UUFBUixLQUFLLEVBQUU7MkVBQTBDO0lBQ3pDO1FBQVIsS0FBSyxFQUFFOzJEQUF5QjtJQUN4QjtRQUFSLEtBQUssRUFBRTsrREFBNkI7SUFDNUI7UUFBUixLQUFLLEVBQUU7aUVBQStCO0lBQzlCO1FBQVIsS0FBSyxFQUFFO2tFQUE4QjtJQUM3QjtRQUFSLEtBQUssRUFBRTtzRUFBb0M7SUFDbkM7UUFBUixLQUFLLEVBQUU7aUVBQWdDO0lBQy9CO1FBQVIsS0FBSyxFQUFFOzREQUF1RTtJQUcvRTtRQURDLEtBQUssRUFBRTs0RUFLUDtJQUlEO1FBREMsS0FBSyxFQUFFO2tFQVNQO0lBSVM7UUFBVCxNQUFNLEVBQUU7c0VBQWtEO0lBQ2pEO1FBQVQsTUFBTSxFQUFFOytEQUFvRDtJQXZDbEQsb0JBQW9CO1FBTGhDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxrQkFBa0I7WUFDNUIsbzlJQUE0Qzs7U0FFN0MsQ0FBQztPQUNXLG9CQUFvQixDQW1pQmhDO0lBQUQsMkJBQUM7Q0FBQSxBQW5pQkQsSUFtaUJDO1NBbmlCWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDR0ZTdG9yYWdlS2V5cyB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy9lbnVtcyc7XHJcbmltcG9ydCB7IENHRlV0aWxpdHlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY2dmLXV0aWxpdHkuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbHVtbkZvcm1hdCwgRm9ybWF0T3B0aW9uRGF0YVR5cGUsIER4RWRpdG9yVmFsdWVDaGFuZ2UgfSBmcm9tICcuLi8uLi9jb21wb25lbnRzL2NvbnRyYWN0cy9jb2x1bW4nO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIEV2ZW50RW1pdHRlciwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBCSUdyaWRDb2x1bW4sIERldkV4dHJlbWVEYXRhVHlwZUZvcm1hdCB9IGZyb20gJy4uL2NvbnRyYWN0cy9ncmlkJztcclxuaW1wb3J0IHsgYXBwVG9hc3QgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvdXRpbGl0eUZ1bmN0aW9ucyc7XHJcbmltcG9ydCB7IGNvbHVtbkZvcm1hdHRpbmdPcHRpb25zLCBjdXN0b21BY3Rpb25Db2x1bW5JbmZvIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2NvbnN0YW50cyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS1jb2xvci1mb3JtYXQnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb2xvci1mb3JtYXQuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbG9yLWZvcm1hdC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb2xvckZvcm1hdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVBbGlnbm1lbnQgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVCYWNrZ3JvdW5kQ29sb3IgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVGb250U3R5bGUgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVGb3JtYXQgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVGb3JtYXRUb2dnbGUgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVJbnB1dENhcHRpb25GaWVsZCA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZUlucHV0RGF0YUZpZWxkQ2hlY2sgPSBmYWxzZTtcclxuICBASW5wdXQoKSBwdWJsaWMgZW5hYmxlUGluID0gdHJ1ZTtcclxuICBASW5wdXQoKSBwdWJsaWMgZW5hYmxlU3RvcmFnZSA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZVRleHRDb2xvciA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGdyaWRJbnN0YW5jZUxpc3QgPSBbXTtcclxuICBASW5wdXQoKSBwdWJsaWMgaXNNYXN0ZXJHcmlkU2VsZWN0ZWQgPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBzaG93QmFzaWNGb3JtYXQgPSBmYWxzZTtcclxuICBASW5wdXQoKSBwdWJsaWMgc3RvcmFnZUtleTogc3RyaW5nID0gQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuZm9ybWF0RGF0YV07XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgc2V0IGRpc2FibGVEZWNpbWFsUHJlY2lzaW9uSW5wdXQoZGF0YTogYm9vbGVhbikge1xyXG4gICAgdGhpcy5kaXNhYmxlRGVjaW1hbFByZWNpc2lvbiA9IGRhdGE7XHJcbiAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YSA9IHRoaXMuZGlzYWJsZURlY2ltYWxQcmVjaXNpb24gPyB0aGlzLnJlbW92ZVByZWNpc2lvbkNvbnRyb2xzKGNvbHVtbkZvcm1hdHRpbmdPcHRpb25zKSA6IGNvbHVtbkZvcm1hdHRpbmdPcHRpb25zO1xyXG4gICAgdGhpcy5pc0NvbG9yUmVhZE9ubHkgPSAhdGhpcy5kaXNhYmxlRGVjaW1hbFByZWNpc2lvbjtcclxuICB9XHJcbiAgZ2V0IGRpc2FibGVEZWNpbWFsUHJlY2lzaW9uSW5wdXQoKTogYm9vbGVhbiB7IHJldHVybiB0aGlzLmRpc2FibGVEZWNpbWFsUHJlY2lzaW9uOyB9XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgc2V0IHNlbGVjdGVkQ29sdW1uSW5mbyhjb2x1bW5JdGVtOiBQQklHcmlkQ29sdW1uKSB7XHJcbiAgICB0aGlzLmNhblVwZGF0ZUNvbHVtbkZvcm1hdHRpbmcgPSBmYWxzZTtcclxuICAgIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhID0gY29sdW1uSXRlbSB8fCB7IGNhcHRpb246ICcnLCBkYXRhRmllbGQ6ICcnIH07XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGE/LmRhdGFGaWVsZC5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHRoaXMucHJlcGFyZVNlbGVjdGVkT3B0aW9uKCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnJlc2V0QWxsRm9ybWF0T3B0aW9ucygpO1xyXG4gICAgfVxyXG4gIH1cclxuICBnZXQgc2VsZWN0ZWRDb2x1bW5JbmZvKCkgeyByZXR1cm4gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGE7IH1cclxuXHJcblxyXG4gIEBPdXRwdXQoKSBwdWJsaWMgZm9ybWF0T3B0aW9uc0NoYW5nZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBmb3JtYXRUb2dnbGVkID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xyXG5cclxuICBiYWNrZ3JvdW5kQ29sb3IgPSAnJztcclxuICBiYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEgPSBjb2x1bW5Gb3JtYXR0aW5nT3B0aW9ucztcclxuICBjYW5VcGRhdGVDb2x1bW5Gb3JtYXR0aW5nOiBib29sZWFuO1xyXG4gIGNhcHRpb25WYWxpZGF0aW9uUGF0dGVybiA9IC9eW2EtekEtWjAtOV9cXHNdKyQvO1xyXG4gIGNvbHVtbnNBY3RpdmVQcm9wZXJ0aWVzVGFic0xpc3QgPSBbeyB0aXRsZTogJ0ZPUk1BVFRJTkcnIH1dO1xyXG4gIGRlY2ltYWxDb3VudGVyID0gMDtcclxuICBkZWNpbWFsVHlwZXM6IHN0cmluZ1tdID0gWydjb21tYScsICdwZXJjZW50JywgJ2N1cnJlbmN5J107XHJcbiAgZGlzYWJsZURlY2ltYWxQcmVjaXNpb24gPSBmYWxzZTtcclxuICBmb3JtYXRPYmplY3Q6IENvbHVtbkZvcm1hdCA9IHt9O1xyXG4gIGlzQ29sb3JSZWFkT25seSA9IHRydWU7XHJcbiAgc2VsZWN0ZWRDb2x1bW5EYXRhOiBQQklHcmlkQ29sdW1uO1xyXG4gIHRleHRDb2xvciA9ICcnO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJlYWRvbmx5IHV0aWxpdHlTZXJ2aWNlOiBDR0ZVdGlsaXR5U2VydmljZSkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5yZXNldEJhc2ljU2V0dGluZ3MoKTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQgJiYgdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5wcmVwYXJlU2VsZWN0ZWRPcHRpb24oKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vI3JlZ2lvbiBQdWJsaWMgTWV0aG9kc1xyXG5cclxuICBwdWJsaWMgb25DYXB0aW9uVmFsdWVDaGFuZ2VkKGFyZ3M6IER4RWRpdG9yVmFsdWVDaGFuZ2UpOiB2b2lkIHtcclxuICAgIGlmIChhcmdzLmV2ZW50KSB7XHJcbiAgICAgIHRoaXMuY2FuVXBkYXRlQ29sdW1uRm9ybWF0dGluZyA9IHRydWU7XHJcbiAgICB9XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdC5jYXB0aW9uID0gdGhpcy5zZWxlY3RlZENvbHVtbkluZm8uY2FwdGlvbiB8fCAnJztcclxuICAgIGNvbnN0IGRhdGEgPSB7XHJcbiAgICAgIGNhcHRpb246IHRoaXMuc2VsZWN0ZWRDb2x1bW5JbmZvLmNhcHRpb24gfHwgJydcclxuICAgIH07XHJcbiAgICBpZiAoZGF0YS5jYXB0aW9uLm1hdGNoKHRoaXMuY2FwdGlvblZhbGlkYXRpb25QYXR0ZXJuKSkge1xyXG4gICAgICB0aGlzLmZvcm1hdE9wdGlvbnNDaGFuZ2VkLmVtaXQoZGF0YSk7XHJcbiAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgfVxyXG4gICAgLy8gZWxzZSB7XHJcbiAgICAvLyAgIHRoaXMuc2VsZWN0ZWRDb2x1bW5JbmZvLmNhcHRpb24gPSBhcmdzICYmIGFyZ3MucHJldmlvdXNWYWx1ZSA/IGFyZ3MucHJldmlvdXNWYWx1ZSA6ICcnO1xyXG4gICAgLy8gfVxyXG5cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkNoYW5nZU9mVGV4dENvbG9yKCk6IHZvaWQge1xyXG4gICAgdGhpcy5mb3JtYXRPYmplY3QudGV4dENvbG9yID0gdGhpcy50ZXh0Q29sb3IgfHwgJyc7XHJcbiAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkNoYW5nZU9mQmFja2dyb3VuZENvbG9yKCk6IHZvaWQge1xyXG4gICAgdGhpcy5mb3JtYXRPYmplY3QuYmFja2dyb3VuZENvbG9yID0gdGhpcy5iYWNrZ3JvdW5kQ29sb3IgfHwgJyc7XHJcbiAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkNvbG9yRm9jdXNJbigpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmlzQ29sb3JSZWFkT25seSkge1xyXG4gICAgICBhcHBUb2FzdCh7IHR5cGU6ICd3YXJuaW5nJywgbWVzc2FnZTogJ0NvbHVtbiBGb3JtYXQ6IFBsZWFzZSBzZWxlY3QgYSBjb2x1bW4uJyB9KTtcclxuICAgIH1cclxuICAgIHRoaXMuY2FuVXBkYXRlQ29sdW1uRm9ybWF0dGluZyA9IHRydWU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZm9ybWF0Q29sdW1uKGFyZ3M6IEZvcm1hdE9wdGlvbkRhdGFUeXBlLCBldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFGaWVsZCA9PT0gY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5kYXRhRmllbGQgfHxcclxuICAgICAgKCghdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkKSB8fCAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkICYmIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFGaWVsZC5sZW5ndGggPT09IDApKVxyXG4gICAgICAmJiB0aGlzLmVuYWJsZUlucHV0RGF0YUZpZWxkQ2hlY2spIHtcclxuICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnd2FybmluZycsIG1lc3NhZ2U6ICdDb2x1bW4gRm9ybWF0OiBQbGVhc2Ugc2VsZWN0IGEgY29sdW1uLicgfSk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmIChhcmdzLnR5cGUgPT09ICdyaWdodCcpIHtcclxuICAgICAgaWYgKHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5sZW5ndGgpIHtcclxuICAgICAgICB0aGlzLmdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChncmlkID0+IHtcclxuICAgICAgICAgIGdyaWQuY29sdW1uT3B0aW9uKGN1c3RvbUFjdGlvbkNvbHVtbkluZm8uZGF0YUZpZWxkLCB7IGZpeGVkOiB0cnVlLCBmaXhlZFBvc2l0aW9uOiAncmlnaHQnIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLnByZXBhcmVMb2FkZXJMb2NhdGlvbihldmVudCk7XHJcbiAgICBhcmdzLmlzU2VsZWN0ZWQgPSAhYXJncy5pc1NlbGVjdGVkO1xyXG5cclxuICAgIGlmIChldmVudCkge1xyXG4gICAgICB0aGlzLmNhblVwZGF0ZUNvbHVtbkZvcm1hdHRpbmcgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgaWYgKCF0aGlzLnNlbGVjdEZvcm1hdChhcmdzKSkge1xyXG4gICAgICBhcmdzLmlzU2VsZWN0ZWQgPSAhYXJncy5pc1NlbGVjdGVkO1xyXG4gICAgfVxyXG4gICAgaWYgKGFyZ3Mua2V5LnRvTG93ZXJDYXNlKCkuaW5kZXhPZignZGVjaW1hbCcpID4gLTEpIHtcclxuICAgICAgYXJncy5pc1NlbGVjdGVkID0gZmFsc2U7ICAvLyB0aGlzIGNhc2UgbWFrZXMgc3VyZSByZW1vdmUvYWRkIGRlY2ltYWwgYXJlIG5vdCBoaWdobGlnaHRlZC5cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkZvcm1hdE9wdGlvbnNDaGFuZ2VkKCk6IHZvaWQge1xyXG4gICAgaWYgKCF0aGlzLmNhblVwZGF0ZUNvbHVtbkZvcm1hdHRpbmcpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhKSB7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLmdyaWRJbnN0YW5jZUxpc3QubGVuZ3RoKSB7XHJcbiAgICAgICAgICB0aGlzLmdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IGl0ZW0ucmVwYWludCgpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvYWRlcicpLmNsYXNzTGlzdC5hZGQoJ2Rpc3BsYXktbm9uZScpO1xyXG4gICAgICB9LCAwKTtcclxuICAgICAgY29uc3QgZm9ybWF0RGF0YUNvbGxlY3Rpb24gPSB0aGlzLmdldE9yU2V0U2Vzc2lvbkRhdGEodGhpcy5zdG9yYWdlS2V5LCAnZ2V0Jyk7XHJcbiAgICAgIGxldCBpbmRleDogbnVtYmVyLCBkYXRhVXBkYXRlZDogYm9vbGVhbjtcclxuICAgICAgZGF0YVVwZGF0ZWQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5mb3JtYXRPYmplY3QuZGF0YUZpZWxkID0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkO1xyXG4gICAgICBmb3IgKGluZGV4ID0gMDsgaW5kZXggPCBmb3JtYXREYXRhQ29sbGVjdGlvbi5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICBpZiAoZm9ybWF0RGF0YUNvbGxlY3Rpb25baW5kZXhdLmRhdGFGaWVsZCA9PT0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkKSB7XHJcbiAgICAgICAgICBmb3JtYXREYXRhQ29sbGVjdGlvbltpbmRleF0gPSB0aGlzLmZvcm1hdE9iamVjdDtcclxuICAgICAgICAgIGRhdGFVcGRhdGVkID0gdHJ1ZTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAoIWRhdGFVcGRhdGVkKSB7XHJcbiAgICAgICAgZm9ybWF0RGF0YUNvbGxlY3Rpb24ucHVzaCh0aGlzLmZvcm1hdE9iamVjdCk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuZ2V0T3JTZXRTZXNzaW9uRGF0YSh0aGlzLnN0b3JhZ2VLZXksICdzZXQnLCBmb3JtYXREYXRhQ29sbGVjdGlvbik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdG9nZ2xlQmFzaWNGb3JtYXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnNob3dCYXNpY0Zvcm1hdCA9ICF0aGlzLnNob3dCYXNpY0Zvcm1hdDtcclxuICAgIHRoaXMuZm9ybWF0VG9nZ2xlZC5lbWl0KHRoaXMuc2hvd0Jhc2ljRm9ybWF0KTtcclxuICB9XHJcblxyXG4gIC8vI2VuZHJlZ2lvbiBQdWJsaWMgTWV0aG9kc1xyXG5cclxuICAvLyNyZWdpb24gUHJpdmF0ZSBNZXRob2RzXHJcblxyXG4gIHByaXZhdGUgc2VsZWN0Rm9ybWF0KGFyZ3M6IEZvcm1hdE9wdGlvbkRhdGFUeXBlKTogYm9vbGVhbiB7XHJcbiAgICBsZXQgZm9ybWF0Tm90QWxsb3dlZCA9IGZhbHNlO1xyXG4gICAgdGhpcy5mb3JtYXRPYmplY3QuZGF0YVR5cGUgPSB0aGlzLmZvcm1hdE9iamVjdC5kYXRhVHlwZSB8fCB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZTtcclxuICAgIHN3aXRjaCAoYXJncy5rZXkpIHtcclxuICAgICAgY2FzZSAnbGVmdEFsaWdubWVudCc6XHJcbiAgICAgICAgdGhpcy5zZXRBbGlnbm1lbnRGb3JtYXQoYXJncywgJ2xlZnQnKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAncmlnaHRBbGlnbm1lbnQnOlxyXG4gICAgICAgIHRoaXMuc2V0QWxpZ25tZW50Rm9ybWF0KGFyZ3MsICdyaWdodCcpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdjZW50ZXJBbGlnbm1lbnQnOlxyXG4gICAgICAgIHRoaXMuc2V0QWxpZ25tZW50Rm9ybWF0KGFyZ3MsICdjZW50ZXInKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnbGVmdFBpbic6XHJcbiAgICAgICAgdGhpcy5zZXRQaW5Qb3NpdGlvbihhcmdzLCAnbGVmdCcpO1xyXG4gICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgY2FzZSAncmlnaHRQaW4nOlxyXG4gICAgICAgIHRoaXMuc2V0UGluUG9zaXRpb24oYXJncywgJ3JpZ2h0Jyk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgJ2JvbGQnOlxyXG4gICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdib2xkJywgYXJncy5pc1NlbGVjdGVkKTtcclxuICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAndW5kZXJsaW5lJzpcclxuICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAndW5kZXJsaW5lJywgYXJncy5pc1NlbGVjdGVkKTtcclxuICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnaXRhbGljJzpcclxuICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnaXRhbGljJywgYXJncy5pc1NlbGVjdGVkKTtcclxuICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnY3VycmVuY3knOlxyXG4gICAgICAgIHRoaXMucmVzZXRTZWxlY3RlZEZvcm1hdChhcmdzLnR5cGUpO1xyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZSAmJiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZS5pbmRleE9mKCdudW1iZXInKSA+IC0xKSB7XHJcbiAgICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnY3VycmVuY3knLCBhcmdzLmlzU2VsZWN0ZWQpO1xyXG4gICAgICAgICAgdGhpcy5kZWNpbWFsQ291bnRlciA9IDA7XHJcbiAgICAgICAgICB0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQgPSBhcmdzLmlzU2VsZWN0ZWQgPyB7IHR5cGU6ICdjdXJyZW5jeScsIHByZWNpc2lvbjogMiB9IDogJyc7XHJcbiAgICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuZW5hYmxlSW5wdXRDYXB0aW9uRmllbGQpIHtcclxuICAgICAgICAgIHRoaXMuY29sdW1uRm9ybWF0Q2hhbmdlRm9yRGF0YURpY3Rpb25hcnkoYXJncywgJ2N1cnJlbmN5JywgJ2N1cnJlbmN5JywgMik7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9ybWF0Tm90QWxsb3dlZCA9IHRydWU7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgJ2FkZERlY2ltYWwnOlxyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZSAmJiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZS5pbmRleE9mKCdudW1iZXInKSA+IC0xKSB7XHJcbiAgICAgICAgICBpZiAodGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVjaW1hbENvdW50ZXIgPSAodGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0IGFzIERldkV4dHJlbWVEYXRhVHlwZUZvcm1hdCkucHJlY2lzaW9uIHx8IDA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnZGVjaW1hbEFkZGVkLScgKyB0aGlzLmRlY2ltYWxDb3VudGVyLCBmYWxzZSk7XHJcbiAgICAgICAgICB0aGlzLmRlY2ltYWxDb3VudGVyKys7XHJcbiAgICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnZGVjaW1hbEFkZGVkLScgKyB0aGlzLmRlY2ltYWxDb3VudGVyLCB0cnVlKTtcclxuICAgICAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCA9IHtcclxuICAgICAgICAgICAgdHlwZTogdGhpcy5nZXREZWNpbWFsVHlwZSgpLFxyXG4gICAgICAgICAgICBwcmVjaXNpb246IHRoaXMuZGVjaW1hbENvdW50ZXJcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtYXROb3RBbGxvd2VkID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAncmVtb3ZlRGVjaW1hbCc6XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlICYmIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlLmluZGV4T2YoJ251bWJlcicpID4gLTEpIHtcclxuICAgICAgICAgIGlmICh0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWNpbWFsQ291bnRlciA9ICh0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQgYXMgRGV2RXh0cmVtZURhdGFUeXBlRm9ybWF0KS5wcmVjaXNpb24gfHwgMDtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnZGVjaW1hbEFkZGVkLScgKyB0aGlzLmRlY2ltYWxDb3VudGVyLCBmYWxzZSk7XHJcbiAgICAgICAgICBpZiAodGhpcy5kZWNpbWFsQ291bnRlciA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5kZWNpbWFsQ291bnRlci0tO1xyXG4gICAgICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnZGVjaW1hbEFkZGVkLScgKyB0aGlzLmRlY2ltYWxDb3VudGVyLCB0cnVlKTtcclxuICAgICAgICAgICAgdGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0ID0ge1xyXG4gICAgICAgICAgICAgIHR5cGU6IHRoaXMuZ2V0RGVjaW1hbFR5cGUoKSxcclxuICAgICAgICAgICAgICBwcmVjaXNpb246IHRoaXMuZGVjaW1hbENvdW50ZXJcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5kZWNpbWFsQ291bnRlciA9PT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnZGVjaW1hbEFkZGVkLScgKyB0aGlzLmRlY2ltYWxDb3VudGVyLCBmYWxzZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtYXROb3RBbGxvd2VkID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAncGVyY2VudGFnZSc6XHJcbiAgICAgICAgdGhpcy5yZXNldFNlbGVjdGVkRm9ybWF0KGFyZ3MudHlwZSk7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlICYmIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlLmluZGV4T2YoJ251bWJlcicpID4gLTEpIHtcclxuICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdwZXJjZW50YWdlJywgYXJncy5pc1NlbGVjdGVkKTtcclxuICAgICAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCA9IGFyZ3MuaXNTZWxlY3RlZCA/IHsgdHlwZTogJ3BlcmNlbnQnLCBwcmVjaXNpb246IDIgfSA6ICcnO1xyXG4gICAgICAgICAgdGhpcy5vbkZvcm1hdE9wdGlvbnNDaGFuZ2VkKCk7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLmVuYWJsZUlucHV0Q2FwdGlvbkZpZWxkKSB7XHJcbiAgICAgICAgICB0aGlzLmNvbHVtbkZvcm1hdENoYW5nZUZvckRhdGFEaWN0aW9uYXJ5KGFyZ3MsICdwZXJjZW50JywgJ3BlcmNlbnRhZ2UnLCAyKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtYXROb3RBbGxvd2VkID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnY29tbWEnOlxyXG4gICAgICAgIHRoaXMucmVzZXRTZWxlY3RlZEZvcm1hdChhcmdzLnR5cGUpO1xyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZSAmJiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZS5pbmRleE9mKCdudW1iZXInKSA+IC0xKSB7XHJcbiAgICAgICAgICB0aGlzLmRlY2ltYWxDb3VudGVyID0gMDtcclxuICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdjb21tYScsIGFyZ3MuaXNTZWxlY3RlZCk7XHJcbiAgICAgICAgICB0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQgPSBhcmdzLmlzU2VsZWN0ZWQgPyB7IHR5cGU6ICdjb21tYScsIHByZWNpc2lvbjogMCB9IDogJyc7XHJcbiAgICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuZW5hYmxlSW5wdXRDYXB0aW9uRmllbGQpIHtcclxuICAgICAgICAgIHRoaXMuY29sdW1uRm9ybWF0Q2hhbmdlRm9yRGF0YURpY3Rpb25hcnkoYXJncywgJ2NvbW1hJywgJ2NvbW1hJywgMCk7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9ybWF0Tm90QWxsb3dlZCA9IHRydWU7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgJ2NhbGVuZGFyJzpcclxuICAgICAgICB0aGlzLnJlc2V0U2VsZWN0ZWRGb3JtYXQoYXJncy50eXBlKTtcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YVR5cGUuaW5kZXhPZignZGF0ZScpID4gLTEgfHwgIXRoaXMuZW5hYmxlSW5wdXREYXRhRmllbGRDaGVjaykge1xyXG4gICAgICAgICAgdGhpcy5wcmVwYXJlQ29sdW1uRm9ybWF0VXNpbmdDbGFzc05hbWUoJ2Nzc0NsYXNzJywgJ2NhbGVuZGFyJywgYXJncy5pc1NlbGVjdGVkKTtcclxuICAgICAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCA9IGFyZ3MuaXNTZWxlY3RlZCA/ICdzaG9ydERhdGUnIDogJyc7XHJcbiAgICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuZW5hYmxlSW5wdXRDYXB0aW9uRmllbGQpIHtcclxuICAgICAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCA9IGFyZ3MuaXNTZWxlY3RlZCA/ICdzaG9ydERhdGUnIDogJyc7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9ybWF0Tm90QWxsb3dlZCA9IHRydWU7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuZW5hYmxlSW5wdXRDYXB0aW9uRmllbGQpIHtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvYWRlcicpLmNsYXNzTGlzdC5hZGQoJ2Rpc3BsYXktbm9uZScpO1xyXG4gICAgICB9LCAwKTtcclxuICAgIH1cclxuICAgIGlmIChmb3JtYXROb3RBbGxvd2VkKSB7XHJcbiAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ3dhcm5pbmcnLCBtZXNzYWdlOiAnRm9ybWF0IGNhblxcJ3QgYmUgYXBwbGllZCBvbiB0aGUgc2VsZWN0ZWQgY29sdW1uLicgfSk7XHJcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5sb2FkZXInKS5jbGFzc0xpc3QuYWRkKCdkaXNwbGF5LW5vbmUnKTtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRydWU7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNldEFsaWdubWVudEZvcm1hdChhcmdzOiBGb3JtYXRPcHRpb25EYXRhVHlwZSwgYWxpZ25tZW50VHlwZTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICB0aGlzLnJlc2V0QWxpZ25tZW50RXhjZXB0Rm9yQ3VycmVudChhcmdzLnR5cGUpO1xyXG4gICAgdGhpcy5mb3JtYXRPYmplY3QuYWxpZ25tZW50ID0gYXJncy5pc1NlbGVjdGVkID8gYWxpZ25tZW50VHlwZSA6ICcnO1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlSW5wdXRDYXB0aW9uRmllbGQpIHtcclxuICAgICAgdGhpcy5vbkZvcm1hdE9wdGlvbnNDaGFuZ2VkKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNldFBpblBvc2l0aW9uKGFyZ3M6IEZvcm1hdE9wdGlvbkRhdGFUeXBlLCBwb3NpdGlvbjogc3RyaW5nKTogdm9pZCB7XHJcbiAgICB0aGlzLnJlc2V0UGluRXhjZXB0Rm9yQ3VycmVudChhcmdzLnR5cGUpO1xyXG4gICAgdGhpcy5mb3JtYXRPYmplY3QuZml4ZWQgPSBhcmdzLmlzU2VsZWN0ZWQ7XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdC5maXhlZFBvc2l0aW9uID0gYXJncy5pc1NlbGVjdGVkID8gcG9zaXRpb24gOiAnJztcclxuICAgIGlmICh0aGlzLmdyaWRJbnN0YW5jZUxpc3QubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWQgPT4ge1xyXG4gICAgICAgIGdyaWQuY29sdW1uT3B0aW9uKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFGaWVsZCxcclxuICAgICAgICAgIHsgZml4ZWQ6IGFyZ3MuaXNTZWxlY3RlZCwgZml4ZWRQb3NpdGlvbjogdGhpcy5mb3JtYXRPYmplY3QuZml4ZWRQb3NpdGlvbiB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5lbmFibGVJbnB1dENhcHRpb25GaWVsZCkge1xyXG4gICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgY29sdW1uRm9ybWF0Q2hhbmdlRm9yRGF0YURpY3Rpb25hcnkoYXJnczogRm9ybWF0T3B0aW9uRGF0YVR5cGUsIHR5cGU6IHN0cmluZywgZm9ybWF0Q2xhc3M6IHN0cmluZywgcHJlY2lzaW9uPzogbnVtYmVyKTogdm9pZCB7XHJcbiAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCBmb3JtYXRDbGFzcywgYXJncy5pc1NlbGVjdGVkKTtcclxuICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCA9IGFyZ3MuaXNTZWxlY3RlZCA/IHsgdHlwZSwgcHJlY2lzaW9uIH0gOiAnJztcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcmVzZXRBbGlnbm1lbnRFeGNlcHRGb3JDdXJyZW50KHR5cGUgPSAnJyk6IHZvaWQge1xyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5hbGlnbm1lbnQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmFsaWdubWVudFtpXS50eXBlICE9PSB0eXBlKSB7XHJcbiAgICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuYWxpZ25tZW50W2ldLmlzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZXNldFBpbkV4Y2VwdEZvckN1cnJlbnQodHlwZSA9ICcnKTogdm9pZCB7XHJcbiAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW4uZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgaWYgKGl0ZW0udHlwZSAhPT0gdHlwZSkge1xyXG4gICAgICAgIGl0ZW0uaXNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKGZvcm1hdFR5cGU6IHN0cmluZywgZm9ybWF0VmFsdWU6IHN0cmluZywgYWRkRm9ybWF0OiBib29sZWFuKSB7XHJcbiAgICBsZXQgbGlzdE9mRXhpc3RpbmdGb3JtYXRzID0gdGhpcy5mb3JtYXRPYmplY3RbZm9ybWF0VHlwZV0gfHwgJyc7XHJcbiAgICBpZiAoYWRkRm9ybWF0KSB7XHJcbiAgICAgIGlmIChsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMgJiYgbGlzdE9mRXhpc3RpbmdGb3JtYXRzLmxlbmd0aCkge1xyXG4gICAgICAgIGNvbnN0IGZvcm1hdEdyb3VwRGF0YSA9IFsnY3VycmVuY3knLCAncGVyY2VudGFnZScsICdjb21tYScsICdjYWxlbmRhciddO1xyXG4gICAgICAgIGlmIChmb3JtYXRHcm91cERhdGEuaW5kZXhPZihmb3JtYXRWYWx1ZSkgPiAtMSkge1xyXG4gICAgICAgICAgZm9yIChsZXQgZm9ybWF0SW5kZXggPSAwOyBmb3JtYXRJbmRleCA8IGZvcm1hdEdyb3VwRGF0YS5sZW5ndGg7IGZvcm1hdEluZGV4KyspIHtcclxuICAgICAgICAgICAgbGlzdE9mRXhpc3RpbmdGb3JtYXRzID0gbGlzdE9mRXhpc3RpbmdGb3JtYXRzLnJlcGxhY2UoZm9ybWF0R3JvdXBEYXRhW2Zvcm1hdEluZGV4XSwgJycpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMgPSBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMucmVwbGFjZShuZXcgUmVnRXhwKGZvcm1hdFZhbHVlLCAnZycpLCAnJykudHJpbSgpO1xyXG4gICAgICAgIGxpc3RPZkV4aXN0aW5nRm9ybWF0cyA9IGxpc3RPZkV4aXN0aW5nRm9ybWF0cyArICcgJyArIGZvcm1hdFZhbHVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGxpc3RPZkV4aXN0aW5nRm9ybWF0cyA9IGZvcm1hdFZhbHVlO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKGxpc3RPZkV4aXN0aW5nRm9ybWF0cyAmJiBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMuaW5kZXhPZihmb3JtYXRWYWx1ZSkgPiAtMSkge1xyXG4gICAgICBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMgPSBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMucmVwbGFjZShuZXcgUmVnRXhwKGZvcm1hdFZhbHVlLCAnZycpLCAnJykudHJpbSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZm9ybWF0T2JqZWN0W2Zvcm1hdFR5cGVdID0gbGlzdE9mRXhpc3RpbmdGb3JtYXRzO1xyXG4gICAgY29uc3QgX2RhdGEgPSB7fTtcclxuICAgIGlmIChmb3JtYXRUeXBlID09PSAnY3NzQ2xhc3MnKSB7XHJcbiAgICAgIF9kYXRhW2Zvcm1hdFR5cGVdID0gbGlzdE9mRXhpc3RpbmdGb3JtYXRzO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgX2RhdGFbZm9ybWF0VHlwZV0gPSBhZGRGb3JtYXQgPyBmb3JtYXRWYWx1ZSA6IGxpc3RPZkV4aXN0aW5nRm9ybWF0cztcclxuICAgIH1cclxuICAgIHJldHVybiBfZGF0YTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcmVzZXRTZWxlY3RlZEZvcm1hdCh0eXBlID0gJycpOiB2b2lkIHtcclxuICAgIGxldCBsaXN0T2ZDbGFzc2VzID0gdGhpcy5mb3JtYXRPYmplY3QuY3NzQ2xhc3M7XHJcbiAgICBpZiAobGlzdE9mQ2xhc3Nlcykge1xyXG4gICAgICBsaXN0T2ZDbGFzc2VzID0gbGlzdE9mQ2xhc3Nlcy5yZXBsYWNlKC9wZXJjZW50YWdlL2csICcnKS5yZXBsYWNlKC9jb21tYS9nLCAnJykucmVwbGFjZSgvY3VycmVuY3kvZywgJycpO1xyXG4gICAgICB0aGlzLmZvcm1hdE9iamVjdC5jc3NDbGFzcyA9IGxpc3RPZkNsYXNzZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5mb3JtYXQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvcm1hdFtpXS50eXBlICE9PSB0eXBlKSB7XHJcbiAgICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9ybWF0W2ldLmlzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZXNldEJhc2ljU2V0dGluZ3MoKTogdm9pZCB7XHJcbiAgICBsZXQgaTogbnVtYmVyO1xyXG4gICAgZm9yIChpID0gMDsgaSA8IHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvcm1hdC5sZW5ndGg7IGkrKykge1xyXG4gICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5mb3JtYXRbaV0uaXNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAoaSA9IDA7IGkgPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5hbGlnbm1lbnQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuYWxpZ25tZW50W2ldLmlzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEucGluLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbltpXS5pc1NlbGVjdGVkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChpID0gMDsgaSA8IHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvbnRTdHlsZS5sZW5ndGg7IGkrKykge1xyXG4gICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5mb250U3R5bGVbaV0uaXNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgdGhpcy50ZXh0Q29sb3IgPSAnJztcclxuICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gJyc7XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdCA9IHt9O1xyXG4gICAgdGhpcy5kZWNpbWFsQ291bnRlciA9IDA7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZXBhcmVTZWxlY3RlZE9wdGlvbigpOiB2b2lkIHtcclxuICAgIHRoaXMucmVzZXRCYXNpY1NldHRpbmdzKCk7XHJcbiAgICBsZXQgc2VsZWN0ZWRDb2x1bW5EZWY6IENvbHVtbkZvcm1hdCA9IG5ldyBPYmplY3QoKSBhcyBDb2x1bW5Gb3JtYXQ7XHJcbiAgICBpZiAodGhpcy5lbmFibGVTdG9yYWdlKSB7XHJcbiAgICAgIGNvbnN0IGNvbHVtbkZvcm1hdENvbGxlY3Rpb246IENvbHVtbkZvcm1hdFtdID0gdGhpcy5nZXRPclNldFNlc3Npb25EYXRhKHRoaXMuc3RvcmFnZUtleSwgJ2dldCcpO1xyXG4gICAgICBjb25zdCBiYXNlRm9ybWF0Q29sbGVjdGlvbjogQ29sdW1uRm9ybWF0W10gPSB0aGlzLmlzTWFzdGVyR3JpZFNlbGVjdGVkID9cclxuICAgICAgICBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuZGljdGlvbmFyeUZvcm1hdERhdGFdKSkgfHwgW10gOiBbXTtcclxuICAgICAgc2VsZWN0ZWRDb2x1bW5EZWYgPSBjb2x1bW5Gb3JtYXRDb2xsZWN0aW9uLmZpbmQoYXJySXRlbSA9PiBhcnJJdGVtLmRhdGFGaWVsZCA9PT0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkKSB8fFxyXG4gICAgICAgIGJhc2VGb3JtYXRDb2xsZWN0aW9uLmZpbmQoYXJySXRlbSA9PiBhcnJJdGVtLmRhdGFGaWVsZCA9PT0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkKSB8fCB7fTtcclxuICAgIH1cclxuICAgIGlmICghT2JqZWN0LmtleXMoc2VsZWN0ZWRDb2x1bW5EZWYpLmxlbmd0aCAmJiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YSkge1xyXG4gICAgICBzZWxlY3RlZENvbHVtbkRlZiA9IHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhIGFzIGFueTtcclxuICAgIH1cclxuICAgIGlmIChzZWxlY3RlZENvbHVtbkRlZikge1xyXG4gICAgICBpZiAoc2VsZWN0ZWRDb2x1bW5EZWYuY3NzQ2xhc3MpIHtcclxuICAgICAgICBjb25zdCBjbGFzc0xpc3QgPSBzZWxlY3RlZENvbHVtbkRlZi5jc3NDbGFzcy5zcGxpdCgnICcpO1xyXG4gICAgICAgIGxldCBpOiBudW1iZXI7XHJcbiAgICAgICAgY2xhc3NMaXN0LmZvckVhY2goKGNzc0NsYXNzOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgIGlmIChjc3NDbGFzcyA9PT0gJ2JvbGQnIHx8IGNzc0NsYXNzID09PSAndW5kZXJsaW5lJyB8fCBjc3NDbGFzcyA9PT0gJ2l0YWxpYycpIHtcclxuICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvbnRTdHlsZS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgIGlmICh0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5mb250U3R5bGVbaV0udHlwZSA9PT0gY3NzQ2xhc3MpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvbnRTdHlsZVtpXS5pc1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmNzc0NsYXNzID0gdGhpcy5mb3JtYXRPYmplY3QuY3NzQ2xhc3MgPyB0aGlzLmZvcm1hdE9iamVjdC5jc3NDbGFzcyArICcgJyArIGNzc0NsYXNzIDogJyAnICsgY3NzQ2xhc3M7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBpZiAoY3NzQ2xhc3MgPT09ICdjdXJyZW5jeScgfHwgY3NzQ2xhc3MgPT09ICdwZXJjZW50YWdlJyB8fCBjc3NDbGFzcyA9PT0gJ2NvbW1hJyB8fCBjc3NDbGFzcyA9PT0gJ2NhbGVuZGFyJykge1xyXG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9ybWF0Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvcm1hdFtpXS50eXBlID09PSBjc3NDbGFzcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9ybWF0W2ldLmlzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWNpbWFsQ291bnRlciA9IHNlbGVjdGVkQ29sdW1uRGVmLmZvcm1hdCA/IChzZWxlY3RlZENvbHVtbkRlZi5mb3JtYXQgYXMgRGV2RXh0cmVtZURhdGFUeXBlRm9ybWF0KS5wcmVjaXNpb24gOiAwO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtYXRPYmplY3QuY3NzQ2xhc3MgPSB0aGlzLmZvcm1hdE9iamVjdC5jc3NDbGFzcyA/IHRoaXMuZm9ybWF0T2JqZWN0LmNzc0NsYXNzICsgJyAnICsgY3NzQ2xhc3MgOiAnICcgKyBjc3NDbGFzcztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGlmIChjc3NDbGFzcy5pbmRleE9mKCdkZWNpbWFsJykgPiAtMSkge1xyXG4gICAgICAgICAgICB0aGlzLmRlY2ltYWxDb3VudGVyID0gKHNlbGVjdGVkQ29sdW1uRGVmLmZvcm1hdCBhcyBEZXZFeHRyZW1lRGF0YVR5cGVGb3JtYXQpLnByZWNpc2lvbjtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHNlbGVjdGVkQ29sdW1uRGVmLmZpeGVkKSB7XHJcbiAgICAgICAgY29uc3QgaXNQb3NpdGlvbkxlZnQgPSBzZWxlY3RlZENvbHVtbkRlZi5maXhlZFBvc2l0aW9uID09PSAnbGVmdCcgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgZm9yIChsZXQgaW5kID0gMDsgaW5kIDwgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEucGluLmxlbmd0aDsgaW5kKyspIHtcclxuICAgICAgICAgIGlmIChpc1Bvc2l0aW9uTGVmdCAmJiB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW5baW5kXS5rZXkgPT09ICdsZWZ0UGluJykge1xyXG4gICAgICAgICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW5baW5kXS5pc1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbltpbmRdLmtleSA9PT0gJ3JpZ2h0UGluJykge1xyXG4gICAgICAgICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW5baW5kXS5pc1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICBsZXQgajogbnVtYmVyO1xyXG4gICAgICBmb3IgKGogPSAwOyBqIDwgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuYWxpZ25tZW50Lmxlbmd0aDsgaisrKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmFsaWdubWVudFtqXS50eXBlID09PSBzZWxlY3RlZENvbHVtbkRlZi5hbGlnbm1lbnQpIHtcclxuICAgICAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmFsaWdubWVudFtqXS5pc1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGZvciAoaiA9IDA7IGogPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW4ubGVuZ3RoOyBqKyspIHtcclxuICAgICAgICBpZiAodGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEucGluW2pdLnR5cGUgPT09IHNlbGVjdGVkQ29sdW1uRGVmLmZpeGVkUG9zaXRpb24pIHtcclxuICAgICAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbltqXS5pc1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy50ZXh0Q29sb3IgPSBzZWxlY3RlZENvbHVtbkRlZi50ZXh0Q29sb3IgfHwgJyc7XHJcbiAgICAgIHRoaXMuYmFja2dyb3VuZENvbG9yID0gc2VsZWN0ZWRDb2x1bW5EZWYuYmFja2dyb3VuZENvbG9yIHx8ICcnO1xyXG4gICAgICB0aGlzLmZvcm1hdE9iamVjdCA9IHNlbGVjdGVkQ29sdW1uRGVmO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuaXNDb2xvclJlYWRPbmx5ID0gIXRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZW1vdmVQcmVjaXNpb25Db250cm9scyhkYXRhKSB7XHJcbiAgICBjb25zdCBjb2x1bW5Gb3JtYXREYXRhID0gT2JqZWN0LmFzc2lnbih7fSwgZGF0YSk7XHJcbiAgICBjb25zdCBmb3JtYXREYXRhID0gY29sdW1uRm9ybWF0RGF0YS5mb3JtYXQuZmlsdGVyKChpdGVtKSA9PiB7XHJcbiAgICAgIGlmIChpdGVtLmtleSAhPT0gJ3JlbW92ZURlY2ltYWwnICYmIGl0ZW0ua2V5ICE9PSAnYWRkRGVjaW1hbCcpIHtcclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBjb2x1bW5Gb3JtYXREYXRhLmZvcm1hdCA9IGZvcm1hdERhdGE7XHJcbiAgICByZXR1cm4gY29sdW1uRm9ybWF0RGF0YTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0RGVjaW1hbFR5cGUoKTogc3RyaW5nIHtcclxuICAgIGxldCBkZWNpbWFsRm9ybWF0VHlwZSA9ICdmaXhlZFBvaW50JztcclxuICAgIGlmICh0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQpIHtcclxuICAgICAgZGVjaW1hbEZvcm1hdFR5cGUgPSB0aGlzLmRlY2ltYWxUeXBlcy5zb21lKHQgPT4gdCA9PT0gKHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCBhcyBEZXZFeHRyZW1lRGF0YVR5cGVGb3JtYXQpLnR5cGUpXHJcbiAgICAgICAgPyAodGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0IGFzIERldkV4dHJlbWVEYXRhVHlwZUZvcm1hdCkudHlwZSA6ICdmaXhlZFBvaW50JztcclxuICAgIH1cclxuICAgIHJldHVybiBkZWNpbWFsRm9ybWF0VHlwZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0T3JTZXRTZXNzaW9uRGF0YShrZXk6IHN0cmluZywgdHlwZTogc3RyaW5nLCBkYXRhID0gbnVsbCk6IEFycmF5PGFueT4ge1xyXG4gICAgaWYgKCF0aGlzLmVuYWJsZVN0b3JhZ2UpIHtcclxuICAgICAgcmV0dXJuIFtdO1xyXG4gICAgfVxyXG4gICAga2V5ID0gdGhpcy5ncmlkSW5zdGFuY2VMaXN0Lmxlbmd0aCA/IHRoaXMudXRpbGl0eVNlcnZpY2UuZ2V0U3RvcmFnZUtleSh0aGlzLmdyaWRJbnN0YW5jZUxpc3RbMF0sIGtleSwgdGhpcy5pc01hc3RlckdyaWRTZWxlY3RlZCkgOiBrZXk7XHJcbiAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgY2FzZSAnZ2V0JzpcclxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGtleSkpIHx8IFtdO1xyXG4gICAgICBjYXNlICdzZXQnOlxyXG4gICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oa2V5LCBKU09OLnN0cmluZ2lmeShkYXRhKSk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gW107XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHJlc2V0QWxsRm9ybWF0T3B0aW9ucygpOiB2b2lkIHtcclxuICAgIHRoaXMucmVzZXRCYXNpY1NldHRpbmdzKCk7XHJcbiAgICB0aGlzLnJlc2V0UGluRXhjZXB0Rm9yQ3VycmVudCgpO1xyXG4gICAgdGhpcy5yZXNldFNlbGVjdGVkRm9ybWF0KCk7XHJcbiAgICB0aGlzLnJlc2V0QWxpZ25tZW50RXhjZXB0Rm9yQ3VycmVudCgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwcmVwYXJlTG9hZGVyTG9jYXRpb24oZXZlbnQ6IE1vdXNlRXZlbnQpOiB2b2lkIHtcclxuICAgIGNvbnN0IGNvcmQgPSAoZXZlbnQuY3VycmVudFRhcmdldCBhcyBIVE1MRWxlbWVudCkuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICBjb25zdCBsZWZ0Q29yZHMgPSBjb3JkLmxlZnQgKyA0ICsgJ3B4JztcclxuICAgIGNvbnN0IHRvcENvcmRzID0gY29yZC50b3AgKyA0ICsgJ3B4JztcclxuICAgIGNvbnN0IGZpcnN0Q2hpbGQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubG9hZGVyJyk7XHJcbiAgICBmaXJzdENoaWxkLmNsYXNzTGlzdC5yZW1vdmUoJ2Rpc3BsYXktbm9uZScpO1xyXG4gICAgKGZpcnN0Q2hpbGQgYXMgSFRNTEVsZW1lbnQpLnN0eWxlLnRvcCA9IHRvcENvcmRzO1xyXG4gICAgKGZpcnN0Q2hpbGQgYXMgSFRNTEVsZW1lbnQpLnN0eWxlLmxlZnQgPSBsZWZ0Q29yZHM7XHJcbiAgfVxyXG5cclxuICAvLyNlbmRyZWdpb24gUHJpdmF0ZSBNZXRob2RzXHJcbn1cclxuIl19