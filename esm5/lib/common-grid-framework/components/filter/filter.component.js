import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { applyFiltersToGrid } from '../../utilities/utilityFunctions';
var FilterComponent = /** @class */ (function () {
    function FilterComponent() {
        this.closeCurrentFlyOut = new EventEmitter();
        this.applyButtonOptions = {
            text: 'Apply',
            type: 'default'
        };
        this.gridOptions = new Object();
        this.listOfColumns = [];
        this.title = 'FILTERS';
        this.showFilterSection = true;
    }
    Object.defineProperty(FilterComponent.prototype, "filterGridOptions", {
        get: function () { return this.gridOptions; },
        set: function (filterGridOptions) {
            var _a, _b;
            this.gridOptions = filterGridOptions;
            this.filterValue = ((_b = (_a = filterGridOptions === null || filterGridOptions === void 0 ? void 0 : filterGridOptions.gridComponentInstance) === null || _a === void 0 ? void 0 : _a.option()) === null || _b === void 0 ? void 0 : _b.filterValue) || this.filterGridOptions.gridFilterValue;
            this.listOfColumns = filterGridOptions.columns.filter(function (c) { return c.allowFiltering !== false; }) || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FilterComponent.prototype, "gridInstanceList", {
        get: function () { return this.selectedGridInstanceList; },
        set: function (gridInstanceList) {
            var _a;
            this.selectedGridInstanceList = gridInstanceList.filter(function (item) { return item.isSelected && Object.keys(item.gridComponentInstance).length; })
                .map(function (item) { return item.gridComponentInstance; });
            this.title = ((_a = gridInstanceList.filter(function (item) { return item.isSelected && Object.keys(item.gridComponentInstance).length; })[0]) === null || _a === void 0 ? void 0 : _a.isMasterGrid) ? 'FILTERS' : 'FILTERS - CHILD VIEW';
        },
        enumerable: true,
        configurable: true
    });
    ;
    FilterComponent.prototype.applyFilter = function () {
        var _this = this;
        this.filterGridOptions.gridFilterValue = this.filterValue;
        // change
        // if (this.gridInstance) {
        //   this.gridInstance.beginUpdate();
        //   this.gridInstance.option('filterValue', this.filterValue);
        //   applyFiltersToGrid(this.gridInstance, this.filterValue);
        //   this.gridInstance.endUpdate();
        //   setTimeout(() => {
        //     const currentGridState = this.gridInstance.state();
        //     currentGridState.gridFilterValue = this.filterValue;
        //     this.gridInstance.state(currentGridState);
        //   }, 100);
        // }
        if (this.selectedGridInstanceList.length) {
            this.selectedGridInstanceList.forEach(function (gridInstance) {
                gridInstance.beginUpdate();
                gridInstance.option('filterValue', _this.filterValue);
                applyFiltersToGrid(gridInstance, _this.filterValue);
                gridInstance.endUpdate();
                setTimeout(function () {
                    var currentGridState = gridInstance.state();
                    currentGridState.gridFilterValue = _this.filterValue;
                    gridInstance.state(currentGridState);
                }, 100);
            });
        }
    };
    FilterComponent.prototype.closeFlyOut = function () {
        this.closeCurrentFlyOut.emit();
    };
    __decorate([
        Input()
    ], FilterComponent.prototype, "filterGridOptions", null);
    __decorate([
        Input()
    ], FilterComponent.prototype, "gridInstanceList", null);
    __decorate([
        Output()
    ], FilterComponent.prototype, "closeCurrentFlyOut", void 0);
    FilterComponent = __decorate([
        Component({
            selector: 'pbi-filter',
            template: "<div class=\"filter-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-filter title-icon\"></i>\r\n            <span class=\"section-title\">{{title}}</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"pointer property-tab-in-view-selection\" (click)=\"showFilterSection = !showFilterSection\">\r\n                <span>VISUAL</span>\r\n                <span [class.fa-angle-down]=\"!showFilterSection\" [class.fa-angle-up]=\"showFilterSection\"\r\n                    class=\"fa basic-format-toggle-icon\"></span>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n    <div class=\"accordion-data-container\" *ngIf=\"showFilterSection\">\r\n        <div>\r\n            <div class=\"filter-options\">\r\n                <dx-filter-builder class=\"filter-builder\" [fields]=\"listOfColumns\" [(value)]=\"filterValue\"\r\n                    [allowHierarchicalFields]=\"true\">\r\n                </dx-filter-builder>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <span class=\"button-container\">\r\n        <dx-button class=\"apply-filter-button\" [text]=\"applyButtonOptions.text\" [type]=\"applyButtonOptions.type\"\r\n            (onClick)=\"applyFilter()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
            styles: [""]
        })
    ], FilterComponent);
    return FilterComponent;
}());
export { FilterComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tcG9uZW50cy9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQWUsS0FBSyxFQUFFLE1BQU0sRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUkvRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQVF0RTtJQTBCRTtRQVhpQix1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXpELHVCQUFrQixHQUFxQjtZQUNyQyxJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxTQUFTO1NBQ2hCLENBQUM7UUFFRixnQkFBVyxHQUFHLElBQUksTUFBTSxFQUF5QixDQUFDO1FBQ2xELGtCQUFhLEdBQW9CLEVBQUUsQ0FBQztRQUVwQyxVQUFLLEdBQUcsU0FBUyxDQUFDO1FBR2xCLHNCQUFpQixHQUFHLElBQUksQ0FBQztJQUZULENBQUM7SUF4QmpCLHNCQUFJLDhDQUFpQjthQUtyQixjQUEwQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO2FBTHBELFVBQXNCLGlCQUFpQjs7WUFDckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxpQkFBaUIsQ0FBQztZQUNyQyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQUEsaUJBQWlCLGFBQWpCLGlCQUFpQix1QkFBakIsaUJBQWlCLENBQUUscUJBQXFCLDBDQUFFLE1BQU0sNENBQUksV0FBVyxLQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLENBQUM7WUFDN0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLGNBQWMsS0FBSyxLQUFLLEVBQTFCLENBQTBCLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDakcsQ0FBQzs7O09BQUE7SUFHUSxzQkFBSSw2Q0FBZ0I7YUFLN0IsY0FBeUIsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDO2FBTHZELFVBQXFCLGdCQUErQzs7WUFDM0UsSUFBSSxDQUFDLHdCQUF3QixHQUFHLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxVQUFVLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxNQUFNLEVBQWpFLENBQWlFLENBQUM7aUJBQy9ILEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxxQkFBcUIsRUFBMUIsQ0FBMEIsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBQSxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxFQUFqRSxDQUFpRSxDQUFDLENBQUMsQ0FBQyxDQUFDLDBDQUFFLFlBQVksRUFBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQztRQUN4SyxDQUFDOzs7T0FBQTtJQU9DLENBQUM7SUFVSCxxQ0FBVyxHQUFYO1FBQUEsaUJBNEJDO1FBM0JDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMxRCxTQUFTO1FBQ1QsMkJBQTJCO1FBQzNCLHFDQUFxQztRQUNyQywrREFBK0Q7UUFDL0QsNkRBQTZEO1FBQzdELG1DQUFtQztRQUNuQyx1QkFBdUI7UUFDdkIsMERBQTBEO1FBQzFELDJEQUEyRDtRQUMzRCxpREFBaUQ7UUFDakQsYUFBYTtRQUNiLElBQUk7UUFFSixJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUU7WUFDeEMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxVQUFBLFlBQVk7Z0JBQ2hELFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDM0IsWUFBWSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNyRCxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNuRCxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQ3pCLFVBQVUsQ0FBQztvQkFDVCxJQUFNLGdCQUFnQixHQUFHLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDOUMsZ0JBQWdCLENBQUMsZUFBZSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUM7b0JBQ3BELFlBQVksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDdkMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ1YsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTSxxQ0FBVyxHQUFsQjtRQUNFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBNUREO1FBREMsS0FBSyxFQUFFOzREQUtQO0lBR1E7UUFBUixLQUFLLEVBQUU7MkRBSVA7SUFFUztRQUFULE1BQU0sRUFBRTsrREFBZ0Q7SUFmOUMsZUFBZTtRQUwzQixTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsWUFBWTtZQUN0QixxOENBQXNDOztTQUV2QyxDQUFDO09BQ1csZUFBZSxDQStEM0I7SUFBRCxzQkFBQztDQUFBLEFBL0RELElBK0RDO1NBL0RZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSG9zdEJpbmRpbmcsIElucHV0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEZXZFeHRyZW1lQnV0dG9uIH0gZnJvbSAnLi4vY29udHJhY3RzL2RldmV4dHJlbWVXaWRnZXRzJztcclxuaW1wb3J0IHsgR3JpZENvbXBvbmVudEluc3RhbmNlcyB9IGZyb20gJy4uL2NvbnRyYWN0cy9jb21tb24tZ3JpZC1mcmFtZXdvcmsnO1xyXG5pbXBvcnQgeyBQQklHcmlkT3B0aW9uc01vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2dyaWRPcHRpb25zLm1vZGVsJztcclxuaW1wb3J0IHsgYXBwbHlGaWx0ZXJzVG9HcmlkIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMnO1xyXG5pbXBvcnQgeyBQQklHcmlkQ29sdW1uIH0gZnJvbSAnLi4vY29udHJhY3RzL2dyaWQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdwYmktZmlsdGVyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZmlsdGVyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9maWx0ZXIuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXJDb21wb25lbnQge1xyXG4gIEBJbnB1dCgpXHJcbiAgc2V0IGZpbHRlckdyaWRPcHRpb25zKGZpbHRlckdyaWRPcHRpb25zKSB7XHJcbiAgICB0aGlzLmdyaWRPcHRpb25zID0gZmlsdGVyR3JpZE9wdGlvbnM7XHJcbiAgICB0aGlzLmZpbHRlclZhbHVlID0gZmlsdGVyR3JpZE9wdGlvbnM/LmdyaWRDb21wb25lbnRJbnN0YW5jZT8ub3B0aW9uKCk/LmZpbHRlclZhbHVlIHx8IHRoaXMuZmlsdGVyR3JpZE9wdGlvbnMuZ3JpZEZpbHRlclZhbHVlO1xyXG4gICAgdGhpcy5saXN0T2ZDb2x1bW5zID0gZmlsdGVyR3JpZE9wdGlvbnMuY29sdW1ucy5maWx0ZXIoKGMpID0+IGMuYWxsb3dGaWx0ZXJpbmcgIT09IGZhbHNlKSB8fCBbXTtcclxuICB9XHJcbiAgZ2V0IGZpbHRlckdyaWRPcHRpb25zKCkgeyByZXR1cm4gdGhpcy5ncmlkT3B0aW9uczsgfVxyXG5cclxuICBASW5wdXQoKSBzZXQgZ3JpZEluc3RhbmNlTGlzdChncmlkSW5zdGFuY2VMaXN0OiBBcnJheTxHcmlkQ29tcG9uZW50SW5zdGFuY2VzPikge1xyXG4gICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QgPSBncmlkSW5zdGFuY2VMaXN0LmZpbHRlcihpdGVtID0+IGl0ZW0uaXNTZWxlY3RlZCAmJiBPYmplY3Qua2V5cyhpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSkubGVuZ3RoKVxyXG4gICAgICAubWFwKGl0ZW0gPT4gaXRlbS5ncmlkQ29tcG9uZW50SW5zdGFuY2UpO1xyXG4gICAgdGhpcy50aXRsZSA9IGdyaWRJbnN0YW5jZUxpc3QuZmlsdGVyKGl0ZW0gPT4gaXRlbS5pc1NlbGVjdGVkICYmIE9iamVjdC5rZXlzKGl0ZW0uZ3JpZENvbXBvbmVudEluc3RhbmNlKS5sZW5ndGgpWzBdPy5pc01hc3RlckdyaWQgPyAnRklMVEVSUycgOiAnRklMVEVSUyAtIENISUxEIFZJRVcnO1xyXG4gIH1cclxuICBnZXQgZ3JpZEluc3RhbmNlTGlzdCgpIHsgcmV0dXJuIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0OyB9XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBjbG9zZUN1cnJlbnRGbHlPdXQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIGFwcGx5QnV0dG9uT3B0aW9uczogRGV2RXh0cmVtZUJ1dHRvbiA9IHtcclxuICAgIHRleHQ6ICdBcHBseScsXHJcbiAgICB0eXBlOiAnZGVmYXVsdCdcclxuICB9OztcclxuICBmaWx0ZXJWYWx1ZTogYW55O1xyXG4gIGdyaWRPcHRpb25zID0gbmV3IE9iamVjdCgpIGFzIFBCSUdyaWRPcHRpb25zTW9kZWw7XHJcbiAgbGlzdE9mQ29sdW1uczogUEJJR3JpZENvbHVtbltdID0gW107XHJcbiAgc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0OiBBcnJheTxhbnk+O1xyXG4gIHRpdGxlID0gJ0ZJTFRFUlMnO1xyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHNob3dGaWx0ZXJTZWN0aW9uID0gdHJ1ZTtcclxuXHJcbiAgYXBwbHlGaWx0ZXIoKTogdm9pZCB7XHJcbiAgICB0aGlzLmZpbHRlckdyaWRPcHRpb25zLmdyaWRGaWx0ZXJWYWx1ZSA9IHRoaXMuZmlsdGVyVmFsdWU7XHJcbiAgICAvLyBjaGFuZ2VcclxuICAgIC8vIGlmICh0aGlzLmdyaWRJbnN0YW5jZSkge1xyXG4gICAgLy8gICB0aGlzLmdyaWRJbnN0YW5jZS5iZWdpblVwZGF0ZSgpO1xyXG4gICAgLy8gICB0aGlzLmdyaWRJbnN0YW5jZS5vcHRpb24oJ2ZpbHRlclZhbHVlJywgdGhpcy5maWx0ZXJWYWx1ZSk7XHJcbiAgICAvLyAgIGFwcGx5RmlsdGVyc1RvR3JpZCh0aGlzLmdyaWRJbnN0YW5jZSwgdGhpcy5maWx0ZXJWYWx1ZSk7XHJcbiAgICAvLyAgIHRoaXMuZ3JpZEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gICAgLy8gICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgIC8vICAgICBjb25zdCBjdXJyZW50R3JpZFN0YXRlID0gdGhpcy5ncmlkSW5zdGFuY2Uuc3RhdGUoKTtcclxuICAgIC8vICAgICBjdXJyZW50R3JpZFN0YXRlLmdyaWRGaWx0ZXJWYWx1ZSA9IHRoaXMuZmlsdGVyVmFsdWU7XHJcbiAgICAvLyAgICAgdGhpcy5ncmlkSW5zdGFuY2Uuc3RhdGUoY3VycmVudEdyaWRTdGF0ZSk7XHJcbiAgICAvLyAgIH0sIDEwMCk7XHJcbiAgICAvLyB9XHJcblxyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0Lmxlbmd0aCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWRJbnN0YW5jZSA9PiB7XHJcbiAgICAgICAgZ3JpZEluc3RhbmNlLmJlZ2luVXBkYXRlKCk7XHJcbiAgICAgICAgZ3JpZEluc3RhbmNlLm9wdGlvbignZmlsdGVyVmFsdWUnLCB0aGlzLmZpbHRlclZhbHVlKTtcclxuICAgICAgICBhcHBseUZpbHRlcnNUb0dyaWQoZ3JpZEluc3RhbmNlLCB0aGlzLmZpbHRlclZhbHVlKTtcclxuICAgICAgICBncmlkSW5zdGFuY2UuZW5kVXBkYXRlKCk7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICBjb25zdCBjdXJyZW50R3JpZFN0YXRlID0gZ3JpZEluc3RhbmNlLnN0YXRlKCk7XHJcbiAgICAgICAgICBjdXJyZW50R3JpZFN0YXRlLmdyaWRGaWx0ZXJWYWx1ZSA9IHRoaXMuZmlsdGVyVmFsdWU7XHJcbiAgICAgICAgICBncmlkSW5zdGFuY2Uuc3RhdGUoY3VycmVudEdyaWRTdGF0ZSk7XHJcbiAgICAgICAgfSwgMTAwKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xvc2VGbHlPdXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsb3NlQ3VycmVudEZseU91dC5lbWl0KCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==