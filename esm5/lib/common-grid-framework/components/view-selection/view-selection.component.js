import { __decorate, __read, __spread } from "tslib";
import validationEngine from 'devextreme/ui/validation_engine';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CGFEventsEnum, CGFStorageKeys } from '../../utilities/enums';
import { CGFUtilityService } from '../../services/cgf-utility.service';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DxTreeViewComponent } from 'devextreme-angular';
import { ViewSelectionModel } from '../models/view-selection.model';
import { appToast } from '../../utilities/utilityFunctions';
import { confirm } from 'devextreme/ui/dialog';
import { crudOperation, customActionColumnInfo } from '../../utilities/constants';
var ViewSelectionComponent = /** @class */ (function () {
    function ViewSelectionComponent(activatedRoute, router, utilityService) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.utilityService = utilityService;
        this.isGridBorderVisible = false;
        this.viewDataSource = new Object();
        this.viewEvent = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.defaults = [];
        this.currentSelectLayoutInfo = new Object();
        this.dropDownOptions = [{ value: 1, name: 'Save As' }];
        this.defaultTheme = 'np.compact';
        this.gridLayoutSettings = {
            colDef: [],
            filters: [],
            getCombinedFilter: [],
            summary: [],
            childGridLayout: [],
            gridName: ''
        };
        this.filterLayoutName = '';
        this.filterValue = null;
        this.groupLayoutList = [];
        this.isLayoutNone = false;
        this.isPinned = false;
        this.layoutDefaultData = [{ key: 1, value: 'User Default' }, { key: 2, value: 'Global Default' }];
        this.layoutList = [];
        this.routerSubscribe = null;
        this.selectedLayoutDefaultIds = [];
        this.selectedViewVisibility = 'Private';
        this.showFilterControlInput = false;
        this.showModalPopUp = false;
        this.showSaveButtonMenuList = false;
        this.themeList = [{ key: 'light.regular', value: 'Light' }, { key: 'light.compact', value: 'Light Compact' }];
        this.uniqueGridInstanceList = [];
        this.viewNameValidationPattern = /^[a-zA-Z0-9-_\s]+$/;
        this.viewTitle = 'Add New View';
        this.viewVisibilityTypes = ['Private', 'Public'];
    }
    Object.defineProperty(ViewSelectionComponent.prototype, "viewList", {
        get: function () { return this.layoutList; },
        set: function (layoutList) {
            this.layoutList = layoutList;
            this.prepareGroupLayoutList();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewSelectionComponent.prototype, "selectedTheme", {
        get: function () { return this.defaultTheme; },
        set: function (themeName) {
            if (sessionStorage.getItem('theme')) {
                this.defaultTheme = sessionStorage.getItem('theme');
            }
            else {
                this.defaultTheme = themeName;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ViewSelectionComponent.prototype, "gridInstanceList", {
        get: function () { return this.uniqueGridInstanceList; },
        set: function (gridInstanceList) {
            var _this = this;
            this.uniqueGridInstanceList = [];
            gridInstanceList.forEach(function (grid) {
                if (!_this.uniqueGridInstanceList.filter(function (item) { return item.gridName === grid.gridName && item.isMasterGrid === grid.isMasterGrid; }).length) {
                    _this.uniqueGridInstanceList.push(grid);
                }
            });
        },
        enumerable: true,
        configurable: true
    });
    //#region Angular LifeCycle event
    ViewSelectionComponent.prototype.ngOnInit = function () {
        var layoutId = Number(this.activatedRoute.snapshot.queryParams['layout']);
        this.currentSelectLayoutInfo = this.layoutList.filter(function (arrItem) {
            return arrItem.id === layoutId;
        })[0];
        if (this.currentSelectLayoutInfo) {
            this.layoutName = this.currentSelectLayoutInfo.name;
        }
        else if (!this.currentSelectLayoutInfo) {
            this.resetLayoutInfo();
        }
        this.setSelectedLayout();
        this.updateThemeSelection();
    };
    //#endregion Angular LifeCycle event
    //#region Public Methods
    ViewSelectionComponent.prototype.onViewSelectionClick = function (layoutItem) {
        this.currentSelectLayoutInfo = layoutItem;
        this.setSelectedLayout(true);
    };
    ViewSelectionComponent.prototype.deleteSelectedLayout = function (layoutItem) {
        var _this = this;
        var dataToDelete = {
            layoutId: layoutItem.id ? (layoutItem.id !== -1 ? layoutItem.id : 0) : 0,
            layoutName: layoutItem.name,
            isPublic: layoutItem.isPublic,
            isGlobalLayout: layoutItem.isGlobalDefault
        };
        var confirmDialog = confirm("Are you sure you want to delete view (" + layoutItem.name + ")?", 'Delete View');
        confirmDialog.done(function (dialogResult) {
            if (dialogResult) {
                _this.deleteLayout(layoutItem);
            }
            else {
                return;
            }
        });
        // this.overrideConfirmPopupTheme();
    };
    ViewSelectionComponent.prototype.updateViewVisibility = function (layoutItem) {
        var _this = this;
        if (this.actionOnSelectedView(layoutItem.id)) {
            var layoutAccessabilityGroup = (layoutItem ? !layoutItem.isPublic : this.currentSelectLayoutInfo.isPublic) ? 'Public' : 'Private';
            var confirmDialog = confirm("Are you sure you want to change view to " + layoutAccessabilityGroup + " ?", 'Save View');
            confirmDialog.done(function (result) {
                if (result) {
                    layoutItem.isPublic = !layoutItem.isPublic;
                    if (_this.viewDataSource) {
                        if (layoutItem.isGlobalDefault) {
                            appToast({ message: 'Global default can\'t set as private view.', type: 'error' });
                            return;
                        }
                        _this.viewDataSource.updateViewVisibility(layoutItem.id, layoutItem.isPublic)
                            .then(function (response) {
                            _this.selectedViewVisibility = layoutItem.isPublic ?
                                _this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'public'; })[0]
                                : _this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'private'; })[0];
                            appToast({ type: 'success', message: (response === null || response === void 0 ? void 0 : response.message) || 'View visibility updated.' });
                        }).catch(function (err) {
                            appToast({ type: 'success', message: (err === null || err === void 0 ? void 0 : err.Message) || 'Unable to update view visibility.' });
                        });
                    }
                    else {
                        _this.viewEvent.emit({ event: CGFEventsEnum.updateViewVisibility, data: layoutItem });
                    }
                }
            });
        }
    };
    ViewSelectionComponent.prototype.addNewView = function () {
        this.gridInstanceList.forEach(function (grid) {
            grid.gridComponentInstance.beginUpdate();
            grid.gridComponentInstance.collapseAll(-1);
            grid.gridComponentInstance.clearGrouping();
            grid.gridComponentInstance.getVisibleColumns().forEach(function (colItem) {
                grid.gridComponentInstance.columnOption(colItem.dataField, { visible: false });
            });
            grid.gridComponentInstance.endUpdate();
        });
        this.viewTitle = 'Add New View';
        this.selectedCrudOperation = crudOperation.add;
        this.layoutId = 0;
        this.showModalPopUp = true;
        this.layoutName = '';
        this.isPinned = false;
        this.selectedLayoutDefaultIds = [];
        this.selectedViewVisibility = this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'private'; })[0];
    };
    ViewSelectionComponent.prototype.closePopupWindow = function () {
        this.selectedCrudOperation = '';
        if (this.activatedRoute.snapshot.queryParams.layout === '0') {
            var queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
            queryParams.layout = this.layoutId.toString();
            this.router.navigate([], { queryParams: queryParams });
        }
        else {
            this.setSelectedLayout();
        }
        this.showModalPopUp = false;
    };
    ViewSelectionComponent.prototype.searchBoxInitialized = function (e) { setTimeout(function () { e.component.focus(); }, 0); };
    ViewSelectionComponent.prototype.saveAsLayout = function () {
        this.saveViewInformation(CGFEventsEnum.saveAsView);
    };
    ViewSelectionComponent.prototype.saveViewInformation = function (enumVal) {
        var _this = this;
        if (!this.isFormValid()) {
            return;
        }
        if (!this.defaultOptionValidation()) {
            appToast({ type: 'warning', message: "Private View can't be made Global Default" });
            return;
        }
        if (this.viewDataSource) {
            this.viewDataSource.addView(this.prepareNewLayoutInfoBeforeEmitting())
                .then(function (response) {
                _this.viewEvent.emit({ event: CGFEventsEnum.updateLayouts, data: response.map(function (v) { return new ViewSelectionModel(v); }) });
                appToast({ type: 'success', message: 'View has been saved successfully' });
            }).catch(function (err) {
                appToast({ type: 'success', message: err.message || 'Unable to save view.' });
            });
        }
        else {
            this.viewEvent.emit({ event: enumVal || CGFEventsEnum.addNewView, data: this.prepareNewLayoutInfoBeforeEmitting() });
        }
        this.showModalPopUp = false;
    };
    // TODO without using setTimeout
    ViewSelectionComponent.prototype.onLayoutNameContentReady = function (args) { setTimeout(function () { args.component.focus(); }, 300); };
    ViewSelectionComponent.prototype.saveCurrentSelectedView = function () {
        var _this = this;
        if (this.viewDataSource) {
            this.viewDataSource.saveView(this.prepareCurrentLayoutInfoBeforeEmitting())
                .then(function (response) {
                _this.viewEvent.emit({ event: CGFEventsEnum.updateLayouts, data: response.map(function (v) { return new ViewSelectionModel(v); }) });
                appToast({ type: 'success', message: 'Selected View has been updated successfully.' });
            }).catch(function (err) {
                appToast({ type: 'error', message: err.message || 'Unable to save selected view.' });
            });
        }
        else {
            this.viewEvent.emit({ event: CGFEventsEnum.saveSelectedView, data: this.prepareCurrentLayoutInfoBeforeEmitting() });
        }
        this.showModalPopUp = false;
    };
    ViewSelectionComponent.prototype.syncTreeViewSelection = function (e) {
        var _a, _b;
        var component = (e && e.component) || ((_a = this.treeView) === null || _a === void 0 ? void 0 : _a.instance);
        if (!component) {
            return;
        }
        if (!((_b = this.selectedLayoutDefaultIds) === null || _b === void 0 ? void 0 : _b.length)) {
            component.unselectAll();
        }
        else {
            this.selectedLayoutDefaultIds.forEach((function (value) { component.selectItem(value); }).bind(this));
        }
    };
    ViewSelectionComponent.prototype.onDefaultVisibilityChange = function (e) {
        if (e.node.itemData) {
            this.selectedLayoutDefaultIds = e.component.getSelectedNodeKeys();
        }
    };
    ViewSelectionComponent.prototype.filterListOfViews = function (args) {
        var _a, _b;
        this.filterValue = (_a = this.filterLayoutName) === null || _a === void 0 ? void 0 : _a.toLowerCase();
        if (!((_b = this.filterValue) === null || _b === void 0 ? void 0 : _b.trim()) && !(args.event.originalEvent instanceof KeyboardEvent)) {
            this.clearFilteredViews();
        }
    };
    ViewSelectionComponent.prototype.cloneSelectedLayout = function (layoutItem) {
        if (this.actionOnSelectedView(layoutItem.id)) {
            this.showModalPopUp = true;
            this.viewTitle = 'Clone View';
            this.selectedCrudOperation = crudOperation.clone;
            var counter = 1;
            var cloneName = 'Clone';
            this.layoutName = layoutItem.name + " " + cloneName;
            this.isPinned = layoutItem.isPinned;
            this.layoutId = 0;
            this.selectedViewVisibility = layoutItem.isPublic ? this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'public'; })[0]
                : this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'private'; })[0];
            this.setLayoutDefaultOptions(layoutItem);
            while (!this.isViewNameUnique(this.layoutName)) {
                this.layoutName = layoutItem.name + " " + cloneName + " " + counter;
                counter += 1;
            }
        }
    };
    ViewSelectionComponent.prototype.editSelectedLayout = function (layoutItem) {
        if (this.actionOnSelectedView(layoutItem.id)) {
            this.selectedCrudOperation = crudOperation.edit;
            this.viewTitle = 'Edit View';
            this.showModalPopUp = true;
            this.layoutId = layoutItem.id;
            this.layoutName = layoutItem.name;
            this.isPinned = layoutItem.isPinned;
            this.selectedViewVisibility = layoutItem.isPublic ? this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'public'; })[0]
                : this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'private'; })[0];
            this.setLayoutDefaultOptions(layoutItem);
            this.setSelectedLayout();
        }
    };
    ViewSelectionComponent.prototype.closeFlyOut = function () {
        this.closeCurrentFlyOut.emit();
    };
    //#endregion Public Methods
    //#region Private Methods
    ViewSelectionComponent.prototype.setSelectedLayout = function (applyLayout) {
        if (applyLayout === void 0) { applyLayout = false; }
        this.populateDefaults();
        this.layoutName = this.currentSelectLayoutInfo.name;
        this.selectedViewVisibility = this.currentSelectLayoutInfo.isPublic ? this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'public'; })[0]
            : this.viewVisibilityTypes.filter(function (item) { return item.toLowerCase() === 'private'; })[0];
        this.isPinned = this.currentSelectLayoutInfo.isPinned;
        this.setLayoutDefaultOptions(this.currentSelectLayoutInfo);
        if (applyLayout) {
            this.applySelectedLayout();
        }
    };
    ViewSelectionComponent.prototype.applySelectedLayout = function () {
        var _this = this;
        var queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        if (this.currentSelectLayoutInfo && this.currentSelectLayoutInfo.id !== -1) {
            if (Number(queryParams.layout) === this.currentSelectLayoutInfo.id) {
                return;
            }
            queryParams.layout = this.currentSelectLayoutInfo.id;
            this.router.navigate([], { queryParams: queryParams });
        }
        else {
            this.resetLayoutInfo();
            queryParams.layout = 0;
            this.router.navigate([], { queryParams: queryParams });
        }
        if (this.viewDataSource) {
            this.viewDataSource.applyView(this.currentSelectLayoutInfo)
                .then(function (updatedViewModel) {
                _this.viewEvent.emit({ event: CGFEventsEnum.applyView, data: updatedViewModel });
                appToast({ type: 'success', message: 'Selected view has been applied successfully' });
            }).catch(function () {
                appToast({ type: 'error', message: 'Unable to apply selected view.' });
            });
        }
        else {
            this.viewEvent.emit({ event: CGFEventsEnum.applyView, data: this.currentSelectLayoutInfo });
        }
    };
    ViewSelectionComponent.prototype.updateGridLayoutSettings = function () {
        var _this = this;
        var childGridLayout = [];
        this.uniqueGridInstanceList.forEach(function (gridInstance) {
            var gridSettings = _this.setLayoutSettings(gridInstance.gridComponentInstance, gridInstance.isMasterGrid);
            gridSettings.gridName = gridInstance.gridName;
            if (gridInstance.isMasterGrid) {
                _this.gridLayoutSettings = gridSettings;
            }
            else {
                childGridLayout.push(gridSettings);
            }
        });
        this.gridLayoutSettings.childGridLayout = childGridLayout.length ? childGridLayout
            : JSON.parse(sessionStorage.getItem('childGridLayout')) || [];
    };
    ViewSelectionComponent.prototype.setLayoutSettings = function (gridInstance, isMasterGrid) {
        if (isMasterGrid === void 0) { isMasterGrid = true; }
        // const gridLayoutSetting = gridInstance.state();
        // gridLayoutSetting.colDef = gridInstance.getVisibleColumns().filter((item) => {
        //   return item.dataField !== customActionColumnInfo.dataField || item.command !== 'empty';
        // });
        // gridLayoutSetting.formatData = JSON.parse(sessionStorage.getItem(getStorageKey(gridInstance, CGFStorageKeys[CGFStorageKeys.formatData], isMasterGrid)));
        // gridLayoutSetting.summary = gridInstance.option('summary');
        // gridLayoutSetting.selectedTheme = this.appliedTheme;
        // gridLayoutSetting.isGridBorderVisible = this.isGridBorderVisible;
        // gridLayoutSetting.conditionalFormatting = JSON.parse(sessionStorage.getItem(getStorageKey(gridInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], isMasterGrid)));
        // return gridLayoutSetting;
    };
    ViewSelectionComponent.prototype.populateDefaults = function () {
        this.defaults = [];
        if (this.currentSelectLayoutInfo.id > 0) {
            if (this.currentSelectLayoutInfo.canDefault) {
                this.defaults.push({ text: this.currentSelectLayoutInfo.isDefault ? 'Clear Default' : 'Set As Default', value: 'Default', icon: 'fas fa-star default-icon-color' });
            }
            if (this.currentSelectLayoutInfo.canGlobalDefault && this.currentSelectLayoutInfo.isPublic) {
                this.defaults.push({
                    text: this.currentSelectLayoutInfo.isGlobalDefault ?
                        'Clear Global Default' : 'Set As Global Default', value: 'GlobalDefault', icon: 'fas fa-star global-default-icon-color'
                });
            }
        }
    };
    ViewSelectionComponent.prototype.deleteLayout = function (viewToDelete) {
        var _this = this;
        if (this.viewDataSource) {
            this.viewDataSource.deleteView(viewToDelete)
                .then(function () {
                appToast({ type: 'success', message: 'View has been deleted successfully.' });
                _this.viewEvent.emit({ event: CGFEventsEnum.updateLayouts, data: __spread(_this.viewList.filter(function (item) { return item.id !== viewToDelete.id; })) });
                _this.prepareGroupLayoutList();
            }).catch(function () {
                appToast({ type: 'error', message: 'View has been was not deleted successfully.' });
            });
        }
        else {
            this.viewEvent.emit({ event: CGFEventsEnum.deleteView, data: viewToDelete });
            // TODO: refactor below duplicate code.
            this.viewList = __spread(this.viewList.filter(function (item) { return item.id !== viewToDelete.id; }));
            this.prepareGroupLayoutList();
        }
    };
    ViewSelectionComponent.prototype.resetLayoutInfo = function () { this.currentSelectLayoutInfo = new Object(); };
    ViewSelectionComponent.prototype.updateThemeSelection = function () { this.defaultTheme = this.defaultTheme || 'dx-swatch-default'; };
    ViewSelectionComponent.prototype.clearFilteredViews = function () {
        this.filterValue = null;
        this.filterLayoutName = '';
        this.showFilterControlInput = false;
    };
    ViewSelectionComponent.prototype.prepareGroupLayoutList = function () {
        var _this = this;
        var groups = new Set(this.layoutList.map(function (item) { return item.isPinned; }));
        this.groupLayoutList = [];
        groups.forEach(function (groupValue) {
            return _this.groupLayoutList.push({
                groupName: groupValue ? 'PINNED' : 'VIEWS',
                isVisible: true,
                values: _this.layoutList.filter(function (i) { return i.isPinned === groupValue; })
            });
        });
        // Sorting to make sure that Pinned is always on top.
        this.groupLayoutList = this.groupLayoutList.sort(function (a, b) {
            if (a.groupName < b.groupName) {
                return -1;
            }
            if (a.groupName > b.groupName) {
                return 1;
            }
            return 0;
        });
    };
    ViewSelectionComponent.prototype.setLayoutDefaultOptions = function (layoutItem) {
        this.selectedLayoutDefaultIds = [];
        if (layoutItem.isGlobalDefault) {
            this.selectedLayoutDefaultIds.push(this.layoutDefaultData.filter(function (item) { return item.value.toLowerCase() === 'global default'; })[0].key);
        }
        if (layoutItem.isDefault) {
            this.selectedLayoutDefaultIds.push(this.layoutDefaultData.filter(function (item) { return item.value.toLowerCase() === 'user default'; })[0].key);
        }
    };
    ViewSelectionComponent.prototype.isViewNameUnique = function (viewName) {
        var filterItem = this.layoutList.filter(function (item) { return item.name === viewName; }) || [];
        return filterItem.length ? false : true;
    };
    ViewSelectionComponent.prototype.isFormValid = function () {
        return validationEngine.validateGroup('viewSave').isValid;
    };
    ViewSelectionComponent.prototype.actionOnSelectedView = function (layoutId) {
        var _a;
        if (((_a = this.currentSelectLayoutInfo) === null || _a === void 0 ? void 0 : _a.id) !== layoutId) {
            appToast({ type: 'warning', message: "Please apply the layout before performing any action on it." });
            return false;
        }
        return true;
    };
    ViewSelectionComponent.prototype.prepareNewLayoutInfoBeforeEmitting = function () {
        return {
            id: this.layoutId,
            name: this.layoutName,
            isGlobalDefault: this.checkIfGlobalDefault(this.selectedLayoutDefaultIds),
            isUserDefault: this.checkIfUserDefault(this.selectedLayoutDefaultIds),
            isPinned: this.isPinned,
            defaultOptions: this.layoutDefaultData.filter(function (item) { return item.selected; }).length > 0 ? this.layoutDefaultData : [],
            visibility: this.selectedViewVisibility.toLowerCase() === 'public',
            state: this.getGridStateOfSelectedGrid()
        };
    };
    ViewSelectionComponent.prototype.prepareCurrentLayoutInfoBeforeEmitting = function () {
        return {
            id: this.currentSelectLayoutInfo.id,
            name: this.currentSelectLayoutInfo.name,
            isPinned: this.currentSelectLayoutInfo.isPinned,
            isGlobalDefault: this.currentSelectLayoutInfo.isGlobalDefault,
            isUserDefault: this.currentSelectLayoutInfo.isDefault,
            defaultOptions: this.layoutDefaultData.filter(function (item) { return item.selected; }).length > 0 ? this.layoutDefaultData : [],
            visibility: this.currentSelectLayoutInfo.isPublic,
            state: this.getGridStateOfSelectedGrid()
        };
    };
    ViewSelectionComponent.prototype.getGridStateOfSelectedGrid = function () {
        var currentSelectedGridInstance = this.gridInstanceList.filter(function (item) { return item.isSelected; })[0];
        var key = this.utilityService.getStorageKey(currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], true);
        var currentGridState = currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance.state();
        currentGridState.visibleColumns = currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance.getVisibleColumns().filter(function (item) {
            return item.dataField !== customActionColumnInfo.dataField || item.command !== 'empty';
        });
        currentGridState.columnFormattingInfo = JSON.parse(sessionStorage.getItem(key)) || [];
        currentGridState.summary = currentSelectedGridInstance === null || currentSelectedGridInstance === void 0 ? void 0 : currentSelectedGridInstance.gridComponentInstance.option('summary');
        currentGridState.selectedTheme = this.defaultTheme;
        currentGridState.isGridBorderVisible = this.isGridBorderVisible;
        currentGridState.conditionalFormattingInfo = JSON.parse(sessionStorage.getItem(key)) || [];
        return {
            gridState: currentGridState,
            columnFormattingInfo: JSON.parse(sessionStorage.getItem(key)) || [],
            conditionFormattingInfo: []
        };
    };
    ViewSelectionComponent.prototype.defaultOptionValidation = function () {
        var _a;
        var isGlobalDefault = this.selectedLayoutDefaultIds.indexOf((_a = this.layoutDefaultData.filter(function (item) { return item.value.toLowerCase() === 'global default'; })[0]) === null || _a === void 0 ? void 0 : _a.key) > -1;
        var isPublic = this.selectedViewVisibility.toLowerCase() === 'public';
        return isGlobalDefault ? isPublic : true;
    };
    ViewSelectionComponent.prototype.checkIfGlobalDefault = function (ids) {
        var globalKey = this.layoutDefaultData.find(function (a) { return a.value === 'Global Default'; });
        return ids.some(function (id) { return globalKey.key === id; });
    };
    ViewSelectionComponent.prototype.checkIfUserDefault = function (ids) {
        var userKey = this.layoutDefaultData.find(function (a) { return a.value === 'User Default'; });
        return ids.some(function (id) { return userKey.key === id; });
    };
    ViewSelectionComponent.ctorParameters = function () { return [
        { type: ActivatedRoute },
        { type: Router },
        { type: CGFUtilityService }
    ]; };
    __decorate([
        ViewChild(DxTreeViewComponent)
    ], ViewSelectionComponent.prototype, "treeView", void 0);
    __decorate([
        Input()
    ], ViewSelectionComponent.prototype, "viewList", null);
    __decorate([
        Input()
    ], ViewSelectionComponent.prototype, "selectedTheme", null);
    __decorate([
        Input()
    ], ViewSelectionComponent.prototype, "gridInstanceList", null);
    __decorate([
        Input()
    ], ViewSelectionComponent.prototype, "isGridBorderVisible", void 0);
    __decorate([
        Input()
    ], ViewSelectionComponent.prototype, "viewDataSource", void 0);
    __decorate([
        Output()
    ], ViewSelectionComponent.prototype, "viewEvent", void 0);
    __decorate([
        Output()
    ], ViewSelectionComponent.prototype, "closeCurrentFlyOut", void 0);
    ViewSelectionComponent = __decorate([
        Component({
            selector: 'pbi-view-selection',
            template: "<div class=\"viewSelectionContainer\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-list title-icon\"></i>\r\n            <span class=\"section-title\">View Selection</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div>\r\n        <div class=\"add-search-container\">\r\n            <div class=\"text-search\">\r\n                <dx-text-box class=\"search-box\" mode=\"search\" placeholder=\"Filter by keyword\"\r\n                    [(value)]=\"filterLayoutName\" maxLength=\"40\" (onInitialized)=\"searchBoxInitialized($event)\"\r\n                    (onValueChanged)=\"filterListOfViews($event)\" valueChangeEvent=\"keyup\">\r\n                </dx-text-box>\r\n            </div>\r\n            <div class=\"add-view\">\r\n                <i class=\"fas fa-plus fly-out-round-add-icon pointer\" title=\"Add\" (click)=\"addNewView()\"></i>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div *ngFor=\"let obj of groupLayoutList\">\r\n        <div class=\"active-properties-tab\">\r\n            <ul>\r\n                <li class=\"pointer property-tab-in-view-selection\" (click)=\"obj.isVisible = !obj.isVisible\">\r\n                    <span>{{obj.groupName}}</span>\r\n                    <i [class.fa-angle-down]=\"!obj.isVisible\" [class.fa-angle-up]=\"obj.isVisible\"\r\n                        class=\"fa basic-format-toggle-icon\"></i>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n        <div [class.dynamic-height-container]=\"groupLayoutList.length === 1\" class=\"views-list-detail-container\"\r\n            *ngIf=\"obj.isVisible\">\r\n            <div *ngFor=\"let item of obj.values\">\r\n                <span class=\"layout-info-container\"\r\n                    *ngIf=\"filterValue ? item.name.toLowerCase().indexOf(filterValue) > -1 : true\">\r\n                    <span [class.selectedLayout]=\"currentSelectLayoutInfo.id === item.id\">\r\n                        <span class=\"applied-layout dynamic-tooltip\" flow=\"down\"\r\n                            attr.title=\"{{item.isPublic ? 'Public View' : 'Private View'}}\">\r\n                            <i class=\"far fa-globe-americas\" (click)=\"updateViewVisibility(item)\"\r\n                                *ngIf=\"item.isPublic\"></i>\r\n                            <i class=\"fas fa-eye-slash\" (click)=\"updateViewVisibility(item)\" *ngIf=\"!item.isPublic\"></i>\r\n                        </span>\r\n                        <span class=\"layoutName\" (click)=\"onViewSelectionClick(item)\">\r\n                            {{item.name}}\r\n                            <i class=\"fas fa-star default-icon-color\" *ngIf=\"item.isDefault\"></i>\r\n                            <i class=\"fas fa-star global-default-icon-color\" *ngIf=\"item.isGlobalDefault\"></i>\r\n                        </span>\r\n                        <i class=\"far fa-clone layoutDeleteIcon dynamic-tooltip\" title=\"Clone Layout\"\r\n                            [class.deleteIconWithSelectedLayout]=\"currentSelectLayoutInfo.id === item.id\"\r\n                            (click)=\"cloneSelectedLayout(item)\"></i>\r\n                        <i class=\"fas fa-pencil layoutDeleteIcon dynamic-tooltip\" title=\"Edit Layout\"\r\n                            [class.deleteIconWithSelectedLayout]=\"currentSelectLayoutInfo.id === item.id\"\r\n                            (click)=\"editSelectedLayout(item)\"></i>\r\n                        <i class=\"far fa-trash-alt layoutDeleteIcon dynamic-tooltip\" title=\"Delete Layout\"\r\n                            [class.deleteIconWithSelectedLayout]=\"currentSelectLayoutInfo.id === item.id\"\r\n                            (click)=\"deleteSelectedLayout(item)\"></i>\r\n                    </span>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"save-button-container\" *ngIf=\"layoutList.length\">\r\n        <dx-button class=\"view-operation-buttons\" text=\"Save\" type=\"default\" (click)=\"saveCurrentSelectedView()\">\r\n        </dx-button>\r\n    </div>\r\n    <div class=\"empty-message\" *ngIf=\"!layoutList.length\">\r\n        <div>View Not Found</div>\r\n    </div>\r\n</div>\r\n\r\n<dx-popup class=\"pbi-modal-popup\" width=\"40%\" height=\"auto\" [showTitle]=\"true\" [title]=\"viewTitle\" [dragEnabled]=\"false\"\r\n    [(visible)]=\"showModalPopUp\">\r\n    <div *dxTemplate=\"let data of 'content'\">\r\n        <dx-validation-group id='viewSave'>\r\n            <form>\r\n                <div class=\"view-flex-container\">\r\n                    <div class=\"view-name-container\">\r\n                        <label class=\"required-field\">Name</label>\r\n                        <div>\r\n                            <div>\r\n                                <dx-text-box (onContentReady)=\"onLayoutNameContentReady($event)\"\r\n                                    class=\"pbi-text-editor view-name-textbox\" [(value)]=\"layoutName\" maxLength=\"40\">\r\n                                    <dx-validator validationGroup=\"viewSave\">\r\n                                        <dxi-validation-rule type=\"required\" message=\"Name is required.\">\r\n                                        </dxi-validation-rule>\r\n                                        <dxi-validation-rule type=\"pattern\" [pattern]=\"viewNameValidationPattern\"\r\n                                            message=\"Name will contain only alphabets(a-z,A-Z)/numbers(0-9)/special characters(-,_,space).\">\r\n                                        </dxi-validation-rule>\r\n                                    </dx-validator>\r\n                                </dx-text-box>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"view-label-container\">\r\n                        <label class=\"required-field\">Default Options</label>\r\n                        <div>\r\n                            <dx-drop-down-box class=\"pbi-text-editor\" [(value)]=\"selectedLayoutDefaultIds\"\r\n                                valueExpr=\"key\" displayExpr=\"value\" [dataSource]=\"layoutDefaultData\"\r\n                                (onValueChanged)=\"syncTreeViewSelection()\">\r\n                                <div *dxTemplate=\"let data of 'content'\">\r\n                                    <dx-tree-view [dataSource]=\"layoutDefaultData\" dataStructure=\"plain\" keyExpr=\"key\"\r\n                                        selectionMode=\"multiple\" showCheckBoxesMode=\"normal\" displayExpr=\"value\"\r\n                                        [selectByClick]=\"true\" (onContentReady)=\"syncTreeViewSelection($event)\"\r\n                                        (onItemSelectionChanged)=\"onDefaultVisibilityChange($event)\">\r\n                                    </dx-tree-view>\r\n                                </div>\r\n                            </dx-drop-down-box>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"view-flex-container\">\r\n                    <div class=\"view-name-container\">\r\n                        <label class=\"required-field\">Pinned</label>\r\n                        <div>\r\n                            <div>\r\n                                <label class=\"switch\">\r\n                                    <!-- <app-bool-input [(State)]=\"isPinned\"></app-bool-input> -->\r\n                                    <span>\r\n                                        <label class=\"switch\">\r\n                                            <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"isPinned\">\r\n                                            <span class=\"slider round\"></span>\r\n                                        </label>\r\n                                    </span>\r\n                                </label>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"view-label-container\">\r\n                        <label class=\"required-field\">View Visibility</label>\r\n                        <div>\r\n                            <div>\r\n                                <dx-radio-group [items]=\"viewVisibilityTypes\" [(value)]=\"selectedViewVisibility\"\r\n                                    layout=\"horizontal\">\r\n                                </dx-radio-group>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"pop-up-footer\">\r\n                    <div class=\"view-action-button\">\r\n                        <dx-button class=\"cancel-button\" text=\"Cancel\" type=\"default\" (click)=\"closePopupWindow()\">\r\n                        </dx-button>\r\n                        <dx-drop-down-button class=\"pbi-split-button\" [splitButton]=\"true\" [useSelectMode]=\"false\"\r\n                            text=\"Save\" [items]=\"dropDownOptions\" displayExpr=\"name\" keyExpr=\"id\"\r\n                            (onButtonClick)=\"saveViewInformation()\" (onItemClick)=\"saveAsLayout()\">\r\n                        </dx-drop-down-button>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </dx-validation-group>\r\n    </div>\r\n</dx-popup>\r\n",
            styles: [""]
        })
    ], ViewSelectionComponent);
    return ViewSelectionComponent;
}());
export { ViewSelectionComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1zZWxlY3Rpb24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL3ZpZXctc2VsZWN0aW9uL3ZpZXctc2VsZWN0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBRUEsT0FBTyxnQkFBZ0IsTUFBTSxpQ0FBaUMsQ0FBQztBQUMvRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNqRSxPQUFPLEVBQUUsYUFBYSxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3RFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBTXpELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM1RCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBT2xGO0lBdUVFLGdDQUFvQixjQUE4QixFQUFVLE1BQWMsRUFBVSxjQUFpQztRQUFqRyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQW1CO1FBekNyRyx3QkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDNUIsbUJBQWMsR0FBb0IsSUFBSSxNQUFNLEVBQXFCLENBQUM7UUFFakUsY0FBUyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDL0IsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV6RCxhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsNEJBQXVCLEdBQUcsSUFBSSxNQUFNLEVBQXdCLENBQUM7UUFDN0Qsb0JBQWUsR0FBRyxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztRQUNsRCxpQkFBWSxHQUFHLFlBQVksQ0FBQztRQUM1Qix1QkFBa0IsR0FBUTtZQUN4QixNQUFNLEVBQUUsRUFBRTtZQUNWLE9BQU8sRUFBRSxFQUFFO1lBQ1gsaUJBQWlCLEVBQUUsRUFBRTtZQUNyQixPQUFPLEVBQUUsRUFBRTtZQUNYLGVBQWUsRUFBRSxFQUFFO1lBQ25CLFFBQVEsRUFBRSxFQUFFO1NBQ2IsQ0FBQztRQUNGLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUN0QixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixvQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUNyQixpQkFBWSxHQUFHLEtBQUssQ0FBQztRQUNyQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLHNCQUFpQixHQUFvQixDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsY0FBYyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7UUFFOUcsZUFBVSxHQUF5QixFQUFFLENBQUM7UUFFdEMsb0JBQWUsR0FBaUIsSUFBSSxDQUFDO1FBR3JDLDZCQUF3QixHQUFrQixFQUFFLENBQUM7UUFDN0MsMkJBQXNCLEdBQUcsU0FBUyxDQUFDO1FBQ25DLDJCQUFzQixHQUFHLEtBQUssQ0FBQztRQUMvQixtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QiwyQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDL0IsY0FBUyxHQUFvQixDQUFDLEVBQUUsR0FBRyxFQUFFLGVBQWUsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsZUFBZSxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsQ0FBQyxDQUFDO1FBQzFILDJCQUFzQixHQUFrQyxFQUFFLENBQUM7UUFDM0QsOEJBQXlCLEdBQUcsb0JBQW9CLENBQUM7UUFDakQsY0FBUyxHQUFHLGNBQWMsQ0FBQztRQUMzQix3QkFBbUIsR0FBRyxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztJQUU2RSxDQUFDO0lBbEUxSCxzQkFBSSw0Q0FBUTthQUlaLGNBQWlCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7YUFKMUMsVUFBYSxVQUFVO1lBQ3JCLElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO1lBQzdCLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQ2hDLENBQUM7OztPQUFBO0lBSUQsc0JBQUksaURBQWE7YUFPakIsY0FBc0IsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzthQVBqRCxVQUFrQixTQUFpQjtZQUNqQyxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxZQUFZLEdBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNyRDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQzthQUMvQjtRQUNILENBQUM7OztPQUFBO0lBR1Esc0JBQUksb0RBQWdCO2FBUTdCLGNBQXlCLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQzthQVJyRCxVQUFxQixnQkFBK0M7WUFBN0UsaUJBT0M7WUFOQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1lBQ2pDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQzNCLElBQUksQ0FBQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssSUFBSSxDQUFDLFlBQVksRUFBMUUsQ0FBMEUsQ0FBQyxDQUFDLE1BQU0sRUFBRTtvQkFDbEksS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDeEM7WUFDSCxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUM7OztPQUFBO0lBNkNELGlDQUFpQztJQUNqQyx5Q0FBUSxHQUFSO1FBQ0UsSUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1FBQzVFLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxVQUFDLE9BQU87WUFDNUQsT0FBTyxPQUFPLENBQUMsRUFBRSxLQUFLLFFBQVEsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNOLElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBQ2hDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQztTQUNyRDthQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDeEMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQ3hCO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUNELG9DQUFvQztJQUVwQyx3QkFBd0I7SUFDakIscURBQW9CLEdBQTNCLFVBQTRCLFVBQThCO1FBQ3hELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxVQUFVLENBQUM7UUFDMUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFTSxxREFBb0IsR0FBM0IsVUFBNEIsVUFBOEI7UUFBMUQsaUJBZ0JDO1FBZkMsSUFBTSxZQUFZLEdBQUc7WUFDbkIsUUFBUSxFQUFFLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxJQUFJO1lBQzNCLFFBQVEsRUFBRSxVQUFVLENBQUMsUUFBUTtZQUM3QixjQUFjLEVBQUUsVUFBVSxDQUFDLGVBQWU7U0FDM0MsQ0FBQztRQUNGLElBQU0sYUFBYSxHQUFRLE9BQU8sQ0FBQywyQ0FBeUMsVUFBVSxDQUFDLElBQUksT0FBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ2hILGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBQyxZQUFxQjtZQUN2QyxJQUFJLFlBQVksRUFBRTtnQkFDaEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUMvQjtpQkFBTTtnQkFDTCxPQUFPO2FBQ1I7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILG9DQUFvQztJQUN0QyxDQUFDO0lBRU0scURBQW9CLEdBQTNCLFVBQTRCLFVBQThCO1FBQTFELGlCQTJCQztRQTFCQyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDNUMsSUFBTSx3QkFBd0IsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1lBQ3BJLElBQU0sYUFBYSxHQUFRLE9BQU8sQ0FBQyw2Q0FBMkMsd0JBQXdCLE9BQUksRUFBRSxXQUFXLENBQUMsQ0FBQztZQUN6SCxhQUFhLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBZTtnQkFDakMsSUFBSSxNQUFNLEVBQUU7b0JBQ1YsVUFBVSxDQUFDLFFBQVEsR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7b0JBQzNDLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTt3QkFDdkIsSUFBSSxVQUFVLENBQUMsZUFBZSxFQUFFOzRCQUM5QixRQUFRLENBQUMsRUFBRSxPQUFPLEVBQUUsNENBQTRDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7NEJBQ25GLE9BQU87eUJBQ1I7d0JBQ0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7NkJBQ3pFLElBQUksQ0FBQyxVQUFDLFFBQVE7NEJBQ2IsS0FBSSxDQUFDLHNCQUFzQixHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztnQ0FDakQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLEVBQS9CLENBQStCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQzNFLENBQUMsQ0FBQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNqRixRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxDQUFBLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxPQUFPLEtBQUksMEJBQTBCLEVBQUUsQ0FBQyxDQUFDO3dCQUMxRixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFHOzRCQUNYLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLENBQUEsR0FBRyxhQUFILEdBQUcsdUJBQUgsR0FBRyxDQUFFLE9BQU8sS0FBSSxtQ0FBbUMsRUFBRSxDQUFDLENBQUM7d0JBQzlGLENBQUMsQ0FBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztxQkFDdEY7aUJBQ0Y7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO0lBQ0gsQ0FBQztJQUVNLDJDQUFVLEdBQWpCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7WUFDaEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMscUJBQXFCLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDM0MsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixFQUFFLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBc0I7Z0JBQzVFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1lBQ2pGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3pDLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFNBQVMsR0FBRyxjQUFjLENBQUM7UUFDaEMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7UUFDbEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLHdCQUF3QixHQUFHLEVBQUUsQ0FBQztRQUNuQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxTQUFTLEVBQWhDLENBQWdDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUM3RyxDQUFDO0lBRU0saURBQWdCLEdBQXZCO1FBQ0UsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsQ0FBQztRQUNoQyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEtBQUssR0FBRyxFQUFFO1lBQzNELElBQU0sV0FBVyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2hGLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUM5QyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsRUFBRSxXQUFXLGFBQUEsRUFBRSxDQUFDLENBQUM7U0FDM0M7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVNLHFEQUFvQixHQUEzQixVQUE0QixDQUFxQixJQUFVLFVBQVUsQ0FBQyxjQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRXBHLDZDQUFZLEdBQW5CO1FBQ0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRU0sb0RBQW1CLEdBQTFCLFVBQTJCLE9BQXVCO1FBQWxELGlCQXNCQztRQXBCQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFO1lBQ3ZCLE9BQU87U0FDUjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUUsRUFBRTtZQUNuQyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSwyQ0FBMkMsRUFBRSxDQUFDLENBQUM7WUFDcEYsT0FBTztTQUNSO1FBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxDQUFDO2lCQUNuRSxJQUFJLENBQUMsVUFBQyxRQUFRO2dCQUNiLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGFBQWEsQ0FBQyxhQUFhLEVBQUUsSUFBSSxFQUFFLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxJQUFJLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxFQUF6QixDQUF5QixDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNoSCxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxrQ0FBa0MsRUFBRSxDQUFDLENBQUM7WUFDN0UsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsR0FBRztnQkFDWCxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxJQUFJLHNCQUFzQixFQUFFLENBQUMsQ0FBQztZQUNoRixDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxPQUFPLElBQUksYUFBYSxDQUFDLFVBQVUsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLGtDQUFrQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1NBQ3RIO1FBQ0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7SUFDOUIsQ0FBQztJQUVELGdDQUFnQztJQUN6Qix5REFBd0IsR0FBL0IsVUFBZ0MsSUFBeUIsSUFBVSxVQUFVLENBQUMsY0FBUSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUVqSCx3REFBdUIsR0FBOUI7UUFBQSxpQkFjQztRQWJDLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsc0NBQXNDLEVBQUUsQ0FBQztpQkFDeEUsSUFBSSxDQUFDLFVBQUMsUUFBUTtnQkFDYixLQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxhQUFhLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsSUFBSSxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsRUFBekIsQ0FBeUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDaEgsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsOENBQThDLEVBQUUsQ0FBQyxDQUFDO1lBQ3pGLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFDLEdBQW9CO2dCQUM1QixRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxJQUFJLCtCQUErQixFQUFFLENBQUMsQ0FBQztZQUN2RixDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxhQUFhLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxzQ0FBc0MsRUFBRSxFQUFFLENBQUMsQ0FBQztTQUNySDtRQUVELElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO0lBQzlCLENBQUM7SUFFTSxzREFBcUIsR0FBNUIsVUFBNkIsQ0FBd0I7O1FBQ25ELElBQU0sU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxNQUFBLElBQUksQ0FBQyxRQUFRLDBDQUFFLFFBQWtDLENBQUEsQ0FBQztRQUMzRixJQUFJLENBQUMsU0FBUyxFQUFFO1lBQUUsT0FBTztTQUFFO1FBQzNCLElBQUksUUFBQyxJQUFJLENBQUMsd0JBQXdCLDBDQUFFLE1BQU0sQ0FBQSxFQUFFO1lBQzFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN6QjthQUFNO1lBQ0wsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQUMsS0FBYSxJQUFPLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUN6RztJQUNILENBQUM7SUFFTSwwREFBeUIsR0FBaEMsVUFBaUMsQ0FBK0I7UUFDOUQsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNuQixJQUFJLENBQUMsd0JBQXdCLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1NBQ25FO0lBQ0gsQ0FBQztJQUVNLGtEQUFpQixHQUF4QixVQUF5QixJQUF3Qjs7UUFDL0MsSUFBSSxDQUFDLFdBQVcsU0FBRyxJQUFJLENBQUMsZ0JBQWdCLDBDQUFFLFdBQVcsRUFBRSxDQUFDO1FBQ3hELElBQUksUUFBQyxJQUFJLENBQUMsV0FBVywwQ0FBRSxJQUFJLEdBQUUsSUFBSSxDQUFDLENBQUUsSUFBSSxDQUFDLEtBQWEsQ0FBQyxhQUFhLFlBQVksYUFBYSxDQUFDLEVBQUU7WUFDOUYsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDM0I7SUFDSCxDQUFDO0lBRU0sb0RBQW1CLEdBQTFCLFVBQTJCLFVBQThCO1FBQ3ZELElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUM1QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBQztZQUM5QixJQUFJLENBQUMscUJBQXFCLEdBQUcsYUFBYSxDQUFDLEtBQUssQ0FBQztZQUNqRCxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUM7WUFDaEIsSUFBTSxTQUFTLEdBQUcsT0FBTyxDQUFDO1lBQzFCLElBQUksQ0FBQyxVQUFVLEdBQU0sVUFBVSxDQUFDLElBQUksU0FBSSxTQUFXLENBQUM7WUFDcEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsRUFBL0IsQ0FBK0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0gsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxFQUFoQyxDQUFnQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakYsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUM5QyxJQUFJLENBQUMsVUFBVSxHQUFNLFVBQVUsQ0FBQyxJQUFJLFNBQUksU0FBUyxTQUFJLE9BQVMsQ0FBQztnQkFDL0QsT0FBTyxJQUFJLENBQUMsQ0FBQzthQUNkO1NBQ0Y7SUFDSCxDQUFDO0lBRU0sbURBQWtCLEdBQXpCLFVBQTBCLFVBQThCO1FBQ3RELElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUM1QyxJQUFJLENBQUMscUJBQXFCLEdBQUcsYUFBYSxDQUFDLElBQUksQ0FBQztZQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztZQUM3QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQyxFQUFFLENBQUM7WUFDOUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ2xDLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUNwQyxJQUFJLENBQUMsc0JBQXNCLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLEVBQS9CLENBQStCLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdILENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsRUFBaEMsQ0FBZ0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pGLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUMxQjtJQUNILENBQUM7SUFFTSw0Q0FBVyxHQUFsQjtRQUNFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBRUQsMkJBQTJCO0lBRTNCLHlCQUF5QjtJQUNqQixrREFBaUIsR0FBekIsVUFBMEIsV0FBbUI7UUFBbkIsNEJBQUEsRUFBQSxtQkFBbUI7UUFDM0MsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDO1FBQ3BELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsRUFBL0IsQ0FBK0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMvSSxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxTQUFTLEVBQWhDLENBQWdDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUM7UUFDdEQsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQzNELElBQUksV0FBVyxFQUFFO1lBQ2YsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDNUI7SUFDSCxDQUFDO0lBRU8sb0RBQW1CLEdBQTNCO1FBQUEsaUJBd0JDO1FBdkJDLElBQU0sV0FBVyxHQUFXLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3hGLElBQUksSUFBSSxDQUFDLHVCQUF1QixJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDMUUsSUFBSSxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxLQUFLLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2xFLE9BQU87YUFDUjtZQUNELFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsQ0FBQztZQUNyRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsRUFBRSxXQUFXLGFBQUEsRUFBRSxDQUFDLENBQUM7U0FDM0M7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN2QixXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsRUFBRSxXQUFXLGFBQUEsRUFBRSxDQUFDLENBQUM7U0FDM0M7UUFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDO2lCQUN4RCxJQUFJLENBQUMsVUFBQyxnQkFBZ0I7Z0JBQ3JCLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGFBQWEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQztnQkFDaEYsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsNkNBQTZDLEVBQUUsQ0FBQyxDQUFDO1lBQ3hGLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDUCxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxnQ0FBZ0MsRUFBRSxDQUFDLENBQUM7WUFDekUsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsYUFBYSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUMsQ0FBQztTQUM3RjtJQUNILENBQUM7SUFFTyx5REFBd0IsR0FBaEM7UUFBQSxpQkFhQztRQVpDLElBQU0sZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsc0JBQXNCLENBQUMsT0FBTyxDQUFDLFVBQUEsWUFBWTtZQUM5QyxJQUFNLFlBQVksR0FBRyxLQUFJLENBQUMsaUJBQWlCLENBQUMsWUFBWSxDQUFDLHFCQUFxQixFQUFFLFlBQVksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUMzRyxZQUFZLENBQUMsUUFBUSxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUM7WUFDOUMsSUFBSSxZQUFZLENBQUMsWUFBWSxFQUFFO2dCQUM3QixLQUFJLENBQUMsa0JBQWtCLEdBQUcsWUFBWSxDQUFDO2FBQ3hDO2lCQUFNO2dCQUNMLGVBQWUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDcEM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLEdBQUcsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsZUFBZTtZQUNoRixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDbEUsQ0FBQztJQUVPLGtEQUFpQixHQUF6QixVQUEwQixZQUFpQixFQUFFLFlBQW1CO1FBQW5CLDZCQUFBLEVBQUEsbUJBQW1CO1FBQzlELGtEQUFrRDtRQUNsRCxpRkFBaUY7UUFDakYsNEZBQTRGO1FBQzVGLE1BQU07UUFDTiwySkFBMko7UUFDM0osOERBQThEO1FBQzlELHVEQUF1RDtRQUN2RCxvRUFBb0U7UUFDcEUsaUxBQWlMO1FBQ2pMLDRCQUE0QjtJQUM5QixDQUFDO0lBRU8saURBQWdCLEdBQXhCO1FBQ0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRTtZQUN2QyxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxJQUFJLEVBQUUsZ0NBQWdDLEVBQUUsQ0FBQyxDQUFDO2FBQ3JLO1lBQ0QsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsRUFBRTtnQkFDMUYsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsZUFBZSxDQUFDLENBQUM7d0JBQ2xELHNCQUFzQixDQUFDLENBQUMsQ0FBQyx1QkFBdUIsRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBRSx1Q0FBdUM7aUJBQzFILENBQUMsQ0FBQzthQUNKO1NBQ0Y7SUFDSCxDQUFDO0lBRU8sNkNBQVksR0FBcEIsVUFBcUIsWUFBZ0M7UUFBckQsaUJBZ0JDO1FBZkMsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztpQkFDekMsSUFBSSxDQUFDO2dCQUNKLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLHFDQUFxQyxFQUFFLENBQUMsQ0FBQztnQkFDOUUsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsYUFBYSxDQUFDLGFBQWEsRUFBRSxJQUFJLFdBQU0sS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsRUFBRSxLQUFLLFlBQVksQ0FBQyxFQUFFLEVBQTNCLENBQTJCLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDbEksS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDaEMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNQLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLDZDQUE2QyxFQUFFLENBQUMsQ0FBQztZQUN0RixDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxhQUFhLENBQUMsVUFBVSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsQ0FBQyxDQUFDO1lBQzdFLHVDQUF1QztZQUN2QyxJQUFJLENBQUMsUUFBUSxZQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEVBQUUsS0FBSyxZQUFZLENBQUMsRUFBRSxFQUEzQixDQUEyQixDQUFDLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUMvQjtJQUNILENBQUM7SUFFTyxnREFBZSxHQUF2QixjQUFrQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxNQUFNLEVBQXdCLENBQUMsQ0FBQyxDQUFDO0lBRTlGLHFEQUFvQixHQUE1QixjQUF1QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLElBQUksbUJBQW1CLENBQUMsQ0FBQyxDQUFDO0lBRTlGLG1EQUFrQixHQUExQjtRQUNFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztJQUN0QyxDQUFDO0lBRU8sdURBQXNCLEdBQTlCO1FBQUEsaUJBbUJDO1FBbEJDLElBQU0sTUFBTSxHQUFHLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFFBQVEsRUFBYixDQUFhLENBQUMsQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO1FBQzFCLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxVQUFVO1lBQ3ZCLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7Z0JBQ3hCLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsT0FBTztnQkFDMUMsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsTUFBTSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLFFBQVEsS0FBSyxVQUFVLEVBQXpCLENBQXlCLENBQUM7YUFDL0QsQ0FBQztRQUpGLENBSUUsQ0FBQyxDQUFDO1FBQ04scURBQXFEO1FBQ3JELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDLEVBQUUsQ0FBQztZQUNwRCxJQUFJLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLFNBQVMsRUFBRTtnQkFDN0IsT0FBTyxDQUFDLENBQUMsQ0FBQzthQUNYO1lBQ0QsSUFBSSxDQUFDLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxTQUFTLEVBQUU7Z0JBQzdCLE9BQU8sQ0FBQyxDQUFDO2FBQ1Y7WUFDRCxPQUFPLENBQUMsQ0FBQztRQUNYLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLHdEQUF1QixHQUEvQixVQUFnQyxVQUE4QjtRQUM1RCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsRUFBRSxDQUFDO1FBQ25DLElBQUksVUFBVSxDQUFDLGVBQWUsRUFBRTtZQUM5QixJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUNoQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUMsSUFBSSxDQUFDLEtBQWdCLENBQUMsV0FBVyxFQUFFLEtBQUssZ0JBQWdCLEVBQXpELENBQXlELENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFhLENBQUMsQ0FBQztTQUN0SDtRQUNELElBQUksVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUN4QixJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUNoQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUMsSUFBSSxDQUFDLEtBQWdCLENBQUMsV0FBVyxFQUFFLEtBQUssY0FBYyxFQUF2RCxDQUF1RCxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBYSxDQUFDLENBQUM7U0FDcEg7SUFDSCxDQUFDO0lBRU8saURBQWdCLEdBQXhCLFVBQXlCLFFBQWdCO1FBQ3ZDLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLElBQUksS0FBSyxRQUFRLEVBQXRCLENBQXNCLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEYsT0FBTyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMxQyxDQUFDO0lBRU8sNENBQVcsR0FBbkI7UUFDRSxPQUFPLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUM7SUFDNUQsQ0FBQztJQUVPLHFEQUFvQixHQUE1QixVQUE2QixRQUFnQjs7UUFDM0MsSUFBSSxPQUFBLElBQUksQ0FBQyx1QkFBdUIsMENBQUUsRUFBRSxNQUFLLFFBQVEsRUFBRTtZQUNqRCxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSw2REFBNkQsRUFBRSxDQUFDLENBQUM7WUFDdEcsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVPLG1FQUFrQyxHQUExQztRQUNFLE9BQU87WUFDTCxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDakIsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQ3JCLGVBQWUsRUFBRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDO1lBQ3pFLGFBQWEsRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDO1lBQ3JFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixjQUFjLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFDLElBQVksQ0FBQyxRQUFRLEVBQXRCLENBQXNCLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEgsVUFBVSxFQUFFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRO1lBQ2xFLEtBQUssRUFBRSxJQUFJLENBQUMsMEJBQTBCLEVBQUU7U0FDekMsQ0FBQTtJQUNILENBQUM7SUFFTyx1RUFBc0MsR0FBOUM7UUFDRSxPQUFPO1lBQ0wsRUFBRSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFO1lBQ25DLElBQUksRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSTtZQUN2QyxRQUFRLEVBQUUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVE7WUFDL0MsZUFBZSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxlQUFlO1lBQzdELGFBQWEsRUFBRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUztZQUNyRCxjQUFjLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFDLElBQVksQ0FBQyxRQUFRLEVBQXRCLENBQXNCLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEgsVUFBVSxFQUFFLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRO1lBQ2pELEtBQUssRUFBRSxJQUFJLENBQUMsMEJBQTBCLEVBQUU7U0FDekMsQ0FBQTtJQUNILENBQUM7SUFFTywyREFBMEIsR0FBbEM7UUFDRSxJQUFNLDJCQUEyQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsVUFBVSxFQUFmLENBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdGLElBQU0sR0FBRyxHQUFXLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUNuRCwyQkFBMkIsYUFBM0IsMkJBQTJCLHVCQUEzQiwyQkFBMkIsQ0FBRSxxQkFBcUIsRUFBRSxjQUFjLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3ZHLElBQU0sZ0JBQWdCLEdBQUcsMkJBQTJCLGFBQTNCLDJCQUEyQix1QkFBM0IsMkJBQTJCLENBQUUscUJBQXFCLENBQUMsS0FBSyxFQUF1QixDQUFDO1FBQ3pHLGdCQUFnQixDQUFDLGNBQWMsR0FBRywyQkFBMkIsYUFBM0IsMkJBQTJCLHVCQUEzQiwyQkFBMkIsQ0FBRSxxQkFBcUIsQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUMsVUFBQyxJQUFJO1lBQ25ILE9BQU8sSUFBSSxDQUFDLFNBQVMsS0FBSyxzQkFBc0IsQ0FBQyxTQUFTLElBQUssSUFBWSxDQUFDLE9BQU8sS0FBSyxPQUFPLENBQUM7UUFDbEcsQ0FBQyxDQUFDLENBQUM7UUFDSCxnQkFBZ0IsQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEYsZ0JBQWdCLENBQUMsT0FBTyxHQUFHLDJCQUEyQixhQUEzQiwyQkFBMkIsdUJBQTNCLDJCQUEyQixDQUFFLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoRyxnQkFBZ0IsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUNuRCxnQkFBZ0IsQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDaEUsZ0JBQWdCLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNGLE9BQU87WUFDTCxTQUFTLEVBQUUsZ0JBQWdCO1lBQzNCLG9CQUFvQixFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUU7WUFDbkUsdUJBQXVCLEVBQUUsRUFBRTtTQUM1QixDQUFDO0lBQ0osQ0FBQztJQUVPLHdEQUF1QixHQUEvQjs7UUFDRSxJQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUMzRCxNQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQyxJQUFJLENBQUMsS0FBZ0IsQ0FBQyxXQUFXLEVBQUUsS0FBSyxnQkFBZ0IsRUFBekQsQ0FBeUQsQ0FBQyxDQUFDLENBQUMsQ0FBQywwQ0FBRSxHQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM3SCxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsV0FBVyxFQUFFLEtBQUssUUFBUSxDQUFDO1FBQ3hFLE9BQU8sZUFBZSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMzQyxDQUFDO0lBR08scURBQW9CLEdBQTVCLFVBQTZCLEdBQWE7UUFDeEMsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxLQUFLLEtBQUssZ0JBQWdCLEVBQTVCLENBQTRCLENBQUMsQ0FBQztRQUNqRixPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFLElBQUksT0FBQSxTQUFTLENBQUMsR0FBRyxLQUFLLEVBQUUsRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFTyxtREFBa0IsR0FBMUIsVUFBMkIsR0FBYTtRQUN0QyxJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEtBQUssS0FBSyxjQUFjLEVBQTFCLENBQTBCLENBQUMsQ0FBQztRQUM3RSxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBQSxFQUFFLElBQUksT0FBQSxPQUFPLENBQUMsR0FBRyxLQUFLLEVBQUUsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO0lBQzVDLENBQUM7O2dCQXZibUMsY0FBYztnQkFBa0IsTUFBTTtnQkFBMEIsaUJBQWlCOztJQXJFckY7UUFBL0IsU0FBUyxDQUFDLG1CQUFtQixDQUFDOzREQUFzQjtJQUdyRDtRQURDLEtBQUssRUFBRTswREFJUDtJQUlEO1FBREMsS0FBSyxFQUFFOytEQU9QO0lBR1E7UUFBUixLQUFLLEVBQUU7a0VBT1A7SUFFUTtRQUFSLEtBQUssRUFBRTt1RUFBb0M7SUFDbkM7UUFBUixLQUFLLEVBQUU7a0VBQTBFO0lBRXhFO1FBQVQsTUFBTSxFQUFFOzZEQUF1QztJQUN0QztRQUFULE1BQU0sRUFBRTtzRUFBZ0Q7SUFsQzlDLHNCQUFzQjtRQUxsQyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsb0JBQW9CO1lBQzlCLHkxU0FBOEM7O1NBRS9DLENBQUM7T0FDVyxzQkFBc0IsQ0FtZ0JsQztJQUFELDZCQUFDO0NBQUEsQUFuZ0JELElBbWdCQztTQW5nQlksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGR4QnV0dG9uIGZyb20gJ2RldmV4dHJlbWUvdWkvYnV0dG9uJztcclxuaW1wb3J0IGR4VHJlZVZpZXcgZnJvbSAnZGV2ZXh0cmVtZS91aS90cmVlX3ZpZXcnO1xyXG5pbXBvcnQgdmFsaWRhdGlvbkVuZ2luZSBmcm9tICdkZXZleHRyZW1lL3VpL3ZhbGlkYXRpb25fZW5naW5lJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtcywgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQ0dGRXZlbnRzRW51bSwgQ0dGU3RvcmFnZUtleXMgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvZW51bXMnO1xyXG5pbXBvcnQgeyBDR0ZVdGlsaXR5U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NnZi11dGlsaXR5LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRHhUcmVlVmlld0NvbXBvbmVudCB9IGZyb20gJ2RldmV4dHJlbWUtYW5ndWxhcic7XHJcbmltcG9ydCB7IEdyaWRDb21wb25lbnRJbnN0YW5jZXMsIFRvYXN0ZXJSZXNwb25zZSB9IGZyb20gJy4uL2NvbnRyYWN0cy9jb21tb24tZ3JpZC1mcmFtZXdvcmsnO1xyXG5pbXBvcnQgeyBHcmlkU3RhdGVJbmZvLCBHcmlkVmlld0luZm8sIFZhbHVlU2V0LCBJVmlld0RhdGFTb3VyY2UgfSBmcm9tICcuLi9jb250cmFjdHMvdmlldy1zZWxlY3Rpb24nO1xyXG5pbXBvcnQgeyBQQklHcmlkQ29sdW1uLCBHcmlkU3RhdGVTYXZlSW5mbyB9IGZyb20gJy4uL2NvbnRyYWN0cy9ncmlkJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRleHRCb3hCYXNlT3B0aW9ucywgVGV4dEJveENvbnRlbnRSZWFkeSwgVGV4dEJveFZhbHVlQ2hhbmdlLCBUcmVlVmlld0NvbnRlbnRSZWFkeSwgVHJlZVZpZXdJdGVtU2VsZWN0aW9uQ2hhbmdlZCB9IGZyb20gJy4uL2NvbnRyYWN0cy9kZXZleHRyZW1lV2lkZ2V0cyc7XHJcbmltcG9ydCB7IFZpZXdTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJy4uL21vZGVscy92aWV3LXNlbGVjdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IGFwcFRvYXN0IH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMnO1xyXG5pbXBvcnQgeyBjb25maXJtIH0gZnJvbSAnZGV2ZXh0cmVtZS91aS9kaWFsb2cnO1xyXG5pbXBvcnQgeyBjcnVkT3BlcmF0aW9uLCBjdXN0b21BY3Rpb25Db2x1bW5JbmZvIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2NvbnN0YW50cyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS12aWV3LXNlbGVjdGlvbicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL3ZpZXctc2VsZWN0aW9uLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi92aWV3LXNlbGVjdGlvbi5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWaWV3U2VsZWN0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQFZpZXdDaGlsZChEeFRyZWVWaWV3Q29tcG9uZW50KSB0cmVlVmlldzogZHhUcmVlVmlldztcclxuXHJcbiAgQElucHV0KClcclxuICBzZXQgdmlld0xpc3QobGF5b3V0TGlzdCkge1xyXG4gICAgdGhpcy5sYXlvdXRMaXN0ID0gbGF5b3V0TGlzdDtcclxuICAgIHRoaXMucHJlcGFyZUdyb3VwTGF5b3V0TGlzdCgpO1xyXG4gIH1cclxuICBnZXQgdmlld0xpc3QoKSB7IHJldHVybiB0aGlzLmxheW91dExpc3Q7IH1cclxuXHJcbiAgQElucHV0KClcclxuICBzZXQgc2VsZWN0ZWRUaGVtZSh0aGVtZU5hbWU6IHN0cmluZykge1xyXG4gICAgaWYgKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oJ3RoZW1lJykpIHtcclxuICAgICAgdGhpcy5kZWZhdWx0VGhlbWUgPSBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCd0aGVtZScpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5kZWZhdWx0VGhlbWUgPSB0aGVtZU5hbWU7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldCBzZWxlY3RlZFRoZW1lKCkgeyByZXR1cm4gdGhpcy5kZWZhdWx0VGhlbWU7IH1cclxuXHJcbiAgQElucHV0KCkgc2V0IGdyaWRJbnN0YW5jZUxpc3QoZ3JpZEluc3RhbmNlTGlzdDogQXJyYXk8R3JpZENvbXBvbmVudEluc3RhbmNlcz4pIHtcclxuICAgIHRoaXMudW5pcXVlR3JpZEluc3RhbmNlTGlzdCA9IFtdO1xyXG4gICAgZ3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWQgPT4ge1xyXG4gICAgICBpZiAoIXRoaXMudW5pcXVlR3JpZEluc3RhbmNlTGlzdC5maWx0ZXIoaXRlbSA9PiBpdGVtLmdyaWROYW1lID09PSBncmlkLmdyaWROYW1lICYmIGl0ZW0uaXNNYXN0ZXJHcmlkID09PSBncmlkLmlzTWFzdGVyR3JpZCkubGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy51bmlxdWVHcmlkSW5zdGFuY2VMaXN0LnB1c2goZ3JpZCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBnZXQgZ3JpZEluc3RhbmNlTGlzdCgpIHsgcmV0dXJuIHRoaXMudW5pcXVlR3JpZEluc3RhbmNlTGlzdDsgfVxyXG4gIEBJbnB1dCgpIHB1YmxpYyBpc0dyaWRCb3JkZXJWaXNpYmxlID0gZmFsc2U7XHJcbiAgQElucHV0KCkgcHVibGljIHZpZXdEYXRhU291cmNlOiBJVmlld0RhdGFTb3VyY2UgPSBuZXcgT2JqZWN0KCkgYXMgSVZpZXdEYXRhU291cmNlO1xyXG5cclxuICBAT3V0cHV0KCkgcHVibGljIHZpZXdFdmVudCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGNsb3NlQ3VycmVudEZseU91dCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgZGVmYXVsdHMgPSBbXTtcclxuICBjdXJyZW50U2VsZWN0TGF5b3V0SW5mbyA9IG5ldyBPYmplY3QoKSBhcyBWaWV3U2VsZWN0aW9uTW9kZWw7XHJcbiAgZHJvcERvd25PcHRpb25zID0gW3sgdmFsdWU6IDEsIG5hbWU6ICdTYXZlIEFzJyB9XTtcclxuICBkZWZhdWx0VGhlbWUgPSAnbnAuY29tcGFjdCc7XHJcbiAgZ3JpZExheW91dFNldHRpbmdzOiBhbnkgPSB7XHJcbiAgICBjb2xEZWY6IFtdLFxyXG4gICAgZmlsdGVyczogW10sXHJcbiAgICBnZXRDb21iaW5lZEZpbHRlcjogW10sXHJcbiAgICBzdW1tYXJ5OiBbXSxcclxuICAgIGNoaWxkR3JpZExheW91dDogW10sXHJcbiAgICBncmlkTmFtZTogJydcclxuICB9O1xyXG4gIGZpbHRlckxheW91dE5hbWUgPSAnJztcclxuICBmaWx0ZXJWYWx1ZSA9IG51bGw7XHJcbiAgZ3JvdXBMYXlvdXRMaXN0ID0gW107XHJcbiAgaXNMYXlvdXROb25lID0gZmFsc2U7XHJcbiAgaXNQaW5uZWQgPSBmYWxzZTtcclxuICBsYXlvdXREZWZhdWx0RGF0YTogQXJyYXk8VmFsdWVTZXQ+ID0gW3sga2V5OiAxLCB2YWx1ZTogJ1VzZXIgRGVmYXVsdCcgfSwgeyBrZXk6IDIsIHZhbHVlOiAnR2xvYmFsIERlZmF1bHQnIH1dO1xyXG4gIGxheW91dElkOiBudW1iZXI7XHJcbiAgbGF5b3V0TGlzdDogVmlld1NlbGVjdGlvbk1vZGVsW10gPSBbXTtcclxuICBsYXlvdXROYW1lOiBzdHJpbmc7XHJcbiAgcm91dGVyU3Vic2NyaWJlOiBTdWJzY3JpcHRpb24gPSBudWxsO1xyXG4gIHNhdmVCdXR0b25PcHRpb25zOiBkeEJ1dHRvbjtcclxuICBzZWxlY3RlZENydWRPcGVyYXRpb246IHN0cmluZztcclxuICBzZWxlY3RlZExheW91dERlZmF1bHRJZHM6IEFycmF5PG51bWJlcj4gPSBbXTtcclxuICBzZWxlY3RlZFZpZXdWaXNpYmlsaXR5ID0gJ1ByaXZhdGUnO1xyXG4gIHNob3dGaWx0ZXJDb250cm9sSW5wdXQgPSBmYWxzZTtcclxuICBzaG93TW9kYWxQb3BVcCA9IGZhbHNlO1xyXG4gIHNob3dTYXZlQnV0dG9uTWVudUxpc3QgPSBmYWxzZTtcclxuICB0aGVtZUxpc3Q6IEFycmF5PFZhbHVlU2V0PiA9IFt7IGtleTogJ2xpZ2h0LnJlZ3VsYXInLCB2YWx1ZTogJ0xpZ2h0JyB9LCB7IGtleTogJ2xpZ2h0LmNvbXBhY3QnLCB2YWx1ZTogJ0xpZ2h0IENvbXBhY3QnIH1dO1xyXG4gIHVuaXF1ZUdyaWRJbnN0YW5jZUxpc3Q6IEFycmF5PEdyaWRDb21wb25lbnRJbnN0YW5jZXM+ID0gW107XHJcbiAgdmlld05hbWVWYWxpZGF0aW9uUGF0dGVybiA9IC9eW2EtekEtWjAtOS1fXFxzXSskLztcclxuICB2aWV3VGl0bGUgPSAnQWRkIE5ldyBWaWV3JztcclxuICB2aWV3VmlzaWJpbGl0eVR5cGVzID0gWydQcml2YXRlJywgJ1B1YmxpYyddO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFjdGl2YXRlZFJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSwgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSB1dGlsaXR5U2VydmljZTogQ0dGVXRpbGl0eVNlcnZpY2UpIHsgfVxyXG5cclxuICAvLyNyZWdpb24gQW5ndWxhciBMaWZlQ3ljbGUgZXZlbnRcclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGNvbnN0IGxheW91dElkID0gTnVtYmVyKHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucXVlcnlQYXJhbXNbJ2xheW91dCddKTtcclxuICAgIHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8gPSB0aGlzLmxheW91dExpc3QuZmlsdGVyKChhcnJJdGVtKSA9PiB7XHJcbiAgICAgIHJldHVybiBhcnJJdGVtLmlkID09PSBsYXlvdXRJZDtcclxuICAgIH0pWzBdO1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8pIHtcclxuICAgICAgdGhpcy5sYXlvdXROYW1lID0gdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5uYW1lO1xyXG4gICAgfSBlbHNlIGlmICghdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mbykge1xyXG4gICAgICB0aGlzLnJlc2V0TGF5b3V0SW5mbygpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zZXRTZWxlY3RlZExheW91dCgpO1xyXG4gICAgdGhpcy51cGRhdGVUaGVtZVNlbGVjdGlvbigpO1xyXG4gIH1cclxuICAvLyNlbmRyZWdpb24gQW5ndWxhciBMaWZlQ3ljbGUgZXZlbnRcclxuXHJcbiAgLy8jcmVnaW9uIFB1YmxpYyBNZXRob2RzXHJcbiAgcHVibGljIG9uVmlld1NlbGVjdGlvbkNsaWNrKGxheW91dEl0ZW06IFZpZXdTZWxlY3Rpb25Nb2RlbCk6IHZvaWQge1xyXG4gICAgdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mbyA9IGxheW91dEl0ZW07XHJcbiAgICB0aGlzLnNldFNlbGVjdGVkTGF5b3V0KHRydWUpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGRlbGV0ZVNlbGVjdGVkTGF5b3V0KGxheW91dEl0ZW06IFZpZXdTZWxlY3Rpb25Nb2RlbCk6IHZvaWQge1xyXG4gICAgY29uc3QgZGF0YVRvRGVsZXRlID0ge1xyXG4gICAgICBsYXlvdXRJZDogbGF5b3V0SXRlbS5pZCA/IChsYXlvdXRJdGVtLmlkICE9PSAtMSA/IGxheW91dEl0ZW0uaWQgOiAwKSA6IDAsXHJcbiAgICAgIGxheW91dE5hbWU6IGxheW91dEl0ZW0ubmFtZSxcclxuICAgICAgaXNQdWJsaWM6IGxheW91dEl0ZW0uaXNQdWJsaWMsXHJcbiAgICAgIGlzR2xvYmFsTGF5b3V0OiBsYXlvdXRJdGVtLmlzR2xvYmFsRGVmYXVsdFxyXG4gICAgfTtcclxuICAgIGNvbnN0IGNvbmZpcm1EaWFsb2c6IGFueSA9IGNvbmZpcm0oYEFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBkZWxldGUgdmlldyAoJHtsYXlvdXRJdGVtLm5hbWV9KT9gLCAnRGVsZXRlIFZpZXcnKTtcclxuICAgIGNvbmZpcm1EaWFsb2cuZG9uZSgoZGlhbG9nUmVzdWx0OiBib29sZWFuKSA9PiB7XHJcbiAgICAgIGlmIChkaWFsb2dSZXN1bHQpIHtcclxuICAgICAgICB0aGlzLmRlbGV0ZUxheW91dChsYXlvdXRJdGVtKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgLy8gdGhpcy5vdmVycmlkZUNvbmZpcm1Qb3B1cFRoZW1lKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdXBkYXRlVmlld1Zpc2liaWxpdHkobGF5b3V0SXRlbTogVmlld1NlbGVjdGlvbk1vZGVsKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5hY3Rpb25PblNlbGVjdGVkVmlldyhsYXlvdXRJdGVtLmlkKSkge1xyXG4gICAgICBjb25zdCBsYXlvdXRBY2Nlc3NhYmlsaXR5R3JvdXAgPSAobGF5b3V0SXRlbSA/ICFsYXlvdXRJdGVtLmlzUHVibGljIDogdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5pc1B1YmxpYykgPyAnUHVibGljJyA6ICdQcml2YXRlJztcclxuICAgICAgY29uc3QgY29uZmlybURpYWxvZzogYW55ID0gY29uZmlybShgQXJlIHlvdSBzdXJlIHlvdSB3YW50IHRvIGNoYW5nZSB2aWV3IHRvICR7bGF5b3V0QWNjZXNzYWJpbGl0eUdyb3VwfSA/YCwgJ1NhdmUgVmlldycpO1xyXG4gICAgICBjb25maXJtRGlhbG9nLmRvbmUoKHJlc3VsdDogYm9vbGVhbikgPT4ge1xyXG4gICAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICAgIGxheW91dEl0ZW0uaXNQdWJsaWMgPSAhbGF5b3V0SXRlbS5pc1B1YmxpYztcclxuICAgICAgICAgIGlmICh0aGlzLnZpZXdEYXRhU291cmNlKSB7XHJcbiAgICAgICAgICAgIGlmIChsYXlvdXRJdGVtLmlzR2xvYmFsRGVmYXVsdCkge1xyXG4gICAgICAgICAgICAgIGFwcFRvYXN0KHsgbWVzc2FnZTogJ0dsb2JhbCBkZWZhdWx0IGNhblxcJ3Qgc2V0IGFzIHByaXZhdGUgdmlldy4nLCB0eXBlOiAnZXJyb3InIH0pO1xyXG4gICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnZpZXdEYXRhU291cmNlLnVwZGF0ZVZpZXdWaXNpYmlsaXR5KGxheW91dEl0ZW0uaWQsIGxheW91dEl0ZW0uaXNQdWJsaWMpXHJcbiAgICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGVkVmlld1Zpc2liaWxpdHkgPSBsYXlvdXRJdGVtLmlzUHVibGljID9cclxuICAgICAgICAgICAgICAgICAgdGhpcy52aWV3VmlzaWJpbGl0eVR5cGVzLmZpbHRlcihpdGVtID0+IGl0ZW0udG9Mb3dlckNhc2UoKSA9PT0gJ3B1YmxpYycpWzBdXHJcbiAgICAgICAgICAgICAgICAgIDogdGhpcy52aWV3VmlzaWJpbGl0eVR5cGVzLmZpbHRlcihpdGVtID0+IGl0ZW0udG9Mb3dlckNhc2UoKSA9PT0gJ3ByaXZhdGUnKVswXTtcclxuICAgICAgICAgICAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ3N1Y2Nlc3MnLCBtZXNzYWdlOiByZXNwb25zZT8ubWVzc2FnZSB8fCAnVmlldyB2aXNpYmlsaXR5IHVwZGF0ZWQuJyB9KTtcclxuICAgICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICdzdWNjZXNzJywgbWVzc2FnZTogZXJyPy5NZXNzYWdlIHx8ICdVbmFibGUgdG8gdXBkYXRlIHZpZXcgdmlzaWJpbGl0eS4nIH0pO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy52aWV3RXZlbnQuZW1pdCh7IGV2ZW50OiBDR0ZFdmVudHNFbnVtLnVwZGF0ZVZpZXdWaXNpYmlsaXR5LCBkYXRhOiBsYXlvdXRJdGVtIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgYWRkTmV3VmlldygpOiB2b2lkIHtcclxuICAgIHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWQgPT4ge1xyXG4gICAgICBncmlkLmdyaWRDb21wb25lbnRJbnN0YW5jZS5iZWdpblVwZGF0ZSgpO1xyXG4gICAgICBncmlkLmdyaWRDb21wb25lbnRJbnN0YW5jZS5jb2xsYXBzZUFsbCgtMSk7XHJcbiAgICAgIGdyaWQuZ3JpZENvbXBvbmVudEluc3RhbmNlLmNsZWFyR3JvdXBpbmcoKTtcclxuICAgICAgZ3JpZC5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZ2V0VmlzaWJsZUNvbHVtbnMoKS5mb3JFYWNoKChjb2xJdGVtOiBQQklHcmlkQ29sdW1uKSA9PiB7XHJcbiAgICAgICAgZ3JpZC5ncmlkQ29tcG9uZW50SW5zdGFuY2UuY29sdW1uT3B0aW9uKGNvbEl0ZW0uZGF0YUZpZWxkLCB7IHZpc2libGU6IGZhbHNlIH0pO1xyXG4gICAgICB9KTtcclxuICAgICAgZ3JpZC5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZW5kVXBkYXRlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLnZpZXdUaXRsZSA9ICdBZGQgTmV3IFZpZXcnO1xyXG4gICAgdGhpcy5zZWxlY3RlZENydWRPcGVyYXRpb24gPSBjcnVkT3BlcmF0aW9uLmFkZDtcclxuICAgIHRoaXMubGF5b3V0SWQgPSAwO1xyXG4gICAgdGhpcy5zaG93TW9kYWxQb3BVcCA9IHRydWU7XHJcbiAgICB0aGlzLmxheW91dE5hbWUgPSAnJztcclxuICAgIHRoaXMuaXNQaW5uZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuc2VsZWN0ZWRMYXlvdXREZWZhdWx0SWRzID0gW107XHJcbiAgICB0aGlzLnNlbGVjdGVkVmlld1Zpc2liaWxpdHkgPSB0aGlzLnZpZXdWaXNpYmlsaXR5VHlwZXMuZmlsdGVyKGl0ZW0gPT4gaXRlbS50b0xvd2VyQ2FzZSgpID09PSAncHJpdmF0ZScpWzBdO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb3NlUG9wdXBXaW5kb3coKTogdm9pZCB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ3J1ZE9wZXJhdGlvbiA9ICcnO1xyXG4gICAgaWYgKHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucXVlcnlQYXJhbXMubGF5b3V0ID09PSAnMCcpIHtcclxuICAgICAgY29uc3QgcXVlcnlQYXJhbXMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmFjdGl2YXRlZFJvdXRlLnNuYXBzaG90LnF1ZXJ5UGFyYW1zKTtcclxuICAgICAgcXVlcnlQYXJhbXMubGF5b3V0ID0gdGhpcy5sYXlvdXRJZC50b1N0cmluZygpO1xyXG4gICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXSwgeyBxdWVyeVBhcmFtcyB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2V0U2VsZWN0ZWRMYXlvdXQoKTtcclxuICAgIH1cclxuICAgIHRoaXMuc2hvd01vZGFsUG9wVXAgPSBmYWxzZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzZWFyY2hCb3hJbml0aWFsaXplZChlOiBUZXh0Qm94QmFzZU9wdGlvbnMpOiB2b2lkIHsgc2V0VGltZW91dCgoKSA9PiB7IGUuY29tcG9uZW50LmZvY3VzKCk7IH0sIDApOyB9XHJcblxyXG4gIHB1YmxpYyBzYXZlQXNMYXlvdXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnNhdmVWaWV3SW5mb3JtYXRpb24oQ0dGRXZlbnRzRW51bS5zYXZlQXNWaWV3KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBzYXZlVmlld0luZm9ybWF0aW9uKGVudW1WYWw/OiBDR0ZFdmVudHNFbnVtKTogdm9pZCB7XHJcblxyXG4gICAgaWYgKCF0aGlzLmlzRm9ybVZhbGlkKCkpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICghdGhpcy5kZWZhdWx0T3B0aW9uVmFsaWRhdGlvbigpKSB7XHJcbiAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ3dhcm5pbmcnLCBtZXNzYWdlOiBgUHJpdmF0ZSBWaWV3IGNhbid0IGJlIG1hZGUgR2xvYmFsIERlZmF1bHRgIH0pO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy52aWV3RGF0YVNvdXJjZSkge1xyXG4gICAgICB0aGlzLnZpZXdEYXRhU291cmNlLmFkZFZpZXcodGhpcy5wcmVwYXJlTmV3TGF5b3V0SW5mb0JlZm9yZUVtaXR0aW5nKCkpXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnZpZXdFdmVudC5lbWl0KHsgZXZlbnQ6IENHRkV2ZW50c0VudW0udXBkYXRlTGF5b3V0cywgZGF0YTogcmVzcG9uc2UubWFwKHYgPT4gbmV3IFZpZXdTZWxlY3Rpb25Nb2RlbCh2KSkgfSk7XHJcbiAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICdzdWNjZXNzJywgbWVzc2FnZTogJ1ZpZXcgaGFzIGJlZW4gc2F2ZWQgc3VjY2Vzc2Z1bGx5JyB9KTtcclxuICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICdzdWNjZXNzJywgbWVzc2FnZTogZXJyLm1lc3NhZ2UgfHwgJ1VuYWJsZSB0byBzYXZlIHZpZXcuJyB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudmlld0V2ZW50LmVtaXQoeyBldmVudDogZW51bVZhbCB8fCBDR0ZFdmVudHNFbnVtLmFkZE5ld1ZpZXcsIGRhdGE6IHRoaXMucHJlcGFyZU5ld0xheW91dEluZm9CZWZvcmVFbWl0dGluZygpIH0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zaG93TW9kYWxQb3BVcCA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgLy8gVE9ETyB3aXRob3V0IHVzaW5nIHNldFRpbWVvdXRcclxuICBwdWJsaWMgb25MYXlvdXROYW1lQ29udGVudFJlYWR5KGFyZ3M6IFRleHRCb3hDb250ZW50UmVhZHkpOiB2b2lkIHsgc2V0VGltZW91dCgoKSA9PiB7IGFyZ3MuY29tcG9uZW50LmZvY3VzKCk7IH0sIDMwMCk7IH1cclxuXHJcbiAgcHVibGljIHNhdmVDdXJyZW50U2VsZWN0ZWRWaWV3KCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMudmlld0RhdGFTb3VyY2UpIHtcclxuICAgICAgdGhpcy52aWV3RGF0YVNvdXJjZS5zYXZlVmlldyh0aGlzLnByZXBhcmVDdXJyZW50TGF5b3V0SW5mb0JlZm9yZUVtaXR0aW5nKCkpXHJcbiAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnZpZXdFdmVudC5lbWl0KHsgZXZlbnQ6IENHRkV2ZW50c0VudW0udXBkYXRlTGF5b3V0cywgZGF0YTogcmVzcG9uc2UubWFwKHYgPT4gbmV3IFZpZXdTZWxlY3Rpb25Nb2RlbCh2KSkgfSk7XHJcbiAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICdzdWNjZXNzJywgbWVzc2FnZTogJ1NlbGVjdGVkIFZpZXcgaGFzIGJlZW4gdXBkYXRlZCBzdWNjZXNzZnVsbHkuJyB9KTtcclxuICAgICAgICB9KS5jYXRjaCgoZXJyOiBUb2FzdGVyUmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ2Vycm9yJywgbWVzc2FnZTogZXJyLm1lc3NhZ2UgfHwgJ1VuYWJsZSB0byBzYXZlIHNlbGVjdGVkIHZpZXcuJyB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudmlld0V2ZW50LmVtaXQoeyBldmVudDogQ0dGRXZlbnRzRW51bS5zYXZlU2VsZWN0ZWRWaWV3LCBkYXRhOiB0aGlzLnByZXBhcmVDdXJyZW50TGF5b3V0SW5mb0JlZm9yZUVtaXR0aW5nKCkgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5zaG93TW9kYWxQb3BVcCA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHN5bmNUcmVlVmlld1NlbGVjdGlvbihlPzogVHJlZVZpZXdDb250ZW50UmVhZHkpOiB2b2lkIHtcclxuICAgIGNvbnN0IGNvbXBvbmVudCA9IChlICYmIGUuY29tcG9uZW50KSB8fCAodGhpcy50cmVlVmlldz8uaW5zdGFuY2UgYXMgdW5rbm93biBhcyBkeFRyZWVWaWV3KTtcclxuICAgIGlmICghY29tcG9uZW50KSB7IHJldHVybjsgfVxyXG4gICAgaWYgKCF0aGlzLnNlbGVjdGVkTGF5b3V0RGVmYXVsdElkcz8ubGVuZ3RoKSB7XHJcbiAgICAgIGNvbXBvbmVudC51bnNlbGVjdEFsbCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZExheW91dERlZmF1bHRJZHMuZm9yRWFjaCgoKHZhbHVlOiBudW1iZXIpID0+IHsgY29tcG9uZW50LnNlbGVjdEl0ZW0odmFsdWUpOyB9KS5iaW5kKHRoaXMpKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkRlZmF1bHRWaXNpYmlsaXR5Q2hhbmdlKGU6IFRyZWVWaWV3SXRlbVNlbGVjdGlvbkNoYW5nZWQpOiB2b2lkIHtcclxuICAgIGlmIChlLm5vZGUuaXRlbURhdGEpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZExheW91dERlZmF1bHRJZHMgPSBlLmNvbXBvbmVudC5nZXRTZWxlY3RlZE5vZGVLZXlzKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZmlsdGVyTGlzdE9mVmlld3MoYXJnczogVGV4dEJveFZhbHVlQ2hhbmdlKTogdm9pZCB7XHJcbiAgICB0aGlzLmZpbHRlclZhbHVlID0gdGhpcy5maWx0ZXJMYXlvdXROYW1lPy50b0xvd2VyQ2FzZSgpO1xyXG4gICAgaWYgKCF0aGlzLmZpbHRlclZhbHVlPy50cmltKCkgJiYgISgoYXJncy5ldmVudCBhcyBhbnkpLm9yaWdpbmFsRXZlbnQgaW5zdGFuY2VvZiBLZXlib2FyZEV2ZW50KSkge1xyXG4gICAgICB0aGlzLmNsZWFyRmlsdGVyZWRWaWV3cygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb25lU2VsZWN0ZWRMYXlvdXQobGF5b3V0SXRlbTogVmlld1NlbGVjdGlvbk1vZGVsKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5hY3Rpb25PblNlbGVjdGVkVmlldyhsYXlvdXRJdGVtLmlkKSkge1xyXG4gICAgICB0aGlzLnNob3dNb2RhbFBvcFVwID0gdHJ1ZTtcclxuICAgICAgdGhpcy52aWV3VGl0bGUgPSAnQ2xvbmUgVmlldyc7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDcnVkT3BlcmF0aW9uID0gY3J1ZE9wZXJhdGlvbi5jbG9uZTtcclxuICAgICAgbGV0IGNvdW50ZXIgPSAxO1xyXG4gICAgICBjb25zdCBjbG9uZU5hbWUgPSAnQ2xvbmUnO1xyXG4gICAgICB0aGlzLmxheW91dE5hbWUgPSBgJHtsYXlvdXRJdGVtLm5hbWV9ICR7Y2xvbmVOYW1lfWA7XHJcbiAgICAgIHRoaXMuaXNQaW5uZWQgPSBsYXlvdXRJdGVtLmlzUGlubmVkO1xyXG4gICAgICB0aGlzLmxheW91dElkID0gMDtcclxuICAgICAgdGhpcy5zZWxlY3RlZFZpZXdWaXNpYmlsaXR5ID0gbGF5b3V0SXRlbS5pc1B1YmxpYyA/IHRoaXMudmlld1Zpc2liaWxpdHlUeXBlcy5maWx0ZXIoaXRlbSA9PiBpdGVtLnRvTG93ZXJDYXNlKCkgPT09ICdwdWJsaWMnKVswXVxyXG4gICAgICAgIDogdGhpcy52aWV3VmlzaWJpbGl0eVR5cGVzLmZpbHRlcihpdGVtID0+IGl0ZW0udG9Mb3dlckNhc2UoKSA9PT0gJ3ByaXZhdGUnKVswXTtcclxuICAgICAgdGhpcy5zZXRMYXlvdXREZWZhdWx0T3B0aW9ucyhsYXlvdXRJdGVtKTtcclxuICAgICAgd2hpbGUgKCF0aGlzLmlzVmlld05hbWVVbmlxdWUodGhpcy5sYXlvdXROYW1lKSkge1xyXG4gICAgICAgIHRoaXMubGF5b3V0TmFtZSA9IGAke2xheW91dEl0ZW0ubmFtZX0gJHtjbG9uZU5hbWV9ICR7Y291bnRlcn1gO1xyXG4gICAgICAgIGNvdW50ZXIgKz0gMTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGVkaXRTZWxlY3RlZExheW91dChsYXlvdXRJdGVtOiBWaWV3U2VsZWN0aW9uTW9kZWwpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmFjdGlvbk9uU2VsZWN0ZWRWaWV3KGxheW91dEl0ZW0uaWQpKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDcnVkT3BlcmF0aW9uID0gY3J1ZE9wZXJhdGlvbi5lZGl0O1xyXG4gICAgICB0aGlzLnZpZXdUaXRsZSA9ICdFZGl0IFZpZXcnO1xyXG4gICAgICB0aGlzLnNob3dNb2RhbFBvcFVwID0gdHJ1ZTtcclxuICAgICAgdGhpcy5sYXlvdXRJZCA9IGxheW91dEl0ZW0uaWQ7XHJcbiAgICAgIHRoaXMubGF5b3V0TmFtZSA9IGxheW91dEl0ZW0ubmFtZTtcclxuICAgICAgdGhpcy5pc1Bpbm5lZCA9IGxheW91dEl0ZW0uaXNQaW5uZWQ7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRWaWV3VmlzaWJpbGl0eSA9IGxheW91dEl0ZW0uaXNQdWJsaWMgPyB0aGlzLnZpZXdWaXNpYmlsaXR5VHlwZXMuZmlsdGVyKGl0ZW0gPT4gaXRlbS50b0xvd2VyQ2FzZSgpID09PSAncHVibGljJylbMF1cclxuICAgICAgICA6IHRoaXMudmlld1Zpc2liaWxpdHlUeXBlcy5maWx0ZXIoaXRlbSA9PiBpdGVtLnRvTG93ZXJDYXNlKCkgPT09ICdwcml2YXRlJylbMF07XHJcbiAgICAgIHRoaXMuc2V0TGF5b3V0RGVmYXVsdE9wdGlvbnMobGF5b3V0SXRlbSk7XHJcbiAgICAgIHRoaXMuc2V0U2VsZWN0ZWRMYXlvdXQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBjbG9zZUZseU91dCgpOiB2b2lkIHtcclxuICAgIHRoaXMuY2xvc2VDdXJyZW50Rmx5T3V0LmVtaXQoKTtcclxuICB9XHJcblxyXG4gIC8vI2VuZHJlZ2lvbiBQdWJsaWMgTWV0aG9kc1xyXG5cclxuICAvLyNyZWdpb24gUHJpdmF0ZSBNZXRob2RzXHJcbiAgcHJpdmF0ZSBzZXRTZWxlY3RlZExheW91dChhcHBseUxheW91dCA9IGZhbHNlKTogdm9pZCB7XHJcbiAgICB0aGlzLnBvcHVsYXRlRGVmYXVsdHMoKTtcclxuICAgIHRoaXMubGF5b3V0TmFtZSA9IHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8ubmFtZTtcclxuICAgIHRoaXMuc2VsZWN0ZWRWaWV3VmlzaWJpbGl0eSA9IHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8uaXNQdWJsaWMgPyB0aGlzLnZpZXdWaXNpYmlsaXR5VHlwZXMuZmlsdGVyKGl0ZW0gPT4gaXRlbS50b0xvd2VyQ2FzZSgpID09PSAncHVibGljJylbMF1cclxuICAgICAgOiB0aGlzLnZpZXdWaXNpYmlsaXR5VHlwZXMuZmlsdGVyKGl0ZW0gPT4gaXRlbS50b0xvd2VyQ2FzZSgpID09PSAncHJpdmF0ZScpWzBdO1xyXG4gICAgdGhpcy5pc1Bpbm5lZCA9IHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8uaXNQaW5uZWQ7XHJcbiAgICB0aGlzLnNldExheW91dERlZmF1bHRPcHRpb25zKHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8pO1xyXG4gICAgaWYgKGFwcGx5TGF5b3V0KSB7XHJcbiAgICAgIHRoaXMuYXBwbHlTZWxlY3RlZExheW91dCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBhcHBseVNlbGVjdGVkTGF5b3V0KCk6IHZvaWQge1xyXG4gICAgY29uc3QgcXVlcnlQYXJhbXM6IFBhcmFtcyA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucXVlcnlQYXJhbXMpO1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8gJiYgdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5pZCAhPT0gLTEpIHtcclxuICAgICAgaWYgKE51bWJlcihxdWVyeVBhcmFtcy5sYXlvdXQpID09PSB0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvLmlkKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHF1ZXJ5UGFyYW1zLmxheW91dCA9IHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8uaWQ7XHJcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtdLCB7IHF1ZXJ5UGFyYW1zIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5yZXNldExheW91dEluZm8oKTtcclxuICAgICAgcXVlcnlQYXJhbXMubGF5b3V0ID0gMDtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW10sIHsgcXVlcnlQYXJhbXMgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy52aWV3RGF0YVNvdXJjZSkge1xyXG4gICAgICB0aGlzLnZpZXdEYXRhU291cmNlLmFwcGx5Vmlldyh0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvKVxyXG4gICAgICAgIC50aGVuKCh1cGRhdGVkVmlld01vZGVsKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnZpZXdFdmVudC5lbWl0KHsgZXZlbnQ6IENHRkV2ZW50c0VudW0uYXBwbHlWaWV3LCBkYXRhOiB1cGRhdGVkVmlld01vZGVsIH0pO1xyXG4gICAgICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnc3VjY2VzcycsIG1lc3NhZ2U6ICdTZWxlY3RlZCB2aWV3IGhhcyBiZWVuIGFwcGxpZWQgc3VjY2Vzc2Z1bGx5JyB9KTtcclxuICAgICAgICB9KS5jYXRjaCgoKSA9PiB7XHJcbiAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICdlcnJvcicsIG1lc3NhZ2U6ICdVbmFibGUgdG8gYXBwbHkgc2VsZWN0ZWQgdmlldy4nIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy52aWV3RXZlbnQuZW1pdCh7IGV2ZW50OiBDR0ZFdmVudHNFbnVtLmFwcGx5VmlldywgZGF0YTogdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mbyB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgdXBkYXRlR3JpZExheW91dFNldHRpbmdzKCk6IHZvaWQge1xyXG4gICAgY29uc3QgY2hpbGRHcmlkTGF5b3V0ID0gW107XHJcbiAgICB0aGlzLnVuaXF1ZUdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChncmlkSW5zdGFuY2UgPT4ge1xyXG4gICAgICBjb25zdCBncmlkU2V0dGluZ3MgPSB0aGlzLnNldExheW91dFNldHRpbmdzKGdyaWRJbnN0YW5jZS5ncmlkQ29tcG9uZW50SW5zdGFuY2UsIGdyaWRJbnN0YW5jZS5pc01hc3RlckdyaWQpO1xyXG4gICAgICBncmlkU2V0dGluZ3MuZ3JpZE5hbWUgPSBncmlkSW5zdGFuY2UuZ3JpZE5hbWU7XHJcbiAgICAgIGlmIChncmlkSW5zdGFuY2UuaXNNYXN0ZXJHcmlkKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkTGF5b3V0U2V0dGluZ3MgPSBncmlkU2V0dGluZ3M7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY2hpbGRHcmlkTGF5b3V0LnB1c2goZ3JpZFNldHRpbmdzKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmdyaWRMYXlvdXRTZXR0aW5ncy5jaGlsZEdyaWRMYXlvdXQgPSBjaGlsZEdyaWRMYXlvdXQubGVuZ3RoID8gY2hpbGRHcmlkTGF5b3V0XHJcbiAgICAgIDogSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCdjaGlsZEdyaWRMYXlvdXQnKSkgfHwgW107XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNldExheW91dFNldHRpbmdzKGdyaWRJbnN0YW5jZTogYW55LCBpc01hc3RlckdyaWQgPSB0cnVlKTogYW55IHtcclxuICAgIC8vIGNvbnN0IGdyaWRMYXlvdXRTZXR0aW5nID0gZ3JpZEluc3RhbmNlLnN0YXRlKCk7XHJcbiAgICAvLyBncmlkTGF5b3V0U2V0dGluZy5jb2xEZWYgPSBncmlkSW5zdGFuY2UuZ2V0VmlzaWJsZUNvbHVtbnMoKS5maWx0ZXIoKGl0ZW0pID0+IHtcclxuICAgIC8vICAgcmV0dXJuIGl0ZW0uZGF0YUZpZWxkICE9PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCB8fCBpdGVtLmNvbW1hbmQgIT09ICdlbXB0eSc7XHJcbiAgICAvLyB9KTtcclxuICAgIC8vIGdyaWRMYXlvdXRTZXR0aW5nLmZvcm1hdERhdGEgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oZ2V0U3RvcmFnZUtleShncmlkSW5zdGFuY2UsIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmZvcm1hdERhdGFdLCBpc01hc3RlckdyaWQpKSk7XHJcbiAgICAvLyBncmlkTGF5b3V0U2V0dGluZy5zdW1tYXJ5ID0gZ3JpZEluc3RhbmNlLm9wdGlvbignc3VtbWFyeScpO1xyXG4gICAgLy8gZ3JpZExheW91dFNldHRpbmcuc2VsZWN0ZWRUaGVtZSA9IHRoaXMuYXBwbGllZFRoZW1lO1xyXG4gICAgLy8gZ3JpZExheW91dFNldHRpbmcuaXNHcmlkQm9yZGVyVmlzaWJsZSA9IHRoaXMuaXNHcmlkQm9yZGVyVmlzaWJsZTtcclxuICAgIC8vIGdyaWRMYXlvdXRTZXR0aW5nLmNvbmRpdGlvbmFsRm9ybWF0dGluZyA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShnZXRTdG9yYWdlS2V5KGdyaWRJbnN0YW5jZSwgQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuY29uZGl0aW9uYWxGb3JtYXR0aW5nXSwgaXNNYXN0ZXJHcmlkKSkpO1xyXG4gICAgLy8gcmV0dXJuIGdyaWRMYXlvdXRTZXR0aW5nO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwb3B1bGF0ZURlZmF1bHRzKCk6IHZvaWQge1xyXG4gICAgdGhpcy5kZWZhdWx0cyA9IFtdO1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8uaWQgPiAwKSB7XHJcbiAgICAgIGlmICh0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvLmNhbkRlZmF1bHQpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHRzLnB1c2goeyB0ZXh0OiB0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvLmlzRGVmYXVsdCA/ICdDbGVhciBEZWZhdWx0JyA6ICdTZXQgQXMgRGVmYXVsdCcsIHZhbHVlOiAnRGVmYXVsdCcsIGljb246ICdmYXMgZmEtc3RhciBkZWZhdWx0LWljb24tY29sb3InIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvLmNhbkdsb2JhbERlZmF1bHQgJiYgdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5pc1B1YmxpYykge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdHMucHVzaCh7XHJcbiAgICAgICAgICB0ZXh0OiB0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvLmlzR2xvYmFsRGVmYXVsdCA/XHJcbiAgICAgICAgICAgICdDbGVhciBHbG9iYWwgRGVmYXVsdCcgOiAnU2V0IEFzIEdsb2JhbCBEZWZhdWx0JywgdmFsdWU6ICdHbG9iYWxEZWZhdWx0JywgaWNvbjogJ2ZhcyBmYS1zdGFyIGdsb2JhbC1kZWZhdWx0LWljb24tY29sb3InXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgZGVsZXRlTGF5b3V0KHZpZXdUb0RlbGV0ZTogVmlld1NlbGVjdGlvbk1vZGVsKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy52aWV3RGF0YVNvdXJjZSkge1xyXG4gICAgICB0aGlzLnZpZXdEYXRhU291cmNlLmRlbGV0ZVZpZXcodmlld1RvRGVsZXRlKVxyXG4gICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ3N1Y2Nlc3MnLCBtZXNzYWdlOiAnVmlldyBoYXMgYmVlbiBkZWxldGVkIHN1Y2Nlc3NmdWxseS4nIH0pO1xyXG4gICAgICAgICAgdGhpcy52aWV3RXZlbnQuZW1pdCh7IGV2ZW50OiBDR0ZFdmVudHNFbnVtLnVwZGF0ZUxheW91dHMsIGRhdGE6IFsuLi50aGlzLnZpZXdMaXN0LmZpbHRlcihpdGVtID0+IGl0ZW0uaWQgIT09IHZpZXdUb0RlbGV0ZS5pZCldIH0pO1xyXG4gICAgICAgICAgdGhpcy5wcmVwYXJlR3JvdXBMYXlvdXRMaXN0KCk7XHJcbiAgICAgICAgfSkuY2F0Y2goKCkgPT4ge1xyXG4gICAgICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnZXJyb3InLCBtZXNzYWdlOiAnVmlldyBoYXMgYmVlbiB3YXMgbm90IGRlbGV0ZWQgc3VjY2Vzc2Z1bGx5LicgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnZpZXdFdmVudC5lbWl0KHsgZXZlbnQ6IENHRkV2ZW50c0VudW0uZGVsZXRlVmlldywgZGF0YTogdmlld1RvRGVsZXRlIH0pO1xyXG4gICAgICAvLyBUT0RPOiByZWZhY3RvciBiZWxvdyBkdXBsaWNhdGUgY29kZS5cclxuICAgICAgdGhpcy52aWV3TGlzdCA9IFsuLi50aGlzLnZpZXdMaXN0LmZpbHRlcihpdGVtID0+IGl0ZW0uaWQgIT09IHZpZXdUb0RlbGV0ZS5pZCldO1xyXG4gICAgICB0aGlzLnByZXBhcmVHcm91cExheW91dExpc3QoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgcmVzZXRMYXlvdXRJbmZvKCk6IHZvaWQgeyB0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvID0gbmV3IE9iamVjdCgpIGFzIFZpZXdTZWxlY3Rpb25Nb2RlbDsgfVxyXG5cclxuICBwcml2YXRlIHVwZGF0ZVRoZW1lU2VsZWN0aW9uKCk6IHZvaWQgeyB0aGlzLmRlZmF1bHRUaGVtZSA9IHRoaXMuZGVmYXVsdFRoZW1lIHx8ICdkeC1zd2F0Y2gtZGVmYXVsdCc7IH1cclxuXHJcbiAgcHJpdmF0ZSBjbGVhckZpbHRlcmVkVmlld3MoKTogdm9pZCB7XHJcbiAgICB0aGlzLmZpbHRlclZhbHVlID0gbnVsbDtcclxuICAgIHRoaXMuZmlsdGVyTGF5b3V0TmFtZSA9ICcnO1xyXG4gICAgdGhpcy5zaG93RmlsdGVyQ29udHJvbElucHV0ID0gZmFsc2U7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZXBhcmVHcm91cExheW91dExpc3QoKTogdm9pZCB7XHJcbiAgICBjb25zdCBncm91cHMgPSBuZXcgU2V0KHRoaXMubGF5b3V0TGlzdC5tYXAoaXRlbSA9PiBpdGVtLmlzUGlubmVkKSk7XHJcbiAgICB0aGlzLmdyb3VwTGF5b3V0TGlzdCA9IFtdO1xyXG4gICAgZ3JvdXBzLmZvckVhY2goZ3JvdXBWYWx1ZSA9PlxyXG4gICAgICB0aGlzLmdyb3VwTGF5b3V0TGlzdC5wdXNoKHtcclxuICAgICAgICBncm91cE5hbWU6IGdyb3VwVmFsdWUgPyAnUElOTkVEJyA6ICdWSUVXUycsXHJcbiAgICAgICAgaXNWaXNpYmxlOiB0cnVlLFxyXG4gICAgICAgIHZhbHVlczogdGhpcy5sYXlvdXRMaXN0LmZpbHRlcihpID0+IGkuaXNQaW5uZWQgPT09IGdyb3VwVmFsdWUpXHJcbiAgICAgIH0pKTtcclxuICAgIC8vIFNvcnRpbmcgdG8gbWFrZSBzdXJlIHRoYXQgUGlubmVkIGlzIGFsd2F5cyBvbiB0b3AuXHJcbiAgICB0aGlzLmdyb3VwTGF5b3V0TGlzdCA9IHRoaXMuZ3JvdXBMYXlvdXRMaXN0LnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgaWYgKGEuZ3JvdXBOYW1lIDwgYi5ncm91cE5hbWUpIHtcclxuICAgICAgICByZXR1cm4gLTE7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGEuZ3JvdXBOYW1lID4gYi5ncm91cE5hbWUpIHtcclxuICAgICAgICByZXR1cm4gMTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gMDtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzZXRMYXlvdXREZWZhdWx0T3B0aW9ucyhsYXlvdXRJdGVtOiBWaWV3U2VsZWN0aW9uTW9kZWwpOiB2b2lkIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRMYXlvdXREZWZhdWx0SWRzID0gW107XHJcbiAgICBpZiAobGF5b3V0SXRlbS5pc0dsb2JhbERlZmF1bHQpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZExheW91dERlZmF1bHRJZHMucHVzaChcclxuICAgICAgICB0aGlzLmxheW91dERlZmF1bHREYXRhLmZpbHRlcihpdGVtID0+IChpdGVtLnZhbHVlIGFzIHN0cmluZykudG9Mb3dlckNhc2UoKSA9PT0gJ2dsb2JhbCBkZWZhdWx0JylbMF0ua2V5IGFzIG51bWJlcik7XHJcbiAgICB9XHJcbiAgICBpZiAobGF5b3V0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZExheW91dERlZmF1bHRJZHMucHVzaChcclxuICAgICAgICB0aGlzLmxheW91dERlZmF1bHREYXRhLmZpbHRlcihpdGVtID0+IChpdGVtLnZhbHVlIGFzIHN0cmluZykudG9Mb3dlckNhc2UoKSA9PT0gJ3VzZXIgZGVmYXVsdCcpWzBdLmtleSBhcyBudW1iZXIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBpc1ZpZXdOYW1lVW5pcXVlKHZpZXdOYW1lOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgIGNvbnN0IGZpbHRlckl0ZW0gPSB0aGlzLmxheW91dExpc3QuZmlsdGVyKGl0ZW0gPT4gaXRlbS5uYW1lID09PSB2aWV3TmFtZSkgfHwgW107XHJcbiAgICByZXR1cm4gZmlsdGVySXRlbS5sZW5ndGggPyBmYWxzZSA6IHRydWU7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGlzRm9ybVZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHZhbGlkYXRpb25FbmdpbmUudmFsaWRhdGVHcm91cCgndmlld1NhdmUnKS5pc1ZhbGlkO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBhY3Rpb25PblNlbGVjdGVkVmlldyhsYXlvdXRJZDogbnVtYmVyKTogYm9vbGVhbiB7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mbz8uaWQgIT09IGxheW91dElkKSB7XHJcbiAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ3dhcm5pbmcnLCBtZXNzYWdlOiBgUGxlYXNlIGFwcGx5IHRoZSBsYXlvdXQgYmVmb3JlIHBlcmZvcm1pbmcgYW55IGFjdGlvbiBvbiBpdC5gIH0pO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcHJlcGFyZU5ld0xheW91dEluZm9CZWZvcmVFbWl0dGluZygpOiBHcmlkVmlld0luZm8ge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgaWQ6IHRoaXMubGF5b3V0SWQsXHJcbiAgICAgIG5hbWU6IHRoaXMubGF5b3V0TmFtZSxcclxuICAgICAgaXNHbG9iYWxEZWZhdWx0OiB0aGlzLmNoZWNrSWZHbG9iYWxEZWZhdWx0KHRoaXMuc2VsZWN0ZWRMYXlvdXREZWZhdWx0SWRzKSxcclxuICAgICAgaXNVc2VyRGVmYXVsdDogdGhpcy5jaGVja0lmVXNlckRlZmF1bHQodGhpcy5zZWxlY3RlZExheW91dERlZmF1bHRJZHMpLFxyXG4gICAgICBpc1Bpbm5lZDogdGhpcy5pc1Bpbm5lZCxcclxuICAgICAgZGVmYXVsdE9wdGlvbnM6IHRoaXMubGF5b3V0RGVmYXVsdERhdGEuZmlsdGVyKGl0ZW0gPT4gKGl0ZW0gYXMgYW55KS5zZWxlY3RlZCkubGVuZ3RoID4gMCA/IHRoaXMubGF5b3V0RGVmYXVsdERhdGEgOiBbXSxcclxuICAgICAgdmlzaWJpbGl0eTogdGhpcy5zZWxlY3RlZFZpZXdWaXNpYmlsaXR5LnRvTG93ZXJDYXNlKCkgPT09ICdwdWJsaWMnLFxyXG4gICAgICBzdGF0ZTogdGhpcy5nZXRHcmlkU3RhdGVPZlNlbGVjdGVkR3JpZCgpXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZXBhcmVDdXJyZW50TGF5b3V0SW5mb0JlZm9yZUVtaXR0aW5nKCk6IEdyaWRWaWV3SW5mbyB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBpZDogdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5pZCxcclxuICAgICAgbmFtZTogdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5uYW1lLFxyXG4gICAgICBpc1Bpbm5lZDogdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5pc1Bpbm5lZCxcclxuICAgICAgaXNHbG9iYWxEZWZhdWx0OiB0aGlzLmN1cnJlbnRTZWxlY3RMYXlvdXRJbmZvLmlzR2xvYmFsRGVmYXVsdCxcclxuICAgICAgaXNVc2VyRGVmYXVsdDogdGhpcy5jdXJyZW50U2VsZWN0TGF5b3V0SW5mby5pc0RlZmF1bHQsXHJcbiAgICAgIGRlZmF1bHRPcHRpb25zOiB0aGlzLmxheW91dERlZmF1bHREYXRhLmZpbHRlcihpdGVtID0+IChpdGVtIGFzIGFueSkuc2VsZWN0ZWQpLmxlbmd0aCA+IDAgPyB0aGlzLmxheW91dERlZmF1bHREYXRhIDogW10sXHJcbiAgICAgIHZpc2liaWxpdHk6IHRoaXMuY3VycmVudFNlbGVjdExheW91dEluZm8uaXNQdWJsaWMsXHJcbiAgICAgIHN0YXRlOiB0aGlzLmdldEdyaWRTdGF0ZU9mU2VsZWN0ZWRHcmlkKClcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0R3JpZFN0YXRlT2ZTZWxlY3RlZEdyaWQoKTogR3JpZFN0YXRlSW5mbyB7XHJcbiAgICBjb25zdCBjdXJyZW50U2VsZWN0ZWRHcmlkSW5zdGFuY2UgPSB0aGlzLmdyaWRJbnN0YW5jZUxpc3QuZmlsdGVyKGl0ZW0gPT4gaXRlbS5pc1NlbGVjdGVkKVswXTtcclxuICAgIGNvbnN0IGtleTogc3RyaW5nID0gdGhpcy51dGlsaXR5U2VydmljZS5nZXRTdG9yYWdlS2V5KFxyXG4gICAgICBjdXJyZW50U2VsZWN0ZWRHcmlkSW5zdGFuY2U/LmdyaWRDb21wb25lbnRJbnN0YW5jZSwgQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuZm9ybWF0RGF0YV0sIHRydWUpO1xyXG4gICAgY29uc3QgY3VycmVudEdyaWRTdGF0ZSA9IGN1cnJlbnRTZWxlY3RlZEdyaWRJbnN0YW5jZT8uZ3JpZENvbXBvbmVudEluc3RhbmNlLnN0YXRlKCkgYXMgR3JpZFN0YXRlU2F2ZUluZm87XHJcbiAgICBjdXJyZW50R3JpZFN0YXRlLnZpc2libGVDb2x1bW5zID0gY3VycmVudFNlbGVjdGVkR3JpZEluc3RhbmNlPy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZ2V0VmlzaWJsZUNvbHVtbnMoKS5maWx0ZXIoKGl0ZW0pID0+IHtcclxuICAgICAgcmV0dXJuIGl0ZW0uZGF0YUZpZWxkICE9PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCB8fCAoaXRlbSBhcyBhbnkpLmNvbW1hbmQgIT09ICdlbXB0eSc7XHJcbiAgICB9KTtcclxuICAgIGN1cnJlbnRHcmlkU3RhdGUuY29sdW1uRm9ybWF0dGluZ0luZm8gPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oa2V5KSkgfHwgW107XHJcbiAgICBjdXJyZW50R3JpZFN0YXRlLnN1bW1hcnkgPSBjdXJyZW50U2VsZWN0ZWRHcmlkSW5zdGFuY2U/LmdyaWRDb21wb25lbnRJbnN0YW5jZS5vcHRpb24oJ3N1bW1hcnknKTtcclxuICAgIGN1cnJlbnRHcmlkU3RhdGUuc2VsZWN0ZWRUaGVtZSA9IHRoaXMuZGVmYXVsdFRoZW1lO1xyXG4gICAgY3VycmVudEdyaWRTdGF0ZS5pc0dyaWRCb3JkZXJWaXNpYmxlID0gdGhpcy5pc0dyaWRCb3JkZXJWaXNpYmxlO1xyXG4gICAgY3VycmVudEdyaWRTdGF0ZS5jb25kaXRpb25hbEZvcm1hdHRpbmdJbmZvID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGtleSkpIHx8IFtdO1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZ3JpZFN0YXRlOiBjdXJyZW50R3JpZFN0YXRlLFxyXG4gICAgICBjb2x1bW5Gb3JtYXR0aW5nSW5mbzogSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGtleSkpIHx8IFtdLFxyXG4gICAgICBjb25kaXRpb25Gb3JtYXR0aW5nSW5mbzogW11cclxuICAgIH07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGRlZmF1bHRPcHRpb25WYWxpZGF0aW9uKCk6IGJvb2xlYW4ge1xyXG4gICAgY29uc3QgaXNHbG9iYWxEZWZhdWx0ID0gdGhpcy5zZWxlY3RlZExheW91dERlZmF1bHRJZHMuaW5kZXhPZihcclxuICAgICAgdGhpcy5sYXlvdXREZWZhdWx0RGF0YS5maWx0ZXIoKGl0ZW0pID0+IChpdGVtLnZhbHVlIGFzIHN0cmluZykudG9Mb3dlckNhc2UoKSA9PT0gJ2dsb2JhbCBkZWZhdWx0JylbMF0/LmtleSBhcyBudW1iZXIpID4gLTE7XHJcbiAgICBjb25zdCBpc1B1YmxpYyA9IHRoaXMuc2VsZWN0ZWRWaWV3VmlzaWJpbGl0eS50b0xvd2VyQ2FzZSgpID09PSAncHVibGljJztcclxuICAgIHJldHVybiBpc0dsb2JhbERlZmF1bHQgPyBpc1B1YmxpYyA6IHRydWU7XHJcbiAgfVxyXG5cclxuXHJcbiAgcHJpdmF0ZSBjaGVja0lmR2xvYmFsRGVmYXVsdChpZHM6IG51bWJlcltdKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCBnbG9iYWxLZXkgPSB0aGlzLmxheW91dERlZmF1bHREYXRhLmZpbmQoYSA9PiBhLnZhbHVlID09PSAnR2xvYmFsIERlZmF1bHQnKTtcclxuICAgIHJldHVybiBpZHMuc29tZShpZCA9PiBnbG9iYWxLZXkua2V5ID09PSBpZCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGNoZWNrSWZVc2VyRGVmYXVsdChpZHM6IG51bWJlcltdKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCB1c2VyS2V5ID0gdGhpcy5sYXlvdXREZWZhdWx0RGF0YS5maW5kKGEgPT4gYS52YWx1ZSA9PT0gJ1VzZXIgRGVmYXVsdCcpO1xyXG4gICAgcmV0dXJuIGlkcy5zb21lKGlkID0+IHVzZXJLZXkua2V5ID09PSBpZCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgLy8jZW5kcmVnaW9uIFByaXZhdGUgTWV0aG9kc1xyXG5cclxufVxyXG4iXX0=