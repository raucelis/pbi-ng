import { __awaiter, __generator } from "tslib";
import { ViewSelectionModel } from '../../models/view-selection.model';
var DataBrowserViewSource = /** @class */ (function () {
    function DataBrowserViewSource(baseUrl, key, httpClient) {
        var _this = this;
        this.baseUrl = baseUrl;
        this.key = key;
        this.httpClient = httpClient;
        this.getViews = function (key) { return __awaiter(_this, void 0, void 0, function () {
            var views;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.httpClient.get(this.getUrl("/layout/view-list/" + key)).toPromise()];
                    case 1:
                        views = _a.sent();
                        return [2 /*return*/, (views === null || views === void 0 ? void 0 : views.map(function (v) { return new ViewSelectionModel(v); })) || []];
                }
            });
        }); };
        this.updateViewVisibility = function (viewId, isPublic) {
            return _this.httpClient.get(_this.getUrl("/layout/layout-visibility/" + viewId + "/" + isPublic)).toPromise();
        };
        this.saveView = function (gridView) { return __awaiter(_this, void 0, void 0, function () {
            var layout, isValid;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        layout = this.transformToDataBrowserObject(gridView, this.key);
                        return [4 /*yield*/, this.httpClient.post(this.getUrl('/layout/layout-validation'), layout).toPromise()];
                    case 1:
                        isValid = _a.sent();
                        if (isValid.IsValidationFailed) {
                            throw this.error(isValid.ValidationMessage.join(', '));
                        }
                        return [4 /*yield*/, this.httpClient.post(this.getUrl("/layout/layout"), layout).toPromise()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        }); };
        this.addView = function (gridView) { return __awaiter(_this, void 0, void 0, function () {
            var layout, isValid;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        layout = this.transformToDataBrowserObject(gridView, this.key);
                        return [4 /*yield*/, this.httpClient.post(this.getUrl('/layout/layout-validation'), layout).toPromise()];
                    case 1:
                        isValid = _a.sent();
                        if (isValid.IsValidationFailed) {
                            throw this.error(isValid.ValidationMessage.join(', '));
                        }
                        return [4 /*yield*/, this.httpClient.post(this.getUrl("/layout/layout"), layout).toPromise()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        }); };
        this.deleteView = function (view) { return __awaiter(_this, void 0, void 0, function () {
            var canDelete;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.httpClient.get(this.getUrl("/layout/delete-layout-validation/" + view.id)).toPromise()];
                    case 1:
                        canDelete = _a.sent();
                        if (canDelete.IsValidationFailed) {
                            throw this.error(canDelete.ValidationMessage.join(', '));
                        }
                        return [4 /*yield*/, this.httpClient.get(this.getUrl("/layout/delete-layout?id=" + view.id + "&gridName=" + this.key + "&isGlobal=" + view.isGlobalDefault)).toPromise()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        }); };
        this.applyView = function (view) { return __awaiter(_this, void 0, void 0, function () {
            var layout;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.httpClient.get(this.getUrl("/layout/layout/" + view.id)).toPromise()];
                    case 1:
                        layout = _a.sent();
                        return [2 /*return*/, this.transformToCGFObject(layout)];
                }
            });
        }); };
        if (baseUrl[baseUrl.length - 1] === "/") {
            this.baseUrl = baseUrl.slice(0, baseUrl.length - 1);
        }
    }
    DataBrowserViewSource.prototype.getUrl = function (endPoint) {
        return new URL(this.baseUrl + endPoint).toString();
    };
    DataBrowserViewSource.prototype.error = function (message) {
        return { message: message, isError: true, exceptionMessage: '', toasterType: 'error' };
    };
    DataBrowserViewSource.prototype.transformToDataBrowserObject = function (grid, key) {
        return {
            GridName: key,
            Name: grid.name,
            Id: grid.id,
            OverrideId: grid.id,
            IsPinned: grid.isPinned,
            IsPublic: grid.visibility,
            JsonLayout: JSON.stringify(grid.state),
            IsUserDefault: grid.isUserDefault,
            IsGlobalDefault: grid.isGlobalDefault,
            DataLoadOptions: null
        };
    };
    DataBrowserViewSource.prototype.transformToCGFObject = function (view) {
        return {
            name: view.Name,
            id: view.Id,
            defaultOptions: null,
            isGlobalDefault: view.IsGlobalDefault,
            isUserDefault: view.IsUserDefault,
            state: JSON.parse(view.JsonLayout),
            visibility: view.IsPublic,
            isPinned: view.IsPinned,
        };
    };
    return DataBrowserViewSource;
}());
export { DataBrowserViewSource };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRGF0YUJyb3dzZXJWaWV3U291cmNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL3ZpZXctc2VsZWN0aW9uL2RhdGFzb3VyY2UvRGF0YUJyb3dzZXJWaWV3U291cmNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFHQSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUd2RTtJQUNJLCtCQUFtQixPQUFlLEVBQVMsR0FBVyxFQUFTLFVBQXNCO1FBQXJGLGlCQUlDO1FBSmtCLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFBUyxRQUFHLEdBQUgsR0FBRyxDQUFRO1FBQVMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUtyRixhQUFRLEdBQW1ELFVBQU8sR0FBRzs7Ozs0QkFDbkQscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQXdCLElBQUksQ0FBQyxNQUFNLENBQUMsdUJBQXFCLEdBQUssQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUE7O3dCQUE3RyxLQUFLLEdBQUcsU0FBcUc7d0JBQ25ILHNCQUFPLENBQUEsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLElBQUksa0JBQWtCLENBQUMsQ0FBQyxDQUFDLEVBQXpCLENBQXlCLE1BQUssRUFBRSxFQUFDOzs7YUFDM0QsQ0FBQztRQUNGLHlCQUFvQixHQUFvRSxVQUFDLE1BQU0sRUFBRSxRQUFRO1lBQ3JHLE9BQU8sS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQWtCLEtBQUksQ0FBQyxNQUFNLENBQUMsK0JBQTZCLE1BQU0sU0FBSSxRQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzVILENBQUMsQ0FBQztRQUNGLGFBQVEsR0FBK0QsVUFBTyxRQUFROzs7Ozt3QkFDNUUsTUFBTSxHQUFHLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUNyRCxxQkFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBc0IsSUFBSSxDQUFDLE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFBOzt3QkFBdkgsT0FBTyxHQUFHLFNBQTZHO3dCQUM3SCxJQUFHLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRTs0QkFDM0IsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzt5QkFDMUQ7d0JBQ00scUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQXdCLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs0QkFBM0csc0JBQU8sU0FBb0csRUFBQzs7O2FBQy9HLENBQUM7UUFDRixZQUFPLEdBQStELFVBQU8sUUFBUTs7Ozs7d0JBQzNFLE1BQU0sR0FBRyxJQUFJLENBQUMsNEJBQTRCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDckQscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQXNCLElBQUksQ0FBQyxNQUFNLENBQUMsMkJBQTJCLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQXZILE9BQU8sR0FBRyxTQUE2Rzt3QkFDN0gsSUFBRyxPQUFPLENBQUMsa0JBQWtCLEVBQUU7NEJBQzNCLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7eUJBQzFEO3dCQUNNLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUF3QixJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUE7NEJBQTNHLHNCQUFPLFNBQW9HLEVBQUM7OzthQUMvRyxDQUFBO1FBQ0QsZUFBVSxHQUEwRCxVQUFPLElBQUk7Ozs7NEJBQ3pELHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFzQixJQUFJLENBQUMsTUFBTSxDQUFDLHNDQUFvQyxJQUFJLENBQUMsRUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQWxJLFNBQVMsR0FBRyxTQUFzSDt3QkFDeEksSUFBRyxTQUFTLENBQUMsa0JBQWtCLEVBQUU7NEJBQzdCLE1BQU0sSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7eUJBQzVEO3dCQUVNLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFrQixJQUFJLENBQUMsTUFBTSxDQUFDLDhCQUE0QixJQUFJLENBQUMsRUFBRSxrQkFBYSxJQUFJLENBQUMsR0FBRyxrQkFBYSxJQUFJLENBQUMsZUFBaUIsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLEVBQUE7NEJBQXZLLHNCQUFPLFNBQWdLLEVBQUM7OzthQUMzSyxDQUFBO1FBQ0QsY0FBUyxHQUF3RCxVQUFPLElBQUk7Ozs7NEJBQ3pELHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUE0QixJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFrQixJQUFJLENBQUMsRUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQW5ILE1BQU0sR0FBRyxTQUEwRzt3QkFFekgsc0JBQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxFQUFDOzs7YUFDNUMsQ0FBQztRQXZDRSxJQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBQztZQUNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7U0FDdEQ7SUFDTCxDQUFDO0lBc0NPLHNDQUFNLEdBQWQsVUFBZSxRQUFnQjtRQUMzQixPQUFPLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDdkQsQ0FBQztJQUVPLHFDQUFLLEdBQWIsVUFBYyxPQUFlO1FBQ3pCLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsZ0JBQWdCLEVBQUUsRUFBRSxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsQ0FBQTtJQUMxRixDQUFDO0lBRU8sNERBQTRCLEdBQXBDLFVBQXFDLElBQWtCLEVBQUUsR0FBVztRQUNoRSxPQUFPO1lBQ0gsUUFBUSxFQUFFLEdBQUc7WUFDYixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7WUFDZixFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWCxVQUFVLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDbkIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVTtZQUN6QixVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1lBQ3RDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYTtZQUNqQyxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7WUFDckMsZUFBZSxFQUFFLElBQUk7U0FDeEIsQ0FBQTtJQUNMLENBQUM7SUFFTyxvREFBb0IsR0FBNUIsVUFBNkIsSUFBK0I7UUFDeEQsT0FBTztZQUNILElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtZQUNmLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRTtZQUNYLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLGVBQWUsRUFBRSxJQUFJLENBQUMsZUFBZTtZQUNyQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWE7WUFDakMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNsQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDekIsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1NBQzFCLENBQUE7SUFDTCxDQUFDO0lBQ0wsNEJBQUM7QUFBRCxDQUFDLEFBOUVELElBOEVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgVG9hc3RlclJlc3BvbnNlIH0gZnJvbSAnLi4vLi4vY29udHJhY3RzL2NvbW1vbi1ncmlkLWZyYW1ld29yayc7XHJcbmltcG9ydCB7IEdyaWRWaWV3SW5mbywgSVZpZXdEYXRhU291cmNlLCBWaWV3U2VsZWN0aW9uTGF5b3V0IH0gZnJvbSAnLi4vLi4vY29udHJhY3RzL3ZpZXctc2VsZWN0aW9uJztcclxuaW1wb3J0IHsgVmlld1NlbGVjdGlvbk1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL3ZpZXctc2VsZWN0aW9uLm1vZGVsJztcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgRGF0YUJyb3dzZXJWaWV3U291cmNlIGltcGxlbWVudHMgSVZpZXdEYXRhU291cmNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBiYXNlVXJsOiBzdHJpbmcsIHB1YmxpYyBrZXk6IHN0cmluZywgcHVibGljIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQpIHtcclxuICAgICAgICBpZihiYXNlVXJsW2Jhc2VVcmwubGVuZ3RoIC0gMV0gPT09IFwiL1wiKXtcclxuICAgICAgICAgICAgdGhpcy5iYXNlVXJsID0gYmFzZVVybC5zbGljZSgwLCBiYXNlVXJsLmxlbmd0aCAtIDEpXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgZ2V0Vmlld3M6IChrZXk6IHN0cmluZykgPT4gUHJvbWlzZTxWaWV3U2VsZWN0aW9uTW9kZWxbXT4gPSBhc3luYyAoa2V5KSA9PiB7XHJcbiAgICAgICAgY29uc3Qgdmlld3MgPSBhd2FpdCB0aGlzLmh0dHBDbGllbnQuZ2V0PFZpZXdTZWxlY3Rpb25MYXlvdXRbXT4odGhpcy5nZXRVcmwoYC9sYXlvdXQvdmlldy1saXN0LyR7a2V5fWApKS50b1Byb21pc2UoKTtcclxuICAgICAgICByZXR1cm4gdmlld3M/Lm1hcCh2ID0+IG5ldyBWaWV3U2VsZWN0aW9uTW9kZWwodikpIHx8IFtdO1xyXG4gICAgfTtcclxuICAgIHVwZGF0ZVZpZXdWaXNpYmlsaXR5OiAodmlld0lkOiBudW1iZXIsIGlzUHVibGljOiBib29sZWFuKSA9PiBQcm9taXNlPFRvYXN0ZXJSZXNwb25zZT4gPSAodmlld0lkLCBpc1B1YmxpYykgPT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PFRvYXN0ZXJSZXNwb25zZT4odGhpcy5nZXRVcmwoYC9sYXlvdXQvbGF5b3V0LXZpc2liaWxpdHkvJHt2aWV3SWR9LyR7aXNQdWJsaWN9YCkpLnRvUHJvbWlzZSgpO1xyXG4gICAgfTtcclxuICAgIHNhdmVWaWV3OiAoZ3JpZFZpZXc6IEdyaWRWaWV3SW5mbykgPT4gUHJvbWlzZTxWaWV3U2VsZWN0aW9uTGF5b3V0W10+ID0gYXN5bmMgKGdyaWRWaWV3KSA9PiB7XHJcbiAgICAgICAgY29uc3QgbGF5b3V0ID0gdGhpcy50cmFuc2Zvcm1Ub0RhdGFCcm93c2VyT2JqZWN0KGdyaWRWaWV3LCB0aGlzLmtleSk7XHJcbiAgICAgICAgY29uc3QgaXNWYWxpZCA9IGF3YWl0IHRoaXMuaHR0cENsaWVudC5wb3N0PElWYWxpZGF0aW9uRW52ZWxvcGU+KHRoaXMuZ2V0VXJsKCcvbGF5b3V0L2xheW91dC12YWxpZGF0aW9uJyksIGxheW91dCkudG9Qcm9taXNlKCk7XHJcbiAgICAgICAgaWYoaXNWYWxpZC5Jc1ZhbGlkYXRpb25GYWlsZWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgdGhpcy5lcnJvcihpc1ZhbGlkLlZhbGlkYXRpb25NZXNzYWdlLmpvaW4oJywgJykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5odHRwQ2xpZW50LnBvc3Q8Vmlld1NlbGVjdGlvbkxheW91dFtdPih0aGlzLmdldFVybChgL2xheW91dC9sYXlvdXRgKSwgbGF5b3V0KS50b1Byb21pc2UoKTtcclxuICAgIH07XHJcbiAgICBhZGRWaWV3OiAoZ3JpZFZpZXc6IEdyaWRWaWV3SW5mbykgPT4gUHJvbWlzZTxWaWV3U2VsZWN0aW9uTGF5b3V0W10+ID0gYXN5bmMgKGdyaWRWaWV3KSA9PiB7XHJcbiAgICAgICAgY29uc3QgbGF5b3V0ID0gdGhpcy50cmFuc2Zvcm1Ub0RhdGFCcm93c2VyT2JqZWN0KGdyaWRWaWV3LCB0aGlzLmtleSk7XHJcbiAgICAgICAgY29uc3QgaXNWYWxpZCA9IGF3YWl0IHRoaXMuaHR0cENsaWVudC5wb3N0PElWYWxpZGF0aW9uRW52ZWxvcGU+KHRoaXMuZ2V0VXJsKCcvbGF5b3V0L2xheW91dC12YWxpZGF0aW9uJyksIGxheW91dCkudG9Qcm9taXNlKCk7XHJcbiAgICAgICAgaWYoaXNWYWxpZC5Jc1ZhbGlkYXRpb25GYWlsZWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgdGhpcy5lcnJvcihpc1ZhbGlkLlZhbGlkYXRpb25NZXNzYWdlLmpvaW4oJywgJykpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5odHRwQ2xpZW50LnBvc3Q8Vmlld1NlbGVjdGlvbkxheW91dFtdPih0aGlzLmdldFVybChgL2xheW91dC9sYXlvdXRgKSwgbGF5b3V0KS50b1Byb21pc2UoKTtcclxuICAgIH1cclxuICAgIGRlbGV0ZVZpZXc6KHZpZXc6IFZpZXdTZWxlY3Rpb25Nb2RlbCkgPT4gUHJvbWlzZTxUb2FzdGVyUmVzcG9uc2U+ID0gYXN5bmMgKHZpZXcpID0+IHtcclxuICAgICAgICBjb25zdCBjYW5EZWxldGUgPSBhd2FpdCB0aGlzLmh0dHBDbGllbnQuZ2V0PElWYWxpZGF0aW9uRW52ZWxvcGU+KHRoaXMuZ2V0VXJsKGAvbGF5b3V0L2RlbGV0ZS1sYXlvdXQtdmFsaWRhdGlvbi8ke3ZpZXcuaWR9YCkpLnRvUHJvbWlzZSgpO1xyXG4gICAgICAgIGlmKGNhbkRlbGV0ZS5Jc1ZhbGlkYXRpb25GYWlsZWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgdGhpcy5lcnJvcihjYW5EZWxldGUuVmFsaWRhdGlvbk1lc3NhZ2Uuam9pbignLCAnKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5odHRwQ2xpZW50LmdldDxUb2FzdGVyUmVzcG9uc2U+KHRoaXMuZ2V0VXJsKGAvbGF5b3V0L2RlbGV0ZS1sYXlvdXQ/aWQ9JHt2aWV3LmlkfSZncmlkTmFtZT0ke3RoaXMua2V5fSZpc0dsb2JhbD0ke3ZpZXcuaXNHbG9iYWxEZWZhdWx0fWApKS50b1Byb21pc2UoKTtcclxuICAgIH1cclxuICAgIGFwcGx5VmlldzogKHZpZXc6IFZpZXdTZWxlY3Rpb25Nb2RlbCkgPT4gUHJvbWlzZTxHcmlkVmlld0luZm8+ID0gYXN5bmMgKHZpZXcpID0+IHtcclxuICAgICAgICBjb25zdCBsYXlvdXQgPSBhd2FpdCB0aGlzLmh0dHBDbGllbnQuZ2V0PElEYXRhQnJvd3NlckxheW91dERldGFpbHM+KHRoaXMuZ2V0VXJsKGAvbGF5b3V0L2xheW91dC8ke3ZpZXcuaWR9YCkpLnRvUHJvbWlzZSgpO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy50cmFuc2Zvcm1Ub0NHRk9iamVjdChsYXlvdXQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBwcml2YXRlIGdldFVybChlbmRQb2ludDogc3RyaW5nKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBVUkwodGhpcy5iYXNlVXJsICsgZW5kUG9pbnQpLnRvU3RyaW5nKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBlcnJvcihtZXNzYWdlOiBzdHJpbmcpOiBUb2FzdGVyUmVzcG9uc2Uge1xyXG4gICAgICAgIHJldHVybiB7IG1lc3NhZ2U6IG1lc3NhZ2UsIGlzRXJyb3I6IHRydWUsIGV4Y2VwdGlvbk1lc3NhZ2U6ICcnLCB0b2FzdGVyVHlwZTogJ2Vycm9yJyB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB0cmFuc2Zvcm1Ub0RhdGFCcm93c2VyT2JqZWN0KGdyaWQ6IEdyaWRWaWV3SW5mbywga2V5OiBzdHJpbmcpOiBJRGF0YUJyb3dzZXJMYXlvdXREZXRhaWxzIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBHcmlkTmFtZToga2V5LFxyXG4gICAgICAgICAgICBOYW1lOiBncmlkLm5hbWUsXHJcbiAgICAgICAgICAgIElkOiBncmlkLmlkLFxyXG4gICAgICAgICAgICBPdmVycmlkZUlkOiBncmlkLmlkLFxyXG4gICAgICAgICAgICBJc1Bpbm5lZDogZ3JpZC5pc1Bpbm5lZCxcclxuICAgICAgICAgICAgSXNQdWJsaWM6IGdyaWQudmlzaWJpbGl0eSxcclxuICAgICAgICAgICAgSnNvbkxheW91dDogSlNPTi5zdHJpbmdpZnkoZ3JpZC5zdGF0ZSksXHJcbiAgICAgICAgICAgIElzVXNlckRlZmF1bHQ6IGdyaWQuaXNVc2VyRGVmYXVsdCxcclxuICAgICAgICAgICAgSXNHbG9iYWxEZWZhdWx0OiBncmlkLmlzR2xvYmFsRGVmYXVsdCxcclxuICAgICAgICAgICAgRGF0YUxvYWRPcHRpb25zOiBudWxsXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdHJhbnNmb3JtVG9DR0ZPYmplY3QodmlldzogSURhdGFCcm93c2VyTGF5b3V0RGV0YWlscyk6IEdyaWRWaWV3SW5mbyB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbmFtZTogdmlldy5OYW1lLFxyXG4gICAgICAgICAgICBpZDogdmlldy5JZCxcclxuICAgICAgICAgICAgZGVmYXVsdE9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIGlzR2xvYmFsRGVmYXVsdDogdmlldy5Jc0dsb2JhbERlZmF1bHQsXHJcbiAgICAgICAgICAgIGlzVXNlckRlZmF1bHQ6IHZpZXcuSXNVc2VyRGVmYXVsdCxcclxuICAgICAgICAgICAgc3RhdGU6IEpTT04ucGFyc2Uodmlldy5Kc29uTGF5b3V0KSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eTogdmlldy5Jc1B1YmxpYyxcclxuICAgICAgICAgICAgaXNQaW5uZWQ6IHZpZXcuSXNQaW5uZWQsXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbmV4cG9ydCBpbnRlcmZhY2UgSURhdGFCcm93c2VyTGF5b3V0RGV0YWlscyB7XHJcbiAgICBJZDogbnVtYmVyO1xyXG4gICAgTmFtZTogc3RyaW5nO1xyXG4gICAgR3JpZE5hbWU6IHN0cmluZztcclxuICAgIEpzb25MYXlvdXQ6IHN0cmluZztcclxuICAgIElzUHVibGljOiBib29sZWFuO1xyXG4gICAgRGF0YUxvYWRPcHRpb25zOiBzdHJpbmc7XHJcbiAgICBJc1Bpbm5lZDogYm9vbGVhbjtcclxuICAgIE92ZXJyaWRlSWQ6IG51bWJlciB8IG51bGw7XHJcbiAgICBJc1VzZXJEZWZhdWx0OiBib29sZWFuO1xyXG4gICAgSXNHbG9iYWxEZWZhdWx0OiBib29sZWFuO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElWYWxpZGF0aW9uRW52ZWxvcGUge1xyXG4gICAgSXNWYWxpZGF0aW9uRmFpbGVkOiBib29sZWFuO1xyXG4gICAgVmFsaWRhdGlvbk1lc3NhZ2U6IHN0cmluZ1tdO1xyXG4gICAgSXNVc2VyQ29uZmlybWF0aW9uUmVxdWlyZWQ6IGJvb2xlYW47XHJcbiAgICBPdmVycmlkZUlkOiBudW1iZXI7XHJcbn1cclxuIl19