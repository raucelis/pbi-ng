import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Subject, } from 'rxjs';
import { buffer, debounceTime } from 'rxjs/operators';
import { customActionColumnInfo } from '../../utilities/constants';
var ColumnChooserComponent = /** @class */ (function () {
    function ColumnChooserComponent() {
        this.refreshOnColumnUpdate = true;
        //#endregion Input Properties
        //#region Output Events
        this.applyButtonClick = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.selectedColumnData = {
            caption: '',
            columnId: -1,
            dataField: '',
            dataType: null,
            description: '',
            format: '',
            groupName: '',
            visible: true
        };
        this.bulkApplyColumnsLookup = {};
        this.columnFieldTitle = 'COLUMNS';
        this.columnSearchValue = '';
        this.columnsData = [];
        this.columnsSubject = new Subject();
        this.copyOfColumnList = [];
        this.isMasterGridSelected = true;
        this.selectAllChecked = false;
        this.selectedCols = [];
        this.selectedGridInstanceList = [];
        this.selectedGroupNames = [];
        this.showBasicFormat = JSON.parse(sessionStorage.getItem('basicFormatVisibility')) || false;
        this.showColumnList = true;
    }
    Object.defineProperty(ColumnChooserComponent.prototype, "columnsList", {
        get: function () { return this.columnsData; },
        //#region Input Properties
        set: function (columnsList) {
            var _this = this;
            this.columnsData = [];
            this.selectedGroupNames = [];
            this.copyOfColumnList = [];
            columnsList.forEach(function (item) {
                _this.columnsData.push(item);
                if (item.groupName && _this.selectedGroupNames.indexOf(item.groupName) === -1) {
                    _this.selectedGroupNames.push(item.groupName);
                }
                _this.copyOfColumnList.push(item);
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColumnChooserComponent.prototype, "selectedColumnInfo", {
        get: function () { return this.selectedColumnData; },
        set: function (columnItem) {
            this.selectedColumnData = columnItem;
            if (columnItem === null || columnItem === void 0 ? void 0 : columnItem.dataField) {
                this.onClickColumnItem(columnItem.dataField);
            }
            this.bringElementToViewArea((columnItem === null || columnItem === void 0 ? void 0 : columnItem.dataField) || '');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColumnChooserComponent.prototype, "gridInstanceList", {
        get: function () { return this.selectedGridInstanceList; },
        set: function (gridInstanceList) {
            var _a;
            var gridInstances = gridInstanceList.filter(function (item) { return item.isSelected && Object.keys(item.gridComponentInstance).length; });
            this.selectedGridInstanceList = gridInstances.map(function (item) { return item.gridComponentInstance; });
            if (gridInstanceList.length > 1 && ((_a = gridInstances[0]) === null || _a === void 0 ? void 0 : _a.gridName)) {
                this.isMasterGridSelected = gridInstances[0].isMasterGrid;
                this.columnFieldTitle = ("COLUMNS" + (this.isMasterGridSelected ? '' : ' - Child View')).toUpperCase();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ColumnChooserComponent.prototype, "debounce", {
        set: function (num) {
            this.subscribeToColumnChanges(num);
        },
        enumerable: true,
        configurable: true
    });
    ;
    Object.defineProperty(ColumnChooserComponent.prototype, "checkMasterCheckBox", {
        //#endregion Output Events
        get: function () {
            var anyOneColumnHidden = this.columnsData.find(function (colItem) {
                return colItem.visible === false && colItem.dataField !== customActionColumnInfo.dataField;
            });
            return anyOneColumnHidden ? false : true;
        },
        set: function (args) { this.onClickMasterCheckBox(args); },
        enumerable: true,
        configurable: true
    });
    //#region Angular Lifecycle Events
    ColumnChooserComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.selectedGridInstanceList.length) {
            setTimeout(function () {
                _this.selectedGridInstanceList.forEach(function (grid) {
                    grid.columnOption(customActionColumnInfo.dataField, { visible: false });
                    sessionStorage.setItem('currentAppliedView', JSON.stringify(grid.state())); // holding state to make sure group-by works when we toggle selectAll
                });
            }, 0);
        }
    };
    ColumnChooserComponent.prototype.ngOnDestroy = function () {
        var _this = this;
        this.selectedCols.forEach(function (col) {
            _this.columnsData.filter(function (column) { return column.dataField === col.dataField; })[0].visible = !col.visible;
        });
        var anyColumnVisible = this.columnsData.find(function (item) { return item.visible; });
        if (this.selectedGridInstanceList.length) {
            this.selectedGridInstanceList.forEach(function (item) {
                item.columnOption(customActionColumnInfo.dataField, { visible: anyColumnVisible ? true : false });
            });
        }
        sessionStorage.removeItem('currentAppliedView');
        // Lets columns finish up rendering if any outstanding. Will unsubcribe itself when
        // destroyed parameter is set to true.
        this.destroyed = true;
        this.columnsSubject.next(null);
    };
    //#endregion Angular Lifecycle Events
    //#region Public Methods
    ColumnChooserComponent.prototype.toggleColumnVisibility = function (dataField, visible) {
        var _a;
        if (this.isCached) {
            var colIndex = this.selectedCols.findIndex(function (data) { return data.dataField === dataField; });
            if (colIndex !== -1) {
                if (visible) {
                    visible = !((_a = this.selectedGridInstanceList[0].getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.filter(function (col) { return col.hasOwnProperty('dataField') && col.dataField === dataField; }).length);
                }
                if (!visible) {
                    this.selectedCols.splice(colIndex, 1);
                }
            }
            else if (colIndex === -1) {
                this.selectedCols.push({ dataField: dataField, visible: visible });
            }
        }
        else if (this.useBulkApply) {
            this.bulkApplyColumnsLookup[dataField.toLowerCase()] = { dataField: dataField, visible: visible };
        }
        else {
            for (var index = 0; index < this.columnsData.length; index++) {
                if (dataField === this.columnsData[index].dataField) {
                    this.columnsData[index].visible = visible;
                    this.columnsSubject.next({ visible: visible, dataField: dataField });
                    break;
                }
            }
        }
    };
    ColumnChooserComponent.prototype.toggleGroupVisibility = function (event, groupName) {
        if (this.selectedGroupNames.indexOf(groupName) > -1) {
            this.selectedGroupNames = this.selectedGroupNames.filter(function (arr) { return arr !== groupName; });
        }
        else {
            this.selectedGroupNames.push(groupName);
        }
    };
    ColumnChooserComponent.prototype.onSearchColumnValueChange = function (args) {
        var _this = this;
        this.copyOfColumnList = JSON.parse(JSON.stringify(this.columnsData.filter(function (data) {
            return data.caption.toLowerCase().indexOf(args.value.toLowerCase()) > -1;
        })));
        var visibleCol = this.selectedCols.filter(function (data) { return data.visible; });
        if (visibleCol.length) {
            visibleCol.forEach(function (item) {
                _this.copyOfColumnList.filter(function (data) { return data.dataField === item.dataField; }).map(function (col) { return col.visible = true; });
            });
        }
    };
    ColumnChooserComponent.prototype.columnChooserClick = function (event) {
        var target = event.target;
        if (target && target.dataset) {
            switch (target.tagName.toLowerCase()) {
                case 'label':
                    this.onClickColumnItem(target.dataset.item);
                    break;
            }
        }
        event.stopPropagation();
    };
    ColumnChooserComponent.prototype.onClickMasterCheckBox = function (val) {
        var _this = this;
        this.selectAllChecked = val;
        this.copyOfColumnList.forEach(function (item) {
            item.visible = val;
        });
        if (this.isCached) {
            this.columnsData.forEach(function (item) {
                if (item.dataField !== customActionColumnInfo.dataField) {
                    _this.toggleColumnVisibility(item.dataField, val);
                }
            });
        }
        if (this.useBulkApply) {
            this.copyOfColumnList.forEach(function (item) {
                _this.bulkApplyColumnsLookup[item.dataField.toLowerCase()] = { dataField: item.dataField, visible: item.visible };
            });
        }
        else {
            setTimeout(function () {
                _this.copyOfColumnList.forEach(function (item) { item.visible = val; });
                _this.selectedGridInstanceList.forEach(function (item) {
                    item.option('columns', []);
                });
                _this.columnsData.forEach(function (item) {
                    if (item.dataField !== customActionColumnInfo.dataField) {
                        item.visible = val;
                    }
                });
                _this.selectedGridInstanceList.forEach(function (item) {
                    item.option('columns', _this.columnsData);
                });
                if (_this.selectAllChecked) {
                    _this.selectedGridInstanceList.forEach(function (item) {
                        item.state(JSON.parse(sessionStorage.getItem('currentAppliedView')));
                    });
                }
            }, 0);
        }
    };
    ColumnChooserComponent.prototype.fetchCachedColumnsData = function () {
        var _this = this;
        if (this.selectedCols.length) {
            var actionColumn_1 = this.columnsData.filter(function (data) { return data.dataField === customActionColumnInfo.dataField && data.groupName === customActionColumnInfo.groupName; })[0];
            var visibleColumns_1 = this.selectedCols.filter(function (data) { return data.visible; }).map(function (item) { return item.dataField; });
            if (actionColumn_1) {
                this.selectedGridInstanceList.forEach(function (item) {
                    item.columnOption(actionColumn_1.dataField, { visible: visibleColumns_1.length ? true : false, fixed: true, fixedPosition: 'right' });
                });
            }
        }
        else {
            this.selectedGridInstanceList.forEach(function (gridInst) {
                gridInst.beginUpdate();
                _this.columnsList.forEach(function (item) {
                    gridInst.columnOption(item.dataField, 'visible', false);
                });
                gridInst.option('dataSource', []);
                gridInst.endUpdate();
            });
        }
        // TODO: check enable and disable grid export. If not then remove below code.
        if ((this.selectedGridInstanceList[0].getVisibleColumns().filter(function (data) { return data.command !== 'empty'; }).length) > 0) {
            this.selectedGridInstanceList[0].option('export.enabled', false);
        }
        this.applyButtonClick.emit(this.selectedCols);
        this.selectedCols = [];
    };
    ColumnChooserComponent.prototype.formatOptionsChanged = function (args) {
        var _this = this;
        this.columnsData.forEach(function (col) {
            var _a;
            if (col.dataField === ((_a = _this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField)) {
                col.caption = args.caption;
                _this.selectedGridInstanceList.forEach(function (item) {
                    item.columnOption(col.dataField, { caption: col.caption });
                });
            }
        });
        this.copyOfColumnList.forEach(function (item) {
            var _a;
            if (item.dataField === ((_a = _this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField)) {
                item.caption = args.caption;
            }
        });
    };
    ColumnChooserComponent.prototype.onFormatToggled = function (val) {
        this.showBasicFormat = val;
        sessionStorage.setItem('basicFormatVisibility', JSON.stringify(val));
    };
    ColumnChooserComponent.prototype.onBulkApplyClick = function () {
        this.bulkApplyColumns(Object.values(this.bulkApplyColumnsLookup));
        this.bulkApplyColumnsLookup = {};
        this.closeFlyOut();
    };
    ColumnChooserComponent.prototype.closeFlyOut = function () {
        this.closeCurrentFlyOut.emit();
    };
    //#endregion Public Methods
    //#region Private Methods
    ColumnChooserComponent.prototype.onClickColumnItem = function (dataField) {
        this.selectedColumnData = this.columnsData.filter(function (element) { return element.dataField.toLowerCase() === dataField.toLowerCase(); })[0];
    };
    ColumnChooserComponent.prototype.bringElementToViewArea = function (dataField) {
        var dataFieldElem = window.document.querySelectorAll("input[data-item='" + dataField + "']");
        var container = window.document.getElementsByClassName('columnChooser-group-columns');
        if (container.length && dataFieldElem.length) {
            var topPos = dataFieldElem[0].offsetTop;
            container[0].scrollTop = topPos - (container[0].offsetTop + 10); // 10 is offset to enhance UX.
        }
    };
    ColumnChooserComponent.prototype.subscribeToColumnChanges = function (debounce) {
        var _this = this;
        if (this.columnSubjectSubscription) {
            this.columnSubjectSubscription.unsubscribe();
        }
        this.columnSubjectSubscription = this.columnsSubject.pipe(buffer(this.columnsSubject.pipe(debounceTime(debounce))))
            .subscribe(function (fields) {
            fields = fields.filter(function (f) { return f; });
            _this.bulkApplyColumns(fields);
            if (_this.destroyed) {
                _this.columnSubjectSubscription.unsubscribe();
            }
        });
    };
    ColumnChooserComponent.prototype.bulkApplyColumns = function (fields) {
        var _this = this;
        if (fields.length > 0) {
            this.selectedGridInstanceList.forEach(function (item) { item.beginCustomLoading('Loading'); item.beginUpdate(); });
            fields.forEach(function (f) {
                _this.selectedGridInstanceList.forEach(function (item) { return item.columnOption(f.dataField, { visible: f.visible }); });
            });
            if (this.refreshOnColumnUpdate) {
                this.selectedGridInstanceList.forEach(function (item) { return item.refresh(true); });
            }
            else {
                this.selectedGridInstanceList.forEach(function (item) { return item.repaint(); });
            }
            this.selectedGridInstanceList.forEach(function (item) { item.endCustomLoading(); item.endUpdate(); });
        }
    };
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "columnsList", null);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "isCached", void 0);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "selectedColumnInfo", null);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "gridInstanceList", null);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "debounce", null);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "useBulkApply", void 0);
    __decorate([
        Input()
    ], ColumnChooserComponent.prototype, "refreshOnColumnUpdate", void 0);
    __decorate([
        Output()
    ], ColumnChooserComponent.prototype, "applyButtonClick", void 0);
    __decorate([
        Output()
    ], ColumnChooserComponent.prototype, "closeCurrentFlyOut", void 0);
    ColumnChooserComponent = __decorate([
        Component({
            selector: 'pbi-column-chooser',
            template: "<div class=\"column-chooser-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-cog title-icon\"></i>\r\n            <span class=\"section-title\">FIELDS</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"property-tab-in-column-chooser\" (click)=\"showColumnList = !showColumnList\">\r\n                <span>{{columnFieldTitle}}</span>\r\n                <i [class.fa-angle-down]=\"!showColumnList\" [class.fa-angle-up]=\"showColumnList\"\r\n                    class=\"fa basic-format-toggle-icon\"></i>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n\r\n    <div class=\"accordion-content columns-search-container\" *ngIf=\"showColumnList\">\r\n        <div class=\"form-group columns-search\">\r\n            <div class=\"text-container\">\r\n                <dx-text-box mode=\"search\" [(value)]=\"columnSearchValue\" valueChangeEvent=\"keyup\"\r\n                    (onValueChanged)=\"onSearchColumnValueChange($event)\" placeholder=\"Enter search text\"\r\n                    [showClearButton]=\"true\">\r\n                </dx-text-box>\r\n            </div>\r\n        </div>\r\n        <div class=\"column-list-container\">\r\n            <div class=\"columnChooser-group-columns\"\r\n                [class.columnChooser-group-columns-without-format]=\"!showBasicFormat\"\r\n                [class.columnChooser-group-columns-with-format]=\"showBasicFormat\">\r\n                <span class=\"column-visible-label\">Visible</span>\r\n                <span class=\"column-caption-label\">Caption</span>\r\n                <div class=\"column-header\">\r\n                    <input type=\"checkbox\" [(ngModel)]=\"checkMasterCheckBox\" />\r\n                    <label class=\"column-list-label\">Select All</label>\r\n                </div>\r\n                <div *ngFor=\"let groupItem of copyOfColumnList;let i = index;\">\r\n                    <div class=\"groupContainer\"\r\n                        *ngIf=\" ((i===0) || (groupItem.groupName != copyOfColumnList[i-1].groupName)) && groupItem.groupName !== '_row_actions_group_' \"\r\n                        (click)=\"toggleGroupVisibility($event, groupItem.groupName)\">\r\n                        <i class=\"dx-icon-spindown\"\r\n                            [class.dx-icon-spindown]=\"selectedGroupNames.indexOf(groupItem.groupName)  > -1 \"\r\n                            [class.dx-icon-spinright]=\"selectedGroupNames.indexOf(groupItem.groupName) === -1\"></i>\r\n                        <span>{{groupItem.groupName || 'Columns'}}</span>\r\n                    </div>\r\n                    <span *ngIf=\"selectedGroupNames.indexOf(groupItem.groupName) > -1\">\r\n                        <div class=\"cgf-columns-list\"\r\n                            [class.selectedColumn]=\"selectedColumnInfo && groupItem.dataField === selectedColumnInfo.dataField\">\r\n                            <input type=\"checkbox\" attr.data-item=\"{{groupItem.dataField}}\"\r\n                                [(ngModel)]=\"groupItem.visible\"\r\n                                (change)=\"toggleColumnVisibility(groupItem.dataField, groupItem.visible)\" />\r\n                            <label class=\"column-list-label\" (click)=\"columnChooserClick($event)\"\r\n                                attr.data-item=\"{{groupItem.dataField}}\">{{groupItem.caption}}</label>\r\n                        </div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"accordion-content\">\r\n        <pbi-color-format [gridInstanceList]=\"selectedGridInstanceList\" [isMasterGridSelected]=\"isMasterGridSelected\"\r\n            [selectedColumnInfo]=\"selectedColumnData\" [enableInputDataFieldCheck]=\"true\"\r\n            [showBasicFormat]=\"showBasicFormat\" (formatOptionsChanged)=\"formatOptionsChanged($event)\"\r\n            (formatToggled)=\"onFormatToggled($event)\">\r\n        </pbi-color-format>\r\n    </div>\r\n    <span class=\"button-container\" *ngIf=\"isCached\">\r\n        <dx-button text=\"Apply\" class=\"cacheButton\" type=\"default\" (onClick)=\"fetchCachedColumnsData()\">\r\n        </dx-button>\r\n    </span>\r\n    <span class=\"button-container\" *ngIf=\"useBulkApply\">\r\n        <dx-button text=\"Apply\" class=\"cacheButton\" type=\"default\" (onClick)=\"onBulkApplyClick()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
            styles: [""]
        })
    ], ColumnChooserComponent);
    return ColumnChooserComponent;
}());
export { ColumnChooserComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL2NvbHVtbi1jaG9vc2VyL2NvbHVtbi1jaG9vc2VyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUkxRixPQUFPLEVBQUUsT0FBTyxHQUFpQixNQUFNLE1BQU0sQ0FBQztBQUM5QyxPQUFPLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBUW5FO0lBcUZFO1FBM0NTLDBCQUFxQixHQUFHLElBQUksQ0FBQztRQUN0Qyw2QkFBNkI7UUFFN0IsdUJBQXVCO1FBQ04scUJBQWdCLEdBQXVDLElBQUksWUFBWSxFQUFFLENBQUM7UUFDMUUsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQVd6RCx1QkFBa0IsR0FBa0I7WUFDbEMsT0FBTyxFQUFFLEVBQUU7WUFDWCxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ1osU0FBUyxFQUFFLEVBQUU7WUFDYixRQUFRLEVBQUUsSUFBSTtZQUNkLFdBQVcsRUFBRSxFQUFFO1lBQ2YsTUFBTSxFQUFFLEVBQUU7WUFDVixTQUFTLEVBQUUsRUFBRTtZQUNiLE9BQU8sRUFBRSxJQUFJO1NBQ2QsQ0FBQztRQUVGLDJCQUFzQixHQUErQixFQUFFLENBQUM7UUFDeEQscUJBQWdCLEdBQUcsU0FBUyxDQUFDO1FBQzdCLHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUV2QixnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQixtQkFBYyxHQUFHLElBQUksT0FBTyxFQUF3QixDQUFDO1FBQ3JELHFCQUFnQixHQUFvQixFQUFFLENBQUM7UUFFdkMseUJBQW9CLEdBQUcsSUFBSSxDQUFDO1FBQzVCLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUN6QixpQkFBWSxHQUF5QixFQUFFLENBQUM7UUFDeEMsNkJBQXdCLEdBQStCLEVBQUUsQ0FBQztRQUMxRCx1QkFBa0IsR0FBYSxFQUFFLENBQUM7UUFDbEMsb0JBQWUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQztRQUN2RixtQkFBYyxHQUFHLElBQUksQ0FBQztJQUVOLENBQUM7SUFqRmpCLHNCQUFJLCtDQUFXO2FBWWYsY0FBb0IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQWQ5QywwQkFBMEI7YUFFMUIsVUFBZ0IsV0FBNEI7WUFENUMsaUJBWUM7WUFWQyxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsRUFBRSxDQUFDO1lBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7WUFDM0IsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ3RCLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksS0FBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7b0JBQzVFLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUM5QztnQkFDRCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25DLENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQzs7O09BQUE7SUFJUSxzQkFBSSxzREFBa0I7YUFPL0IsY0FBMkIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO2FBUG5ELFVBQXVCLFVBQXlCO1lBQ3ZELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLENBQUM7WUFDckMsSUFBSSxVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsU0FBUyxFQUFFO2dCQUN6QixJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzlDO1lBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUEsVUFBVSxhQUFWLFVBQVUsdUJBQVYsVUFBVSxDQUFFLFNBQVMsS0FBSSxFQUFFLENBQUMsQ0FBQztRQUMzRCxDQUFDOzs7T0FBQTtJQUdRLHNCQUFJLG9EQUFnQjthQVE3QixjQUF5QixPQUFPLElBQUksQ0FBQyx3QkFBK0IsQ0FBQyxDQUFDLENBQUM7YUFSOUQsVUFBcUIsZ0JBQTBDOztZQUN0RSxJQUFNLGFBQWEsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxFQUFqRSxDQUFpRSxDQUFDLENBQUM7WUFDekgsSUFBSSxDQUFDLHdCQUF3QixHQUFHLGFBQWEsQ0FBQyxHQUFHLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMscUJBQXFCLEVBQTFCLENBQTBCLENBQUMsQ0FBQztZQUN0RixJQUFJLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLFdBQUksYUFBYSxDQUFDLENBQUMsQ0FBQywwQ0FBRSxRQUFRLENBQUEsRUFBRTtnQkFDN0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7Z0JBQzFELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFBLGFBQVUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBRSxDQUFBLENBQUMsV0FBVyxFQUFFLENBQUM7YUFDcEc7UUFDSCxDQUFDOzs7T0FBQTtJQUdRLHNCQUFJLDRDQUFRO2FBQVosVUFBYSxHQUFXO1lBQy9CLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxDQUFDOzs7T0FBQTtJQUFBLENBQUM7SUFVRixzQkFBSSx1REFBbUI7UUFGdkIsMEJBQTBCO2FBRTFCO1lBQ0UsSUFBTSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU87Z0JBQ3RELE9BQU8sT0FBTyxDQUFDLE9BQU8sS0FBSyxLQUFLLElBQUksT0FBTyxDQUFDLFNBQVMsS0FBSyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7WUFDN0YsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMzQyxDQUFDO2FBQ0QsVUFBd0IsSUFBSSxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQURsRTtJQWdDRCxrQ0FBa0M7SUFDbEMseUNBQVEsR0FBUjtRQUFBLGlCQVVDO1FBVEMsSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFO1lBQ3hDLFVBQVUsQ0FBQztnQkFDVCxLQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztvQkFDeEUsY0FBYyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMscUVBQXFFO2dCQUN4RyxDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNQO0lBQ0gsQ0FBQztJQUVELDRDQUFXLEdBQVg7UUFBQSxpQkFlQztRQWRDLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRztZQUMzQixLQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxTQUFTLEtBQUssR0FBRyxDQUFDLFNBQVMsRUFBbEMsQ0FBa0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDbEcsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLE9BQU8sRUFBWixDQUFZLENBQUMsQ0FBQztRQUNyRSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUU7WUFDeEMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7Z0JBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7WUFDcEcsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELGNBQWMsQ0FBQyxVQUFVLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUNoRCxtRkFBbUY7UUFDbkYsc0NBQXNDO1FBQ3RDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFDRCxxQ0FBcUM7SUFFckMsd0JBQXdCO0lBQ2pCLHVEQUFzQixHQUE3QixVQUE4QixTQUFpQixFQUFFLE9BQWdCOztRQUMvRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBNUIsQ0FBNEIsQ0FBQyxDQUFDO1lBQ25GLElBQUksUUFBUSxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUNuQixJQUFJLE9BQU8sRUFBRTtvQkFDWCxPQUFPLEdBQUcsUUFBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLEVBQUUsMENBQzdELE1BQU0sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQTlELENBQThELEVBQUUsTUFBTSxDQUFBLENBQUM7aUJBQ3hGO2dCQUNELElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ1osSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUN2QzthQUNGO2lCQUFNLElBQUksUUFBUSxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLFNBQVMsV0FBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQzthQUNoRDtTQUNGO2FBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQzVCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLFNBQVMsV0FBQSxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUM7U0FDL0U7YUFBTTtZQUNMLEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDNUQsSUFBSSxTQUFTLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEVBQUU7b0JBQ25ELElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztvQkFDMUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxTQUFTLFdBQUEsRUFBRSxDQUFDLENBQUM7b0JBQ2pELE1BQU07aUJBQ1A7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVNLHNEQUFxQixHQUE1QixVQUE2QixLQUFpQixFQUFFLFNBQWlCO1FBQy9ELElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNuRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsS0FBSyxTQUFTLEVBQWpCLENBQWlCLENBQUMsQ0FBQztTQUNwRjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN6QztJQUNILENBQUM7SUFFTSwwREFBeUIsR0FBaEMsVUFBaUMsSUFBd0I7UUFBekQsaUJBU0M7UUFSQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSTtZQUM1RSxPQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7UUFBakUsQ0FBaUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxPQUFPLEVBQVosQ0FBWSxDQUFDLENBQUM7UUFDbEUsSUFBSSxVQUFVLENBQUMsTUFBTSxFQUFFO1lBQ3JCLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO2dCQUNyQixLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsU0FBUyxFQUFqQyxDQUFpQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQUMsR0FBa0IsSUFBSyxPQUFBLEdBQUcsQ0FBQyxPQUFPLEdBQUcsSUFBSSxFQUFsQixDQUFrQixDQUFDLENBQUM7WUFDMUgsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTSxtREFBa0IsR0FBekIsVUFBMEIsS0FBaUI7UUFDekMsSUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQTBCLENBQUM7UUFDaEQsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUM1QixRQUFRLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUU7Z0JBQ3BDLEtBQUssT0FBTztvQkFDVixJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDNUMsTUFBTTthQUNUO1NBQ0Y7UUFDRCxLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVNLHNEQUFxQixHQUE1QixVQUE2QixHQUFZO1FBQXpDLGlCQXlDQztRQXhDQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQkFDM0IsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLHNCQUFzQixDQUFDLFNBQVMsRUFBRTtvQkFDdkQsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7aUJBQ2xEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUFDLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUN2QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtnQkFDaEMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkgsQ0FBQyxDQUFDLENBQUM7U0FFSjthQUNJO1lBQ0gsVUFBVSxDQUFDO2dCQUNULEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQU0sSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0QsS0FBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2dCQUM3QixDQUFDLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7b0JBQzNCLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUU7d0JBQ3ZELElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO3FCQUNwQjtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtvQkFDeEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMzQyxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDekIsS0FBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxVQUFBLElBQUk7d0JBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN2RSxDQUFDLENBQUMsQ0FBQztpQkFDSjtZQUNILENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNQO0lBQ0gsQ0FBQztJQUVNLHVEQUFzQixHQUE3QjtRQUFBLGlCQTBCQztRQXpCQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQzVCLElBQU0sY0FBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFNBQVMsS0FBSyxzQkFBc0IsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxzQkFBc0IsQ0FBQyxTQUFTLEVBQTFHLENBQTBHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwSyxJQUFNLGdCQUFjLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsT0FBTyxFQUFaLENBQVksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxTQUFTLEVBQWQsQ0FBYyxDQUFDLENBQUM7WUFDbEcsSUFBSSxjQUFZLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLGNBQVksQ0FBQyxTQUFTLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7Z0JBQ3BJLENBQUMsQ0FBQyxDQUFDO2FBQ0o7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxVQUFBLFFBQVE7Z0JBQzVDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDdkIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUMzQixRQUFRLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUMxRCxDQUFDLENBQUMsQ0FBQztnQkFDSCxRQUFRLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDbEMsUUFBUSxDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxDQUFDO1NBRUo7UUFDRCw2RUFBNkU7UUFDN0UsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFDLElBQVksQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFqQyxDQUFpQyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZILElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDbEU7UUFDRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRU0scURBQW9CLEdBQTNCLFVBQTRCLElBQUk7UUFBaEMsaUJBY0M7UUFiQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUc7O1lBQzFCLElBQUksR0FBRyxDQUFDLFNBQVMsWUFBSyxLQUFJLENBQUMsa0JBQWtCLDBDQUFFLFNBQVMsQ0FBQSxFQUFFO2dCQUN4RCxHQUFHLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQzNCLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJO29CQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7Z0JBQzdELENBQUMsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJOztZQUNoQyxJQUFJLElBQUksQ0FBQyxTQUFTLFlBQUssS0FBSSxDQUFDLGtCQUFrQiwwQ0FBRSxTQUFTLENBQUEsRUFBRTtnQkFDekQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQzdCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRU0sZ0RBQWUsR0FBdEIsVUFBdUIsR0FBWTtRQUNqQyxJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQztRQUMzQixjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRU0saURBQWdCLEdBQXZCO1FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRU0sNENBQVcsR0FBbEI7UUFDRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUNELDJCQUEyQjtJQUUzQix5QkFBeUI7SUFFakIsa0RBQWlCLEdBQXpCLFVBQTBCLFNBQWlCO1FBQ3pDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxDQUFDLFdBQVcsRUFBRSxFQUEzRCxDQUEyRCxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDL0gsQ0FBQztJQUVPLHVEQUFzQixHQUE5QixVQUErQixTQUFpQjtRQUM5QyxJQUFNLGFBQWEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLHNCQUFvQixTQUFTLE9BQUksQ0FBQyxDQUFDO1FBQzFGLElBQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsc0JBQXNCLENBQUMsNkJBQTZCLENBQUMsQ0FBQztRQUN4RixJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUM1QyxJQUFNLE1BQU0sR0FBSSxhQUFhLENBQUMsQ0FBQyxDQUFpQixDQUFDLFNBQVMsQ0FBQztZQUMzRCxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxHQUFHLE1BQU0sR0FBRyxDQUFFLFNBQVMsQ0FBQyxDQUFDLENBQWlCLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsOEJBQThCO1NBQ2pIO0lBQ0gsQ0FBQztJQUVPLHlEQUF3QixHQUFoQyxVQUFpQyxRQUFnQjtRQUFqRCxpQkFhQztRQVpDLElBQUksSUFBSSxDQUFDLHlCQUF5QixFQUFFO1lBQ2xDLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUM5QztRQUNELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNoSCxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQ2YsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLEVBQUQsQ0FBQyxDQUFDLENBQUM7WUFDL0IsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRTlCLElBQUksS0FBSSxDQUFDLFNBQVMsRUFBRTtnQkFDbEIsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQzlDO1FBQ0gsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8saURBQWdCLEdBQXhCLFVBQXlCLE1BQThCO1FBQXZELGlCQWFDO1FBWkMsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNyQixJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFNLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNHLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDO2dCQUNkLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQXRELENBQXNELENBQUMsQ0FBQztZQUN4RyxDQUFDLENBQUMsQ0FBQTtZQUNGLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO2dCQUM5QixJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO2FBQ25FO2lCQUFNO2dCQUNMLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQWQsQ0FBYyxDQUFDLENBQUM7YUFDL0Q7WUFDRCxJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFNLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDL0Y7SUFDSCxDQUFDO0lBL1REO1FBREMsS0FBSyxFQUFFOzZEQVlQO0lBR1E7UUFBUixLQUFLLEVBQUU7NERBQTBCO0lBQ3pCO1FBQVIsS0FBSyxFQUFFO29FQU1QO0lBR1E7UUFBUixLQUFLLEVBQUU7a0VBT1A7SUFHUTtRQUFSLEtBQUssRUFBRTswREFFUDtJQUNRO1FBQVIsS0FBSyxFQUFFO2dFQUFvQjtJQUNuQjtRQUFSLEtBQUssRUFBRTt5RUFBOEI7SUFJNUI7UUFBVCxNQUFNLEVBQUU7b0VBQWtGO0lBQ2pGO1FBQVQsTUFBTSxFQUFFO3NFQUFnRDtJQS9DOUMsc0JBQXNCO1FBTGxDLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxvQkFBb0I7WUFDOUIsdWhKQUE4Qzs7U0FFL0MsQ0FBQztPQUNXLHNCQUFzQixDQXVVbEM7SUFBRCw2QkFBQztDQUFBLEFBdlVELElBdVVDO1NBdlVZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbHVtblZpc2liaWxpdHlCdWxrTG9va3VwLCBDb2x1bW5WaXNpYmlsaXR5VHlwZSwgU2VsZWN0ZWRDb2x1bW5JbmZvIH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbHVtbic7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEeFRleHRCb3hDb21wb25lbnQgfSBmcm9tICdkZXZleHRyZW1lLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBHcmlkQ29tcG9uZW50SW5zdGFuY2VzIH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbW1vbi1ncmlkLWZyYW1ld29yayc7XHJcbmltcG9ydCB7IFBCSUdyaWRDb2x1bW4gfSBmcm9tICcuLi9jb250cmFjdHMvZ3JpZCc7XHJcbmltcG9ydCB7IFN1YmplY3QsIFN1YnNjcmlwdGlvbiwgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgYnVmZmVyLCBkZWJvdW5jZVRpbWUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IGN1c3RvbUFjdGlvbkNvbHVtbkluZm8gfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvY29uc3RhbnRzJztcclxuaW1wb3J0IERldkV4cHJlc3MgZnJvbSAnZGV2ZXh0cmVtZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS1jb2x1bW4tY2hvb3NlcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbHVtbi1jaG9vc2VyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb2x1bW4tY2hvb3Nlci5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbHVtbkNob29zZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG4gIC8vI3JlZ2lvbiBJbnB1dCBQcm9wZXJ0aWVzXHJcbiAgQElucHV0KClcclxuICBzZXQgY29sdW1uc0xpc3QoY29sdW1uc0xpc3Q6IFBCSUdyaWRDb2x1bW5bXSkge1xyXG4gICAgdGhpcy5jb2x1bW5zRGF0YSA9IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMgPSBbXTtcclxuICAgIHRoaXMuY29weU9mQ29sdW1uTGlzdCA9IFtdO1xyXG4gICAgY29sdW1uc0xpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgdGhpcy5jb2x1bW5zRGF0YS5wdXNoKGl0ZW0pO1xyXG4gICAgICBpZiAoaXRlbS5ncm91cE5hbWUgJiYgdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMuaW5kZXhPZihpdGVtLmdyb3VwTmFtZSkgPT09IC0xKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMucHVzaChpdGVtLmdyb3VwTmFtZSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jb3B5T2ZDb2x1bW5MaXN0LnB1c2goaXRlbSk7XHJcbiAgICB9KTtcclxuICB9XHJcbiAgZ2V0IGNvbHVtbnNMaXN0KCkgeyByZXR1cm4gdGhpcy5jb2x1bW5zRGF0YTsgfVxyXG5cclxuICBASW5wdXQoKSBwdWJsaWMgaXNDYWNoZWQ6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgc2V0IHNlbGVjdGVkQ29sdW1uSW5mbyhjb2x1bW5JdGVtOiBQQklHcmlkQ29sdW1uKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YSA9IGNvbHVtbkl0ZW07XHJcbiAgICBpZiAoY29sdW1uSXRlbT8uZGF0YUZpZWxkKSB7XHJcbiAgICAgIHRoaXMub25DbGlja0NvbHVtbkl0ZW0oY29sdW1uSXRlbS5kYXRhRmllbGQpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5icmluZ0VsZW1lbnRUb1ZpZXdBcmVhKGNvbHVtbkl0ZW0/LmRhdGFGaWVsZCB8fCAnJyk7XHJcbiAgfVxyXG4gIGdldCBzZWxlY3RlZENvbHVtbkluZm8oKSB7IHJldHVybiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YTsgfVxyXG5cclxuICBASW5wdXQoKSBzZXQgZ3JpZEluc3RhbmNlTGlzdChncmlkSW5zdGFuY2VMaXN0OiBHcmlkQ29tcG9uZW50SW5zdGFuY2VzW10pIHtcclxuICAgIGNvbnN0IGdyaWRJbnN0YW5jZXMgPSBncmlkSW5zdGFuY2VMaXN0LmZpbHRlcihpdGVtID0+IGl0ZW0uaXNTZWxlY3RlZCAmJiBPYmplY3Qua2V5cyhpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSkubGVuZ3RoKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0ID0gZ3JpZEluc3RhbmNlcy5tYXAoaXRlbSA9PiBpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSk7XHJcbiAgICBpZiAoZ3JpZEluc3RhbmNlTGlzdC5sZW5ndGggPiAxICYmIGdyaWRJbnN0YW5jZXNbMF0/LmdyaWROYW1lKSB7XHJcbiAgICAgIHRoaXMuaXNNYXN0ZXJHcmlkU2VsZWN0ZWQgPSBncmlkSW5zdGFuY2VzWzBdLmlzTWFzdGVyR3JpZDtcclxuICAgICAgdGhpcy5jb2x1bW5GaWVsZFRpdGxlID0gYENPTFVNTlMke3RoaXMuaXNNYXN0ZXJHcmlkU2VsZWN0ZWQgPyAnJyA6ICcgLSBDaGlsZCBWaWV3J31gLnRvVXBwZXJDYXNlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldCBncmlkSW5zdGFuY2VMaXN0KCkgeyByZXR1cm4gdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QgYXMgYW55OyB9XHJcblxyXG4gIEBJbnB1dCgpIHNldCBkZWJvdW5jZShudW06IG51bWJlcikge1xyXG4gICAgdGhpcy5zdWJzY3JpYmVUb0NvbHVtbkNoYW5nZXMobnVtKTtcclxuICB9O1xyXG4gIEBJbnB1dCgpIHVzZUJ1bGtBcHBseTogdHJ1ZTtcclxuICBASW5wdXQoKSByZWZyZXNoT25Db2x1bW5VcGRhdGUgPSB0cnVlO1xyXG4gIC8vI2VuZHJlZ2lvbiBJbnB1dCBQcm9wZXJ0aWVzXHJcblxyXG4gIC8vI3JlZ2lvbiBPdXRwdXQgRXZlbnRzXHJcbiAgQE91dHB1dCgpIHB1YmxpYyBhcHBseUJ1dHRvbkNsaWNrOiBFdmVudEVtaXR0ZXI8U2VsZWN0ZWRDb2x1bW5JbmZvW10+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgY2xvc2VDdXJyZW50Rmx5T3V0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIC8vI2VuZHJlZ2lvbiBPdXRwdXQgRXZlbnRzXHJcblxyXG4gIGdldCBjaGVja01hc3RlckNoZWNrQm94KCkge1xyXG4gICAgY29uc3QgYW55T25lQ29sdW1uSGlkZGVuID0gdGhpcy5jb2x1bW5zRGF0YS5maW5kKGNvbEl0ZW0gPT4ge1xyXG4gICAgICByZXR1cm4gY29sSXRlbS52aXNpYmxlID09PSBmYWxzZSAmJiBjb2xJdGVtLmRhdGFGaWVsZCAhPT0gY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5kYXRhRmllbGQ7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBhbnlPbmVDb2x1bW5IaWRkZW4gPyBmYWxzZSA6IHRydWU7XHJcbiAgfVxyXG4gIHNldCBjaGVja01hc3RlckNoZWNrQm94KGFyZ3MpIHsgdGhpcy5vbkNsaWNrTWFzdGVyQ2hlY2tCb3goYXJncyk7IH1cclxuXHJcbiAgc2VsZWN0ZWRDb2x1bW5EYXRhOiBQQklHcmlkQ29sdW1uID0ge1xyXG4gICAgY2FwdGlvbjogJycsXHJcbiAgICBjb2x1bW5JZDogLTEsXHJcbiAgICBkYXRhRmllbGQ6ICcnLFxyXG4gICAgZGF0YVR5cGU6IG51bGwsXHJcbiAgICBkZXNjcmlwdGlvbjogJycsXHJcbiAgICBmb3JtYXQ6ICcnLFxyXG4gICAgZ3JvdXBOYW1lOiAnJyxcclxuICAgIHZpc2libGU6IHRydWVcclxuICB9O1xyXG5cclxuICBidWxrQXBwbHlDb2x1bW5zTG9va3VwOiBDb2x1bW5WaXNpYmlsaXR5QnVsa0xvb2t1cCA9IHt9O1xyXG4gIGNvbHVtbkZpZWxkVGl0bGUgPSAnQ09MVU1OUyc7XHJcbiAgY29sdW1uU2VhcmNoVmFsdWUgPSAnJztcclxuICBjb2x1bW5TdWJqZWN0U3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgY29sdW1uc0RhdGEgPSBbXTtcclxuICBjb2x1bW5zU3ViamVjdCA9IG5ldyBTdWJqZWN0PENvbHVtblZpc2liaWxpdHlUeXBlPigpO1xyXG4gIGNvcHlPZkNvbHVtbkxpc3Q6IFBCSUdyaWRDb2x1bW5bXSA9IFtdO1xyXG4gIGRlc3Ryb3llZDogYm9vbGVhbjtcclxuICBpc01hc3RlckdyaWRTZWxlY3RlZCA9IHRydWU7XHJcbiAgc2VsZWN0QWxsQ2hlY2tlZCA9IGZhbHNlO1xyXG4gIHNlbGVjdGVkQ29sczogU2VsZWN0ZWRDb2x1bW5JbmZvW10gPSBbXTtcclxuICBzZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3Q6IERldkV4cHJlc3MudWkuZHhEYXRhR3JpZFtdID0gW107XHJcbiAgc2VsZWN0ZWRHcm91cE5hbWVzOiBzdHJpbmdbXSA9IFtdO1xyXG4gIHNob3dCYXNpY0Zvcm1hdCA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgnYmFzaWNGb3JtYXRWaXNpYmlsaXR5JykpIHx8IGZhbHNlO1xyXG4gIHNob3dDb2x1bW5MaXN0ID0gdHJ1ZTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgLy8jcmVnaW9uIEFuZ3VsYXIgTGlmZWN5Y2xlIEV2ZW50c1xyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0Lmxlbmd0aCkge1xyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWQgPT4ge1xyXG4gICAgICAgICAgZ3JpZC5jb2x1bW5PcHRpb24oY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5kYXRhRmllbGQsIHsgdmlzaWJsZTogZmFsc2UgfSk7XHJcbiAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50QXBwbGllZFZpZXcnLFxyXG4gICAgICAgICAgICBKU09OLnN0cmluZ2lmeShncmlkLnN0YXRlKCkpKTsgLy8gaG9sZGluZyBzdGF0ZSB0byBtYWtlIHN1cmUgZ3JvdXAtYnkgd29ya3Mgd2hlbiB3ZSB0b2dnbGUgc2VsZWN0QWxsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0sIDApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29scy5mb3JFYWNoKGNvbCA9PiB7XHJcbiAgICAgIHRoaXMuY29sdW1uc0RhdGEuZmlsdGVyKGNvbHVtbiA9PiBjb2x1bW4uZGF0YUZpZWxkID09PSBjb2wuZGF0YUZpZWxkKVswXS52aXNpYmxlID0gIWNvbC52aXNpYmxlO1xyXG4gICAgfSk7XHJcbiAgICBjb25zdCBhbnlDb2x1bW5WaXNpYmxlID0gdGhpcy5jb2x1bW5zRGF0YS5maW5kKGl0ZW0gPT4gaXRlbS52aXNpYmxlKTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdC5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICBpdGVtLmNvbHVtbk9wdGlvbihjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCwgeyB2aXNpYmxlOiBhbnlDb2x1bW5WaXNpYmxlID8gdHJ1ZSA6IGZhbHNlIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHNlc3Npb25TdG9yYWdlLnJlbW92ZUl0ZW0oJ2N1cnJlbnRBcHBsaWVkVmlldycpO1xyXG4gICAgLy8gTGV0cyBjb2x1bW5zIGZpbmlzaCB1cCByZW5kZXJpbmcgaWYgYW55IG91dHN0YW5kaW5nLiBXaWxsIHVuc3ViY3JpYmUgaXRzZWxmIHdoZW5cclxuICAgIC8vIGRlc3Ryb3llZCBwYXJhbWV0ZXIgaXMgc2V0IHRvIHRydWUuXHJcbiAgICB0aGlzLmRlc3Ryb3llZCA9IHRydWU7XHJcbiAgICB0aGlzLmNvbHVtbnNTdWJqZWN0Lm5leHQobnVsbCk7XHJcbiAgfVxyXG4gIC8vI2VuZHJlZ2lvbiBBbmd1bGFyIExpZmVjeWNsZSBFdmVudHNcclxuXHJcbiAgLy8jcmVnaW9uIFB1YmxpYyBNZXRob2RzXHJcbiAgcHVibGljIHRvZ2dsZUNvbHVtblZpc2liaWxpdHkoZGF0YUZpZWxkOiBzdHJpbmcsIHZpc2libGU6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmlzQ2FjaGVkKSB7XHJcbiAgICAgIGNvbnN0IGNvbEluZGV4ID0gdGhpcy5zZWxlY3RlZENvbHMuZmluZEluZGV4KGRhdGEgPT4gZGF0YS5kYXRhRmllbGQgPT09IGRhdGFGaWVsZCk7XHJcbiAgICAgIGlmIChjb2xJbmRleCAhPT0gLTEpIHtcclxuICAgICAgICBpZiAodmlzaWJsZSkge1xyXG4gICAgICAgICAgdmlzaWJsZSA9ICF0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdFswXS5nZXRWaXNpYmxlQ29sdW1ucygpPy5cclxuICAgICAgICAgICAgZmlsdGVyKGNvbCA9PiBjb2wuaGFzT3duUHJvcGVydHkoJ2RhdGFGaWVsZCcpICYmIGNvbC5kYXRhRmllbGQgPT09IGRhdGFGaWVsZCkubGVuZ3RoO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXZpc2libGUpIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRDb2xzLnNwbGljZShjb2xJbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKGNvbEluZGV4ID09PSAtMSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRDb2xzLnB1c2goeyBkYXRhRmllbGQsIHZpc2libGUgfSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAodGhpcy51c2VCdWxrQXBwbHkpIHtcclxuICAgICAgdGhpcy5idWxrQXBwbHlDb2x1bW5zTG9va3VwW2RhdGFGaWVsZC50b0xvd2VyQ2FzZSgpXSA9IHsgZGF0YUZpZWxkLCB2aXNpYmxlIH07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgdGhpcy5jb2x1bW5zRGF0YS5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICBpZiAoZGF0YUZpZWxkID09PSB0aGlzLmNvbHVtbnNEYXRhW2luZGV4XS5kYXRhRmllbGQpIHtcclxuICAgICAgICAgIHRoaXMuY29sdW1uc0RhdGFbaW5kZXhdLnZpc2libGUgPSB2aXNpYmxlO1xyXG4gICAgICAgICAgdGhpcy5jb2x1bW5zU3ViamVjdC5uZXh0KHsgdmlzaWJsZSwgZGF0YUZpZWxkIH0pO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdG9nZ2xlR3JvdXBWaXNpYmlsaXR5KGV2ZW50OiBNb3VzZUV2ZW50LCBncm91cE5hbWU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRHcm91cE5hbWVzLmluZGV4T2YoZ3JvdXBOYW1lKSA+IC0xKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRHcm91cE5hbWVzID0gdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMuZmlsdGVyKGFyciA9PiBhcnIgIT09IGdyb3VwTmFtZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkR3JvdXBOYW1lcy5wdXNoKGdyb3VwTmFtZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25TZWFyY2hDb2x1bW5WYWx1ZUNoYW5nZShhcmdzOiBEeFRleHRCb3hDb21wb25lbnQpOiB2b2lkIHtcclxuICAgIHRoaXMuY29weU9mQ29sdW1uTGlzdCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5jb2x1bW5zRGF0YS5maWx0ZXIoZGF0YSA9PlxyXG4gICAgICBkYXRhLmNhcHRpb24udG9Mb3dlckNhc2UoKS5pbmRleE9mKGFyZ3MudmFsdWUudG9Mb3dlckNhc2UoKSkgPiAtMSkpKTtcclxuICAgIGNvbnN0IHZpc2libGVDb2wgPSB0aGlzLnNlbGVjdGVkQ29scy5maWx0ZXIoZGF0YSA9PiBkYXRhLnZpc2libGUpO1xyXG4gICAgaWYgKHZpc2libGVDb2wubGVuZ3RoKSB7XHJcbiAgICAgIHZpc2libGVDb2wuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICB0aGlzLmNvcHlPZkNvbHVtbkxpc3QuZmlsdGVyKGRhdGEgPT4gZGF0YS5kYXRhRmllbGQgPT09IGl0ZW0uZGF0YUZpZWxkKS5tYXAoKGNvbDogUEJJR3JpZENvbHVtbikgPT4gY29sLnZpc2libGUgPSB0cnVlKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY29sdW1uQ2hvb3NlckNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZCB7XHJcbiAgICBjb25zdCB0YXJnZXQgPSBldmVudC50YXJnZXQgYXMgSFRNTElucHV0RWxlbWVudDtcclxuICAgIGlmICh0YXJnZXQgJiYgdGFyZ2V0LmRhdGFzZXQpIHtcclxuICAgICAgc3dpdGNoICh0YXJnZXQudGFnTmFtZS50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgY2FzZSAnbGFiZWwnOlxyXG4gICAgICAgICAgdGhpcy5vbkNsaWNrQ29sdW1uSXRlbSh0YXJnZXQuZGF0YXNldC5pdGVtKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkNsaWNrTWFzdGVyQ2hlY2tCb3godmFsOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICB0aGlzLnNlbGVjdEFsbENoZWNrZWQgPSB2YWw7XHJcbiAgICB0aGlzLmNvcHlPZkNvbHVtbkxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgaXRlbS52aXNpYmxlID0gdmFsO1xyXG4gICAgfSk7XHJcbiAgICBpZiAodGhpcy5pc0NhY2hlZCkge1xyXG4gICAgICB0aGlzLmNvbHVtbnNEYXRhLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgaWYgKGl0ZW0uZGF0YUZpZWxkICE9PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCkge1xyXG4gICAgICAgICAgdGhpcy50b2dnbGVDb2x1bW5WaXNpYmlsaXR5KGl0ZW0uZGF0YUZpZWxkLCB2YWwpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9IGlmICh0aGlzLnVzZUJ1bGtBcHBseSkge1xyXG4gICAgICB0aGlzLmNvcHlPZkNvbHVtbkxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICB0aGlzLmJ1bGtBcHBseUNvbHVtbnNMb29rdXBbaXRlbS5kYXRhRmllbGQudG9Mb3dlckNhc2UoKV0gPSB7IGRhdGFGaWVsZDogaXRlbS5kYXRhRmllbGQsIHZpc2libGU6IGl0ZW0udmlzaWJsZSB9O1xyXG4gICAgICB9KTtcclxuXHJcbiAgICB9XHJcbiAgICBlbHNlIHtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5jb3B5T2ZDb2x1bW5MaXN0LmZvckVhY2goaXRlbSA9PiB7IGl0ZW0udmlzaWJsZSA9IHZhbDsgfSk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgIGl0ZW0ub3B0aW9uKCdjb2x1bW5zJywgW10pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmNvbHVtbnNEYXRhLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICBpZiAoaXRlbS5kYXRhRmllbGQgIT09IGN1c3RvbUFjdGlvbkNvbHVtbkluZm8uZGF0YUZpZWxkKSB7XHJcbiAgICAgICAgICAgIGl0ZW0udmlzaWJsZSA9IHZhbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgIGl0ZW0ub3B0aW9uKCdjb2x1bW5zJywgdGhpcy5jb2x1bW5zRGF0YSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdEFsbENoZWNrZWQpIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGl0ZW0uc3RhdGUoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCdjdXJyZW50QXBwbGllZFZpZXcnKSkpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LCAwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBmZXRjaENhY2hlZENvbHVtbnNEYXRhKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2xzLmxlbmd0aCkge1xyXG4gICAgICBjb25zdCBhY3Rpb25Db2x1bW4gPSB0aGlzLmNvbHVtbnNEYXRhLmZpbHRlcihkYXRhID0+IGRhdGEuZGF0YUZpZWxkID09PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCAmJiBkYXRhLmdyb3VwTmFtZSA9PT0gY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5ncm91cE5hbWUpWzBdO1xyXG4gICAgICBjb25zdCB2aXNpYmxlQ29sdW1ucyA9IHRoaXMuc2VsZWN0ZWRDb2xzLmZpbHRlcihkYXRhID0+IGRhdGEudmlzaWJsZSkubWFwKGl0ZW0gPT4gaXRlbS5kYXRhRmllbGQpO1xyXG4gICAgICBpZiAoYWN0aW9uQ29sdW1uKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgIGl0ZW0uY29sdW1uT3B0aW9uKGFjdGlvbkNvbHVtbi5kYXRhRmllbGQsIHsgdmlzaWJsZTogdmlzaWJsZUNvbHVtbnMubGVuZ3RoID8gdHJ1ZSA6IGZhbHNlLCBmaXhlZDogdHJ1ZSwgZml4ZWRQb3NpdGlvbjogJ3JpZ2h0JyB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChncmlkSW5zdCA9PiB7XHJcbiAgICAgICAgZ3JpZEluc3QuYmVnaW5VcGRhdGUoKTtcclxuICAgICAgICB0aGlzLmNvbHVtbnNMaXN0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICBncmlkSW5zdC5jb2x1bW5PcHRpb24oaXRlbS5kYXRhRmllbGQsICd2aXNpYmxlJywgZmFsc2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGdyaWRJbnN0Lm9wdGlvbignZGF0YVNvdXJjZScsIFtdKTtcclxuICAgICAgICBncmlkSW5zdC5lbmRVcGRhdGUoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG4gICAgLy8gVE9ETzogY2hlY2sgZW5hYmxlIGFuZCBkaXNhYmxlIGdyaWQgZXhwb3J0LiBJZiBub3QgdGhlbiByZW1vdmUgYmVsb3cgY29kZS5cclxuICAgIGlmICgodGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3RbMF0uZ2V0VmlzaWJsZUNvbHVtbnMoKS5maWx0ZXIoZGF0YSA9PiAoZGF0YSBhcyBhbnkpLmNvbW1hbmQgIT09ICdlbXB0eScpLmxlbmd0aCkgPiAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0WzBdLm9wdGlvbignZXhwb3J0LmVuYWJsZWQnLCBmYWxzZSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmFwcGx5QnV0dG9uQ2xpY2suZW1pdCh0aGlzLnNlbGVjdGVkQ29scyk7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29scyA9IFtdO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGZvcm1hdE9wdGlvbnNDaGFuZ2VkKGFyZ3MpOiB2b2lkIHtcclxuICAgIHRoaXMuY29sdW1uc0RhdGEuZm9yRWFjaChjb2wgPT4ge1xyXG4gICAgICBpZiAoY29sLmRhdGFGaWVsZCA9PT0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGE/LmRhdGFGaWVsZCkge1xyXG4gICAgICAgIGNvbC5jYXB0aW9uID0gYXJncy5jYXB0aW9uO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICBpdGVtLmNvbHVtbk9wdGlvbihjb2wuZGF0YUZpZWxkLCB7IGNhcHRpb246IGNvbC5jYXB0aW9uIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuY29weU9mQ29sdW1uTGlzdC5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICBpZiAoaXRlbS5kYXRhRmllbGQgPT09IHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhPy5kYXRhRmllbGQpIHtcclxuICAgICAgICBpdGVtLmNhcHRpb24gPSBhcmdzLmNhcHRpb247XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRm9ybWF0VG9nZ2xlZCh2YWw6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgIHRoaXMuc2hvd0Jhc2ljRm9ybWF0ID0gdmFsO1xyXG4gICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnYmFzaWNGb3JtYXRWaXNpYmlsaXR5JywgSlNPTi5zdHJpbmdpZnkodmFsKSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25CdWxrQXBwbHlDbGljaygpOiB2b2lkIHtcclxuICAgIHRoaXMuYnVsa0FwcGx5Q29sdW1ucyhPYmplY3QudmFsdWVzKHRoaXMuYnVsa0FwcGx5Q29sdW1uc0xvb2t1cCkpO1xyXG4gICAgdGhpcy5idWxrQXBwbHlDb2x1bW5zTG9va3VwID0ge307XHJcbiAgICB0aGlzLmNsb3NlRmx5T3V0KCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xvc2VGbHlPdXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsb3NlQ3VycmVudEZseU91dC5lbWl0KCk7XHJcbiAgfVxyXG4gIC8vI2VuZHJlZ2lvbiBQdWJsaWMgTWV0aG9kc1xyXG5cclxuICAvLyNyZWdpb24gUHJpdmF0ZSBNZXRob2RzXHJcblxyXG4gIHByaXZhdGUgb25DbGlja0NvbHVtbkl0ZW0oZGF0YUZpZWxkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhID0gdGhpcy5jb2x1bW5zRGF0YS5maWx0ZXIoZWxlbWVudCA9PiBlbGVtZW50LmRhdGFGaWVsZC50b0xvd2VyQ2FzZSgpID09PSBkYXRhRmllbGQudG9Mb3dlckNhc2UoKSlbMF07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJyaW5nRWxlbWVudFRvVmlld0FyZWEoZGF0YUZpZWxkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIGNvbnN0IGRhdGFGaWVsZEVsZW0gPSB3aW5kb3cuZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgaW5wdXRbZGF0YS1pdGVtPScke2RhdGFGaWVsZH0nXWApO1xyXG4gICAgY29uc3QgY29udGFpbmVyID0gd2luZG93LmRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2NvbHVtbkNob29zZXItZ3JvdXAtY29sdW1ucycpO1xyXG4gICAgaWYgKGNvbnRhaW5lci5sZW5ndGggJiYgZGF0YUZpZWxkRWxlbS5sZW5ndGgpIHtcclxuICAgICAgY29uc3QgdG9wUG9zID0gKGRhdGFGaWVsZEVsZW1bMF0gYXMgSFRNTEVsZW1lbnQpLm9mZnNldFRvcDtcclxuICAgICAgY29udGFpbmVyWzBdLnNjcm9sbFRvcCA9IHRvcFBvcyAtICgoY29udGFpbmVyWzBdIGFzIEhUTUxFbGVtZW50KS5vZmZzZXRUb3AgKyAxMCk7IC8vIDEwIGlzIG9mZnNldCB0byBlbmhhbmNlIFVYLlxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzdWJzY3JpYmVUb0NvbHVtbkNoYW5nZXMoZGVib3VuY2U6IG51bWJlcik6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuY29sdW1uU3ViamVjdFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLmNvbHVtblN1YmplY3RTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICAgIHRoaXMuY29sdW1uU3ViamVjdFN1YnNjcmlwdGlvbiA9IHRoaXMuY29sdW1uc1N1YmplY3QucGlwZShidWZmZXIodGhpcy5jb2x1bW5zU3ViamVjdC5waXBlKGRlYm91bmNlVGltZShkZWJvdW5jZSkpKSlcclxuICAgICAgLnN1YnNjcmliZShmaWVsZHMgPT4ge1xyXG4gICAgICAgIGZpZWxkcyA9IGZpZWxkcy5maWx0ZXIoZiA9PiBmKTtcclxuICAgICAgICB0aGlzLmJ1bGtBcHBseUNvbHVtbnMoZmllbGRzKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZGVzdHJveWVkKSB7XHJcbiAgICAgICAgICB0aGlzLmNvbHVtblN1YmplY3RTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJ1bGtBcHBseUNvbHVtbnMoZmllbGRzOiBDb2x1bW5WaXNpYmlsaXR5VHlwZVtdKTogdm9pZCB7XHJcbiAgICBpZiAoZmllbGRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHsgaXRlbS5iZWdpbkN1c3RvbUxvYWRpbmcoJ0xvYWRpbmcnKTsgaXRlbS5iZWdpblVwZGF0ZSgpOyB9KTtcclxuICAgICAgZmllbGRzLmZvckVhY2goZiA9PiB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IGl0ZW0uY29sdW1uT3B0aW9uKGYuZGF0YUZpZWxkLCB7IHZpc2libGU6IGYudmlzaWJsZSB9KSk7XHJcbiAgICAgIH0pXHJcbiAgICAgIGlmICh0aGlzLnJlZnJlc2hPbkNvbHVtblVwZGF0ZSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiBpdGVtLnJlZnJlc2godHJ1ZSkpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiBpdGVtLnJlcGFpbnQoKSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHsgaXRlbS5lbmRDdXN0b21Mb2FkaW5nKCk7IGl0ZW0uZW5kVXBkYXRlKCk7IH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8jZW5kcmVnaW9uIFByaXZhdGUgTWV0aG9kc1xyXG5cclxufVxyXG4iXX0=