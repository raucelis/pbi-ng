import { __decorate } from "tslib";
import { CGFFlyOutEnum } from '../../utilities/enums';
import { Component, Output, Input, EventEmitter } from '@angular/core';
import { actionTypes, cgfFlyOutList } from '../../utilities/constants';
import { appToast, openDestination, filterSelectedRowData } from '../../utilities/utilityFunctions';
var FlyOutActionIconContainerComponent = /** @class */ (function () {
    function FlyOutActionIconContainerComponent() {
        this.position = 'right';
        this.showLoader = false;
        this.showParametersControls = false;
        this.visibleFlyOuts = [];
        this.flyOutSelectionClick = new EventEmitter();
        this.refreshEntityData = new EventEmitter();
        this.activeFlyOut = { id: -1 };
        this.cgfLeftMenuDropDownListItems = [];
        this.cgfLeftMenus = [];
        this.currentFlyOutSection = { id: -1 };
        this.flyOutSections = [];
        this.leftMenuDropDownList = [];
        this.leftMenuList = [];
    }
    Object.defineProperty(FlyOutActionIconContainerComponent.prototype, "resetContainerSettings", {
        set: function (val) {
            if (val) {
                this.currentFlyOutSection = { id: -1 };
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FlyOutActionIconContainerComponent.prototype, "selectedFlyOut", {
        get: function () { return this.activeFlyOut; },
        set: function (val) {
            if (val) {
                this.activeFlyOut = val;
            }
            else {
                this.activeFlyOut = { id: -1 };
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FlyOutActionIconContainerComponent.prototype, "cgfLeftMenuList", {
        get: function () { return this.cgfLeftMenus; },
        set: function (data) {
            this.cgfLeftMenus = data;
            if (data) {
                this.prepareExtraActions();
            }
        },
        enumerable: true,
        configurable: true
    });
    FlyOutActionIconContainerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.flyOutSections = cgfFlyOutList.filter(function (item) { return _this.visibleFlyOuts.indexOf(item.id) > -1; });
    };
    FlyOutActionIconContainerComponent.prototype.onFlyOutIconClick = function (evt, item) {
        if (item.id === CGFFlyOutEnum.newTab) {
            open(location.href, '_blank');
            return;
        }
        if (item.id === CGFFlyOutEnum.refreshData) {
            this.refreshData();
            return;
        }
        if (item.id === this.activeFlyOut.id) {
            this.activeFlyOut = { id: -1 };
        }
        else {
            this.activeFlyOut = item;
        }
        this.showParametersControls = false;
        this.flyOutSelectionClick.emit({ evt: evt, item: item });
    };
    FlyOutActionIconContainerComponent.prototype.showParametersControlsContainer = function () {
        this.currentFlyOutSection = { id: -1 };
        this.showParametersControls = this.showParametersControls ? false : true;
        this.flyOutSelectionClick.emit({ showEntityParameter: this.showParametersControls });
    };
    FlyOutActionIconContainerComponent.prototype.refreshData = function () {
        if (this.disableRefreshIcon) {
            appToast({ type: 'error', message: 'Parameters are required.' });
            return;
        }
        this.showLoader = true;
        this.refreshEntityData.emit();
    };
    FlyOutActionIconContainerComponent.prototype.cgfLeftMenuClick = function (data) {
        openDestination(data, filterSelectedRowData(this.gridInstance), this.dataBrowserEntityParameters, null); // this.appService
    };
    FlyOutActionIconContainerComponent.prototype.onMoreItemClick = function (args) {
        if (!args.itemData.items) {
            openDestination(args.itemData, filterSelectedRowData(this.gridInstance), this.dataBrowserEntityParameters);
        }
    };
    FlyOutActionIconContainerComponent.prototype.onFlyOutSelectionClick = function (event, item) {
        if (item.id === CGFFlyOutEnum.newTab) {
            open(location.href, '_blank');
            return;
        }
        if (item.id === CGFFlyOutEnum.refreshData) {
            this.refreshData();
            return;
        }
        if (item.id === this.currentFlyOutSection.id) {
            this.currentFlyOutSection = { id: -1 };
        }
        else {
            this.currentFlyOutSection = item;
        }
        this.showParametersControls = false;
        this.flyOutSelectionClick.emit({ event: event, item: item });
    };
    FlyOutActionIconContainerComponent.prototype.prepareExtraActions = function () {
        this.leftMenuList = this.cgfLeftMenus.filter(function (x) { return x.IsFrequentlyUsed === true && x.ShowAsContextMenu === false
            && x.DestinationType !== actionTypes.childEntity; });
        this.cgfLeftMenuDropDownListItems = this.cgfLeftMenus.filter(function (x) { return x.IsFrequentlyUsed === false && x.ShowAsContextMenu === false
            && x.DestinationType !== actionTypes.childEntity; });
        this.leftMenuDropDownList = [{ text: 'More', icon: 'overflow', items: this.cgfLeftMenuDropDownListItems }];
        setTimeout(function () {
            var elm = document.querySelector('.optionsActions .dx-menu-item-popout-container');
            if (elm !== null) {
                elm.remove();
            }
        }, 100);
    };
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "dataBrowserEntityParameters", void 0);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "disableRefreshIcon", void 0);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "gridInstance", void 0);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "position", void 0);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "showLoader", void 0);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "showParametersControls", void 0);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "visibleFlyOuts", void 0);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "resetContainerSettings", null);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "selectedFlyOut", null);
    __decorate([
        Input()
    ], FlyOutActionIconContainerComponent.prototype, "cgfLeftMenuList", null);
    __decorate([
        Output()
    ], FlyOutActionIconContainerComponent.prototype, "flyOutSelectionClick", void 0);
    __decorate([
        Output()
    ], FlyOutActionIconContainerComponent.prototype, "refreshEntityData", void 0);
    FlyOutActionIconContainerComponent = __decorate([
        Component({
            selector: 'pbi-fly-out-action-icon-container',
            template: "<div [ngClass]=\"{'flex-end': position === 'right', 'flex-start': position === 'left'}\">\r\n    <ul class=\"no-list-style filter-list\">\r\n        <li *ngFor=\"let item of leftMenuList\">\r\n            <span (click)=\"cgfLeftMenuClick(item)\">\r\n                <i *ngIf=\"item.IconClassName\" class=\"{{item.IconClassName}} pointer dynamic-tooltip flyOutSelectionIcon\"\r\n                    attr.title=\"{{item.DisplayName}} \"></i>\r\n                <button *ngIf=\"!item.IconClassName\">{{item.DisplayName}}</button>\r\n            </span>\r\n        </li>\r\n    </ul>\r\n    <dx-menu cssClass=\"optionsActions\" #menu [dataSource]=\"leftMenuDropDownList\" displayExpr=\"DisplayName\"\r\n        (onItemClick)=\"onMoreItemClick($event)\" title=\"More\" class=\"left-menu-item-list-control\"\r\n        *ngIf=\"cgfLeftMenuDropDownListItems.length > 0\">\r\n    </dx-menu>\r\n    <div>\r\n        <ul class=\"icon-list\">\r\n            <li *ngFor=\"let sectionItem of flyOutSections\">\r\n                <i class=\"{{sectionItem.src}}\" [class.pointer-none-events]=\"\"\r\n                    [class.active-flyOut]=\"activeFlyOut.id === sectionItem.id\" [class.loading]=\"\"\r\n                    title=\"{{sectionItem.title}}\" (click)=\"onFlyOutIconClick($event, sectionItem)\">\r\n                </i>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n",
            styles: [""]
        })
    ], FlyOutActionIconContainerComponent);
    return FlyOutActionIconContainerComponent;
}());
export { FlyOutActionIconContainerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmx5LW91dC1hY3Rpb24taWNvbi1jb250YWluZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3RELE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBT3BHO0lBK0NFO1FBMUNnQixhQUFRLEdBQXFCLE9BQU8sQ0FBQztRQUNyQyxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLDJCQUFzQixHQUFHLEtBQUssQ0FBQztRQUMvQixtQkFBYyxHQUFvQixFQUFFLENBQUM7UUE0QnBDLHlCQUFvQixHQUFRLElBQUksWUFBWSxFQUFFLENBQUM7UUFDL0Msc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV4RCxpQkFBWSxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUIsaUNBQTRCLEdBQUcsRUFBRSxDQUFDO1FBQ2xDLGlCQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLHlCQUFvQixHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEMsbUJBQWMsR0FBeUIsRUFBRSxDQUFDO1FBQzFDLHlCQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMxQixpQkFBWSxHQUFHLEVBQUUsQ0FBQztJQUVGLENBQUM7SUFwQ2pCLHNCQUFJLHNFQUFzQjthQUExQixVQUEyQixHQUFZO1lBQ3JDLElBQUksR0FBRyxFQUFFO2dCQUNQLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ3hDO1FBQ0gsQ0FBQzs7O09BQUE7SUFHRCxzQkFBSSw4REFBYzthQU9sQixjQUF1QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2FBUGxELFVBQW1CLEdBQUc7WUFDcEIsSUFBSSxHQUFHLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUM7YUFDekI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ2hDO1FBQ0gsQ0FBQzs7O09BQUE7SUFJRCxzQkFBSSwrREFBZTthQU1uQixjQUF3QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2FBTm5ELFVBQW9CLElBQUk7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxJQUFJLEVBQUU7Z0JBQ1IsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7YUFDNUI7UUFDSCxDQUFDOzs7T0FBQTtJQWdCRCxxREFBUSxHQUFSO1FBQUEsaUJBRUM7UUFEQyxJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQXpDLENBQXlDLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRU0sOERBQWlCLEdBQXhCLFVBQXlCLEdBQWUsRUFBRSxJQUFtQjtRQUMzRCxJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztZQUM5QixPQUFPO1NBQ1I7UUFDRCxJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssYUFBYSxDQUFDLFdBQVcsRUFBRTtZQUN6QyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkIsT0FBTztTQUNSO1FBRUQsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUNoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDMUI7UUFDRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVNLDRFQUErQixHQUF0QztRQUNFLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ3pFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxtQkFBbUIsRUFBRSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFFTSx3REFBVyxHQUFsQjtRQUNFLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzNCLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLENBQUMsQ0FBQztZQUNqRSxPQUFPO1NBQ1I7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVNLDZEQUFnQixHQUF2QixVQUF3QixJQUFJO1FBQzFCLGVBQWUsQ0FBQyxJQUFJLEVBQUUscUJBQXFCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLGtCQUFrQjtJQUM3SCxDQUFDO0lBRU0sNERBQWUsR0FBdEIsVUFBdUIsSUFBSTtRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUU7WUFDeEIsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUscUJBQXFCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1NBQzVHO0lBQ0gsQ0FBQztJQUVNLG1FQUFzQixHQUE3QixVQUE4QixLQUFVLEVBQUUsSUFBbUI7UUFDM0QsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDOUIsT0FBTztTQUNSO1FBQ0QsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLGFBQWEsQ0FBQyxXQUFXLEVBQUU7WUFDekMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLE9BQU87U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsb0JBQW9CLENBQUMsRUFBRSxFQUFFO1lBQzVDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ3hDO2FBQU07WUFDTCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1NBQ2xDO1FBQ0QsSUFBSSxDQUFDLHNCQUFzQixHQUFHLEtBQUssQ0FBQztRQUNwQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxPQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUUsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTyxnRUFBbUIsR0FBM0I7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGdCQUFnQixLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsaUJBQWlCLEtBQUssS0FBSztlQUN6RyxDQUFDLENBQUMsZUFBZSxLQUFLLFdBQVcsQ0FBQyxXQUFXLEVBREEsQ0FDQSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLDRCQUE0QixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLGdCQUFnQixLQUFLLEtBQUssSUFBSSxDQUFDLENBQUMsaUJBQWlCLEtBQUssS0FBSztlQUMxSCxDQUFDLENBQUMsZUFBZSxLQUFLLFdBQVcsQ0FBQyxXQUFXLEVBRGdCLENBQ2hCLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUMsQ0FBQztRQUMzRyxVQUFVLENBQUM7WUFDVCxJQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLGdEQUFnRCxDQUFDLENBQUM7WUFDckYsSUFBSSxHQUFHLEtBQUssSUFBSSxFQUFFO2dCQUNoQixHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDZDtRQUNILENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNWLENBQUM7SUE5SFE7UUFBUixLQUFLLEVBQUU7MkZBQTJFO0lBQzFFO1FBQVIsS0FBSyxFQUFFO2tGQUFvQztJQUNuQztRQUFSLEtBQUssRUFBRTs0RUFBaUM7SUFDaEM7UUFBUixLQUFLLEVBQUU7d0VBQTZDO0lBQzVDO1FBQVIsS0FBSyxFQUFFOzBFQUEyQjtJQUMxQjtRQUFSLEtBQUssRUFBRTtzRkFBdUM7SUFDdEM7UUFBUixLQUFLLEVBQUU7OEVBQTZDO0lBR3JEO1FBREMsS0FBSyxFQUFFO29GQUtQO0lBR0Q7UUFEQyxLQUFLLEVBQUU7NEVBT1A7SUFJRDtRQURDLEtBQUssRUFBRTs2RUFNUDtJQUdTO1FBQVQsTUFBTSxFQUFFO29GQUF1RDtJQUN0RDtRQUFULE1BQU0sRUFBRTtpRkFBK0M7SUFyQzdDLGtDQUFrQztRQUw5QyxTQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsbUNBQW1DO1lBQzdDLGszQ0FBNkQ7O1NBRTlELENBQUM7T0FDVyxrQ0FBa0MsQ0FrSTlDO0lBQUQseUNBQUM7Q0FBQSxBQWxJRCxJQWtJQztTQWxJWSxrQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZHhEYXRhR3JpZCBmcm9tICdkZXZleHRyZW1lL3VpL2RhdGFfZ3JpZCc7XHJcbmltcG9ydCB7IENHRkZseU91dEVudW0gfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvZW51bXMnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgT3V0cHV0LCBJbnB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVyc0VudmVsb3BlLCBGbHlPdXRTZWN0aW9uIH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbW1vbi1ncmlkLWZyYW1ld29yayc7XHJcbmltcG9ydCB7IGFjdGlvblR5cGVzLCBjZ2ZGbHlPdXRMaXN0IH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IGFwcFRvYXN0LCBvcGVuRGVzdGluYXRpb24sIGZpbHRlclNlbGVjdGVkUm93RGF0YSB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy91dGlsaXR5RnVuY3Rpb25zJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAncGJpLWZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vZmx5LW91dC1hY3Rpb24taWNvbi1jb250YWluZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmx5T3V0QWN0aW9uSWNvbkNvbnRhaW5lckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBJbnB1dCgpIHB1YmxpYyBkYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnM6IERhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVyc0VudmVsb3BlW107XHJcbiAgQElucHV0KCkgcHVibGljIGRpc2FibGVSZWZyZXNoSWNvbjogYm9vbGVhbjtcclxuICBASW5wdXQoKSBwdWJsaWMgZ3JpZEluc3RhbmNlOiBkeERhdGFHcmlkO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBwb3NpdGlvbjogJ3JpZ2h0JyB8ICdsZWZ0JyA9ICdyaWdodCc7XHJcbiAgQElucHV0KCkgcHVibGljIHNob3dMb2FkZXIgPSBmYWxzZTtcclxuICBASW5wdXQoKSBwdWJsaWMgc2hvd1BhcmFtZXRlcnNDb250cm9scyA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyB2aXNpYmxlRmx5T3V0czogQ0dGRmx5T3V0RW51bVtdID0gW107XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgc2V0IHJlc2V0Q29udGFpbmVyU2V0dGluZ3ModmFsOiBib29sZWFuKSB7XHJcbiAgICBpZiAodmFsKSB7XHJcbiAgICAgIHRoaXMuY3VycmVudEZseU91dFNlY3Rpb24gPSB7IGlkOiAtMSB9O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQElucHV0KClcclxuICBzZXQgc2VsZWN0ZWRGbHlPdXQodmFsKSB7XHJcbiAgICBpZiAodmFsKSB7XHJcbiAgICAgIHRoaXMuYWN0aXZlRmx5T3V0ID0gdmFsO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hY3RpdmVGbHlPdXQgPSB7IGlkOiAtMSB9O1xyXG4gICAgfVxyXG4gIH1cclxuICBnZXQgc2VsZWN0ZWRGbHlPdXQoKSB7IHJldHVybiB0aGlzLmFjdGl2ZUZseU91dDsgfVxyXG5cclxuICBASW5wdXQoKVxyXG4gIHNldCBjZ2ZMZWZ0TWVudUxpc3QoZGF0YSkge1xyXG4gICAgdGhpcy5jZ2ZMZWZ0TWVudXMgPSBkYXRhO1xyXG4gICAgaWYgKGRhdGEpIHtcclxuICAgICAgdGhpcy5wcmVwYXJlRXh0cmFBY3Rpb25zKCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldCBjZ2ZMZWZ0TWVudUxpc3QoKSB7IHJldHVybiB0aGlzLmNnZkxlZnRNZW51czsgfVxyXG5cclxuICBAT3V0cHV0KCkgcHVibGljIGZseU91dFNlbGVjdGlvbkNsaWNrOiBhbnkgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyByZWZyZXNoRW50aXR5RGF0YSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgYWN0aXZlRmx5T3V0ID0geyBpZDogLTEgfTtcclxuICBjZ2ZMZWZ0TWVudURyb3BEb3duTGlzdEl0ZW1zID0gW107XHJcbiAgY2dmTGVmdE1lbnVzID0gW107XHJcbiAgY3VycmVudEZseU91dFNlY3Rpb24gPSB7IGlkOiAtMSB9O1xyXG4gIGZseU91dFNlY3Rpb25zOiBBcnJheTxGbHlPdXRTZWN0aW9uPiA9IFtdO1xyXG4gIGxlZnRNZW51RHJvcERvd25MaXN0ID0gW107XHJcbiAgbGVmdE1lbnVMaXN0ID0gW107XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5mbHlPdXRTZWN0aW9ucyA9IGNnZkZseU91dExpc3QuZmlsdGVyKGl0ZW0gPT4gdGhpcy52aXNpYmxlRmx5T3V0cy5pbmRleE9mKGl0ZW0uaWQpID4gLTEpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRmx5T3V0SWNvbkNsaWNrKGV2dDogTW91c2VFdmVudCwgaXRlbTogRmx5T3V0U2VjdGlvbik6IHZvaWQge1xyXG4gICAgaWYgKGl0ZW0uaWQgPT09IENHRkZseU91dEVudW0ubmV3VGFiKSB7XHJcbiAgICAgIG9wZW4obG9jYXRpb24uaHJlZiwgJ19ibGFuaycpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAoaXRlbS5pZCA9PT0gQ0dGRmx5T3V0RW51bS5yZWZyZXNoRGF0YSkge1xyXG4gICAgICB0aGlzLnJlZnJlc2hEYXRhKCk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBpZiAoaXRlbS5pZCA9PT0gdGhpcy5hY3RpdmVGbHlPdXQuaWQpIHtcclxuICAgICAgdGhpcy5hY3RpdmVGbHlPdXQgPSB7IGlkOiAtMSB9O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hY3RpdmVGbHlPdXQgPSBpdGVtO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zaG93UGFyYW1ldGVyc0NvbnRyb2xzID0gZmFsc2U7XHJcbiAgICB0aGlzLmZseU91dFNlbGVjdGlvbkNsaWNrLmVtaXQoeyBldnQsIGl0ZW0gfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2hvd1BhcmFtZXRlcnNDb250cm9sc0NvbnRhaW5lcigpIHtcclxuICAgIHRoaXMuY3VycmVudEZseU91dFNlY3Rpb24gPSB7IGlkOiAtMSB9O1xyXG4gICAgdGhpcy5zaG93UGFyYW1ldGVyc0NvbnRyb2xzID0gdGhpcy5zaG93UGFyYW1ldGVyc0NvbnRyb2xzID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgdGhpcy5mbHlPdXRTZWxlY3Rpb25DbGljay5lbWl0KHsgc2hvd0VudGl0eVBhcmFtZXRlcjogdGhpcy5zaG93UGFyYW1ldGVyc0NvbnRyb2xzIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHJlZnJlc2hEYXRhKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZGlzYWJsZVJlZnJlc2hJY29uKSB7XHJcbiAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ2Vycm9yJywgbWVzc2FnZTogJ1BhcmFtZXRlcnMgYXJlIHJlcXVpcmVkLicgfSk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHRoaXMuc2hvd0xvYWRlciA9IHRydWU7XHJcbiAgICB0aGlzLnJlZnJlc2hFbnRpdHlEYXRhLmVtaXQoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjZ2ZMZWZ0TWVudUNsaWNrKGRhdGEpOiB2b2lkIHtcclxuICAgIG9wZW5EZXN0aW5hdGlvbihkYXRhLCBmaWx0ZXJTZWxlY3RlZFJvd0RhdGEodGhpcy5ncmlkSW5zdGFuY2UpLCB0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVycywgbnVsbCk7IC8vIHRoaXMuYXBwU2VydmljZVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uTW9yZUl0ZW1DbGljayhhcmdzKTogdm9pZCB7XHJcbiAgICBpZiAoIWFyZ3MuaXRlbURhdGEuaXRlbXMpIHtcclxuICAgICAgb3BlbkRlc3RpbmF0aW9uKGFyZ3MuaXRlbURhdGEsIGZpbHRlclNlbGVjdGVkUm93RGF0YSh0aGlzLmdyaWRJbnN0YW5jZSksIHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkZseU91dFNlbGVjdGlvbkNsaWNrKGV2ZW50OiBhbnksIGl0ZW06IEZseU91dFNlY3Rpb24pOiB2b2lkIHtcclxuICAgIGlmIChpdGVtLmlkID09PSBDR0ZGbHlPdXRFbnVtLm5ld1RhYikge1xyXG4gICAgICBvcGVuKGxvY2F0aW9uLmhyZWYsICdfYmxhbmsnKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKGl0ZW0uaWQgPT09IENHRkZseU91dEVudW0ucmVmcmVzaERhdGEpIHtcclxuICAgICAgdGhpcy5yZWZyZXNoRGF0YSgpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGl0ZW0uaWQgPT09IHRoaXMuY3VycmVudEZseU91dFNlY3Rpb24uaWQpIHtcclxuICAgICAgdGhpcy5jdXJyZW50Rmx5T3V0U2VjdGlvbiA9IHsgaWQ6IC0xIH07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmN1cnJlbnRGbHlPdXRTZWN0aW9uID0gaXRlbTtcclxuICAgIH1cclxuICAgIHRoaXMuc2hvd1BhcmFtZXRlcnNDb250cm9scyA9IGZhbHNlO1xyXG4gICAgdGhpcy5mbHlPdXRTZWxlY3Rpb25DbGljay5lbWl0KHsgZXZlbnQsIGl0ZW0gfSk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZXBhcmVFeHRyYUFjdGlvbnMoKTogdm9pZCB7XHJcbiAgICB0aGlzLmxlZnRNZW51TGlzdCA9IHRoaXMuY2dmTGVmdE1lbnVzLmZpbHRlcih4ID0+IHguSXNGcmVxdWVudGx5VXNlZCA9PT0gdHJ1ZSAmJiB4LlNob3dBc0NvbnRleHRNZW51ID09PSBmYWxzZVxyXG4gICAgICAmJiB4LkRlc3RpbmF0aW9uVHlwZSAhPT0gYWN0aW9uVHlwZXMuY2hpbGRFbnRpdHkpO1xyXG4gICAgdGhpcy5jZ2ZMZWZ0TWVudURyb3BEb3duTGlzdEl0ZW1zID0gdGhpcy5jZ2ZMZWZ0TWVudXMuZmlsdGVyKHggPT4geC5Jc0ZyZXF1ZW50bHlVc2VkID09PSBmYWxzZSAmJiB4LlNob3dBc0NvbnRleHRNZW51ID09PSBmYWxzZVxyXG4gICAgICAmJiB4LkRlc3RpbmF0aW9uVHlwZSAhPT0gYWN0aW9uVHlwZXMuY2hpbGRFbnRpdHkpO1xyXG4gICAgdGhpcy5sZWZ0TWVudURyb3BEb3duTGlzdCA9IFt7IHRleHQ6ICdNb3JlJywgaWNvbjogJ292ZXJmbG93JywgaXRlbXM6IHRoaXMuY2dmTGVmdE1lbnVEcm9wRG93bkxpc3RJdGVtcyB9XTtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICBjb25zdCBlbG0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcub3B0aW9uc0FjdGlvbnMgLmR4LW1lbnUtaXRlbS1wb3BvdXQtY29udGFpbmVyJyk7XHJcbiAgICAgIGlmIChlbG0gIT09IG51bGwpIHtcclxuICAgICAgICBlbG0ucmVtb3ZlKCk7XHJcbiAgICAgIH1cclxuICAgIH0sIDEwMCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=