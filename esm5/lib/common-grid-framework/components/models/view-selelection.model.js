var ViewSelectionModel = /** @class */ (function () {
    function ViewSelectionModel(args) {
        this.canDefault = false;
        this.canDelete = false;
        this.canGlobalDefault = false;
        this.canSave = false;
        this.canSaveNew = false;
        this.id = -1;
        this.isDefault = false;
        this.isGlobalDefault = false;
        this.isPinned = false;
        this.isPublic = false;
        this.name = '';
        if (args) {
            this.canDefault = args.CanDefault ? args.CanDefault : false;
            this.canDelete = args.CanDelete ? args.CanDelete : false;
            this.canGlobalDefault = args.CanGlobalDefault ? args.CanGlobalDefault : false;
            this.canSave = args.CanSave ? args.CanSave : false;
            this.canSaveNew = args.CanSaveNew ? args.CanSaveNew : false;
            this.id = args.Id ? args.Id : -1;
            this.isDefault = args.IsDefault ? args.IsDefault : false;
            this.isPublic = args.IsPublic ? args.IsPublic : false;
            this.name = args.Name ? args.Name : '';
            this.isGlobalDefault = args.IsGlobalDefault ? args.IsGlobalDefault : false;
            this.isPinned = args.IsPinned ? args.IsPinned : false;
        }
    }
    return ViewSelectionModel;
}());
export { ViewSelectionModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy1zZWxlbGVjdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tcG9uZW50cy9tb2RlbHMvdmlldy1zZWxlbGVjdGlvbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtJQVlJLDRCQUFZLElBQTBCO1FBWHRDLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsWUFBTyxHQUFHLEtBQUssQ0FBQztRQUNoQixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ25CLE9BQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNSLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLFNBQUksR0FBRyxFQUFFLENBQUM7UUFFTixJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ3pELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzlFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ25ELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzVELElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDekQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDdEQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDdkMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDM0UsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDekQ7SUFDTCxDQUFDO0lBQ0wseUJBQUM7QUFBRCxDQUFDLEFBM0JELElBMkJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVmlld1NlbGVjdGlvbkxheW91dCB9IGZyb20gJy4uL2NvbnRyYWN0cy92aWV3LXNlbGVjdGlvbic7XHJcblxyXG5leHBvcnQgY2xhc3MgVmlld1NlbGVjdGlvbk1vZGVsIHtcclxuICAgIGNhbkRlZmF1bHQgPSBmYWxzZTtcclxuICAgIGNhbkRlbGV0ZSA9IGZhbHNlO1xyXG4gICAgY2FuR2xvYmFsRGVmYXVsdCA9IGZhbHNlO1xyXG4gICAgY2FuU2F2ZSA9IGZhbHNlO1xyXG4gICAgY2FuU2F2ZU5ldyA9IGZhbHNlO1xyXG4gICAgaWQgPSAtMTtcclxuICAgIGlzRGVmYXVsdCA9IGZhbHNlO1xyXG4gICAgaXNHbG9iYWxEZWZhdWx0ID0gZmFsc2U7XHJcbiAgICBpc1Bpbm5lZCA9IGZhbHNlO1xyXG4gICAgaXNQdWJsaWMgPSBmYWxzZTtcclxuICAgIG5hbWUgPSAnJztcclxuICAgIGNvbnN0cnVjdG9yKGFyZ3M/OiBWaWV3U2VsZWN0aW9uTGF5b3V0KSB7XHJcbiAgICAgICAgaWYgKGFyZ3MpIHtcclxuICAgICAgICAgICAgdGhpcy5jYW5EZWZhdWx0ID0gYXJncy5DYW5EZWZhdWx0ID8gYXJncy5DYW5EZWZhdWx0IDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuY2FuRGVsZXRlID0gYXJncy5DYW5EZWxldGUgPyBhcmdzLkNhbkRlbGV0ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmNhbkdsb2JhbERlZmF1bHQgPSBhcmdzLkNhbkdsb2JhbERlZmF1bHQgPyBhcmdzLkNhbkdsb2JhbERlZmF1bHQgOiBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5jYW5TYXZlID0gYXJncy5DYW5TYXZlID8gYXJncy5DYW5TYXZlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuY2FuU2F2ZU5ldyA9IGFyZ3MuQ2FuU2F2ZU5ldyA/IGFyZ3MuQ2FuU2F2ZU5ldyA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmlkID0gYXJncy5JZCA/IGFyZ3MuSWQgOiAtMTtcclxuICAgICAgICAgICAgdGhpcy5pc0RlZmF1bHQgPSBhcmdzLklzRGVmYXVsdCA/IGFyZ3MuSXNEZWZhdWx0IDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuaXNQdWJsaWMgPSBhcmdzLklzUHVibGljID8gYXJncy5Jc1B1YmxpYyA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLm5hbWUgPSBhcmdzLk5hbWUgPyBhcmdzLk5hbWUgOiAnJztcclxuICAgICAgICAgICAgdGhpcy5pc0dsb2JhbERlZmF1bHQgPSBhcmdzLklzR2xvYmFsRGVmYXVsdCA/IGFyZ3MuSXNHbG9iYWxEZWZhdWx0IDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuaXNQaW5uZWQgPSBhcmdzLklzUGlubmVkID8gYXJncy5Jc1Bpbm5lZCA6IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=