import { __assign, __values } from "tslib";
import { rowTypeInfo, customActionColumnInfo } from '../../utilities/constants';
import { CGFStorageKeys } from '../../utilities/enums';
import { getStorageKey, getClassNameByThemeName, applyFilterCssClass, appToast, ToDictionary } from '../../utilities/utilityFunctions';
var PBIGridOptionsModel = /** @class */ (function () {
    // TODO: Parameterized constructor for assigning values
    function PBIGridOptionsModel() {
        /* https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxDataGrid/Configuration/ for configuration of the dxGrid */
        //#region Grid Configuration Members
        // baseColumnsList: Array<IMS_Web.IGridColumn> = [];
        // columns: Array<IMS_Web.IGridColumn> = [];
        /* Specifies the shortcut key that sets focus on the widget.
            IE, Chrome, Safari, Opera 15+: [ALT] + accesskey
            Opera prior version 15: [SHIFT][ESC] + accesskey
            Firefox: [ALT][SHIFT] + accesskey */
        this.accessKey = 'g';
        this.allowAdding = false;
        this.allowColumnReordering = true;
        this.allowColumnResizing = true;
        this.allowDataExport = false;
        this.allowDeleting = false;
        this.allowReordering = false; // make sure rows can be re-ordered
        this.allowSelectedDataExport = false;
        this.allowUpdating = false;
        this.allowedPageSizes = [50, 150, 300];
        this.autoExpandAll = false;
        this.autoNavigateToFocusedRow = true;
        this.columnAutoWidth = false;
        this.columnMinWidth = 50;
        this.columnRenderingMode = 'virtual';
        this.columnResizingMode = 'widget';
        this.columns = [];
        this.contextMenuMappingList = [];
        this.dataSource = [];
        this.dateSerializationFormat = 'yyyy-MM-ddtHH:mm:ss';
        this.disabled = false;
        this.editingMode = 'row';
        this.enableActiveState = false;
        this.enableCache = true;
        this.enableCellHint = true;
        this.enableColumnChooser = false;
        this.enableColumnFixing = true;
        this.enableColumnHiding = true;
        this.enableContextGrpMenu = true;
        this.enableContextMenu = true;
        this.enableErrorRow = true;
        this.enableGridFormatting = false;
        this.enableLoadPanel = false;
        this.enableStateStoring = false;
        this.filterSyncEnabled = true;
        this.filterValue = null;
        this.gridComponentInstance = null;
        this.gridFilterValue = null;
        this.gridName = 'PortfolioBI Data Grid';
        this.highlightChanges = false;
        this.hoverStateEnabled = true;
        this.isMasterGrid = true;
        this.listGroupedColumns = [];
        this.noDataText = 'No Data';
        this.pageSize = 50;
        this.pagingEnabled = true;
        this.refreshMode = 'full';
        this.remoteOperationsEnabled = false;
        this.repaintChangesOnly = false;
        this.rowAlternationEnabled = false;
        this.scrollMode = 'standard';
        this.selectedTheme = '';
        this.selectedThemeClass = ''; // Empty means regular theme will be applied.
        this.selectionMode = 'single';
        this.showBorders = true;
        this.showColumnHeaders = true;
        this.showColumnLines = true;
        this.showDragIcons = false;
        this.showFilterPanel = true; // flag to show/hide filter info at bottom of grid.
        this.showFilterRow = true;
        this.showGroupPanel = true;
        this.showHeaderFilter = true;
        this.showPageInfo = true;
        this.showPageSizeSelector = false;
        this.showPager = true;
        this.showRowLines = true;
        this.showScrollbars = true;
        this.stateStorageType = null;
        this.showSearchPanel = false;
        this.sortingType = 'multiple';
        this.useIcons = false;
        this.stringSpecificOperators = ['contains', 'notcontains', 'startswith', 'endswith'];
        this.childEntityList = [];
        this.isMasterDetailEnabled = false;
    }
    PBIGridOptionsModel.prototype.applyViewToGrid = function (viewJson) {
        this.applyStateProperties(viewJson === null || viewJson === void 0 ? void 0 : viewJson.state.gridState);
    };
    PBIGridOptionsModel.prototype.onContextMenuPreparing = function (cellObject) {
        var _this = this;
        var _a;
        if (!this.enableContextMenu) {
            return;
        }
        // Added code to disable ungroupAll, when no column is grouped.
        if (cellObject.target === 'header' || cellObject.target === 'headerPanel') {
            var groupCount = cellObject.component.columnOption('groupIndex:0');
            if (!groupCount) {
                if (cellObject.items) {
                    cellObject.items.forEach(function (item) {
                        if (item.value === 'ungroupAll') {
                            item.disabled = true;
                        }
                    });
                }
            }
            (_a = cellObject.items) === null || _a === void 0 ? void 0 : _a.push({
                disabled: false,
                icon: '',
                onItemClick: function () {
                    _this.gridComponentInstance.beginUpdate();
                    var colCount = _this.gridComponentInstance.columnCount();
                    for (var i = 0; i < colCount; i++) {
                        if (_this.gridComponentInstance.columnOption(i, 'visible')) {
                            _this.gridComponentInstance.columnOption(i, 'width', 'auto');
                        }
                    }
                    _this.gridComponentInstance.endUpdate();
                },
                text: 'Auto Fit',
                value: 'autoFit',
            });
        }
        if (cellObject.row && cellObject.row.rowType === 'group') {
            cellObject.items.push({
                text: 'Expand All',
                onItemClick: function () { cellObject.component.expandAll(void 0); }
            });
            cellObject.items.push({
                text: 'Collapse All',
                onItemClick: function () { cellObject.component.collapseAll(void 0); }
            });
        }
        if (cellObject.row && cellObject.row.rowType === 'data') {
            var isNumberType = cellObject.column.dataType === 'number' ? true : false;
            cellObject.items = [{
                    visible: isNumberType,
                    text: 'Sum',
                    onItemClick: function () {
                        _this.addOrRemoveAggregationSummary(cellObject, 'sum');
                    }
                },
                {
                    visible: isNumberType,
                    text: 'Avg',
                    onItemClick: function () { _this.addOrRemoveAggregationSummary(cellObject, 'avg'); }
                },
                {
                    text: 'Max',
                    onItemClick: function () { _this.addOrRemoveAggregationSummary(cellObject, 'max'); }
                },
                {
                    text: 'Min',
                    onItemClick: function () { _this.addOrRemoveAggregationSummary(cellObject, 'min'); }
                },
                {
                    text: 'Count',
                    onItemClick: function () { _this.addOrRemoveAggregationSummary(cellObject, 'count'); }
                },
                {
                    text: 'Reset',
                    onItemClick: function () { _this.addOrRemoveAggregationSummary(cellObject, 'reset'); }
                }];
        }
    };
    ;
    PBIGridOptionsModel.prototype.onCellPrepared = function (cellInfo) {
        if (cellInfo.column && cellInfo.column.hasOwnProperty('dataField')) {
            // Added try catch so that in case formatting logic fails, user can still use CGF entity.
            try {
                this.cellFormat(cellInfo, true);
            }
            catch (error) {
                console.error(error);
                console.log('Cell formatting broke while exporting.', cellInfo);
            }
        }
    };
    PBIGridOptionsModel.prototype.onRowPrepared = function (args) {
        if (args.rowType === rowTypeInfo.data) {
            this.setCustomColumnData(args);
        }
        this.formatRows(args.rowType, args.data, args.rowElement, null);
    };
    PBIGridOptionsModel.prototype.cellFormat = function (cellInfo, isCellPrepared, excelGridCellInfo) {
        if (excelGridCellInfo === void 0) { excelGridCellInfo = null; }
        var _a, _b, _c;
        if (cellInfo) {
            var dataValue = null;
            switch (cellInfo.rowType) {
                case rowTypeInfo.totalFooter:
                    if (((_a = cellInfo.summaryItems) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                        dataValue = (_b = cellInfo.summaryItems[0]) === null || _b === void 0 ? void 0 : _b.value;
                    }
                    break;
                case rowTypeInfo.group:
                    if (!cellInfo.column.hasOwnProperty('command')
                        || (cellInfo.column.hasOwnProperty('command')
                            && cellInfo.column.command !== 'expand')) {
                        if ((_c = cellInfo.totalItem) === null || _c === void 0 ? void 0 : _c.summaryCells) {
                            var filterItem = cellInfo.totalItem.summaryCells.filter(function (item) { var _a; return cellInfo.column.dataField === ((_a = item[0]) === null || _a === void 0 ? void 0 : _a.column); })[0];
                            if (filterItem === null || filterItem === void 0 ? void 0 : filterItem.length) {
                                dataValue = filterItem[0].value;
                            }
                            else {
                                dataValue = cellInfo.value;
                            }
                        }
                    }
                    break;
                case rowTypeInfo.data:
                    dataValue = cellInfo.value;
                    break;
            }
            if (this.allowedRowTypes(cellInfo.rowType)) {
                if (!this.listGroupedColumns.length && cellInfo.component) {
                    this.listGroupedColumns = this.getGroupedColumnList(cellInfo.component);
                }
                this.prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue);
            }
            // else if (typeof (dataValue) === 'boolean' && cellInfo.rowType === rowTypeInfo.data) {
            //     this.prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue);
            // }
        }
    };
    PBIGridOptionsModel.prototype.allowedRowTypes = function (type) {
        switch (type) {
            case rowTypeInfo.data:
            case rowTypeInfo.group:
            case rowTypeInfo.totalFooter:
                return true;
        }
        return false;
    };
    PBIGridOptionsModel.prototype.prepareFormattingInfo = function (cellInfo, isCellPrepared, excelGridCellInfo, dataValue) {
        var _this = this;
        var rowTypeFilterForFormatting = 'row';
        var columnFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid))) || [];
        var baseFormatCollection = this.isMasterGrid ? JSON.parse(sessionStorage.getItem(CGFStorageKeys[CGFStorageKeys.dictionaryFormatData])) || [] : [];
        var conditionalFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid))) || [];
        var conditionFilterItemsForColumns = conditionalFormatCollection.filter(function (arrItem) { return arrItem.dataField === cellInfo.column.dataField && arrItem.applyType !== rowTypeFilterForFormatting; });
        var conditionalFormattingRequired = false;
        var formatData = columnFormatCollection.find(function (arrItem) { return arrItem.dataField === cellInfo.column.dataField; }) ||
            baseFormatCollection.find(function (arrItem) { return arrItem.dataField === cellInfo.column.dataField; });
        var applyBasicFormatting = true;
        conditionFilterItemsForColumns.forEach(function (filterItem) {
            var _a, _b, _c, _d, _e;
            if (filterItem.condition && cellInfo.rowType === 'data') {
                conditionalFormattingRequired = _this.checkConditionSatisfied(filterItem, cellInfo.data);
            }
            if (conditionalFormattingRequired) {
                var _formattingData = __assign({}, formatData);
                if ((_formattingData === null || _formattingData === void 0 ? void 0 : _formattingData.dataField) === filterItem.dataField) {
                    for (var key in _formattingData) {
                        if (_formattingData.hasOwnProperty(key)) {
                            _formattingData.textColor = filterItem.textColor || _formattingData.textColor;
                            _formattingData.backgroundColor = filterItem.backgroundColor || _formattingData.backgroundColor;
                            // reset then apply
                            _formattingData.cssClass = (_c = (_b = (_a = _formattingData.cssClass) === null || _a === void 0 ? void 0 : _a.replace('bold', '')) === null || _b === void 0 ? void 0 : _b.replace('underline', '')) === null || _c === void 0 ? void 0 : _c.replace('italic', '');
                            _formattingData.cssClass = (_e = (_d = filterItem.cssClass) === null || _d === void 0 ? void 0 : _d.concat(' ')) === null || _e === void 0 ? void 0 : _e.concat(_formattingData.cssClass);
                        }
                    }
                }
                else {
                    _formattingData = __assign({}, filterItem);
                }
                _this.applyFormatting(dataValue, _formattingData, isCellPrepared, cellInfo, excelGridCellInfo);
                applyBasicFormatting = false;
            }
        });
        if (formatData && applyBasicFormatting) {
            this.applyFormatting(dataValue, formatData, isCellPrepared, cellInfo, excelGridCellInfo);
        }
        else if (cellInfo.value !== cellInfo.text) { // !IMPORTANT Check why we need to use this condition. Since it worked in previous versions.
            cellInfo.text = cellInfo.value;
        }
    };
    PBIGridOptionsModel.prototype.checkConditionSatisfied = function (conditionInfo, rowData) {
        var conditionPassed = false;
        var updatedValue = this.startValidation(JSON.parse(JSON.stringify(conditionInfo.condition)), rowData);
        if (JSON.stringify(conditionInfo.condition) !== JSON.stringify(updatedValue)) {
            conditionPassed = this.transformAndRunValidation(updatedValue);
        }
        return conditionPassed;
    };
    PBIGridOptionsModel.prototype.startValidation = function (value, rowData) {
        var _this = this;
        if (value && Array.isArray(value[0])) {
            return value.map(function (item) {
                return Array.isArray(item[0]) ? _this.startValidation(item, rowData) : _this.validate(item, rowData);
            });
        }
        return this.validate(value, rowData);
    };
    PBIGridOptionsModel.prototype.validate = function (item, rowData) {
        if (typeof item === 'string' && this.isReservedKeyWord(item)) {
            return item;
        }
        var _dataType = item[2] ? typeof item[2] : 'string';
        if (item[2] === 0 || item[2] === false) {
            _dataType = typeof item[2];
        }
        switch (_dataType) {
            case 'number':
                item = this.validateNumberType(item, rowData);
                break;
            case 'boolean':
                item = this.validateBooleanType(item, rowData);
                break;
            case 'string':
                if (item[2] && !isNaN(new Date(item[2]).getTime())) { // condition to check whether string is date type
                    if (this.stringSpecificOperators.indexOf(item[1]) > -1) { // condition since new date will return values for strings like '99'
                        item = this.validateStringType(item, rowData);
                    }
                    else {
                        item = this.validateDateType(item, rowData);
                    }
                }
                else {
                    item = this.validateStringType(item, rowData);
                }
                break;
            case 'object':
                if (Array.isArray(item[2])) {
                    if (typeof item[2][0] === 'string') {
                        item = this.validateDateType(item, rowData);
                    }
                    else if (typeof item[2][0] === 'number') {
                        item = this.validateNumberType(item, rowData);
                    }
                }
                break;
        }
        return item;
    };
    PBIGridOptionsModel.prototype.isReservedKeyWord = function (value) {
        switch (value) {
            case 'or':
            case 'and':
                return true;
        }
    };
    PBIGridOptionsModel.prototype.validateNumberType = function (item, rowData) {
        var filterVal1 = item[2][0] || item[2];
        var filterVal2 = item[2][1] || item[2];
        var isPassed = this.validateOperatorAndOperands(item[1], rowData[item[0]], filterVal1, filterVal2);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    };
    PBIGridOptionsModel.prototype.validateBooleanType = function (item, rowData) {
        var isPassed = this.validateOperatorAndOperands(item[1], rowData[item[0]], item[2]);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed.toString(); // this conversion(only for boolean) will be used in parent functions.
        }
        return item;
    };
    PBIGridOptionsModel.prototype.validateStringType = function (item, rowData) {
        var _a, _b;
        if (item[2] !== null) {
            item[2] = (_a = item[2]) === null || _a === void 0 ? void 0 : _a.toLowerCase();
        }
        var dataFieldValueInRow = rowData[item[0]];
        if (typeof rowData[item[0]] === 'string') {
            dataFieldValueInRow = ((_b = rowData[item[0]]) === null || _b === void 0 ? void 0 : _b.toLowerCase()) || null;
        }
        var isPassed = this.validateOperatorAndOperands(item[1], dataFieldValueInRow, item[2] === undefined ? '' : item[2]);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    };
    PBIGridOptionsModel.prototype.validateDateType = function (item, rowData) {
        var filterVal1 = item[2][0].length > 6 ? item[2][0] : item[2]; // conditions to check string is date only and 6 is since we have mm/dd/yyyy format
        var filterVal2 = item[2][1].length > 6 ? item[2][1] : item[2];
        filterVal1 = this.convertDateForComparison(filterVal1).getTime();
        filterVal2 = this.convertDateForComparison(filterVal2).getTime();
        var rowColData = this.convertDateForComparison(rowData[item[0]]).getTime();
        rowColData = isNaN(rowColData) ? '' : rowColData;
        var isPassed = this.validateOperatorAndOperands(item[1], rowColData, filterVal1, filterVal2);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    };
    PBIGridOptionsModel.prototype.convertDateForComparison = function (dateValue) {
        var dateTime = new Date(dateValue);
        var formattedDate = (dateTime.getMonth() + 1) + '-' + dateTime.getDate() + '-' + dateTime.getFullYear();
        return new Date(formattedDate);
    };
    PBIGridOptionsModel.prototype.validateOperatorAndOperands = function (operator, rowColumnData, filterValue, filterValue2) {
        if (typeof (rowColumnData) !== 'undefined') {
            switch (operator) {
                case 'contains':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.indexOf(filterValue)) > -1 ? true : false;
                case 'notcontains':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.indexOf(filterValue)) === -1 ? false : true;
                case 'startswith':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.startsWith(filterValue)) ? true : false;
                case 'endswith':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.endsWith(filterValue)) ? true : false;
                case 'isblank':
                    return rowColumnData === null || rowColumnData === '' ? true : false;
                case 'isnotblank':
                    return rowColumnData !== null && rowColumnData !== '' ? true : false;
                case 'between':
                    // we have assumption that fist value is min and second is max
                    return rowColumnData > filterValue && rowColumnData < filterValue2 ? true : false;
                case '=':
                    return rowColumnData === filterValue ? true : false;
                case '<>':
                    return rowColumnData !== filterValue ? true : false;
                case '<':
                    return rowColumnData < filterValue ? true : false;
                case '>':
                    return rowColumnData > filterValue ? true : false;
                case '>=':
                    return rowColumnData >= filterValue ? true : false;
                case '<=':
                    return rowColumnData <= filterValue ? true : false;
            }
        }
        return void 0;
    };
    PBIGridOptionsModel.prototype.transformAndRunValidation = function (value) {
        var _this = this;
        var evalString = '';
        value.forEach(function (item) {
            if (Array.isArray(item) && !Array.isArray(item[2])) {
                evalString += item[2];
            }
            else if (typeof item === 'string' || typeof item === 'boolean') {
                switch (item) {
                    case 'and':
                        evalString += ' && ';
                        break;
                    case 'or':
                        evalString += ' || ';
                        break;
                    case true:
                    case false:
                        evalString += " " + item + " ";
                        break;
                    case 'true':
                    case 'false':
                        evalString += " " + JSON.parse(item) + " ";
                        break;
                }
            }
            else if (Array.isArray(item)) {
                evalString = _this.transformAndRunValidation(item);
            }
        });
        try {
            // tslint:disable-next-line: no-eval
            return eval(evalString);
        }
        catch (e) {
            console.error(e);
            return false;
        }
    };
    PBIGridOptionsModel.prototype.getGroupedColumnList = function (gridComponent) {
        var listGroupedColumns = [];
        var count = gridComponent.option('columns').length;
        for (var i = 0; i < count; i++) {
            var groupedColum = gridComponent.columnOption('groupIndex:' + i.toString());
            if (groupedColum) {
                listGroupedColumns.push(groupedColum.dataField.toLowerCase());
            }
        }
        return listGroupedColumns;
    };
    PBIGridOptionsModel.prototype.applyFormatting = function (dataValue, formatData, isCellPrepared, cellInfo, excelGridCellInfo) {
        var _a, _b;
        for (var _key in formatData) {
            if (formatData.hasOwnProperty(_key)) {
                var keyValue = formatData[_key] || '';
                switch (_key) {
                    case 'cssClass':
                        if (isCellPrepared) {
                            var classArray = ("" + keyValue).split(' ');
                            classArray.forEach(function (item) { if (item.trim()) {
                                cellInfo.cellElement.classList.add(item.trim());
                            } });
                        }
                        else if ((keyValue === null || keyValue === void 0 ? void 0 : keyValue.length) > 0) {
                            this.excelCellFormat("" + keyValue, excelGridCellInfo, dataValue);
                        }
                        break;
                    case 'format':
                        var assigneeObject = '';
                        if (isCellPrepared && formatData.dataType !== 'boolean') {
                            assigneeObject = dataValue; // first reset than apply format.
                        }
                        if (typeof keyValue !== 'string') {
                            switch (keyValue.type) {
                                case 'currency':
                                    assigneeObject = this.prepareUSDFormat(keyValue.precision, dataValue);
                                    break;
                                case 'percent':
                                    assigneeObject = this.preparePercentFormat(keyValue.precision, dataValue);
                                    break;
                                case 'comma':
                                    assigneeObject = this.prepareCommaFormat(keyValue.precision, dataValue);
                                    break;
                                case 'fixedPoint':
                                    assigneeObject = dataValue.toFixed(keyValue.precision);
                                    break;
                            }
                        }
                        else if (formatData[_key] === 'shortDate') {
                            var date = new Date(dataValue);
                            assigneeObject = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                        }
                        if (isCellPrepared && assigneeObject) { // added "AND" condition to prevent overriding value for boolean column
                            cellInfo.cellElement.innerText = assigneeObject;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.value = assigneeObject || dataValue;
                        }
                        break;
                    case 'alignment':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.textAlign = keyValue;
                        }
                        else {
                            excelGridCellInfo.alignment = { horizontal: keyValue };
                        }
                        if (cellInfo.rowType === 'totalFooter' && ((_b = (_a = cellInfo.cellElement) === null || _a === void 0 ? void 0 : _a.childNodes) === null || _b === void 0 ? void 0 : _b.length)) {
                            cellInfo.cellElement.childNodes[0].style.textAlign = keyValue;
                        }
                        break;
                    case 'textColor':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.color = keyValue;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.font = __assign(__assign({}, excelGridCellInfo.font), { color: { argb: keyValue === null || keyValue === void 0 ? void 0 : keyValue.replace('#', 'ff') } });
                        }
                        break;
                    case 'backgroundColor':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.backgroundColor = keyValue;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: keyValue === null || keyValue === void 0 ? void 0 : keyValue.replace('#', 'ff') } };
                        }
                        break;
                    // case 'fixed': // !Important this case has been handled in databrowser.component.ts
                }
            }
        }
    };
    PBIGridOptionsModel.prototype.excelCellFormat = function (classList, excelGridCellInfo, val) {
        var e_1, _a;
        var classListData = classList.split(' ');
        var decimalPlacesData;
        classListData = classListData.filter(function (item) {
            if (!item.toLowerCase().includes('decimaladded')) {
                return item;
            }
            else {
                decimalPlacesData = item;
            }
        });
        if (decimalPlacesData) {
            classListData.unshift(decimalPlacesData);
        }
        try {
            for (var classListData_1 = __values(classListData), classListData_1_1 = classListData_1.next(); !classListData_1_1.done; classListData_1_1 = classListData_1.next()) {
                var formatClass = classListData_1_1.value;
                switch (formatClass.toLowerCase()) {
                    case 'bold':
                        excelGridCellInfo.font = __assign(__assign({}, excelGridCellInfo.font), { bold: true });
                        break;
                    case 'underline':
                        excelGridCellInfo.font = __assign(__assign({}, excelGridCellInfo.font), { underline: true });
                        break;
                    case 'italic':
                        excelGridCellInfo.font = __assign(__assign({}, excelGridCellInfo.font), { italic: true });
                        break;
                }
                if (formatClass.toLowerCase().includes('decimaladded')) {
                    var precisionData = formatClass.split('-');
                    if (precisionData.length > 0) {
                        excelGridCellInfo.value = val === null || val === void 0 ? void 0 : val.toFixed(Number(precisionData[1]));
                    }
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (classListData_1_1 && !classListData_1_1.done && (_a = classListData_1.return)) _a.call(classListData_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    PBIGridOptionsModel.prototype.prepareUSDFormat = function (precision, value) {
        precision = precision === 0 ? 0 : precision || 2; // Default to 2 Decimal Places;
        return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    };
    PBIGridOptionsModel.prototype.preparePercentFormat = function (precision, value) {
        precision = precision === 0 ? 0 : precision || 3; // Default to 3 Decimal Places;
        return new Intl.NumberFormat('en-US', { style: 'percent', minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    };
    PBIGridOptionsModel.prototype.prepareCommaFormat = function (precision, value) {
        precision = precision === 0 ? 0 : precision || 0; // Default to 2 Decimal Places;
        return new Intl.NumberFormat('en-US', { useGrouping: true, minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    };
    PBIGridOptionsModel.prototype.addOrRemoveAggregationSummary = function (argsEvt, typeOfAggregation) {
        this.gridComponentInstance.beginUpdate();
        var totalSummaryItems = argsEvt.component.option('summary.totalItems') || [], groupSummaryItems = argsEvt.component.option('summary.groupItems') || [], indexOfTotalSummary = this.checkForExistingSummaryType(totalSummaryItems, argsEvt.column.dataField), indexOfGroupSummary = this.checkForExistingSummaryType(groupSummaryItems, argsEvt.column.dataField);
        var isNotTypeCountWithFormat = argsEvt.column.format && typeOfAggregation !== 'count';
        var summaryObj = {
            column: argsEvt.column.dataField,
            summaryType: typeOfAggregation,
            alignByColumn: true,
            displayFormat: '{0}',
            valueFormat: isNotTypeCountWithFormat ? argsEvt.column.format : { type: 'fixedPoint', precision: 0 }
            // showInGroupFooter: true
        };
        if (typeOfAggregation === 'reset') {
            if (indexOfTotalSummary !== -1 || indexOfGroupSummary !== -1) {
                totalSummaryItems.splice(indexOfTotalSummary, 1);
                groupSummaryItems.splice(indexOfGroupSummary, 1);
                this.gridComponentInstance.option('summary.totalItems', totalSummaryItems);
                this.gridComponentInstance.option('summary.groupItems', groupSummaryItems);
                // Making sure filters are reapplied
                if (argsEvt.component.state().filters) {
                    argsEvt.component.filter(argsEvt.component.state().filters);
                }
            }
            this.gridComponentInstance.endUpdate();
            return;
        }
        if (indexOfTotalSummary === -1 || indexOfGroupSummary === -1) {
            totalSummaryItems.push(summaryObj);
            groupSummaryItems.push(summaryObj);
        }
        else {
            totalSummaryItems.splice(indexOfTotalSummary, 1);
            groupSummaryItems.splice(indexOfGroupSummary, 1);
            totalSummaryItems.push(summaryObj);
            groupSummaryItems.push(summaryObj);
        }
        argsEvt.component.option('summary.totalItems', totalSummaryItems);
        argsEvt.component.option('summary.groupItems', groupSummaryItems);
        // Making sure filters are reapplied
        if (argsEvt.component.state().filters) {
            argsEvt.component.filter(argsEvt.component.state().filters);
        }
        this.gridComponentInstance.endUpdate();
    };
    PBIGridOptionsModel.prototype.checkForExistingSummaryType = function (summaryItemsCollection, columnName) {
        for (var i = 0; i < summaryItemsCollection.length; i++) {
            if (summaryItemsCollection[i].column === columnName) {
                return i;
            }
        }
        return -1;
    };
    PBIGridOptionsModel.prototype.applyStateProperties = function (viewJson) {
        var _this = this;
        var _a, _b, _c, _d, _e;
        if (!this.gridComponentInstance || !this.gridComponentInstance.NAME) {
            return;
        }
        this.selectedTheme = '';
        this.gridComponentInstance.beginCustomLoading('Refreshing Grid');
        this.gridComponentInstance.beginUpdate();
        if (viewJson) {
            if (viewJson.columns) {
                var viewColumnsLookup = ToDictionary((viewJson === null || viewJson === void 0 ? void 0 : viewJson.columns) || [], function (a) { return a.dataField.toLowerCase(); });
                for (var i = 0; i < this.columns.length; i++) {
                    var key = this.columns[i].dataField.toLowerCase();
                    if (viewColumnsLookup.hasOwnProperty(key)) {
                        this.columns[i].visible = viewColumnsLookup[key].visible;
                    }
                    else {
                        this.columns[i].visible = false;
                    }
                }
            }
            // Below loop is to OVERRIDE CAPTION OF COLUMN
            if (viewJson.visibleColumns && viewJson.visibleColumns.length) {
                for (var index = 0; index < viewJson.visibleColumns.length; index++) {
                    for (var _index = 0; _index < this.columns.length; _index++) {
                        var dataField = viewJson.visibleColumns[index].dataField;
                        if (dataField && dataField !== customActionColumnInfo.dataField &&
                            this.columns[_index].dataField.toLowerCase() === dataField.toLowerCase()) {
                            this.columns[_index].caption = viewJson.visibleColumns[index].caption;
                            // resetting already applied filter class.
                            this.columns[_index].cssClass = (_a = this.columns[_index].cssClass) === null || _a === void 0 ? void 0 : _a.replace(/filterApplied/g, '');
                            break;
                        }
                    }
                }
            }
            if (((_c = (_b = viewJson.summary) === null || _b === void 0 ? void 0 : _b.groupItems) === null || _c === void 0 ? void 0 : _c.length) || ((_e = (_d = viewJson.summary) === null || _d === void 0 ? void 0 : _d.totalItems) === null || _e === void 0 ? void 0 : _e.length)) {
                this.gridComponentInstance.option('summary', viewJson.summary);
            }
            else {
                viewJson.summary.groupItems = [];
                viewJson.summary.totalItems = [];
                this.gridComponentInstance.option('summary', viewJson.summary);
            }
            var formatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid)) || '""');
            if (formatCollection) {
                viewJson.columnFormattingInfo = Object.assign([], true, formatCollection, viewJson.columnFormattingInfo);
            }
            sessionStorage.setItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid), JSON.stringify(viewJson.columnFormattingInfo ? viewJson.columnFormattingInfo : ''));
            sessionStorage.setItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid), JSON.stringify(viewJson.conditionalFormattingInfo ? viewJson.conditionalFormattingInfo : null));
            this.gridFilterValue = viewJson ? viewJson.filterValue : null;
            if (this.gridFilterValue) {
                this.gridComponentInstance.option('filterValue', this.gridFilterValue);
                setTimeout(function () {
                    applyFilterCssClass(_this.gridFilterValue, _this.gridComponentInstance);
                }, 10);
            }
            if (viewJson.hasOwnProperty(CGFStorageKeys[CGFStorageKeys.childGridView])) {
                // this.setChildGridLayoutInfo(layoutJson);
            }
            appToast({ message: 'Selected view settings has been applied.', type: 'success' });
        }
        else {
            sessionStorage.removeItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid));
            sessionStorage.removeItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid));
            this.columns.forEach(function (column) {
                if (column.cssClass && column.cssClass.toLowerCase().trim() === 'filterapplied') {
                    column.cssClass = '';
                }
            });
        }
        this.selectedTheme = viewJson ? viewJson.selectedTheme || 'np.compact' : 'np.compact';
        sessionStorage.setItem('theme', this.selectedTheme);
        this.showColumnLines = viewJson ? viewJson.isGridBorderVisible || false : false;
        this.selectedThemeClass = getClassNameByThemeName(this.selectedTheme);
        this.gridComponentInstance.endUpdate();
        this.gridComponentInstance.state(viewJson);
        this.gridComponentInstance.endCustomLoading();
    };
    PBIGridOptionsModel.prototype.setCustomColumnData = function (args) {
        var _this = this;
        var customCols = args.columns.filter(function (col) { return col.expression && col.groupName === 'Custom Columns'; });
        customCols.forEach(function (col) {
            args.data[col.dataField] = args.values[col.index];
            _this.columns.filter(function (column) { return column.dataField === col.dataField; })[0].dataType = col.dataType;
        });
    };
    PBIGridOptionsModel.prototype.formatRows = function (dataType, rowData, rowElement, excelCell) {
        var _this = this;
        if (dataType === rowTypeInfo.data) {
            var conditionalFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid))) || [];
            var conditionFilterItems = conditionalFormatCollection.filter(function (arrItem) { return arrItem.applyType === 'row'; });
            var conditionalFormattingRequired_1 = false;
            conditionFilterItems.forEach(function (filterItem) {
                var _a;
                if (filterItem.condition) {
                    conditionalFormattingRequired_1 = _this.checkConditionSatisfied(filterItem, rowData);
                    if (conditionalFormattingRequired_1) {
                        if (rowElement) {
                            for (var index = 0; index < rowElement.children.length; index++) {
                                var existingClasses = ((_a = rowElement.children[index].classList) === null || _a === void 0 ? void 0 : _a.toString().replace('bold', '').replace('underline', '').replace('italic', '')) || '';
                                rowElement.children[index].style.backgroundColor = filterItem.backgroundColor || rowElement.children[index].style.backgroundColor;
                                rowElement.children[index].style.color = filterItem.textColor || rowElement.children[index].style.color;
                                rowElement.children[index].classList = existingClasses.concat(' ').concat(filterItem.cssClass);
                            }
                        }
                        else if (excelCell) {
                            if (filterItem.backgroundColor) {
                                excelCell.fill = {
                                    type: 'pattern', pattern: 'solid',
                                    fgColor: { argb: filterItem.backgroundColor.replace('#', 'ff') }
                                };
                            }
                            if (filterItem.textColor) {
                                excelCell.font = {
                                    color: { argb: filterItem.textColor.replace('#', 'ff') }
                                };
                            }
                            if (filterItem.cssClass) {
                                _this.excelCellFormat(filterItem.cssClass, excelCell, null);
                            }
                        }
                    }
                }
            });
        }
    };
    return PBIGridOptionsModel;
}());
export { PBIGridOptionsModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZE9wdGlvbnMubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29tbW9uLWdyaWQtZnJhbWV3b3JrL2NvbXBvbmVudHMvbW9kZWxzL2dyaWRPcHRpb25zLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUF1QkEsT0FBTyxFQUFFLFdBQVcsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2hGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUV2RCxPQUFPLEVBQUUsYUFBYSxFQUFFLHVCQUF1QixFQUFFLG1CQUFtQixFQUFFLFFBQVEsRUFBRSxZQUFZLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUl2STtJQXFISSx1REFBdUQ7SUFDdkQ7UUFySEEsK0hBQStIO1FBRS9ILG9DQUFvQztRQUNwQyxvREFBb0Q7UUFDcEQsNENBQTRDO1FBQzVDOzs7Z0RBR3dDO1FBQ3hDLGNBQVMsR0FBRyxHQUFHLENBQUM7UUFDaEIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsMEJBQXFCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLHdCQUFtQixHQUFHLElBQUksQ0FBQztRQUMzQixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixvQkFBZSxHQUFHLEtBQUssQ0FBQyxDQUFDLG1DQUFtQztRQUM1RCw0QkFBdUIsR0FBRyxLQUFLLENBQUM7UUFDaEMsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIscUJBQWdCLEdBQUcsQ0FBQyxFQUFFLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ2xDLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLDZCQUF3QixHQUFHLElBQUksQ0FBQztRQUNoQyxvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixtQkFBYyxHQUFHLEVBQUUsQ0FBQztRQUNwQix3QkFBbUIsR0FBRyxTQUFTLENBQUM7UUFDaEMsdUJBQWtCLEdBQUcsUUFBUSxDQUFDO1FBQzlCLFlBQU8sR0FBb0IsRUFBRSxDQUFDO1FBQzlCLDJCQUFzQixHQUFRLEVBQUUsQ0FBQztRQUNqQyxlQUFVLEdBQXlGLEVBQUUsQ0FBQztRQUN0Ryw0QkFBdUIsR0FBRyxxQkFBcUIsQ0FBQztRQUNoRCxhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBRXBCLHNCQUFpQixHQUFHLEtBQUssQ0FBQztRQUMxQixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0Qix3QkFBbUIsR0FBRyxLQUFLLENBQUM7UUFDNUIsdUJBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQzFCLHVCQUFrQixHQUFHLElBQUksQ0FBQztRQUMxQix5QkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDNUIsc0JBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLHlCQUFvQixHQUFHLEtBQUssQ0FBQztRQUM3QixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4Qix1QkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDM0Isc0JBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLGdCQUFXLEdBQW1DLElBQUksQ0FBQztRQUNuRCwwQkFBcUIsR0FBZSxJQUFJLENBQUM7UUFDekMsb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFDdkIsYUFBUSxHQUFHLHVCQUF1QixDQUFDO1FBRW5DLHFCQUFnQixHQUFHLEtBQUssQ0FBQztRQUN6QixzQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDekIsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsdUJBQWtCLEdBQWEsRUFBRSxDQUFDO1FBQ2xDLGVBQVUsR0FBRyxTQUFTLENBQUM7UUFDdkIsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLGdCQUFXLEdBQW1DLE1BQU0sQ0FBQztRQUNyRCw0QkFBdUIsR0FBRyxLQUFLLENBQUM7UUFDaEMsdUJBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQzNCLDBCQUFxQixHQUFHLEtBQUssQ0FBQztRQUM5QixlQUFVLEdBQUcsVUFBVSxDQUFDO1FBRXhCLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ25CLHVCQUFrQixHQUFHLEVBQUUsQ0FBQyxDQUFFLDZDQUE2QztRQUN2RSxrQkFBYSxHQUFHLFFBQVEsQ0FBQztRQUN6QixnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQixzQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDekIsb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFDdkIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsb0JBQWUsR0FBRyxJQUFJLENBQUMsQ0FBQyxtREFBbUQ7UUFDM0Usa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFDckIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIscUJBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQ3hCLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLHlCQUFvQixHQUFHLEtBQUssQ0FBQztRQUM3QixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLHFCQUFnQixHQUFpRCxJQUFJLENBQUM7UUFDdEUsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsZ0JBQVcsR0FBRyxVQUFVLENBQUM7UUFDekIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUVULDRCQUF1QixHQUFHLENBQUMsVUFBVSxFQUFFLGFBQWEsRUFBRSxZQUFZLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFFeEYsb0JBQWUsR0FBZSxFQUFFLENBQUM7UUFDakMsMEJBQXFCLEdBQUcsS0FBSyxDQUFDO0lBOEJkLENBQUM7SUFFViw2Q0FBZSxHQUF0QixVQUF1QixRQUFzQjtRQUN6QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRU0sb0RBQXNCLEdBQTdCLFVBQThCLFVBQTZDO1FBQTNFLGlCQTBFQzs7UUF6RUcsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUFFLE9BQU87U0FBRTtRQUN4QywrREFBK0Q7UUFDL0QsSUFBSSxVQUFVLENBQUMsTUFBTSxLQUFLLFFBQVEsSUFBSSxVQUFVLENBQUMsTUFBTSxLQUFLLGFBQWEsRUFBRTtZQUN2RSxJQUFNLFVBQVUsR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNyRSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNiLElBQUksVUFBVSxDQUFDLEtBQUssRUFBRTtvQkFDbEIsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO3dCQUMxQixJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssWUFBWSxFQUFFOzRCQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzt5QkFDeEI7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7aUJBQ047YUFDSjtZQUNELE1BQUEsVUFBVSxDQUFDLEtBQUssMENBQUUsSUFBSSxDQUFDO2dCQUNuQixRQUFRLEVBQUUsS0FBSztnQkFDZixJQUFJLEVBQUUsRUFBRTtnQkFDUixXQUFXLEVBQUU7b0JBQ1QsS0FBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN6QyxJQUFNLFFBQVEsR0FBRyxLQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQzFELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQy9CLElBQUksS0FBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLEVBQUU7NEJBQ3ZELEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQzt5QkFDL0Q7cUJBQ0o7b0JBQ0QsS0FBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMzQyxDQUFDO2dCQUNELElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsU0FBUzthQUNuQixFQUFFO1NBQ047UUFFRCxJQUFJLFVBQVUsQ0FBQyxHQUFHLElBQUksVUFBVSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO1lBQ3RELFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNsQixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsV0FBVyxFQUFFLGNBQVEsVUFBVSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDakUsQ0FBQyxDQUFDO1lBQ0gsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7Z0JBQ2xCLElBQUksRUFBRSxjQUFjO2dCQUNwQixXQUFXLEVBQUUsY0FBUSxVQUFVLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNuRSxDQUFDLENBQUM7U0FDTjtRQUVELElBQUksVUFBVSxDQUFDLEdBQUcsSUFBSSxVQUFVLENBQUMsR0FBRyxDQUFDLE9BQU8sS0FBSyxNQUFNLEVBQUU7WUFDckQsSUFBTSxZQUFZLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUM1RSxVQUFVLENBQUMsS0FBSyxHQUFHLENBQUM7b0JBQ2hCLE9BQU8sRUFBRSxZQUFZO29CQUNyQixJQUFJLEVBQUUsS0FBSztvQkFDWCxXQUFXLEVBQUU7d0JBQ1QsS0FBSSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDMUQsQ0FBQztpQkFDSjtnQkFDRDtvQkFDSSxPQUFPLEVBQUUsWUFBWTtvQkFDckIsSUFBSSxFQUFFLEtBQUs7b0JBQ1gsV0FBVyxFQUFFLGNBQVEsS0FBSSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hGO2dCQUNEO29CQUNJLElBQUksRUFBRSxLQUFLO29CQUNYLFdBQVcsRUFBRSxjQUFRLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNoRjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsS0FBSztvQkFDWCxXQUFXLEVBQUUsY0FBUSxLQUFJLENBQUMsNkJBQTZCLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDaEY7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLE9BQU87b0JBQ2IsV0FBVyxFQUFFLGNBQVEsS0FBSSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2xGO2dCQUNEO29CQUNJLElBQUksRUFBRSxPQUFPO29CQUNiLFdBQVcsRUFBRSxjQUFRLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsRixDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7SUFBQSxDQUFDO0lBRUssNENBQWMsR0FBckIsVUFBc0IsUUFBbUM7UUFDckQsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ2hFLHlGQUF5RjtZQUN6RixJQUFJO2dCQUNBLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ25DO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ1osT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3Q0FBd0MsRUFBRSxRQUFRLENBQUMsQ0FBQzthQUNuRTtTQUNKO0lBQ0wsQ0FBQztJQUVNLDJDQUFhLEdBQXBCLFVBQXFCLElBQThCO1FBQy9DLElBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxXQUFXLENBQUMsSUFBSSxFQUFFO1lBQ25DLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsQztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVPLHdDQUFVLEdBQWxCLFVBQW1CLFFBQW1DLEVBQUUsY0FBdUIsRUFBRSxpQkFBZ0Q7UUFBaEQsa0NBQUEsRUFBQSx3QkFBZ0Q7O1FBQzdILElBQUksUUFBUSxFQUFFO1lBQ1YsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLFFBQVEsUUFBUSxDQUFDLE9BQU8sRUFBRTtnQkFDdEIsS0FBSyxXQUFXLENBQUMsV0FBVztvQkFDeEIsSUFBSSxPQUFBLFFBQVEsQ0FBQyxZQUFZLDBDQUFFLE1BQU0sSUFBRyxDQUFDLEVBQUU7d0JBQ25DLFNBQVMsU0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQywwQ0FBRSxLQUFLLENBQUM7cUJBQy9DO29CQUNELE1BQU07Z0JBQ1YsS0FBSyxXQUFXLENBQUMsS0FBSztvQkFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQzsyQkFDdkMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUM7K0JBQ3JDLFFBQVEsQ0FBQyxNQUFjLENBQUMsT0FBTyxLQUFLLFFBQVEsQ0FBQyxFQUFFO3dCQUN2RCxVQUFJLFFBQVEsQ0FBQyxTQUFTLDBDQUFFLFlBQVksRUFBRTs0QkFDbEMsSUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxZQUMxRCxPQUFBLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxZQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsMENBQUUsTUFBTSxDQUFBLENBQUEsRUFBQSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3RELElBQUksVUFBVSxhQUFWLFVBQVUsdUJBQVYsVUFBVSxDQUFFLE1BQU0sRUFBRTtnQ0FDcEIsU0FBUyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7NkJBQ25DO2lDQUFNO2dDQUNILFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDOzZCQUM5Qjt5QkFDSjtxQkFDSjtvQkFDRCxNQUFNO2dCQUNWLEtBQUssV0FBVyxDQUFDLElBQUk7b0JBQ2pCLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO29CQUMzQixNQUFNO2FBQ2I7WUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsU0FBUyxFQUFFO29CQUN2RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDM0U7Z0JBQ0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFFBQVEsRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLENBQUM7YUFDdEY7WUFDRCx3RkFBd0Y7WUFDeEYsMEZBQTBGO1lBQzFGLElBQUk7U0FDUDtJQUNMLENBQUM7SUFFTyw2Q0FBZSxHQUF2QixVQUF3QixJQUFZO1FBQ2hDLFFBQVEsSUFBSSxFQUFFO1lBQ1YsS0FBSyxXQUFXLENBQUMsSUFBSSxDQUFDO1lBQ3RCLEtBQUssV0FBVyxDQUFDLEtBQUssQ0FBQztZQUN2QixLQUFLLFdBQVcsQ0FBQyxXQUFXO2dCQUN4QixPQUFPLElBQUksQ0FBQztTQUNuQjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFTyxtREFBcUIsR0FBN0IsVUFBOEIsUUFBbUMsRUFBRSxjQUF1QixFQUFFLGlCQUF5QyxFQUFFLFNBQWM7UUFBckosaUJBeUNDO1FBeENHLElBQU0sMEJBQTBCLEdBQUcsS0FBSyxDQUFDO1FBQ3pDLElBQU0sc0JBQXNCLEdBQ3hCLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEosSUFBTSxvQkFBb0IsR0FBbUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDcEssSUFBTSwyQkFBMkIsR0FDN0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2pLLElBQU0sOEJBQThCLEdBQUcsMkJBQTJCLENBQUMsTUFBTSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLFNBQVMsS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsSUFBSSxPQUFPLENBQUMsU0FBUyxLQUFLLDBCQUEwQixFQUFuRyxDQUFtRyxDQUFDLENBQUM7UUFDMUwsSUFBSSw2QkFBNkIsR0FBRyxLQUFLLENBQUM7UUFDMUMsSUFBTSxVQUFVLEdBQWlCLHNCQUFzQixDQUFDLElBQUksQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxTQUFTLEtBQUssUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQS9DLENBQStDLENBQUM7WUFDcEgsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLFNBQVMsS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBL0MsQ0FBK0MsQ0FBQyxDQUFDO1FBQzFGLElBQUksb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLDhCQUE4QixDQUFDLE9BQU8sQ0FBQyxVQUFBLFVBQVU7O1lBQzdDLElBQUksVUFBVSxDQUFDLFNBQVMsSUFBSSxRQUFRLENBQUMsT0FBTyxLQUFLLE1BQU0sRUFBRTtnQkFDckQsNkJBQTZCLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDM0Y7WUFDRCxJQUFJLDZCQUE2QixFQUFFO2dCQUMvQixJQUFJLGVBQWUsZ0JBQVEsVUFBVSxDQUFFLENBQUM7Z0JBQ3hDLElBQUksQ0FBQSxlQUFlLGFBQWYsZUFBZSx1QkFBZixlQUFlLENBQUUsU0FBUyxNQUFLLFVBQVUsQ0FBQyxTQUFTLEVBQUU7b0JBQ3JELEtBQUssSUFBTSxHQUFHLElBQUksZUFBZSxFQUFFO3dCQUMvQixJQUFJLGVBQWUsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7NEJBQ3JDLGVBQWUsQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDLFNBQVMsSUFBSSxlQUFlLENBQUMsU0FBUyxDQUFDOzRCQUM5RSxlQUFlLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxlQUFlLElBQUksZUFBZSxDQUFDLGVBQWUsQ0FBQzs0QkFDaEcsbUJBQW1COzRCQUNuQixlQUFlLENBQUMsUUFBUSxxQkFBRyxlQUFlLENBQUMsUUFBUSwwQ0FBRSxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsMkNBQUcsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLDJDQUFHLE9BQU8sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUM7NEJBQzFILGVBQWUsQ0FBQyxRQUFRLGVBQUcsVUFBVSxDQUFDLFFBQVEsMENBQUUsTUFBTSxDQUFDLEdBQUcsMkNBQUcsTUFBTSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQzt5QkFDakc7cUJBQ0o7aUJBQ0o7cUJBQU07b0JBQ0gsZUFBZSxnQkFBUSxVQUFVLENBQUUsQ0FBQztpQkFDdkM7Z0JBQ0QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDOUYsb0JBQW9CLEdBQUcsS0FBSyxDQUFDO2FBQ2hDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLFVBQVUsSUFBSSxvQkFBb0IsRUFBRTtZQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1NBQzVGO2FBQU0sSUFBSSxRQUFRLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQyxJQUFJLEVBQUUsRUFBRSw0RkFBNEY7WUFDdkksUUFBUSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO1NBQ2xDO0lBQ0wsQ0FBQztJQUVPLHFEQUF1QixHQUEvQixVQUFnQyxhQUEyQixFQUFFLE9BQVk7UUFDckUsSUFBSSxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3hHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRTtZQUMxRSxlQUFlLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ2xFO1FBQ0QsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQztJQUVPLDZDQUFlLEdBQXZCLFVBQXdCLEtBQUssRUFBRSxPQUFPO1FBQXRDLGlCQU9DO1FBTkcsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNsQyxPQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFvQjtnQkFDbEMsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdkcsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVPLHNDQUFRLEdBQWhCLFVBQWlCLElBQUksRUFBRSxPQUFPO1FBQzFCLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUMxRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ3BELElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxFQUFFO1lBQ3BDLFNBQVMsR0FBRyxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUM5QjtRQUNELFFBQVEsU0FBUyxFQUFFO1lBQ2YsS0FBSyxRQUFRO2dCQUNULElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUM5QyxNQUFNO1lBQ1YsS0FBSyxTQUFTO2dCQUNWLElBQUksR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUMvQyxNQUFNO1lBQ1YsS0FBSyxRQUFRO2dCQUNULElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsRUFBRSxpREFBaUQ7b0JBQ25HLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFLG9FQUFvRTt3QkFDMUgsSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7cUJBQ2pEO3lCQUFNO3dCQUNILElBQUksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO3FCQUMvQztpQkFDSjtxQkFBTTtvQkFDSCxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztpQkFDakQ7Z0JBQ0QsTUFBTTtZQUNWLEtBQUssUUFBUTtnQkFDVCxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3hCLElBQUksT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxFQUFFO3dCQUNoQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztxQkFDL0M7eUJBQU0sSUFBSSxPQUFPLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLEVBQUU7d0JBQ3ZDLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO3FCQUNqRDtpQkFDSjtnQkFDRCxNQUFNO1NBQ2I7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU8sK0NBQWlCLEdBQXpCLFVBQTBCLEtBQWE7UUFDbkMsUUFBUSxLQUFLLEVBQUU7WUFDWCxLQUFLLElBQUksQ0FBQztZQUNWLEtBQUssS0FBSztnQkFDTixPQUFPLElBQUksQ0FBQztTQUNuQjtJQUNMLENBQUM7SUFFTyxnREFBa0IsR0FBMUIsVUFBMkIsSUFBZ0IsRUFBRSxPQUFZO1FBQ3JELElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFBQyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ25GLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNyRyxJQUFJLE9BQU8sUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUFFLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxRQUFRLENBQUM7U0FBRTtRQUMxRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRU8saURBQW1CLEdBQTNCLFVBQTRCLElBQWdCLEVBQUUsT0FBWTtRQUN0RCxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN0RixJQUFJLE9BQU8sUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUMvQixJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUUsc0VBQXNFO1NBQ3pHO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVPLGdEQUFrQixHQUExQixVQUEyQixJQUFnQixFQUFFLE9BQVk7O1FBQ3JELElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBRTtZQUNsQixJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQywwQ0FBRSxXQUFXLEVBQUUsQ0FBQztTQUNwQztRQUNELElBQUksbUJBQW1CLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNDLElBQUksT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxFQUFFO1lBQ3RDLG1CQUFtQixHQUFHLE9BQUEsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQywwQ0FBRSxXQUFXLE9BQU0sSUFBSSxDQUFDO1NBQ2pFO1FBQ0QsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxtQkFBbUIsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RILElBQUksT0FBTyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQztTQUFFO1FBQzFELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTyw4Q0FBZ0IsR0FBeEIsVUFBeUIsSUFBZ0IsRUFBRSxPQUFZO1FBQ25ELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLG1GQUFtRjtRQUNsSixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUQsVUFBVSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNqRSxVQUFVLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2pFLElBQUksVUFBVSxHQUFRLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUNoRixVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUNqRCxJQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDL0YsSUFBSSxPQUFPLFFBQVEsS0FBSyxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQztTQUN0QjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxzREFBd0IsR0FBaEMsVUFBaUMsU0FBaUI7UUFDOUMsSUFBTSxRQUFRLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDckMsSUFBTSxhQUFhLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxPQUFPLEVBQUUsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzFHLE9BQU8sSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVPLHlEQUEyQixHQUFuQyxVQUFvQyxRQUFnQixFQUFFLGFBQWtCLEVBQUUsV0FBc0MsRUFBRSxZQUF3QztRQUN0SixJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxXQUFXLEVBQUU7WUFDeEMsUUFBUSxRQUFRLEVBQUU7Z0JBQ2QsS0FBSyxVQUFVO29CQUNYLE9BQU8sQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsT0FBTyxDQUFDLFdBQVcsS0FBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ25FLEtBQUssYUFBYTtvQkFDZCxPQUFPLENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE9BQU8sQ0FBQyxXQUFXLE9BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNyRSxLQUFLLFlBQVk7b0JBQ2IsT0FBTyxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxVQUFVLENBQUMsV0FBVyxHQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDakUsS0FBSyxVQUFVO29CQUNYLE9BQU8sQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsUUFBUSxDQUFDLFdBQVcsR0FBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQy9ELEtBQUssU0FBUztvQkFDVixPQUFPLGFBQWEsS0FBSyxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pFLEtBQUssWUFBWTtvQkFDYixPQUFPLGFBQWEsS0FBSyxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pFLEtBQUssU0FBUztvQkFDViw4REFBOEQ7b0JBQzlELE9BQU8sYUFBYSxHQUFHLFdBQVcsSUFBSSxhQUFhLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDdEYsS0FBSyxHQUFHO29CQUNKLE9BQU8sYUFBYSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3hELEtBQUssSUFBSTtvQkFDTCxPQUFPLGFBQWEsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUN4RCxLQUFLLEdBQUc7b0JBQ0osT0FBTyxhQUFhLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDdEQsS0FBSyxHQUFHO29CQUNKLE9BQU8sYUFBYSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3RELEtBQUssSUFBSTtvQkFDTCxPQUFPLGFBQWEsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUN2RCxLQUFLLElBQUk7b0JBQ0wsT0FBTyxhQUFhLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUMxRDtTQUNKO1FBQ0QsT0FBTyxLQUFLLENBQUMsQ0FBQztJQUNsQixDQUFDO0lBRU8sdURBQXlCLEdBQWpDLFVBQWtDLEtBQVk7UUFBOUMsaUJBaUNDO1FBaENHLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNwQixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSTtZQUNkLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2hELFVBQVUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDekI7aUJBQU0sSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLElBQUksT0FBTyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUM5RCxRQUFRLElBQUksRUFBRTtvQkFDVixLQUFLLEtBQUs7d0JBQ04sVUFBVSxJQUFJLE1BQU0sQ0FBQzt3QkFDckIsTUFBTTtvQkFDVixLQUFLLElBQUk7d0JBQ0wsVUFBVSxJQUFJLE1BQU0sQ0FBQzt3QkFDckIsTUFBTTtvQkFDVixLQUFLLElBQUksQ0FBQztvQkFDVixLQUFLLEtBQUs7d0JBQ04sVUFBVSxJQUFJLE1BQUksSUFBSSxNQUFHLENBQUM7d0JBQzFCLE1BQU07b0JBQ1YsS0FBSyxNQUFNLENBQUM7b0JBQ1osS0FBSyxPQUFPO3dCQUNSLFVBQVUsSUFBSSxNQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQUcsQ0FBQzt3QkFDdEMsTUFBTTtpQkFDYjthQUNKO2lCQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDNUIsVUFBVSxHQUFHLEtBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNyRDtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSTtZQUNBLG9DQUFvQztZQUNwQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUMzQjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQixPQUFPLEtBQUssQ0FBQztTQUNoQjtJQUNMLENBQUM7SUFFTyxrREFBb0IsR0FBNUIsVUFBNkIsYUFBa0I7UUFDM0MsSUFBTSxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDOUIsSUFBTSxLQUFLLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDckQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QixJQUFNLFlBQVksR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUM5RSxJQUFJLFlBQVksRUFBRTtnQkFDZCxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO2FBQ2pFO1NBQ0o7UUFDRCxPQUFPLGtCQUFrQixDQUFDO0lBQzlCLENBQUM7SUFFTyw2Q0FBZSxHQUF2QixVQUF3QixTQUFpQyxFQUFFLFVBQXdCLEVBQUUsY0FBdUIsRUFBRSxRQUFtQyxFQUFFLGlCQUF5Qzs7UUFDeEwsS0FBSyxJQUFNLElBQUksSUFBSSxVQUFVLEVBQUU7WUFDM0IsSUFBSSxVQUFVLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNqQyxJQUFNLFFBQVEsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUN4QyxRQUFRLElBQUksRUFBRTtvQkFDVixLQUFLLFVBQVU7d0JBQ1gsSUFBSSxjQUFjLEVBQUU7NEJBQ2hCLElBQU0sVUFBVSxHQUFHLENBQUEsS0FBRyxRQUFVLENBQUEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7NEJBQzVDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0NBQUUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDOzZCQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ3pHOzZCQUFNLElBQUksQ0FBQSxRQUFRLGFBQVIsUUFBUSx1QkFBUixRQUFRLENBQUUsTUFBTSxJQUFHLENBQUMsRUFBRTs0QkFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFHLFFBQVUsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUMsQ0FBQzt5QkFDckU7d0JBQ0QsTUFBTTtvQkFDVixLQUFLLFFBQVE7d0JBQ1QsSUFBSSxjQUFjLEdBQTJCLEVBQUUsQ0FBQzt3QkFDaEQsSUFBSSxjQUFjLElBQUksVUFBVSxDQUFDLFFBQVEsS0FBSyxTQUFTLEVBQUU7NEJBQ3JELGNBQWMsR0FBRyxTQUFTLENBQUMsQ0FBQyxpQ0FBaUM7eUJBQ2hFO3dCQUNELElBQUksT0FBTyxRQUFRLEtBQUssUUFBUSxFQUFFOzRCQUM5QixRQUFRLFFBQVEsQ0FBQyxJQUFJLEVBQUU7Z0NBQ25CLEtBQUssVUFBVTtvQ0FDWCxjQUFjLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBbUIsQ0FBQyxDQUFDO29DQUNoRixNQUFNO2dDQUNWLEtBQUssU0FBUztvQ0FDVixjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBbUIsQ0FBQyxDQUFDO29DQUNwRixNQUFNO2dDQUNWLEtBQUssT0FBTztvQ0FDUixjQUFjLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBbUIsQ0FBQyxDQUFDO29DQUNsRixNQUFNO2dDQUNWLEtBQUssWUFBWTtvQ0FDYixjQUFjLEdBQUksU0FBb0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29DQUNuRSxNQUFNOzZCQUNiO3lCQUNKOzZCQUFNLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLFdBQVcsRUFBRTs0QkFDekMsSUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7NEJBQ2pDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7eUJBQzVGO3dCQUNELElBQUksY0FBYyxJQUFJLGNBQWMsRUFBRSxFQUFHLHVFQUF1RTs0QkFDNUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsY0FBd0IsQ0FBQzt5QkFDN0Q7NkJBQU0sSUFBSSxpQkFBaUIsRUFBRTs0QkFDMUIsaUJBQWlCLENBQUMsS0FBSyxHQUFHLGNBQWMsSUFBSSxTQUFTLENBQUM7eUJBQ3pEO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxXQUFXO3dCQUNaLElBQUksY0FBYyxFQUFFOzRCQUNoQixRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO3lCQUNuRDs2QkFBTTs0QkFDSCxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLENBQUM7eUJBQzFEO3dCQUNELElBQUksUUFBUSxDQUFDLE9BQU8sS0FBSyxhQUFhLGlCQUFJLFFBQVEsQ0FBQyxXQUFXLDBDQUFFLFVBQVUsMENBQUUsTUFBTSxDQUFBLEVBQUU7NEJBQy9FLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO3lCQUMxRTt3QkFDRCxNQUFNO29CQUNWLEtBQUssV0FBVzt3QkFDWixJQUFJLGNBQWMsRUFBRTs0QkFDaEIsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQzt5QkFDL0M7NkJBQU0sSUFBSSxpQkFBaUIsRUFBRTs0QkFDMUIsaUJBQWlCLENBQUMsSUFBSSx5QkFDZixpQkFBaUIsQ0FBQyxJQUFJLEdBQUssRUFBRSxLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUNsRixDQUFDO3lCQUNMO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxpQkFBaUI7d0JBQ2xCLElBQUksY0FBYyxFQUFFOzRCQUNoQixRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDO3lCQUN6RDs2QkFBTSxJQUFJLGlCQUFpQixFQUFFOzRCQUMxQixpQkFBaUIsQ0FBQyxJQUFJLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQzt5QkFDbkg7d0JBQ0QsTUFBTTtvQkFDVixxRkFBcUY7aUJBQ3hGO2FBQ0o7U0FDSjtJQUNMLENBQUM7SUFFTyw2Q0FBZSxHQUF2QixVQUF3QixTQUFpQixFQUFFLGlCQUFzQixFQUFFLEdBQVE7O1FBQ3ZFLElBQUksYUFBYSxHQUFhLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkQsSUFBSSxpQkFBeUIsQ0FBQztRQUM5QixhQUFhLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxVQUFDLElBQVk7WUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQzlDLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7aUJBQU07Z0JBQ0gsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2FBQzVCO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLGlCQUFpQixFQUFFO1lBQ25CLGFBQWEsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUM1Qzs7WUFDRCxLQUEwQixJQUFBLGtCQUFBLFNBQUEsYUFBYSxDQUFBLDRDQUFBLHVFQUFFO2dCQUFwQyxJQUFNLFdBQVcsMEJBQUE7Z0JBQ2xCLFFBQVEsV0FBVyxDQUFDLFdBQVcsRUFBRSxFQUFFO29CQUMvQixLQUFLLE1BQU07d0JBQ1AsaUJBQWlCLENBQUMsSUFBSSx5QkFDZixpQkFBaUIsQ0FBQyxJQUFJLEdBQUssRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQy9DLENBQUM7d0JBQ0YsTUFBTTtvQkFDVixLQUFLLFdBQVc7d0JBQ1osaUJBQWlCLENBQUMsSUFBSSx5QkFDZixpQkFBaUIsQ0FBQyxJQUFJLEdBQUssRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFLENBQ3BELENBQUM7d0JBQ0YsTUFBTTtvQkFDVixLQUFLLFFBQVE7d0JBQ1QsaUJBQWlCLENBQUMsSUFBSSx5QkFDZixpQkFBaUIsQ0FBQyxJQUFJLEdBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQ2pELENBQUM7d0JBQ0YsTUFBTTtpQkFDYjtnQkFDRCxJQUFJLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEVBQUU7b0JBQ3BELElBQU0sYUFBYSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzdDLElBQUksYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQzFCLGlCQUFpQixDQUFDLEtBQUssR0FBRyxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNwRTtpQkFDSjthQUNKOzs7Ozs7Ozs7SUFDTCxDQUFDO0lBRU8sOENBQWdCLEdBQXhCLFVBQXlCLFNBQWlCLEVBQUUsS0FBYTtRQUNyRCxTQUFTLEdBQUcsU0FBUyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUMsK0JBQStCO1FBQ2pGLE9BQU8sSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxxQkFBcUIsRUFBRSxTQUFTLEVBQUUscUJBQXFCLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDcEssQ0FBQztJQUVPLGtEQUFvQixHQUE1QixVQUE2QixTQUFpQixFQUFFLEtBQWE7UUFDekQsU0FBUyxHQUFHLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLCtCQUErQjtRQUNqRixPQUFPLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixFQUFFLFNBQVMsRUFBRSxxQkFBcUIsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNsSixDQUFDO0lBRU8sZ0RBQWtCLEdBQTFCLFVBQTJCLFNBQWlCLEVBQUUsS0FBYTtRQUN2RCxTQUFTLEdBQUcsU0FBUyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUMsK0JBQStCO1FBQ2pGLE9BQU8sSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUscUJBQXFCLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25KLENBQUM7SUFFTywyREFBNkIsR0FBckMsVUFBc0MsT0FBTyxFQUFFLGlCQUF5QjtRQUNwRSxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDekMsSUFBTSxpQkFBaUIsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsRUFDMUUsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLEVBQ3hFLG1CQUFtQixHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxpQkFBaUIsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUNuRyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsaUJBQWlCLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN4RyxJQUFNLHdCQUF3QixHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLGlCQUFpQixLQUFLLE9BQU8sQ0FBQztRQUN4RixJQUFNLFVBQVUsR0FBRztZQUNmLE1BQU0sRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDaEMsV0FBVyxFQUFFLGlCQUFpQjtZQUM5QixhQUFhLEVBQUUsSUFBSTtZQUNuQixhQUFhLEVBQUUsS0FBSztZQUNwQixXQUFXLEVBQUUsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLENBQUMsRUFBRTtZQUNwRywwQkFBMEI7U0FDN0IsQ0FBQztRQUNGLElBQUksaUJBQWlCLEtBQUssT0FBTyxFQUFFO1lBQy9CLElBQUksbUJBQW1CLEtBQUssQ0FBQyxDQUFDLElBQUksbUJBQW1CLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBQzFELGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDakQsaUJBQWlCLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQzNFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDM0Usb0NBQW9DO2dCQUNwQyxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsT0FBTyxFQUFFO29CQUNuQyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUMvRDthQUNKO1lBQ0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDO1lBQ3ZDLE9BQU87U0FDVjtRQUNELElBQUksbUJBQW1CLEtBQUssQ0FBQyxDQUFDLElBQUksbUJBQW1CLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDMUQsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ25DLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN0QzthQUFNO1lBQ0gsaUJBQWlCLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ2pELGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNqRCxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDbkMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ3RDO1FBQ0QsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUNsRSxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2xFLG9DQUFvQztRQUNwQyxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsT0FBTyxFQUFFO1lBQ25DLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDL0Q7UUFDRCxJQUFJLENBQUMscUJBQXFCLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVPLHlEQUEyQixHQUFuQyxVQUFvQyxzQkFBc0IsRUFBRSxVQUFVO1FBQ2xFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO2dCQUNqRCxPQUFPLENBQUMsQ0FBQzthQUNaO1NBQ0o7UUFDRCxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2QsQ0FBQztJQUVPLGtEQUFvQixHQUE1QixVQUE2QixRQUEyQjtRQUF4RCxpQkE4RUM7O1FBN0VHLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLElBQUksQ0FBRSxJQUFJLENBQUMscUJBQTZCLENBQUMsSUFBSSxFQUFFO1lBQUUsT0FBTztTQUFFO1FBQ3pGLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN6QyxJQUFJLFFBQVEsRUFBRTtZQUNWLElBQUksUUFBUSxDQUFDLE9BQU8sRUFBRTtnQkFDbEIsSUFBTSxpQkFBaUIsR0FBRyxZQUFZLENBQUMsQ0FBQSxRQUFRLGFBQVIsUUFBUSx1QkFBUixRQUFRLENBQUUsT0FBTyxLQUFJLEVBQUUsRUFBRSxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEVBQXpCLENBQXlCLENBQUMsQ0FBQztnQkFDaEcsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUMxQyxJQUFNLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDcEQsSUFBSSxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7d0JBQ3ZDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztxQkFDNUQ7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO3FCQUNuQztpQkFDSjthQUNKO1lBQ0QsOENBQThDO1lBQzlDLElBQUksUUFBUSxDQUFDLGNBQWMsSUFBSSxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRTtnQkFDM0QsS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNqRSxLQUFLLElBQUksTUFBTSxHQUFHLENBQUMsRUFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsTUFBTSxFQUFFLEVBQUU7d0JBQ3pELElBQU0sU0FBUyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDO3dCQUMzRCxJQUFJLFNBQVMsSUFBSSxTQUFTLEtBQUssc0JBQXNCLENBQUMsU0FBUzs0QkFDM0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxDQUFDLFdBQVcsRUFBRSxFQUFFOzRCQUMxRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQzs0QkFDdEUsMENBQTBDOzRCQUMxQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsU0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsMENBQUUsT0FBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxDQUFDOzRCQUM3RixNQUFNO3lCQUNUO3FCQUNKO2lCQUNKO2FBQ0o7WUFDRCxJQUFJLGFBQUEsUUFBUSxDQUFDLE9BQU8sMENBQUUsVUFBVSwwQ0FBRSxNQUFNLGtCQUFJLFFBQVEsQ0FBQyxPQUFPLDBDQUFFLFVBQVUsMENBQUUsTUFBTSxDQUFBLEVBQUU7Z0JBQzlFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNsRTtpQkFBTTtnQkFDSCxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ2pDLFFBQVEsQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztnQkFDakMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2xFO1lBQ0QsSUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQ3RELGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQztZQUN0SCxJQUFJLGdCQUFnQixFQUFFO2dCQUNsQixRQUFRLENBQUMsb0JBQW9CLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2FBQzVHO1lBQ0QsY0FBYyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUMxSCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hGLGNBQWMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFDM0QsY0FBYyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFDeEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNwRyxJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzlELElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN2RSxVQUFVLENBQUM7b0JBQ1AsbUJBQW1CLENBQUMsS0FBSSxDQUFDLGVBQWUsRUFBRSxLQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDMUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ1Y7WUFDRCxJQUFJLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFO2dCQUN2RSwyQ0FBMkM7YUFDOUM7WUFDRCxRQUFRLENBQUMsRUFBRSxPQUFPLEVBQUUsMENBQTBDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUM7U0FDdEY7YUFBTTtZQUNILGNBQWMsQ0FBQyxVQUFVLENBQ3JCLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUM3RyxjQUFjLENBQUMsVUFBVSxDQUNyQixhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLGNBQWMsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUN4SCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFBLE1BQU07Z0JBQ3ZCLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksRUFBRSxLQUFLLGVBQWUsRUFBRTtvQkFDN0UsTUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7aUJBQ3hCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsYUFBYSxJQUFJLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO1FBQ3RGLGNBQWMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLG1CQUFtQixJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxrQkFBa0IsR0FBRyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGdCQUFnQixFQUFFLENBQUM7SUFDbEQsQ0FBQztJQUVPLGlEQUFtQixHQUEzQixVQUE0QixJQUFTO1FBQXJDLGlCQU1DO1FBTEcsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsVUFBVSxJQUFJLEdBQUcsQ0FBQyxTQUFTLEtBQUssZ0JBQWdCLEVBQXBELENBQW9ELENBQUMsQ0FBQztRQUNwRyxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQUEsR0FBRztZQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsRCxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLE1BQU0sQ0FBQyxTQUFTLEtBQUssR0FBRyxDQUFDLFNBQVMsRUFBbEMsQ0FBa0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ2pHLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLHdDQUFVLEdBQWxCLFVBQW1CLFFBQWdCLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTO1FBQW5FLGlCQXFDQztRQXBDRyxJQUFJLFFBQVEsS0FBSyxXQUFXLENBQUMsSUFBSSxFQUFFO1lBQy9CLElBQU0sMkJBQTJCLEdBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLGNBQWMsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNqSyxJQUFNLG9CQUFvQixHQUFHLDJCQUEyQixDQUFDLE1BQU0sQ0FBQyxVQUFBLE9BQU8sSUFBSSxPQUFBLE9BQU8sQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUEzQixDQUEyQixDQUFDLENBQUM7WUFDeEcsSUFBSSwrQkFBNkIsR0FBRyxLQUFLLENBQUM7WUFDMUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLFVBQUEsVUFBVTs7Z0JBQ25DLElBQUksVUFBVSxDQUFDLFNBQVMsRUFBRTtvQkFDdEIsK0JBQTZCLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFDbEYsSUFBSSwrQkFBNkIsRUFBRTt3QkFDL0IsSUFBSSxVQUFVLEVBQUU7NEJBQ1osS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO2dDQUM3RCxJQUFNLGVBQWUsR0FBRyxPQUFBLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUywwQ0FBRSxRQUFRLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLFFBQVEsRUFBRSxFQUFFLE1BQUssRUFBRSxDQUFDO2dDQUNsSixVQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFDLGVBQWUsSUFBSSxVQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUM7Z0NBQ2xJLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsU0FBUyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztnQ0FDeEcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEdBQUcsZUFBZSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzZCQUNsRzt5QkFDSjs2QkFBTSxJQUFJLFNBQVMsRUFBRTs0QkFDbEIsSUFBSSxVQUFVLENBQUMsZUFBZSxFQUFFO2dDQUM1QixTQUFTLENBQUMsSUFBSSxHQUFHO29DQUNiLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLE9BQU87b0NBQ2pDLE9BQU8sRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7aUNBQ25FLENBQUM7NkJBQ0w7NEJBQ0QsSUFBSSxVQUFVLENBQUMsU0FBUyxFQUFFO2dDQUN0QixTQUFTLENBQUMsSUFBSSxHQUFHO29DQUNiLEtBQUssRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7aUNBQzNELENBQUM7NkJBQ0w7NEJBQ0QsSUFBSSxVQUFVLENBQUMsUUFBUSxFQUFFO2dDQUNyQixLQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDOzZCQUM5RDt5QkFDSjtxQkFDSjtpQkFDSjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDO0lBRUwsMEJBQUM7QUFBRCxDQUFDLEFBcHpCRCxJQW96QkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgUEJJR3JpZENvbmZpZ3VyYXRpb24sXHJcbiAgICBQQklHcmlkRXZlbnRzLFxyXG4gICAgUEJJR3JpZENvbHVtbixcclxuICAgIEdyaWRDZWxsQ2xpY2tBcmd1bWVudHMsXHJcbiAgICBHcmlkQ2VsbFByZXBhcmVkQXJndW1lbnRzLFxyXG4gICAgR3JpZEJhc2VBcmd1bWVudHMsXHJcbiAgICBHcmlkQ29udGV4dE1lbnVQcmVwYXJpbmdBcmd1bWVudHMsXHJcbiAgICBHcmlkRWRpdGluZ1N0YXJ0QXJndW1lbnRzLFxyXG4gICAgR3JpZEVkaXRvclByZXBhcmVkQXJndW1lbnRzLFxyXG4gICAgR3JpZEVkaXRvclByZXBhcmluZ0FyZ3VtZW50cyxcclxuICAgIEdyaWRFeHBvcnRpbmdBcmd1bWVudHMsXHJcbiAgICBHcmlkSW5pdE5ld1Jvd0FyZ3VtZW50cyxcclxuICAgIEdyaWRJbml0aWFsaXplZEFyZ3VtZW50cyxcclxuICAgIEdyaWRSb3dDbGlja0FyZ3VtZW50cyxcclxuICAgIEdyaWRSb3dQcmVwYXJlZEFyZ3VtZW50cyxcclxuICAgIEdyaWRSb3dSZW1vdmVkQXJndW1lbnRzLFxyXG4gICAgR3JpZFJvd1ZhbGlkYXRpbmdBcmd1bWVudHMsXHJcbiAgICBHcmlkU2VsZWN0aW9uQ2hhbmdlZEFyZ3VtZW50cyxcclxuICAgIEdyaWRSb3dVcGRhdGluZ0FyZ3VtZW50c1xyXG59IGZyb20gJy4uL2NvbnRyYWN0cy9ncmlkJztcclxuaW1wb3J0IERldkV4cHJlc3MgZnJvbSAnZGV2ZXh0cmVtZSc7XHJcbmltcG9ydCBkeERhdGFHcmlkLCB7IGR4RGF0YUdyaWRDb2x1bW4gfSBmcm9tICdkZXZleHRyZW1lL3VpL2RhdGFfZ3JpZCc7XHJcbmltcG9ydCB7IHJvd1R5cGVJbmZvLCBjdXN0b21BY3Rpb25Db2x1bW5JbmZvIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IENHRlN0b3JhZ2VLZXlzIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2VudW1zJztcclxuaW1wb3J0IHsgQ29sdW1uRm9ybWF0IH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbHVtbic7XHJcbmltcG9ydCB7IGdldFN0b3JhZ2VLZXksIGdldENsYXNzTmFtZUJ5VGhlbWVOYW1lLCBhcHBseUZpbHRlckNzc0NsYXNzLCBhcHBUb2FzdCwgVG9EaWN0aW9uYXJ5IH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMnO1xyXG5pbXBvcnQgeyBHcmlkU3RhdGVTYXZlSW5mbyB9IGZyb20gJy4uL2NvbnRyYWN0cy9ncmlkJztcclxuaW1wb3J0IHsgR3JpZFZpZXdJbmZvIH0gZnJvbSAnLi4vY29udHJhY3RzL3ZpZXctc2VsZWN0aW9uJztcclxuXHJcbmV4cG9ydCBjbGFzcyBQQklHcmlkT3B0aW9uc01vZGVsIGltcGxlbWVudHMgUEJJR3JpZENvbmZpZ3VyYXRpb24sIFBCSUdyaWRFdmVudHMge1xyXG4gICAgLyogaHR0cHM6Ly9qcy5kZXZleHByZXNzLmNvbS9Eb2N1bWVudGF0aW9uL0FwaVJlZmVyZW5jZS9VSV9XaWRnZXRzL2R4RGF0YUdyaWQvQ29uZmlndXJhdGlvbi8gZm9yIGNvbmZpZ3VyYXRpb24gb2YgdGhlIGR4R3JpZCAqL1xyXG5cclxuICAgIC8vI3JlZ2lvbiBHcmlkIENvbmZpZ3VyYXRpb24gTWVtYmVyc1xyXG4gICAgLy8gYmFzZUNvbHVtbnNMaXN0OiBBcnJheTxJTVNfV2ViLklHcmlkQ29sdW1uPiA9IFtdO1xyXG4gICAgLy8gY29sdW1uczogQXJyYXk8SU1TX1dlYi5JR3JpZENvbHVtbj4gPSBbXTtcclxuICAgIC8qIFNwZWNpZmllcyB0aGUgc2hvcnRjdXQga2V5IHRoYXQgc2V0cyBmb2N1cyBvbiB0aGUgd2lkZ2V0LlxyXG4gICAgICAgIElFLCBDaHJvbWUsIFNhZmFyaSwgT3BlcmEgMTUrOiBbQUxUXSArIGFjY2Vzc2tleVxyXG4gICAgICAgIE9wZXJhIHByaW9yIHZlcnNpb24gMTU6IFtTSElGVF1bRVNDXSArIGFjY2Vzc2tleVxyXG4gICAgICAgIEZpcmVmb3g6IFtBTFRdW1NISUZUXSArIGFjY2Vzc2tleSAqL1xyXG4gICAgYWNjZXNzS2V5ID0gJ2cnO1xyXG4gICAgYWxsb3dBZGRpbmcgPSBmYWxzZTtcclxuICAgIGFsbG93Q29sdW1uUmVvcmRlcmluZyA9IHRydWU7XHJcbiAgICBhbGxvd0NvbHVtblJlc2l6aW5nID0gdHJ1ZTtcclxuICAgIGFsbG93RGF0YUV4cG9ydCA9IGZhbHNlO1xyXG4gICAgYWxsb3dEZWxldGluZyA9IGZhbHNlO1xyXG4gICAgYWxsb3dSZW9yZGVyaW5nID0gZmFsc2U7IC8vIG1ha2Ugc3VyZSByb3dzIGNhbiBiZSByZS1vcmRlcmVkXHJcbiAgICBhbGxvd1NlbGVjdGVkRGF0YUV4cG9ydCA9IGZhbHNlO1xyXG4gICAgYWxsb3dVcGRhdGluZyA9IGZhbHNlO1xyXG4gICAgYWxsb3dlZFBhZ2VTaXplcyA9IFs1MCwgMTUwLCAzMDBdO1xyXG4gICAgYXV0b0V4cGFuZEFsbCA9IGZhbHNlO1xyXG4gICAgYXV0b05hdmlnYXRlVG9Gb2N1c2VkUm93ID0gdHJ1ZTtcclxuICAgIGNvbHVtbkF1dG9XaWR0aCA9IGZhbHNlO1xyXG4gICAgY29sdW1uTWluV2lkdGggPSA1MDtcclxuICAgIGNvbHVtblJlbmRlcmluZ01vZGUgPSAndmlydHVhbCc7XHJcbiAgICBjb2x1bW5SZXNpemluZ01vZGUgPSAnd2lkZ2V0JztcclxuICAgIGNvbHVtbnM6IFBCSUdyaWRDb2x1bW5bXSA9IFtdO1xyXG4gICAgY29udGV4dE1lbnVNYXBwaW5nTGlzdDogYW55ID0gW107XHJcbiAgICBkYXRhU291cmNlOiBzdHJpbmcgfCBBcnJheTxhbnk+IHwgRGV2RXhwcmVzcy5kYXRhLkRhdGFTb3VyY2UgfCBEZXZFeHByZXNzLmRhdGEuRGF0YVNvdXJjZU9wdGlvbnMgPSBbXTtcclxuICAgIGRhdGVTZXJpYWxpemF0aW9uRm9ybWF0ID0gJ3l5eXktTU0tZGR0SEg6bW06c3MnO1xyXG4gICAgZGlzYWJsZWQgPSBmYWxzZTtcclxuICAgIGVkaXRpbmdNb2RlID0gJ3Jvdyc7XHJcbiAgICBlbGVtZW50QXR0cjogYW55O1xyXG4gICAgZW5hYmxlQWN0aXZlU3RhdGUgPSBmYWxzZTtcclxuICAgIGVuYWJsZUNhY2hlID0gdHJ1ZTtcclxuICAgIGVuYWJsZUNlbGxIaW50ID0gdHJ1ZTtcclxuICAgIGVuYWJsZUNvbHVtbkNob29zZXIgPSBmYWxzZTtcclxuICAgIGVuYWJsZUNvbHVtbkZpeGluZyA9IHRydWU7XHJcbiAgICBlbmFibGVDb2x1bW5IaWRpbmcgPSB0cnVlO1xyXG4gICAgZW5hYmxlQ29udGV4dEdycE1lbnUgPSB0cnVlO1xyXG4gICAgZW5hYmxlQ29udGV4dE1lbnUgPSB0cnVlO1xyXG4gICAgZW5hYmxlRXJyb3JSb3cgPSB0cnVlO1xyXG4gICAgZW5hYmxlR3JpZEZvcm1hdHRpbmcgPSBmYWxzZTtcclxuICAgIGVuYWJsZUxvYWRQYW5lbCA9IGZhbHNlO1xyXG4gICAgZW5hYmxlU3RhdGVTdG9yaW5nID0gZmFsc2U7XHJcbiAgICBmaWx0ZXJTeW5jRW5hYmxlZCA9IHRydWU7XHJcbiAgICBmaWx0ZXJWYWx1ZTogc3RyaW5nIHwgQXJyYXk8YW55PiB8IEZ1bmN0aW9uID0gbnVsbDtcclxuICAgIGdyaWRDb21wb25lbnRJbnN0YW5jZTogZHhEYXRhR3JpZCA9IG51bGw7XHJcbiAgICBncmlkRmlsdGVyVmFsdWUgPSBudWxsO1xyXG4gICAgZ3JpZE5hbWUgPSAnUG9ydGZvbGlvQkkgRGF0YSBHcmlkJztcclxuICAgIGhlaWdodDogbnVtYmVyIHwgc3RyaW5nIHwgKCgpID0+IG51bWJlciB8IHN0cmluZyk7XHJcbiAgICBoaWdobGlnaHRDaGFuZ2VzID0gZmFsc2U7XHJcbiAgICBob3ZlclN0YXRlRW5hYmxlZCA9IHRydWU7XHJcbiAgICBpc01hc3RlckdyaWQgPSB0cnVlO1xyXG4gICAgbGlzdEdyb3VwZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFtdO1xyXG4gICAgbm9EYXRhVGV4dCA9ICdObyBEYXRhJztcclxuICAgIHBhZ2VTaXplID0gNTA7XHJcbiAgICBwYWdpbmdFbmFibGVkID0gdHJ1ZTtcclxuICAgIHJlZnJlc2hNb2RlOiAnZnVsbCcgfCAncmVzaGFwZScgfCAncmVwYWludCcgPSAnZnVsbCc7XHJcbiAgICByZW1vdGVPcGVyYXRpb25zRW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgcmVwYWludENoYW5nZXNPbmx5ID0gZmFsc2U7XHJcbiAgICByb3dBbHRlcm5hdGlvbkVuYWJsZWQgPSBmYWxzZTtcclxuICAgIHNjcm9sbE1vZGUgPSAnc3RhbmRhcmQnO1xyXG4gICAgc2VsZWN0ZWRSb3dEYXRhOiBhbnk7XHJcbiAgICBzZWxlY3RlZFRoZW1lID0gJyc7XHJcbiAgICBzZWxlY3RlZFRoZW1lQ2xhc3MgPSAnJzsgIC8vIEVtcHR5IG1lYW5zIHJlZ3VsYXIgdGhlbWUgd2lsbCBiZSBhcHBsaWVkLlxyXG4gICAgc2VsZWN0aW9uTW9kZSA9ICdzaW5nbGUnO1xyXG4gICAgc2hvd0JvcmRlcnMgPSB0cnVlO1xyXG4gICAgc2hvd0NvbHVtbkhlYWRlcnMgPSB0cnVlO1xyXG4gICAgc2hvd0NvbHVtbkxpbmVzID0gdHJ1ZTtcclxuICAgIHNob3dEcmFnSWNvbnMgPSBmYWxzZTtcclxuICAgIHNob3dGaWx0ZXJQYW5lbCA9IHRydWU7IC8vIGZsYWcgdG8gc2hvdy9oaWRlIGZpbHRlciBpbmZvIGF0IGJvdHRvbSBvZiBncmlkLlxyXG4gICAgc2hvd0ZpbHRlclJvdyA9IHRydWU7XHJcbiAgICBzaG93R3JvdXBQYW5lbCA9IHRydWU7XHJcbiAgICBzaG93SGVhZGVyRmlsdGVyID0gdHJ1ZTtcclxuICAgIHNob3dQYWdlSW5mbyA9IHRydWU7XHJcbiAgICBzaG93UGFnZVNpemVTZWxlY3RvciA9IGZhbHNlO1xyXG4gICAgc2hvd1BhZ2VyID0gdHJ1ZTtcclxuICAgIHNob3dSb3dMaW5lcyA9IHRydWU7XHJcbiAgICBzaG93U2Nyb2xsYmFycyA9IHRydWU7XHJcbiAgICBzdGF0ZVN0b3JhZ2VUeXBlOiAnY3VzdG9tJyB8ICdsb2NhbFN0b3JhZ2UnIHwgJ3Nlc3Npb25TdG9yYWdlJyA9IG51bGw7XHJcbiAgICBzaG93U2VhcmNoUGFuZWwgPSBmYWxzZTtcclxuICAgIHNvcnRpbmdUeXBlID0gJ211bHRpcGxlJztcclxuICAgIHVzZUljb25zID0gZmFsc2U7XHJcblxyXG4gICAgcHJpdmF0ZSBzdHJpbmdTcGVjaWZpY09wZXJhdG9ycyA9IFsnY29udGFpbnMnLCAnbm90Y29udGFpbnMnLCAnc3RhcnRzd2l0aCcsICdlbmRzd2l0aCddO1xyXG5cclxuICAgIGNoaWxkRW50aXR5TGlzdDogQXJyYXk8YW55PiA9IFtdO1xyXG4gICAgaXNNYXN0ZXJEZXRhaWxFbmFibGVkID0gZmFsc2U7XHJcblxyXG4gICAgLy8gRXZlbiB0aG91Z2ggaXQgaXMgYSBmdW5jdGlvbiBpdCBpcyBiZWluZyB1c2VkIGEgY29uZmlndXJhdGlvbiBmb3IgdGhlIGdyaWQncyBjb2x1bW5cclxuICAgIGN1c3RvbWl6ZUNvbHVtbnM6IChjb2x1bW5zOiBkeERhdGFHcmlkQ29sdW1uW10pID0+IGFueTtcclxuXHJcbiAgICAvLyNlbmRyZWdpb24gR3JpZCBDb25maWd1cmF0aW9uIE1lbWJlcnNcclxuXHJcbiAgICAvLyNyZWdpb24gR3JpZCBFdmVudHNcclxuICAgIG9uQ2VsbENsaWNrOiAoY2VsbE9iamVjdDogR3JpZENlbGxDbGlja0FyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgLy8gb25DZWxsUHJlcGFyZWQ6IChjZWxsSW5mbzogR3JpZENlbGxQcmVwYXJlZEFyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgb25Db250ZW50UmVhZHk6IChncmlkT2JqZWN0OiBHcmlkQmFzZUFyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgLy8gb25Db250ZXh0TWVudVByZXBhcmluZzogKGNlbGxPYmplY3Q6IEdyaWRDb250ZXh0TWVudVByZXBhcmluZ0FyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgb25FZGl0aW5nU3RhcnQ6IChyb3dJdGVtOiBHcmlkRWRpdGluZ1N0YXJ0QXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvbkVkaXRvclByZXBhcmVkOiAocm93OiBHcmlkRWRpdG9yUHJlcGFyZWRBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uRWRpdG9yUHJlcGFyaW5nOiAocm93T2JqZWN0OiBHcmlkRWRpdG9yUHJlcGFyaW5nQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvbkV4cG9ydGluZzogKGFyZ3M6IEdyaWRFeHBvcnRpbmdBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uSW5pdE5ld1JvdzogKGFyZ3M6IEdyaWRJbml0TmV3Um93QXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvbkluaXRpYWxpemVkOiAoZ3JpZE9iamVjdDogR3JpZEluaXRpYWxpemVkQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvblJlb3JkZXI6IChlOiBhbnkpID0+IHZvaWQ7XHJcbiAgICBvblJvd0NsaWNrOiAocm93SXRlbTogR3JpZFJvd0NsaWNrQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvblJvd1JlbW92ZWQ6IChyb3dJdGVtOiBHcmlkUm93UmVtb3ZlZEFyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgb25Sb3dVcGRhdGluZzogKHJvd0l0ZW06IEdyaWRSb3dVcGRhdGluZ0FyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgb25Sb3dWYWxpZGF0aW5nOiAocm93SXRlbTogR3JpZFJvd1ZhbGlkYXRpbmdBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uU2VsZWN0aW9uQ2hhbmdlZDogKHNlbGVjdGVkSXRlbTogR3JpZFNlbGVjdGlvbkNoYW5nZWRBcmd1bWVudHMpID0+IGFueTtcclxuICAgIC8vI2VuZHJlZ2lvbiBHcmlkIEV2ZW50c1xyXG5cclxuICAgIG9uQ3VzdG9tQnV0dG9uQ2xpY2s6IChhcmdzOiB7IGV2dDogTW91c2VFdmVudCwgcm93RGF0YTogYW55LCBidG5UeXBlOiBzdHJpbmcgfSkgPT4gYW55O1xyXG4gICAgb25Vc2VyRGVmaW5lZEN1c3RvbUJ1dHRvbkNsaWNrOiAoYXJnczogeyBldnQ6IE1vdXNlRXZlbnQsIHJvd0RhdGE6IGFueSwgYWN0aW9uSXRlbTogYW55IH0pID0+IGFueTtcclxuXHJcbiAgICAvLyBUT0RPOiBQYXJhbWV0ZXJpemVkIGNvbnN0cnVjdG9yIGZvciBhc3NpZ25pbmcgdmFsdWVzXHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICAgIHB1YmxpYyBhcHBseVZpZXdUb0dyaWQodmlld0pzb246IEdyaWRWaWV3SW5mbyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuYXBwbHlTdGF0ZVByb3BlcnRpZXModmlld0pzb24/LnN0YXRlLmdyaWRTdGF0ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uQ29udGV4dE1lbnVQcmVwYXJpbmcoY2VsbE9iamVjdDogR3JpZENvbnRleHRNZW51UHJlcGFyaW5nQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmVuYWJsZUNvbnRleHRNZW51KSB7IHJldHVybjsgfVxyXG4gICAgICAgIC8vIEFkZGVkIGNvZGUgdG8gZGlzYWJsZSB1bmdyb3VwQWxsLCB3aGVuIG5vIGNvbHVtbiBpcyBncm91cGVkLlxyXG4gICAgICAgIGlmIChjZWxsT2JqZWN0LnRhcmdldCA9PT0gJ2hlYWRlcicgfHwgY2VsbE9iamVjdC50YXJnZXQgPT09ICdoZWFkZXJQYW5lbCcpIHtcclxuICAgICAgICAgICAgY29uc3QgZ3JvdXBDb3VudCA9IGNlbGxPYmplY3QuY29tcG9uZW50LmNvbHVtbk9wdGlvbignZ3JvdXBJbmRleDowJyk7XHJcbiAgICAgICAgICAgIGlmICghZ3JvdXBDb3VudCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNlbGxPYmplY3QuaXRlbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjZWxsT2JqZWN0Lml0ZW1zLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGl0ZW0udmFsdWUgPT09ICd1bmdyb3VwQWxsJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlbS5kaXNhYmxlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjZWxsT2JqZWN0Lml0ZW1zPy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIGRpc2FibGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGljb246ICcnLFxyXG4gICAgICAgICAgICAgICAgb25JdGVtQ2xpY2s6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5iZWdpblVwZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvbENvdW50ID0gdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuY29sdW1uQ291bnQoKTtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNvbENvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmNvbHVtbk9wdGlvbihpLCAndmlzaWJsZScpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5jb2x1bW5PcHRpb24oaSwgJ3dpZHRoJywgJ2F1dG8nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5lbmRVcGRhdGUoKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB0ZXh0OiAnQXV0byBGaXQnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6ICdhdXRvRml0JyxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY2VsbE9iamVjdC5yb3cgJiYgY2VsbE9iamVjdC5yb3cucm93VHlwZSA9PT0gJ2dyb3VwJykge1xyXG4gICAgICAgICAgICBjZWxsT2JqZWN0Lml0ZW1zLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgdGV4dDogJ0V4cGFuZCBBbGwnLFxyXG4gICAgICAgICAgICAgICAgb25JdGVtQ2xpY2s6ICgpID0+IHsgY2VsbE9iamVjdC5jb21wb25lbnQuZXhwYW5kQWxsKHZvaWQgMCk7IH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNlbGxPYmplY3QuaXRlbXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICB0ZXh0OiAnQ29sbGFwc2UgQWxsJyxcclxuICAgICAgICAgICAgICAgIG9uSXRlbUNsaWNrOiAoKSA9PiB7IGNlbGxPYmplY3QuY29tcG9uZW50LmNvbGxhcHNlQWxsKHZvaWQgMCk7IH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoY2VsbE9iamVjdC5yb3cgJiYgY2VsbE9iamVjdC5yb3cucm93VHlwZSA9PT0gJ2RhdGEnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlzTnVtYmVyVHlwZSA9IGNlbGxPYmplY3QuY29sdW1uLmRhdGFUeXBlID09PSAnbnVtYmVyJyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgY2VsbE9iamVjdC5pdGVtcyA9IFt7XHJcbiAgICAgICAgICAgICAgICB2aXNpYmxlOiBpc051bWJlclR5cGUsXHJcbiAgICAgICAgICAgICAgICB0ZXh0OiAnU3VtJyxcclxuICAgICAgICAgICAgICAgIG9uSXRlbUNsaWNrOiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRPclJlbW92ZUFnZ3JlZ2F0aW9uU3VtbWFyeShjZWxsT2JqZWN0LCAnc3VtJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHZpc2libGU6IGlzTnVtYmVyVHlwZSxcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdBdmcnLFxyXG4gICAgICAgICAgICAgICAgb25JdGVtQ2xpY2s6ICgpID0+IHsgdGhpcy5hZGRPclJlbW92ZUFnZ3JlZ2F0aW9uU3VtbWFyeShjZWxsT2JqZWN0LCAnYXZnJyk7IH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGV4dDogJ01heCcsXHJcbiAgICAgICAgICAgICAgICBvbkl0ZW1DbGljazogKCkgPT4geyB0aGlzLmFkZE9yUmVtb3ZlQWdncmVnYXRpb25TdW1tYXJ5KGNlbGxPYmplY3QsICdtYXgnKTsgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0OiAnTWluJyxcclxuICAgICAgICAgICAgICAgIG9uSXRlbUNsaWNrOiAoKSA9PiB7IHRoaXMuYWRkT3JSZW1vdmVBZ2dyZWdhdGlvblN1bW1hcnkoY2VsbE9iamVjdCwgJ21pbicpOyB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdDb3VudCcsXHJcbiAgICAgICAgICAgICAgICBvbkl0ZW1DbGljazogKCkgPT4geyB0aGlzLmFkZE9yUmVtb3ZlQWdncmVnYXRpb25TdW1tYXJ5KGNlbGxPYmplY3QsICdjb3VudCcpOyB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdSZXNldCcsXHJcbiAgICAgICAgICAgICAgICBvbkl0ZW1DbGljazogKCkgPT4geyB0aGlzLmFkZE9yUmVtb3ZlQWdncmVnYXRpb25TdW1tYXJ5KGNlbGxPYmplY3QsICdyZXNldCcpOyB9XHJcbiAgICAgICAgICAgIH1dO1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcHVibGljIG9uQ2VsbFByZXBhcmVkKGNlbGxJbmZvOiBHcmlkQ2VsbFByZXBhcmVkQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGNlbGxJbmZvLmNvbHVtbiAmJiBjZWxsSW5mby5jb2x1bW4uaGFzT3duUHJvcGVydHkoJ2RhdGFGaWVsZCcpKSB7XHJcbiAgICAgICAgICAgIC8vIEFkZGVkIHRyeSBjYXRjaCBzbyB0aGF0IGluIGNhc2UgZm9ybWF0dGluZyBsb2dpYyBmYWlscywgdXNlciBjYW4gc3RpbGwgdXNlIENHRiBlbnRpdHkuXHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNlbGxGb3JtYXQoY2VsbEluZm8sIHRydWUpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygnQ2VsbCBmb3JtYXR0aW5nIGJyb2tlIHdoaWxlIGV4cG9ydGluZy4nLCBjZWxsSW5mbyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uUm93UHJlcGFyZWQoYXJnczogR3JpZFJvd1ByZXBhcmVkQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGFyZ3Mucm93VHlwZSA9PT0gcm93VHlwZUluZm8uZGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLnNldEN1c3RvbUNvbHVtbkRhdGEoYXJncyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZm9ybWF0Um93cyhhcmdzLnJvd1R5cGUsIGFyZ3MuZGF0YSwgYXJncy5yb3dFbGVtZW50LCBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNlbGxGb3JtYXQoY2VsbEluZm86IEdyaWRDZWxsUHJlcGFyZWRBcmd1bWVudHMsIGlzQ2VsbFByZXBhcmVkOiBib29sZWFuLCBleGNlbEdyaWRDZWxsSW5mbzogR3JpZEV4cG9ydGluZ0FyZ3VtZW50cyA9IG51bGwpIHtcclxuICAgICAgICBpZiAoY2VsbEluZm8pIHtcclxuICAgICAgICAgICAgbGV0IGRhdGFWYWx1ZSA9IG51bGw7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoY2VsbEluZm8ucm93VHlwZSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSByb3dUeXBlSW5mby50b3RhbEZvb3RlcjpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY2VsbEluZm8uc3VtbWFyeUl0ZW1zPy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFWYWx1ZSA9IGNlbGxJbmZvLnN1bW1hcnlJdGVtc1swXT8udmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSByb3dUeXBlSW5mby5ncm91cDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWNlbGxJbmZvLmNvbHVtbi5oYXNPd25Qcm9wZXJ0eSgnY29tbWFuZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHx8IChjZWxsSW5mby5jb2x1bW4uaGFzT3duUHJvcGVydHkoJ2NvbW1hbmQnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJiYgKGNlbGxJbmZvLmNvbHVtbiBhcyBhbnkpLmNvbW1hbmQgIT09ICdleHBhbmQnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2VsbEluZm8udG90YWxJdGVtPy5zdW1tYXJ5Q2VsbHMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpbHRlckl0ZW0gPSBjZWxsSW5mby50b3RhbEl0ZW0uc3VtbWFyeUNlbGxzLmZpbHRlcihpdGVtID0+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2VsbEluZm8uY29sdW1uLmRhdGFGaWVsZCA9PT0gaXRlbVswXT8uY29sdW1uKVswXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWx0ZXJJdGVtPy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhVmFsdWUgPSBmaWx0ZXJJdGVtWzBdLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhVmFsdWUgPSBjZWxsSW5mby52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2Ugcm93VHlwZUluZm8uZGF0YTpcclxuICAgICAgICAgICAgICAgICAgICBkYXRhVmFsdWUgPSBjZWxsSW5mby52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodGhpcy5hbGxvd2VkUm93VHlwZXMoY2VsbEluZm8ucm93VHlwZSkpIHtcclxuICAgICAgICAgICAgICAgIGlmICghdGhpcy5saXN0R3JvdXBlZENvbHVtbnMubGVuZ3RoICYmIGNlbGxJbmZvLmNvbXBvbmVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdEdyb3VwZWRDb2x1bW5zID0gdGhpcy5nZXRHcm91cGVkQ29sdW1uTGlzdChjZWxsSW5mby5jb21wb25lbnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5wcmVwYXJlRm9ybWF0dGluZ0luZm8oY2VsbEluZm8sIGlzQ2VsbFByZXBhcmVkLCBleGNlbEdyaWRDZWxsSW5mbywgZGF0YVZhbHVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyBlbHNlIGlmICh0eXBlb2YgKGRhdGFWYWx1ZSkgPT09ICdib29sZWFuJyAmJiBjZWxsSW5mby5yb3dUeXBlID09PSByb3dUeXBlSW5mby5kYXRhKSB7XHJcbiAgICAgICAgICAgIC8vICAgICB0aGlzLnByZXBhcmVGb3JtYXR0aW5nSW5mbyhjZWxsSW5mbywgaXNDZWxsUHJlcGFyZWQsIGV4Y2VsR3JpZENlbGxJbmZvLCBkYXRhVmFsdWUpO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWxsb3dlZFJvd1R5cGVzKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHN3aXRjaCAodHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlIHJvd1R5cGVJbmZvLmRhdGE6XHJcbiAgICAgICAgICAgIGNhc2Ugcm93VHlwZUluZm8uZ3JvdXA6XHJcbiAgICAgICAgICAgIGNhc2Ugcm93VHlwZUluZm8udG90YWxGb290ZXI6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcHJlcGFyZUZvcm1hdHRpbmdJbmZvKGNlbGxJbmZvOiBHcmlkQ2VsbFByZXBhcmVkQXJndW1lbnRzLCBpc0NlbGxQcmVwYXJlZDogYm9vbGVhbiwgZXhjZWxHcmlkQ2VsbEluZm86IEdyaWRFeHBvcnRpbmdBcmd1bWVudHMsIGRhdGFWYWx1ZTogYW55KTogdm9pZCB7XHJcbiAgICAgICAgY29uc3Qgcm93VHlwZUZpbHRlckZvckZvcm1hdHRpbmcgPSAncm93JztcclxuICAgICAgICBjb25zdCBjb2x1bW5Gb3JtYXRDb2xsZWN0aW9uOiBDb2x1bW5Gb3JtYXRbXSA9XHJcbiAgICAgICAgICAgIEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShnZXRTdG9yYWdlS2V5KHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLCBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5mb3JtYXREYXRhXSwgdGhpcy5pc01hc3RlckdyaWQpKSkgfHwgW107XHJcbiAgICAgICAgY29uc3QgYmFzZUZvcm1hdENvbGxlY3Rpb246IENvbHVtbkZvcm1hdFtdID0gdGhpcy5pc01hc3RlckdyaWQgPyBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuZGljdGlvbmFyeUZvcm1hdERhdGFdKSkgfHwgW10gOiBbXTtcclxuICAgICAgICBjb25zdCBjb25kaXRpb25hbEZvcm1hdENvbGxlY3Rpb246IENvbHVtbkZvcm1hdFtdID1cclxuICAgICAgICAgICAgSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGdldFN0b3JhZ2VLZXkodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UsIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmNvbmRpdGlvbmFsRm9ybWF0dGluZ10sIHRoaXMuaXNNYXN0ZXJHcmlkKSkpIHx8IFtdO1xyXG4gICAgICAgIGNvbnN0IGNvbmRpdGlvbkZpbHRlckl0ZW1zRm9yQ29sdW1ucyA9IGNvbmRpdGlvbmFsRm9ybWF0Q29sbGVjdGlvbi5maWx0ZXIoYXJySXRlbSA9PiBhcnJJdGVtLmRhdGFGaWVsZCA9PT0gY2VsbEluZm8uY29sdW1uLmRhdGFGaWVsZCAmJiBhcnJJdGVtLmFwcGx5VHlwZSAhPT0gcm93VHlwZUZpbHRlckZvckZvcm1hdHRpbmcpO1xyXG4gICAgICAgIGxldCBjb25kaXRpb25hbEZvcm1hdHRpbmdSZXF1aXJlZCA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IGZvcm1hdERhdGE6IENvbHVtbkZvcm1hdCA9IGNvbHVtbkZvcm1hdENvbGxlY3Rpb24uZmluZChhcnJJdGVtID0+IGFyckl0ZW0uZGF0YUZpZWxkID09PSBjZWxsSW5mby5jb2x1bW4uZGF0YUZpZWxkKSB8fFxyXG4gICAgICAgICAgICBiYXNlRm9ybWF0Q29sbGVjdGlvbi5maW5kKGFyckl0ZW0gPT4gYXJySXRlbS5kYXRhRmllbGQgPT09IGNlbGxJbmZvLmNvbHVtbi5kYXRhRmllbGQpO1xyXG4gICAgICAgIGxldCBhcHBseUJhc2ljRm9ybWF0dGluZyA9IHRydWU7XHJcbiAgICAgICAgY29uZGl0aW9uRmlsdGVySXRlbXNGb3JDb2x1bW5zLmZvckVhY2goZmlsdGVySXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChmaWx0ZXJJdGVtLmNvbmRpdGlvbiAmJiBjZWxsSW5mby5yb3dUeXBlID09PSAnZGF0YScpIHtcclxuICAgICAgICAgICAgICAgIGNvbmRpdGlvbmFsRm9ybWF0dGluZ1JlcXVpcmVkID0gdGhpcy5jaGVja0NvbmRpdGlvblNhdGlzZmllZChmaWx0ZXJJdGVtLCBjZWxsSW5mby5kYXRhKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoY29uZGl0aW9uYWxGb3JtYXR0aW5nUmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgICAgIGxldCBfZm9ybWF0dGluZ0RhdGEgPSB7IC4uLmZvcm1hdERhdGEgfTtcclxuICAgICAgICAgICAgICAgIGlmIChfZm9ybWF0dGluZ0RhdGE/LmRhdGFGaWVsZCA9PT0gZmlsdGVySXRlbS5kYXRhRmllbGQpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBfZm9ybWF0dGluZ0RhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKF9mb3JtYXR0aW5nRGF0YS5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfZm9ybWF0dGluZ0RhdGEudGV4dENvbG9yID0gZmlsdGVySXRlbS50ZXh0Q29sb3IgfHwgX2Zvcm1hdHRpbmdEYXRhLnRleHRDb2xvcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9mb3JtYXR0aW5nRGF0YS5iYWNrZ3JvdW5kQ29sb3IgPSBmaWx0ZXJJdGVtLmJhY2tncm91bmRDb2xvciB8fCBfZm9ybWF0dGluZ0RhdGEuYmFja2dyb3VuZENvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gcmVzZXQgdGhlbiBhcHBseVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2Zvcm1hdHRpbmdEYXRhLmNzc0NsYXNzID0gX2Zvcm1hdHRpbmdEYXRhLmNzc0NsYXNzPy5yZXBsYWNlKCdib2xkJywgJycpPy5yZXBsYWNlKCd1bmRlcmxpbmUnLCAnJyk/LnJlcGxhY2UoJ2l0YWxpYycsICcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9mb3JtYXR0aW5nRGF0YS5jc3NDbGFzcyA9IGZpbHRlckl0ZW0uY3NzQ2xhc3M/LmNvbmNhdCgnICcpPy5jb25jYXQoX2Zvcm1hdHRpbmdEYXRhLmNzc0NsYXNzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgX2Zvcm1hdHRpbmdEYXRhID0geyAuLi5maWx0ZXJJdGVtIH07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFwcGx5Rm9ybWF0dGluZyhkYXRhVmFsdWUsIF9mb3JtYXR0aW5nRGF0YSwgaXNDZWxsUHJlcGFyZWQsIGNlbGxJbmZvLCBleGNlbEdyaWRDZWxsSW5mbyk7XHJcbiAgICAgICAgICAgICAgICBhcHBseUJhc2ljRm9ybWF0dGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmIChmb3JtYXREYXRhICYmIGFwcGx5QmFzaWNGb3JtYXR0aW5nKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwbHlGb3JtYXR0aW5nKGRhdGFWYWx1ZSwgZm9ybWF0RGF0YSwgaXNDZWxsUHJlcGFyZWQsIGNlbGxJbmZvLCBleGNlbEdyaWRDZWxsSW5mbyk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChjZWxsSW5mby52YWx1ZSAhPT0gY2VsbEluZm8udGV4dCkgeyAvLyAhSU1QT1JUQU5UIENoZWNrIHdoeSB3ZSBuZWVkIHRvIHVzZSB0aGlzIGNvbmRpdGlvbi4gU2luY2UgaXQgd29ya2VkIGluIHByZXZpb3VzIHZlcnNpb25zLlxyXG4gICAgICAgICAgICBjZWxsSW5mby50ZXh0ID0gY2VsbEluZm8udmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY2hlY2tDb25kaXRpb25TYXRpc2ZpZWQoY29uZGl0aW9uSW5mbzogQ29sdW1uRm9ybWF0LCByb3dEYXRhOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgY29uZGl0aW9uUGFzc2VkID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3QgdXBkYXRlZFZhbHVlID0gdGhpcy5zdGFydFZhbGlkYXRpb24oSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShjb25kaXRpb25JbmZvLmNvbmRpdGlvbikpLCByb3dEYXRhKTtcclxuICAgICAgICBpZiAoSlNPTi5zdHJpbmdpZnkoY29uZGl0aW9uSW5mby5jb25kaXRpb24pICE9PSBKU09OLnN0cmluZ2lmeSh1cGRhdGVkVmFsdWUpKSB7XHJcbiAgICAgICAgICAgIGNvbmRpdGlvblBhc3NlZCA9IHRoaXMudHJhbnNmb3JtQW5kUnVuVmFsaWRhdGlvbih1cGRhdGVkVmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY29uZGl0aW9uUGFzc2VkO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc3RhcnRWYWxpZGF0aW9uKHZhbHVlLCByb3dEYXRhKSB7XHJcbiAgICAgICAgaWYgKHZhbHVlICYmIEFycmF5LmlzQXJyYXkodmFsdWVbMF0pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZS5tYXAoKGl0ZW06IGFueVtdIHwgc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gQXJyYXkuaXNBcnJheShpdGVtWzBdKSA/IHRoaXMuc3RhcnRWYWxpZGF0aW9uKGl0ZW0sIHJvd0RhdGEpIDogdGhpcy52YWxpZGF0ZShpdGVtLCByb3dEYXRhKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbGlkYXRlKHZhbHVlLCByb3dEYXRhKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHZhbGlkYXRlKGl0ZW0sIHJvd0RhdGEpOiBhbnkge1xyXG4gICAgICAgIGlmICh0eXBlb2YgaXRlbSA9PT0gJ3N0cmluZycgJiYgdGhpcy5pc1Jlc2VydmVkS2V5V29yZChpdGVtKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IF9kYXRhVHlwZSA9IGl0ZW1bMl0gPyB0eXBlb2YgaXRlbVsyXSA6ICdzdHJpbmcnO1xyXG4gICAgICAgIGlmIChpdGVtWzJdID09PSAwIHx8IGl0ZW1bMl0gPT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIF9kYXRhVHlwZSA9IHR5cGVvZiBpdGVtWzJdO1xyXG4gICAgICAgIH1cclxuICAgICAgICBzd2l0Y2ggKF9kYXRhVHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlICdudW1iZXInOlxyXG4gICAgICAgICAgICAgICAgaXRlbSA9IHRoaXMudmFsaWRhdGVOdW1iZXJUeXBlKGl0ZW0sIHJvd0RhdGEpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ2Jvb2xlYW4nOlxyXG4gICAgICAgICAgICAgICAgaXRlbSA9IHRoaXMudmFsaWRhdGVCb29sZWFuVHlwZShpdGVtLCByb3dEYXRhKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdzdHJpbmcnOlxyXG4gICAgICAgICAgICAgICAgaWYgKGl0ZW1bMl0gJiYgIWlzTmFOKG5ldyBEYXRlKGl0ZW1bMl0pLmdldFRpbWUoKSkpIHsgLy8gY29uZGl0aW9uIHRvIGNoZWNrIHdoZXRoZXIgc3RyaW5nIGlzIGRhdGUgdHlwZVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0cmluZ1NwZWNpZmljT3BlcmF0b3JzLmluZGV4T2YoaXRlbVsxXSkgPiAtMSkgeyAvLyBjb25kaXRpb24gc2luY2UgbmV3IGRhdGUgd2lsbCByZXR1cm4gdmFsdWVzIGZvciBzdHJpbmdzIGxpa2UgJzk5J1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtID0gdGhpcy52YWxpZGF0ZVN0cmluZ1R5cGUoaXRlbSwgcm93RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbSA9IHRoaXMudmFsaWRhdGVEYXRlVHlwZShpdGVtLCByb3dEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSB0aGlzLnZhbGlkYXRlU3RyaW5nVHlwZShpdGVtLCByb3dEYXRhKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdvYmplY3QnOlxyXG4gICAgICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoaXRlbVsyXSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGl0ZW1bMl1bMF0gPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSB0aGlzLnZhbGlkYXRlRGF0ZVR5cGUoaXRlbSwgcm93RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgaXRlbVsyXVswXSA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbSA9IHRoaXMudmFsaWRhdGVOdW1iZXJUeXBlKGl0ZW0sIHJvd0RhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzUmVzZXJ2ZWRLZXlXb3JkKHZhbHVlOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICBzd2l0Y2ggKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ29yJzpcclxuICAgICAgICAgICAgY2FzZSAnYW5kJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHZhbGlkYXRlTnVtYmVyVHlwZShpdGVtOiBBcnJheTxhbnk+LCByb3dEYXRhOiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGNvbnN0IGZpbHRlclZhbDEgPSBpdGVtWzJdWzBdIHx8IGl0ZW1bMl07IGNvbnN0IGZpbHRlclZhbDIgPSBpdGVtWzJdWzFdIHx8IGl0ZW1bMl07XHJcbiAgICAgICAgY29uc3QgaXNQYXNzZWQgPSB0aGlzLnZhbGlkYXRlT3BlcmF0b3JBbmRPcGVyYW5kcyhpdGVtWzFdLCByb3dEYXRhW2l0ZW1bMF1dLCBmaWx0ZXJWYWwxLCBmaWx0ZXJWYWwyKTtcclxuICAgICAgICBpZiAodHlwZW9mIGlzUGFzc2VkID09PSAnYm9vbGVhbicpIHsgaXRlbVsyXSA9IGlzUGFzc2VkOyB9XHJcbiAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB2YWxpZGF0ZUJvb2xlYW5UeXBlKGl0ZW06IEFycmF5PGFueT4sIHJvd0RhdGE6IGFueSk6IGFueSB7XHJcbiAgICAgICAgY29uc3QgaXNQYXNzZWQgPSB0aGlzLnZhbGlkYXRlT3BlcmF0b3JBbmRPcGVyYW5kcyhpdGVtWzFdLCByb3dEYXRhW2l0ZW1bMF1dLCBpdGVtWzJdKTtcclxuICAgICAgICBpZiAodHlwZW9mIGlzUGFzc2VkID09PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgaXRlbVsyXSA9IGlzUGFzc2VkLnRvU3RyaW5nKCk7ICAvLyB0aGlzIGNvbnZlcnNpb24ob25seSBmb3IgYm9vbGVhbikgd2lsbCBiZSB1c2VkIGluIHBhcmVudCBmdW5jdGlvbnMuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdmFsaWRhdGVTdHJpbmdUeXBlKGl0ZW06IEFycmF5PGFueT4sIHJvd0RhdGE6IGFueSkge1xyXG4gICAgICAgIGlmIChpdGVtWzJdICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIGl0ZW1bMl0gPSBpdGVtWzJdPy50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgZGF0YUZpZWxkVmFsdWVJblJvdyA9IHJvd0RhdGFbaXRlbVswXV07XHJcbiAgICAgICAgaWYgKHR5cGVvZiByb3dEYXRhW2l0ZW1bMF1dID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBkYXRhRmllbGRWYWx1ZUluUm93ID0gcm93RGF0YVtpdGVtWzBdXT8udG9Mb3dlckNhc2UoKSB8fCBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBpc1Bhc3NlZCA9IHRoaXMudmFsaWRhdGVPcGVyYXRvckFuZE9wZXJhbmRzKGl0ZW1bMV0sIGRhdGFGaWVsZFZhbHVlSW5Sb3csIGl0ZW1bMl0gPT09IHVuZGVmaW5lZCA/ICcnIDogaXRlbVsyXSk7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBpc1Bhc3NlZCA9PT0gJ2Jvb2xlYW4nKSB7IGl0ZW1bMl0gPSBpc1Bhc3NlZDsgfVxyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdmFsaWRhdGVEYXRlVHlwZShpdGVtOiBBcnJheTxhbnk+LCByb3dEYXRhOiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGxldCBmaWx0ZXJWYWwxID0gaXRlbVsyXVswXS5sZW5ndGggPiA2ID8gaXRlbVsyXVswXSA6IGl0ZW1bMl07IC8vIGNvbmRpdGlvbnMgdG8gY2hlY2sgc3RyaW5nIGlzIGRhdGUgb25seSBhbmQgNiBpcyBzaW5jZSB3ZSBoYXZlIG1tL2RkL3l5eXkgZm9ybWF0XHJcbiAgICAgICAgbGV0IGZpbHRlclZhbDIgPSBpdGVtWzJdWzFdLmxlbmd0aCA+IDYgPyBpdGVtWzJdWzFdIDogaXRlbVsyXTtcclxuICAgICAgICBmaWx0ZXJWYWwxID0gdGhpcy5jb252ZXJ0RGF0ZUZvckNvbXBhcmlzb24oZmlsdGVyVmFsMSkuZ2V0VGltZSgpO1xyXG4gICAgICAgIGZpbHRlclZhbDIgPSB0aGlzLmNvbnZlcnREYXRlRm9yQ29tcGFyaXNvbihmaWx0ZXJWYWwyKS5nZXRUaW1lKCk7XHJcbiAgICAgICAgbGV0IHJvd0NvbERhdGE6IGFueSA9IHRoaXMuY29udmVydERhdGVGb3JDb21wYXJpc29uKHJvd0RhdGFbaXRlbVswXV0pLmdldFRpbWUoKTtcclxuICAgICAgICByb3dDb2xEYXRhID0gaXNOYU4ocm93Q29sRGF0YSkgPyAnJyA6IHJvd0NvbERhdGE7XHJcbiAgICAgICAgY29uc3QgaXNQYXNzZWQgPSB0aGlzLnZhbGlkYXRlT3BlcmF0b3JBbmRPcGVyYW5kcyhpdGVtWzFdLCByb3dDb2xEYXRhLCBmaWx0ZXJWYWwxLCBmaWx0ZXJWYWwyKTtcclxuICAgICAgICBpZiAodHlwZW9mIGlzUGFzc2VkID09PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgaXRlbVsyXSA9IGlzUGFzc2VkO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNvbnZlcnREYXRlRm9yQ29tcGFyaXNvbihkYXRlVmFsdWU6IHN0cmluZyk6IERhdGUge1xyXG4gICAgICAgIGNvbnN0IGRhdGVUaW1lID0gbmV3IERhdGUoZGF0ZVZhbHVlKTtcclxuICAgICAgICBjb25zdCBmb3JtYXR0ZWREYXRlID0gKGRhdGVUaW1lLmdldE1vbnRoKCkgKyAxKSArICctJyArIGRhdGVUaW1lLmdldERhdGUoKSArICctJyArIGRhdGVUaW1lLmdldEZ1bGxZZWFyKCk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBEYXRlKGZvcm1hdHRlZERhdGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdmFsaWRhdGVPcGVyYXRvckFuZE9wZXJhbmRzKG9wZXJhdG9yOiBzdHJpbmcsIHJvd0NvbHVtbkRhdGE6IGFueSwgZmlsdGVyVmFsdWU6IHN0cmluZyB8IG51bWJlciB8IGJvb2xlYW4sIGZpbHRlclZhbHVlMj86IHN0cmluZyB8IG51bWJlciB8IGJvb2xlYW4pOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodHlwZW9mIChyb3dDb2x1bW5EYXRhKSAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgc3dpdGNoIChvcGVyYXRvcikge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnY29udGFpbnMnOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhPy5pbmRleE9mKGZpbHRlclZhbHVlKSA+IC0xID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnbm90Y29udGFpbnMnOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhPy5pbmRleE9mKGZpbHRlclZhbHVlKSA9PT0gLTEgPyBmYWxzZSA6IHRydWU7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdzdGFydHN3aXRoJzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YT8uc3RhcnRzV2l0aChmaWx0ZXJWYWx1ZSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdlbmRzd2l0aCc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGE/LmVuZHNXaXRoKGZpbHRlclZhbHVlKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ2lzYmxhbmsnOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhID09PSBudWxsIHx8IHJvd0NvbHVtbkRhdGEgPT09ICcnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnaXNub3RibGFuayc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGEgIT09IG51bGwgJiYgcm93Q29sdW1uRGF0YSAhPT0gJycgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdiZXR3ZWVuJzpcclxuICAgICAgICAgICAgICAgICAgICAvLyB3ZSBoYXZlIGFzc3VtcHRpb24gdGhhdCBmaXN0IHZhbHVlIGlzIG1pbiBhbmQgc2Vjb25kIGlzIG1heFxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhID4gZmlsdGVyVmFsdWUgJiYgcm93Q29sdW1uRGF0YSA8IGZpbHRlclZhbHVlMiA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJz0nOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhID09PSBmaWx0ZXJWYWx1ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJzw+JzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YSAhPT0gZmlsdGVyVmFsdWUgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjYXNlICc8JzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YSA8IGZpbHRlclZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnPic6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGEgPiBmaWx0ZXJWYWx1ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJz49JzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YSA+PSBmaWx0ZXJWYWx1ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJzw9JzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YSA8PSBmaWx0ZXJWYWx1ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdm9pZCAwO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdHJhbnNmb3JtQW5kUnVuVmFsaWRhdGlvbih2YWx1ZTogYW55W10pIHtcclxuICAgICAgICBsZXQgZXZhbFN0cmluZyA9ICcnO1xyXG4gICAgICAgIHZhbHVlLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGl0ZW0pICYmICFBcnJheS5pc0FycmF5KGl0ZW1bMl0pKSB7XHJcbiAgICAgICAgICAgICAgICBldmFsU3RyaW5nICs9IGl0ZW1bMl07XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGl0ZW0gPT09ICdzdHJpbmcnIHx8IHR5cGVvZiBpdGVtID09PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2FuZCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGV2YWxTdHJpbmcgKz0gJyAmJiAnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdvcic6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGV2YWxTdHJpbmcgKz0gJyB8fCAnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIHRydWU6XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBmYWxzZTpcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZhbFN0cmluZyArPSBgICR7aXRlbX0gYDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAndHJ1ZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnZmFsc2UnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBldmFsU3RyaW5nICs9IGAgJHtKU09OLnBhcnNlKGl0ZW0pfSBgO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIGlmIChBcnJheS5pc0FycmF5KGl0ZW0pKSB7XHJcbiAgICAgICAgICAgICAgICBldmFsU3RyaW5nID0gdGhpcy50cmFuc2Zvcm1BbmRSdW5WYWxpZGF0aW9uKGl0ZW0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBuby1ldmFsXHJcbiAgICAgICAgICAgIHJldHVybiBldmFsKGV2YWxTdHJpbmcpO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldEdyb3VwZWRDb2x1bW5MaXN0KGdyaWRDb21wb25lbnQ6IGFueSk6IHN0cmluZ1tdIHtcclxuICAgICAgICBjb25zdCBsaXN0R3JvdXBlZENvbHVtbnMgPSBbXTtcclxuICAgICAgICBjb25zdCBjb3VudCA9IGdyaWRDb21wb25lbnQub3B0aW9uKCdjb2x1bW5zJykubGVuZ3RoO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICBjb25zdCBncm91cGVkQ29sdW0gPSBncmlkQ29tcG9uZW50LmNvbHVtbk9wdGlvbignZ3JvdXBJbmRleDonICsgaS50b1N0cmluZygpKTtcclxuICAgICAgICAgICAgaWYgKGdyb3VwZWRDb2x1bSkge1xyXG4gICAgICAgICAgICAgICAgbGlzdEdyb3VwZWRDb2x1bW5zLnB1c2goZ3JvdXBlZENvbHVtLmRhdGFGaWVsZC50b0xvd2VyQ2FzZSgpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbGlzdEdyb3VwZWRDb2x1bW5zO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYXBwbHlGb3JtYXR0aW5nKGRhdGFWYWx1ZTogc3RyaW5nIHwgbnVtYmVyIHwgRGF0ZSwgZm9ybWF0RGF0YTogQ29sdW1uRm9ybWF0LCBpc0NlbGxQcmVwYXJlZDogYm9vbGVhbiwgY2VsbEluZm86IEdyaWRDZWxsUHJlcGFyZWRBcmd1bWVudHMsIGV4Y2VsR3JpZENlbGxJbmZvOiBHcmlkRXhwb3J0aW5nQXJndW1lbnRzKTogdm9pZCB7XHJcbiAgICAgICAgZm9yIChjb25zdCBfa2V5IGluIGZvcm1hdERhdGEpIHtcclxuICAgICAgICAgICAgaWYgKGZvcm1hdERhdGEuaGFzT3duUHJvcGVydHkoX2tleSkpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGtleVZhbHVlID0gZm9ybWF0RGF0YVtfa2V5XSB8fCAnJztcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoX2tleSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Nzc0NsYXNzJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzQ2VsbFByZXBhcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjbGFzc0FycmF5ID0gYCR7a2V5VmFsdWV9YC5zcGxpdCgnICcpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NBcnJheS5mb3JFYWNoKGl0ZW0gPT4geyBpZiAoaXRlbS50cmltKCkpIHsgY2VsbEluZm8uY2VsbEVsZW1lbnQuY2xhc3NMaXN0LmFkZChpdGVtLnRyaW0oKSk7IH0gfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoa2V5VmFsdWU/Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXhjZWxDZWxsRm9ybWF0KGAke2tleVZhbHVlfWAsIGV4Y2VsR3JpZENlbGxJbmZvLCBkYXRhVmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Zvcm1hdCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBhc3NpZ25lZU9iamVjdDogc3RyaW5nIHwgbnVtYmVyIHwgRGF0ZSA9ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNDZWxsUHJlcGFyZWQgJiYgZm9ybWF0RGF0YS5kYXRhVHlwZSAhPT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhc3NpZ25lZU9iamVjdCA9IGRhdGFWYWx1ZTsgLy8gZmlyc3QgcmVzZXQgdGhhbiBhcHBseSBmb3JtYXQuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBrZXlWYWx1ZSAhPT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN3aXRjaCAoa2V5VmFsdWUudHlwZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgJ2N1cnJlbmN5JzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzaWduZWVPYmplY3QgPSB0aGlzLnByZXBhcmVVU0RGb3JtYXQoa2V5VmFsdWUucHJlY2lzaW9uLCBkYXRhVmFsdWUgYXMgbnVtYmVyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAncGVyY2VudCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2lnbmVlT2JqZWN0ID0gdGhpcy5wcmVwYXJlUGVyY2VudEZvcm1hdChrZXlWYWx1ZS5wcmVjaXNpb24sIGRhdGFWYWx1ZSBhcyBudW1iZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdjb21tYSc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2lnbmVlT2JqZWN0ID0gdGhpcy5wcmVwYXJlQ29tbWFGb3JtYXQoa2V5VmFsdWUucHJlY2lzaW9uLCBkYXRhVmFsdWUgYXMgbnVtYmVyKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAnZml4ZWRQb2ludCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2lnbmVlT2JqZWN0ID0gKGRhdGFWYWx1ZSBhcyBudW1iZXIpLnRvRml4ZWQoa2V5VmFsdWUucHJlY2lzaW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZm9ybWF0RGF0YVtfa2V5XSA9PT0gJ3Nob3J0RGF0ZScpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZShkYXRhVmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzaWduZWVPYmplY3QgPSAoZGF0ZS5nZXRNb250aCgpICsgMSkgKyAnLycgKyBkYXRlLmdldERhdGUoKSArICcvJyArIGRhdGUuZ2V0RnVsbFllYXIoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNDZWxsUHJlcGFyZWQgJiYgYXNzaWduZWVPYmplY3QpIHsgIC8vIGFkZGVkIFwiQU5EXCIgY29uZGl0aW9uIHRvIHByZXZlbnQgb3ZlcnJpZGluZyB2YWx1ZSBmb3IgYm9vbGVhbiBjb2x1bW5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNlbGxJbmZvLmNlbGxFbGVtZW50LmlubmVyVGV4dCA9IGFzc2lnbmVlT2JqZWN0IGFzIHN0cmluZztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChleGNlbEdyaWRDZWxsSW5mbykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhjZWxHcmlkQ2VsbEluZm8udmFsdWUgPSBhc3NpZ25lZU9iamVjdCB8fCBkYXRhVmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnYWxpZ25tZW50JzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzQ2VsbFByZXBhcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZWxsSW5mby5jZWxsRWxlbWVudC5zdHlsZS50ZXh0QWxpZ24gPSBrZXlWYWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4Y2VsR3JpZENlbGxJbmZvLmFsaWdubWVudCA9IHsgaG9yaXpvbnRhbDoga2V5VmFsdWUgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoY2VsbEluZm8ucm93VHlwZSA9PT0gJ3RvdGFsRm9vdGVyJyAmJiBjZWxsSW5mby5jZWxsRWxlbWVudD8uY2hpbGROb2Rlcz8ubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2VsbEluZm8uY2VsbEVsZW1lbnQuY2hpbGROb2Rlc1swXSBhcyBhbnkpLnN0eWxlLnRleHRBbGlnbiA9IGtleVZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3RleHRDb2xvcic6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc0NlbGxQcmVwYXJlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2VsbEluZm8uY2VsbEVsZW1lbnQuc3R5bGUuY29sb3IgPSBrZXlWYWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChleGNlbEdyaWRDZWxsSW5mbykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhjZWxHcmlkQ2VsbEluZm8uZm9udCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAuLi5leGNlbEdyaWRDZWxsSW5mby5mb250LCAuLi57IGNvbG9yOiB7IGFyZ2I6IGtleVZhbHVlPy5yZXBsYWNlKCcjJywgJ2ZmJykgfSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2JhY2tncm91bmRDb2xvcic6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc0NlbGxQcmVwYXJlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2VsbEluZm8uY2VsbEVsZW1lbnQuc3R5bGUuYmFja2dyb3VuZENvbG9yID0ga2V5VmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXhjZWxHcmlkQ2VsbEluZm8pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4Y2VsR3JpZENlbGxJbmZvLmZpbGwgPSB7IHR5cGU6ICdwYXR0ZXJuJywgcGF0dGVybjogJ3NvbGlkJywgZmdDb2xvcjogeyBhcmdiOiBrZXlWYWx1ZT8ucmVwbGFjZSgnIycsICdmZicpIH0gfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAvLyBjYXNlICdmaXhlZCc6IC8vICFJbXBvcnRhbnQgdGhpcyBjYXNlIGhhcyBiZWVuIGhhbmRsZWQgaW4gZGF0YWJyb3dzZXIuY29tcG9uZW50LnRzXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBleGNlbENlbGxGb3JtYXQoY2xhc3NMaXN0OiBzdHJpbmcsIGV4Y2VsR3JpZENlbGxJbmZvOiBhbnksIHZhbDogYW55KTogdm9pZCB7XHJcbiAgICAgICAgbGV0IGNsYXNzTGlzdERhdGE6IHN0cmluZ1tdID0gY2xhc3NMaXN0LnNwbGl0KCcgJyk7XHJcbiAgICAgICAgbGV0IGRlY2ltYWxQbGFjZXNEYXRhOiBzdHJpbmc7XHJcbiAgICAgICAgY2xhc3NMaXN0RGF0YSA9IGNsYXNzTGlzdERhdGEuZmlsdGVyKChpdGVtOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgaWYgKCFpdGVtLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXMoJ2RlY2ltYWxhZGRlZCcpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGRlY2ltYWxQbGFjZXNEYXRhID0gaXRlbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGlmIChkZWNpbWFsUGxhY2VzRGF0YSkge1xyXG4gICAgICAgICAgICBjbGFzc0xpc3REYXRhLnVuc2hpZnQoZGVjaW1hbFBsYWNlc0RhdGEpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3IgKGNvbnN0IGZvcm1hdENsYXNzIG9mIGNsYXNzTGlzdERhdGEpIHtcclxuICAgICAgICAgICAgc3dpdGNoIChmb3JtYXRDbGFzcy50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdib2xkJzpcclxuICAgICAgICAgICAgICAgICAgICBleGNlbEdyaWRDZWxsSW5mby5mb250ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuLi5leGNlbEdyaWRDZWxsSW5mby5mb250LCAuLi57IGJvbGQ6IHRydWUgfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlICd1bmRlcmxpbmUnOlxyXG4gICAgICAgICAgICAgICAgICAgIGV4Y2VsR3JpZENlbGxJbmZvLmZvbnQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC4uLmV4Y2VsR3JpZENlbGxJbmZvLmZvbnQsIC4uLnsgdW5kZXJsaW5lOiB0cnVlIH1cclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnaXRhbGljJzpcclxuICAgICAgICAgICAgICAgICAgICBleGNlbEdyaWRDZWxsSW5mby5mb250ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAuLi5leGNlbEdyaWRDZWxsSW5mby5mb250LCAuLi57IGl0YWxpYzogdHJ1ZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZm9ybWF0Q2xhc3MudG9Mb3dlckNhc2UoKS5pbmNsdWRlcygnZGVjaW1hbGFkZGVkJykpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHByZWNpc2lvbkRhdGEgPSBmb3JtYXRDbGFzcy5zcGxpdCgnLScpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHByZWNpc2lvbkRhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGV4Y2VsR3JpZENlbGxJbmZvLnZhbHVlID0gdmFsPy50b0ZpeGVkKE51bWJlcihwcmVjaXNpb25EYXRhWzFdKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcmVwYXJlVVNERm9ybWF0KHByZWNpc2lvbjogbnVtYmVyLCB2YWx1ZTogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgICAgICBwcmVjaXNpb24gPSBwcmVjaXNpb24gPT09IDAgPyAwIDogcHJlY2lzaW9uIHx8IDI7IC8vIERlZmF1bHQgdG8gMiBEZWNpbWFsIFBsYWNlcztcclxuICAgICAgICByZXR1cm4gbmV3IEludGwuTnVtYmVyRm9ybWF0KCdlbi1VUycsIHsgc3R5bGU6ICdjdXJyZW5jeScsIGN1cnJlbmN5OiAnVVNEJywgbWluaW11bUZyYWN0aW9uRGlnaXRzOiBwcmVjaXNpb24sIG1heGltdW1GcmFjdGlvbkRpZ2l0czogcHJlY2lzaW9uIH0pLmZvcm1hdCh2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcmVwYXJlUGVyY2VudEZvcm1hdChwcmVjaXNpb246IG51bWJlciwgdmFsdWU6IG51bWJlcik6IHN0cmluZyB7XHJcbiAgICAgICAgcHJlY2lzaW9uID0gcHJlY2lzaW9uID09PSAwID8gMCA6IHByZWNpc2lvbiB8fCAzOyAvLyBEZWZhdWx0IHRvIDMgRGVjaW1hbCBQbGFjZXM7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBJbnRsLk51bWJlckZvcm1hdCgnZW4tVVMnLCB7IHN0eWxlOiAncGVyY2VudCcsIG1pbmltdW1GcmFjdGlvbkRpZ2l0czogcHJlY2lzaW9uLCBtYXhpbXVtRnJhY3Rpb25EaWdpdHM6IHByZWNpc2lvbiB9KS5mb3JtYXQodmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcHJlcGFyZUNvbW1hRm9ybWF0KHByZWNpc2lvbjogbnVtYmVyLCB2YWx1ZTogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgICAgICBwcmVjaXNpb24gPSBwcmVjaXNpb24gPT09IDAgPyAwIDogcHJlY2lzaW9uIHx8IDA7IC8vIERlZmF1bHQgdG8gMiBEZWNpbWFsIFBsYWNlcztcclxuICAgICAgICByZXR1cm4gbmV3IEludGwuTnVtYmVyRm9ybWF0KCdlbi1VUycsIHsgdXNlR3JvdXBpbmc6IHRydWUsIG1pbmltdW1GcmFjdGlvbkRpZ2l0czogcHJlY2lzaW9uLCBtYXhpbXVtRnJhY3Rpb25EaWdpdHM6IHByZWNpc2lvbiB9KS5mb3JtYXQodmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWRkT3JSZW1vdmVBZ2dyZWdhdGlvblN1bW1hcnkoYXJnc0V2dCwgdHlwZU9mQWdncmVnYXRpb246IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmJlZ2luVXBkYXRlKCk7XHJcbiAgICAgICAgY29uc3QgdG90YWxTdW1tYXJ5SXRlbXMgPSBhcmdzRXZ0LmNvbXBvbmVudC5vcHRpb24oJ3N1bW1hcnkudG90YWxJdGVtcycpIHx8IFtdLFxyXG4gICAgICAgICAgICBncm91cFN1bW1hcnlJdGVtcyA9IGFyZ3NFdnQuY29tcG9uZW50Lm9wdGlvbignc3VtbWFyeS5ncm91cEl0ZW1zJykgfHwgW10sXHJcbiAgICAgICAgICAgIGluZGV4T2ZUb3RhbFN1bW1hcnkgPSB0aGlzLmNoZWNrRm9yRXhpc3RpbmdTdW1tYXJ5VHlwZSh0b3RhbFN1bW1hcnlJdGVtcywgYXJnc0V2dC5jb2x1bW4uZGF0YUZpZWxkKSxcclxuICAgICAgICAgICAgaW5kZXhPZkdyb3VwU3VtbWFyeSA9IHRoaXMuY2hlY2tGb3JFeGlzdGluZ1N1bW1hcnlUeXBlKGdyb3VwU3VtbWFyeUl0ZW1zLCBhcmdzRXZ0LmNvbHVtbi5kYXRhRmllbGQpO1xyXG4gICAgICAgIGNvbnN0IGlzTm90VHlwZUNvdW50V2l0aEZvcm1hdCA9IGFyZ3NFdnQuY29sdW1uLmZvcm1hdCAmJiB0eXBlT2ZBZ2dyZWdhdGlvbiAhPT0gJ2NvdW50JztcclxuICAgICAgICBjb25zdCBzdW1tYXJ5T2JqID0ge1xyXG4gICAgICAgICAgICBjb2x1bW46IGFyZ3NFdnQuY29sdW1uLmRhdGFGaWVsZCxcclxuICAgICAgICAgICAgc3VtbWFyeVR5cGU6IHR5cGVPZkFnZ3JlZ2F0aW9uLFxyXG4gICAgICAgICAgICBhbGlnbkJ5Q29sdW1uOiB0cnVlLFxyXG4gICAgICAgICAgICBkaXNwbGF5Rm9ybWF0OiAnezB9JyxcclxuICAgICAgICAgICAgdmFsdWVGb3JtYXQ6IGlzTm90VHlwZUNvdW50V2l0aEZvcm1hdCA/IGFyZ3NFdnQuY29sdW1uLmZvcm1hdCA6IHsgdHlwZTogJ2ZpeGVkUG9pbnQnLCBwcmVjaXNpb246IDAgfVxyXG4gICAgICAgICAgICAvLyBzaG93SW5Hcm91cEZvb3RlcjogdHJ1ZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKHR5cGVPZkFnZ3JlZ2F0aW9uID09PSAncmVzZXQnKSB7XHJcbiAgICAgICAgICAgIGlmIChpbmRleE9mVG90YWxTdW1tYXJ5ICE9PSAtMSB8fCBpbmRleE9mR3JvdXBTdW1tYXJ5ICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgdG90YWxTdW1tYXJ5SXRlbXMuc3BsaWNlKGluZGV4T2ZUb3RhbFN1bW1hcnksIDEpO1xyXG4gICAgICAgICAgICAgICAgZ3JvdXBTdW1tYXJ5SXRlbXMuc3BsaWNlKGluZGV4T2ZHcm91cFN1bW1hcnksIDEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2Uub3B0aW9uKCdzdW1tYXJ5LnRvdGFsSXRlbXMnLCB0b3RhbFN1bW1hcnlJdGVtcyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5vcHRpb24oJ3N1bW1hcnkuZ3JvdXBJdGVtcycsIGdyb3VwU3VtbWFyeUl0ZW1zKTtcclxuICAgICAgICAgICAgICAgIC8vIE1ha2luZyBzdXJlIGZpbHRlcnMgYXJlIHJlYXBwbGllZFxyXG4gICAgICAgICAgICAgICAgaWYgKGFyZ3NFdnQuY29tcG9uZW50LnN0YXRlKCkuZmlsdGVycykge1xyXG4gICAgICAgICAgICAgICAgICAgIGFyZ3NFdnQuY29tcG9uZW50LmZpbHRlcihhcmdzRXZ0LmNvbXBvbmVudC5zdGF0ZSgpLmZpbHRlcnMpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpbmRleE9mVG90YWxTdW1tYXJ5ID09PSAtMSB8fCBpbmRleE9mR3JvdXBTdW1tYXJ5ID09PSAtMSkge1xyXG4gICAgICAgICAgICB0b3RhbFN1bW1hcnlJdGVtcy5wdXNoKHN1bW1hcnlPYmopO1xyXG4gICAgICAgICAgICBncm91cFN1bW1hcnlJdGVtcy5wdXNoKHN1bW1hcnlPYmopO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRvdGFsU3VtbWFyeUl0ZW1zLnNwbGljZShpbmRleE9mVG90YWxTdW1tYXJ5LCAxKTtcclxuICAgICAgICAgICAgZ3JvdXBTdW1tYXJ5SXRlbXMuc3BsaWNlKGluZGV4T2ZHcm91cFN1bW1hcnksIDEpO1xyXG4gICAgICAgICAgICB0b3RhbFN1bW1hcnlJdGVtcy5wdXNoKHN1bW1hcnlPYmopO1xyXG4gICAgICAgICAgICBncm91cFN1bW1hcnlJdGVtcy5wdXNoKHN1bW1hcnlPYmopO1xyXG4gICAgICAgIH1cclxuICAgICAgICBhcmdzRXZ0LmNvbXBvbmVudC5vcHRpb24oJ3N1bW1hcnkudG90YWxJdGVtcycsIHRvdGFsU3VtbWFyeUl0ZW1zKTtcclxuICAgICAgICBhcmdzRXZ0LmNvbXBvbmVudC5vcHRpb24oJ3N1bW1hcnkuZ3JvdXBJdGVtcycsIGdyb3VwU3VtbWFyeUl0ZW1zKTtcclxuICAgICAgICAvLyBNYWtpbmcgc3VyZSBmaWx0ZXJzIGFyZSByZWFwcGxpZWRcclxuICAgICAgICBpZiAoYXJnc0V2dC5jb21wb25lbnQuc3RhdGUoKS5maWx0ZXJzKSB7XHJcbiAgICAgICAgICAgIGFyZ3NFdnQuY29tcG9uZW50LmZpbHRlcihhcmdzRXZ0LmNvbXBvbmVudC5zdGF0ZSgpLmZpbHRlcnMpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5lbmRVcGRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNoZWNrRm9yRXhpc3RpbmdTdW1tYXJ5VHlwZShzdW1tYXJ5SXRlbXNDb2xsZWN0aW9uLCBjb2x1bW5OYW1lKSB7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzdW1tYXJ5SXRlbXNDb2xsZWN0aW9uLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChzdW1tYXJ5SXRlbXNDb2xsZWN0aW9uW2ldLmNvbHVtbiA9PT0gY29sdW1uTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIC0xO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYXBwbHlTdGF0ZVByb3BlcnRpZXModmlld0pzb246IEdyaWRTdGF0ZVNhdmVJbmZvKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZSB8fCAhKHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlIGFzIGFueSkuTkFNRSkgeyByZXR1cm47IH1cclxuICAgICAgICB0aGlzLnNlbGVjdGVkVGhlbWUgPSAnJztcclxuICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5iZWdpbkN1c3RvbUxvYWRpbmcoJ1JlZnJlc2hpbmcgR3JpZCcpO1xyXG4gICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmJlZ2luVXBkYXRlKCk7XHJcbiAgICAgICAgaWYgKHZpZXdKc29uKSB7XHJcbiAgICAgICAgICAgIGlmICh2aWV3SnNvbi5jb2x1bW5zKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB2aWV3Q29sdW1uc0xvb2t1cCA9IFRvRGljdGlvbmFyeSh2aWV3SnNvbj8uY29sdW1ucyB8fCBbXSwgYSA9PiBhLmRhdGFGaWVsZC50b0xvd2VyQ2FzZSgpKTtcclxuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5jb2x1bW5zLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qga2V5ID0gdGhpcy5jb2x1bW5zW2ldLmRhdGFGaWVsZC50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh2aWV3Q29sdW1uc0xvb2t1cC5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uc1tpXS52aXNpYmxlID0gdmlld0NvbHVtbnNMb29rdXBba2V5XS52aXNpYmxlO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uc1tpXS52aXNpYmxlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIEJlbG93IGxvb3AgaXMgdG8gT1ZFUlJJREUgQ0FQVElPTiBPRiBDT0xVTU5cclxuICAgICAgICAgICAgaWYgKHZpZXdKc29uLnZpc2libGVDb2x1bW5zICYmIHZpZXdKc29uLnZpc2libGVDb2x1bW5zLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IHZpZXdKc29uLnZpc2libGVDb2x1bW5zLmxlbmd0aDsgaW5kZXgrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IF9pbmRleCA9IDA7IF9pbmRleCA8IHRoaXMuY29sdW1ucy5sZW5ndGg7IF9pbmRleCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRhdGFGaWVsZCA9IHZpZXdKc29uLnZpc2libGVDb2x1bW5zW2luZGV4XS5kYXRhRmllbGQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhRmllbGQgJiYgZGF0YUZpZWxkICE9PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2x1bW5zW19pbmRleF0uZGF0YUZpZWxkLnRvTG93ZXJDYXNlKCkgPT09IGRhdGFGaWVsZC50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbHVtbnNbX2luZGV4XS5jYXB0aW9uID0gdmlld0pzb24udmlzaWJsZUNvbHVtbnNbaW5kZXhdLmNhcHRpb247XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByZXNldHRpbmcgYWxyZWFkeSBhcHBsaWVkIGZpbHRlciBjbGFzcy5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uc1tfaW5kZXhdLmNzc0NsYXNzID0gdGhpcy5jb2x1bW5zW19pbmRleF0uY3NzQ2xhc3M/LnJlcGxhY2UoL2ZpbHRlckFwcGxpZWQvZywgJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHZpZXdKc29uLnN1bW1hcnk/Lmdyb3VwSXRlbXM/Lmxlbmd0aCB8fCB2aWV3SnNvbi5zdW1tYXJ5Py50b3RhbEl0ZW1zPy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLm9wdGlvbignc3VtbWFyeScsIHZpZXdKc29uLnN1bW1hcnkpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdmlld0pzb24uc3VtbWFyeS5ncm91cEl0ZW1zID0gW107XHJcbiAgICAgICAgICAgICAgICB2aWV3SnNvbi5zdW1tYXJ5LnRvdGFsSXRlbXMgPSBbXTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLm9wdGlvbignc3VtbWFyeScsIHZpZXdKc29uLnN1bW1hcnkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGZvcm1hdENvbGxlY3Rpb24gPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXHJcbiAgICAgICAgICAgICAgICBnZXRTdG9yYWdlS2V5KHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLCBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5mb3JtYXREYXRhXSwgdGhpcy5pc01hc3RlckdyaWQpKSB8fCAnXCJcIicpO1xyXG4gICAgICAgICAgICBpZiAoZm9ybWF0Q29sbGVjdGlvbikge1xyXG4gICAgICAgICAgICAgICAgdmlld0pzb24uY29sdW1uRm9ybWF0dGluZ0luZm8gPSBPYmplY3QuYXNzaWduKFtdLCB0cnVlLCBmb3JtYXRDb2xsZWN0aW9uLCB2aWV3SnNvbi5jb2x1bW5Gb3JtYXR0aW5nSW5mbyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShnZXRTdG9yYWdlS2V5KHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLCBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5mb3JtYXREYXRhXSwgdGhpcy5pc01hc3RlckdyaWQpLFxyXG4gICAgICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkodmlld0pzb24uY29sdW1uRm9ybWF0dGluZ0luZm8gPyB2aWV3SnNvbi5jb2x1bW5Gb3JtYXR0aW5nSW5mbyA6ICcnKSk7XHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oZ2V0U3RvcmFnZUtleSh0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZSxcclxuICAgICAgICAgICAgICAgIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmNvbmRpdGlvbmFsRm9ybWF0dGluZ10sIHRoaXMuaXNNYXN0ZXJHcmlkKSxcclxuICAgICAgICAgICAgICAgIEpTT04uc3RyaW5naWZ5KHZpZXdKc29uLmNvbmRpdGlvbmFsRm9ybWF0dGluZ0luZm8gPyB2aWV3SnNvbi5jb25kaXRpb25hbEZvcm1hdHRpbmdJbmZvIDogbnVsbCkpO1xyXG4gICAgICAgICAgICB0aGlzLmdyaWRGaWx0ZXJWYWx1ZSA9IHZpZXdKc29uID8gdmlld0pzb24uZmlsdGVyVmFsdWUgOiBudWxsO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5ncmlkRmlsdGVyVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLm9wdGlvbignZmlsdGVyVmFsdWUnLCB0aGlzLmdyaWRGaWx0ZXJWYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBhcHBseUZpbHRlckNzc0NsYXNzKHRoaXMuZ3JpZEZpbHRlclZhbHVlLCB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZSk7XHJcbiAgICAgICAgICAgICAgICB9LCAxMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHZpZXdKc29uLmhhc093blByb3BlcnR5KENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmNoaWxkR3JpZFZpZXddKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5zZXRDaGlsZEdyaWRMYXlvdXRJbmZvKGxheW91dEpzb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGFwcFRvYXN0KHsgbWVzc2FnZTogJ1NlbGVjdGVkIHZpZXcgc2V0dGluZ3MgaGFzIGJlZW4gYXBwbGllZC4nLCB0eXBlOiAnc3VjY2VzcycgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2UucmVtb3ZlSXRlbShcclxuICAgICAgICAgICAgICAgIGdldFN0b3JhZ2VLZXkodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UsIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmZvcm1hdERhdGFdLCB0aGlzLmlzTWFzdGVyR3JpZCkpO1xyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKFxyXG4gICAgICAgICAgICAgICAgZ2V0U3RvcmFnZUtleSh0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZSwgQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuY29uZGl0aW9uYWxGb3JtYXR0aW5nXSwgdGhpcy5pc01hc3RlckdyaWQpKTtcclxuICAgICAgICAgICAgdGhpcy5jb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChjb2x1bW4uY3NzQ2xhc3MgJiYgY29sdW1uLmNzc0NsYXNzLnRvTG93ZXJDYXNlKCkudHJpbSgpID09PSAnZmlsdGVyYXBwbGllZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb2x1bW4uY3NzQ2xhc3MgPSAnJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRUaGVtZSA9IHZpZXdKc29uID8gdmlld0pzb24uc2VsZWN0ZWRUaGVtZSB8fCAnbnAuY29tcGFjdCcgOiAnbnAuY29tcGFjdCc7XHJcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgndGhlbWUnLCB0aGlzLnNlbGVjdGVkVGhlbWUpO1xyXG4gICAgICAgIHRoaXMuc2hvd0NvbHVtbkxpbmVzID0gdmlld0pzb24gPyB2aWV3SnNvbi5pc0dyaWRCb3JkZXJWaXNpYmxlIHx8IGZhbHNlIDogZmFsc2U7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFRoZW1lQ2xhc3MgPSBnZXRDbGFzc05hbWVCeVRoZW1lTmFtZSh0aGlzLnNlbGVjdGVkVGhlbWUpO1xyXG4gICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLnN0YXRlKHZpZXdKc29uKTtcclxuICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5lbmRDdXN0b21Mb2FkaW5nKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzZXRDdXN0b21Db2x1bW5EYXRhKGFyZ3M6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGN1c3RvbUNvbHMgPSBhcmdzLmNvbHVtbnMuZmlsdGVyKGNvbCA9PiBjb2wuZXhwcmVzc2lvbiAmJiBjb2wuZ3JvdXBOYW1lID09PSAnQ3VzdG9tIENvbHVtbnMnKTtcclxuICAgICAgICBjdXN0b21Db2xzLmZvckVhY2goY29sID0+IHtcclxuICAgICAgICAgICAgYXJncy5kYXRhW2NvbC5kYXRhRmllbGRdID0gYXJncy52YWx1ZXNbY29sLmluZGV4XTtcclxuICAgICAgICAgICAgdGhpcy5jb2x1bW5zLmZpbHRlcihjb2x1bW4gPT4gY29sdW1uLmRhdGFGaWVsZCA9PT0gY29sLmRhdGFGaWVsZClbMF0uZGF0YVR5cGUgPSBjb2wuZGF0YVR5cGU7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBmb3JtYXRSb3dzKGRhdGFUeXBlOiBzdHJpbmcsIHJvd0RhdGEsIHJvd0VsZW1lbnQsIGV4Y2VsQ2VsbCk6IHZvaWQge1xyXG4gICAgICAgIGlmIChkYXRhVHlwZSA9PT0gcm93VHlwZUluZm8uZGF0YSkge1xyXG4gICAgICAgICAgICBjb25zdCBjb25kaXRpb25hbEZvcm1hdENvbGxlY3Rpb246IENvbHVtbkZvcm1hdFtdID1cclxuICAgICAgICAgICAgICAgIEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShnZXRTdG9yYWdlS2V5KHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLCBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5jb25kaXRpb25hbEZvcm1hdHRpbmddLCB0aGlzLmlzTWFzdGVyR3JpZCkpKSB8fCBbXTtcclxuICAgICAgICAgICAgY29uc3QgY29uZGl0aW9uRmlsdGVySXRlbXMgPSBjb25kaXRpb25hbEZvcm1hdENvbGxlY3Rpb24uZmlsdGVyKGFyckl0ZW0gPT4gYXJySXRlbS5hcHBseVR5cGUgPT09ICdyb3cnKTtcclxuICAgICAgICAgICAgbGV0IGNvbmRpdGlvbmFsRm9ybWF0dGluZ1JlcXVpcmVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIGNvbmRpdGlvbkZpbHRlckl0ZW1zLmZvckVhY2goZmlsdGVySXRlbSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZmlsdGVySXRlbS5jb25kaXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25kaXRpb25hbEZvcm1hdHRpbmdSZXF1aXJlZCA9IHRoaXMuY2hlY2tDb25kaXRpb25TYXRpc2ZpZWQoZmlsdGVySXRlbSwgcm93RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNvbmRpdGlvbmFsRm9ybWF0dGluZ1JlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyb3dFbGVtZW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgcm93RWxlbWVudC5jaGlsZHJlbi5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBleGlzdGluZ0NsYXNzZXMgPSByb3dFbGVtZW50LmNoaWxkcmVuW2luZGV4XS5jbGFzc0xpc3Q/LnRvU3RyaW5nKCkucmVwbGFjZSgnYm9sZCcsICcnKS5yZXBsYWNlKCd1bmRlcmxpbmUnLCAnJykucmVwbGFjZSgnaXRhbGljJywgJycpIHx8ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd0VsZW1lbnQuY2hpbGRyZW5baW5kZXhdLnN0eWxlLmJhY2tncm91bmRDb2xvciA9IGZpbHRlckl0ZW0uYmFja2dyb3VuZENvbG9yIHx8IHJvd0VsZW1lbnQuY2hpbGRyZW5baW5kZXhdLnN0eWxlLmJhY2tncm91bmRDb2xvcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3dFbGVtZW50LmNoaWxkcmVuW2luZGV4XS5zdHlsZS5jb2xvciA9IGZpbHRlckl0ZW0udGV4dENvbG9yIHx8IHJvd0VsZW1lbnQuY2hpbGRyZW5baW5kZXhdLnN0eWxlLmNvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd0VsZW1lbnQuY2hpbGRyZW5baW5kZXhdLmNsYXNzTGlzdCA9IGV4aXN0aW5nQ2xhc3Nlcy5jb25jYXQoJyAnKS5jb25jYXQoZmlsdGVySXRlbS5jc3NDbGFzcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZXhjZWxDZWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZmlsdGVySXRlbS5iYWNrZ3JvdW5kQ29sb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGNlbENlbGwuZmlsbCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3BhdHRlcm4nLCBwYXR0ZXJuOiAnc29saWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmZ0NvbG9yOiB7IGFyZ2I6IGZpbHRlckl0ZW0uYmFja2dyb3VuZENvbG9yLnJlcGxhY2UoJyMnLCAnZmYnKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWx0ZXJJdGVtLnRleHRDb2xvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4Y2VsQ2VsbC5mb250ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcjogeyBhcmdiOiBmaWx0ZXJJdGVtLnRleHRDb2xvci5yZXBsYWNlKCcjJywgJ2ZmJykgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZmlsdGVySXRlbS5jc3NDbGFzcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXhjZWxDZWxsRm9ybWF0KGZpbHRlckl0ZW0uY3NzQ2xhc3MsIGV4Y2VsQ2VsbCwgbnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19