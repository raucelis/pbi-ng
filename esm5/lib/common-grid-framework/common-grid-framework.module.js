import { __decorate } from "tslib";
//#region Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxDataGridModule, DxScrollViewModule, DxDropDownBoxModule, DxTemplateModule, DxButtonModule, DxTextBoxModule, DxColorBoxModule, DxValidationGroupModule, DxValidatorModule, DxDropDownButtonModule, DxRadioGroupModule, DxTreeViewModule, DxPopupModule, DxSelectBoxModule, DxFilterBuilderModule, DxListModule, DxMenuModule, DxNumberBoxModule, DxDateBoxModule } from 'devextreme-angular';
//#endregion
//#region Components of Common Grid Framework
import { ColorFormatComponent } from './components/color-format/color-format.component';
import { ColumnChooserComponent } from './components/column-chooser/column-chooser.component';
import { CommonGridFrameworkComponent } from './components/common-grid-framework/common-grid-framework.component';
import { ConditionalFormattingComponent } from './components/conditional-formatting/conditional-formatting.component';
import { FilterComponent } from './components/filter/filter.component';
import { FlyOutActionIconContainerComponent } from './components/fly-out-action-icon-container/fly-out-action-icon-container.component';
import { FormsModule } from '@angular/forms';
import { GridComponent } from './components/grid/grid.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ViewSelectionComponent } from './components/view-selection/view-selection.component';
import { EntityParametersComponent } from './components/entity-parameters/entity-parameters.component';
//#endregion Components of Common Grid Framework
var components = [
    ColorFormatComponent,
    ColumnChooserComponent,
    CommonGridFrameworkComponent,
    ConditionalFormattingComponent,
    EntityParametersComponent,
    FilterComponent,
    FlyOutActionIconContainerComponent,
    GridComponent,
    SettingsComponent,
    ViewSelectionComponent,
];
var PBICommonGridFrameworkModule = /** @class */ (function () {
    function PBICommonGridFrameworkModule() {
    }
    PBICommonGridFrameworkModule = __decorate([
        NgModule({
            declarations: [components],
            imports: [
                CommonModule,
                DxButtonModule,
                DxColorBoxModule,
                DxDataGridModule,
                DxDateBoxModule,
                DxDropDownBoxModule,
                DxDropDownButtonModule,
                DxFilterBuilderModule,
                DxListModule,
                DxMenuModule,
                DxNumberBoxModule,
                DxPopupModule,
                DxRadioGroupModule,
                DxScrollViewModule,
                DxSelectBoxModule,
                DxTemplateModule,
                DxTextBoxModule,
                DxTreeViewModule,
                DxValidationGroupModule,
                DxValidatorModule,
                FormsModule,
            ],
            exports: components
        })
    ], PBICommonGridFrameworkModule);
    return PBICommonGridFrameworkModule;
}());
export { PBICommonGridFrameworkModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLWdyaWQtZnJhbWV3b3JrLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tbW9uLWdyaWQtZnJhbWV3b3JrLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsaUJBQWlCO0FBQ2pCLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFDTCxnQkFBZ0IsRUFDaEIsa0JBQWtCLEVBQ2xCLG1CQUFtQixFQUNuQixnQkFBZ0IsRUFDaEIsY0FBYyxFQUNkLGVBQWUsRUFDZixnQkFBZ0IsRUFDaEIsdUJBQXVCLEVBQ3ZCLGlCQUFpQixFQUNqQixzQkFBc0IsRUFDdEIsa0JBQWtCLEVBQ2xCLGdCQUFnQixFQUVoQixhQUFhLEVBQUUsaUJBQWlCLEVBQUUscUJBQXFCLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxpQkFBaUIsRUFBRSxlQUFlLEVBQ3hILE1BQU0sb0JBQW9CLENBQUM7QUFDNUIsWUFBWTtBQUVaLDZDQUE2QztBQUM3QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN4RixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUM5RixPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxvRUFBb0UsQ0FBQztBQUNsSCxPQUFPLEVBQUUsOEJBQThCLEVBQUUsTUFBTSxzRUFBc0UsQ0FBQztBQUN0SCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGtDQUFrQyxFQUFFLE1BQU0sb0ZBQW9GLENBQUM7QUFDeEksT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNqRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUM5RixPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSw0REFBNEQsQ0FBQztBQUN2RyxnREFBZ0Q7QUFHaEQsSUFBTSxVQUFVLEdBQVU7SUFDeEIsb0JBQW9CO0lBQ3BCLHNCQUFzQjtJQUN0Qiw0QkFBNEI7SUFDNUIsOEJBQThCO0lBQzlCLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2Ysa0NBQWtDO0lBQ2xDLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsc0JBQXNCO0NBQ3ZCLENBQUM7QUE2QkY7SUFBQTtJQUE0QyxDQUFDO0lBQWhDLDRCQUE0QjtRQTNCeEMsUUFBUSxDQUFDO1lBQ1IsWUFBWSxFQUFFLENBQUMsVUFBVSxDQUFDO1lBQzFCLE9BQU8sRUFBRTtnQkFDUCxZQUFZO2dCQUNaLGNBQWM7Z0JBQ2QsZ0JBQWdCO2dCQUNoQixnQkFBZ0I7Z0JBQ2hCLGVBQWU7Z0JBQ2YsbUJBQW1CO2dCQUNuQixzQkFBc0I7Z0JBQ3RCLHFCQUFxQjtnQkFDckIsWUFBWTtnQkFDWixZQUFZO2dCQUNaLGlCQUFpQjtnQkFDakIsYUFBYTtnQkFDYixrQkFBa0I7Z0JBQ2xCLGtCQUFrQjtnQkFDbEIsaUJBQWlCO2dCQUNqQixnQkFBZ0I7Z0JBQ2hCLGVBQWU7Z0JBQ2YsZ0JBQWdCO2dCQUNoQix1QkFBdUI7Z0JBQ3ZCLGlCQUFpQjtnQkFDakIsV0FBVzthQUNaO1lBQ0QsT0FBTyxFQUFFLFVBQVU7U0FDcEIsQ0FBQztPQUNXLDRCQUE0QixDQUFJO0lBQUQsbUNBQUM7Q0FBQSxBQUE3QyxJQUE2QztTQUFoQyw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyIvLyNyZWdpb24gTW9kdWxlc1xyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge1xyXG4gIER4RGF0YUdyaWRNb2R1bGUsXHJcbiAgRHhTY3JvbGxWaWV3TW9kdWxlLFxyXG4gIER4RHJvcERvd25Cb3hNb2R1bGUsXHJcbiAgRHhUZW1wbGF0ZU1vZHVsZSxcclxuICBEeEJ1dHRvbk1vZHVsZSxcclxuICBEeFRleHRCb3hNb2R1bGUsXHJcbiAgRHhDb2xvckJveE1vZHVsZSxcclxuICBEeFZhbGlkYXRpb25Hcm91cE1vZHVsZSxcclxuICBEeFZhbGlkYXRvck1vZHVsZSxcclxuICBEeERyb3BEb3duQnV0dG9uTW9kdWxlLFxyXG4gIER4UmFkaW9Hcm91cE1vZHVsZSxcclxuICBEeFRyZWVWaWV3TW9kdWxlLFxyXG4gIER4UG9wb3Zlck1vZHVsZSxcclxuICBEeFBvcHVwTW9kdWxlLCBEeFNlbGVjdEJveE1vZHVsZSwgRHhGaWx0ZXJCdWlsZGVyTW9kdWxlLCBEeExpc3RNb2R1bGUsIER4TWVudU1vZHVsZSwgRHhOdW1iZXJCb3hNb2R1bGUsIER4RGF0ZUJveE1vZHVsZVxyXG59IGZyb20gJ2RldmV4dHJlbWUtYW5ndWxhcic7XHJcbi8vI2VuZHJlZ2lvblxyXG5cclxuLy8jcmVnaW9uIENvbXBvbmVudHMgb2YgQ29tbW9uIEdyaWQgRnJhbWV3b3JrXHJcbmltcG9ydCB7IENvbG9yRm9ybWF0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbG9yLWZvcm1hdC9jb2xvci1mb3JtYXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29sdW1uQ2hvb3NlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jb2x1bW4tY2hvb3Nlci9jb2x1bW4tY2hvb3Nlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb21tb25HcmlkRnJhbWV3b3JrQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21tb24tZ3JpZC1mcmFtZXdvcmsuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29uZGl0aW9uYWxGb3JtYXR0aW5nQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbmRpdGlvbmFsLWZvcm1hdHRpbmcvY29uZGl0aW9uYWwtZm9ybWF0dGluZy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGaWx0ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmlsdGVyL2ZpbHRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGbHlPdXRBY3Rpb25JY29uQ29udGFpbmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBHcmlkQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2dyaWQvZ3JpZC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZXR0aW5nc0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9zZXR0aW5ncy9zZXR0aW5ncy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBWaWV3U2VsZWN0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3ZpZXctc2VsZWN0aW9uL3ZpZXctc2VsZWN0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEVudGl0eVBhcmFtZXRlcnNDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZW50aXR5LXBhcmFtZXRlcnMvZW50aXR5LXBhcmFtZXRlcnMuY29tcG9uZW50JztcclxuLy8jZW5kcmVnaW9uIENvbXBvbmVudHMgb2YgQ29tbW9uIEdyaWQgRnJhbWV3b3JrXHJcblxyXG5cclxuY29uc3QgY29tcG9uZW50czogYW55W10gPSBbXHJcbiAgQ29sb3JGb3JtYXRDb21wb25lbnQsXHJcbiAgQ29sdW1uQ2hvb3NlckNvbXBvbmVudCxcclxuICBDb21tb25HcmlkRnJhbWV3b3JrQ29tcG9uZW50LFxyXG4gIENvbmRpdGlvbmFsRm9ybWF0dGluZ0NvbXBvbmVudCxcclxuICBFbnRpdHlQYXJhbWV0ZXJzQ29tcG9uZW50LFxyXG4gIEZpbHRlckNvbXBvbmVudCxcclxuICBGbHlPdXRBY3Rpb25JY29uQ29udGFpbmVyQ29tcG9uZW50LFxyXG4gIEdyaWRDb21wb25lbnQsXHJcbiAgU2V0dGluZ3NDb21wb25lbnQsXHJcbiAgVmlld1NlbGVjdGlvbkNvbXBvbmVudCxcclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbY29tcG9uZW50c10sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgRHhCdXR0b25Nb2R1bGUsXHJcbiAgICBEeENvbG9yQm94TW9kdWxlLFxyXG4gICAgRHhEYXRhR3JpZE1vZHVsZSxcclxuICAgIER4RGF0ZUJveE1vZHVsZSxcclxuICAgIER4RHJvcERvd25Cb3hNb2R1bGUsXHJcbiAgICBEeERyb3BEb3duQnV0dG9uTW9kdWxlLFxyXG4gICAgRHhGaWx0ZXJCdWlsZGVyTW9kdWxlLFxyXG4gICAgRHhMaXN0TW9kdWxlLFxyXG4gICAgRHhNZW51TW9kdWxlLFxyXG4gICAgRHhOdW1iZXJCb3hNb2R1bGUsXHJcbiAgICBEeFBvcHVwTW9kdWxlLFxyXG4gICAgRHhSYWRpb0dyb3VwTW9kdWxlLFxyXG4gICAgRHhTY3JvbGxWaWV3TW9kdWxlLFxyXG4gICAgRHhTZWxlY3RCb3hNb2R1bGUsXHJcbiAgICBEeFRlbXBsYXRlTW9kdWxlLFxyXG4gICAgRHhUZXh0Qm94TW9kdWxlLFxyXG4gICAgRHhUcmVlVmlld01vZHVsZSxcclxuICAgIER4VmFsaWRhdGlvbkdyb3VwTW9kdWxlLFxyXG4gICAgRHhWYWxpZGF0b3JNb2R1bGUsXHJcbiAgICBGb3Jtc01vZHVsZSxcclxuICBdLFxyXG4gIGV4cG9ydHM6IGNvbXBvbmVudHNcclxufSlcclxuZXhwb3J0IGNsYXNzIFBCSUNvbW1vbkdyaWRGcmFtZXdvcmtNb2R1bGUgeyB9XHJcbiJdfQ==