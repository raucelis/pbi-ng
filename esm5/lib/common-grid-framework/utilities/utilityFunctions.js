import notify from 'devextreme/ui/notify';
import { SecurityMasterAction } from './enums';
import { actionTypes } from './constants';
// this is duplicate function specific to models.
export function getStorageKey(gridInstance, keyValue, isMasterGrid) {
    if (isMasterGrid === void 0) { isMasterGrid = true; }
    var masterDetailInfo = gridInstance === null || gridInstance === void 0 ? void 0 : gridInstance.option('masterDetail');
    return isMasterGrid ? keyValue : keyValue + "_" + masterDetailInfo.template;
}
export function getClassNameByThemeName(themeName) {
    switch (themeName) {
        case 'np.compact':
        case 'dx-swatch-default':
            return 'dx-swatch-default';
        case 'light.compact':
        case 'dx-swatch-regular':
            return 'dx-swatch-regular';
        case 'light.regular':
        default:
            return '';
    }
}
export function applyFilterCssClass(filter, gridInstance) {
    if (filter === void 0) { filter = []; }
    var i, cssClasses, columnInfo;
    for (i = 0; filter && i < filter.length; i++) {
        if (Array.isArray(filter[i])) {
            if (Array.isArray(filter[i][0])) {
                applyFilterCssClass(filter[i], gridInstance);
            }
            else {
                columnInfo = gridInstance.columnOption(filter[i][0]);
                if (columnInfo && (columnInfo.dataField === filter[i][0])) {
                    cssClasses = gridInstance.columnOption(filter[i][0], 'cssClass');
                    gridInstance.columnOption(filter[i][0], 'cssClass', cssClasses + ' filterApplied');
                }
            }
        }
        else {
            columnInfo = gridInstance.columnOption(filter[i]);
            if (columnInfo && (columnInfo.dataField === filter[i])) {
                cssClasses = gridInstance.columnOption(filter[i], 'cssClass');
                gridInstance.columnOption(filter[i], 'cssClass', cssClasses ? cssClasses + ' filterApplied' : 'filterApplied');
            }
        }
    }
}
export function applyFiltersToGrid(gridInstance, filterValue) {
    if (gridInstance) {
        // const listOfVisibleColumns = gridInstance.getVisibleColumns();
        // let cssClasses: string;
        // for (let i = 0; i < listOfVisibleColumns.length; i++) {
        //     cssClasses = gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass');
        //     if (cssClasses) {
        //         gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass', cssClasses.replace(new RegExp('filterApplied', 'g'), '').trim());
        //     }
        // }
        if (filterValue) {
            // applyFilterCssClass(filterValue, gridInstance);
            var alertOptions = {
                type: 'warning', message: 'To permanently apply this filter,please go to the \'View Selection\' and click \'Save\''
                    + ' in order to associate this filter with a view.'
            };
            appToast(alertOptions);
        }
    }
}
export function appToast(options) {
    var defaultOptions = {
        message: 'Oops.Something went wrong !',
        closeOnClick: true,
        width: '550px',
        type: 'error',
        displayTime: 3000,
        position: {
            my: 'center top',
            at: 'center top'
        },
    };
    // * extend the below switch for other toast(notify) types
    if (typeof options.type === 'string') {
        switch (options.type.toLowerCase()) {
            case 'warning':
                defaultOptions.displayTime = 10000;
                break;
            default:
                defaultOptions.displayTime = 3000;
                break;
        }
    }
    options = Object.assign({}, defaultOptions, options);
    notify(options);
}
export function ToDictionary(array, key) {
    var dic = {};
    array.forEach(function (e) {
        var k = key(e);
        if (!dic.hasOwnProperty(k)) {
            dic[key(e)] = e;
        }
        else {
            console.warn('Duplicate Key in Array', e);
        }
        ;
    });
    return dic;
}
export function filterSelectedRowData(gridInstance, rowData) {
    var visibleColumns = gridInstance.getVisibleColumns().map(function (item) {
        return item['dataField'];
    });
    var selectedRowData = [];
    if (rowData && Object.keys(rowData).length && Object.keys(rowData.data).length) {
        selectedRowData.push(JSON.parse(JSON.stringify(rowData.data)));
    }
    else {
        selectedRowData = JSON.parse(JSON.stringify(gridInstance.getSelectedRowsData()));
    }
    if (selectedRowData.length) {
        for (var columnProperty in selectedRowData[0]) {
            if (!visibleColumns.includes(columnProperty)) {
                delete selectedRowData[0][columnProperty];
            }
        }
    }
    return selectedRowData;
}
export function openDestination(data, selectedRowData, entityParameterInfo, appService) {
    if (entityParameterInfo === void 0) { entityParameterInfo = []; }
    if (appService === void 0) { appService = null; }
    var _a, _b;
    var urlDynamicParamCount = 0, paramFilter = [], url, urlParams, paramterMapping, delimiter, destinationUrl, staticParams;
    var undefinedSourceColumnNames = '';
    var comma = ',';
    switch ((_a = data.DestinationType) === null || _a === void 0 ? void 0 : _a.toLowerCase().trim()) {
        case actionTypes.addSecurity.toLowerCase():
            if (appService) {
                appService.showSM({ SecurityId: null, Action: SecurityMasterAction.AddSecurity });
            }
            return;
        case actionTypes.dispatcherJob.toLowerCase():
            url = location.href.split('#')[0] + "#/DispatcherManagement?webMenuId=" + getWebMenuIdByKey('DispatcherManagement') + "&jobId=" + data.jobId;
            open(url, '_blank');
            return;
        case actionTypes.lookup.toLowerCase():
            url = location.href.split('#')[0] + "#/LookupTables?webMenuId=" + getWebMenuIdByKey('LookupTables') + "&lookupId=" + data.LookupId;
            open(url, '_blank');
            return;
        case actionTypes.entity.toLowerCase():
        case actionTypes.editSecurity.toLowerCase():
        case actionTypes.url.toLowerCase():
            if (selectedRowData && selectedRowData.length) {
                switch ((_b = data.DestinationType) === null || _b === void 0 ? void 0 : _b.toLowerCase()) {
                    case actionTypes.editSecurity.toLowerCase():
                        if (data.ShowAsContextMenu) {
                            return true;
                        }
                        else {
                            var secKey = '';
                            for (var key in selectedRowData[0]) {
                                if (selectedRowData[0].hasOwnProperty(key) && (key.toLowerCase() === 'securityid' || key.toLowerCase() === 'security id')) {
                                    secKey = key;
                                }
                            }
                            if (selectedRowData[0][secKey] && appService) {
                                appService.showSM({ SecurityId: selectedRowData[0][secKey], Action: SecurityMasterAction.EditSecurity });
                                return true;
                            }
                            else {
                                appToast({ type: 'error', message: "Following column(s) Security Id is required. Please make sure same exist(s) in the grid with value.", displayTime: 4000 });
                                return false;
                            }
                        }
                        break;
                    case actionTypes.entity.toLowerCase():
                        var queryParams = location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                        queryParams['entity'] = data.DestinationEntity;
                        queryParams['webMenuId'] = data.DestinationWebMenuItemId;
                        queryParams['layout'] = 0;
                        queryParams['gridname'] = data.DestinationEntityName + '_' + data.DestinationEntity;
                        paramterMapping = data.ParameterMapping;
                        var paramInfo = extractDestinationEntityParameters(selectedRowData, paramterMapping, entityParameterInfo);
                        undefinedSourceColumnNames = paramInfo.undefinedSourceColumnNames;
                        url = location.hash.split('?')[0] + '?webMenuId=' + queryParams['webMenuId'] + '&entity=' + queryParams['entity'] +
                            '&gridname=' + queryParams['gridname'] + paramInfo.param + '&layout=' + queryParams['layout'];
                        break;
                    case actionTypes.url.toLowerCase():
                        destinationUrl = data.DestinationUrl.split('?'), paramterMapping = data.ParameterMapping;
                        url = destinationUrl[0], staticParams = destinationUrl[1];
                        if (staticParams !== undefined) {
                            url = url + '?' + staticParams;
                        }
                        urlParams = url.match(/{[^{}]*}/gi);
                        if (urlParams != null) {
                            urlParams.forEach(function (item) {
                                paramterMapping.forEach(function (paramItem) {
                                    if (paramItem.ParameterName === item.substr(1).slice(0, -1)) {
                                        if (paramItem.IsSourceParam) {
                                            var sourceParamName_1 = paramItem.SourceColumnName.split('@')[1].trim().toLowerCase();
                                            var parameter = entityParameterInfo.filter(function (par) { return par.Name.trim().toLowerCase() === sourceParamName_1
                                                || par.Name.trim().toLowerCase() === paramItem.SourceColumnName.trim().toLowerCase(); })[0];
                                            if (parameter.Value) {
                                                url = url.replace(item, checkAndFormatDate(parameter.Value));
                                            }
                                            else {
                                                undefinedSourceColumnNames += paramItem.SourceColumnName + comma;
                                            }
                                        }
                                        else {
                                            if (selectedRowData[0][item.SourceColumnName] || selectedRowData[0][paramItem.SourceColumnName]) {
                                                url = url.replace(item, checkAndFormatDate(selectedRowData[0][paramItem.SourceColumnName]));
                                            }
                                            else {
                                                undefinedSourceColumnNames += item.SourceColumnName ? item.SourceColumnName + comma : item + comma;
                                            }
                                        }
                                    }
                                });
                            });
                        }
                        paramterMapping.forEach(function (item, index) {
                            if (urlParams != null) {
                                paramFilter = urlParams.filter(function (urlItem) { return item.ParameterName === urlItem.substr(1).slice(0, -1); });
                            }
                            if (!paramFilter.length) {
                                delimiter = (urlDynamicParamCount === 0 && staticParams === undefined) ? '?' : '&';
                                if (selectedRowData[0][item.SourceColumnName]) {
                                    url = url + delimiter + item.ParameterName + '=' + checkAndFormatDate(selectedRowData[0][item.SourceColumnName]);
                                }
                                else {
                                    undefinedSourceColumnNames += item.SourceColumnName + comma;
                                }
                                urlDynamicParamCount++;
                            }
                        });
                        break;
                }
                if (!undefinedSourceColumnNames) {
                    open(url, '_blank');
                }
                else {
                    if (undefinedSourceColumnNames.endsWith(comma)) {
                        undefinedSourceColumnNames = undefinedSourceColumnNames.substring(0, undefinedSourceColumnNames.length - 1);
                        appToast({
                            type: 'error', message: "Following column(s) " + undefinedSourceColumnNames + " is required. Please make sure same exist(s) in the grid or parameter with value.",
                            displayTime: 4000
                        });
                    }
                }
            }
            else {
                appToast({ type: 'warning', message: 'Please select a row.' });
            }
    }
    return false;
}
export function extractDestinationEntityParameters(selectedRowData, parameterMapping, entityParameterInfo) {
    var param = '';
    var undefinedSourceColumnNames = '';
    var comma = ',';
    parameterMapping.forEach(function (item) {
        var paramName = item.OriginalParameterName || item.ParameterName;
        if (item.IsSourceParam) {
            var sourceParamName_2 = item.SourceColumnName.split('@')[1].trim().toLowerCase();
            var parameter = entityParameterInfo.filter(function (par) { return par.Name.trim().toLowerCase() === sourceParamName_2
                || par.Name.trim().toLowerCase() === item.SourceColumnName.trim().toLowerCase(); })[0];
            if (parameter === null || parameter === void 0 ? void 0 : parameter.Value) {
                param = param + '&' + paramName + '=' + checkAndFormatDate(parameter.Value);
            }
            else {
                undefinedSourceColumnNames += item.SourceColumnName + comma;
            }
        }
        else {
            if (selectedRowData[0][item.SourceColumnName]) {
                param = param + '&' + paramName + '=' + checkAndFormatDate(selectedRowData[0][item.SourceColumnName]);
            }
            else {
                undefinedSourceColumnNames += item.SourceColumnName + comma;
            }
        }
    });
    return { param: param, undefinedSourceColumnNames: undefinedSourceColumnNames };
}
export function convertDateToUsFormat(value) {
    var dateValue = new Date(value);
    var dd = dateValue.getDate();
    var mm = dateValue.getMonth() + 1;
    var yyyy = dateValue.getFullYear();
    dd = dd < 10 ? '0' + dd : dd;
    mm = mm < 10 ? '0' + mm : mm;
    return mm + "/" + dd + "/" + yyyy;
}
/**
 * this method will return web menu id based on key
 * @param  {string} key
 * @returns number
 */
function getWebMenuIdByKey(key) {
    var webMenus = window['menuData'] || [];
    if (webMenus.length) {
        var filteredItem = webMenus.filter(function (arrItem) { var _a; return ((_a = arrItem.Key) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === key.toLowerCase(); })[0];
        return filteredItem ? filteredItem.OptomasToolId : undefined;
    }
}
// convert from YYYY-MM-DDTHH:mm:ss.SSS to MM/DD/YYYY, else return the param as it is
function checkAndFormatDate(value) {
    // const moment = window['moment'];
    // // ref: https://flaviocopes.com/momentjs/
    // if (moment(value, moment.HTML5_FMT.DATETIME_LOCAL_MS, true).isValid()) {
    //     return moment(value).format('MM/DD/YYYY');
    // } else {
    //     return value;
    // }
    return value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbGl0eUZ1bmN0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxNQUFNLE1BQU0sc0JBQXNCLENBQUM7QUFHMUMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFMUMsaURBQWlEO0FBQ2pELE1BQU0sVUFBVSxhQUFhLENBQUMsWUFBd0IsRUFBRSxRQUFnQixFQUFFLFlBQW1CO0lBQW5CLDZCQUFBLEVBQUEsbUJBQW1CO0lBQ3pGLElBQU0sZ0JBQWdCLEdBQUcsWUFBWSxhQUFaLFlBQVksdUJBQVosWUFBWSxDQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5RCxPQUFPLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBSSxRQUFRLFNBQUksZ0JBQWdCLENBQUMsUUFBVSxDQUFDO0FBQ2hGLENBQUM7QUFFRCxNQUFNLFVBQVUsdUJBQXVCLENBQUMsU0FBaUI7SUFDckQsUUFBUSxTQUFTLEVBQUU7UUFDZixLQUFLLFlBQVksQ0FBQztRQUNsQixLQUFLLG1CQUFtQjtZQUNwQixPQUFPLG1CQUFtQixDQUFDO1FBQy9CLEtBQUssZUFBZSxDQUFDO1FBQ3JCLEtBQUssbUJBQW1CO1lBQ3BCLE9BQU8sbUJBQW1CLENBQUM7UUFDL0IsS0FBSyxlQUFlLENBQUM7UUFDckI7WUFDSSxPQUFPLEVBQUUsQ0FBQztLQUNqQjtBQUNMLENBQUM7QUFFRCxNQUFNLFVBQVUsbUJBQW1CLENBQUMsTUFBVyxFQUFFLFlBQVk7SUFBekIsdUJBQUEsRUFBQSxXQUFXO0lBQzNDLElBQUksQ0FBUyxFQUFFLFVBQWtCLEVBQUUsVUFBeUIsQ0FBQztJQUM3RCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsTUFBTSxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1FBQzFDLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUMxQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzdCLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUNoRDtpQkFBTTtnQkFDSCxVQUFVLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckQsSUFBSSxVQUFVLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUN2RCxVQUFVLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBQ2pFLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsRUFBRSxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQztpQkFDdEY7YUFDSjtTQUNKO2FBQU07WUFDSCxVQUFVLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNsRCxJQUFJLFVBQVUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3BELFVBQVUsR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDOUQsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNsSDtTQUNKO0tBQ0o7QUFDTCxDQUFDO0FBRUQsTUFBTSxVQUFVLGtCQUFrQixDQUFDLFlBQXdCLEVBQUUsV0FBd0I7SUFDakYsSUFBSSxZQUFZLEVBQUU7UUFDZCxpRUFBaUU7UUFDakUsMEJBQTBCO1FBQzFCLDBEQUEwRDtRQUMxRCw2RkFBNkY7UUFDN0Ysd0JBQXdCO1FBQ3hCLHFKQUFxSjtRQUNySixRQUFRO1FBQ1IsSUFBSTtRQUVKLElBQUksV0FBVyxFQUFFO1lBQ2Isa0RBQWtEO1lBQ2xELElBQU0sWUFBWSxHQUFHO2dCQUNqQixJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSx5RkFBeUY7c0JBQzdHLGlEQUFpRDthQUMxRCxDQUFDO1lBQ0YsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQzFCO0tBQ0o7QUFDTCxDQUFDO0FBRUQsTUFBTSxVQUFVLFFBQVEsQ0FBQyxPQUF3QjtJQUM3QyxJQUFNLGNBQWMsR0FBRztRQUNuQixPQUFPLEVBQUUsNkJBQTZCO1FBQ3RDLFlBQVksRUFBRSxJQUFJO1FBQ2xCLEtBQUssRUFBRSxPQUFPO1FBQ2QsSUFBSSxFQUFFLE9BQU87UUFDYixXQUFXLEVBQUUsSUFBSTtRQUNqQixRQUFRLEVBQUU7WUFDTixFQUFFLEVBQUUsWUFBWTtZQUNoQixFQUFFLEVBQUUsWUFBWTtTQUNuQjtLQUNKLENBQUM7SUFDRiwwREFBMEQ7SUFDMUQsSUFBSSxPQUFPLE9BQU8sQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO1FBQ2xDLFFBQVEsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUNoQyxLQUFLLFNBQVM7Z0JBQ1YsY0FBYyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7Z0JBQ25DLE1BQU07WUFDVjtnQkFDSSxjQUFjLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztnQkFDbEMsTUFBTTtTQUNiO0tBQ0o7SUFFRCxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3JELE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztBQUNwQixDQUFDO0FBR0QsTUFBTSxVQUFVLFlBQVksQ0FBSSxLQUFVLEVBQUUsR0FBb0M7SUFDNUUsSUFBTSxHQUFHLEdBQXlCLEVBQUUsQ0FBQztJQUNyQyxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQUEsQ0FBQztRQUNYLElBQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN4QixHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ25CO2FBQU07WUFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzdDO1FBQUEsQ0FBQztJQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ0gsT0FBTyxHQUFHLENBQUM7QUFDZixDQUFDO0FBRUQsTUFBTSxVQUFVLHFCQUFxQixDQUFDLFlBQVksRUFBRSxPQUFhO0lBQzdELElBQU0sY0FBYyxHQUFHLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUk7UUFDN0QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDN0IsQ0FBQyxDQUFDLENBQUM7SUFDSCxJQUFJLGVBQWUsR0FBRyxFQUFFLENBQUM7SUFDekIsSUFBSSxPQUFPLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFO1FBQzVFLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7S0FDbEU7U0FBTTtRQUNILGVBQWUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQyxDQUFDO0tBQ3BGO0lBQ0QsSUFBSSxlQUFlLENBQUMsTUFBTSxFQUFFO1FBQ3hCLEtBQUssSUFBTSxjQUFjLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzdDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUMxQyxPQUFPLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUM3QztTQUNKO0tBQ0o7SUFDRCxPQUFPLGVBQWUsQ0FBQztBQUMzQixDQUFDO0FBRUQsTUFBTSxVQUFVLGVBQWUsQ0FBQyxJQUFJLEVBQUUsZUFBb0IsRUFBRSxtQkFBK0IsRUFBRSxVQUFpQjtJQUFsRCxvQ0FBQSxFQUFBLHdCQUErQjtJQUFFLDJCQUFBLEVBQUEsaUJBQWlCOztJQUMxRyxJQUFJLG9CQUFvQixHQUFHLENBQUMsRUFBRSxXQUFXLEdBQUcsRUFBRSxFQUFFLEdBQVcsRUFBRSxTQUFTLEVBQUUsZUFBc0IsRUFBRSxTQUFpQixFQUFFLGNBQXNCLEVBQUUsWUFBb0IsQ0FBQztJQUNoSyxJQUFJLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztJQUNwQyxJQUFNLEtBQUssR0FBRyxHQUFHLENBQUM7SUFFbEIsY0FBUSxJQUFJLENBQUMsZUFBZSwwQ0FBRSxXQUFXLEdBQUcsSUFBSSxJQUFJO1FBQ2hELEtBQUssV0FBVyxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7WUFDdEMsSUFBSSxVQUFVLEVBQUU7Z0JBQ1osVUFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLG9CQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7YUFDckY7WUFDRCxPQUFPO1FBQ1gsS0FBSyxXQUFXLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRTtZQUN4QyxHQUFHLEdBQU0sUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLHlDQUFvQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxlQUFVLElBQUksQ0FBQyxLQUFPLENBQUM7WUFDeEksSUFBSSxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUNwQixPQUFPO1FBQ1gsS0FBSyxXQUFXLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRTtZQUNqQyxHQUFHLEdBQU0sUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlDQUE0QixpQkFBaUIsQ0FBQyxjQUFjLENBQUMsa0JBQWEsSUFBSSxDQUFDLFFBQVUsQ0FBQztZQUM5SCxJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3BCLE9BQU87UUFDWCxLQUFLLFdBQVcsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdEMsS0FBSyxXQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzVDLEtBQUssV0FBVyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUU7WUFDOUIsSUFBSSxlQUFlLElBQUksZUFBZSxDQUFDLE1BQU0sRUFBRTtnQkFDM0MsY0FBUSxJQUFJLENBQUMsZUFBZSwwQ0FBRSxXQUFXLElBQUk7b0JBQ3pDLEtBQUssV0FBVyxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUU7d0JBQ3ZDLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFOzRCQUN4QixPQUFPLElBQUksQ0FBQzt5QkFDZjs2QkFBTTs0QkFDSCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7NEJBQ2hCLEtBQUssSUFBTSxHQUFHLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dDQUNsQyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssWUFBWSxJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxhQUFhLENBQUMsRUFBRTtvQ0FDdkgsTUFBTSxHQUFHLEdBQUcsQ0FBQztpQ0FDaEI7NkJBQ0o7NEJBQ0QsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksVUFBVSxFQUFFO2dDQUMxQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUUsVUFBVSxFQUFFLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxNQUFNLEVBQUUsb0JBQW9CLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQTtnQ0FDeEcsT0FBTyxJQUFJLENBQUM7NkJBQ2Y7aUNBQU07Z0NBQ0gsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUscUdBQXFHLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0NBQy9KLE9BQU8sS0FBSyxDQUFDOzZCQUNoQjt5QkFDSjt3QkFDRCxNQUFNO29CQUNWLEtBQUssV0FBVyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUU7d0JBQ2pDLElBQU0sV0FBVyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzFGLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7d0JBQy9DLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUM7d0JBQ3pELFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzFCLFdBQVcsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMscUJBQXFCLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQzt3QkFFcEYsZUFBZSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQzt3QkFDeEMsSUFBTSxTQUFTLEdBQUcsa0NBQWtDLENBQUMsZUFBZSxFQUFFLGVBQWUsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO3dCQUM1RywwQkFBMEIsR0FBRyxTQUFTLENBQUMsMEJBQTBCLENBQUM7d0JBQ2xFLEdBQUcsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxhQUFhLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLFVBQVUsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDOzRCQUM3RyxZQUFZLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxLQUFLLEdBQUcsVUFBVSxHQUFHLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDbEcsTUFBTTtvQkFDVixLQUFLLFdBQVcsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFO3dCQUM5QixjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsZUFBZSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQzt3QkFDekYsR0FBRyxHQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUMxRCxJQUFJLFlBQVksS0FBSyxTQUFTLEVBQUU7NEJBQzVCLEdBQUcsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQzt5QkFDbEM7d0JBQ0QsU0FBUyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUM7d0JBQ3BDLElBQUksU0FBUyxJQUFJLElBQUksRUFBRTs0QkFDbkIsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7Z0NBQ25CLGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQyxTQUFTO29DQUM5QixJQUFJLFNBQVMsQ0FBQyxhQUFhLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0NBQ3pELElBQUksU0FBUyxDQUFDLGFBQWEsRUFBRTs0Q0FDekIsSUFBTSxpQkFBZSxHQUFHLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7NENBQ3RGLElBQU0sU0FBUyxHQUFHLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssaUJBQWU7bURBQzlGLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxFQURwQyxDQUNvQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NENBRTdGLElBQUksU0FBUyxDQUFDLEtBQUssRUFBRTtnREFDakIsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzZDQUNoRTtpREFBTTtnREFDSCwwQkFBMEIsSUFBSSxTQUFTLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDOzZDQUNwRTt5Q0FDSjs2Q0FBTTs0Q0FDSCxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEVBQUU7Z0RBQzdGLEdBQUcsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDOzZDQUMvRjtpREFBTTtnREFDSCwwQkFBMEIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7NkNBQ3RHO3lDQUNKO3FDQUNKO2dDQUNMLENBQUMsQ0FBQyxDQUFDOzRCQUNQLENBQUMsQ0FBQyxDQUFDO3lCQUNOO3dCQUNELGVBQWUsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJLEVBQUUsS0FBSzs0QkFDaEMsSUFBSSxTQUFTLElBQUksSUFBSSxFQUFFO2dDQUNuQixXQUFXLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFDLE9BQU8sSUFBSyxPQUFBLElBQUksQ0FBQyxhQUFhLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQXJELENBQXFELENBQUMsQ0FBQzs2QkFDdEc7NEJBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUU7Z0NBQ3JCLFNBQVMsR0FBRyxDQUFDLG9CQUFvQixLQUFLLENBQUMsSUFBSSxZQUFZLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO2dDQUNuRixJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtvQ0FDM0MsR0FBRyxHQUFHLEdBQUcsR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7aUNBQ3BIO3FDQUFNO29DQUNILDBCQUEwQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7aUNBQy9EO2dDQUNELG9CQUFvQixFQUFFLENBQUM7NkJBQzFCO3dCQUNMLENBQUMsQ0FBQyxDQUFDO3dCQUNILE1BQU07aUJBQ2I7Z0JBQ0QsSUFBSSxDQUFDLDBCQUEwQixFQUFFO29CQUM3QixJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxJQUFJLDBCQUEwQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDNUMsMEJBQTBCLEdBQUcsMEJBQTBCLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSwwQkFBMEIsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQzVHLFFBQVEsQ0FBQzs0QkFDTCxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSx5QkFBdUIsMEJBQTBCLHNGQUFtRjs0QkFDMUosV0FBVyxFQUFFLElBQUk7eUJBQ3RCLENBQUMsQ0FBQztxQkFDTjtpQkFDSjthQUNKO2lCQUFNO2dCQUNILFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLENBQUMsQ0FBQzthQUNsRTtLQUNSO0lBQ0QsT0FBTyxLQUFLLENBQUM7QUFDakIsQ0FBQztBQUVELE1BQU0sVUFBVSxrQ0FBa0MsQ0FBQyxlQUFzQixFQUFFLGdCQUF1QixFQUFFLG1CQUEwQjtJQUMxSCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDZixJQUFJLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztJQUNwQyxJQUFNLEtBQUssR0FBRyxHQUFHLENBQUM7SUFDbEIsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtRQUMxQixJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUNuRSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDcEIsSUFBTSxpQkFBZSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDakYsSUFBTSxTQUFTLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxpQkFBZTttQkFDOUYsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLEVBRC9CLENBQytCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUV4RixJQUFJLFNBQVMsYUFBVCxTQUFTLHVCQUFULFNBQVMsQ0FBRSxLQUFLLEVBQUU7Z0JBQ2xCLEtBQUssR0FBRyxLQUFLLEdBQUcsR0FBRyxHQUFHLFNBQVMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQy9FO2lCQUFNO2dCQUNILDBCQUEwQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7YUFDL0Q7U0FDSjthQUFNO1lBQ0gsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUU7Z0JBQzNDLEtBQUssR0FBRyxLQUFLLEdBQUcsR0FBRyxHQUFHLFNBQVMsR0FBRyxHQUFHLEdBQUcsa0JBQWtCLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7YUFDekc7aUJBQU07Z0JBQ0gsMEJBQTBCLElBQUksSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQzthQUMvRDtTQUNKO0lBQ0wsQ0FBQyxDQUFDLENBQUM7SUFFSCxPQUFPLEVBQUUsS0FBSyxPQUFBLEVBQUUsMEJBQTBCLDRCQUFBLEVBQUUsQ0FBQztBQUNqRCxDQUFDO0FBRUQsTUFBTSxVQUFVLHFCQUFxQixDQUFDLEtBQW9CO0lBQ3RELElBQU0sU0FBUyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLElBQUksRUFBRSxHQUFvQixTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7SUFBQyxJQUFJLEVBQUUsR0FBb0IsU0FBUyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUFDLElBQU0sSUFBSSxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN4SSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzdCLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDN0IsT0FBVSxFQUFFLFNBQUksRUFBRSxTQUFJLElBQU0sQ0FBQztBQUNqQyxDQUFDO0FBR0Q7Ozs7R0FJRztBQUNILFNBQVMsaUJBQWlCLENBQUMsR0FBVztJQUNsQyxJQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzFDLElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTtRQUNqQixJQUFNLFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUMsT0FBTyxZQUFLLE9BQUEsT0FBQSxPQUFPLENBQUMsR0FBRywwQ0FBRSxXQUFXLFFBQU8sR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFBLEVBQUEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3ZHLE9BQU8sWUFBWSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7S0FDaEU7QUFDTCxDQUFDO0FBRUQscUZBQXFGO0FBQ3JGLFNBQVMsa0JBQWtCLENBQUMsS0FBYTtJQUNyQyxtQ0FBbUM7SUFDbkMsNENBQTRDO0lBQzVDLDJFQUEyRTtJQUMzRSxpREFBaUQ7SUFDakQsV0FBVztJQUNYLG9CQUFvQjtJQUNwQixJQUFJO0lBQ0osT0FBTyxLQUFLLENBQUM7QUFDakIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBkeERhdGFHcmlkIGZyb20gJ2RldmV4dHJlbWUvdWkvZGF0YV9ncmlkJztcclxuaW1wb3J0IG5vdGlmeSBmcm9tICdkZXZleHRyZW1lL3VpL25vdGlmeSc7XHJcbmltcG9ydCB7IEFwcFRvYXN0T3B0aW9ucyB9IGZyb20gJy4uL2NvbXBvbmVudHMvY29udHJhY3RzL2RldmV4dHJlbWVXaWRnZXRzJztcclxuaW1wb3J0IHsgUEJJR3JpZENvbHVtbiB9IGZyb20gJy4uL2NvbXBvbmVudHMvY29udHJhY3RzL2dyaWQnO1xyXG5pbXBvcnQgeyBTZWN1cml0eU1hc3RlckFjdGlvbiB9IGZyb20gJy4vZW51bXMnO1xyXG5pbXBvcnQgeyBhY3Rpb25UeXBlcyB9IGZyb20gJy4vY29uc3RhbnRzJztcclxuXHJcbi8vIHRoaXMgaXMgZHVwbGljYXRlIGZ1bmN0aW9uIHNwZWNpZmljIHRvIG1vZGVscy5cclxuZXhwb3J0IGZ1bmN0aW9uIGdldFN0b3JhZ2VLZXkoZ3JpZEluc3RhbmNlOiBkeERhdGFHcmlkLCBrZXlWYWx1ZTogc3RyaW5nLCBpc01hc3RlckdyaWQgPSB0cnVlKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IG1hc3RlckRldGFpbEluZm8gPSBncmlkSW5zdGFuY2U/Lm9wdGlvbignbWFzdGVyRGV0YWlsJyk7XHJcbiAgICByZXR1cm4gaXNNYXN0ZXJHcmlkID8ga2V5VmFsdWUgOiBgJHtrZXlWYWx1ZX1fJHttYXN0ZXJEZXRhaWxJbmZvLnRlbXBsYXRlfWA7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRDbGFzc05hbWVCeVRoZW1lTmFtZSh0aGVtZU5hbWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBzd2l0Y2ggKHRoZW1lTmFtZSkge1xyXG4gICAgICAgIGNhc2UgJ25wLmNvbXBhY3QnOlxyXG4gICAgICAgIGNhc2UgJ2R4LXN3YXRjaC1kZWZhdWx0JzpcclxuICAgICAgICAgICAgcmV0dXJuICdkeC1zd2F0Y2gtZGVmYXVsdCc7XHJcbiAgICAgICAgY2FzZSAnbGlnaHQuY29tcGFjdCc6XHJcbiAgICAgICAgY2FzZSAnZHgtc3dhdGNoLXJlZ3VsYXInOlxyXG4gICAgICAgICAgICByZXR1cm4gJ2R4LXN3YXRjaC1yZWd1bGFyJztcclxuICAgICAgICBjYXNlICdsaWdodC5yZWd1bGFyJzpcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBhcHBseUZpbHRlckNzc0NsYXNzKGZpbHRlciA9IFtdLCBncmlkSW5zdGFuY2UpOiB2b2lkIHtcclxuICAgIGxldCBpOiBudW1iZXIsIGNzc0NsYXNzZXM6IHN0cmluZywgY29sdW1uSW5mbzogUEJJR3JpZENvbHVtbjtcclxuICAgIGZvciAoaSA9IDA7IGZpbHRlciAmJiBpIDwgZmlsdGVyLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZmlsdGVyW2ldKSkge1xyXG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShmaWx0ZXJbaV1bMF0pKSB7XHJcbiAgICAgICAgICAgICAgICBhcHBseUZpbHRlckNzc0NsYXNzKGZpbHRlcltpXSwgZ3JpZEluc3RhbmNlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbHVtbkluZm8gPSBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXVswXSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoY29sdW1uSW5mbyAmJiAoY29sdW1uSW5mby5kYXRhRmllbGQgPT09IGZpbHRlcltpXVswXSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBjc3NDbGFzc2VzID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV1bMF0sICdjc3NDbGFzcycpO1xyXG4gICAgICAgICAgICAgICAgICAgIGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldWzBdLCAnY3NzQ2xhc3MnLCBjc3NDbGFzc2VzICsgJyBmaWx0ZXJBcHBsaWVkJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb2x1bW5JbmZvID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV0pO1xyXG4gICAgICAgICAgICBpZiAoY29sdW1uSW5mbyAmJiAoY29sdW1uSW5mby5kYXRhRmllbGQgPT09IGZpbHRlcltpXSkpIHtcclxuICAgICAgICAgICAgICAgIGNzc0NsYXNzZXMgPSBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXSwgJ2Nzc0NsYXNzJyk7XHJcbiAgICAgICAgICAgICAgICBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXSwgJ2Nzc0NsYXNzJywgY3NzQ2xhc3NlcyA/IGNzc0NsYXNzZXMgKyAnIGZpbHRlckFwcGxpZWQnIDogJ2ZpbHRlckFwcGxpZWQnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGFwcGx5RmlsdGVyc1RvR3JpZChncmlkSW5zdGFuY2U6IGR4RGF0YUdyaWQsIGZpbHRlclZhbHVlPzogQXJyYXk8YW55Pik6IHZvaWQge1xyXG4gICAgaWYgKGdyaWRJbnN0YW5jZSkge1xyXG4gICAgICAgIC8vIGNvbnN0IGxpc3RPZlZpc2libGVDb2x1bW5zID0gZ3JpZEluc3RhbmNlLmdldFZpc2libGVDb2x1bW5zKCk7XHJcbiAgICAgICAgLy8gbGV0IGNzc0NsYXNzZXM6IHN0cmluZztcclxuICAgICAgICAvLyBmb3IgKGxldCBpID0gMDsgaSA8IGxpc3RPZlZpc2libGVDb2x1bW5zLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgLy8gICAgIGNzc0NsYXNzZXMgPSBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGxpc3RPZlZpc2libGVDb2x1bW5zW2ldLmRhdGFGaWVsZCwgJ2Nzc0NsYXNzJyk7XHJcbiAgICAgICAgLy8gICAgIGlmIChjc3NDbGFzc2VzKSB7XHJcbiAgICAgICAgLy8gICAgICAgICBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGxpc3RPZlZpc2libGVDb2x1bW5zW2ldLmRhdGFGaWVsZCwgJ2Nzc0NsYXNzJywgY3NzQ2xhc3Nlcy5yZXBsYWNlKG5ldyBSZWdFeHAoJ2ZpbHRlckFwcGxpZWQnLCAnZycpLCAnJykudHJpbSgpKTtcclxuICAgICAgICAvLyAgICAgfVxyXG4gICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgaWYgKGZpbHRlclZhbHVlKSB7XHJcbiAgICAgICAgICAgIC8vIGFwcGx5RmlsdGVyQ3NzQ2xhc3MoZmlsdGVyVmFsdWUsIGdyaWRJbnN0YW5jZSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGFsZXJ0T3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHR5cGU6ICd3YXJuaW5nJywgbWVzc2FnZTogJ1RvIHBlcm1hbmVudGx5IGFwcGx5IHRoaXMgZmlsdGVyLHBsZWFzZSBnbyB0byB0aGUgXFwnVmlldyBTZWxlY3Rpb25cXCcgYW5kIGNsaWNrIFxcJ1NhdmVcXCcnXHJcbiAgICAgICAgICAgICAgICAgICAgKyAnIGluIG9yZGVyIHRvIGFzc29jaWF0ZSB0aGlzIGZpbHRlciB3aXRoIGEgdmlldy4nXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIGFwcFRvYXN0KGFsZXJ0T3B0aW9ucyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gYXBwVG9hc3Qob3B0aW9uczogQXBwVG9hc3RPcHRpb25zKTogdm9pZCB7XHJcbiAgICBjb25zdCBkZWZhdWx0T3B0aW9ucyA9IHtcclxuICAgICAgICBtZXNzYWdlOiAnT29wcy5Tb21ldGhpbmcgd2VudCB3cm9uZyAhJyxcclxuICAgICAgICBjbG9zZU9uQ2xpY2s6IHRydWUsXHJcbiAgICAgICAgd2lkdGg6ICc1NTBweCcsXHJcbiAgICAgICAgdHlwZTogJ2Vycm9yJyxcclxuICAgICAgICBkaXNwbGF5VGltZTogMzAwMCxcclxuICAgICAgICBwb3NpdGlvbjoge1xyXG4gICAgICAgICAgICBteTogJ2NlbnRlciB0b3AnLFxyXG4gICAgICAgICAgICBhdDogJ2NlbnRlciB0b3AnXHJcbiAgICAgICAgfSxcclxuICAgIH07XHJcbiAgICAvLyAqIGV4dGVuZCB0aGUgYmVsb3cgc3dpdGNoIGZvciBvdGhlciB0b2FzdChub3RpZnkpIHR5cGVzXHJcbiAgICBpZiAodHlwZW9mIG9wdGlvbnMudHlwZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICBzd2l0Y2ggKG9wdGlvbnMudHlwZS50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3dhcm5pbmcnOlxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdE9wdGlvbnMuZGlzcGxheVRpbWUgPSAxMDAwMDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgZGVmYXVsdE9wdGlvbnMuZGlzcGxheVRpbWUgPSAzMDAwO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCBkZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XHJcbiAgICBub3RpZnkob3B0aW9ucyk7XHJcbn1cclxuXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gVG9EaWN0aW9uYXJ5PFQ+KGFycmF5OiBUW10sIGtleTogKGVsZW1lbnQ6IFQpID0+IHN0cmluZyB8IG51bWJlcik6IHsgW2tleTogc3RyaW5nXTogVCB9IHtcclxuICAgIGNvbnN0IGRpYzogeyBba2V5OiBzdHJpbmddOiBUIH0gPSB7fTtcclxuICAgIGFycmF5LmZvckVhY2goZSA9PiB7XHJcbiAgICAgICAgY29uc3QgayA9IGtleShlKTtcclxuICAgICAgICBpZiAoIWRpYy5oYXNPd25Qcm9wZXJ0eShrKSkge1xyXG4gICAgICAgICAgICBkaWNba2V5KGUpXSA9IGU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKCdEdXBsaWNhdGUgS2V5IGluIEFycmF5JywgZSk7XHJcbiAgICAgICAgfTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIGRpYztcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGZpbHRlclNlbGVjdGVkUm93RGF0YShncmlkSW5zdGFuY2UsIHJvd0RhdGE/OiBhbnkpOiBBcnJheTxhbnk+IHtcclxuICAgIGNvbnN0IHZpc2libGVDb2x1bW5zID0gZ3JpZEluc3RhbmNlLmdldFZpc2libGVDb2x1bW5zKCkubWFwKChpdGVtKSA9PiB7XHJcbiAgICAgICAgcmV0dXJuIGl0ZW1bJ2RhdGFGaWVsZCddO1xyXG4gICAgfSk7XHJcbiAgICBsZXQgc2VsZWN0ZWRSb3dEYXRhID0gW107XHJcbiAgICBpZiAocm93RGF0YSAmJiBPYmplY3Qua2V5cyhyb3dEYXRhKS5sZW5ndGggJiYgT2JqZWN0LmtleXMocm93RGF0YS5kYXRhKS5sZW5ndGgpIHtcclxuICAgICAgICBzZWxlY3RlZFJvd0RhdGEucHVzaChKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHJvd0RhdGEuZGF0YSkpKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2VsZWN0ZWRSb3dEYXRhID0gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShncmlkSW5zdGFuY2UuZ2V0U2VsZWN0ZWRSb3dzRGF0YSgpKSk7XHJcbiAgICB9XHJcbiAgICBpZiAoc2VsZWN0ZWRSb3dEYXRhLmxlbmd0aCkge1xyXG4gICAgICAgIGZvciAoY29uc3QgY29sdW1uUHJvcGVydHkgaW4gc2VsZWN0ZWRSb3dEYXRhWzBdKSB7XHJcbiAgICAgICAgICAgIGlmICghdmlzaWJsZUNvbHVtbnMuaW5jbHVkZXMoY29sdW1uUHJvcGVydHkpKSB7XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgc2VsZWN0ZWRSb3dEYXRhWzBdW2NvbHVtblByb3BlcnR5XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBzZWxlY3RlZFJvd0RhdGE7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBvcGVuRGVzdGluYXRpb24oZGF0YSwgc2VsZWN0ZWRSb3dEYXRhOiBhbnksIGVudGl0eVBhcmFtZXRlckluZm86IGFueVtdID0gW10sIGFwcFNlcnZpY2UgPSBudWxsKTogYm9vbGVhbiB7XHJcbiAgICBsZXQgdXJsRHluYW1pY1BhcmFtQ291bnQgPSAwLCBwYXJhbUZpbHRlciA9IFtdLCB1cmw6IHN0cmluZywgdXJsUGFyYW1zLCBwYXJhbXRlck1hcHBpbmc6IGFueVtdLCBkZWxpbWl0ZXI6IHN0cmluZywgZGVzdGluYXRpb25Vcmw6IHN0cmluZywgc3RhdGljUGFyYW1zOiBzdHJpbmc7XHJcbiAgICBsZXQgdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMgPSAnJztcclxuICAgIGNvbnN0IGNvbW1hID0gJywnO1xyXG5cclxuICAgIHN3aXRjaCAoZGF0YS5EZXN0aW5hdGlvblR5cGU/LnRvTG93ZXJDYXNlKCkudHJpbSgpKSB7XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5hZGRTZWN1cml0eS50b0xvd2VyQ2FzZSgpOlxyXG4gICAgICAgICAgICBpZiAoYXBwU2VydmljZSkge1xyXG4gICAgICAgICAgICAgICAgYXBwU2VydmljZS5zaG93U00oeyBTZWN1cml0eUlkOiBudWxsLCBBY3Rpb246IFNlY3VyaXR5TWFzdGVyQWN0aW9uLkFkZFNlY3VyaXR5IH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLmRpc3BhdGNoZXJKb2IudG9Mb3dlckNhc2UoKTpcclxuICAgICAgICAgICAgdXJsID0gYCR7bG9jYXRpb24uaHJlZi5zcGxpdCgnIycpWzBdfSMvRGlzcGF0Y2hlck1hbmFnZW1lbnQ/d2ViTWVudUlkPSR7Z2V0V2ViTWVudUlkQnlLZXkoJ0Rpc3BhdGNoZXJNYW5hZ2VtZW50Jyl9JmpvYklkPSR7ZGF0YS5qb2JJZH1gO1xyXG4gICAgICAgICAgICBvcGVuKHVybCwgJ19ibGFuaycpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5sb29rdXAudG9Mb3dlckNhc2UoKTpcclxuICAgICAgICAgICAgdXJsID0gYCR7bG9jYXRpb24uaHJlZi5zcGxpdCgnIycpWzBdfSMvTG9va3VwVGFibGVzP3dlYk1lbnVJZD0ke2dldFdlYk1lbnVJZEJ5S2V5KCdMb29rdXBUYWJsZXMnKX0mbG9va3VwSWQ9JHtkYXRhLkxvb2t1cElkfWA7XHJcbiAgICAgICAgICAgIG9wZW4odXJsLCAnX2JsYW5rJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLmVudGl0eS50b0xvd2VyQ2FzZSgpOlxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuZWRpdFNlY3VyaXR5LnRvTG93ZXJDYXNlKCk6XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy51cmwudG9Mb3dlckNhc2UoKTpcclxuICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93RGF0YSAmJiBzZWxlY3RlZFJvd0RhdGEubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKGRhdGEuRGVzdGluYXRpb25UeXBlPy50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5lZGl0U2VjdXJpdHkudG9Mb3dlckNhc2UoKTpcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuU2hvd0FzQ29udGV4dE1lbnUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHNlY0tleSA9ICcnO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gc2VsZWN0ZWRSb3dEYXRhWzBdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93RGF0YVswXS5oYXNPd25Qcm9wZXJ0eShrZXkpICYmIChrZXkudG9Mb3dlckNhc2UoKSA9PT0gJ3NlY3VyaXR5aWQnIHx8IGtleS50b0xvd2VyQ2FzZSgpID09PSAnc2VjdXJpdHkgaWQnKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZWNLZXkgPSBrZXk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93RGF0YVswXVtzZWNLZXldICYmIGFwcFNlcnZpY2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHBTZXJ2aWNlLnNob3dTTSh7IFNlY3VyaXR5SWQ6IHNlbGVjdGVkUm93RGF0YVswXVtzZWNLZXldLCBBY3Rpb246IFNlY3VyaXR5TWFzdGVyQWN0aW9uLkVkaXRTZWN1cml0eSB9KVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICdlcnJvcicsIG1lc3NhZ2U6IGBGb2xsb3dpbmcgY29sdW1uKHMpIFNlY3VyaXR5IElkIGlzIHJlcXVpcmVkLiBQbGVhc2UgbWFrZSBzdXJlIHNhbWUgZXhpc3QocykgaW4gdGhlIGdyaWQgd2l0aCB2YWx1ZS5gLCBkaXNwbGF5VGltZTogNDAwMCB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5lbnRpdHkudG9Mb3dlckNhc2UoKTpcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcXVlcnlQYXJhbXMgPSBsb2NhdGlvbi5ocmVmLnNsaWNlKHdpbmRvdy5sb2NhdGlvbi5ocmVmLmluZGV4T2YoJz8nKSArIDEpLnNwbGl0KCcmJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXJ5UGFyYW1zWydlbnRpdHknXSA9IGRhdGEuRGVzdGluYXRpb25FbnRpdHk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXJ5UGFyYW1zWyd3ZWJNZW51SWQnXSA9IGRhdGEuRGVzdGluYXRpb25XZWJNZW51SXRlbUlkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWVyeVBhcmFtc1snbGF5b3V0J10gPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWVyeVBhcmFtc1snZ3JpZG5hbWUnXSA9IGRhdGEuRGVzdGluYXRpb25FbnRpdHlOYW1lICsgJ18nICsgZGF0YS5EZXN0aW5hdGlvbkVudGl0eTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtdGVyTWFwcGluZyA9IGRhdGEuUGFyYW1ldGVyTWFwcGluZztcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcGFyYW1JbmZvID0gZXh0cmFjdERlc3RpbmF0aW9uRW50aXR5UGFyYW1ldGVycyhzZWxlY3RlZFJvd0RhdGEsIHBhcmFtdGVyTWFwcGluZywgZW50aXR5UGFyYW1ldGVySW5mbyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzID0gcGFyYW1JbmZvLnVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1cmwgPSBsb2NhdGlvbi5oYXNoLnNwbGl0KCc/JylbMF0gKyAnP3dlYk1lbnVJZD0nICsgcXVlcnlQYXJhbXNbJ3dlYk1lbnVJZCddICsgJyZlbnRpdHk9JyArIHF1ZXJ5UGFyYW1zWydlbnRpdHknXSArXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnJmdyaWRuYW1lPScgKyBxdWVyeVBhcmFtc1snZ3JpZG5hbWUnXSArIHBhcmFtSW5mby5wYXJhbSArICcmbGF5b3V0PScgKyBxdWVyeVBhcmFtc1snbGF5b3V0J107XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgYWN0aW9uVHlwZXMudXJsLnRvTG93ZXJDYXNlKCk6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc3RpbmF0aW9uVXJsID0gZGF0YS5EZXN0aW5hdGlvblVybC5zcGxpdCgnPycpLCBwYXJhbXRlck1hcHBpbmcgPSBkYXRhLlBhcmFtZXRlck1hcHBpbmc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybCA9IGRlc3RpbmF0aW9uVXJsWzBdLCBzdGF0aWNQYXJhbXMgPSBkZXN0aW5hdGlvblVybFsxXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHN0YXRpY1BhcmFtcyAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmwgPSB1cmwgKyAnPycgKyBzdGF0aWNQYXJhbXM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdXJsUGFyYW1zID0gdXJsLm1hdGNoKC97W157fV0qfS9naSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh1cmxQYXJhbXMgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsUGFyYW1zLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXRlck1hcHBpbmcuZm9yRWFjaCgocGFyYW1JdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbUl0ZW0uUGFyYW1ldGVyTmFtZSA9PT0gaXRlbS5zdWJzdHIoMSkuc2xpY2UoMCwgLTEpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1JdGVtLklzU291cmNlUGFyYW0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzb3VyY2VQYXJhbU5hbWUgPSBwYXJhbUl0ZW0uU291cmNlQ29sdW1uTmFtZS5zcGxpdCgnQCcpWzFdLnRyaW0oKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtZXRlciA9IGVudGl0eVBhcmFtZXRlckluZm8uZmlsdGVyKHBhciA9PiBwYXIuTmFtZS50cmltKCkudG9Mb3dlckNhc2UoKSA9PT0gc291cmNlUGFyYW1OYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHx8IHBhci5OYW1lLnRyaW0oKS50b0xvd2VyQ2FzZSgpID09PSBwYXJhbUl0ZW0uU291cmNlQ29sdW1uTmFtZS50cmltKCkudG9Mb3dlckNhc2UoKSlbMF07XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbWV0ZXIuVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsID0gdXJsLnJlcGxhY2UoaXRlbSwgY2hlY2tBbmRGb3JtYXREYXRlKHBhcmFtZXRlci5WYWx1ZSkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzICs9IHBhcmFtSXRlbS5Tb3VyY2VDb2x1bW5OYW1lICsgY29tbWE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRSb3dEYXRhWzBdW2l0ZW0uU291cmNlQ29sdW1uTmFtZV0gfHwgc2VsZWN0ZWRSb3dEYXRhWzBdW3BhcmFtSXRlbS5Tb3VyY2VDb2x1bW5OYW1lXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmwgPSB1cmwucmVwbGFjZShpdGVtLCBjaGVja0FuZEZvcm1hdERhdGUoc2VsZWN0ZWRSb3dEYXRhWzBdW3BhcmFtSXRlbS5Tb3VyY2VDb2x1bW5OYW1lXSkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzICs9IGl0ZW0uU291cmNlQ29sdW1uTmFtZSA/IGl0ZW0uU291cmNlQ29sdW1uTmFtZSArIGNvbW1hIDogaXRlbSArIGNvbW1hO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW10ZXJNYXBwaW5nLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodXJsUGFyYW1zICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYXJhbUZpbHRlciA9IHVybFBhcmFtcy5maWx0ZXIoKHVybEl0ZW0pID0+IGl0ZW0uUGFyYW1ldGVyTmFtZSA9PT0gdXJsSXRlbS5zdWJzdHIoMSkuc2xpY2UoMCwgLTEpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmICghcGFyYW1GaWx0ZXIubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVsaW1pdGVyID0gKHVybER5bmFtaWNQYXJhbUNvdW50ID09PSAwICYmIHN0YXRpY1BhcmFtcyA9PT0gdW5kZWZpbmVkKSA/ICc/JyA6ICcmJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRSb3dEYXRhWzBdW2l0ZW0uU291cmNlQ29sdW1uTmFtZV0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsID0gdXJsICsgZGVsaW1pdGVyICsgaXRlbS5QYXJhbWV0ZXJOYW1lICsgJz0nICsgY2hlY2tBbmRGb3JtYXREYXRlKHNlbGVjdGVkUm93RGF0YVswXVtpdGVtLlNvdXJjZUNvbHVtbk5hbWVdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcyArPSBpdGVtLlNvdXJjZUNvbHVtbk5hbWUgKyBjb21tYTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsRHluYW1pY1BhcmFtQ291bnQrKztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKCF1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcykge1xyXG4gICAgICAgICAgICAgICAgICAgIG9wZW4odXJsLCAnX2JsYW5rJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcy5lbmRzV2l0aChjb21tYSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMgPSB1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcy5zdWJzdHJpbmcoMCwgdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMubGVuZ3RoIC0gMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFwcFRvYXN0KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdlcnJvcicsIG1lc3NhZ2U6IGBGb2xsb3dpbmcgY29sdW1uKHMpICR7dW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXN9IGlzIHJlcXVpcmVkLiBQbGVhc2UgbWFrZSBzdXJlIHNhbWUgZXhpc3QocykgaW4gdGhlIGdyaWQgb3IgcGFyYW1ldGVyIHdpdGggdmFsdWUuYFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLCBkaXNwbGF5VGltZTogNDAwMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICd3YXJuaW5nJywgbWVzc2FnZTogJ1BsZWFzZSBzZWxlY3QgYSByb3cuJyB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZXh0cmFjdERlc3RpbmF0aW9uRW50aXR5UGFyYW1ldGVycyhzZWxlY3RlZFJvd0RhdGE6IGFueVtdLCBwYXJhbWV0ZXJNYXBwaW5nOiBhbnlbXSwgZW50aXR5UGFyYW1ldGVySW5mbzogYW55W10pIHtcclxuICAgIGxldCBwYXJhbSA9ICcnO1xyXG4gICAgbGV0IHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzID0gJyc7XHJcbiAgICBjb25zdCBjb21tYSA9ICcsJztcclxuICAgIHBhcmFtZXRlck1hcHBpbmcuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtTmFtZSA9IGl0ZW0uT3JpZ2luYWxQYXJhbWV0ZXJOYW1lIHx8IGl0ZW0uUGFyYW1ldGVyTmFtZTtcclxuICAgICAgICBpZiAoaXRlbS5Jc1NvdXJjZVBhcmFtKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHNvdXJjZVBhcmFtTmFtZSA9IGl0ZW0uU291cmNlQ29sdW1uTmFtZS5zcGxpdCgnQCcpWzFdLnRyaW0oKS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSBlbnRpdHlQYXJhbWV0ZXJJbmZvLmZpbHRlcihwYXIgPT4gcGFyLk5hbWUudHJpbSgpLnRvTG93ZXJDYXNlKCkgPT09IHNvdXJjZVBhcmFtTmFtZVxyXG4gICAgICAgICAgICAgICAgfHwgcGFyLk5hbWUudHJpbSgpLnRvTG93ZXJDYXNlKCkgPT09IGl0ZW0uU291cmNlQ29sdW1uTmFtZS50cmltKCkudG9Mb3dlckNhc2UoKSlbMF07XHJcblxyXG4gICAgICAgICAgICBpZiAocGFyYW1ldGVyPy5WYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgcGFyYW0gPSBwYXJhbSArICcmJyArIHBhcmFtTmFtZSArICc9JyArIGNoZWNrQW5kRm9ybWF0RGF0ZShwYXJhbWV0ZXIuVmFsdWUpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMgKz0gaXRlbS5Tb3VyY2VDb2x1bW5OYW1lICsgY29tbWE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRSb3dEYXRhWzBdW2l0ZW0uU291cmNlQ29sdW1uTmFtZV0pIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtID0gcGFyYW0gKyAnJicgKyBwYXJhbU5hbWUgKyAnPScgKyBjaGVja0FuZEZvcm1hdERhdGUoc2VsZWN0ZWRSb3dEYXRhWzBdW2l0ZW0uU291cmNlQ29sdW1uTmFtZV0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMgKz0gaXRlbS5Tb3VyY2VDb2x1bW5OYW1lICsgY29tbWE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4geyBwYXJhbSwgdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGNvbnZlcnREYXRlVG9Vc0Zvcm1hdCh2YWx1ZTogRGF0ZSB8IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBkYXRlVmFsdWUgPSBuZXcgRGF0ZSh2YWx1ZSk7XHJcbiAgICBsZXQgZGQ6IG51bWJlciB8IHN0cmluZyA9IGRhdGVWYWx1ZS5nZXREYXRlKCk7IGxldCBtbTogbnVtYmVyIHwgc3RyaW5nID0gZGF0ZVZhbHVlLmdldE1vbnRoKCkgKyAxOyBjb25zdCB5eXl5ID0gZGF0ZVZhbHVlLmdldEZ1bGxZZWFyKCk7XHJcbiAgICBkZCA9IGRkIDwgMTAgPyAnMCcgKyBkZCA6IGRkO1xyXG4gICAgbW0gPSBtbSA8IDEwID8gJzAnICsgbW0gOiBtbTtcclxuICAgIHJldHVybiBgJHttbX0vJHtkZH0vJHt5eXl5fWA7XHJcbn1cclxuXHJcblxyXG4vKipcclxuICogdGhpcyBtZXRob2Qgd2lsbCByZXR1cm4gd2ViIG1lbnUgaWQgYmFzZWQgb24ga2V5XHJcbiAqIEBwYXJhbSAge3N0cmluZ30ga2V5XHJcbiAqIEByZXR1cm5zIG51bWJlclxyXG4gKi9cclxuZnVuY3Rpb24gZ2V0V2ViTWVudUlkQnlLZXkoa2V5OiBzdHJpbmcpOiBudW1iZXIge1xyXG4gICAgY29uc3Qgd2ViTWVudXMgPSB3aW5kb3dbJ21lbnVEYXRhJ10gfHwgW107XHJcbiAgICBpZiAod2ViTWVudXMubGVuZ3RoKSB7XHJcbiAgICAgICAgY29uc3QgZmlsdGVyZWRJdGVtID0gd2ViTWVudXMuZmlsdGVyKChhcnJJdGVtKSA9PiBhcnJJdGVtLktleT8udG9Mb3dlckNhc2UoKSA9PT0ga2V5LnRvTG93ZXJDYXNlKCkpWzBdO1xyXG4gICAgICAgIHJldHVybiBmaWx0ZXJlZEl0ZW0gPyBmaWx0ZXJlZEl0ZW0uT3B0b21hc1Rvb2xJZCA6IHVuZGVmaW5lZDtcclxuICAgIH1cclxufVxyXG5cclxuLy8gY29udmVydCBmcm9tIFlZWVktTU0tRERUSEg6bW06c3MuU1NTIHRvIE1NL0REL1lZWVksIGVsc2UgcmV0dXJuIHRoZSBwYXJhbSBhcyBpdCBpc1xyXG5mdW5jdGlvbiBjaGVja0FuZEZvcm1hdERhdGUodmFsdWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAvLyBjb25zdCBtb21lbnQgPSB3aW5kb3dbJ21vbWVudCddO1xyXG4gICAgLy8gLy8gcmVmOiBodHRwczovL2ZsYXZpb2NvcGVzLmNvbS9tb21lbnRqcy9cclxuICAgIC8vIGlmIChtb21lbnQodmFsdWUsIG1vbWVudC5IVE1MNV9GTVQuREFURVRJTUVfTE9DQUxfTVMsIHRydWUpLmlzVmFsaWQoKSkge1xyXG4gICAgLy8gICAgIHJldHVybiBtb21lbnQodmFsdWUpLmZvcm1hdCgnTU0vREQvWVlZWScpO1xyXG4gICAgLy8gfSBlbHNlIHtcclxuICAgIC8vICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICAvLyB9XHJcbiAgICByZXR1cm4gdmFsdWU7XHJcbn1cclxuXHJcbiJdfQ==