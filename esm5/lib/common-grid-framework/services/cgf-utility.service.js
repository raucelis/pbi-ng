import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { appToast } from '../utilities/utilityFunctions';
import * as i0 from "@angular/core";
var CGFUtilityService = /** @class */ (function () {
    function CGFUtilityService() {
    }
    CGFUtilityService.prototype.applyFiltersToGrid = function (gridInstance, filterValue) {
        if (gridInstance) {
            var listOfVisibleColumns = gridInstance.getVisibleColumns();
            var cssClasses = void 0;
            for (var i = 0; i < listOfVisibleColumns.length; i++) {
                cssClasses = gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass');
                if (cssClasses) {
                    gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass', cssClasses.replace(new RegExp('filterApplied', 'g'), '').trim());
                }
            }
            if (filterValue) {
                this.applyFilterClass(filterValue, gridInstance);
                var alertOptions = {
                    type: 'warning', message: 'To permanently apply this filter,please go to the \'View Selection\' and click \'Save\''
                        + ' in order to associate this filter with a view.'
                };
                appToast(alertOptions);
            }
        }
    };
    CGFUtilityService.prototype.applyFilterClass = function (filter, gridInstance) {
        if (filter === void 0) { filter = []; }
        var i;
        var cssClasses;
        var columnInfo;
        for (i = 0; filter && i < filter.length; i++) {
            if (Array.isArray(filter[i])) {
                if (Array.isArray(filter[i][0])) {
                    this.applyFilterClass(filter[i], gridInstance);
                }
                else {
                    columnInfo = gridInstance.columnOption(filter[i][0]);
                    if (columnInfo && (columnInfo.dataField === filter[i][0])) {
                        cssClasses = gridInstance.columnOption(filter[i][0], 'cssClass');
                        gridInstance.columnOption(filter[i][0], 'cssClass', cssClasses + ' filterApplied');
                    }
                }
            }
            else {
                columnInfo = gridInstance.columnOption(filter[i]);
                if (columnInfo && (columnInfo.dataField === filter[i])) {
                    cssClasses = gridInstance.columnOption(filter[i], 'cssClass');
                    gridInstance.columnOption(filter[i], 'cssClass', cssClasses ? cssClasses + ' filterApplied' : 'filterApplied');
                }
            }
        }
    };
    CGFUtilityService.prototype.getStorageKey = function (gridInstance, keyValue, isMasterGrid) {
        if (isMasterGrid === void 0) { isMasterGrid = true; }
        var masterDetailInfo = gridInstance === null || gridInstance === void 0 ? void 0 : gridInstance.option('masterDetail');
        return isMasterGrid ? keyValue : keyValue + "_" + masterDetailInfo.template;
    };
    CGFUtilityService.ɵprov = i0.ɵɵdefineInjectable({ factory: function CGFUtilityService_Factory() { return new CGFUtilityService(); }, token: CGFUtilityService, providedIn: "root" });
    CGFUtilityService = __decorate([
        Injectable({ providedIn: 'root' })
    ], CGFUtilityService);
    return CGFUtilityService;
}());
export { CGFUtilityService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2dmLXV0aWxpdHkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvc2VydmljZXMvY2dmLXV0aWxpdHkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sK0JBQStCLENBQUM7O0FBR3pEO0lBRUU7SUFBZ0IsQ0FBQztJQUVWLDhDQUFrQixHQUF6QixVQUEwQixZQUFpQixFQUFFLFdBQWlCO1FBQzVELElBQUksWUFBWSxFQUFFO1lBQ2hCLElBQU0sb0JBQW9CLEdBQUcsWUFBWSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDOUQsSUFBSSxVQUFVLFNBQVEsQ0FBQztZQUN2QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsb0JBQW9CLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwRCxVQUFVLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ3RGLElBQUksVUFBVSxFQUFFO29CQUNkLFlBQVksQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUN6RCxVQUFVLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxlQUFlLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDaEY7YUFDRjtZQUVELElBQUksV0FBVyxFQUFFO2dCQUNmLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ2pELElBQU0sWUFBWSxHQUFHO29CQUNuQixJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSx5RkFBeUY7MEJBQy9HLGlEQUFpRDtpQkFDdEQsQ0FBQztnQkFDRixRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDeEI7U0FDRjtJQUNILENBQUM7SUFFTSw0Q0FBZ0IsR0FBdkIsVUFBd0IsTUFBVyxFQUFFLFlBQVk7UUFBekIsdUJBQUEsRUFBQSxXQUFXO1FBQ2pDLElBQUksQ0FBUyxDQUFDO1FBQUMsSUFBSSxVQUFrQixDQUFDO1FBQUMsSUFBSSxVQUF5QixDQUFDO1FBQ3JFLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxNQUFNLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDNUMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUM1QixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQy9CLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUM7aUJBQ2hEO3FCQUFNO29CQUNMLFVBQVUsR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUNyRCxJQUFJLFVBQVUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQ3pELFVBQVUsR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDakUsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxFQUFFLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDO3FCQUNwRjtpQkFDRjthQUNGO2lCQUFNO2dCQUNMLFVBQVUsR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNsRCxJQUFJLFVBQVUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7b0JBQ3RELFVBQVUsR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztvQkFDOUQsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsQ0FBQztpQkFDaEg7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVNLHlDQUFhLEdBQXBCLFVBQXFCLFlBQXdCLEVBQUUsUUFBZ0IsRUFBRSxZQUFtQjtRQUFuQiw2QkFBQSxFQUFBLG1CQUFtQjtRQUNsRixJQUFNLGdCQUFnQixHQUFHLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDOUQsT0FBTyxZQUFZLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUksUUFBUSxTQUFJLGdCQUFnQixDQUFDLFFBQVUsQ0FBQztJQUM5RSxDQUFDOztJQXJEVSxpQkFBaUI7UUFEN0IsVUFBVSxDQUFDLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxDQUFDO09BQ3RCLGlCQUFpQixDQXNEN0I7NEJBNUREO0NBNERDLEFBdERELElBc0RDO1NBdERZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUEJJR3JpZENvbHVtbiB9IGZyb20gJy4uL2NvbXBvbmVudHMvY29udHJhY3RzL2dyaWQnO1xyXG5pbXBvcnQgZHhEYXRhR3JpZCBmcm9tICdkZXZleHRyZW1lL3VpL2RhdGFfZ3JpZCc7XHJcbmltcG9ydCB7IGFwcFRvYXN0IH0gZnJvbSAnLi4vdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMnO1xyXG5cclxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcclxuZXhwb3J0IGNsYXNzIENHRlV0aWxpdHlTZXJ2aWNlIHtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgcHVibGljIGFwcGx5RmlsdGVyc1RvR3JpZChncmlkSW5zdGFuY2U6IGFueSwgZmlsdGVyVmFsdWU/OiBhbnkpOiB2b2lkIHtcclxuICAgIGlmIChncmlkSW5zdGFuY2UpIHtcclxuICAgICAgY29uc3QgbGlzdE9mVmlzaWJsZUNvbHVtbnMgPSBncmlkSW5zdGFuY2UuZ2V0VmlzaWJsZUNvbHVtbnMoKTtcclxuICAgICAgbGV0IGNzc0NsYXNzZXM6IHN0cmluZztcclxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBsaXN0T2ZWaXNpYmxlQ29sdW1ucy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGNzc0NsYXNzZXMgPSBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGxpc3RPZlZpc2libGVDb2x1bW5zW2ldLmRhdGFGaWVsZCwgJ2Nzc0NsYXNzJyk7XHJcbiAgICAgICAgaWYgKGNzc0NsYXNzZXMpIHtcclxuICAgICAgICAgIGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24obGlzdE9mVmlzaWJsZUNvbHVtbnNbaV0uZGF0YUZpZWxkLFxyXG4gICAgICAgICAgICAnY3NzQ2xhc3MnLCBjc3NDbGFzc2VzLnJlcGxhY2UobmV3IFJlZ0V4cCgnZmlsdGVyQXBwbGllZCcsICdnJyksICcnKS50cmltKCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKGZpbHRlclZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5hcHBseUZpbHRlckNsYXNzKGZpbHRlclZhbHVlLCBncmlkSW5zdGFuY2UpO1xyXG4gICAgICAgIGNvbnN0IGFsZXJ0T3B0aW9ucyA9IHtcclxuICAgICAgICAgIHR5cGU6ICd3YXJuaW5nJywgbWVzc2FnZTogJ1RvIHBlcm1hbmVudGx5IGFwcGx5IHRoaXMgZmlsdGVyLHBsZWFzZSBnbyB0byB0aGUgXFwnVmlldyBTZWxlY3Rpb25cXCcgYW5kIGNsaWNrIFxcJ1NhdmVcXCcnXHJcbiAgICAgICAgICAgICsgJyBpbiBvcmRlciB0byBhc3NvY2lhdGUgdGhpcyBmaWx0ZXIgd2l0aCBhIHZpZXcuJ1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgYXBwVG9hc3QoYWxlcnRPcHRpb25zKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGFwcGx5RmlsdGVyQ2xhc3MoZmlsdGVyID0gW10sIGdyaWRJbnN0YW5jZSk6IHZvaWQge1xyXG4gICAgbGV0IGk6IG51bWJlcjsgbGV0IGNzc0NsYXNzZXM6IHN0cmluZzsgbGV0IGNvbHVtbkluZm86IFBCSUdyaWRDb2x1bW47XHJcbiAgICBmb3IgKGkgPSAwOyBmaWx0ZXIgJiYgaSA8IGZpbHRlci5sZW5ndGg7IGkrKykge1xyXG4gICAgICBpZiAoQXJyYXkuaXNBcnJheShmaWx0ZXJbaV0pKSB7XHJcbiAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZmlsdGVyW2ldWzBdKSkge1xyXG4gICAgICAgICAgdGhpcy5hcHBseUZpbHRlckNsYXNzKGZpbHRlcltpXSwgZ3JpZEluc3RhbmNlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY29sdW1uSW5mbyA9IGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldWzBdKTtcclxuICAgICAgICAgIGlmIChjb2x1bW5JbmZvICYmIChjb2x1bW5JbmZvLmRhdGFGaWVsZCA9PT0gZmlsdGVyW2ldWzBdKSkge1xyXG4gICAgICAgICAgICBjc3NDbGFzc2VzID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV1bMF0sICdjc3NDbGFzcycpO1xyXG4gICAgICAgICAgICBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXVswXSwgJ2Nzc0NsYXNzJywgY3NzQ2xhc3NlcyArICcgZmlsdGVyQXBwbGllZCcpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb2x1bW5JbmZvID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV0pO1xyXG4gICAgICAgIGlmIChjb2x1bW5JbmZvICYmIChjb2x1bW5JbmZvLmRhdGFGaWVsZCA9PT0gZmlsdGVyW2ldKSkge1xyXG4gICAgICAgICAgY3NzQ2xhc3NlcyA9IGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldLCAnY3NzQ2xhc3MnKTtcclxuICAgICAgICAgIGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldLCAnY3NzQ2xhc3MnLCBjc3NDbGFzc2VzID8gY3NzQ2xhc3NlcyArICcgZmlsdGVyQXBwbGllZCcgOiAnZmlsdGVyQXBwbGllZCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldFN0b3JhZ2VLZXkoZ3JpZEluc3RhbmNlOiBkeERhdGFHcmlkLCBrZXlWYWx1ZTogc3RyaW5nLCBpc01hc3RlckdyaWQgPSB0cnVlKTogc3RyaW5nIHtcclxuICAgIGNvbnN0IG1hc3RlckRldGFpbEluZm8gPSBncmlkSW5zdGFuY2U/Lm9wdGlvbignbWFzdGVyRGV0YWlsJyk7XHJcbiAgICByZXR1cm4gaXNNYXN0ZXJHcmlkID8ga2V5VmFsdWUgOiBgJHtrZXlWYWx1ZX1fJHttYXN0ZXJEZXRhaWxJbmZvLnRlbXBsYXRlfWA7XHJcbiAgfVxyXG59XHJcbiJdfQ==