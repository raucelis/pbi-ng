import dxDataGrid from 'devextreme/ui/data_grid';
import { CGFFlyOutEnum } from '../../utilities/enums';
import { OnInit, EventEmitter } from '@angular/core';
import { DataBrowserEntityParametersEnvelope, FlyOutSection } from '../contracts/common-grid-framework';
export declare class FlyOutActionIconContainerComponent implements OnInit {
    dataBrowserEntityParameters: DataBrowserEntityParametersEnvelope[];
    disableRefreshIcon: boolean;
    gridInstance: dxDataGrid;
    position: 'right' | 'left';
    showLoader: boolean;
    showParametersControls: boolean;
    visibleFlyOuts: CGFFlyOutEnum[];
    set resetContainerSettings(val: boolean);
    set selectedFlyOut(val: {
        id: number;
    });
    get selectedFlyOut(): {
        id: number;
    };
    set cgfLeftMenuList(data: any[]);
    get cgfLeftMenuList(): any[];
    flyOutSelectionClick: any;
    refreshEntityData: EventEmitter<any>;
    activeFlyOut: {
        id: number;
    };
    cgfLeftMenuDropDownListItems: any[];
    cgfLeftMenus: any[];
    currentFlyOutSection: {
        id: number;
    };
    flyOutSections: Array<FlyOutSection>;
    leftMenuDropDownList: any[];
    leftMenuList: any[];
    constructor();
    ngOnInit(): void;
    onFlyOutIconClick(evt: MouseEvent, item: FlyOutSection): void;
    showParametersControlsContainer(): void;
    refreshData(): void;
    cgfLeftMenuClick(data: any): void;
    onMoreItemClick(args: any): void;
    onFlyOutSelectionClick(event: any, item: FlyOutSection): void;
    private prepareExtraActions;
}
