import { ViewSelectionLayout } from '../contracts/view-selection';
export declare class ViewSelectionModel {
    canDefault: boolean;
    canDelete: boolean;
    canGlobalDefault: boolean;
    canSave: boolean;
    canSaveNew: boolean;
    id: number;
    isDefault: boolean;
    isGlobalDefault: boolean;
    isPinned: boolean;
    isPublic: boolean;
    name: string;
    constructor(args?: ViewSelectionLayout);
}
