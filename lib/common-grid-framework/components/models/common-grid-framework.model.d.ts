import { CGFFlyOutEnum, CGFSettingsEnum } from '../../utilities/enums';
import { CommonGridFramework, DataBrowserEntityParametersEnvelope } from '../contracts/common-grid-framework';
import { IViewDataSource } from '../contracts/view-selection';
import { PBIGridOptionsModel } from './gridOptions.model';
import { ViewSelectionModel } from './view-selection.model';
export declare class PBICommonGridFrameworkOptionsModel implements CommonGridFramework {
    columnDebounce: number;
    flyOutPosition: 'left' | 'right';
    gridOptions: PBIGridOptionsModel;
    viewList: ViewSelectionModel[];
    refreshOnColumnUpdate: boolean;
    settingFlyOutOptions: Array<CGFSettingsEnum>;
    showCGF: boolean;
    showFlyoutButtonContainer: boolean;
    showGrid: boolean;
    showLoader: boolean;
    useBulkApply: boolean;
    visibleFlyOuts: Array<CGFFlyOutEnum>;
    viewDataSource: IViewDataSource;
    dataBrowserEntityParameters: DataBrowserEntityParametersEnvelope[];
}
