import { HttpClient } from '@angular/common/http';
import { ToasterResponse } from '../../contracts/common-grid-framework';
import { GridViewInfo, IViewDataSource, ViewSelectionLayout } from '../../contracts/view-selection';
import { ViewSelectionModel } from '../../models/view-selection.model';
export declare class DataBrowserViewSource implements IViewDataSource {
    baseUrl: string;
    key: string;
    httpClient: HttpClient;
    constructor(baseUrl: string, key: string, httpClient: HttpClient);
    getViews: (key: string) => Promise<ViewSelectionModel[]>;
    updateViewVisibility: (viewId: number, isPublic: boolean) => Promise<ToasterResponse>;
    saveView: (gridView: GridViewInfo) => Promise<ViewSelectionLayout[]>;
    addView: (gridView: GridViewInfo) => Promise<ViewSelectionLayout[]>;
    deleteView: (view: ViewSelectionModel) => Promise<ToasterResponse>;
    applyView: (view: ViewSelectionModel) => Promise<GridViewInfo>;
    private getUrl;
    private error;
    private transformToDataBrowserObject;
    private transformToCGFObject;
}
export interface IDataBrowserLayoutDetails {
    Id: number;
    Name: string;
    GridName: string;
    JsonLayout: string;
    IsPublic: boolean;
    DataLoadOptions: string;
    IsPinned: boolean;
    OverrideId: number | null;
    IsUserDefault: boolean;
    IsGlobalDefault: boolean;
}
export interface IValidationEnvelope {
    IsValidationFailed: boolean;
    ValidationMessage: string[];
    IsUserConfirmationRequired: boolean;
    OverrideId: number;
}
