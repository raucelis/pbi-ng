import { ActivatedRoute, Router } from '@angular/router';
import { AfterViewInit, DoCheck, EventEmitter } from '@angular/core';
import { DataBrowserEntityParametersEnvelope, DateShortCutParams } from '../contracts/common-grid-framework';
export declare class EntityParametersComponent implements DoCheck, AfterViewInit {
    private router;
    private activatedRoute;
    treeView: any;
    dataBrowserEntityParameters: DataBrowserEntityParametersEnvelope[];
    disableRefreshIcon: EventEmitter<any>;
    entityParamEvent: EventEmitter<any>;
    flyOutSelectionClick: EventEmitter<any>;
    checkBox: {
        checked: {};
    };
    dateShortcutsItems: Array<DateShortCutParams>;
    disableParameterApply: boolean;
    dxDateBox: any;
    gridInstances: any[];
    selectedDateShortcut: string;
    sortable: any;
    dropDownOptions: any;
    textBox: {
        stringType: {};
        numberType: {};
    };
    constructor(router: Router, activatedRoute: ActivatedRoute);
    ngDoCheck(): void;
    ngAfterViewInit(): void;
    templateFunction(data: string): string;
    applyParameters(): void;
    selectionChangedHandler(selectedRowEvent: any, order: number): void;
    onContentReadyHandler(e: any, order: number): void;
    onValueChangedDropDownBox(e: any, order: any): void;
    onInitializedHandler(e: any, order: any): void;
    private prepareGridOptions;
    private getGridInstance;
    onDateValueChanged(e: any, item: any): void;
    /**
    * Method will be called on content ready of datebox.
    * @returns void
    */
    dateShortcutContentReady(item: DataBrowserEntityParametersEnvelope): void;
    dateShortcutOpen(item: DataBrowserEntityParametersEnvelope): void;
    /**
     * Method will be called on date short cut click.
     * @returns void
     */
    dateShortcutItemClick(selectedData: any, globalItem: DataBrowserEntityParametersEnvelope): void;
    private IsNullEmptyOrWhiteSpace;
    onParameterDropDownOpened(args?: any): void;
    onKeyDown_Grid(evt: any): void;
    onMouseOver(item: any): void;
    onMouseOut(item: any): void;
}
