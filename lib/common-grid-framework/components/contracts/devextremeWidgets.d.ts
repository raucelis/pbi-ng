import { dxElement } from 'devextreme/core/element';
import DevExpress from 'devextreme';
import dxDropDownBox from 'devextreme/ui/drop_down_box';
import dxTreeView, { dxTreeViewNode } from 'devextreme/ui/tree_view';
import dxTextBox from 'devextreme/ui/text_box';
export interface DropdownBoxValueChangeArguments {
    component?: dxDropDownBox;
    element?: dxElement;
    model?: any;
    value?: any;
    previousValue?: any;
    event?: DevExpress.events.event;
}
export interface AppToastOptions {
    closeOnClick?: boolean;
    displayTime?: number;
    message: string;
    position?: {
        my: string;
        at: string;
    };
    type: string;
    width?: string;
}
export interface TreeViewItemSelectionChanged {
    component?: dxTreeView;
    element?: DevExpress.core.dxElement;
    model?: any;
    node?: dxTreeViewNode;
    itemElement?: DevExpress.core.dxElement;
}
export interface TreeViewContentReady {
    component?: dxTreeView;
    element?: DevExpress.core.dxElement;
    model?: any;
}
export interface TextBoxBaseOptions {
    component?: dxTextBox;
    element?: DevExpress.core.dxElement;
}
export interface TextBoxContentReady extends TextBoxBaseOptions {
    model?: any;
}
export interface TextBoxValueChange extends TextBoxBaseOptions {
    model?: any;
    value?: any;
    previousValue?: any;
    event?: DevExpress.events.event;
}
export interface DevExtremeButton {
    text?: string;
    type?: string;
    onClick?: (e: any) => any;
    useSubmitBehavior?: boolean;
}
