import { DevExtremeDataTypeFormat } from './grid';
import DevExpress from 'devextreme';
import Editor from 'devextreme/ui/editor/editor';
export interface SelectedColumnInfo {
    dataField: string;
    visible: boolean;
}
export interface ColumnFormat {
    alignment?: string;
    applyType?: string;
    backgroundColor?: string;
    caption?: string;
    condition?: string;
    cssClass?: string;
    dataField?: string;
    dataType?: string;
    fixed?: boolean;
    fixedPosition?: string;
    format?: string | DevExtremeDataTypeFormat;
    formatRuleName?: string;
    gridName?: string;
    textColor?: string;
}
export interface FormatOptionDataType {
    activesrc: string;
    isSelected: boolean;
    key: string;
    src: string;
    title: string;
    type: string;
}
export interface DxEditorValueChange {
    component?: Editor;
    element?: DevExpress.core.dxElement;
    model?: any;
    value?: any;
    previousValue?: any;
    event?: DevExpress.events.event;
}
export interface ColumnVisibilityType {
    visible: boolean;
    dataField: string;
}
export interface ColumnVisibilityBulkLookup {
    [dataField: string]: ColumnVisibilityType;
}
