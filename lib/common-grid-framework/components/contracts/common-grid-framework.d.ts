import dxDataGrid from 'devextreme/ui/data_grid';
import { CGFFlyOutEnum, CGFSettingsEnum } from '../../utilities/enums';
import { IViewDataSource } from './view-selection';
import { PBIGridColumn } from './grid';
import { PBIGridOptionsModel } from '../models/gridOptions.model';
import { ViewSelectionModel } from '../models/view-selection.model';
export interface CommonGridFramework {
    columnDebounce: number;
    dataBrowserEntityParameters: DataBrowserEntityParametersEnvelope[];
    flyOutPosition: 'left' | 'right';
    gridOptions: PBIGridOptionsModel;
    refreshOnColumnUpdate: boolean;
    settingFlyOutOptions: Array<CGFSettingsEnum>;
    showCGF: boolean;
    showFlyoutButtonContainer: boolean;
    showGrid: boolean;
    showLoader: boolean;
    useBulkApply: boolean;
    viewDataSource: IViewDataSource;
    viewList: ViewSelectionModel[];
    visibleFlyOuts: CGFFlyOutEnum[];
}
export interface FlyOutSection {
    id: number;
    src: string;
    title: string;
    isVisible: boolean;
    enableLoader?: boolean;
}
export interface GridComponentInstances {
    gridComponentInstance: dxDataGrid;
    isMasterGrid: boolean;
    gridName: string;
    isSelected: boolean;
}
export interface SelectedEntityColumn {
    columnList: Array<PBIGridColumn>;
    gridName: string;
}
export interface ControlProperties {
    id: number;
    key?: string;
    title?: string;
    icon?: string;
    isDisabled?: boolean;
    isHidden?: boolean;
    displayOrder?: number;
    enableLoader?: boolean;
}
export interface ToasterResponse {
    isError: boolean;
    message: string;
    toasterType: string;
    exceptionMessage: string;
}
export interface DataBrowserEntityParametersEnvelope {
    DataType: string;
    DefaultListValue: string[];
    DefaultSingleValue: any;
    DefaultValue: any;
    HideInDataBrowser: boolean;
    IsOptional: boolean;
    IsVisible: boolean;
    LookUpValues: any;
    Name: string;
    Order: number;
    OriginalName: string;
    Type: string;
    Value: any;
    dateShortcutIcon?: string;
    dateShortcutId?: string;
}
export interface DateShortCutParams {
    Id: string;
    Name: string;
    Value: string;
    IsSelected: boolean;
}
export interface ParameterValueGridOption {
    DataSource: any;
    Pager?: {
        showPageSizeSelector: boolean;
        allowedPageSizes: number[];
        showInfo: boolean;
        visible: boolean;
    };
    Paging?: {
        enabled: boolean;
        pageSize: number;
    };
}
