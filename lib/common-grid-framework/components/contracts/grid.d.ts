import DevExpress from 'devextreme';
import dxDataGrid, { dxDataGridRowObject, dxDataGridColumn } from 'devextreme/ui/data_grid';
import dxDraggable from 'devextreme/ui/draggable';
import dxSortable from 'devextreme/ui/sortable';
import { RequiredRule, NumericRule, RangeRule, StringLengthRule, CustomRule, CompareRule, PatternRule, EmailRule, AsyncRule } from 'devextreme/ui/validation_engine';
import { dxElement } from 'devextreme/core/element';
export interface DevExtremeDataTypeFormat {
    type: string;
    precision: number;
}
export interface Lookup {
    dataSource: Array<any>;
    displayExpr?: string;
    valueExpr?: string;
    onOpened?: (args: any) => void;
}
export interface ValidationRules {
    type: string;
    message: string;
    max?: number;
    pattern?: any;
    validationCallback?: any;
}
export interface PBIGridColumnFunctions {
    calculateCellValue?: (e: any) => any;
}
export interface PBIGridColumn extends dxDataGridColumn {
    caption: string;
    dataField: string;
    columnId?: number | string;
    groupName?: string;
    description?: string;
}
export interface PBIGridConfiguration {
    accessKey: string;
    allowAdding: boolean;
    allowColumnReordering: boolean;
    allowColumnResizing: boolean;
    allowDataExport: boolean;
    allowDeleting: boolean;
    allowReordering: boolean;
    allowSelectedDataExport: boolean;
    allowUpdating: boolean;
    allowedPageSizes: number[];
    autoExpandAll: boolean;
    autoNavigateToFocusedRow: boolean;
    columnAutoWidth: boolean;
    columnMinWidth: string | number;
    columnRenderingMode: string;
    columnResizingMode: string;
    columns: PBIGridColumn[];
    contextMenuMappingList: [];
    customizeColumns: (columns: Array<dxDataGridColumn>) => any;
    dataSource: string | Array<any> | DevExpress.data.DataSource | DevExpress.data.DataSourceOptions;
    dateSerializationFormat: string;
    disabled: boolean;
    editingMode: string;
    elementAttr: any;
    enableActiveState: boolean;
    enableCache: boolean;
    enableCellHint: boolean;
    enableColumnChooser: boolean;
    enableColumnFixing: boolean;
    enableColumnHiding: boolean;
    enableContextGrpMenu: boolean;
    enableContextMenu: boolean;
    enableErrorRow: boolean;
    enableGridFormatting: boolean;
    enableLoadPanel: boolean;
    enableStateStoring: boolean;
    filterSyncEnabled: boolean;
    filterValue: string | Array<any> | Function;
    gridComponentInstance: DevExpress.ui.dxDataGrid;
    gridFilterValue: any;
    gridName: string;
    height: number | string | (() => number | string);
    highlightChanges: boolean;
    hoverStateEnabled: boolean;
    isMasterGrid: boolean;
    listGroupedColumns: string[];
    noDataText: string;
    pageSize: number;
    pagingEnabled: boolean;
    refreshMode: 'full' | 'reshape' | 'repaint';
    remoteOperationsEnabled: boolean;
    repaintChangesOnly: boolean;
    rowAlternationEnabled: boolean;
    scrollMode: string;
    selectedRowData: any;
    selectedTheme: string;
    selectedThemeClass: string;
    selectionMode: string;
    showBorders: boolean;
    showColumnHeaders: boolean;
    showColumnLines: boolean;
    showDragIcons: boolean;
    showFilterPanel: boolean;
    showFilterRow: boolean;
    showGroupPanel: boolean;
    showHeaderFilter: boolean;
    showPageInfo: boolean;
    showPageSizeSelector: boolean;
    showPager: boolean;
    showRowLines: boolean;
    showScrollbars: boolean;
    showSearchPanel: boolean;
    sortingType: string;
    stateStorageType: 'custom' | 'localStorage' | 'sessionStorage';
    useIcons: boolean;
}
export interface GridColumnChooser {
    allowSearch?: boolean;
    emptyPanelText?: string;
    enabled?: boolean;
    height?: number;
    mode?: 'dragAndDrop' | 'select';
    searchTimeout?: number;
    title?: string;
    width?: number;
}
export interface PBIGridEvents {
    onCellClick: (cellObject: GridCellClickArguments) => any;
    onCellPrepared: (cellInfo: GridCellPreparedArguments) => any;
    onContentReady: (gridObject: GridBaseArguments) => any;
    onContextMenuPreparing: (cellObject: GridContextMenuPreparingArguments) => any;
    onEditingStart: (rowItem: GridEditingStartArguments) => any;
    onEditorPrepared: (row: GridEditorPreparedArguments) => any;
    onEditorPreparing: (rowObject: GridEditorPreparingArguments) => any;
    onExporting: (args: GridExportingArguments) => any;
    onInitNewRow: (args: GridInitNewRowArguments) => any;
    onInitialized: (gridObject: GridInitializedArguments) => any;
    onReorder: (e: GridReorderArguments) => any;
    onRowClick: (rowItem: GridRowClickArguments) => any;
    onRowPrepared: (rowItem: GridRowPreparedArguments) => any;
    onRowRemoved: (rowItem: GridRowRemovedArguments) => any;
    onRowUpdating: (rowItem: GridRowUpdatingArguments) => any;
    onRowValidating: (rowItem: GridRowValidatingArguments) => any;
    onSelectionChanged: (selectedItem: GridSelectionChangedArguments) => any;
    onCustomButtonClick?: (args: {
        evt: MouseEvent;
        rowData: any;
        btnType: string;
    }) => any;
    onUserDefinedCustomButtonClick?: (args: {
        evt: MouseEvent;
        rowData: any;
        actionItem: any;
    }) => any;
}
export interface GridInitializedArguments {
    component?: dxDataGrid;
    element?: dxElement;
}
export interface GridBaseArguments extends GridInitializedArguments {
    model?: any;
}
export interface GridCellClickArguments extends GridBaseArguments {
    cellElement?: dxElement;
    column?: any;
    columnIndex?: number;
    data?: any;
    displayValue?: any;
    event?: DevExpress.events.event;
    key?: any;
    row?: dxDataGridRowObject;
    rowIndex?: number;
    rowType?: string;
    text?: string;
    value?: any;
}
export interface GridCellPreparedArguments extends GridBaseArguments {
    cellElement?: dxElement;
    column?: dxDataGridColumn;
    columnIndex?: number;
    data?: any;
    displayValue?: any;
    isExpanded?: boolean;
    isNewRow?: boolean;
    isSelected?: boolean;
    key?: any;
    oldValue?: any;
    row?: dxDataGridRowObject;
    rowIndex?: number;
    rowType?: string;
    text?: string;
    value?: any;
    watch?: Function;
    summaryItems?: any;
    totalItem?: any;
}
export interface GridCellDoubleClick extends GridBaseArguments {
    cellElement?: dxElement;
    column?: dxDataGridColumn;
    columnIndex?: number;
    data?: any;
    displayValue?: any;
    event?: DevExpress.events.event;
    key?: any;
    row?: dxDataGridRowObject;
    rowIndex?: number;
    rowType?: string;
    text?: string;
    value?: any;
}
export interface GridCellHoverChanged extends GridBaseArguments {
    eventType?: string;
    data?: any;
    key?: any;
    value?: any;
    text?: string;
    displayValue?: any;
    columnIndex?: number;
    rowIndex?: number;
    column?: dxDataGridColumn;
    rowType?: string;
    cellElement?: dxElement;
    row?: dxDataGridRowObject;
}
export interface GridContextMenuPreparingArguments extends GridBaseArguments {
    column?: dxDataGridColumn;
    columnIndex?: number;
    items?: Array<any>;
    row?: dxDataGridRowObject;
    rowIndex?: number;
    target?: string;
    targetElement?: dxElement;
}
export interface GridEditingStartArguments extends GridBaseArguments {
    data?: any;
    key?: any;
    cancel?: boolean;
    column?: any;
}
export interface GridEditorPreparedArguments extends GridBaseArguments {
    parentType?: string;
    value?: any;
    setValue?: any;
    updateValueTimeout?: number;
    width?: number;
    disabled?: boolean;
    rtlEnabled?: boolean;
    editorElement?: dxElement;
    readOnly?: boolean;
    dataField?: string;
    row?: dxDataGridRowObject;
}
export interface GridEditorPreparingArguments extends GridBaseArguments {
    parentType?: string;
    value?: any;
    setValue?: any;
    updateValueTimeout?: number;
    width?: number;
    disabled?: boolean;
    rtlEnabled?: boolean;
    cancel?: boolean;
    editorElement?: dxElement;
    readOnly?: boolean;
    editorName?: string;
    editorOptions?: any;
    dataField?: string;
    row?: dxDataGridRowObject;
}
export interface GridExportingArguments extends GridBaseArguments {
    alignment?: any;
    cancel?: boolean;
    fileName?: string;
    fill: any;
    font?: any;
    value?: any;
}
export interface GridFocusedCellChanged extends GridBaseArguments {
    cellElement?: dxElement;
    columnIndex?: number;
    rowIndex?: number;
    row?: dxDataGridRowObject;
    column?: dxDataGridColumn;
}
export interface GridFocusedCellChanging extends GridBaseArguments {
    cellElement?: dxElement;
    prevColumnIndex?: number;
    prevRowIndex?: number;
    newColumnIndex?: number;
    newRowIndex?: number;
    event?: DevExpress.events.event;
    rows?: Array<dxDataGridRowObject>;
    columns?: Array<dxDataGridColumn>;
    cancel?: boolean;
    isHighlighted?: boolean;
}
export interface GridFocusedRowChanged extends GridBaseArguments {
    rowElement?: dxElement;
    rowIndex?: number;
    row?: dxDataGridRowObject;
}
export interface GridFocusedRowChanging extends GridBaseArguments {
    rowElement?: dxElement;
    prevRowIndex?: number;
    newRowIndex?: number;
    event?: DevExpress.events.event;
    rows?: Array<dxDataGridRowObject>;
    cancel?: boolean;
}
export interface GridRowClickArguments extends GridBaseArguments {
    event?: DevExpress.events.event;
    data?: any;
    key?: any;
    values?: Array<any>;
    columns?: Array<any>;
    rowIndex?: number;
    rowType?: string;
    isSelected?: boolean;
    isExpanded?: boolean;
    isNewRow?: boolean;
    groupIndex?: number;
    rowElement?: dxElement;
    handled?: boolean;
}
export interface GridRowDblClick extends GridBaseArguments {
    event?: DevExpress.events.event;
    data?: any;
    key?: any;
    values?: Array<any>;
    columns?: Array<dxDataGridColumn>;
    rowIndex?: number;
    rowType?: string;
    isSelected?: boolean;
    isExpanded?: boolean;
    isNewRow?: boolean;
    groupIndex?: number;
    rowElement?: dxElement;
}
export interface GridRowPreparedArguments extends GridBaseArguments {
    data?: any;
    key?: any;
    values?: Array<any>;
    columns?: Array<dxDataGridColumn>;
    rowIndex?: number;
    rowType?: string;
    groupIndex?: number;
    isSelected?: boolean;
    isExpanded?: boolean;
    isNewRow?: boolean;
    rowElement?: dxElement;
}
export interface GridInitNewRowArguments extends GridBaseArguments {
    data?: any;
    promise?: Promise<void> | JQueryPromise<void>;
}
export interface GridRowRemovedArguments extends GridBaseArguments {
    data?: any;
    key?: any;
    error?: Error;
}
export interface GridRowValidatingArguments extends GridBaseArguments {
    brokenRules?: Array<RequiredRule | NumericRule | RangeRule | StringLengthRule | CustomRule | CompareRule | PatternRule | EmailRule | AsyncRule>;
    isValid?: boolean;
    key?: any;
    newData?: any;
    oldData?: any;
    errorText?: string;
    promise?: Promise<void> | JQueryPromise<void>;
}
export interface GridSelectionChangedArguments extends GridBaseArguments {
    currentSelectedRowKeys?: Array<any>;
    currentDeselectedRowKeys?: Array<any>;
    selectedRowKeys?: Array<any>;
    selectedRowsData?: Array<any>;
}
export interface GridRowUpdatingArguments extends GridBaseArguments {
    oldData?: any;
    newData?: any;
    key?: any;
    cancel?: boolean | Promise<void> | JQueryPromise<void>;
}
export interface GridReorderArguments extends GridBaseArguments {
    event?: DevExpress.events.event;
    itemData?: any;
    itemElement?: dxElement;
    fromIndex?: number;
    toIndex?: number;
    fromComponent?: dxSortable | dxDraggable;
    toComponent?: dxSortable | dxDraggable;
    fromData?: any;
    toData?: any;
    dropInsideItem?: boolean;
    promise?: Promise<void> | JQueryPromise<void>;
}
export interface DevExtremeDataTypeFormat {
    type: string;
    precision: number;
}
export interface GridStateSaveInfo {
    columnFormattingInfo?: Array<any>;
    columns?: Array<any>;
    conditionalFormattingInfo?: Array<any>;
    filterValue?: any;
    isGridBorderVisible?: boolean;
    selectedTheme?: string;
    summary?: any;
    visibleColumns?: Array<dxDataGridColumn>;
}
