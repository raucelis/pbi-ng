import { CGFEventsEnum } from '../../utilities/enums';
import { ViewSelectionModel } from '../models/view-selection.model';
import { ToasterResponse } from './common-grid-framework';
export interface ViewSelectionLayout {
    CanDefault: boolean;
    CanDelete: boolean;
    CanGlobalDefault: boolean;
    CanSave: boolean;
    CanSaveNew: boolean;
    Id: number;
    IsDefault: boolean;
    IsGlobalDefault: boolean;
    IsPublic: boolean;
    Name: string;
    IsPinned: boolean;
}
export interface ValueSet {
    key: string | number;
    value: string | boolean;
}
export interface GridViewInfo {
    id: number;
    name: string;
    isPinned: boolean;
    defaultOptions: Array<ValueSet>;
    visibility: boolean;
    isGlobalDefault: boolean;
    isUserDefault: boolean;
    state: GridStateInfo;
}
export interface GridStateInfo {
    gridState: any;
    columnFormattingInfo: Array<any>;
    conditionFormattingInfo: Array<any>;
}
export interface IViewDataSource {
    key: string;
    getViews: (key: string) => Promise<ViewSelectionModel[]>;
    updateViewVisibility: (viewId: number, isPublic: boolean) => Promise<ToasterResponse>;
    saveView: (gridView: GridViewInfo) => Promise<ViewSelectionLayout[]>;
    addView: (gridView: GridViewInfo) => Promise<ViewSelectionLayout[]>;
    deleteView: (view: ViewSelectionModel) => Promise<ToasterResponse>;
    applyView: (gridView: ViewSelectionModel) => Promise<GridViewInfo>;
}
export interface ViewAPIArguments {
    event: CGFEventsEnum;
    data: any;
}
