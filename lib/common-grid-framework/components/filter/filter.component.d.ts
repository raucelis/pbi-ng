import { EventEmitter } from '@angular/core';
import { DevExtremeButton } from '../contracts/devextremeWidgets';
import { GridComponentInstances } from '../contracts/common-grid-framework';
import { PBIGridOptionsModel } from '../models/gridOptions.model';
import { PBIGridColumn } from '../contracts/grid';
export declare class FilterComponent {
    set filterGridOptions(filterGridOptions: PBIGridOptionsModel);
    get filterGridOptions(): PBIGridOptionsModel;
    set gridInstanceList(gridInstanceList: Array<GridComponentInstances>);
    get gridInstanceList(): Array<GridComponentInstances>;
    closeCurrentFlyOut: EventEmitter<any>;
    applyButtonOptions: DevExtremeButton;
    filterValue: any;
    gridOptions: PBIGridOptionsModel;
    listOfColumns: PBIGridColumn[];
    selectedGridInstanceList: Array<any>;
    title: string;
    constructor();
    showFilterSection: boolean;
    applyFilter(): void;
    closeFlyOut(): void;
}
