import dxDataGrid from 'devextreme/ui/data_grid';
export declare class CGFUtilityService {
    constructor();
    applyFiltersToGrid(gridInstance: any, filterValue?: any): void;
    applyFilterClass(filter: any[], gridInstance: any): void;
    getStorageKey(gridInstance: dxDataGrid, keyValue: string, isMasterGrid?: boolean): string;
}
