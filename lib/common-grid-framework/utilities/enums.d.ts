export declare enum CGFFlyOutEnum {
    none = -1,
    refreshData = 1,
    filter = 2,
    columnChooser = 3,
    customColumns = 4,
    viewSelection = 5,
    gridSettings = 6,
    conditionFormatting = 7,
    newTab = 8
}
export declare enum CGFEventsEnum {
    addUnboundColumns = 0,
    applyEntityParameters = 1,
    cancelEntityParameters = 2,
    deleteUnboundColumns = 3,
    editSecurity = 4,
    getCachedData = 5,
    refreshEntityData = 6,
    refreshTheme = 7,
    saveParameterOrder = 8,
    addNewView = 9,
    applyView = 10,
    cloneView = 11,
    deleteView = 12,
    getViews = 13,
    saveAsView = 14,
    saveSelectedView = 15,
    updateViews = 16,
    updateViewVisibility = 17,
    updateLayouts = 18
}
export declare enum CGFStorageKeys {
    conditionalFormatting = 0,
    dictionaryFormatData = 1,
    formatData = 2,
    childGridView = 3
}
export declare enum CGFSettingsEnum {
    theme = 1,
    gridLines = 2,
    autoFit = 3,
    expAllData = 4,
    expExcelLink = 5,
    expEmailReport = 6,
    advDem = 7
}
export declare enum CGFFeatureUpdateEnum {
    clear = 0,
    customColumnUpdated = 1,
    cachedData = 2
}
export declare enum SecurityMasterAction {
    AddSecurity = 0,
    EditSecurity = 1
}
