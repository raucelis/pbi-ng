import dxDataGrid from 'devextreme/ui/data_grid';
import { AppToastOptions } from '../components/contracts/devextremeWidgets';
export declare function getStorageKey(gridInstance: dxDataGrid, keyValue: string, isMasterGrid?: boolean): string;
export declare function getClassNameByThemeName(themeName: string): string;
export declare function applyFilterCssClass(filter: any[], gridInstance: any): void;
export declare function applyFiltersToGrid(gridInstance: dxDataGrid, filterValue?: Array<any>): void;
export declare function appToast(options: AppToastOptions): void;
export declare function ToDictionary<T>(array: T[], key: (element: T) => string | number): {
    [key: string]: T;
};
export declare function filterSelectedRowData(gridInstance: any, rowData?: any): Array<any>;
export declare function openDestination(data: any, selectedRowData: any, entityParameterInfo?: any[], appService?: any): boolean;
export declare function extractDestinationEntityParameters(selectedRowData: any[], parameterMapping: any[], entityParameterInfo: any[]): {
    param: string;
    undefinedSourceColumnNames: string;
};
export declare function convertDateToUsFormat(value: Date | string): string;
