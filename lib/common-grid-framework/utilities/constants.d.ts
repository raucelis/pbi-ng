import { FlyOutSection, ControlProperties, DateShortCutParams } from '../components/contracts/common-grid-framework';
export declare const cgfFlyOutList: Array<FlyOutSection>;
export declare const columnFormattingOptions: {
    format: {
        type: string;
        key: string;
        title: string;
        icon: string;
        isSelected: boolean;
    }[];
    alignment: {
        type: string;
        key: string;
        title: string;
        icon: string;
        isSelected: boolean;
    }[];
    pin: {
        type: string;
        key: string;
        title: string;
        icon: string;
        isSelected: boolean;
    }[];
    fontSize: string;
    fontStyle: {
        type: string;
        key: string;
        title: string;
        icon: string;
        isSelected: boolean;
    }[];
};
export declare const rowTypeInfo: {
    data: string;
    group: string;
    totalFooter: string;
};
export declare const customActionColumnInfo: {
    dataField: string;
    groupName: string;
    caption: string;
};
export declare const crudOperation: {
    add: string;
    edit: string;
    clone: string;
};
export declare const SettingExportOptions: Array<ControlProperties>;
export declare const actionTypes: {
    entity: string;
    url: string;
    addSecurity: string;
    editSecurity: string;
    childEntity: string;
    dispatcherJob: string;
    lookup: string;
    securityType: string;
};
export declare const dateShortcuts: Array<DateShortCutParams>;
