import { Params } from '@angular/router';
/**
 * The configuration for the navigation menu.
 */
export interface NavigationMenuConfig {
    /** Whether the user interface is collapsed or not. */
    collapsed: boolean;
    /** An array of section configurations.  */
    sections: NavigationSectionConfig[];
    /** Whether the Angular router uses the hash (#) or not. */
    useHash?: boolean;
    /** Directory where the Angular app is running. */
    href?: string;
    /** Whether to show the toggle button or not. */
    showToggleButton?: boolean;
}
/**
 * The base configuration for a navigable item.
 */
export interface NavigableItemConfig {
    /** Used when sorting the collection of items (not supported yet). */
    displayOrder?: number;
    /** CSS class from any icon library available. */
    icon?: string;
    /**  */
    link?: string;
    /** A collection of matrix and query URL parameters. */
    queryParams?: Params;
    /** Whether to enable an option to open the link in a new browser tab or not. */
    openNewTab?: boolean;
    /** Human-friendly text for the item. */
    title: string;
}
/**
 * The configuration for a section of the navigation menu.
 */
export interface NavigationSectionConfig extends NavigableItemConfig {
    /**  */
    actions?: NavigationActionConfig[];
    /** The configuration for the section content. */
    content?: NavigationContentConfig;
    /** Whether to render an alternative user interface for information or not. */
    informational?: boolean;
    /** Whether to enable deep searching capabilities in the navigation tree or not. */
    searchable?: boolean;
}
/**
 * The configuration for an item of the navigation menu.
 */
export interface NavigationItemConfig extends NavigableItemConfig {
    content?: NavigationContentConfig;
}
/**
 * The configuration for either section or item content.
 */
export interface NavigationContentConfig {
    groups?: NavigationItemGroupConfig[];
}
/**
 * The configuration for a custom action.
 */
export interface NavigationActionConfig {
    /** Unique id that will be returned when the user clicks an action, designed for custom application handlers. */
    id: string;
    /** CSS class from any icon library available. */
    icon: string;
    /** Human-friendly text for the item that is rendered as a tooltip. */
    title: string;
}
/**
 * The configuration of a group of navigation items.
 */
export interface NavigationItemGroupConfig {
    /** Used when sorting the collection of items (not supported yet). */
    displayOrder?: number;
    /** An array of navigation item configurations.*/
    items: NavigationItemConfig[];
    /** Human-friendly text for the group. */
    title?: string;
}
