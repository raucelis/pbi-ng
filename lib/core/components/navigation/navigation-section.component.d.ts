import { EventEmitter, OnInit } from '@angular/core';
import { NavigationSectionConfig, NavigationContentConfig, NavigationActionConfig, NavigableItemConfig, NavigationItemConfig } from '../contracts/navigation';
import { SearchService } from '../../services/search.service';
export declare class NavigationSectionComponent implements OnInit {
    private readonly searchService;
    config: NavigationSectionConfig;
    action: EventEmitter<string>;
    navigateTo: EventEmitter<NavigableItemConfig>;
    navigateToNewTab: EventEmitter<NavigableItemConfig>;
    private _filteredContentConfig;
    get contentConfig(): NavigationContentConfig;
    get infoItems(): NavigationItemConfig[];
    searchQuery: string;
    constructor(searchService: SearchService);
    ngOnInit(): void;
    search(): void;
    onActionClick(action: NavigationActionConfig): void;
    onNavigateTo(config: NavigableItemConfig, newTab?: boolean): void;
    private flattenTree;
}
