import { EventEmitter, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { NavigationContentConfig, NavigationItemConfig, NavigableItemConfig } from '../contracts/navigation';
export declare class NavigationContentComponent implements OnInit {
    private el;
    private readonly renderer;
    private _config;
    set config(v: NavigationContentConfig);
    get config(): NavigationContentConfig;
    set top(v: number);
    get browsing(): boolean;
    navigateTo: EventEmitter<NavigableItemConfig>;
    navigateToNewTab: EventEmitter<NavigableItemConfig>;
    private items;
    activeItemConfig: NavigationItemConfig;
    private _contentTop;
    get contentTop(): number;
    constructor(el: ElementRef, renderer: Renderer2);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    browse(config: NavigationItemConfig, event: MouseEvent): void;
    browseOrNavigate(config: NavigationItemConfig, event: MouseEvent): void;
    isBrowsing(config: NavigationItemConfig): boolean;
    onNavigateTo(config: NavigableItemConfig, newTab?: boolean): void;
    private computeContentMaxHeight;
}
