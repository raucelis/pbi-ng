export declare class SearchService {
    /**
     * Returns elements of the array that contain the given string, case insensitive, anywhere in their object hierarchy.
     * @param input The array to search.
     * @param text The string to match, case insensitive.
     * @param exclude The list of properties to exclude from the search.
     */
    deepArrayContains<T>(input: T[], text: string, exclude?: string[]): T[];
    /**
     * Returns elements of the array that match the provided function.
     * @param input The array to search.
     * @param filter The filter function to apply.
     */
    deepArrayFilter<T>(input: T[], filter: (property: any) => boolean, exclude?: string[]): Generator<T, void, unknown>;
    /**
     * Finds if any properties of the given object, or their children, match the given string, case insensitive.
     * @param obj The object to search.
     * @param text The string to match, case insensitive.
     */
    deepContains<T>(obj: T, text: string): boolean;
    /**
     * Finds if any properties of the given object, or their children, match the given filter.
     * @param obj The object to search.
     * @param filter The filter function to apply.
     */
    deepMatch<T>(obj: T, filter: (property: any) => boolean, exclude?: string[]): boolean;
    treeWalk<T>(arr: T[], childPath: keyof T, callback: (item: T) => void): any;
}
