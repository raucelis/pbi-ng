export var CGFFlyOutEnum;
(function (CGFFlyOutEnum) {
    CGFFlyOutEnum[CGFFlyOutEnum["none"] = -1] = "none";
    CGFFlyOutEnum[CGFFlyOutEnum["refreshData"] = 1] = "refreshData";
    CGFFlyOutEnum[CGFFlyOutEnum["filter"] = 2] = "filter";
    CGFFlyOutEnum[CGFFlyOutEnum["columnChooser"] = 3] = "columnChooser";
    CGFFlyOutEnum[CGFFlyOutEnum["customColumns"] = 4] = "customColumns";
    CGFFlyOutEnum[CGFFlyOutEnum["viewSelection"] = 5] = "viewSelection";
    CGFFlyOutEnum[CGFFlyOutEnum["gridSettings"] = 6] = "gridSettings";
    CGFFlyOutEnum[CGFFlyOutEnum["conditionFormatting"] = 7] = "conditionFormatting";
    CGFFlyOutEnum[CGFFlyOutEnum["newTab"] = 8] = "newTab"; // for open url in new tab
})(CGFFlyOutEnum || (CGFFlyOutEnum = {}));
export var CGFEventsEnum;
(function (CGFEventsEnum) {
    CGFEventsEnum[CGFEventsEnum["addUnboundColumns"] = 0] = "addUnboundColumns";
    CGFEventsEnum[CGFEventsEnum["applyEntityParameters"] = 1] = "applyEntityParameters";
    CGFEventsEnum[CGFEventsEnum["cancelEntityParameters"] = 2] = "cancelEntityParameters";
    CGFEventsEnum[CGFEventsEnum["deleteUnboundColumns"] = 3] = "deleteUnboundColumns";
    CGFEventsEnum[CGFEventsEnum["editSecurity"] = 4] = "editSecurity";
    CGFEventsEnum[CGFEventsEnum["getCachedData"] = 5] = "getCachedData";
    CGFEventsEnum[CGFEventsEnum["refreshEntityData"] = 6] = "refreshEntityData";
    CGFEventsEnum[CGFEventsEnum["refreshTheme"] = 7] = "refreshTheme";
    CGFEventsEnum[CGFEventsEnum["saveParameterOrder"] = 8] = "saveParameterOrder";
    // layout related enums
    CGFEventsEnum[CGFEventsEnum["addNewView"] = 9] = "addNewView";
    CGFEventsEnum[CGFEventsEnum["applyView"] = 10] = "applyView";
    CGFEventsEnum[CGFEventsEnum["cloneView"] = 11] = "cloneView";
    CGFEventsEnum[CGFEventsEnum["deleteView"] = 12] = "deleteView";
    CGFEventsEnum[CGFEventsEnum["getViews"] = 13] = "getViews";
    CGFEventsEnum[CGFEventsEnum["saveAsView"] = 14] = "saveAsView";
    CGFEventsEnum[CGFEventsEnum["saveSelectedView"] = 15] = "saveSelectedView";
    CGFEventsEnum[CGFEventsEnum["updateViews"] = 16] = "updateViews";
    CGFEventsEnum[CGFEventsEnum["updateViewVisibility"] = 17] = "updateViewVisibility";
    CGFEventsEnum[CGFEventsEnum["updateLayouts"] = 18] = "updateLayouts";
})(CGFEventsEnum || (CGFEventsEnum = {}));
export var CGFStorageKeys;
(function (CGFStorageKeys) {
    CGFStorageKeys[CGFStorageKeys["conditionalFormatting"] = 0] = "conditionalFormatting";
    CGFStorageKeys[CGFStorageKeys["dictionaryFormatData"] = 1] = "dictionaryFormatData";
    CGFStorageKeys[CGFStorageKeys["formatData"] = 2] = "formatData";
    CGFStorageKeys[CGFStorageKeys["childGridView"] = 3] = "childGridView";
})(CGFStorageKeys || (CGFStorageKeys = {}));
export var CGFSettingsEnum;
(function (CGFSettingsEnum) {
    CGFSettingsEnum[CGFSettingsEnum["theme"] = 1] = "theme";
    CGFSettingsEnum[CGFSettingsEnum["gridLines"] = 2] = "gridLines";
    CGFSettingsEnum[CGFSettingsEnum["autoFit"] = 3] = "autoFit";
    CGFSettingsEnum[CGFSettingsEnum["expAllData"] = 4] = "expAllData";
    CGFSettingsEnum[CGFSettingsEnum["expExcelLink"] = 5] = "expExcelLink";
    CGFSettingsEnum[CGFSettingsEnum["expEmailReport"] = 6] = "expEmailReport";
    CGFSettingsEnum[CGFSettingsEnum["advDem"] = 7] = "advDem";
})(CGFSettingsEnum || (CGFSettingsEnum = {}));
export var CGFFeatureUpdateEnum;
(function (CGFFeatureUpdateEnum) {
    CGFFeatureUpdateEnum[CGFFeatureUpdateEnum["clear"] = 0] = "clear";
    CGFFeatureUpdateEnum[CGFFeatureUpdateEnum["customColumnUpdated"] = 1] = "customColumnUpdated";
    CGFFeatureUpdateEnum[CGFFeatureUpdateEnum["cachedData"] = 2] = "cachedData";
})(CGFFeatureUpdateEnum || (CGFFeatureUpdateEnum = {}));
export var SecurityMasterAction;
(function (SecurityMasterAction) {
    SecurityMasterAction[SecurityMasterAction["AddSecurity"] = 0] = "AddSecurity";
    SecurityMasterAction[SecurityMasterAction["EditSecurity"] = 1] = "EditSecurity";
})(SecurityMasterAction || (SecurityMasterAction = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW51bXMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29tbW9uLWdyaWQtZnJhbWV3b3JrL3V0aWxpdGllcy9lbnVtcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxNQUFNLENBQU4sSUFBWSxhQVVYO0FBVkQsV0FBWSxhQUFhO0lBQ3JCLGtEQUFTLENBQUE7SUFDVCwrREFBZSxDQUFBO0lBQ2YscURBQU0sQ0FBQTtJQUNOLG1FQUFhLENBQUE7SUFDYixtRUFBYSxDQUFBO0lBQ2IsbUVBQWEsQ0FBQTtJQUNiLGlFQUFZLENBQUE7SUFDWiwrRUFBbUIsQ0FBQTtJQUNuQixxREFBTSxDQUFBLENBQW1CLDBCQUEwQjtBQUN2RCxDQUFDLEVBVlcsYUFBYSxLQUFiLGFBQWEsUUFVeEI7QUFFRCxNQUFNLENBQU4sSUFBWSxhQXNCWDtBQXRCRCxXQUFZLGFBQWE7SUFDckIsMkVBQWlCLENBQUE7SUFDakIsbUZBQXFCLENBQUE7SUFDckIscUZBQXNCLENBQUE7SUFDdEIsaUZBQW9CLENBQUE7SUFDcEIsaUVBQVksQ0FBQTtJQUNaLG1FQUFhLENBQUE7SUFDYiwyRUFBaUIsQ0FBQTtJQUNqQixpRUFBWSxDQUFBO0lBQ1osNkVBQWtCLENBQUE7SUFFbEIsdUJBQXVCO0lBQ3ZCLDZEQUFVLENBQUE7SUFDViw0REFBUyxDQUFBO0lBQ1QsNERBQVMsQ0FBQTtJQUNULDhEQUFVLENBQUE7SUFDViwwREFBUSxDQUFBO0lBQ1IsOERBQVUsQ0FBQTtJQUNWLDBFQUFnQixDQUFBO0lBQ2hCLGdFQUFXLENBQUE7SUFDWCxrRkFBb0IsQ0FBQTtJQUNwQixvRUFBYSxDQUFBO0FBQ2pCLENBQUMsRUF0QlcsYUFBYSxLQUFiLGFBQWEsUUFzQnhCO0FBRUQsTUFBTSxDQUFOLElBQVksY0FLWDtBQUxELFdBQVksY0FBYztJQUN0QixxRkFBcUIsQ0FBQTtJQUNyQixtRkFBb0IsQ0FBQTtJQUNwQiwrREFBVSxDQUFBO0lBQ1YscUVBQWEsQ0FBQTtBQUNqQixDQUFDLEVBTFcsY0FBYyxLQUFkLGNBQWMsUUFLekI7QUFFRCxNQUFNLENBQU4sSUFBWSxlQVFYO0FBUkQsV0FBWSxlQUFlO0lBQ3ZCLHVEQUFTLENBQUE7SUFDVCwrREFBUyxDQUFBO0lBQ1QsMkRBQU8sQ0FBQTtJQUNQLGlFQUFVLENBQUE7SUFDVixxRUFBWSxDQUFBO0lBQ1oseUVBQWMsQ0FBQTtJQUNkLHlEQUFNLENBQUE7QUFDVixDQUFDLEVBUlcsZUFBZSxLQUFmLGVBQWUsUUFRMUI7QUFFRCxNQUFNLENBQU4sSUFBWSxvQkFJWDtBQUpELFdBQVksb0JBQW9CO0lBQzVCLGlFQUFLLENBQUE7SUFDTCw2RkFBbUIsQ0FBQTtJQUNuQiwyRUFBVSxDQUFBO0FBQ2QsQ0FBQyxFQUpXLG9CQUFvQixLQUFwQixvQkFBb0IsUUFJL0I7QUFFRCxNQUFNLENBQU4sSUFBWSxvQkFHWDtBQUhELFdBQVksb0JBQW9CO0lBQzVCLDZFQUFXLENBQUE7SUFDWCwrRUFBWSxDQUFBO0FBQ2hCLENBQUMsRUFIVyxvQkFBb0IsS0FBcEIsb0JBQW9CLFFBRy9CIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmV4cG9ydCBlbnVtIENHRkZseU91dEVudW0ge1xyXG4gICAgbm9uZSA9IC0xLFxyXG4gICAgcmVmcmVzaERhdGEgPSAxLFxyXG4gICAgZmlsdGVyLCAgICAgICAgICAgICAgICAgIC8vIGZvciBmaWx0ZXIgcGFuZWxcclxuICAgIGNvbHVtbkNob29zZXIsICAgICAgICAgICAvLyBmb3IgY29sdW1uIGNob29zZXJcclxuICAgIGN1c3RvbUNvbHVtbnMsICAgICAgICAgICAvLyBmb3IgY3VzdG9tIGNvbHVtbnNcclxuICAgIHZpZXdTZWxlY3Rpb24sICAgICAgICAgICAvLyBmb3IgdmlldyBzZWxlY3Rpb25cclxuICAgIGdyaWRTZXR0aW5ncywgICAgICAgICAgICAvLyBmb3Igc2V0dGluZ3NcclxuICAgIGNvbmRpdGlvbkZvcm1hdHRpbmcsICAgICAvLyBmb3IgY29uZGl0aW9uYWwgZm9ybWF0dGluZy5cclxuICAgIG5ld1RhYiAgICAgICAgICAgICAgICAgICAvLyBmb3Igb3BlbiB1cmwgaW4gbmV3IHRhYlxyXG59XHJcblxyXG5leHBvcnQgZW51bSBDR0ZFdmVudHNFbnVtIHtcclxuICAgIGFkZFVuYm91bmRDb2x1bW5zLCAgICAgICAgICAvLyBjYXB0dXJlIGV2ZW50cyByZWxhdGVkIHRvIHVuYm91bmQgY29sdW1uc1xyXG4gICAgYXBwbHlFbnRpdHlQYXJhbWV0ZXJzLCAgICAgIC8vIGNhcHR1cmUgZXZlbnRzIHJlbGF0ZWQgdG8gYXBwbHlpbmcgcGFyYW1ldGVyc1xyXG4gICAgY2FuY2VsRW50aXR5UGFyYW1ldGVycyxcclxuICAgIGRlbGV0ZVVuYm91bmRDb2x1bW5zLFxyXG4gICAgZWRpdFNlY3VyaXR5LFxyXG4gICAgZ2V0Q2FjaGVkRGF0YSxcclxuICAgIHJlZnJlc2hFbnRpdHlEYXRhLFxyXG4gICAgcmVmcmVzaFRoZW1lLFxyXG4gICAgc2F2ZVBhcmFtZXRlck9yZGVyLFxyXG5cclxuICAgIC8vIGxheW91dCByZWxhdGVkIGVudW1zXHJcbiAgICBhZGROZXdWaWV3LFxyXG4gICAgYXBwbHlWaWV3LFxyXG4gICAgY2xvbmVWaWV3LFxyXG4gICAgZGVsZXRlVmlldyxcclxuICAgIGdldFZpZXdzLFxyXG4gICAgc2F2ZUFzVmlldyxcclxuICAgIHNhdmVTZWxlY3RlZFZpZXcsXHJcbiAgICB1cGRhdGVWaWV3cyxcclxuICAgIHVwZGF0ZVZpZXdWaXNpYmlsaXR5LFxyXG4gICAgdXBkYXRlTGF5b3V0cyxcclxufVxyXG5cclxuZXhwb3J0IGVudW0gQ0dGU3RvcmFnZUtleXMge1xyXG4gICAgY29uZGl0aW9uYWxGb3JtYXR0aW5nLFxyXG4gICAgZGljdGlvbmFyeUZvcm1hdERhdGEsXHJcbiAgICBmb3JtYXREYXRhLFxyXG4gICAgY2hpbGRHcmlkVmlld1xyXG59XHJcblxyXG5leHBvcnQgZW51bSBDR0ZTZXR0aW5nc0VudW0ge1xyXG4gICAgdGhlbWUgPSAxLFxyXG4gICAgZ3JpZExpbmVzLFxyXG4gICAgYXV0b0ZpdCxcclxuICAgIGV4cEFsbERhdGEsXHJcbiAgICBleHBFeGNlbExpbmssXHJcbiAgICBleHBFbWFpbFJlcG9ydCxcclxuICAgIGFkdkRlbVxyXG59XHJcblxyXG5leHBvcnQgZW51bSBDR0ZGZWF0dXJlVXBkYXRlRW51bSB7XHJcbiAgICBjbGVhcixcclxuICAgIGN1c3RvbUNvbHVtblVwZGF0ZWQsXHJcbiAgICBjYWNoZWREYXRhXHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIFNlY3VyaXR5TWFzdGVyQWN0aW9uIHtcclxuICAgIEFkZFNlY3VyaXR5LFxyXG4gICAgRWRpdFNlY3VyaXR5LFxyXG59XHJcbiJdfQ==