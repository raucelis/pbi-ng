import notify from 'devextreme/ui/notify';
import { SecurityMasterAction } from './enums';
import { actionTypes } from './constants';
// this is duplicate function specific to models.
export function getStorageKey(gridInstance, keyValue, isMasterGrid = true) {
    const masterDetailInfo = gridInstance === null || gridInstance === void 0 ? void 0 : gridInstance.option('masterDetail');
    return isMasterGrid ? keyValue : `${keyValue}_${masterDetailInfo.template}`;
}
export function getClassNameByThemeName(themeName) {
    switch (themeName) {
        case 'np.compact':
        case 'dx-swatch-default':
            return 'dx-swatch-default';
        case 'light.compact':
        case 'dx-swatch-regular':
            return 'dx-swatch-regular';
        case 'light.regular':
        default:
            return '';
    }
}
export function applyFilterCssClass(filter = [], gridInstance) {
    let i, cssClasses, columnInfo;
    for (i = 0; filter && i < filter.length; i++) {
        if (Array.isArray(filter[i])) {
            if (Array.isArray(filter[i][0])) {
                applyFilterCssClass(filter[i], gridInstance);
            }
            else {
                columnInfo = gridInstance.columnOption(filter[i][0]);
                if (columnInfo && (columnInfo.dataField === filter[i][0])) {
                    cssClasses = gridInstance.columnOption(filter[i][0], 'cssClass');
                    gridInstance.columnOption(filter[i][0], 'cssClass', cssClasses + ' filterApplied');
                }
            }
        }
        else {
            columnInfo = gridInstance.columnOption(filter[i]);
            if (columnInfo && (columnInfo.dataField === filter[i])) {
                cssClasses = gridInstance.columnOption(filter[i], 'cssClass');
                gridInstance.columnOption(filter[i], 'cssClass', cssClasses ? cssClasses + ' filterApplied' : 'filterApplied');
            }
        }
    }
}
export function applyFiltersToGrid(gridInstance, filterValue) {
    if (gridInstance) {
        // const listOfVisibleColumns = gridInstance.getVisibleColumns();
        // let cssClasses: string;
        // for (let i = 0; i < listOfVisibleColumns.length; i++) {
        //     cssClasses = gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass');
        //     if (cssClasses) {
        //         gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass', cssClasses.replace(new RegExp('filterApplied', 'g'), '').trim());
        //     }
        // }
        if (filterValue) {
            // applyFilterCssClass(filterValue, gridInstance);
            const alertOptions = {
                type: 'warning', message: 'To permanently apply this filter,please go to the \'View Selection\' and click \'Save\''
                    + ' in order to associate this filter with a view.'
            };
            appToast(alertOptions);
        }
    }
}
export function appToast(options) {
    const defaultOptions = {
        message: 'Oops.Something went wrong !',
        closeOnClick: true,
        width: '550px',
        type: 'error',
        displayTime: 3000,
        position: {
            my: 'center top',
            at: 'center top'
        },
    };
    // * extend the below switch for other toast(notify) types
    if (typeof options.type === 'string') {
        switch (options.type.toLowerCase()) {
            case 'warning':
                defaultOptions.displayTime = 10000;
                break;
            default:
                defaultOptions.displayTime = 3000;
                break;
        }
    }
    options = Object.assign({}, defaultOptions, options);
    notify(options);
}
export function ToDictionary(array, key) {
    const dic = {};
    array.forEach(e => {
        const k = key(e);
        if (!dic.hasOwnProperty(k)) {
            dic[key(e)] = e;
        }
        else {
            console.warn('Duplicate Key in Array', e);
        }
        ;
    });
    return dic;
}
export function filterSelectedRowData(gridInstance, rowData) {
    const visibleColumns = gridInstance.getVisibleColumns().map((item) => {
        return item['dataField'];
    });
    let selectedRowData = [];
    if (rowData && Object.keys(rowData).length && Object.keys(rowData.data).length) {
        selectedRowData.push(JSON.parse(JSON.stringify(rowData.data)));
    }
    else {
        selectedRowData = JSON.parse(JSON.stringify(gridInstance.getSelectedRowsData()));
    }
    if (selectedRowData.length) {
        for (const columnProperty in selectedRowData[0]) {
            if (!visibleColumns.includes(columnProperty)) {
                delete selectedRowData[0][columnProperty];
            }
        }
    }
    return selectedRowData;
}
export function openDestination(data, selectedRowData, entityParameterInfo = [], appService = null) {
    var _a, _b;
    let urlDynamicParamCount = 0, paramFilter = [], url, urlParams, paramterMapping, delimiter, destinationUrl, staticParams;
    let undefinedSourceColumnNames = '';
    const comma = ',';
    switch ((_a = data.DestinationType) === null || _a === void 0 ? void 0 : _a.toLowerCase().trim()) {
        case actionTypes.addSecurity.toLowerCase():
            if (appService) {
                appService.showSM({ SecurityId: null, Action: SecurityMasterAction.AddSecurity });
            }
            return;
        case actionTypes.dispatcherJob.toLowerCase():
            url = `${location.href.split('#')[0]}#/DispatcherManagement?webMenuId=${getWebMenuIdByKey('DispatcherManagement')}&jobId=${data.jobId}`;
            open(url, '_blank');
            return;
        case actionTypes.lookup.toLowerCase():
            url = `${location.href.split('#')[0]}#/LookupTables?webMenuId=${getWebMenuIdByKey('LookupTables')}&lookupId=${data.LookupId}`;
            open(url, '_blank');
            return;
        case actionTypes.entity.toLowerCase():
        case actionTypes.editSecurity.toLowerCase():
        case actionTypes.url.toLowerCase():
            if (selectedRowData && selectedRowData.length) {
                switch ((_b = data.DestinationType) === null || _b === void 0 ? void 0 : _b.toLowerCase()) {
                    case actionTypes.editSecurity.toLowerCase():
                        if (data.ShowAsContextMenu) {
                            return true;
                        }
                        else {
                            let secKey = '';
                            for (const key in selectedRowData[0]) {
                                if (selectedRowData[0].hasOwnProperty(key) && (key.toLowerCase() === 'securityid' || key.toLowerCase() === 'security id')) {
                                    secKey = key;
                                }
                            }
                            if (selectedRowData[0][secKey] && appService) {
                                appService.showSM({ SecurityId: selectedRowData[0][secKey], Action: SecurityMasterAction.EditSecurity });
                                return true;
                            }
                            else {
                                appToast({ type: 'error', message: `Following column(s) Security Id is required. Please make sure same exist(s) in the grid with value.`, displayTime: 4000 });
                                return false;
                            }
                        }
                        break;
                    case actionTypes.entity.toLowerCase():
                        const queryParams = location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                        queryParams['entity'] = data.DestinationEntity;
                        queryParams['webMenuId'] = data.DestinationWebMenuItemId;
                        queryParams['layout'] = 0;
                        queryParams['gridname'] = data.DestinationEntityName + '_' + data.DestinationEntity;
                        paramterMapping = data.ParameterMapping;
                        const paramInfo = extractDestinationEntityParameters(selectedRowData, paramterMapping, entityParameterInfo);
                        undefinedSourceColumnNames = paramInfo.undefinedSourceColumnNames;
                        url = location.hash.split('?')[0] + '?webMenuId=' + queryParams['webMenuId'] + '&entity=' + queryParams['entity'] +
                            '&gridname=' + queryParams['gridname'] + paramInfo.param + '&layout=' + queryParams['layout'];
                        break;
                    case actionTypes.url.toLowerCase():
                        destinationUrl = data.DestinationUrl.split('?'), paramterMapping = data.ParameterMapping;
                        url = destinationUrl[0], staticParams = destinationUrl[1];
                        if (staticParams !== undefined) {
                            url = url + '?' + staticParams;
                        }
                        urlParams = url.match(/{[^{}]*}/gi);
                        if (urlParams != null) {
                            urlParams.forEach((item) => {
                                paramterMapping.forEach((paramItem) => {
                                    if (paramItem.ParameterName === item.substr(1).slice(0, -1)) {
                                        if (paramItem.IsSourceParam) {
                                            const sourceParamName = paramItem.SourceColumnName.split('@')[1].trim().toLowerCase();
                                            const parameter = entityParameterInfo.filter(par => par.Name.trim().toLowerCase() === sourceParamName
                                                || par.Name.trim().toLowerCase() === paramItem.SourceColumnName.trim().toLowerCase())[0];
                                            if (parameter.Value) {
                                                url = url.replace(item, checkAndFormatDate(parameter.Value));
                                            }
                                            else {
                                                undefinedSourceColumnNames += paramItem.SourceColumnName + comma;
                                            }
                                        }
                                        else {
                                            if (selectedRowData[0][item.SourceColumnName] || selectedRowData[0][paramItem.SourceColumnName]) {
                                                url = url.replace(item, checkAndFormatDate(selectedRowData[0][paramItem.SourceColumnName]));
                                            }
                                            else {
                                                undefinedSourceColumnNames += item.SourceColumnName ? item.SourceColumnName + comma : item + comma;
                                            }
                                        }
                                    }
                                });
                            });
                        }
                        paramterMapping.forEach((item, index) => {
                            if (urlParams != null) {
                                paramFilter = urlParams.filter((urlItem) => item.ParameterName === urlItem.substr(1).slice(0, -1));
                            }
                            if (!paramFilter.length) {
                                delimiter = (urlDynamicParamCount === 0 && staticParams === undefined) ? '?' : '&';
                                if (selectedRowData[0][item.SourceColumnName]) {
                                    url = url + delimiter + item.ParameterName + '=' + checkAndFormatDate(selectedRowData[0][item.SourceColumnName]);
                                }
                                else {
                                    undefinedSourceColumnNames += item.SourceColumnName + comma;
                                }
                                urlDynamicParamCount++;
                            }
                        });
                        break;
                }
                if (!undefinedSourceColumnNames) {
                    open(url, '_blank');
                }
                else {
                    if (undefinedSourceColumnNames.endsWith(comma)) {
                        undefinedSourceColumnNames = undefinedSourceColumnNames.substring(0, undefinedSourceColumnNames.length - 1);
                        appToast({
                            type: 'error', message: `Following column(s) ${undefinedSourceColumnNames} is required. Please make sure same exist(s) in the grid or parameter with value.`,
                            displayTime: 4000
                        });
                    }
                }
            }
            else {
                appToast({ type: 'warning', message: 'Please select a row.' });
            }
    }
    return false;
}
export function extractDestinationEntityParameters(selectedRowData, parameterMapping, entityParameterInfo) {
    let param = '';
    let undefinedSourceColumnNames = '';
    const comma = ',';
    parameterMapping.forEach((item) => {
        const paramName = item.OriginalParameterName || item.ParameterName;
        if (item.IsSourceParam) {
            const sourceParamName = item.SourceColumnName.split('@')[1].trim().toLowerCase();
            const parameter = entityParameterInfo.filter(par => par.Name.trim().toLowerCase() === sourceParamName
                || par.Name.trim().toLowerCase() === item.SourceColumnName.trim().toLowerCase())[0];
            if (parameter === null || parameter === void 0 ? void 0 : parameter.Value) {
                param = param + '&' + paramName + '=' + checkAndFormatDate(parameter.Value);
            }
            else {
                undefinedSourceColumnNames += item.SourceColumnName + comma;
            }
        }
        else {
            if (selectedRowData[0][item.SourceColumnName]) {
                param = param + '&' + paramName + '=' + checkAndFormatDate(selectedRowData[0][item.SourceColumnName]);
            }
            else {
                undefinedSourceColumnNames += item.SourceColumnName + comma;
            }
        }
    });
    return { param, undefinedSourceColumnNames };
}
export function convertDateToUsFormat(value) {
    const dateValue = new Date(value);
    let dd = dateValue.getDate();
    let mm = dateValue.getMonth() + 1;
    const yyyy = dateValue.getFullYear();
    dd = dd < 10 ? '0' + dd : dd;
    mm = mm < 10 ? '0' + mm : mm;
    return `${mm}/${dd}/${yyyy}`;
}
/**
 * this method will return web menu id based on key
 * @param  {string} key
 * @returns number
 */
function getWebMenuIdByKey(key) {
    const webMenus = window['menuData'] || [];
    if (webMenus.length) {
        const filteredItem = webMenus.filter((arrItem) => { var _a; return ((_a = arrItem.Key) === null || _a === void 0 ? void 0 : _a.toLowerCase()) === key.toLowerCase(); })[0];
        return filteredItem ? filteredItem.OptomasToolId : undefined;
    }
}
// convert from YYYY-MM-DDTHH:mm:ss.SSS to MM/DD/YYYY, else return the param as it is
function checkAndFormatDate(value) {
    // const moment = window['moment'];
    // // ref: https://flaviocopes.com/momentjs/
    // if (moment(value, moment.HTML5_FMT.DATETIME_LOCAL_MS, true).isValid()) {
    //     return moment(value).format('MM/DD/YYYY');
    // } else {
    //     return value;
    // }
    return value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbGl0eUZ1bmN0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxNQUFNLE1BQU0sc0JBQXNCLENBQUM7QUFHMUMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sU0FBUyxDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFMUMsaURBQWlEO0FBQ2pELE1BQU0sVUFBVSxhQUFhLENBQUMsWUFBd0IsRUFBRSxRQUFnQixFQUFFLFlBQVksR0FBRyxJQUFJO0lBQ3pGLE1BQU0sZ0JBQWdCLEdBQUcsWUFBWSxhQUFaLFlBQVksdUJBQVosWUFBWSxDQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUM5RCxPQUFPLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsSUFBSSxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsQ0FBQztBQUNoRixDQUFDO0FBRUQsTUFBTSxVQUFVLHVCQUF1QixDQUFDLFNBQWlCO0lBQ3JELFFBQVEsU0FBUyxFQUFFO1FBQ2YsS0FBSyxZQUFZLENBQUM7UUFDbEIsS0FBSyxtQkFBbUI7WUFDcEIsT0FBTyxtQkFBbUIsQ0FBQztRQUMvQixLQUFLLGVBQWUsQ0FBQztRQUNyQixLQUFLLG1CQUFtQjtZQUNwQixPQUFPLG1CQUFtQixDQUFDO1FBQy9CLEtBQUssZUFBZSxDQUFDO1FBQ3JCO1lBQ0ksT0FBTyxFQUFFLENBQUM7S0FDakI7QUFDTCxDQUFDO0FBRUQsTUFBTSxVQUFVLG1CQUFtQixDQUFDLE1BQU0sR0FBRyxFQUFFLEVBQUUsWUFBWTtJQUN6RCxJQUFJLENBQVMsRUFBRSxVQUFrQixFQUFFLFVBQXlCLENBQUM7SUFDN0QsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLE1BQU0sSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUMxQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDMUIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUM3QixtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsWUFBWSxDQUFDLENBQUM7YUFDaEQ7aUJBQU07Z0JBQ0gsVUFBVSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JELElBQUksVUFBVSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDdkQsVUFBVSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO29CQUNqRSxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLEVBQUUsVUFBVSxHQUFHLGdCQUFnQixDQUFDLENBQUM7aUJBQ3RGO2FBQ0o7U0FDSjthQUFNO1lBQ0gsVUFBVSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEQsSUFBSSxVQUFVLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNwRCxVQUFVLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQzlELFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDbEg7U0FDSjtLQUNKO0FBQ0wsQ0FBQztBQUVELE1BQU0sVUFBVSxrQkFBa0IsQ0FBQyxZQUF3QixFQUFFLFdBQXdCO0lBQ2pGLElBQUksWUFBWSxFQUFFO1FBQ2QsaUVBQWlFO1FBQ2pFLDBCQUEwQjtRQUMxQiwwREFBMEQ7UUFDMUQsNkZBQTZGO1FBQzdGLHdCQUF3QjtRQUN4QixxSkFBcUo7UUFDckosUUFBUTtRQUNSLElBQUk7UUFFSixJQUFJLFdBQVcsRUFBRTtZQUNiLGtEQUFrRDtZQUNsRCxNQUFNLFlBQVksR0FBRztnQkFDakIsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUseUZBQXlGO3NCQUM3RyxpREFBaUQ7YUFDMUQsQ0FBQztZQUNGLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUMxQjtLQUNKO0FBQ0wsQ0FBQztBQUVELE1BQU0sVUFBVSxRQUFRLENBQUMsT0FBd0I7SUFDN0MsTUFBTSxjQUFjLEdBQUc7UUFDbkIsT0FBTyxFQUFFLDZCQUE2QjtRQUN0QyxZQUFZLEVBQUUsSUFBSTtRQUNsQixLQUFLLEVBQUUsT0FBTztRQUNkLElBQUksRUFBRSxPQUFPO1FBQ2IsV0FBVyxFQUFFLElBQUk7UUFDakIsUUFBUSxFQUFFO1lBQ04sRUFBRSxFQUFFLFlBQVk7WUFDaEIsRUFBRSxFQUFFLFlBQVk7U0FDbkI7S0FDSixDQUFDO0lBQ0YsMERBQTBEO0lBQzFELElBQUksT0FBTyxPQUFPLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtRQUNsQyxRQUFRLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDaEMsS0FBSyxTQUFTO2dCQUNWLGNBQWMsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2dCQUNuQyxNQUFNO1lBQ1Y7Z0JBQ0ksY0FBYyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7Z0JBQ2xDLE1BQU07U0FDYjtLQUNKO0lBRUQsT0FBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLGNBQWMsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNyRCxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDcEIsQ0FBQztBQUdELE1BQU0sVUFBVSxZQUFZLENBQUksS0FBVSxFQUFFLEdBQW9DO0lBQzVFLE1BQU0sR0FBRyxHQUF5QixFQUFFLENBQUM7SUFDckMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtRQUNkLE1BQU0sQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqQixJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN4QixHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQ25CO2FBQU07WUFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQzdDO1FBQUEsQ0FBQztJQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ0gsT0FBTyxHQUFHLENBQUM7QUFDZixDQUFDO0FBRUQsTUFBTSxVQUFVLHFCQUFxQixDQUFDLFlBQVksRUFBRSxPQUFhO0lBQzdELE1BQU0sY0FBYyxHQUFHLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1FBQ2pFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ0gsSUFBSSxlQUFlLEdBQUcsRUFBRSxDQUFDO0lBQ3pCLElBQUksT0FBTyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRTtRQUM1RSxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0tBQ2xFO1NBQU07UUFDSCxlQUFlLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQztLQUNwRjtJQUNELElBQUksZUFBZSxDQUFDLE1BQU0sRUFBRTtRQUN4QixLQUFLLE1BQU0sY0FBYyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUM3QyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDMUMsT0FBTyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDN0M7U0FDSjtLQUNKO0lBQ0QsT0FBTyxlQUFlLENBQUM7QUFDM0IsQ0FBQztBQUVELE1BQU0sVUFBVSxlQUFlLENBQUMsSUFBSSxFQUFFLGVBQW9CLEVBQUUsc0JBQTZCLEVBQUUsRUFBRSxVQUFVLEdBQUcsSUFBSTs7SUFDMUcsSUFBSSxvQkFBb0IsR0FBRyxDQUFDLEVBQUUsV0FBVyxHQUFHLEVBQUUsRUFBRSxHQUFXLEVBQUUsU0FBUyxFQUFFLGVBQXNCLEVBQUUsU0FBaUIsRUFBRSxjQUFzQixFQUFFLFlBQW9CLENBQUM7SUFDaEssSUFBSSwwQkFBMEIsR0FBRyxFQUFFLENBQUM7SUFDcEMsTUFBTSxLQUFLLEdBQUcsR0FBRyxDQUFDO0lBRWxCLGNBQVEsSUFBSSxDQUFDLGVBQWUsMENBQUUsV0FBVyxHQUFHLElBQUksSUFBSTtRQUNoRCxLQUFLLFdBQVcsQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFO1lBQ3RDLElBQUksVUFBVSxFQUFFO2dCQUNaLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxvQkFBb0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO2FBQ3JGO1lBQ0QsT0FBTztRQUNYLEtBQUssV0FBVyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUU7WUFDeEMsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLG9DQUFvQyxpQkFBaUIsQ0FBQyxzQkFBc0IsQ0FBQyxVQUFVLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUN4SSxJQUFJLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3BCLE9BQU87UUFDWCxLQUFLLFdBQVcsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFO1lBQ2pDLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyw0QkFBNEIsaUJBQWlCLENBQUMsY0FBYyxDQUFDLGFBQWEsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQzlILElBQUksQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDcEIsT0FBTztRQUNYLEtBQUssV0FBVyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN0QyxLQUFLLFdBQVcsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDNUMsS0FBSyxXQUFXLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRTtZQUM5QixJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsTUFBTSxFQUFFO2dCQUMzQyxjQUFRLElBQUksQ0FBQyxlQUFlLDBDQUFFLFdBQVcsSUFBSTtvQkFDekMsS0FBSyxXQUFXLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRTt3QkFDdkMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7NEJBQ3hCLE9BQU8sSUFBSSxDQUFDO3lCQUNmOzZCQUFNOzRCQUNILElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQzs0QkFDaEIsS0FBSyxNQUFNLEdBQUcsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0NBQ2xDLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxZQUFZLElBQUksR0FBRyxDQUFDLFdBQVcsRUFBRSxLQUFLLGFBQWEsQ0FBQyxFQUFFO29DQUN2SCxNQUFNLEdBQUcsR0FBRyxDQUFDO2lDQUNoQjs2QkFDSjs0QkFDRCxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxVQUFVLEVBQUU7Z0NBQzFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRSxVQUFVLEVBQUUsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLE1BQU0sRUFBRSxvQkFBb0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFBO2dDQUN4RyxPQUFPLElBQUksQ0FBQzs2QkFDZjtpQ0FBTTtnQ0FDSCxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxxR0FBcUcsRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztnQ0FDL0osT0FBTyxLQUFLLENBQUM7NkJBQ2hCO3lCQUNKO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxXQUFXLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRTt3QkFDakMsTUFBTSxXQUFXLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDMUYsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDL0MsV0FBVyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQzt3QkFDekQsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDMUIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO3dCQUVwRixlQUFlLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO3dCQUN4QyxNQUFNLFNBQVMsR0FBRyxrQ0FBa0MsQ0FBQyxlQUFlLEVBQUUsZUFBZSxFQUFFLG1CQUFtQixDQUFDLENBQUM7d0JBQzVHLDBCQUEwQixHQUFHLFNBQVMsQ0FBQywwQkFBMEIsQ0FBQzt3QkFDbEUsR0FBRyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLGFBQWEsR0FBRyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsVUFBVSxHQUFHLFdBQVcsQ0FBQyxRQUFRLENBQUM7NEJBQzdHLFlBQVksR0FBRyxXQUFXLENBQUMsVUFBVSxDQUFDLEdBQUcsU0FBUyxDQUFDLEtBQUssR0FBRyxVQUFVLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUNsRyxNQUFNO29CQUNWLEtBQUssV0FBVyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUU7d0JBQzlCLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxlQUFlLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO3dCQUN6RixHQUFHLEdBQUcsY0FBYyxDQUFDLENBQUMsQ0FBQyxFQUFFLFlBQVksR0FBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQzFELElBQUksWUFBWSxLQUFLLFNBQVMsRUFBRTs0QkFDNUIsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsWUFBWSxDQUFDO3lCQUNsQzt3QkFDRCxTQUFTLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxTQUFTLElBQUksSUFBSSxFQUFFOzRCQUNuQixTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0NBQ3ZCLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtvQ0FDbEMsSUFBSSxTQUFTLENBQUMsYUFBYSxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO3dDQUN6RCxJQUFJLFNBQVMsQ0FBQyxhQUFhLEVBQUU7NENBQ3pCLE1BQU0sZUFBZSxHQUFHLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7NENBQ3RGLE1BQU0sU0FBUyxHQUFHLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssZUFBZTttREFDOUYsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0Q0FFN0YsSUFBSSxTQUFTLENBQUMsS0FBSyxFQUFFO2dEQUNqQixHQUFHLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsa0JBQWtCLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7NkNBQ2hFO2lEQUFNO2dEQUNILDBCQUEwQixJQUFJLFNBQVMsQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7NkNBQ3BFO3lDQUNKOzZDQUFNOzRDQUNILElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsRUFBRTtnREFDN0YsR0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUM7NkNBQy9GO2lEQUFNO2dEQUNILDBCQUEwQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQzs2Q0FDdEc7eUNBQ0o7cUNBQ0o7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsQ0FBQyxDQUFDLENBQUM7eUJBQ047d0JBQ0QsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTs0QkFDcEMsSUFBSSxTQUFTLElBQUksSUFBSSxFQUFFO2dDQUNuQixXQUFXLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsS0FBSyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUN0Rzs0QkFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRTtnQ0FDckIsU0FBUyxHQUFHLENBQUMsb0JBQW9CLEtBQUssQ0FBQyxJQUFJLFlBQVksS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7Z0NBQ25GLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO29DQUMzQyxHQUFHLEdBQUcsR0FBRyxHQUFHLFNBQVMsR0FBRyxJQUFJLENBQUMsYUFBYSxHQUFHLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztpQ0FDcEg7cUNBQU07b0NBQ0gsMEJBQTBCLElBQUksSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztpQ0FDL0Q7Z0NBQ0Qsb0JBQW9CLEVBQUUsQ0FBQzs2QkFDMUI7d0JBQ0wsQ0FBQyxDQUFDLENBQUM7d0JBQ0gsTUFBTTtpQkFDYjtnQkFDRCxJQUFJLENBQUMsMEJBQTBCLEVBQUU7b0JBQzdCLElBQUksQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILElBQUksMEJBQTBCLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUM1QywwQkFBMEIsR0FBRywwQkFBMEIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLDBCQUEwQixDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDNUcsUUFBUSxDQUFDOzRCQUNMLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLHVCQUF1QiwwQkFBMEIsbUZBQW1GOzRCQUMxSixXQUFXLEVBQUUsSUFBSTt5QkFDdEIsQ0FBQyxDQUFDO3FCQUNOO2lCQUNKO2FBQ0o7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsQ0FBQyxDQUFDO2FBQ2xFO0tBQ1I7SUFDRCxPQUFPLEtBQUssQ0FBQztBQUNqQixDQUFDO0FBRUQsTUFBTSxVQUFVLGtDQUFrQyxDQUFDLGVBQXNCLEVBQUUsZ0JBQXVCLEVBQUUsbUJBQTBCO0lBQzFILElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUNmLElBQUksMEJBQTBCLEdBQUcsRUFBRSxDQUFDO0lBQ3BDLE1BQU0sS0FBSyxHQUFHLEdBQUcsQ0FBQztJQUNsQixnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtRQUM5QixNQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMscUJBQXFCLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUNuRSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDcEIsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNqRixNQUFNLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxLQUFLLGVBQWU7bUJBQzlGLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFeEYsSUFBSSxTQUFTLGFBQVQsU0FBUyx1QkFBVCxTQUFTLENBQUUsS0FBSyxFQUFFO2dCQUNsQixLQUFLLEdBQUcsS0FBSyxHQUFHLEdBQUcsR0FBRyxTQUFTLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUMvRTtpQkFBTTtnQkFDSCwwQkFBMEIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2FBQy9EO1NBQ0o7YUFBTTtZQUNILElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO2dCQUMzQyxLQUFLLEdBQUcsS0FBSyxHQUFHLEdBQUcsR0FBRyxTQUFTLEdBQUcsR0FBRyxHQUFHLGtCQUFrQixDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO2FBQ3pHO2lCQUFNO2dCQUNILDBCQUEwQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7YUFDL0Q7U0FDSjtJQUNMLENBQUMsQ0FBQyxDQUFDO0lBRUgsT0FBTyxFQUFFLEtBQUssRUFBRSwwQkFBMEIsRUFBRSxDQUFDO0FBQ2pELENBQUM7QUFFRCxNQUFNLFVBQVUscUJBQXFCLENBQUMsS0FBb0I7SUFDdEQsTUFBTSxTQUFTLEdBQUcsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsSUFBSSxFQUFFLEdBQW9CLFNBQVMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUFDLElBQUksRUFBRSxHQUFvQixTQUFTLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQUMsTUFBTSxJQUFJLEdBQUcsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3hJLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDN0IsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUM3QixPQUFPLEdBQUcsRUFBRSxJQUFJLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQztBQUNqQyxDQUFDO0FBR0Q7Ozs7R0FJRztBQUNILFNBQVMsaUJBQWlCLENBQUMsR0FBVztJQUNsQyxNQUFNLFFBQVEsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzFDLElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTtRQUNqQixNQUFNLFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUUsV0FBQyxPQUFBLE9BQUEsT0FBTyxDQUFDLEdBQUcsMENBQUUsV0FBVyxRQUFPLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQSxFQUFBLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2RyxPQUFPLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO0tBQ2hFO0FBQ0wsQ0FBQztBQUVELHFGQUFxRjtBQUNyRixTQUFTLGtCQUFrQixDQUFDLEtBQWE7SUFDckMsbUNBQW1DO0lBQ25DLDRDQUE0QztJQUM1QywyRUFBMkU7SUFDM0UsaURBQWlEO0lBQ2pELFdBQVc7SUFDWCxvQkFBb0I7SUFDcEIsSUFBSTtJQUNKLE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZHhEYXRhR3JpZCBmcm9tICdkZXZleHRyZW1lL3VpL2RhdGFfZ3JpZCc7XHJcbmltcG9ydCBub3RpZnkgZnJvbSAnZGV2ZXh0cmVtZS91aS9ub3RpZnknO1xyXG5pbXBvcnQgeyBBcHBUb2FzdE9wdGlvbnMgfSBmcm9tICcuLi9jb21wb25lbnRzL2NvbnRyYWN0cy9kZXZleHRyZW1lV2lkZ2V0cyc7XHJcbmltcG9ydCB7IFBCSUdyaWRDb2x1bW4gfSBmcm9tICcuLi9jb21wb25lbnRzL2NvbnRyYWN0cy9ncmlkJztcclxuaW1wb3J0IHsgU2VjdXJpdHlNYXN0ZXJBY3Rpb24gfSBmcm9tICcuL2VudW1zJztcclxuaW1wb3J0IHsgYWN0aW9uVHlwZXMgfSBmcm9tICcuL2NvbnN0YW50cyc7XHJcblxyXG4vLyB0aGlzIGlzIGR1cGxpY2F0ZSBmdW5jdGlvbiBzcGVjaWZpYyB0byBtb2RlbHMuXHJcbmV4cG9ydCBmdW5jdGlvbiBnZXRTdG9yYWdlS2V5KGdyaWRJbnN0YW5jZTogZHhEYXRhR3JpZCwga2V5VmFsdWU6IHN0cmluZywgaXNNYXN0ZXJHcmlkID0gdHJ1ZSk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBtYXN0ZXJEZXRhaWxJbmZvID0gZ3JpZEluc3RhbmNlPy5vcHRpb24oJ21hc3RlckRldGFpbCcpO1xyXG4gICAgcmV0dXJuIGlzTWFzdGVyR3JpZCA/IGtleVZhbHVlIDogYCR7a2V5VmFsdWV9XyR7bWFzdGVyRGV0YWlsSW5mby50ZW1wbGF0ZX1gO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZ2V0Q2xhc3NOYW1lQnlUaGVtZU5hbWUodGhlbWVOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgc3dpdGNoICh0aGVtZU5hbWUpIHtcclxuICAgICAgICBjYXNlICducC5jb21wYWN0JzpcclxuICAgICAgICBjYXNlICdkeC1zd2F0Y2gtZGVmYXVsdCc6XHJcbiAgICAgICAgICAgIHJldHVybiAnZHgtc3dhdGNoLWRlZmF1bHQnO1xyXG4gICAgICAgIGNhc2UgJ2xpZ2h0LmNvbXBhY3QnOlxyXG4gICAgICAgIGNhc2UgJ2R4LXN3YXRjaC1yZWd1bGFyJzpcclxuICAgICAgICAgICAgcmV0dXJuICdkeC1zd2F0Y2gtcmVndWxhcic7XHJcbiAgICAgICAgY2FzZSAnbGlnaHQucmVndWxhcic6XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gYXBwbHlGaWx0ZXJDc3NDbGFzcyhmaWx0ZXIgPSBbXSwgZ3JpZEluc3RhbmNlKTogdm9pZCB7XHJcbiAgICBsZXQgaTogbnVtYmVyLCBjc3NDbGFzc2VzOiBzdHJpbmcsIGNvbHVtbkluZm86IFBCSUdyaWRDb2x1bW47XHJcbiAgICBmb3IgKGkgPSAwOyBmaWx0ZXIgJiYgaSA8IGZpbHRlci5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGZpbHRlcltpXSkpIHtcclxuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZmlsdGVyW2ldWzBdKSkge1xyXG4gICAgICAgICAgICAgICAgYXBwbHlGaWx0ZXJDc3NDbGFzcyhmaWx0ZXJbaV0sIGdyaWRJbnN0YW5jZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb2x1bW5JbmZvID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV1bMF0pO1xyXG4gICAgICAgICAgICAgICAgaWYgKGNvbHVtbkluZm8gJiYgKGNvbHVtbkluZm8uZGF0YUZpZWxkID09PSBmaWx0ZXJbaV1bMF0pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY3NzQ2xhc3NlcyA9IGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldWzBdLCAnY3NzQ2xhc3MnKTtcclxuICAgICAgICAgICAgICAgICAgICBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXVswXSwgJ2Nzc0NsYXNzJywgY3NzQ2xhc3NlcyArICcgZmlsdGVyQXBwbGllZCcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29sdW1uSW5mbyA9IGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldKTtcclxuICAgICAgICAgICAgaWYgKGNvbHVtbkluZm8gJiYgKGNvbHVtbkluZm8uZGF0YUZpZWxkID09PSBmaWx0ZXJbaV0pKSB7XHJcbiAgICAgICAgICAgICAgICBjc3NDbGFzc2VzID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV0sICdjc3NDbGFzcycpO1xyXG4gICAgICAgICAgICAgICAgZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV0sICdjc3NDbGFzcycsIGNzc0NsYXNzZXMgPyBjc3NDbGFzc2VzICsgJyBmaWx0ZXJBcHBsaWVkJyA6ICdmaWx0ZXJBcHBsaWVkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBhcHBseUZpbHRlcnNUb0dyaWQoZ3JpZEluc3RhbmNlOiBkeERhdGFHcmlkLCBmaWx0ZXJWYWx1ZT86IEFycmF5PGFueT4pOiB2b2lkIHtcclxuICAgIGlmIChncmlkSW5zdGFuY2UpIHtcclxuICAgICAgICAvLyBjb25zdCBsaXN0T2ZWaXNpYmxlQ29sdW1ucyA9IGdyaWRJbnN0YW5jZS5nZXRWaXNpYmxlQ29sdW1ucygpO1xyXG4gICAgICAgIC8vIGxldCBjc3NDbGFzc2VzOiBzdHJpbmc7XHJcbiAgICAgICAgLy8gZm9yIChsZXQgaSA9IDA7IGkgPCBsaXN0T2ZWaXNpYmxlQ29sdW1ucy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIC8vICAgICBjc3NDbGFzc2VzID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihsaXN0T2ZWaXNpYmxlQ29sdW1uc1tpXS5kYXRhRmllbGQsICdjc3NDbGFzcycpO1xyXG4gICAgICAgIC8vICAgICBpZiAoY3NzQ2xhc3Nlcykge1xyXG4gICAgICAgIC8vICAgICAgICAgZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihsaXN0T2ZWaXNpYmxlQ29sdW1uc1tpXS5kYXRhRmllbGQsICdjc3NDbGFzcycsIGNzc0NsYXNzZXMucmVwbGFjZShuZXcgUmVnRXhwKCdmaWx0ZXJBcHBsaWVkJywgJ2cnKSwgJycpLnRyaW0oKSk7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIGlmIChmaWx0ZXJWYWx1ZSkge1xyXG4gICAgICAgICAgICAvLyBhcHBseUZpbHRlckNzc0NsYXNzKGZpbHRlclZhbHVlLCBncmlkSW5zdGFuY2UpO1xyXG4gICAgICAgICAgICBjb25zdCBhbGVydE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnd2FybmluZycsIG1lc3NhZ2U6ICdUbyBwZXJtYW5lbnRseSBhcHBseSB0aGlzIGZpbHRlcixwbGVhc2UgZ28gdG8gdGhlIFxcJ1ZpZXcgU2VsZWN0aW9uXFwnIGFuZCBjbGljayBcXCdTYXZlXFwnJ1xyXG4gICAgICAgICAgICAgICAgICAgICsgJyBpbiBvcmRlciB0byBhc3NvY2lhdGUgdGhpcyBmaWx0ZXIgd2l0aCBhIHZpZXcuJ1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICBhcHBUb2FzdChhbGVydE9wdGlvbnMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGFwcFRvYXN0KG9wdGlvbnM6IEFwcFRvYXN0T3B0aW9ucyk6IHZvaWQge1xyXG4gICAgY29uc3QgZGVmYXVsdE9wdGlvbnMgPSB7XHJcbiAgICAgICAgbWVzc2FnZTogJ09vcHMuU29tZXRoaW5nIHdlbnQgd3JvbmcgIScsXHJcbiAgICAgICAgY2xvc2VPbkNsaWNrOiB0cnVlLFxyXG4gICAgICAgIHdpZHRoOiAnNTUwcHgnLFxyXG4gICAgICAgIHR5cGU6ICdlcnJvcicsXHJcbiAgICAgICAgZGlzcGxheVRpbWU6IDMwMDAsXHJcbiAgICAgICAgcG9zaXRpb246IHtcclxuICAgICAgICAgICAgbXk6ICdjZW50ZXIgdG9wJyxcclxuICAgICAgICAgICAgYXQ6ICdjZW50ZXIgdG9wJ1xyXG4gICAgICAgIH0sXHJcbiAgICB9O1xyXG4gICAgLy8gKiBleHRlbmQgdGhlIGJlbG93IHN3aXRjaCBmb3Igb3RoZXIgdG9hc3Qobm90aWZ5KSB0eXBlc1xyXG4gICAgaWYgKHR5cGVvZiBvcHRpb25zLnR5cGUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgc3dpdGNoIChvcHRpb25zLnR5cGUudG9Mb3dlckNhc2UoKSkge1xyXG4gICAgICAgICAgICBjYXNlICd3YXJuaW5nJzpcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRPcHRpb25zLmRpc3BsYXlUaW1lID0gMTAwMDA7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRPcHRpb25zLmRpc3BsYXlUaW1lID0gMzAwMDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvcHRpb25zID0gT2JqZWN0LmFzc2lnbih7fSwgZGVmYXVsdE9wdGlvbnMsIG9wdGlvbnMpO1xyXG4gICAgbm90aWZ5KG9wdGlvbnMpO1xyXG59XHJcblxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIFRvRGljdGlvbmFyeTxUPihhcnJheTogVFtdLCBrZXk6IChlbGVtZW50OiBUKSA9PiBzdHJpbmcgfCBudW1iZXIpOiB7IFtrZXk6IHN0cmluZ106IFQgfSB7XHJcbiAgICBjb25zdCBkaWM6IHsgW2tleTogc3RyaW5nXTogVCB9ID0ge307XHJcbiAgICBhcnJheS5mb3JFYWNoKGUgPT4ge1xyXG4gICAgICAgIGNvbnN0IGsgPSBrZXkoZSk7XHJcbiAgICAgICAgaWYgKCFkaWMuaGFzT3duUHJvcGVydHkoaykpIHtcclxuICAgICAgICAgICAgZGljW2tleShlKV0gPSBlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybignRHVwbGljYXRlIEtleSBpbiBBcnJheScsIGUpO1xyXG4gICAgICAgIH07XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBkaWM7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBmaWx0ZXJTZWxlY3RlZFJvd0RhdGEoZ3JpZEluc3RhbmNlLCByb3dEYXRhPzogYW55KTogQXJyYXk8YW55PiB7XHJcbiAgICBjb25zdCB2aXNpYmxlQ29sdW1ucyA9IGdyaWRJbnN0YW5jZS5nZXRWaXNpYmxlQ29sdW1ucygpLm1hcCgoaXRlbSkgPT4ge1xyXG4gICAgICAgIHJldHVybiBpdGVtWydkYXRhRmllbGQnXTtcclxuICAgIH0pO1xyXG4gICAgbGV0IHNlbGVjdGVkUm93RGF0YSA9IFtdO1xyXG4gICAgaWYgKHJvd0RhdGEgJiYgT2JqZWN0LmtleXMocm93RGF0YSkubGVuZ3RoICYmIE9iamVjdC5rZXlzKHJvd0RhdGEuZGF0YSkubGVuZ3RoKSB7XHJcbiAgICAgICAgc2VsZWN0ZWRSb3dEYXRhLnB1c2goSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShyb3dEYXRhLmRhdGEpKSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNlbGVjdGVkUm93RGF0YSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoZ3JpZEluc3RhbmNlLmdldFNlbGVjdGVkUm93c0RhdGEoKSkpO1xyXG4gICAgfVxyXG4gICAgaWYgKHNlbGVjdGVkUm93RGF0YS5sZW5ndGgpIHtcclxuICAgICAgICBmb3IgKGNvbnN0IGNvbHVtblByb3BlcnR5IGluIHNlbGVjdGVkUm93RGF0YVswXSkge1xyXG4gICAgICAgICAgICBpZiAoIXZpc2libGVDb2x1bW5zLmluY2x1ZGVzKGNvbHVtblByb3BlcnR5KSkge1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIHNlbGVjdGVkUm93RGF0YVswXVtjb2x1bW5Qcm9wZXJ0eV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gc2VsZWN0ZWRSb3dEYXRhO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gb3BlbkRlc3RpbmF0aW9uKGRhdGEsIHNlbGVjdGVkUm93RGF0YTogYW55LCBlbnRpdHlQYXJhbWV0ZXJJbmZvOiBhbnlbXSA9IFtdLCBhcHBTZXJ2aWNlID0gbnVsbCk6IGJvb2xlYW4ge1xyXG4gICAgbGV0IHVybER5bmFtaWNQYXJhbUNvdW50ID0gMCwgcGFyYW1GaWx0ZXIgPSBbXSwgdXJsOiBzdHJpbmcsIHVybFBhcmFtcywgcGFyYW10ZXJNYXBwaW5nOiBhbnlbXSwgZGVsaW1pdGVyOiBzdHJpbmcsIGRlc3RpbmF0aW9uVXJsOiBzdHJpbmcsIHN0YXRpY1BhcmFtczogc3RyaW5nO1xyXG4gICAgbGV0IHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzID0gJyc7XHJcbiAgICBjb25zdCBjb21tYSA9ICcsJztcclxuXHJcbiAgICBzd2l0Y2ggKGRhdGEuRGVzdGluYXRpb25UeXBlPy50b0xvd2VyQ2FzZSgpLnRyaW0oKSkge1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuYWRkU2VjdXJpdHkudG9Mb3dlckNhc2UoKTpcclxuICAgICAgICAgICAgaWYgKGFwcFNlcnZpY2UpIHtcclxuICAgICAgICAgICAgICAgIGFwcFNlcnZpY2Uuc2hvd1NNKHsgU2VjdXJpdHlJZDogbnVsbCwgQWN0aW9uOiBTZWN1cml0eU1hc3RlckFjdGlvbi5BZGRTZWN1cml0eSB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5kaXNwYXRjaGVySm9iLnRvTG93ZXJDYXNlKCk6XHJcbiAgICAgICAgICAgIHVybCA9IGAke2xvY2F0aW9uLmhyZWYuc3BsaXQoJyMnKVswXX0jL0Rpc3BhdGNoZXJNYW5hZ2VtZW50P3dlYk1lbnVJZD0ke2dldFdlYk1lbnVJZEJ5S2V5KCdEaXNwYXRjaGVyTWFuYWdlbWVudCcpfSZqb2JJZD0ke2RhdGEuam9iSWR9YDtcclxuICAgICAgICAgICAgb3Blbih1cmwsICdfYmxhbmsnKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMubG9va3VwLnRvTG93ZXJDYXNlKCk6XHJcbiAgICAgICAgICAgIHVybCA9IGAke2xvY2F0aW9uLmhyZWYuc3BsaXQoJyMnKVswXX0jL0xvb2t1cFRhYmxlcz93ZWJNZW51SWQ9JHtnZXRXZWJNZW51SWRCeUtleSgnTG9va3VwVGFibGVzJyl9Jmxvb2t1cElkPSR7ZGF0YS5Mb29rdXBJZH1gO1xyXG4gICAgICAgICAgICBvcGVuKHVybCwgJ19ibGFuaycpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgY2FzZSBhY3Rpb25UeXBlcy5lbnRpdHkudG9Mb3dlckNhc2UoKTpcclxuICAgICAgICBjYXNlIGFjdGlvblR5cGVzLmVkaXRTZWN1cml0eS50b0xvd2VyQ2FzZSgpOlxyXG4gICAgICAgIGNhc2UgYWN0aW9uVHlwZXMudXJsLnRvTG93ZXJDYXNlKCk6XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3RlZFJvd0RhdGEgJiYgc2VsZWN0ZWRSb3dEYXRhLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChkYXRhLkRlc3RpbmF0aW9uVHlwZT8udG9Mb3dlckNhc2UoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuZWRpdFNlY3VyaXR5LnRvTG93ZXJDYXNlKCk6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLlNob3dBc0NvbnRleHRNZW51KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBzZWNLZXkgPSAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3Qga2V5IGluIHNlbGVjdGVkUm93RGF0YVswXSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZFJvd0RhdGFbMF0uaGFzT3duUHJvcGVydHkoa2V5KSAmJiAoa2V5LnRvTG93ZXJDYXNlKCkgPT09ICdzZWN1cml0eWlkJyB8fCBrZXkudG9Mb3dlckNhc2UoKSA9PT0gJ3NlY3VyaXR5IGlkJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2VjS2V5ID0ga2V5O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZFJvd0RhdGFbMF1bc2VjS2V5XSAmJiBhcHBTZXJ2aWNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBwU2VydmljZS5zaG93U00oeyBTZWN1cml0eUlkOiBzZWxlY3RlZFJvd0RhdGFbMF1bc2VjS2V5XSwgQWN0aW9uOiBTZWN1cml0eU1hc3RlckFjdGlvbi5FZGl0U2VjdXJpdHkgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnZXJyb3InLCBtZXNzYWdlOiBgRm9sbG93aW5nIGNvbHVtbihzKSBTZWN1cml0eSBJZCBpcyByZXF1aXJlZC4gUGxlYXNlIG1ha2Ugc3VyZSBzYW1lIGV4aXN0KHMpIGluIHRoZSBncmlkIHdpdGggdmFsdWUuYCwgZGlzcGxheVRpbWU6IDQwMDAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgYWN0aW9uVHlwZXMuZW50aXR5LnRvTG93ZXJDYXNlKCk6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHF1ZXJ5UGFyYW1zID0gbG9jYXRpb24uaHJlZi5zbGljZSh3aW5kb3cubG9jYXRpb24uaHJlZi5pbmRleE9mKCc/JykgKyAxKS5zcGxpdCgnJicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWVyeVBhcmFtc1snZW50aXR5J10gPSBkYXRhLkRlc3RpbmF0aW9uRW50aXR5O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWVyeVBhcmFtc1snd2ViTWVudUlkJ10gPSBkYXRhLkRlc3RpbmF0aW9uV2ViTWVudUl0ZW1JZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVlcnlQYXJhbXNbJ2xheW91dCddID0gMDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcXVlcnlQYXJhbXNbJ2dyaWRuYW1lJ10gPSBkYXRhLkRlc3RpbmF0aW9uRW50aXR5TmFtZSArICdfJyArIGRhdGEuRGVzdGluYXRpb25FbnRpdHk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXRlck1hcHBpbmcgPSBkYXRhLlBhcmFtZXRlck1hcHBpbmc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHBhcmFtSW5mbyA9IGV4dHJhY3REZXN0aW5hdGlvbkVudGl0eVBhcmFtZXRlcnMoc2VsZWN0ZWRSb3dEYXRhLCBwYXJhbXRlck1hcHBpbmcsIGVudGl0eVBhcmFtZXRlckluZm8pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcyA9IHBhcmFtSW5mby51bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcztcclxuICAgICAgICAgICAgICAgICAgICAgICAgdXJsID0gbG9jYXRpb24uaGFzaC5zcGxpdCgnPycpWzBdICsgJz93ZWJNZW51SWQ9JyArIHF1ZXJ5UGFyYW1zWyd3ZWJNZW51SWQnXSArICcmZW50aXR5PScgKyBxdWVyeVBhcmFtc1snZW50aXR5J10gK1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJyZncmlkbmFtZT0nICsgcXVlcnlQYXJhbXNbJ2dyaWRuYW1lJ10gKyBwYXJhbUluZm8ucGFyYW0gKyAnJmxheW91dD0nICsgcXVlcnlQYXJhbXNbJ2xheW91dCddO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIGFjdGlvblR5cGVzLnVybC50b0xvd2VyQ2FzZSgpOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZXN0aW5hdGlvblVybCA9IGRhdGEuRGVzdGluYXRpb25Vcmwuc3BsaXQoJz8nKSwgcGFyYW10ZXJNYXBwaW5nID0gZGF0YS5QYXJhbWV0ZXJNYXBwaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB1cmwgPSBkZXN0aW5hdGlvblVybFswXSwgc3RhdGljUGFyYW1zID0gZGVzdGluYXRpb25VcmxbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdGF0aWNQYXJhbXMgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsID0gdXJsICsgJz8nICsgc3RhdGljUGFyYW1zO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVybFBhcmFtcyA9IHVybC5tYXRjaCgve1tee31dKn0vZ2kpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodXJsUGFyYW1zICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybFBhcmFtcy5mb3JFYWNoKChpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW10ZXJNYXBwaW5nLmZvckVhY2goKHBhcmFtSXRlbSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1JdGVtLlBhcmFtZXRlck5hbWUgPT09IGl0ZW0uc3Vic3RyKDEpLnNsaWNlKDAsIC0xKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHBhcmFtSXRlbS5Jc1NvdXJjZVBhcmFtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc291cmNlUGFyYW1OYW1lID0gcGFyYW1JdGVtLlNvdXJjZUNvbHVtbk5hbWUuc3BsaXQoJ0AnKVsxXS50cmltKCkudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBwYXJhbWV0ZXIgPSBlbnRpdHlQYXJhbWV0ZXJJbmZvLmZpbHRlcihwYXIgPT4gcGFyLk5hbWUudHJpbSgpLnRvTG93ZXJDYXNlKCkgPT09IHNvdXJjZVBhcmFtTmFtZVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB8fCBwYXIuTmFtZS50cmltKCkudG9Mb3dlckNhc2UoKSA9PT0gcGFyYW1JdGVtLlNvdXJjZUNvbHVtbk5hbWUudHJpbSgpLnRvTG93ZXJDYXNlKCkpWzBdO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1ldGVyLlZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybCA9IHVybC5yZXBsYWNlKGl0ZW0sIGNoZWNrQW5kRm9ybWF0RGF0ZShwYXJhbWV0ZXIuVmFsdWUpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcyArPSBwYXJhbUl0ZW0uU291cmNlQ29sdW1uTmFtZSArIGNvbW1hO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93RGF0YVswXVtpdGVtLlNvdXJjZUNvbHVtbk5hbWVdIHx8IHNlbGVjdGVkUm93RGF0YVswXVtwYXJhbUl0ZW0uU291cmNlQ29sdW1uTmFtZV0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdXJsID0gdXJsLnJlcGxhY2UoaXRlbSwgY2hlY2tBbmRGb3JtYXREYXRlKHNlbGVjdGVkUm93RGF0YVswXVtwYXJhbUl0ZW0uU291cmNlQ29sdW1uTmFtZV0pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcyArPSBpdGVtLlNvdXJjZUNvbHVtbk5hbWUgPyBpdGVtLlNvdXJjZUNvbHVtbk5hbWUgKyBjb21tYSA6IGl0ZW0gKyBjb21tYTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtdGVyTWFwcGluZy5mb3JFYWNoKChpdGVtLCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHVybFBhcmFtcyAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1GaWx0ZXIgPSB1cmxQYXJhbXMuZmlsdGVyKCh1cmxJdGVtKSA9PiBpdGVtLlBhcmFtZXRlck5hbWUgPT09IHVybEl0ZW0uc3Vic3RyKDEpLnNsaWNlKDAsIC0xKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoIXBhcmFtRmlsdGVyLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlbGltaXRlciA9ICh1cmxEeW5hbWljUGFyYW1Db3VudCA9PT0gMCAmJiBzdGF0aWNQYXJhbXMgPT09IHVuZGVmaW5lZCkgPyAnPycgOiAnJic7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93RGF0YVswXVtpdGVtLlNvdXJjZUNvbHVtbk5hbWVdKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybCA9IHVybCArIGRlbGltaXRlciArIGl0ZW0uUGFyYW1ldGVyTmFtZSArICc9JyArIGNoZWNrQW5kRm9ybWF0RGF0ZShzZWxlY3RlZFJvd0RhdGFbMF1baXRlbS5Tb3VyY2VDb2x1bW5OYW1lXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMgKz0gaXRlbS5Tb3VyY2VDb2x1bW5OYW1lICsgY29tbWE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHVybER5bmFtaWNQYXJhbUNvdW50Kys7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICghdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBvcGVuKHVybCwgJ19ibGFuaycpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMuZW5kc1dpdGgoY29tbWEpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzID0gdW5kZWZpbmVkU291cmNlQ29sdW1uTmFtZXMuc3Vic3RyaW5nKDAsIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzLmxlbmd0aCAtIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhcHBUb2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnZXJyb3InLCBtZXNzYWdlOiBgRm9sbG93aW5nIGNvbHVtbihzKSAke3VuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzfSBpcyByZXF1aXJlZC4gUGxlYXNlIG1ha2Ugc3VyZSBzYW1lIGV4aXN0KHMpIGluIHRoZSBncmlkIG9yIHBhcmFtZXRlciB3aXRoIHZhbHVlLmBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICwgZGlzcGxheVRpbWU6IDQwMDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnd2FybmluZycsIG1lc3NhZ2U6ICdQbGVhc2Ugc2VsZWN0IGEgcm93LicgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBmYWxzZTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGV4dHJhY3REZXN0aW5hdGlvbkVudGl0eVBhcmFtZXRlcnMoc2VsZWN0ZWRSb3dEYXRhOiBhbnlbXSwgcGFyYW1ldGVyTWFwcGluZzogYW55W10sIGVudGl0eVBhcmFtZXRlckluZm86IGFueVtdKSB7XHJcbiAgICBsZXQgcGFyYW0gPSAnJztcclxuICAgIGxldCB1bmRlZmluZWRTb3VyY2VDb2x1bW5OYW1lcyA9ICcnO1xyXG4gICAgY29uc3QgY29tbWEgPSAnLCc7XHJcbiAgICBwYXJhbWV0ZXJNYXBwaW5nLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICBjb25zdCBwYXJhbU5hbWUgPSBpdGVtLk9yaWdpbmFsUGFyYW1ldGVyTmFtZSB8fCBpdGVtLlBhcmFtZXRlck5hbWU7XHJcbiAgICAgICAgaWYgKGl0ZW0uSXNTb3VyY2VQYXJhbSkge1xyXG4gICAgICAgICAgICBjb25zdCBzb3VyY2VQYXJhbU5hbWUgPSBpdGVtLlNvdXJjZUNvbHVtbk5hbWUuc3BsaXQoJ0AnKVsxXS50cmltKCkudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgY29uc3QgcGFyYW1ldGVyID0gZW50aXR5UGFyYW1ldGVySW5mby5maWx0ZXIocGFyID0+IHBhci5OYW1lLnRyaW0oKS50b0xvd2VyQ2FzZSgpID09PSBzb3VyY2VQYXJhbU5hbWVcclxuICAgICAgICAgICAgICAgIHx8IHBhci5OYW1lLnRyaW0oKS50b0xvd2VyQ2FzZSgpID09PSBpdGVtLlNvdXJjZUNvbHVtbk5hbWUudHJpbSgpLnRvTG93ZXJDYXNlKCkpWzBdO1xyXG5cclxuICAgICAgICAgICAgaWYgKHBhcmFtZXRlcj8uVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHBhcmFtID0gcGFyYW0gKyAnJicgKyBwYXJhbU5hbWUgKyAnPScgKyBjaGVja0FuZEZvcm1hdERhdGUocGFyYW1ldGVyLlZhbHVlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzICs9IGl0ZW0uU291cmNlQ29sdW1uTmFtZSArIGNvbW1hO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHNlbGVjdGVkUm93RGF0YVswXVtpdGVtLlNvdXJjZUNvbHVtbk5hbWVdKSB7XHJcbiAgICAgICAgICAgICAgICBwYXJhbSA9IHBhcmFtICsgJyYnICsgcGFyYW1OYW1lICsgJz0nICsgY2hlY2tBbmRGb3JtYXREYXRlKHNlbGVjdGVkUm93RGF0YVswXVtpdGVtLlNvdXJjZUNvbHVtbk5hbWVdKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzICs9IGl0ZW0uU291cmNlQ29sdW1uTmFtZSArIGNvbW1hO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIHsgcGFyYW0sIHVuZGVmaW5lZFNvdXJjZUNvbHVtbk5hbWVzIH07XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBjb252ZXJ0RGF0ZVRvVXNGb3JtYXQodmFsdWU6IERhdGUgfCBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgY29uc3QgZGF0ZVZhbHVlID0gbmV3IERhdGUodmFsdWUpO1xyXG4gICAgbGV0IGRkOiBudW1iZXIgfCBzdHJpbmcgPSBkYXRlVmFsdWUuZ2V0RGF0ZSgpOyBsZXQgbW06IG51bWJlciB8IHN0cmluZyA9IGRhdGVWYWx1ZS5nZXRNb250aCgpICsgMTsgY29uc3QgeXl5eSA9IGRhdGVWYWx1ZS5nZXRGdWxsWWVhcigpO1xyXG4gICAgZGQgPSBkZCA8IDEwID8gJzAnICsgZGQgOiBkZDtcclxuICAgIG1tID0gbW0gPCAxMCA/ICcwJyArIG1tIDogbW07XHJcbiAgICByZXR1cm4gYCR7bW19LyR7ZGR9LyR7eXl5eX1gO1xyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIHRoaXMgbWV0aG9kIHdpbGwgcmV0dXJuIHdlYiBtZW51IGlkIGJhc2VkIG9uIGtleVxyXG4gKiBAcGFyYW0gIHtzdHJpbmd9IGtleVxyXG4gKiBAcmV0dXJucyBudW1iZXJcclxuICovXHJcbmZ1bmN0aW9uIGdldFdlYk1lbnVJZEJ5S2V5KGtleTogc3RyaW5nKTogbnVtYmVyIHtcclxuICAgIGNvbnN0IHdlYk1lbnVzID0gd2luZG93WydtZW51RGF0YSddIHx8IFtdO1xyXG4gICAgaWYgKHdlYk1lbnVzLmxlbmd0aCkge1xyXG4gICAgICAgIGNvbnN0IGZpbHRlcmVkSXRlbSA9IHdlYk1lbnVzLmZpbHRlcigoYXJySXRlbSkgPT4gYXJySXRlbS5LZXk/LnRvTG93ZXJDYXNlKCkgPT09IGtleS50b0xvd2VyQ2FzZSgpKVswXTtcclxuICAgICAgICByZXR1cm4gZmlsdGVyZWRJdGVtID8gZmlsdGVyZWRJdGVtLk9wdG9tYXNUb29sSWQgOiB1bmRlZmluZWQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8vIGNvbnZlcnQgZnJvbSBZWVlZLU1NLUREVEhIOm1tOnNzLlNTUyB0byBNTS9ERC9ZWVlZLCBlbHNlIHJldHVybiB0aGUgcGFyYW0gYXMgaXQgaXNcclxuZnVuY3Rpb24gY2hlY2tBbmRGb3JtYXREYXRlKHZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgLy8gY29uc3QgbW9tZW50ID0gd2luZG93Wydtb21lbnQnXTtcclxuICAgIC8vIC8vIHJlZjogaHR0cHM6Ly9mbGF2aW9jb3Blcy5jb20vbW9tZW50anMvXHJcbiAgICAvLyBpZiAobW9tZW50KHZhbHVlLCBtb21lbnQuSFRNTDVfRk1ULkRBVEVUSU1FX0xPQ0FMX01TLCB0cnVlKS5pc1ZhbGlkKCkpIHtcclxuICAgIC8vICAgICByZXR1cm4gbW9tZW50KHZhbHVlKS5mb3JtYXQoJ01NL0REL1lZWVknKTtcclxuICAgIC8vIH0gZWxzZSB7XHJcbiAgICAvLyAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgLy8gfVxyXG4gICAgcmV0dXJuIHZhbHVlO1xyXG59XHJcblxyXG4iXX0=