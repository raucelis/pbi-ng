import { __decorate } from "tslib";
//#region Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxDataGridModule, DxScrollViewModule, DxDropDownBoxModule, DxTemplateModule, DxButtonModule, DxTextBoxModule, DxColorBoxModule, DxValidationGroupModule, DxValidatorModule, DxDropDownButtonModule, DxRadioGroupModule, DxTreeViewModule, DxPopupModule, DxSelectBoxModule, DxFilterBuilderModule, DxListModule, DxMenuModule, DxNumberBoxModule, DxDateBoxModule } from 'devextreme-angular';
//#endregion
//#region Components of Common Grid Framework
import { ColorFormatComponent } from './components/color-format/color-format.component';
import { ColumnChooserComponent } from './components/column-chooser/column-chooser.component';
import { CommonGridFrameworkComponent } from './components/common-grid-framework/common-grid-framework.component';
import { ConditionalFormattingComponent } from './components/conditional-formatting/conditional-formatting.component';
import { FilterComponent } from './components/filter/filter.component';
import { FlyOutActionIconContainerComponent } from './components/fly-out-action-icon-container/fly-out-action-icon-container.component';
import { FormsModule } from '@angular/forms';
import { GridComponent } from './components/grid/grid.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ViewSelectionComponent } from './components/view-selection/view-selection.component';
import { EntityParametersComponent } from './components/entity-parameters/entity-parameters.component';
//#endregion Components of Common Grid Framework
const components = [
    ColorFormatComponent,
    ColumnChooserComponent,
    CommonGridFrameworkComponent,
    ConditionalFormattingComponent,
    EntityParametersComponent,
    FilterComponent,
    FlyOutActionIconContainerComponent,
    GridComponent,
    SettingsComponent,
    ViewSelectionComponent,
];
let PBICommonGridFrameworkModule = class PBICommonGridFrameworkModule {
};
PBICommonGridFrameworkModule = __decorate([
    NgModule({
        declarations: [components],
        imports: [
            CommonModule,
            DxButtonModule,
            DxColorBoxModule,
            DxDataGridModule,
            DxDateBoxModule,
            DxDropDownBoxModule,
            DxDropDownButtonModule,
            DxFilterBuilderModule,
            DxListModule,
            DxMenuModule,
            DxNumberBoxModule,
            DxPopupModule,
            DxRadioGroupModule,
            DxScrollViewModule,
            DxSelectBoxModule,
            DxTemplateModule,
            DxTextBoxModule,
            DxTreeViewModule,
            DxValidationGroupModule,
            DxValidatorModule,
            FormsModule,
        ],
        exports: components
    })
], PBICommonGridFrameworkModule);
export { PBICommonGridFrameworkModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLWdyaWQtZnJhbWV3b3JrLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tbW9uLWdyaWQtZnJhbWV3b3JrLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsaUJBQWlCO0FBQ2pCLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFDTCxnQkFBZ0IsRUFDaEIsa0JBQWtCLEVBQ2xCLG1CQUFtQixFQUNuQixnQkFBZ0IsRUFDaEIsY0FBYyxFQUNkLGVBQWUsRUFDZixnQkFBZ0IsRUFDaEIsdUJBQXVCLEVBQ3ZCLGlCQUFpQixFQUNqQixzQkFBc0IsRUFDdEIsa0JBQWtCLEVBQ2xCLGdCQUFnQixFQUVoQixhQUFhLEVBQUUsaUJBQWlCLEVBQUUscUJBQXFCLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxpQkFBaUIsRUFBRSxlQUFlLEVBQ3hILE1BQU0sb0JBQW9CLENBQUM7QUFDNUIsWUFBWTtBQUVaLDZDQUE2QztBQUM3QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUN4RixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUM5RixPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxvRUFBb0UsQ0FBQztBQUNsSCxPQUFPLEVBQUUsOEJBQThCLEVBQUUsTUFBTSxzRUFBc0UsQ0FBQztBQUN0SCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGtDQUFrQyxFQUFFLE1BQU0sb0ZBQW9GLENBQUM7QUFDeEksT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNqRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUM5RixPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSw0REFBNEQsQ0FBQztBQUN2RyxnREFBZ0Q7QUFHaEQsTUFBTSxVQUFVLEdBQVU7SUFDeEIsb0JBQW9CO0lBQ3BCLHNCQUFzQjtJQUN0Qiw0QkFBNEI7SUFDNUIsOEJBQThCO0lBQzlCLHlCQUF5QjtJQUN6QixlQUFlO0lBQ2Ysa0NBQWtDO0lBQ2xDLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsc0JBQXNCO0NBQ3ZCLENBQUM7QUE2QkYsSUFBYSw0QkFBNEIsR0FBekMsTUFBYSw0QkFBNEI7Q0FBSSxDQUFBO0FBQWhDLDRCQUE0QjtJQTNCeEMsUUFBUSxDQUFDO1FBQ1IsWUFBWSxFQUFFLENBQUMsVUFBVSxDQUFDO1FBQzFCLE9BQU8sRUFBRTtZQUNQLFlBQVk7WUFDWixjQUFjO1lBQ2QsZ0JBQWdCO1lBQ2hCLGdCQUFnQjtZQUNoQixlQUFlO1lBQ2YsbUJBQW1CO1lBQ25CLHNCQUFzQjtZQUN0QixxQkFBcUI7WUFDckIsWUFBWTtZQUNaLFlBQVk7WUFDWixpQkFBaUI7WUFDakIsYUFBYTtZQUNiLGtCQUFrQjtZQUNsQixrQkFBa0I7WUFDbEIsaUJBQWlCO1lBQ2pCLGdCQUFnQjtZQUNoQixlQUFlO1lBQ2YsZ0JBQWdCO1lBQ2hCLHVCQUF1QjtZQUN2QixpQkFBaUI7WUFDakIsV0FBVztTQUNaO1FBQ0QsT0FBTyxFQUFFLFVBQVU7S0FDcEIsQ0FBQztHQUNXLDRCQUE0QixDQUFJO1NBQWhDLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbIi8vI3JlZ2lvbiBNb2R1bGVzXHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7XHJcbiAgRHhEYXRhR3JpZE1vZHVsZSxcclxuICBEeFNjcm9sbFZpZXdNb2R1bGUsXHJcbiAgRHhEcm9wRG93bkJveE1vZHVsZSxcclxuICBEeFRlbXBsYXRlTW9kdWxlLFxyXG4gIER4QnV0dG9uTW9kdWxlLFxyXG4gIER4VGV4dEJveE1vZHVsZSxcclxuICBEeENvbG9yQm94TW9kdWxlLFxyXG4gIER4VmFsaWRhdGlvbkdyb3VwTW9kdWxlLFxyXG4gIER4VmFsaWRhdG9yTW9kdWxlLFxyXG4gIER4RHJvcERvd25CdXR0b25Nb2R1bGUsXHJcbiAgRHhSYWRpb0dyb3VwTW9kdWxlLFxyXG4gIER4VHJlZVZpZXdNb2R1bGUsXHJcbiAgRHhQb3BvdmVyTW9kdWxlLFxyXG4gIER4UG9wdXBNb2R1bGUsIER4U2VsZWN0Qm94TW9kdWxlLCBEeEZpbHRlckJ1aWxkZXJNb2R1bGUsIER4TGlzdE1vZHVsZSwgRHhNZW51TW9kdWxlLCBEeE51bWJlckJveE1vZHVsZSwgRHhEYXRlQm94TW9kdWxlXHJcbn0gZnJvbSAnZGV2ZXh0cmVtZS1hbmd1bGFyJztcclxuLy8jZW5kcmVnaW9uXHJcblxyXG4vLyNyZWdpb24gQ29tcG9uZW50cyBvZiBDb21tb24gR3JpZCBGcmFtZXdvcmtcclxuaW1wb3J0IHsgQ29sb3JGb3JtYXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29sb3ItZm9ybWF0L2NvbG9yLWZvcm1hdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb2x1bW5DaG9vc2VyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NvbHVtbi1jaG9vc2VyL2NvbHVtbi1jaG9vc2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbW1vbkdyaWRGcmFtZXdvcmtDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29tbW9uLWdyaWQtZnJhbWV3b3JrL2NvbW1vbi1ncmlkLWZyYW1ld29yay5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb25kaXRpb25hbEZvcm1hdHRpbmdDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY29uZGl0aW9uYWwtZm9ybWF0dGluZy9jb25kaXRpb25hbC1mb3JtYXR0aW5nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZpbHRlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZseU91dEFjdGlvbkljb25Db250YWluZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZmx5LW91dC1hY3Rpb24taWNvbi1jb250YWluZXIvZmx5LW91dC1hY3Rpb24taWNvbi1jb250YWluZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEdyaWRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZ3JpZC9ncmlkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFNldHRpbmdzQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3NldHRpbmdzL3NldHRpbmdzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFZpZXdTZWxlY3Rpb25Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdmlldy1zZWxlY3Rpb24vdmlldy1zZWxlY3Rpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgRW50aXR5UGFyYW1ldGVyc0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9lbnRpdHktcGFyYW1ldGVycy9lbnRpdHktcGFyYW1ldGVycy5jb21wb25lbnQnO1xyXG4vLyNlbmRyZWdpb24gQ29tcG9uZW50cyBvZiBDb21tb24gR3JpZCBGcmFtZXdvcmtcclxuXHJcblxyXG5jb25zdCBjb21wb25lbnRzOiBhbnlbXSA9IFtcclxuICBDb2xvckZvcm1hdENvbXBvbmVudCxcclxuICBDb2x1bW5DaG9vc2VyQ29tcG9uZW50LFxyXG4gIENvbW1vbkdyaWRGcmFtZXdvcmtDb21wb25lbnQsXHJcbiAgQ29uZGl0aW9uYWxGb3JtYXR0aW5nQ29tcG9uZW50LFxyXG4gIEVudGl0eVBhcmFtZXRlcnNDb21wb25lbnQsXHJcbiAgRmlsdGVyQ29tcG9uZW50LFxyXG4gIEZseU91dEFjdGlvbkljb25Db250YWluZXJDb21wb25lbnQsXHJcbiAgR3JpZENvbXBvbmVudCxcclxuICBTZXR0aW5nc0NvbXBvbmVudCxcclxuICBWaWV3U2VsZWN0aW9uQ29tcG9uZW50LFxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtjb21wb25lbnRzXSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBDb21tb25Nb2R1bGUsXHJcbiAgICBEeEJ1dHRvbk1vZHVsZSxcclxuICAgIER4Q29sb3JCb3hNb2R1bGUsXHJcbiAgICBEeERhdGFHcmlkTW9kdWxlLFxyXG4gICAgRHhEYXRlQm94TW9kdWxlLFxyXG4gICAgRHhEcm9wRG93bkJveE1vZHVsZSxcclxuICAgIER4RHJvcERvd25CdXR0b25Nb2R1bGUsXHJcbiAgICBEeEZpbHRlckJ1aWxkZXJNb2R1bGUsXHJcbiAgICBEeExpc3RNb2R1bGUsXHJcbiAgICBEeE1lbnVNb2R1bGUsXHJcbiAgICBEeE51bWJlckJveE1vZHVsZSxcclxuICAgIER4UG9wdXBNb2R1bGUsXHJcbiAgICBEeFJhZGlvR3JvdXBNb2R1bGUsXHJcbiAgICBEeFNjcm9sbFZpZXdNb2R1bGUsXHJcbiAgICBEeFNlbGVjdEJveE1vZHVsZSxcclxuICAgIER4VGVtcGxhdGVNb2R1bGUsXHJcbiAgICBEeFRleHRCb3hNb2R1bGUsXHJcbiAgICBEeFRyZWVWaWV3TW9kdWxlLFxyXG4gICAgRHhWYWxpZGF0aW9uR3JvdXBNb2R1bGUsXHJcbiAgICBEeFZhbGlkYXRvck1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gIF0sXHJcbiAgZXhwb3J0czogY29tcG9uZW50c1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUEJJQ29tbW9uR3JpZEZyYW1ld29ya01vZHVsZSB7IH1cclxuIl19