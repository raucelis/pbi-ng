import { __decorate } from "tslib";
import { CGFStorageKeys } from '../../utilities/enums';
import { CGFUtilityService } from '../../services/cgf-utility.service';
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { appToast } from '../../utilities/utilityFunctions';
import { columnFormattingOptions, customActionColumnInfo } from '../../utilities/constants';
let ColorFormatComponent = class ColorFormatComponent {
    constructor(utilityService) {
        this.utilityService = utilityService;
        this.enableAlignment = true;
        this.enableBackgroundColor = true;
        this.enableFontStyle = true;
        this.enableFormat = true;
        this.enableFormatToggle = true;
        this.enableInputCaptionField = true;
        this.enableInputDataFieldCheck = false;
        this.enablePin = true;
        this.enableStorage = true;
        this.enableTextColor = true;
        this.gridInstanceList = [];
        this.isMasterGridSelected = true;
        this.showBasicFormat = false;
        this.storageKey = CGFStorageKeys[CGFStorageKeys.formatData];
        this.formatOptionsChanged = new EventEmitter();
        this.formatToggled = new EventEmitter();
        this.backgroundColor = '';
        this.basicFormatIconsAndOptionsData = columnFormattingOptions;
        this.captionValidationPattern = /^[a-zA-Z0-9_\s]+$/;
        this.columnsActivePropertiesTabsList = [{ title: 'FORMATTING' }];
        this.decimalCounter = 0;
        this.decimalTypes = ['comma', 'percent', 'currency'];
        this.disableDecimalPrecision = false;
        this.formatObject = {};
        this.isColorReadOnly = true;
        this.textColor = '';
    }
    set disableDecimalPrecisionInput(data) {
        this.disableDecimalPrecision = data;
        this.basicFormatIconsAndOptionsData = this.disableDecimalPrecision ? this.removePrecisionControls(columnFormattingOptions) : columnFormattingOptions;
        this.isColorReadOnly = !this.disableDecimalPrecision;
    }
    get disableDecimalPrecisionInput() { return this.disableDecimalPrecision; }
    set selectedColumnInfo(columnItem) {
        var _a;
        this.canUpdateColumnFormatting = false;
        this.selectedColumnData = columnItem || { caption: '', dataField: '' };
        if (((_a = this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField.length) > 0) {
            this.prepareSelectedOption();
        }
        else {
            this.resetAllFormatOptions();
        }
    }
    get selectedColumnInfo() { return this.selectedColumnData; }
    ngOnInit() {
        this.resetBasicSettings();
        if (this.selectedColumnData.dataField && this.selectedColumnData.dataField.length > 0) {
            this.prepareSelectedOption();
        }
    }
    //#region Public Methods
    onCaptionValueChanged(args) {
        if (args.event) {
            this.canUpdateColumnFormatting = true;
        }
        this.formatObject.caption = this.selectedColumnInfo.caption || '';
        const data = {
            caption: this.selectedColumnInfo.caption || ''
        };
        if (data.caption.match(this.captionValidationPattern)) {
            this.formatOptionsChanged.emit(data);
            this.onFormatOptionsChanged();
        }
        // else {
        //   this.selectedColumnInfo.caption = args && args.previousValue ? args.previousValue : '';
        // }
    }
    onChangeOfTextColor() {
        this.formatObject.textColor = this.textColor || '';
        this.onFormatOptionsChanged();
    }
    onChangeOfBackgroundColor() {
        this.formatObject.backgroundColor = this.backgroundColor || '';
        this.onFormatOptionsChanged();
    }
    onColorFocusIn() {
        if (this.isColorReadOnly) {
            appToast({ type: 'warning', message: 'Column Format: Please select a column.' });
        }
        this.canUpdateColumnFormatting = true;
    }
    formatColumn(args, event) {
        if (this.selectedColumnData.dataField === customActionColumnInfo.dataField ||
            ((!this.selectedColumnData.dataField) || (this.selectedColumnData.dataField && this.selectedColumnData.dataField.length === 0))
                && this.enableInputDataFieldCheck) {
            appToast({ type: 'warning', message: 'Column Format: Please select a column.' });
            return;
        }
        if (args.type === 'right') {
            if (this.gridInstanceList.length) {
                this.gridInstanceList.forEach(grid => {
                    grid.columnOption(customActionColumnInfo.dataField, { fixed: true, fixedPosition: 'right' });
                });
            }
        }
        this.prepareLoaderLocation(event);
        args.isSelected = !args.isSelected;
        if (event) {
            this.canUpdateColumnFormatting = true;
        }
        if (!this.selectFormat(args)) {
            args.isSelected = !args.isSelected;
        }
        if (args.key.toLowerCase().indexOf('decimal') > -1) {
            args.isSelected = false; // this case makes sure remove/add decimal are not highlighted.
        }
    }
    onFormatOptionsChanged() {
        if (!this.canUpdateColumnFormatting) {
            return;
        }
        if (this.selectedColumnData) {
            setTimeout(() => {
                if (this.gridInstanceList.length) {
                    this.gridInstanceList.forEach(item => item.repaint());
                }
                document.querySelector('.loader').classList.add('display-none');
            }, 0);
            const formatDataCollection = this.getOrSetSessionData(this.storageKey, 'get');
            let index, dataUpdated;
            dataUpdated = false;
            this.formatObject.dataField = this.selectedColumnData.dataField;
            for (index = 0; index < formatDataCollection.length; index++) {
                if (formatDataCollection[index].dataField === this.selectedColumnData.dataField) {
                    formatDataCollection[index] = this.formatObject;
                    dataUpdated = true;
                    break;
                }
            }
            if (!dataUpdated) {
                formatDataCollection.push(this.formatObject);
            }
            this.getOrSetSessionData(this.storageKey, 'set', formatDataCollection);
        }
    }
    toggleBasicFormat() {
        this.showBasicFormat = !this.showBasicFormat;
        this.formatToggled.emit(this.showBasicFormat);
    }
    //#endregion Public Methods
    //#region Private Methods
    selectFormat(args) {
        let formatNotAllowed = false;
        this.formatObject.dataType = this.formatObject.dataType || this.selectedColumnData.dataType;
        switch (args.key) {
            case 'leftAlignment':
                this.setAlignmentFormat(args, 'left');
                break;
            case 'rightAlignment':
                this.setAlignmentFormat(args, 'right');
                break;
            case 'centerAlignment':
                this.setAlignmentFormat(args, 'center');
                break;
            case 'leftPin':
                this.setPinPosition(args, 'left');
                break;
            case 'rightPin':
                this.setPinPosition(args, 'right');
                break;
            case 'bold':
                this.prepareColumnFormatUsingClassName('cssClass', 'bold', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'underline':
                this.prepareColumnFormatUsingClassName('cssClass', 'underline', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'italic':
                this.prepareColumnFormatUsingClassName('cssClass', 'italic', args.isSelected);
                this.onFormatOptionsChanged();
                break;
            case 'currency':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'currency', args.isSelected);
                    this.decimalCounter = 0;
                    this.formatObject.format = args.isSelected ? { type: 'currency', precision: 2 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'currency', 'currency', 2);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'addDecimal':
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    if (this.formatObject.format) {
                        this.decimalCounter = this.formatObject.format.precision || 0;
                    }
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    this.decimalCounter++;
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, true);
                    this.formatObject.format = {
                        type: this.getDecimalType(),
                        precision: this.decimalCounter
                    };
                    this.onFormatOptionsChanged();
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'removeDecimal':
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    if (this.formatObject.format) {
                        this.decimalCounter = this.formatObject.format.precision || 0;
                    }
                    this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    if (this.decimalCounter > 0) {
                        this.decimalCounter--;
                        this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, true);
                        this.formatObject.format = {
                            type: this.getDecimalType(),
                            precision: this.decimalCounter
                        };
                    }
                    else if (this.decimalCounter === 0) {
                        this.prepareColumnFormatUsingClassName('cssClass', 'decimalAdded-' + this.decimalCounter, false);
                    }
                    this.onFormatOptionsChanged();
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'percentage':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'percentage', args.isSelected);
                    this.formatObject.format = args.isSelected ? { type: 'percent', precision: 2 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'percent', 'percentage', 2);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'comma':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType && this.selectedColumnData.dataType.indexOf('number') > -1) {
                    this.decimalCounter = 0;
                    this.prepareColumnFormatUsingClassName('cssClass', 'comma', args.isSelected);
                    this.formatObject.format = args.isSelected ? { type: 'comma', precision: 0 } : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.columnFormatChangeForDataDictionary(args, 'comma', 'comma', 0);
                    break;
                }
                formatNotAllowed = true;
                break;
            case 'calendar':
                this.resetSelectedFormat(args.type);
                if (this.selectedColumnData.dataType.indexOf('date') > -1 || !this.enableInputDataFieldCheck) {
                    this.prepareColumnFormatUsingClassName('cssClass', 'calendar', args.isSelected);
                    this.formatObject.format = args.isSelected ? 'shortDate' : '';
                    this.onFormatOptionsChanged();
                    break;
                }
                else if (!this.enableInputCaptionField) {
                    this.formatObject.format = args.isSelected ? 'shortDate' : '';
                    break;
                }
                formatNotAllowed = true;
                break;
        }
        if (!this.enableInputCaptionField) {
            setTimeout(() => {
                document.querySelector('.loader').classList.add('display-none');
            }, 0);
        }
        if (formatNotAllowed) {
            appToast({ type: 'warning', message: 'Format can\'t be applied on the selected column.' });
            document.querySelector('.loader').classList.add('display-none');
            return false;
        }
        return true;
    }
    setAlignmentFormat(args, alignmentType) {
        this.resetAlignmentExceptForCurrent(args.type);
        this.formatObject.alignment = args.isSelected ? alignmentType : '';
        if (this.enableInputCaptionField) {
            this.onFormatOptionsChanged();
        }
    }
    setPinPosition(args, position) {
        this.resetPinExceptForCurrent(args.type);
        this.formatObject.fixed = args.isSelected;
        this.formatObject.fixedPosition = args.isSelected ? position : '';
        if (this.gridInstanceList.length) {
            this.gridInstanceList.forEach(grid => {
                grid.columnOption(this.selectedColumnData.dataField, { fixed: args.isSelected, fixedPosition: this.formatObject.fixedPosition });
            });
        }
        if (this.enableInputCaptionField) {
            this.onFormatOptionsChanged();
        }
    }
    columnFormatChangeForDataDictionary(args, type, formatClass, precision) {
        this.prepareColumnFormatUsingClassName('cssClass', formatClass, args.isSelected);
        this.formatObject.format = args.isSelected ? { type, precision } : '';
    }
    resetAlignmentExceptForCurrent(type = '') {
        for (let i = 0; i < this.basicFormatIconsAndOptionsData.alignment.length; i++) {
            if (this.basicFormatIconsAndOptionsData.alignment[i].type !== type) {
                this.basicFormatIconsAndOptionsData.alignment[i].isSelected = false;
            }
        }
    }
    resetPinExceptForCurrent(type = '') {
        this.basicFormatIconsAndOptionsData.pin.forEach(item => {
            if (item.type !== type) {
                item.isSelected = false;
            }
        });
    }
    prepareColumnFormatUsingClassName(formatType, formatValue, addFormat) {
        let listOfExistingFormats = this.formatObject[formatType] || '';
        if (addFormat) {
            if (listOfExistingFormats && listOfExistingFormats.length) {
                const formatGroupData = ['currency', 'percentage', 'comma', 'calendar'];
                if (formatGroupData.indexOf(formatValue) > -1) {
                    for (let formatIndex = 0; formatIndex < formatGroupData.length; formatIndex++) {
                        listOfExistingFormats = listOfExistingFormats.replace(formatGroupData[formatIndex], '');
                    }
                }
                listOfExistingFormats = listOfExistingFormats.replace(new RegExp(formatValue, 'g'), '').trim();
                listOfExistingFormats = listOfExistingFormats + ' ' + formatValue;
            }
            else {
                listOfExistingFormats = formatValue;
            }
        }
        else if (listOfExistingFormats && listOfExistingFormats.indexOf(formatValue) > -1) {
            listOfExistingFormats = listOfExistingFormats.replace(new RegExp(formatValue, 'g'), '').trim();
        }
        this.formatObject[formatType] = listOfExistingFormats;
        const _data = {};
        if (formatType === 'cssClass') {
            _data[formatType] = listOfExistingFormats;
        }
        else {
            _data[formatType] = addFormat ? formatValue : listOfExistingFormats;
        }
        return _data;
    }
    resetSelectedFormat(type = '') {
        let listOfClasses = this.formatObject.cssClass;
        if (listOfClasses) {
            listOfClasses = listOfClasses.replace(/percentage/g, '').replace(/comma/g, '').replace(/currency/g, '');
            this.formatObject.cssClass = listOfClasses;
        }
        for (let i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
            if (this.basicFormatIconsAndOptionsData.format[i].type !== type) {
                this.basicFormatIconsAndOptionsData.format[i].isSelected = false;
            }
        }
    }
    resetBasicSettings() {
        let i;
        for (i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
            this.basicFormatIconsAndOptionsData.format[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.alignment.length; i++) {
            this.basicFormatIconsAndOptionsData.alignment[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.pin.length; i++) {
            this.basicFormatIconsAndOptionsData.pin[i].isSelected = false;
        }
        for (i = 0; i < this.basicFormatIconsAndOptionsData.fontStyle.length; i++) {
            this.basicFormatIconsAndOptionsData.fontStyle[i].isSelected = false;
        }
        this.textColor = '';
        this.backgroundColor = '';
        this.formatObject = {};
        this.decimalCounter = 0;
    }
    prepareSelectedOption() {
        this.resetBasicSettings();
        let selectedColumnDef = new Object();
        if (this.enableStorage) {
            const columnFormatCollection = this.getOrSetSessionData(this.storageKey, 'get');
            const baseFormatCollection = this.isMasterGridSelected ?
                JSON.parse(sessionStorage.getItem(CGFStorageKeys[CGFStorageKeys.dictionaryFormatData])) || [] : [];
            selectedColumnDef = columnFormatCollection.find(arrItem => arrItem.dataField === this.selectedColumnData.dataField) ||
                baseFormatCollection.find(arrItem => arrItem.dataField === this.selectedColumnData.dataField) || {};
        }
        if (!Object.keys(selectedColumnDef).length && this.selectedColumnData) {
            selectedColumnDef = this.selectedColumnData;
        }
        if (selectedColumnDef) {
            if (selectedColumnDef.cssClass) {
                const classList = selectedColumnDef.cssClass.split(' ');
                let i;
                classList.forEach((cssClass) => {
                    if (cssClass === 'bold' || cssClass === 'underline' || cssClass === 'italic') {
                        for (i = 0; i < this.basicFormatIconsAndOptionsData.fontStyle.length; i++) {
                            if (this.basicFormatIconsAndOptionsData.fontStyle[i].type === cssClass) {
                                this.basicFormatIconsAndOptionsData.fontStyle[i].isSelected = true;
                                this.formatObject.cssClass = this.formatObject.cssClass ? this.formatObject.cssClass + ' ' + cssClass : ' ' + cssClass;
                                break;
                            }
                        }
                    }
                    if (cssClass === 'currency' || cssClass === 'percentage' || cssClass === 'comma' || cssClass === 'calendar') {
                        for (i = 0; i < this.basicFormatIconsAndOptionsData.format.length; i++) {
                            if (this.basicFormatIconsAndOptionsData.format[i].type === cssClass) {
                                this.basicFormatIconsAndOptionsData.format[i].isSelected = true;
                                this.decimalCounter = selectedColumnDef.format ? selectedColumnDef.format.precision : 0;
                                this.formatObject.cssClass = this.formatObject.cssClass ? this.formatObject.cssClass + ' ' + cssClass : ' ' + cssClass;
                                break;
                            }
                        }
                    }
                    if (cssClass.indexOf('decimal') > -1) {
                        this.decimalCounter = selectedColumnDef.format.precision;
                    }
                });
            }
            if (selectedColumnDef.fixed) {
                const isPositionLeft = selectedColumnDef.fixedPosition === 'left' ? true : false;
                for (let ind = 0; ind < this.basicFormatIconsAndOptionsData.pin.length; ind++) {
                    if (isPositionLeft && this.basicFormatIconsAndOptionsData.pin[ind].key === 'leftPin') {
                        this.basicFormatIconsAndOptionsData.pin[ind].isSelected = true;
                        break;
                    }
                    else if (this.basicFormatIconsAndOptionsData.pin[ind].key === 'rightPin') {
                        this.basicFormatIconsAndOptionsData.pin[ind].isSelected = true;
                        break;
                    }
                }
            }
            let j;
            for (j = 0; j < this.basicFormatIconsAndOptionsData.alignment.length; j++) {
                if (this.basicFormatIconsAndOptionsData.alignment[j].type === selectedColumnDef.alignment) {
                    this.basicFormatIconsAndOptionsData.alignment[j].isSelected = true;
                }
            }
            for (j = 0; j < this.basicFormatIconsAndOptionsData.pin.length; j++) {
                if (this.basicFormatIconsAndOptionsData.pin[j].type === selectedColumnDef.fixedPosition) {
                    this.basicFormatIconsAndOptionsData.pin[j].isSelected = true;
                }
            }
            this.textColor = selectedColumnDef.textColor || '';
            this.backgroundColor = selectedColumnDef.backgroundColor || '';
            this.formatObject = selectedColumnDef;
        }
        this.isColorReadOnly = !this.selectedColumnData;
    }
    removePrecisionControls(data) {
        const columnFormatData = Object.assign({}, data);
        const formatData = columnFormatData.format.filter((item) => {
            if (item.key !== 'removeDecimal' && item.key !== 'addDecimal') {
                return item;
            }
        });
        columnFormatData.format = formatData;
        return columnFormatData;
    }
    getDecimalType() {
        let decimalFormatType = 'fixedPoint';
        if (this.formatObject.format) {
            decimalFormatType = this.decimalTypes.some(t => t === this.formatObject.format.type)
                ? this.formatObject.format.type : 'fixedPoint';
        }
        return decimalFormatType;
    }
    getOrSetSessionData(key, type, data = null) {
        if (!this.enableStorage) {
            return [];
        }
        key = this.gridInstanceList.length ? this.utilityService.getStorageKey(this.gridInstanceList[0], key, this.isMasterGridSelected) : key;
        switch (type) {
            case 'get':
                return JSON.parse(sessionStorage.getItem(key)) || [];
            case 'set':
                sessionStorage.setItem(key, JSON.stringify(data));
                break;
        }
        return [];
    }
    resetAllFormatOptions() {
        this.resetBasicSettings();
        this.resetPinExceptForCurrent();
        this.resetSelectedFormat();
        this.resetAlignmentExceptForCurrent();
    }
    prepareLoaderLocation(event) {
        const cord = event.currentTarget.getBoundingClientRect();
        const leftCords = cord.left + 4 + 'px';
        const topCords = cord.top + 4 + 'px';
        const firstChild = document.querySelector('.loader');
        firstChild.classList.remove('display-none');
        firstChild.style.top = topCords;
        firstChild.style.left = leftCords;
    }
};
ColorFormatComponent.ctorParameters = () => [
    { type: CGFUtilityService }
];
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableAlignment", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableBackgroundColor", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableFontStyle", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableFormat", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableFormatToggle", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableInputCaptionField", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableInputDataFieldCheck", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enablePin", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableStorage", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "enableTextColor", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "gridInstanceList", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "isMasterGridSelected", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "showBasicFormat", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "storageKey", void 0);
__decorate([
    Input()
], ColorFormatComponent.prototype, "disableDecimalPrecisionInput", null);
__decorate([
    Input()
], ColorFormatComponent.prototype, "selectedColumnInfo", null);
__decorate([
    Output()
], ColorFormatComponent.prototype, "formatOptionsChanged", void 0);
__decorate([
    Output()
], ColorFormatComponent.prototype, "formatToggled", void 0);
ColorFormatComponent = __decorate([
    Component({
        selector: 'pbi-color-format',
        template: "<div id=\"loader-container\">\r\n  <div class=\"loader display-none\"></div>\r\n</div>\r\n\r\n<div>\r\n  <div class=\"active-properties-tab\">\r\n    <ul>\r\n      <li *ngFor=\"let tabs of columnsActivePropertiesTabsList\"\r\n        [class.property-tab-in-column-chooser]=\"enableFormatToggle\"\r\n        [class.property-tab-in-data-dictionary]=\"!enableFormatToggle\"\r\n        (click)=\"enableFormatToggle && toggleBasicFormat()\">{{tabs.title}}\r\n        <span *ngIf=\"enableFormatToggle\" [class.fa-angle-down]=\"!showBasicFormat\" [class.fa-angle-up]=\"showBasicFormat\"\r\n          class=\"fa float-right basic-format-toggle-icon\"></span>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n  <div class=\"active-properties-basic-tab-content\" *ngIf=\"showBasicFormat || !enableFormatToggle\">\r\n    <div class=\"format-controls-container\">\r\n      <!-- <div *ngIf=\"enableInputDataFieldCheck\">\r\n                <div>Column ID: <span class=\"bold\">{{selectedColumnInfo.dataField}}</span></div>\r\n            </div> -->\r\n      <div *ngIf=\"enableInputCaptionField\">\r\n        <div>Caption:</div>\r\n        <div class=\"caption-font-style\">\r\n          <dx-text-box class=\"caption-input\" [(value)]=\"selectedColumnInfo.caption\"\r\n            (onValueChanged)=\"onCaptionValueChanged($event)\" maxLength=\"40\">\r\n            <dx-validator>\r\n              <dxi-validation-rule type=\"pattern\" [pattern]=\"captionValidationPattern\"\r\n                message=\"Caption will contain only alphabets(a-z,A-Z)/numbers(0-9)/underscore(_)/space.\">\r\n              </dxi-validation-rule>\r\n            </dx-validator>\r\n          </dx-text-box>\r\n        </div>\r\n      </div>\r\n      <div class=\"format-alignments-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enableFormat\">\r\n          <div>Format:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.format\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableAlignment\">\r\n          <div>Alignment:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.alignment\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"pin-font-style-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enablePin\">\r\n          <div>Pin:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.pin\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableFontStyle\">\r\n          <div>Font Style:</div>\r\n          <div class=\"icon-container\">\r\n            <i [class.active]=\"formatItem.isSelected\" class=\"{{formatItem.icon}}\"\r\n              *ngFor=\"let formatItem of basicFormatIconsAndOptionsData.fontStyle\" title=\"{{formatItem.title}}\"\r\n              (click)=\"formatColumn(formatItem, $event)\"></i>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"text-font-style-container\">\r\n        <div class=\"flex-child\" *ngIf=\"enableTextColor\">\r\n          <div>Text Color:</div>\r\n          <div class=\"text-margin-style\">\r\n            <dx-color-box [(value)]=\"textColor\" [readOnly]=\"isColorReadOnly\" (onFocusIn)=\"onColorFocusIn()\"\r\n              (onValueChanged)=\"onChangeOfTextColor()\">\r\n            </dx-color-box>\r\n          </div>\r\n        </div>\r\n        <div class=\"flex-child\" *ngIf=\"enableBackgroundColor\">\r\n          <div>Background Color:</div>\r\n          <div class=\"background-margin-style\">\r\n            <dx-color-box [(value)]=\"backgroundColor\" [readOnly]=\"isColorReadOnly\" (onFocusIn)=\"onColorFocusIn()\"\r\n              (onValueChanged)=\"onChangeOfBackgroundColor()\">\r\n            </dx-color-box>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n",
        styles: [""]
    })
], ColorFormatComponent);
export { ColorFormatComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sb3ItZm9ybWF0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tcG9uZW50cy9jb2xvci1mb3JtYXQvY29sb3ItZm9ybWF0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBRXZFLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzVELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBTzVGLElBQWEsb0JBQW9CLEdBQWpDLE1BQWEsb0JBQW9CO0lBc0QvQixZQUE2QixjQUFpQztRQUFqQyxtQkFBYyxHQUFkLGNBQWMsQ0FBbUI7UUFwRDlDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLDBCQUFxQixHQUFHLElBQUksQ0FBQztRQUM3QixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQix1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDMUIsNEJBQXVCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLDhCQUF5QixHQUFHLEtBQUssQ0FBQztRQUNsQyxjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGtCQUFhLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUN0Qix5QkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDNUIsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFDeEIsZUFBVSxHQUFXLGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7UUF1QjlELHlCQUFvQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDMUMsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBRTdELG9CQUFlLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLG1DQUE4QixHQUFHLHVCQUF1QixDQUFDO1FBRXpELDZCQUF3QixHQUFHLG1CQUFtQixDQUFDO1FBQy9DLG9DQUErQixHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztRQUM1RCxtQkFBYyxHQUFHLENBQUMsQ0FBQztRQUNuQixpQkFBWSxHQUFhLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUMxRCw0QkFBdUIsR0FBRyxLQUFLLENBQUM7UUFDaEMsaUJBQVksR0FBaUIsRUFBRSxDQUFDO1FBQ2hDLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBRXZCLGNBQVMsR0FBRyxFQUFFLENBQUM7SUFFbUQsQ0FBQztJQXBDbkUsSUFBSSw0QkFBNEIsQ0FBQyxJQUFhO1FBQzVDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUM7UUFDcEMsSUFBSSxDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLHVCQUF1QixDQUFDO1FBQ3JKLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUM7SUFDdkQsQ0FBQztJQUNELElBQUksNEJBQTRCLEtBQWMsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO0lBR3BGLElBQUksa0JBQWtCLENBQUMsVUFBeUI7O1FBQzlDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFVBQVUsSUFBSSxFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLEVBQUUsRUFBRSxDQUFDO1FBQ3ZFLElBQUksT0FBQSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLFNBQVMsQ0FBQyxNQUFNLElBQUcsQ0FBQyxFQUFFO1lBQ2pELElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1NBQzlCO2FBQU07WUFDTCxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztTQUM5QjtJQUNILENBQUM7SUFDRCxJQUFJLGtCQUFrQixLQUFLLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztJQXFCNUQsUUFBUTtRQUNOLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDckYsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7U0FDOUI7SUFDSCxDQUFDO0lBRUQsd0JBQXdCO0lBRWpCLHFCQUFxQixDQUFDLElBQXlCO1FBQ3BELElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNkLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLENBQUM7U0FDdkM7UUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUNsRSxNQUFNLElBQUksR0FBRztZQUNYLE9BQU8sRUFBRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxJQUFJLEVBQUU7U0FDL0MsQ0FBQztRQUNGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEVBQUU7WUFDckQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyQyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUMvQjtRQUNELFNBQVM7UUFDVCw0RkFBNEY7UUFDNUYsSUFBSTtJQUVOLENBQUM7SUFFTSxtQkFBbUI7UUFDeEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUM7UUFDbkQsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVNLHlCQUF5QjtRQUM5QixJQUFJLENBQUMsWUFBWSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxJQUFJLEVBQUUsQ0FBQztRQUMvRCxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRU0sY0FBYztRQUNuQixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDeEIsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsd0NBQXdDLEVBQUUsQ0FBQyxDQUFDO1NBQ2xGO1FBQ0QsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQztJQUN4QyxDQUFDO0lBRU0sWUFBWSxDQUFDLElBQTBCLEVBQUUsS0FBaUI7UUFDL0QsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxLQUFLLHNCQUFzQixDQUFDLFNBQVM7WUFDeEUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQzttQkFDNUgsSUFBSSxDQUFDLHlCQUF5QixFQUFFO1lBQ25DLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLHdDQUF3QyxFQUFFLENBQUMsQ0FBQztZQUNqRixPQUFPO1NBQ1I7UUFDRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTyxFQUFFO1lBQ3pCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtnQkFDaEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkMsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO2dCQUMvRixDQUFDLENBQUMsQ0FBQzthQUNKO1NBQ0Y7UUFDRCxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7UUFFbkMsSUFBSSxLQUFLLEVBQUU7WUFDVCxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDO1NBQ3ZDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO1lBQ2xELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLENBQUUsK0RBQStEO1NBQzFGO0lBQ0gsQ0FBQztJQUVNLHNCQUFzQjtRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFO1lBQ25DLE9BQU87U0FDUjtRQUNELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzNCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO29CQUNoQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7aUJBQ3ZEO2dCQUNELFFBQVEsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNsRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDTixNQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQzlFLElBQUksS0FBYSxFQUFFLFdBQW9CLENBQUM7WUFDeEMsV0FBVyxHQUFHLEtBQUssQ0FBQztZQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDO1lBQ2hFLEtBQUssS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsb0JBQW9CLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUM1RCxJQUFJLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsa0JBQWtCLENBQUMsU0FBUyxFQUFFO29CQUMvRSxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUNoRCxXQUFXLEdBQUcsSUFBSSxDQUFDO29CQUNuQixNQUFNO2lCQUNQO2FBQ0Y7WUFDRCxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNoQixvQkFBb0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2FBQzlDO1lBRUQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsS0FBSyxFQUFFLG9CQUFvQixDQUFDLENBQUM7U0FDeEU7SUFDSCxDQUFDO0lBRU0saUJBQWlCO1FBQ3RCLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQsMkJBQTJCO0lBRTNCLHlCQUF5QjtJQUVqQixZQUFZLENBQUMsSUFBMEI7UUFDN0MsSUFBSSxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQztRQUM1RixRQUFRLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDaEIsS0FBSyxlQUFlO2dCQUNsQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUN0QyxNQUFNO1lBQ1IsS0FBSyxnQkFBZ0I7Z0JBQ25CLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBQ3ZDLE1BQU07WUFDUixLQUFLLGlCQUFpQjtnQkFDcEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDeEMsTUFBTTtZQUNSLEtBQUssU0FBUztnQkFDWixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDbEMsTUFBTTtZQUVSLEtBQUssVUFBVTtnQkFDYixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDbkMsTUFBTTtZQUNSLEtBQUssTUFBTTtnQkFDVCxJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQzVFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5QixNQUFNO1lBQ1IsS0FBSyxXQUFXO2dCQUNkLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxVQUFVLEVBQUUsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDakYsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7Z0JBQzlCLE1BQU07WUFDUixLQUFLLFFBQVE7Z0JBQ1gsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUM5RSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztnQkFDOUIsTUFBTTtZQUNSLEtBQUssVUFBVTtnQkFDYixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQy9GLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDaEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDckYsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLE1BQU07aUJBQ1A7cUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtvQkFDeEMsSUFBSSxDQUFDLG1DQUFtQyxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUMxRSxNQUFNO2lCQUNQO2dCQUNELGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDeEIsTUFBTTtZQUNSLEtBQUssWUFBWTtnQkFDZixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQy9GLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7d0JBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFtQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUM7cUJBQzdGO29CQUNELElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxVQUFVLEVBQUUsZUFBZSxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQ2pHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDdEIsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDaEcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUc7d0JBQ3pCLElBQUksRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFO3dCQUMzQixTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWM7cUJBQy9CLENBQUM7b0JBQ0YsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLE1BQU07aUJBQ1A7Z0JBQ0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxlQUFlO2dCQUNsQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQy9GLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7d0JBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFtQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUM7cUJBQzdGO29CQUVELElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxVQUFVLEVBQUUsZUFBZSxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQ2pHLElBQUksSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLEVBQUU7d0JBQzNCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzt3QkFDdEIsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDaEcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUc7NEJBQ3pCLElBQUksRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFOzRCQUMzQixTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWM7eUJBQy9CLENBQUM7cUJBQ0g7eUJBQU0sSUFBSSxJQUFJLENBQUMsY0FBYyxLQUFLLENBQUMsRUFBRTt3QkFDcEMsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxlQUFlLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsQ0FBQztxQkFDbEc7b0JBQ0QsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLE1BQU07aUJBQ1A7Z0JBQ0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixNQUFNO1lBQ1IsS0FBSyxZQUFZO2dCQUNmLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BDLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDL0YsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxZQUFZLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNsRixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ3BGLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUM5QixNQUFNO2lCQUNQO3FCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUU7b0JBQ3hDLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDM0UsTUFBTTtpQkFDUDtnQkFDRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUMvRixJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUM3RSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ2xGLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUM5QixNQUFNO2lCQUNQO3FCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsdUJBQXVCLEVBQUU7b0JBQ3hDLElBQUksQ0FBQyxtQ0FBbUMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDcEUsTUFBTTtpQkFDUDtnQkFDRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7Z0JBQ3hCLE1BQU07WUFDUixLQUFLLFVBQVU7Z0JBQ2IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEMsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtvQkFDNUYsSUFBSSxDQUFDLGlDQUFpQyxDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUNoRixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDOUQsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7b0JBQzlCLE1BQU07aUJBQ1A7cUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtvQkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQzlELE1BQU07aUJBQ1A7Z0JBQ0QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUN4QixNQUFNO1NBQ1Q7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBQ2pDLFVBQVUsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsUUFBUSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ2xFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNQO1FBQ0QsSUFBSSxnQkFBZ0IsRUFBRTtZQUNwQixRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxrREFBa0QsRUFBRSxDQUFDLENBQUM7WUFDM0YsUUFBUSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ2hFLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNkLENBQUM7SUFFTyxrQkFBa0IsQ0FBQyxJQUEwQixFQUFFLGFBQXFCO1FBQzFFLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbkUsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDL0I7SUFDSCxDQUFDO0lBRU8sY0FBYyxDQUFDLElBQTBCLEVBQUUsUUFBZ0I7UUFDakUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzFDLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ2xFLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRTtZQUNoQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNuQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQ2pELEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLENBQUMsQ0FBQztZQUNoRixDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsdUJBQXVCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDL0I7SUFDSCxDQUFDO0lBRU8sbUNBQW1DLENBQUMsSUFBMEIsRUFBRSxJQUFZLEVBQUUsV0FBbUIsRUFBRSxTQUFrQjtRQUMzSCxJQUFJLENBQUMsaUNBQWlDLENBQUMsVUFBVSxFQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN4RSxDQUFDO0lBRU8sOEJBQThCLENBQUMsSUFBSSxHQUFHLEVBQUU7UUFDOUMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzdFLElBQUksSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFFO2dCQUNsRSxJQUFJLENBQUMsOEJBQThCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7YUFDckU7U0FDRjtJQUNILENBQUM7SUFFTyx3QkFBd0IsQ0FBQyxJQUFJLEdBQUcsRUFBRTtRQUN4QyxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNyRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFFO2dCQUN0QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzthQUN6QjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVPLGlDQUFpQyxDQUFDLFVBQWtCLEVBQUUsV0FBbUIsRUFBRSxTQUFrQjtRQUNuRyxJQUFJLHFCQUFxQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2hFLElBQUksU0FBUyxFQUFFO1lBQ2IsSUFBSSxxQkFBcUIsSUFBSSxxQkFBcUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3pELE1BQU0sZUFBZSxHQUFHLENBQUMsVUFBVSxFQUFFLFlBQVksRUFBRSxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ3hFLElBQUksZUFBZSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDN0MsS0FBSyxJQUFJLFdBQVcsR0FBRyxDQUFDLEVBQUUsV0FBVyxHQUFHLGVBQWUsQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLEVBQUU7d0JBQzdFLHFCQUFxQixHQUFHLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7cUJBQ3pGO2lCQUNGO2dCQUNELHFCQUFxQixHQUFHLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQy9GLHFCQUFxQixHQUFHLHFCQUFxQixHQUFHLEdBQUcsR0FBRyxXQUFXLENBQUM7YUFDbkU7aUJBQU07Z0JBQ0wscUJBQXFCLEdBQUcsV0FBVyxDQUFDO2FBQ3JDO1NBQ0Y7YUFBTSxJQUFJLHFCQUFxQixJQUFJLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNuRixxQkFBcUIsR0FBRyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2hHO1FBRUQsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsR0FBRyxxQkFBcUIsQ0FBQztRQUN0RCxNQUFNLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBSSxVQUFVLEtBQUssVUFBVSxFQUFFO1lBQzdCLEtBQUssQ0FBQyxVQUFVLENBQUMsR0FBRyxxQkFBcUIsQ0FBQztTQUMzQzthQUFNO1lBQ0wsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQztTQUNyRTtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQztJQUVPLG1CQUFtQixDQUFDLElBQUksR0FBRyxFQUFFO1FBQ25DLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO1FBQy9DLElBQUksYUFBYSxFQUFFO1lBQ2pCLGFBQWEsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDeEcsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsYUFBYSxDQUFDO1NBQzVDO1FBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzFFLElBQUksSUFBSSxDQUFDLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFFO2dCQUMvRCxJQUFJLENBQUMsOEJBQThCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7YUFDbEU7U0FDRjtJQUNILENBQUM7SUFFTyxrQkFBa0I7UUFDeEIsSUFBSSxDQUFTLENBQUM7UUFDZCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RFLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztTQUNsRTtRQUVELEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDekUsSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQ3JFO1FBRUQsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNuRSxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDL0Q7UUFFRCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3pFLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztTQUNyRTtRQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxlQUFlLEdBQUcsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFFTyxxQkFBcUI7UUFDM0IsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDMUIsSUFBSSxpQkFBaUIsR0FBaUIsSUFBSSxNQUFNLEVBQWtCLENBQUM7UUFDbkUsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLE1BQU0sc0JBQXNCLEdBQW1CLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ2hHLE1BQU0sb0JBQW9CLEdBQW1CLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dCQUN0RSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUNyRyxpQkFBaUIsR0FBRyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUM7Z0JBQ2pILG9CQUFvQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUN2RztRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUNyRSxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQXlCLENBQUM7U0FDcEQ7UUFDRCxJQUFJLGlCQUFpQixFQUFFO1lBQ3JCLElBQUksaUJBQWlCLENBQUMsUUFBUSxFQUFFO2dCQUM5QixNQUFNLFNBQVMsR0FBRyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLENBQVMsQ0FBQztnQkFDZCxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsUUFBZ0IsRUFBRSxFQUFFO29CQUNyQyxJQUFJLFFBQVEsS0FBSyxNQUFNLElBQUksUUFBUSxLQUFLLFdBQVcsSUFBSSxRQUFRLEtBQUssUUFBUSxFQUFFO3dCQUM1RSxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOzRCQUN6RSxJQUFJLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtnQ0FDdEUsSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2dDQUNuRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztnQ0FDdkgsTUFBTTs2QkFDUDt5QkFDRjtxQkFDRjtvQkFFRCxJQUFJLFFBQVEsS0FBSyxVQUFVLElBQUksUUFBUSxLQUFLLFlBQVksSUFBSSxRQUFRLEtBQUssT0FBTyxJQUFJLFFBQVEsS0FBSyxVQUFVLEVBQUU7d0JBQzNHLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3RFLElBQUksSUFBSSxDQUFDLDhCQUE4QixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO2dDQUNuRSxJQUFJLENBQUMsOEJBQThCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0NBQ2hFLElBQUksQ0FBQyxjQUFjLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBRSxpQkFBaUIsQ0FBQyxNQUFtQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dDQUN0SCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLFFBQVEsQ0FBQztnQ0FDdkgsTUFBTTs2QkFDUDt5QkFDRjtxQkFDRjtvQkFFRCxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7d0JBQ3BDLElBQUksQ0FBQyxjQUFjLEdBQUksaUJBQWlCLENBQUMsTUFBbUMsQ0FBQyxTQUFTLENBQUM7cUJBQ3hGO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFFRCxJQUFJLGlCQUFpQixDQUFDLEtBQUssRUFBRTtnQkFDM0IsTUFBTSxjQUFjLEdBQUcsaUJBQWlCLENBQUMsYUFBYSxLQUFLLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ2pGLEtBQUssSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUUsRUFBRTtvQkFDN0UsSUFBSSxjQUFjLElBQUksSUFBSSxDQUFDLDhCQUE4QixDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEtBQUssU0FBUyxFQUFFO3dCQUNwRixJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7d0JBQy9ELE1BQU07cUJBQ1A7eUJBQU0sSUFBSSxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsS0FBSyxVQUFVLEVBQUU7d0JBQzFFLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzt3QkFDL0QsTUFBTTtxQkFDUDtpQkFDRjthQUNGO1lBRUQsSUFBSSxDQUFTLENBQUM7WUFDZCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN6RSxJQUFJLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxLQUFLLGlCQUFpQixDQUFDLFNBQVMsRUFBRTtvQkFDekYsSUFBSSxDQUFDLDhCQUE4QixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO2lCQUNwRTthQUNGO1lBRUQsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbkUsSUFBSSxJQUFJLENBQUMsOEJBQThCLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksS0FBSyxpQkFBaUIsQ0FBQyxhQUFhLEVBQUU7b0JBQ3ZGLElBQUksQ0FBQyw4QkFBOEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztpQkFDOUQ7YUFDRjtZQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsaUJBQWlCLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQztZQUNuRCxJQUFJLENBQUMsZUFBZSxHQUFHLGlCQUFpQixDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUM7WUFDL0QsSUFBSSxDQUFDLFlBQVksR0FBRyxpQkFBaUIsQ0FBQztTQUN2QztRQUVELElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDbEQsQ0FBQztJQUVPLHVCQUF1QixDQUFDLElBQUk7UUFDbEMsTUFBTSxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUNqRCxNQUFNLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDekQsSUFBSSxJQUFJLENBQUMsR0FBRyxLQUFLLGVBQWUsSUFBSSxJQUFJLENBQUMsR0FBRyxLQUFLLFlBQVksRUFBRTtnQkFDN0QsT0FBTyxJQUFJLENBQUM7YUFDYjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztRQUNyQyxPQUFPLGdCQUFnQixDQUFDO0lBQzFCLENBQUM7SUFFTyxjQUFjO1FBQ3BCLElBQUksaUJBQWlCLEdBQUcsWUFBWSxDQUFDO1FBQ3JDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDNUIsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFtQyxDQUFDLElBQUksQ0FBQztnQkFDaEgsQ0FBQyxDQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBbUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztTQUNoRjtRQUNELE9BQU8saUJBQWlCLENBQUM7SUFDM0IsQ0FBQztJQUVPLG1CQUFtQixDQUFDLEdBQVcsRUFBRSxJQUFZLEVBQUUsSUFBSSxHQUFHLElBQUk7UUFDaEUsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsT0FBTyxFQUFFLENBQUM7U0FDWDtRQUNELEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7UUFDdkksUUFBUSxJQUFJLEVBQUU7WUFDWixLQUFLLEtBQUs7Z0JBQ1IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkQsS0FBSyxLQUFLO2dCQUNSLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDbEQsTUFBTTtTQUNUO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDWixDQUFDO0lBRU8scUJBQXFCO1FBQzNCLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO0lBQ3hDLENBQUM7SUFFTyxxQkFBcUIsQ0FBQyxLQUFpQjtRQUM3QyxNQUFNLElBQUksR0FBSSxLQUFLLENBQUMsYUFBNkIsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1FBQzFFLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQztRQUN2QyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDckMsTUFBTSxVQUFVLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNyRCxVQUFVLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUMzQyxVQUEwQixDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsUUFBUSxDQUFDO1FBQ2hELFVBQTBCLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxTQUFTLENBQUM7SUFDckQsQ0FBQztDQUdGLENBQUE7O1lBN2U4QyxpQkFBaUI7O0FBcERyRDtJQUFSLEtBQUssRUFBRTs2REFBK0I7QUFDOUI7SUFBUixLQUFLLEVBQUU7bUVBQXFDO0FBQ3BDO0lBQVIsS0FBSyxFQUFFOzZEQUErQjtBQUM5QjtJQUFSLEtBQUssRUFBRTswREFBNEI7QUFDM0I7SUFBUixLQUFLLEVBQUU7Z0VBQWtDO0FBQ2pDO0lBQVIsS0FBSyxFQUFFO3FFQUF1QztBQUN0QztJQUFSLEtBQUssRUFBRTt1RUFBMEM7QUFDekM7SUFBUixLQUFLLEVBQUU7dURBQXlCO0FBQ3hCO0lBQVIsS0FBSyxFQUFFOzJEQUE2QjtBQUM1QjtJQUFSLEtBQUssRUFBRTs2REFBK0I7QUFDOUI7SUFBUixLQUFLLEVBQUU7OERBQThCO0FBQzdCO0lBQVIsS0FBSyxFQUFFO2tFQUFvQztBQUNuQztJQUFSLEtBQUssRUFBRTs2REFBZ0M7QUFDL0I7SUFBUixLQUFLLEVBQUU7d0RBQXVFO0FBRy9FO0lBREMsS0FBSyxFQUFFO3dFQUtQO0FBSUQ7SUFEQyxLQUFLLEVBQUU7OERBU1A7QUFJUztJQUFULE1BQU0sRUFBRTtrRUFBa0Q7QUFDakQ7SUFBVCxNQUFNLEVBQUU7MkRBQW9EO0FBdkNsRCxvQkFBb0I7SUFMaEMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLGtCQUFrQjtRQUM1QixvOUlBQTRDOztLQUU3QyxDQUFDO0dBQ1csb0JBQW9CLENBbWlCaEM7U0FuaUJZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENHRlN0b3JhZ2VLZXlzIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2VudW1zJztcclxuaW1wb3J0IHsgQ0dGVXRpbGl0eVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9jZ2YtdXRpbGl0eS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29sdW1uRm9ybWF0LCBGb3JtYXRPcHRpb25EYXRhVHlwZSwgRHhFZGl0b3JWYWx1ZUNoYW5nZSB9IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvY29udHJhY3RzL2NvbHVtbic7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUEJJR3JpZENvbHVtbiwgRGV2RXh0cmVtZURhdGFUeXBlRm9ybWF0IH0gZnJvbSAnLi4vY29udHJhY3RzL2dyaWQnO1xyXG5pbXBvcnQgeyBhcHBUb2FzdCB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy91dGlsaXR5RnVuY3Rpb25zJztcclxuaW1wb3J0IHsgY29sdW1uRm9ybWF0dGluZ09wdGlvbnMsIGN1c3RvbUFjdGlvbkNvbHVtbkluZm8gfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvY29uc3RhbnRzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAncGJpLWNvbG9yLWZvcm1hdCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbG9yLWZvcm1hdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29sb3ItZm9ybWF0LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbG9yRm9ybWF0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZUFsaWdubWVudCA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZUJhY2tncm91bmRDb2xvciA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZUZvbnRTdHlsZSA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZUZvcm1hdCA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZUZvcm1hdFRvZ2dsZSA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIGVuYWJsZUlucHV0Q2FwdGlvbkZpZWxkID0gdHJ1ZTtcclxuICBASW5wdXQoKSBwdWJsaWMgZW5hYmxlSW5wdXREYXRhRmllbGRDaGVjayA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVQaW4gPSB0cnVlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBlbmFibGVTdG9yYWdlID0gdHJ1ZTtcclxuICBASW5wdXQoKSBwdWJsaWMgZW5hYmxlVGV4dENvbG9yID0gdHJ1ZTtcclxuICBASW5wdXQoKSBwdWJsaWMgZ3JpZEluc3RhbmNlTGlzdCA9IFtdO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBpc01hc3RlckdyaWRTZWxlY3RlZCA9IHRydWU7XHJcbiAgQElucHV0KCkgcHVibGljIHNob3dCYXNpY0Zvcm1hdCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBzdG9yYWdlS2V5OiBzdHJpbmcgPSBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5mb3JtYXREYXRhXTtcclxuXHJcbiAgQElucHV0KClcclxuICBzZXQgZGlzYWJsZURlY2ltYWxQcmVjaXNpb25JbnB1dChkYXRhOiBib29sZWFuKSB7XHJcbiAgICB0aGlzLmRpc2FibGVEZWNpbWFsUHJlY2lzaW9uID0gZGF0YTtcclxuICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhID0gdGhpcy5kaXNhYmxlRGVjaW1hbFByZWNpc2lvbiA/IHRoaXMucmVtb3ZlUHJlY2lzaW9uQ29udHJvbHMoY29sdW1uRm9ybWF0dGluZ09wdGlvbnMpIDogY29sdW1uRm9ybWF0dGluZ09wdGlvbnM7XHJcbiAgICB0aGlzLmlzQ29sb3JSZWFkT25seSA9ICF0aGlzLmRpc2FibGVEZWNpbWFsUHJlY2lzaW9uO1xyXG4gIH1cclxuICBnZXQgZGlzYWJsZURlY2ltYWxQcmVjaXNpb25JbnB1dCgpOiBib29sZWFuIHsgcmV0dXJuIHRoaXMuZGlzYWJsZURlY2ltYWxQcmVjaXNpb247IH1cclxuXHJcbiAgQElucHV0KClcclxuICBzZXQgc2VsZWN0ZWRDb2x1bW5JbmZvKGNvbHVtbkl0ZW06IFBCSUdyaWRDb2x1bW4pIHtcclxuICAgIHRoaXMuY2FuVXBkYXRlQ29sdW1uRm9ybWF0dGluZyA9IGZhbHNlO1xyXG4gICAgdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEgPSBjb2x1bW5JdGVtIHx8IHsgY2FwdGlvbjogJycsIGRhdGFGaWVsZDogJycgfTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YT8uZGF0YUZpZWxkLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5wcmVwYXJlU2VsZWN0ZWRPcHRpb24oKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucmVzZXRBbGxGb3JtYXRPcHRpb25zKCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldCBzZWxlY3RlZENvbHVtbkluZm8oKSB7IHJldHVybiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YTsgfVxyXG5cclxuXHJcbiAgQE91dHB1dCgpIHB1YmxpYyBmb3JtYXRPcHRpb25zQ2hhbmdlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGZvcm1hdFRvZ2dsZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XHJcblxyXG4gIGJhY2tncm91bmRDb2xvciA9ICcnO1xyXG4gIGJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YSA9IGNvbHVtbkZvcm1hdHRpbmdPcHRpb25zO1xyXG4gIGNhblVwZGF0ZUNvbHVtbkZvcm1hdHRpbmc6IGJvb2xlYW47XHJcbiAgY2FwdGlvblZhbGlkYXRpb25QYXR0ZXJuID0gL15bYS16QS1aMC05X1xcc10rJC87XHJcbiAgY29sdW1uc0FjdGl2ZVByb3BlcnRpZXNUYWJzTGlzdCA9IFt7IHRpdGxlOiAnRk9STUFUVElORycgfV07XHJcbiAgZGVjaW1hbENvdW50ZXIgPSAwO1xyXG4gIGRlY2ltYWxUeXBlczogc3RyaW5nW10gPSBbJ2NvbW1hJywgJ3BlcmNlbnQnLCAnY3VycmVuY3knXTtcclxuICBkaXNhYmxlRGVjaW1hbFByZWNpc2lvbiA9IGZhbHNlO1xyXG4gIGZvcm1hdE9iamVjdDogQ29sdW1uRm9ybWF0ID0ge307XHJcbiAgaXNDb2xvclJlYWRPbmx5ID0gdHJ1ZTtcclxuICBzZWxlY3RlZENvbHVtbkRhdGE6IFBCSUdyaWRDb2x1bW47XHJcbiAgdGV4dENvbG9yID0gJyc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcmVhZG9ubHkgdXRpbGl0eVNlcnZpY2U6IENHRlV0aWxpdHlTZXJ2aWNlKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnJlc2V0QmFzaWNTZXR0aW5ncygpO1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFGaWVsZCAmJiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQubGVuZ3RoID4gMCkge1xyXG4gICAgICB0aGlzLnByZXBhcmVTZWxlY3RlZE9wdGlvbigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8jcmVnaW9uIFB1YmxpYyBNZXRob2RzXHJcblxyXG4gIHB1YmxpYyBvbkNhcHRpb25WYWx1ZUNoYW5nZWQoYXJnczogRHhFZGl0b3JWYWx1ZUNoYW5nZSk6IHZvaWQge1xyXG4gICAgaWYgKGFyZ3MuZXZlbnQpIHtcclxuICAgICAgdGhpcy5jYW5VcGRhdGVDb2x1bW5Gb3JtYXR0aW5nID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIHRoaXMuZm9ybWF0T2JqZWN0LmNhcHRpb24gPSB0aGlzLnNlbGVjdGVkQ29sdW1uSW5mby5jYXB0aW9uIHx8ICcnO1xyXG4gICAgY29uc3QgZGF0YSA9IHtcclxuICAgICAgY2FwdGlvbjogdGhpcy5zZWxlY3RlZENvbHVtbkluZm8uY2FwdGlvbiB8fCAnJ1xyXG4gICAgfTtcclxuICAgIGlmIChkYXRhLmNhcHRpb24ubWF0Y2godGhpcy5jYXB0aW9uVmFsaWRhdGlvblBhdHRlcm4pKSB7XHJcbiAgICAgIHRoaXMuZm9ybWF0T3B0aW9uc0NoYW5nZWQuZW1pdChkYXRhKTtcclxuICAgICAgdGhpcy5vbkZvcm1hdE9wdGlvbnNDaGFuZ2VkKCk7XHJcbiAgICB9XHJcbiAgICAvLyBlbHNlIHtcclxuICAgIC8vICAgdGhpcy5zZWxlY3RlZENvbHVtbkluZm8uY2FwdGlvbiA9IGFyZ3MgJiYgYXJncy5wcmV2aW91c1ZhbHVlID8gYXJncy5wcmV2aW91c1ZhbHVlIDogJyc7XHJcbiAgICAvLyB9XHJcblxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uQ2hhbmdlT2ZUZXh0Q29sb3IoKTogdm9pZCB7XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdC50ZXh0Q29sb3IgPSB0aGlzLnRleHRDb2xvciB8fCAnJztcclxuICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uQ2hhbmdlT2ZCYWNrZ3JvdW5kQ29sb3IoKTogdm9pZCB7XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdC5iYWNrZ3JvdW5kQ29sb3IgPSB0aGlzLmJhY2tncm91bmRDb2xvciB8fCAnJztcclxuICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uQ29sb3JGb2N1c0luKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuaXNDb2xvclJlYWRPbmx5KSB7XHJcbiAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ3dhcm5pbmcnLCBtZXNzYWdlOiAnQ29sdW1uIEZvcm1hdDogUGxlYXNlIHNlbGVjdCBhIGNvbHVtbi4nIH0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5jYW5VcGRhdGVDb2x1bW5Gb3JtYXR0aW5nID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBmb3JtYXRDb2x1bW4oYXJnczogRm9ybWF0T3B0aW9uRGF0YVR5cGUsIGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkID09PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCB8fFxyXG4gICAgICAoKCF0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQpIHx8ICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQgJiYgdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkLmxlbmd0aCA9PT0gMCkpXHJcbiAgICAgICYmIHRoaXMuZW5hYmxlSW5wdXREYXRhRmllbGRDaGVjaykge1xyXG4gICAgICBhcHBUb2FzdCh7IHR5cGU6ICd3YXJuaW5nJywgbWVzc2FnZTogJ0NvbHVtbiBGb3JtYXQ6IFBsZWFzZSBzZWxlY3QgYSBjb2x1bW4uJyB9KTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKGFyZ3MudHlwZSA9PT0gJ3JpZ2h0Jykge1xyXG4gICAgICBpZiAodGhpcy5ncmlkSW5zdGFuY2VMaXN0Lmxlbmd0aCkge1xyXG4gICAgICAgIHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWQgPT4ge1xyXG4gICAgICAgICAgZ3JpZC5jb2x1bW5PcHRpb24oY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5kYXRhRmllbGQsIHsgZml4ZWQ6IHRydWUsIGZpeGVkUG9zaXRpb246ICdyaWdodCcgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMucHJlcGFyZUxvYWRlckxvY2F0aW9uKGV2ZW50KTtcclxuICAgIGFyZ3MuaXNTZWxlY3RlZCA9ICFhcmdzLmlzU2VsZWN0ZWQ7XHJcblxyXG4gICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgIHRoaXMuY2FuVXBkYXRlQ29sdW1uRm9ybWF0dGluZyA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBpZiAoIXRoaXMuc2VsZWN0Rm9ybWF0KGFyZ3MpKSB7XHJcbiAgICAgIGFyZ3MuaXNTZWxlY3RlZCA9ICFhcmdzLmlzU2VsZWN0ZWQ7XHJcbiAgICB9XHJcbiAgICBpZiAoYXJncy5rZXkudG9Mb3dlckNhc2UoKS5pbmRleE9mKCdkZWNpbWFsJykgPiAtMSkge1xyXG4gICAgICBhcmdzLmlzU2VsZWN0ZWQgPSBmYWxzZTsgIC8vIHRoaXMgY2FzZSBtYWtlcyBzdXJlIHJlbW92ZS9hZGQgZGVjaW1hbCBhcmUgbm90IGhpZ2hsaWdodGVkLlxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMuY2FuVXBkYXRlQ29sdW1uRm9ybWF0dGluZykge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGEpIHtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5sZW5ndGgpIHtcclxuICAgICAgICAgIHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGl0ZW0gPT4gaXRlbS5yZXBhaW50KCkpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubG9hZGVyJykuY2xhc3NMaXN0LmFkZCgnZGlzcGxheS1ub25lJyk7XHJcbiAgICAgIH0sIDApO1xyXG4gICAgICBjb25zdCBmb3JtYXREYXRhQ29sbGVjdGlvbiA9IHRoaXMuZ2V0T3JTZXRTZXNzaW9uRGF0YSh0aGlzLnN0b3JhZ2VLZXksICdnZXQnKTtcclxuICAgICAgbGV0IGluZGV4OiBudW1iZXIsIGRhdGFVcGRhdGVkOiBib29sZWFuO1xyXG4gICAgICBkYXRhVXBkYXRlZCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmZvcm1hdE9iamVjdC5kYXRhRmllbGQgPSB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQ7XHJcbiAgICAgIGZvciAoaW5kZXggPSAwOyBpbmRleCA8IGZvcm1hdERhdGFDb2xsZWN0aW9uLmxlbmd0aDsgaW5kZXgrKykge1xyXG4gICAgICAgIGlmIChmb3JtYXREYXRhQ29sbGVjdGlvbltpbmRleF0uZGF0YUZpZWxkID09PSB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQpIHtcclxuICAgICAgICAgIGZvcm1hdERhdGFDb2xsZWN0aW9uW2luZGV4XSA9IHRoaXMuZm9ybWF0T2JqZWN0O1xyXG4gICAgICAgICAgZGF0YVVwZGF0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlmICghZGF0YVVwZGF0ZWQpIHtcclxuICAgICAgICBmb3JtYXREYXRhQ29sbGVjdGlvbi5wdXNoKHRoaXMuZm9ybWF0T2JqZWN0KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5nZXRPclNldFNlc3Npb25EYXRhKHRoaXMuc3RvcmFnZUtleSwgJ3NldCcsIGZvcm1hdERhdGFDb2xsZWN0aW9uKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyB0b2dnbGVCYXNpY0Zvcm1hdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuc2hvd0Jhc2ljRm9ybWF0ID0gIXRoaXMuc2hvd0Jhc2ljRm9ybWF0O1xyXG4gICAgdGhpcy5mb3JtYXRUb2dnbGVkLmVtaXQodGhpcy5zaG93QmFzaWNGb3JtYXQpO1xyXG4gIH1cclxuXHJcbiAgLy8jZW5kcmVnaW9uIFB1YmxpYyBNZXRob2RzXHJcblxyXG4gIC8vI3JlZ2lvbiBQcml2YXRlIE1ldGhvZHNcclxuXHJcbiAgcHJpdmF0ZSBzZWxlY3RGb3JtYXQoYXJnczogRm9ybWF0T3B0aW9uRGF0YVR5cGUpOiBib29sZWFuIHtcclxuICAgIGxldCBmb3JtYXROb3RBbGxvd2VkID0gZmFsc2U7XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdC5kYXRhVHlwZSA9IHRoaXMuZm9ybWF0T2JqZWN0LmRhdGFUeXBlIHx8IHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlO1xyXG4gICAgc3dpdGNoIChhcmdzLmtleSkge1xyXG4gICAgICBjYXNlICdsZWZ0QWxpZ25tZW50JzpcclxuICAgICAgICB0aGlzLnNldEFsaWdubWVudEZvcm1hdChhcmdzLCAnbGVmdCcpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdyaWdodEFsaWdubWVudCc6XHJcbiAgICAgICAgdGhpcy5zZXRBbGlnbm1lbnRGb3JtYXQoYXJncywgJ3JpZ2h0Jyk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgJ2NlbnRlckFsaWdubWVudCc6XHJcbiAgICAgICAgdGhpcy5zZXRBbGlnbm1lbnRGb3JtYXQoYXJncywgJ2NlbnRlcicpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdsZWZ0UGluJzpcclxuICAgICAgICB0aGlzLnNldFBpblBvc2l0aW9uKGFyZ3MsICdsZWZ0Jyk7XHJcbiAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICBjYXNlICdyaWdodFBpbic6XHJcbiAgICAgICAgdGhpcy5zZXRQaW5Qb3NpdGlvbihhcmdzLCAncmlnaHQnKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnYm9sZCc6XHJcbiAgICAgICAgdGhpcy5wcmVwYXJlQ29sdW1uRm9ybWF0VXNpbmdDbGFzc05hbWUoJ2Nzc0NsYXNzJywgJ2JvbGQnLCBhcmdzLmlzU2VsZWN0ZWQpO1xyXG4gICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICd1bmRlcmxpbmUnOlxyXG4gICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICd1bmRlcmxpbmUnLCBhcmdzLmlzU2VsZWN0ZWQpO1xyXG4gICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdpdGFsaWMnOlxyXG4gICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdpdGFsaWMnLCBhcmdzLmlzU2VsZWN0ZWQpO1xyXG4gICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdjdXJyZW5jeSc6XHJcbiAgICAgICAgdGhpcy5yZXNldFNlbGVjdGVkRm9ybWF0KGFyZ3MudHlwZSk7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlICYmIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlLmluZGV4T2YoJ251bWJlcicpID4gLTEpIHtcclxuICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdjdXJyZW5jeScsIGFyZ3MuaXNTZWxlY3RlZCk7XHJcbiAgICAgICAgICB0aGlzLmRlY2ltYWxDb3VudGVyID0gMDtcclxuICAgICAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCA9IGFyZ3MuaXNTZWxlY3RlZCA/IHsgdHlwZTogJ2N1cnJlbmN5JywgcHJlY2lzaW9uOiAyIH0gOiAnJztcclxuICAgICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5lbmFibGVJbnB1dENhcHRpb25GaWVsZCkge1xyXG4gICAgICAgICAgdGhpcy5jb2x1bW5Gb3JtYXRDaGFuZ2VGb3JEYXRhRGljdGlvbmFyeShhcmdzLCAnY3VycmVuY3knLCAnY3VycmVuY3knLCAyKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtYXROb3RBbGxvd2VkID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnYWRkRGVjaW1hbCc6XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlICYmIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlLmluZGV4T2YoJ251bWJlcicpID4gLTEpIHtcclxuICAgICAgICAgIGlmICh0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWNpbWFsQ291bnRlciA9ICh0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQgYXMgRGV2RXh0cmVtZURhdGFUeXBlRm9ybWF0KS5wcmVjaXNpb24gfHwgMDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdkZWNpbWFsQWRkZWQtJyArIHRoaXMuZGVjaW1hbENvdW50ZXIsIGZhbHNlKTtcclxuICAgICAgICAgIHRoaXMuZGVjaW1hbENvdW50ZXIrKztcclxuICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdkZWNpbWFsQWRkZWQtJyArIHRoaXMuZGVjaW1hbENvdW50ZXIsIHRydWUpO1xyXG4gICAgICAgICAgdGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0ID0ge1xyXG4gICAgICAgICAgICB0eXBlOiB0aGlzLmdldERlY2ltYWxUeXBlKCksXHJcbiAgICAgICAgICAgIHByZWNpc2lvbjogdGhpcy5kZWNpbWFsQ291bnRlclxyXG4gICAgICAgICAgfTtcclxuICAgICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcm1hdE5vdEFsbG93ZWQgPSB0cnVlO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdyZW1vdmVEZWNpbWFsJzpcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YVR5cGUgJiYgdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YVR5cGUuaW5kZXhPZignbnVtYmVyJykgPiAtMSkge1xyXG4gICAgICAgICAgaWYgKHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlY2ltYWxDb3VudGVyID0gKHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCBhcyBEZXZFeHRyZW1lRGF0YVR5cGVGb3JtYXQpLnByZWNpc2lvbiB8fCAwO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdkZWNpbWFsQWRkZWQtJyArIHRoaXMuZGVjaW1hbENvdW50ZXIsIGZhbHNlKTtcclxuICAgICAgICAgIGlmICh0aGlzLmRlY2ltYWxDb3VudGVyID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlY2ltYWxDb3VudGVyLS07XHJcbiAgICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdkZWNpbWFsQWRkZWQtJyArIHRoaXMuZGVjaW1hbENvdW50ZXIsIHRydWUpO1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQgPSB7XHJcbiAgICAgICAgICAgICAgdHlwZTogdGhpcy5nZXREZWNpbWFsVHlwZSgpLFxyXG4gICAgICAgICAgICAgIHByZWNpc2lvbjogdGhpcy5kZWNpbWFsQ291bnRlclxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmRlY2ltYWxDb3VudGVyID09PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsICdkZWNpbWFsQWRkZWQtJyArIHRoaXMuZGVjaW1hbENvdW50ZXIsIGZhbHNlKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcm1hdE5vdEFsbG93ZWQgPSB0cnVlO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdwZXJjZW50YWdlJzpcclxuICAgICAgICB0aGlzLnJlc2V0U2VsZWN0ZWRGb3JtYXQoYXJncy50eXBlKTtcclxuICAgICAgICBpZiAodGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YVR5cGUgJiYgdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YVR5cGUuaW5kZXhPZignbnVtYmVyJykgPiAtMSkge1xyXG4gICAgICAgICAgdGhpcy5wcmVwYXJlQ29sdW1uRm9ybWF0VXNpbmdDbGFzc05hbWUoJ2Nzc0NsYXNzJywgJ3BlcmNlbnRhZ2UnLCBhcmdzLmlzU2VsZWN0ZWQpO1xyXG4gICAgICAgICAgdGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0ID0gYXJncy5pc1NlbGVjdGVkID8geyB0eXBlOiAncGVyY2VudCcsIHByZWNpc2lvbjogMiB9IDogJyc7XHJcbiAgICAgICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIXRoaXMuZW5hYmxlSW5wdXRDYXB0aW9uRmllbGQpIHtcclxuICAgICAgICAgIHRoaXMuY29sdW1uRm9ybWF0Q2hhbmdlRm9yRGF0YURpY3Rpb25hcnkoYXJncywgJ3BlcmNlbnQnLCAncGVyY2VudGFnZScsIDIpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvcm1hdE5vdEFsbG93ZWQgPSB0cnVlO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdjb21tYSc6XHJcbiAgICAgICAgdGhpcy5yZXNldFNlbGVjdGVkRm9ybWF0KGFyZ3MudHlwZSk7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlICYmIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhLmRhdGFUeXBlLmluZGV4T2YoJ251bWJlcicpID4gLTEpIHtcclxuICAgICAgICAgIHRoaXMuZGVjaW1hbENvdW50ZXIgPSAwO1xyXG4gICAgICAgICAgdGhpcy5wcmVwYXJlQ29sdW1uRm9ybWF0VXNpbmdDbGFzc05hbWUoJ2Nzc0NsYXNzJywgJ2NvbW1hJywgYXJncy5pc1NlbGVjdGVkKTtcclxuICAgICAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCA9IGFyZ3MuaXNTZWxlY3RlZCA/IHsgdHlwZTogJ2NvbW1hJywgcHJlY2lzaW9uOiAwIH0gOiAnJztcclxuICAgICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5lbmFibGVJbnB1dENhcHRpb25GaWVsZCkge1xyXG4gICAgICAgICAgdGhpcy5jb2x1bW5Gb3JtYXRDaGFuZ2VGb3JEYXRhRGljdGlvbmFyeShhcmdzLCAnY29tbWEnLCAnY29tbWEnLCAwKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtYXROb3RBbGxvd2VkID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSAnY2FsZW5kYXInOlxyXG4gICAgICAgIHRoaXMucmVzZXRTZWxlY3RlZEZvcm1hdChhcmdzLnR5cGUpO1xyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhVHlwZS5pbmRleE9mKCdkYXRlJykgPiAtMSB8fCAhdGhpcy5lbmFibGVJbnB1dERhdGFGaWVsZENoZWNrKSB7XHJcbiAgICAgICAgICB0aGlzLnByZXBhcmVDb2x1bW5Gb3JtYXRVc2luZ0NsYXNzTmFtZSgnY3NzQ2xhc3MnLCAnY2FsZW5kYXInLCBhcmdzLmlzU2VsZWN0ZWQpO1xyXG4gICAgICAgICAgdGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0ID0gYXJncy5pc1NlbGVjdGVkID8gJ3Nob3J0RGF0ZScgOiAnJztcclxuICAgICAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5lbmFibGVJbnB1dENhcHRpb25GaWVsZCkge1xyXG4gICAgICAgICAgdGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0ID0gYXJncy5pc1NlbGVjdGVkID8gJ3Nob3J0RGF0ZScgOiAnJztcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmb3JtYXROb3RBbGxvd2VkID0gdHJ1ZTtcclxuICAgICAgICBicmVhaztcclxuICAgIH1cclxuICAgIGlmICghdGhpcy5lbmFibGVJbnB1dENhcHRpb25GaWVsZCkge1xyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubG9hZGVyJykuY2xhc3NMaXN0LmFkZCgnZGlzcGxheS1ub25lJyk7XHJcbiAgICAgIH0sIDApO1xyXG4gICAgfVxyXG4gICAgaWYgKGZvcm1hdE5vdEFsbG93ZWQpIHtcclxuICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnd2FybmluZycsIG1lc3NhZ2U6ICdGb3JtYXQgY2FuXFwndCBiZSBhcHBsaWVkIG9uIHRoZSBzZWxlY3RlZCBjb2x1bW4uJyB9KTtcclxuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxvYWRlcicpLmNsYXNzTGlzdC5hZGQoJ2Rpc3BsYXktbm9uZScpO1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgc2V0QWxpZ25tZW50Rm9ybWF0KGFyZ3M6IEZvcm1hdE9wdGlvbkRhdGFUeXBlLCBhbGlnbm1lbnRUeXBlOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMucmVzZXRBbGlnbm1lbnRFeGNlcHRGb3JDdXJyZW50KGFyZ3MudHlwZSk7XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdC5hbGlnbm1lbnQgPSBhcmdzLmlzU2VsZWN0ZWQgPyBhbGlnbm1lbnRUeXBlIDogJyc7XHJcbiAgICBpZiAodGhpcy5lbmFibGVJbnB1dENhcHRpb25GaWVsZCkge1xyXG4gICAgICB0aGlzLm9uRm9ybWF0T3B0aW9uc0NoYW5nZWQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgc2V0UGluUG9zaXRpb24oYXJnczogRm9ybWF0T3B0aW9uRGF0YVR5cGUsIHBvc2l0aW9uOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMucmVzZXRQaW5FeGNlcHRGb3JDdXJyZW50KGFyZ3MudHlwZSk7XHJcbiAgICB0aGlzLmZvcm1hdE9iamVjdC5maXhlZCA9IGFyZ3MuaXNTZWxlY3RlZDtcclxuICAgIHRoaXMuZm9ybWF0T2JqZWN0LmZpeGVkUG9zaXRpb24gPSBhcmdzLmlzU2VsZWN0ZWQgPyBwb3NpdGlvbiA6ICcnO1xyXG4gICAgaWYgKHRoaXMuZ3JpZEluc3RhbmNlTGlzdC5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5ncmlkSW5zdGFuY2VMaXN0LmZvckVhY2goZ3JpZCA9PiB7XHJcbiAgICAgICAgZ3JpZC5jb2x1bW5PcHRpb24odGhpcy5zZWxlY3RlZENvbHVtbkRhdGEuZGF0YUZpZWxkLFxyXG4gICAgICAgICAgeyBmaXhlZDogYXJncy5pc1NlbGVjdGVkLCBmaXhlZFBvc2l0aW9uOiB0aGlzLmZvcm1hdE9iamVjdC5maXhlZFBvc2l0aW9uIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmVuYWJsZUlucHV0Q2FwdGlvbkZpZWxkKSB7XHJcbiAgICAgIHRoaXMub25Gb3JtYXRPcHRpb25zQ2hhbmdlZCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBjb2x1bW5Gb3JtYXRDaGFuZ2VGb3JEYXRhRGljdGlvbmFyeShhcmdzOiBGb3JtYXRPcHRpb25EYXRhVHlwZSwgdHlwZTogc3RyaW5nLCBmb3JtYXRDbGFzczogc3RyaW5nLCBwcmVjaXNpb24/OiBudW1iZXIpOiB2b2lkIHtcclxuICAgIHRoaXMucHJlcGFyZUNvbHVtbkZvcm1hdFVzaW5nQ2xhc3NOYW1lKCdjc3NDbGFzcycsIGZvcm1hdENsYXNzLCBhcmdzLmlzU2VsZWN0ZWQpO1xyXG4gICAgdGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0ID0gYXJncy5pc1NlbGVjdGVkID8geyB0eXBlLCBwcmVjaXNpb24gfSA6ICcnO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZXNldEFsaWdubWVudEV4Y2VwdEZvckN1cnJlbnQodHlwZSA9ICcnKTogdm9pZCB7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmFsaWdubWVudC5sZW5ndGg7IGkrKykge1xyXG4gICAgICBpZiAodGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuYWxpZ25tZW50W2ldLnR5cGUgIT09IHR5cGUpIHtcclxuICAgICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5hbGlnbm1lbnRbaV0uaXNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHJlc2V0UGluRXhjZXB0Rm9yQ3VycmVudCh0eXBlID0gJycpOiB2b2lkIHtcclxuICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbi5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICBpZiAoaXRlbS50eXBlICE9PSB0eXBlKSB7XHJcbiAgICAgICAgaXRlbS5pc1NlbGVjdGVkID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwcmVwYXJlQ29sdW1uRm9ybWF0VXNpbmdDbGFzc05hbWUoZm9ybWF0VHlwZTogc3RyaW5nLCBmb3JtYXRWYWx1ZTogc3RyaW5nLCBhZGRGb3JtYXQ6IGJvb2xlYW4pIHtcclxuICAgIGxldCBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMgPSB0aGlzLmZvcm1hdE9iamVjdFtmb3JtYXRUeXBlXSB8fCAnJztcclxuICAgIGlmIChhZGRGb3JtYXQpIHtcclxuICAgICAgaWYgKGxpc3RPZkV4aXN0aW5nRm9ybWF0cyAmJiBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMubGVuZ3RoKSB7XHJcbiAgICAgICAgY29uc3QgZm9ybWF0R3JvdXBEYXRhID0gWydjdXJyZW5jeScsICdwZXJjZW50YWdlJywgJ2NvbW1hJywgJ2NhbGVuZGFyJ107XHJcbiAgICAgICAgaWYgKGZvcm1hdEdyb3VwRGF0YS5pbmRleE9mKGZvcm1hdFZhbHVlKSA+IC0xKSB7XHJcbiAgICAgICAgICBmb3IgKGxldCBmb3JtYXRJbmRleCA9IDA7IGZvcm1hdEluZGV4IDwgZm9ybWF0R3JvdXBEYXRhLmxlbmd0aDsgZm9ybWF0SW5kZXgrKykge1xyXG4gICAgICAgICAgICBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMgPSBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHMucmVwbGFjZShmb3JtYXRHcm91cERhdGFbZm9ybWF0SW5kZXhdLCAnJyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxpc3RPZkV4aXN0aW5nRm9ybWF0cyA9IGxpc3RPZkV4aXN0aW5nRm9ybWF0cy5yZXBsYWNlKG5ldyBSZWdFeHAoZm9ybWF0VmFsdWUsICdnJyksICcnKS50cmltKCk7XHJcbiAgICAgICAgbGlzdE9mRXhpc3RpbmdGb3JtYXRzID0gbGlzdE9mRXhpc3RpbmdGb3JtYXRzICsgJyAnICsgZm9ybWF0VmFsdWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGlzdE9mRXhpc3RpbmdGb3JtYXRzID0gZm9ybWF0VmFsdWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAobGlzdE9mRXhpc3RpbmdGb3JtYXRzICYmIGxpc3RPZkV4aXN0aW5nRm9ybWF0cy5pbmRleE9mKGZvcm1hdFZhbHVlKSA+IC0xKSB7XHJcbiAgICAgIGxpc3RPZkV4aXN0aW5nRm9ybWF0cyA9IGxpc3RPZkV4aXN0aW5nRm9ybWF0cy5yZXBsYWNlKG5ldyBSZWdFeHAoZm9ybWF0VmFsdWUsICdnJyksICcnKS50cmltKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5mb3JtYXRPYmplY3RbZm9ybWF0VHlwZV0gPSBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHM7XHJcbiAgICBjb25zdCBfZGF0YSA9IHt9O1xyXG4gICAgaWYgKGZvcm1hdFR5cGUgPT09ICdjc3NDbGFzcycpIHtcclxuICAgICAgX2RhdGFbZm9ybWF0VHlwZV0gPSBsaXN0T2ZFeGlzdGluZ0Zvcm1hdHM7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBfZGF0YVtmb3JtYXRUeXBlXSA9IGFkZEZvcm1hdCA/IGZvcm1hdFZhbHVlIDogbGlzdE9mRXhpc3RpbmdGb3JtYXRzO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIF9kYXRhO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZXNldFNlbGVjdGVkRm9ybWF0KHR5cGUgPSAnJyk6IHZvaWQge1xyXG4gICAgbGV0IGxpc3RPZkNsYXNzZXMgPSB0aGlzLmZvcm1hdE9iamVjdC5jc3NDbGFzcztcclxuICAgIGlmIChsaXN0T2ZDbGFzc2VzKSB7XHJcbiAgICAgIGxpc3RPZkNsYXNzZXMgPSBsaXN0T2ZDbGFzc2VzLnJlcGxhY2UoL3BlcmNlbnRhZ2UvZywgJycpLnJlcGxhY2UoL2NvbW1hL2csICcnKS5yZXBsYWNlKC9jdXJyZW5jeS9nLCAnJyk7XHJcbiAgICAgIHRoaXMuZm9ybWF0T2JqZWN0LmNzc0NsYXNzID0gbGlzdE9mQ2xhc3NlcztcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvcm1hdC5sZW5ndGg7IGkrKykge1xyXG4gICAgICBpZiAodGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9ybWF0W2ldLnR5cGUgIT09IHR5cGUpIHtcclxuICAgICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5mb3JtYXRbaV0uaXNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHJlc2V0QmFzaWNTZXR0aW5ncygpOiB2b2lkIHtcclxuICAgIGxldCBpOiBudW1iZXI7XHJcbiAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9ybWF0Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvcm1hdFtpXS5pc1NlbGVjdGVkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChpID0gMDsgaSA8IHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmFsaWdubWVudC5sZW5ndGg7IGkrKykge1xyXG4gICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5hbGlnbm1lbnRbaV0uaXNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAoaSA9IDA7IGkgPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW4ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEucGluW2ldLmlzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9udFN0eWxlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvbnRTdHlsZVtpXS5pc1NlbGVjdGVkID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICB0aGlzLnRleHRDb2xvciA9ICcnO1xyXG4gICAgdGhpcy5iYWNrZ3JvdW5kQ29sb3IgPSAnJztcclxuICAgIHRoaXMuZm9ybWF0T2JqZWN0ID0ge307XHJcbiAgICB0aGlzLmRlY2ltYWxDb3VudGVyID0gMDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcHJlcGFyZVNlbGVjdGVkT3B0aW9uKCk6IHZvaWQge1xyXG4gICAgdGhpcy5yZXNldEJhc2ljU2V0dGluZ3MoKTtcclxuICAgIGxldCBzZWxlY3RlZENvbHVtbkRlZjogQ29sdW1uRm9ybWF0ID0gbmV3IE9iamVjdCgpIGFzIENvbHVtbkZvcm1hdDtcclxuICAgIGlmICh0aGlzLmVuYWJsZVN0b3JhZ2UpIHtcclxuICAgICAgY29uc3QgY29sdW1uRm9ybWF0Q29sbGVjdGlvbjogQ29sdW1uRm9ybWF0W10gPSB0aGlzLmdldE9yU2V0U2Vzc2lvbkRhdGEodGhpcy5zdG9yYWdlS2V5LCAnZ2V0Jyk7XHJcbiAgICAgIGNvbnN0IGJhc2VGb3JtYXRDb2xsZWN0aW9uOiBDb2x1bW5Gb3JtYXRbXSA9IHRoaXMuaXNNYXN0ZXJHcmlkU2VsZWN0ZWQgP1xyXG4gICAgICAgIEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5kaWN0aW9uYXJ5Rm9ybWF0RGF0YV0pKSB8fCBbXSA6IFtdO1xyXG4gICAgICBzZWxlY3RlZENvbHVtbkRlZiA9IGNvbHVtbkZvcm1hdENvbGxlY3Rpb24uZmluZChhcnJJdGVtID0+IGFyckl0ZW0uZGF0YUZpZWxkID09PSB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQpIHx8XHJcbiAgICAgICAgYmFzZUZvcm1hdENvbGxlY3Rpb24uZmluZChhcnJJdGVtID0+IGFyckl0ZW0uZGF0YUZpZWxkID09PSB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5kYXRhRmllbGQpIHx8IHt9O1xyXG4gICAgfVxyXG4gICAgaWYgKCFPYmplY3Qua2V5cyhzZWxlY3RlZENvbHVtbkRlZikubGVuZ3RoICYmIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhKSB7XHJcbiAgICAgIHNlbGVjdGVkQ29sdW1uRGVmID0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEgYXMgYW55O1xyXG4gICAgfVxyXG4gICAgaWYgKHNlbGVjdGVkQ29sdW1uRGVmKSB7XHJcbiAgICAgIGlmIChzZWxlY3RlZENvbHVtbkRlZi5jc3NDbGFzcykge1xyXG4gICAgICAgIGNvbnN0IGNsYXNzTGlzdCA9IHNlbGVjdGVkQ29sdW1uRGVmLmNzc0NsYXNzLnNwbGl0KCcgJyk7XHJcbiAgICAgICAgbGV0IGk6IG51bWJlcjtcclxuICAgICAgICBjbGFzc0xpc3QuZm9yRWFjaCgoY3NzQ2xhc3M6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgaWYgKGNzc0NsYXNzID09PSAnYm9sZCcgfHwgY3NzQ2xhc3MgPT09ICd1bmRlcmxpbmUnIHx8IGNzc0NsYXNzID09PSAnaXRhbGljJykge1xyXG4gICAgICAgICAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9udFN0eWxlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLmZvbnRTdHlsZVtpXS50eXBlID09PSBjc3NDbGFzcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9udFN0eWxlW2ldLmlzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtYXRPYmplY3QuY3NzQ2xhc3MgPSB0aGlzLmZvcm1hdE9iamVjdC5jc3NDbGFzcyA/IHRoaXMuZm9ybWF0T2JqZWN0LmNzc0NsYXNzICsgJyAnICsgY3NzQ2xhc3MgOiAnICcgKyBjc3NDbGFzcztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGlmIChjc3NDbGFzcyA9PT0gJ2N1cnJlbmN5JyB8fCBjc3NDbGFzcyA9PT0gJ3BlcmNlbnRhZ2UnIHx8IGNzc0NsYXNzID09PSAnY29tbWEnIHx8IGNzc0NsYXNzID09PSAnY2FsZW5kYXInKSB7XHJcbiAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5mb3JtYXQubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICBpZiAodGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuZm9ybWF0W2ldLnR5cGUgPT09IGNzc0NsYXNzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5mb3JtYXRbaV0uaXNTZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRlY2ltYWxDb3VudGVyID0gc2VsZWN0ZWRDb2x1bW5EZWYuZm9ybWF0ID8gKHNlbGVjdGVkQ29sdW1uRGVmLmZvcm1hdCBhcyBEZXZFeHRyZW1lRGF0YVR5cGVGb3JtYXQpLnByZWNpc2lvbiA6IDA7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZvcm1hdE9iamVjdC5jc3NDbGFzcyA9IHRoaXMuZm9ybWF0T2JqZWN0LmNzc0NsYXNzID8gdGhpcy5mb3JtYXRPYmplY3QuY3NzQ2xhc3MgKyAnICcgKyBjc3NDbGFzcyA6ICcgJyArIGNzc0NsYXNzO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgaWYgKGNzc0NsYXNzLmluZGV4T2YoJ2RlY2ltYWwnKSA+IC0xKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVjaW1hbENvdW50ZXIgPSAoc2VsZWN0ZWRDb2x1bW5EZWYuZm9ybWF0IGFzIERldkV4dHJlbWVEYXRhVHlwZUZvcm1hdCkucHJlY2lzaW9uO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoc2VsZWN0ZWRDb2x1bW5EZWYuZml4ZWQpIHtcclxuICAgICAgICBjb25zdCBpc1Bvc2l0aW9uTGVmdCA9IHNlbGVjdGVkQ29sdW1uRGVmLmZpeGVkUG9zaXRpb24gPT09ICdsZWZ0JyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICBmb3IgKGxldCBpbmQgPSAwOyBpbmQgPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW4ubGVuZ3RoOyBpbmQrKykge1xyXG4gICAgICAgICAgaWYgKGlzUG9zaXRpb25MZWZ0ICYmIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbltpbmRdLmtleSA9PT0gJ2xlZnRQaW4nKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbltpbmRdLmlzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEucGluW2luZF0ua2V5ID09PSAncmlnaHRQaW4nKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbltpbmRdLmlzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGxldCBqOiBudW1iZXI7XHJcbiAgICAgIGZvciAoaiA9IDA7IGogPCB0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5hbGlnbm1lbnQubGVuZ3RoOyBqKyspIHtcclxuICAgICAgICBpZiAodGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuYWxpZ25tZW50W2pdLnR5cGUgPT09IHNlbGVjdGVkQ29sdW1uRGVmLmFsaWdubWVudCkge1xyXG4gICAgICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEuYWxpZ25tZW50W2pdLmlzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgZm9yIChqID0gMDsgaiA8IHRoaXMuYmFzaWNGb3JtYXRJY29uc0FuZE9wdGlvbnNEYXRhLnBpbi5sZW5ndGg7IGorKykge1xyXG4gICAgICAgIGlmICh0aGlzLmJhc2ljRm9ybWF0SWNvbnNBbmRPcHRpb25zRGF0YS5waW5bal0udHlwZSA9PT0gc2VsZWN0ZWRDb2x1bW5EZWYuZml4ZWRQb3NpdGlvbikge1xyXG4gICAgICAgICAgdGhpcy5iYXNpY0Zvcm1hdEljb25zQW5kT3B0aW9uc0RhdGEucGluW2pdLmlzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICB0aGlzLnRleHRDb2xvciA9IHNlbGVjdGVkQ29sdW1uRGVmLnRleHRDb2xvciB8fCAnJztcclxuICAgICAgdGhpcy5iYWNrZ3JvdW5kQ29sb3IgPSBzZWxlY3RlZENvbHVtbkRlZi5iYWNrZ3JvdW5kQ29sb3IgfHwgJyc7XHJcbiAgICAgIHRoaXMuZm9ybWF0T2JqZWN0ID0gc2VsZWN0ZWRDb2x1bW5EZWY7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5pc0NvbG9yUmVhZE9ubHkgPSAhdGhpcy5zZWxlY3RlZENvbHVtbkRhdGE7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHJlbW92ZVByZWNpc2lvbkNvbnRyb2xzKGRhdGEpIHtcclxuICAgIGNvbnN0IGNvbHVtbkZvcm1hdERhdGEgPSBPYmplY3QuYXNzaWduKHt9LCBkYXRhKTtcclxuICAgIGNvbnN0IGZvcm1hdERhdGEgPSBjb2x1bW5Gb3JtYXREYXRhLmZvcm1hdC5maWx0ZXIoKGl0ZW0pID0+IHtcclxuICAgICAgaWYgKGl0ZW0ua2V5ICE9PSAncmVtb3ZlRGVjaW1hbCcgJiYgaXRlbS5rZXkgIT09ICdhZGREZWNpbWFsJykge1xyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGNvbHVtbkZvcm1hdERhdGEuZm9ybWF0ID0gZm9ybWF0RGF0YTtcclxuICAgIHJldHVybiBjb2x1bW5Gb3JtYXREYXRhO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXREZWNpbWFsVHlwZSgpOiBzdHJpbmcge1xyXG4gICAgbGV0IGRlY2ltYWxGb3JtYXRUeXBlID0gJ2ZpeGVkUG9pbnQnO1xyXG4gICAgaWYgKHRoaXMuZm9ybWF0T2JqZWN0LmZvcm1hdCkge1xyXG4gICAgICBkZWNpbWFsRm9ybWF0VHlwZSA9IHRoaXMuZGVjaW1hbFR5cGVzLnNvbWUodCA9PiB0ID09PSAodGhpcy5mb3JtYXRPYmplY3QuZm9ybWF0IGFzIERldkV4dHJlbWVEYXRhVHlwZUZvcm1hdCkudHlwZSlcclxuICAgICAgICA/ICh0aGlzLmZvcm1hdE9iamVjdC5mb3JtYXQgYXMgRGV2RXh0cmVtZURhdGFUeXBlRm9ybWF0KS50eXBlIDogJ2ZpeGVkUG9pbnQnO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGRlY2ltYWxGb3JtYXRUeXBlO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRPclNldFNlc3Npb25EYXRhKGtleTogc3RyaW5nLCB0eXBlOiBzdHJpbmcsIGRhdGEgPSBudWxsKTogQXJyYXk8YW55PiB7XHJcbiAgICBpZiAoIXRoaXMuZW5hYmxlU3RvcmFnZSkge1xyXG4gICAgICByZXR1cm4gW107XHJcbiAgICB9XHJcbiAgICBrZXkgPSB0aGlzLmdyaWRJbnN0YW5jZUxpc3QubGVuZ3RoID8gdGhpcy51dGlsaXR5U2VydmljZS5nZXRTdG9yYWdlS2V5KHRoaXMuZ3JpZEluc3RhbmNlTGlzdFswXSwga2V5LCB0aGlzLmlzTWFzdGVyR3JpZFNlbGVjdGVkKSA6IGtleTtcclxuICAgIHN3aXRjaCAodHlwZSkge1xyXG4gICAgICBjYXNlICdnZXQnOlxyXG4gICAgICAgIHJldHVybiBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oa2V5KSkgfHwgW107XHJcbiAgICAgIGNhc2UgJ3NldCc6XHJcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShrZXksIEpTT04uc3RyaW5naWZ5KGRhdGEpKTtcclxuICAgICAgICBicmVhaztcclxuICAgIH1cclxuICAgIHJldHVybiBbXTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcmVzZXRBbGxGb3JtYXRPcHRpb25zKCk6IHZvaWQge1xyXG4gICAgdGhpcy5yZXNldEJhc2ljU2V0dGluZ3MoKTtcclxuICAgIHRoaXMucmVzZXRQaW5FeGNlcHRGb3JDdXJyZW50KCk7XHJcbiAgICB0aGlzLnJlc2V0U2VsZWN0ZWRGb3JtYXQoKTtcclxuICAgIHRoaXMucmVzZXRBbGlnbm1lbnRFeGNlcHRGb3JDdXJyZW50KCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHByZXBhcmVMb2FkZXJMb2NhdGlvbihldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgY29uc3QgY29yZCA9IChldmVudC5jdXJyZW50VGFyZ2V0IGFzIEhUTUxFbGVtZW50KS5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgIGNvbnN0IGxlZnRDb3JkcyA9IGNvcmQubGVmdCArIDQgKyAncHgnO1xyXG4gICAgY29uc3QgdG9wQ29yZHMgPSBjb3JkLnRvcCArIDQgKyAncHgnO1xyXG4gICAgY29uc3QgZmlyc3RDaGlsZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5sb2FkZXInKTtcclxuICAgIGZpcnN0Q2hpbGQuY2xhc3NMaXN0LnJlbW92ZSgnZGlzcGxheS1ub25lJyk7XHJcbiAgICAoZmlyc3RDaGlsZCBhcyBIVE1MRWxlbWVudCkuc3R5bGUudG9wID0gdG9wQ29yZHM7XHJcbiAgICAoZmlyc3RDaGlsZCBhcyBIVE1MRWxlbWVudCkuc3R5bGUubGVmdCA9IGxlZnRDb3JkcztcclxuICB9XHJcblxyXG4gIC8vI2VuZHJlZ2lvbiBQcml2YXRlIE1ldGhvZHNcclxufVxyXG4iXX0=