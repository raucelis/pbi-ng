import { __decorate } from "tslib";
import { CGFFlyOutEnum, CGFEventsEnum, CGFStorageKeys, CGFFeatureUpdateEnum } from '../../utilities/enums';
import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { appToast, convertDateToUsFormat, getStorageKey } from '../../utilities/utilityFunctions';
import { cgfFlyOutList } from '../../utilities/constants';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FlyOutActionIconContainerComponent } from '../fly-out-action-icon-container/fly-out-action-icon-container.component';
let CommonGridFrameworkComponent = class CommonGridFrameworkComponent {
    //#endregion Data Members
    constructor(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.securityIdColumnName = '';
        this.isEditSecurity = false;
        this.showEntityParameter = false;
        this.isCached = false;
        this.cgfEvent = new EventEmitter();
        this.loadChildEntityData = new EventEmitter();
        this.selectionChanged = new EventEmitter();
        //#region Data Members
        this.cachedColumns = [];
        this.cachingEnabled = false;
        this.conditionalFormattingStorageKey = CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
        this.currentSelectedFlyOut = null;
        this.disableRefresh = true;
        this.flyOutEnums = CGFFlyOutEnum; // this variable is for HTML mapping
        this.flyOutTitle = '';
        this.gridComponentInstanceList = [];
        this.resetSettings = false;
        this.selectedColumn = null;
        this.selectedEntityInfo = { columnList: [], gridName: '' };
        this.selectedGridInstance = new Object();
        this.selectedGridOptions = new Object();
        this.CGFStorageKeyCollection = [
            CGFStorageKeys[CGFStorageKeys.conditionalFormatting],
            CGFStorageKeys[CGFStorageKeys.dictionaryFormatData],
            CGFStorageKeys[CGFStorageKeys.formatData],
            CGFStorageKeys[CGFStorageKeys.childGridView]
        ];
    }
    set currentFlyOut(flyOut) {
        if (flyOut) {
            const item = cgfFlyOutList.find(f => f.id === flyOut);
            this.onFlyOutSelectionChange({ item });
        }
    }
    set cgfFeatureUpdate(option) {
        switch (option) {
            case CGFFeatureUpdateEnum.customColumnUpdated:
                this.handleCellClick(this.selectedGridOptions, null);
                break;
            case CGFFeatureUpdateEnum.cachedData:
                this.updateCachedDataSource();
                break;
        }
    }
    get containsVisibleParameters() {
        const visibleParams = this.options.dataBrowserEntityParameters.filter(p => !p.HideInDataBrowser);
        return visibleParams.length > 0;
    }
    ngOnInit() {
        var _a;
        if ((_a = this.options) === null || _a === void 0 ? void 0 : _a.dataBrowserEntityParameters.length) {
            this.options.dataBrowserEntityParameters.map(val => {
                if (val.DefaultSingleValue) {
                    if (val.DataType.toLowerCase() === 'boolean') {
                        val.Value = isNaN(val.DefaultSingleValue) ? val.DefaultSingleValue.toLowerCase() === 'true' : Number(val.DefaultSingleValue) === 1;
                    }
                    if (val.DataType.toLowerCase() === 'datetime') {
                        val.Value = val.DefaultSingleValue ? convertDateToUsFormat(val.DefaultSingleValue) : '';
                    }
                    else {
                        val.Value = val.DefaultSingleValue;
                    }
                }
                else if (val.DataType.toLowerCase() === 'boolean' && !val.Value) {
                    val.Value = false;
                }
                else if (val.Type.toLowerCase() === 'sqllist' || val.Type.toLowerCase() === 'list') {
                    val.Value = val.DefaultValue;
                }
            });
        }
    }
    onGridInitialized() {
        this.tryToApplyInitialViews();
    }
    onFlyOutSelectionChange(args) {
        var _a, _b;
        if (!this.selectedGridInstance || !Object.keys(this.selectedGridInstance).length) {
            this.selectedGridInstance = this.options.gridOptions.gridComponentInstance;
            this.selectedGridOptions = this.options.gridOptions;
        }
        if (typeof args.item === 'undefined' && args.showEntityParameter) {
            this.showEntityParameter = args.showEntityParameter;
            this.closeCurrentFlyOut();
        }
        else {
            this.showEntityParameter = false;
            if (((_a = args.item) === null || _a === void 0 ? void 0 : _a.id) === ((_b = this.currentSelectedFlyOut) === null || _b === void 0 ? void 0 : _b.id)) {
                this.closeCurrentFlyOut();
            }
            else {
                this.currentSelectedFlyOut = args.item || null;
            }
        }
        this.setGridInstanceList(this.selectedGridOptions);
    }
    viewEvents(args) {
        if (this.options.viewDataSource) {
            switch (args.event) {
                case CGFEventsEnum.applyView:
                    this.options.gridOptions.applyViewToGrid(args.data);
                    break;
                case CGFEventsEnum.updateLayouts:
                    this.options.viewList = args.data;
                default:
                    break;
            }
        }
        else {
            this.cgfEvent.emit(args);
        }
    }
    customColumnEvent(args) {
        this.cgfEvent.emit(args);
    }
    entityParamEvent(args) {
        this.cgfEvent.emit(args);
    }
    onFocusoutEntityParam(args) {
        this.disableRefresh = args;
    }
    cellClickEvent(cellClickObject) {
        this.handleCellClick(this.options.gridOptions, cellClickObject);
        this.selectionChanged.emit(cellClickObject);
    }
    gridChanged(event) {
        if (event) {
            const currentFlyOutId = this.currentSelectedFlyOut.id;
            this.currentSelectedFlyOut = { id: CGFFlyOutEnum.newTab };
            setTimeout(() => { this.currentSelectedFlyOut.id = currentFlyOutId; }, 0);
            this.selectedColumn = null;
        }
        else {
            if (this.selectedGridOptions && Object.keys(this.selectedGridOptions).length) {
                this.setGridInstanceList(this.selectedGridOptions);
            }
        }
    }
    onGridOptionChange(args) {
        var _a, _b;
        switch (args.key) {
            case 'gridLines':
                this.options.gridOptions.showColumnLines = args.value;
                if ((_a = this.options.gridOptions.childEntityList[0]) === null || _a === void 0 ? void 0 : _a.gridOptions) {
                    this.options.gridOptions.childEntityList[0].gridOptions.showColumnLines = args.value;
                }
                break;
            case 'theme':
                this.options.gridOptions.selectedTheme = args.value;
                // this.options.gridOptions.selectedThemeClass = getClassNameByThemeName(this.options.gridOptions.selectedTheme);
                if ((_b = this.options.gridOptions.childEntityList[0]) === null || _b === void 0 ? void 0 : _b.gridOptions) {
                    this.options.gridOptions.childEntityList[0].gridOptions.selectedTheme = args.value;
                }
                break;
        }
    }
    customButtonClick(args) {
        switch (args.btnType) {
            case 'edit':
                const visibleCols = this.options.gridOptions.gridComponentInstance.getVisibleColumns();
                if (visibleCols && visibleCols.length) {
                    const cols = visibleCols.map(data => data.dataField).filter(col => {
                        return col && (col.toLowerCase() === 'securityid' || col.toLowerCase() === 'security id');
                    });
                    if (cols && cols.length) {
                        this.cgfEvent.emit({ event: CGFEventsEnum.editSecurity, data: args.rowData });
                    }
                    else {
                        appToast({ type: 'error', message: 'SecurityId is a required field. Please select from column list.' });
                    }
                    break;
                }
                break;
            case 'add':
                this.cgfEvent.emit({ event: CGFEventsEnum.editSecurity, data: {} });
                break;
        }
    }
    applyButtonClick(args) {
        this.closeCurrentFlyOut();
        this.cachedColumns = args;
        this.cgfEvent.emit({ event: CGFEventsEnum.getCachedData, data: args });
    }
    onRefreshEntityData() {
        this.closeCurrentFlyOut();
        this.cgfEvent.emit({ event: CGFEventsEnum.refreshEntityData });
    }
    saveOrderOfParameters() {
        this.cgfEvent.emit({ event: CGFEventsEnum.saveParameterOrder });
    }
    changeInValueOfConditionalFormatting() {
        this.options.gridOptions.gridComponentInstance.repaint();
    }
    getChildEntityData(event) {
        var _a;
        if (this.isCached) {
            const primaryKeyColName = this.options.gridOptions.childEntityList[0].primaryKeyColumn;
            const isPrimaryColumnExists = (_a = this.options.gridOptions.gridComponentInstance.getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.filter(col => col.dataField === primaryKeyColName);
            if (!isPrimaryColumnExists.length) {
                appToast({
                    type: 'error', message: `Following column(s) ${primaryKeyColName} is required. Please make sure same exists with value.`,
                    displayTime: 4000
                });
                return;
            }
        }
        this.loadChildEntityData.emit(event);
    }
    childEntityCellClick(args) {
        this.options.gridOptions.childEntityList.forEach(grid => {
            if (grid.gridOptions.gridName === args.childGridOptions.gridName) {
                this.selectedGridOptions = grid.gridOptions;
            }
        });
        this.handleCellClick(this.selectedGridOptions, args.cellObject);
    }
    resetGridFormattingInfo() {
        let sessionCount = sessionStorage.length;
        while (sessionCount--) {
            const key = sessionStorage.key(sessionCount);
            if (this.CGFStorageKeyCollection.findIndex(item => key.indexOf(item) > -1 ? true : false) > -1) {
                sessionStorage.removeItem(key);
            }
        }
        sessionStorage.removeItem('theme');
        this.gridComponentInstanceList.forEach(item => item.gridComponentInstance.state({}));
    }
    closeCurrentFlyOut() {
        this.currentSelectedFlyOut = null;
        // this.resetSettings = true;
    }
    updateParameterSectionVisibility() {
        var _a;
        if (this.appCGFContainer) {
            (_a = this.appCGFContainer) === null || _a === void 0 ? void 0 : _a.showParametersControlsContainer();
        }
        else {
            this.onFlyOutSelectionClick({ showEntityParameter: !this.showEntityParameter });
        }
    }
    onFlyOutSelectionClick(args) {
        this.resetSettings = false;
        if (!this.selectedGridInstance || !Object.keys(this.selectedGridInstance).length) {
            this.selectedGridInstance = this.options.gridOptions.gridComponentInstance;
            this.selectedGridOptions = this.options.gridOptions;
        }
        if (typeof args.item === 'undefined' && args.showEntityParameter) {
            this.showEntityParameter = args.showEntityParameter;
            this.currentSelectedFlyOut = new Object();
        }
        else {
            this.showEntityParameter = false;
            if (args.item && args.item.id === this.currentSelectedFlyOut.id) {
                this.currentSelectedFlyOut = new Object();
            }
            else {
                this.currentSelectedFlyOut = args.item || {};
            }
        }
        this.setGridInstanceList(this.selectedGridOptions);
        this.setFlyOutTitle();
    }
    handleCellClick(dataGridOptions, cellObject) {
        var _a;
        this.selectedGridInstance = dataGridOptions.gridComponentInstance;
        this.selectedGridOptions = dataGridOptions;
        this.selectedEntityInfo.gridName = dataGridOptions.gridName;
        this.selectedEntityInfo.columnList = dataGridOptions.columns;
        this.setGridInstanceList(this.selectedGridOptions);
        if ((_a = cellObject.column) === null || _a === void 0 ? void 0 : _a.dataField) {
            this.selectedColumn = Object.assign({}, dataGridOptions.columns.filter((item) => item.dataField === cellObject.column.dataField)[0]);
        }
        else {
            this.selectedColumn = null;
        }
    }
    setGridInstanceList(currentGridOptions) {
        var _a;
        this.gridComponentInstanceList = [];
        const gridName = currentGridOptions.gridName || this.options.gridOptions.gridName;
        const isMasterGridSelected = this.options.gridOptions.gridName === gridName && currentGridOptions.isMasterGrid;
        // this.conditionalFormattingTitle = isMasterGridSelected ? 'CONDITIONAL FORMATTING' : 'CONDITIONAL FORMATTING - CHILD VIEW';
        // Adding Master grid instance
        this.gridComponentInstanceList.push({
            isMasterGrid: true,
            gridName: this.options.gridOptions.gridName,
            gridComponentInstance: this.options.gridOptions.gridComponentInstance,
            isSelected: isMasterGridSelected
        });
        // Adding child grid instances
        if (this.options.gridOptions.childEntityList.length) {
            this.options.gridOptions.childEntityList[0].childGridInstanceList.forEach(childGridInstance => {
                this.gridComponentInstanceList.push({
                    isMasterGrid: false,
                    gridName: this.options.gridOptions.childEntityList[0].gridOptions.gridName,
                    gridComponentInstance: childGridInstance,
                    isSelected: !isMasterGridSelected,
                });
            });
        }
        const selectedGridInstance = this.gridComponentInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length);
        if (selectedGridInstance.length) {
            this.conditionalFormattingStorageKey = getStorageKey(selectedGridInstance[0].gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], selectedGridInstance[0].isMasterGrid);
            if (CGFFlyOutEnum.conditionFormatting === ((_a = this.currentSelectedFlyOut) === null || _a === void 0 ? void 0 : _a.id)) {
                this.selectedEntityInfo.columnList = this.prepareLookUpDataForConditonalFormatting(this.selectedEntityInfo.columnList);
            }
        }
        this.cachingEnabled = isMasterGridSelected ? this.isCached : false;
    }
    prepareLookUpDataForConditonalFormatting(columnList) {
        const arr = [];
        for (let index = 0; index < columnList.length; index++) {
            columnList[index].lookup = { dataSource: this.getDataLookDataSource(columnList[index].dataField) };
            // columnList[index].filterOperations = getFilterOperations(columnList[index].dataType);
        }
        return columnList;
    }
    getDataLookDataSource(dataField) {
        const arr = [];
        this.options.gridOptions.dataSource.forEach(item => {
            if (arr.indexOf(item[dataField]) === -1) {
                arr.push(item[dataField]);
            }
        });
        return [...new Set(arr)];
    }
    updateCachedDataSource() {
        this.options.gridOptions.gridComponentInstance.beginUpdate();
        this.cachedColumns.forEach(item => {
            this.options.gridOptions.gridComponentInstance.columnOption(item.dataField, 'visible', item.visible);
        });
        this.options.gridOptions.gridComponentInstance.option('dataSource', this.options.gridOptions.dataSource);
        this.options.gridOptions.gridComponentInstance.endUpdate();
    }
    tryToApplyInitialViews() {
        if (this.options.viewDataSource) {
            this.options.viewDataSource.getViews(this.options.viewDataSource.key).then(layouts => {
                this.options.viewList = layouts;
                if (this.isViewDefinedInQueryParams()) {
                    this.tryToApplyQueryParamLayout(layouts);
                }
                else {
                    this.tryToApplyDefaultLayout(layouts);
                }
            }).catch(() => {
                appToast({ type: 'error', message: 'Unable to retrieve the layouts for this grid' });
            });
        }
    }
    tryToApplyQueryParamLayout(views) {
        const queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        if (this.isViewDefinedInQueryParams()) {
            const queryView = views.find(f => f.id === Number(queryParams.layout));
            this.options.viewDataSource.applyView(queryView).then(gridView => {
                this.viewEvents({ event: CGFEventsEnum.applyView, data: gridView });
            }).catch(() => {
                appToast({ type: 'error', message: `The layout with ID: ${queryParams.layout} was not applied successfully` });
            });
        }
    }
    isViewDefinedInQueryParams() {
        const queryParams = this.activatedRoute.snapshot.queryParams;
        return queryParams.layout && queryParams.layout > 0;
    }
    tryToApplyDefaultLayout(views) {
        const defaultView = this.getDefaultView(views);
        if (defaultView) {
            this.options.viewDataSource.applyView(defaultView).then(gridView => {
                const queryParams = Object.assign({}, this.activatedRoute.snapshot.queryParams);
                queryParams.layout = gridView.id;
                this.router.navigate([], { queryParams });
                this.viewEvents({ event: CGFEventsEnum.applyView, data: gridView });
            }).catch(() => {
                appToast({ type: 'error', message: 'The default layout was not applied successfully' });
            });
        }
    }
    getDefaultView(views) {
        return views.filter(view => view.isDefault || view.isGlobalDefault)[0] || views[0] || null;
    }
    setFlyOutTitle() {
        if (this.currentSelectedFlyOut) {
            const isMasterSelected = this.selectedGridOptions.isMasterGrid;
            switch (this.currentSelectedFlyOut.id) {
                case CGFFlyOutEnum.conditionFormatting:
                    this.flyOutTitle = isMasterSelected ? 'CONDITIONAL FORMATTING' : 'CONDITIONAL FORMATTING - CHILD VIEW';
                    break;
                case CGFFlyOutEnum.customColumns:
                    this.flyOutTitle = isMasterSelected ? 'CUSTOM COLUMN' : 'CUSTOM COLUMN - CHILD VIEW';
                    break;
            }
        }
        else {
            this.flyOutTitle = '';
        }
    }
};
CommonGridFrameworkComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router }
];
__decorate([
    ViewChild(FlyOutActionIconContainerComponent)
], CommonGridFrameworkComponent.prototype, "appCGFContainer", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "options", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "securityIdColumnName", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "isEditSecurity", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "showEntityParameter", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "isCached", void 0);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "currentFlyOut", null);
__decorate([
    Input()
], CommonGridFrameworkComponent.prototype, "cgfFeatureUpdate", null);
__decorate([
    Output()
], CommonGridFrameworkComponent.prototype, "cgfEvent", void 0);
__decorate([
    Output()
], CommonGridFrameworkComponent.prototype, "loadChildEntityData", void 0);
__decorate([
    Output()
], CommonGridFrameworkComponent.prototype, "selectionChanged", void 0);
CommonGridFrameworkComponent = __decorate([
    Component({
        selector: 'pbi-common-grid-framework',
        template: "<ng-template [ngIf]=\"options.showCGF\">\r\n    <div class=\"action-container\">\r\n        <div *ngIf=\"containsVisibleParameters\" class=\"entity-param-with-edit-button-container\">\r\n            <div>\r\n                <div>\r\n                    <div class=\"data-entity-parameter-container\">\r\n                        <span class=\"ellipsis data-entity-parameter-edit-label\"\r\n                            title=\"To Edit/View Parameters click on edit icon.\">\r\n                            <span *ngFor=\"let arrItem of options.dataBrowserEntityParameters;let i = index;\">\r\n                                <ng-template *ngIf=\"!arrItem.HideInDataBrowser\">\r\n                                    <span> {{arrItem.Name}} : {{arrItem.Value}} </span>\r\n                                </ng-template>\r\n                                <ng-template\r\n                                    *ngIf=\"i < options.dataBrowserEntityParameters.length - 1 && !arrItem.HideInDataBrowser\">\r\n                                    <span>|</span>\r\n                                </ng-template>\r\n                            </span>\r\n                        </span>\r\n                        <span class=\"parameter-edit-icon-container\" (click)=\"updateParameterSectionVisibility()\">\r\n                            <i class=\"fas fa-pen pointer parameter-edit-icon\" title=\"Edit/View Parameters\"></i>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ng-template [ngIf]=\"options.showFlyoutButtonContainer\">\r\n            <pbi-fly-out-action-icon-container [position]=\"options.flyOutPosition\"\r\n                [visibleFlyOuts]=\"options.visibleFlyOuts\" [selectedFlyOut]=\"currentSelectedFlyOut\"\r\n                [resetContainerSettings]=\"resetSettings\" [showParametersControls]=\"showEntityParameter\"\r\n                [cgfLeftMenuList]=\"options.gridOptions.contextMenuMappingList\" [showLoader]=\"options.showLoader\"\r\n                [dataBrowserEntityParameters]=\"options.dataBrowserEntityParameters\"\r\n                [disableRefreshIcon]=\"disableRefresh\" (flyOutSelectionClick)=\"onFlyOutSelectionChange($event)\"\r\n                (refreshEntityData)=\"onRefreshEntityData()\">\r\n            </pbi-fly-out-action-icon-container>\r\n        </ng-template>\r\n    </div>\r\n    <div class=\"parameter-controls-container\" *ngIf=\"options.dataBrowserEntityParameters.length && showEntityParameter\">\r\n        <div>\r\n            <div>\r\n                <label class=\"parameter-header-label\">Parameters</label>\r\n                <i class=\"far fa-save pointer\" style=\"margin-left: 10px; font-size: 14px; color: #9b9ea0;\"\r\n                    title=\"Save order of parameters.\" (click)=\"saveOrderOfParameters()\"></i>\r\n            </div>\r\n            <div class=\" text-align-right parameters-close-button\">\r\n                <i class=\"fas fa-times pointer\" title=\"Close\" (click)=\"updateParameterSectionVisibility()\"></i>\r\n            </div>\r\n        </div>\r\n        <div class=\"parameter-controls-data-container\">\r\n            <pbi-entity-parameters [(dataBrowserEntityParameters)]=\"options.dataBrowserEntityParameters\"\r\n                (entityParamEvent)=\"entityParamEvent($event)\" (flyOutSelectionClick)=\"onFlyOutSelectionClick($event)\"\r\n                (disableRefreshIcon)=\"onFocusoutEntityParam($event)\">\r\n            </pbi-entity-parameters>\r\n        </div>\r\n    </div>\r\n    <div class=\"grid-and-fly-out-container\">\r\n        <div\r\n            [ngClass]=\"{'fly-out-close-grid-width': !currentSelectedFlyOut, 'fly-out-open-grid-width': currentSelectedFlyOut}\">\r\n            <ng-template [ngIf]=\"options.showGrid\">\r\n                <pbi-grid (gridInitialized)=\"onGridInitialized()\" [gridOptions]=\"options.gridOptions\"\r\n                    (gridChanged)=\"gridChanged($event)\" (gridCellClick)=\"cellClickEvent($event)\"\r\n                    (customButtonClick)=\"customButtonClick($event)\" (childEntityData)=\"getChildEntityData($event)\"\r\n                    (childEntityCellClick)=\"childEntityCellClick($event)\">\r\n                </pbi-grid>\r\n            </ng-template>\r\n        </div>\r\n        <div class=\"fly-out-container\" *ngIf=\"currentSelectedFlyOut?.id > 0\">\r\n            <div [ngSwitch]=\"currentSelectedFlyOut.id\">\r\n                <pbi-filter *ngSwitchCase=\"flyOutEnums.filter\" [filterGridOptions]=\"selectedGridOptions\"\r\n                    [gridInstanceList]=\"gridComponentInstanceList\" (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-filter>\r\n                <pbi-column-chooser *ngSwitchCase=\"flyOutEnums.columnChooser\"\r\n                    [(columnsList)]=\"options.gridOptions.columns\" [gridInstanceList]=\"gridComponentInstanceList\"\r\n                    [(selectedColumnInfo)]=\"selectedColumn\" [isCached]=\"isCached\" [useBulkApply]=\"options.useBulkApply\"\r\n                    [refreshOnColumnUpdate]=\"options.refreshOnColumnUpdate\" [debounce]=\"options.columnDebounce\"\r\n                    (closeCurrentFlyOut)=\"closeCurrentFlyOut()\" (applyButtonClick)=\"applyButtonClick($event)\">\r\n                </pbi-column-chooser>\r\n                <pbi-view-selection *ngSwitchCase=\"flyOutEnums.viewSelection\"\r\n                    [gridInstanceList]=\"gridComponentInstanceList\" [(viewList)]=\"options.viewList\"\r\n                    [(selectedTheme)]=\"options.gridOptions.selectedTheme\" [viewDataSource]=\"options.viewDataSource\"\r\n                    [(isGridBorderVisible)]=\"options.gridOptions.showColumnLines\" (viewEvent)=\"viewEvents($event)\"\r\n                    (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-view-selection>\r\n                <pbi-settings *ngSwitchCase=\"flyOutEnums.gridSettings\"\r\n                    [cgfSettingsOptions]=\"options.settingFlyOutOptions\"\r\n                    [(selectedTheme)]=\"options.gridOptions.selectedTheme\"\r\n                    [(selectedBorderSwitch)]=\"options.gridOptions.showColumnLines\"\r\n                    [gridInstanceList]=\"gridComponentInstanceList\" (gridSwitchValueChange)=\"onGridOptionChange($event)\"\r\n                    (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-settings>\r\n                <pbi-conditional-formatting *ngSwitchCase=\"flyOutEnums.conditionFormatting\"\r\n                    [listOfColumns]=\"selectedEntityInfo.columnList\" [storageKey]=\"conditionalFormattingStorageKey\"\r\n                    (valueChange)=\"changeInValueOfConditionalFormatting()\" (closeCurrentFlyOut)=\"closeCurrentFlyOut()\">\r\n                </pbi-conditional-formatting>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</ng-template>\r\n",
        styles: [""]
    })
], CommonGridFrameworkComponent);
export { CommonGridFrameworkComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLWdyaWQtZnJhbWV3b3JrLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tcG9uZW50cy9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tbW9uLWdyaWQtZnJhbWV3b3JrLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsT0FBTyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDM0csT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBVSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFNMUYsT0FBTyxFQUFFLFFBQVEsRUFBRSxxQkFBcUIsRUFBRSxhQUFhLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNsRyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFHMUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDakUsT0FBTyxFQUFFLGtDQUFrQyxFQUFFLE1BQU0sMEVBQTBFLENBQUM7QUFPOUgsSUFBYSw0QkFBNEIsR0FBekMsTUFBYSw0QkFBNEI7SUFvRHZDLHlCQUF5QjtJQUV6QixZQUFvQixjQUE4QixFQUFVLE1BQWM7UUFBdEQsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQWpEMUQseUJBQW9CLEdBQUcsRUFBRSxDQUFDO1FBQzFCLG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLHdCQUFtQixHQUFHLEtBQUssQ0FBQztRQUM1QixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBbUJoQixhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUM5Qix3QkFBbUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3pDLHFCQUFnQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFdkQsc0JBQXNCO1FBQ3RCLGtCQUFhLEdBQXlCLEVBQUUsQ0FBQztRQUN6QyxtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixvQ0FBK0IsR0FBRyxjQUFjLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDdkYsMEJBQXFCLEdBQWtCLElBQUksQ0FBQztRQUM1QyxtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0QixnQkFBVyxHQUFHLGFBQWEsQ0FBQyxDQUFDLG9DQUFvQztRQUNqRSxnQkFBVyxHQUFHLEVBQUUsQ0FBQztRQUNqQiw4QkFBeUIsR0FBNkIsRUFBRSxDQUFDO1FBQ3pELGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLG1CQUFjLEdBQWtCLElBQUksQ0FBQztRQUNyQyx1QkFBa0IsR0FBeUIsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsQ0FBQztRQUM1RSx5QkFBb0IsR0FBZSxJQUFJLE1BQU0sRUFBZ0IsQ0FBQztRQUM5RCx3QkFBbUIsR0FBRyxJQUFJLE1BQU0sRUFBeUIsQ0FBQztRQUVsRCw0QkFBdUIsR0FBa0I7WUFDL0MsY0FBYyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQztZQUNwRCxjQUFjLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDO1lBQ25ELGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDO1lBQ3pDLGNBQWMsQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDO1NBQzdDLENBQUM7SUFHNEUsQ0FBQztJQTdDdEUsSUFBSSxhQUFhLENBQUMsTUFBcUI7UUFDOUMsSUFBSSxNQUFNLEVBQUU7WUFDVixNQUFNLElBQUksR0FBRyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsdUJBQXVCLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFBO1NBQ3ZDO0lBQ0gsQ0FBQztJQUVRLElBQUksZ0JBQWdCLENBQUMsTUFBNEI7UUFDeEQsUUFBUSxNQUFNLEVBQUU7WUFDZCxLQUFLLG9CQUFvQixDQUFDLG1CQUFtQjtnQkFDM0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3JELE1BQU07WUFDUixLQUFLLG9CQUFvQixDQUFDLFVBQVU7Z0JBQ2xDLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUM5QixNQUFNO1NBQ1Q7SUFDSCxDQUFDO0lBK0JELElBQUkseUJBQXlCO1FBQzNCLE1BQU0sYUFBYSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsMkJBQTJCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNqRyxPQUFPLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFFRCxRQUFROztRQUNOLFVBQUksSUFBSSxDQUFDLE9BQU8sMENBQUUsMkJBQTJCLENBQUMsTUFBTSxFQUFFO1lBQ3BELElBQUksQ0FBQyxPQUFPLENBQUMsMkJBQTJCLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNqRCxJQUFJLEdBQUcsQ0FBQyxrQkFBa0IsRUFBRTtvQkFDMUIsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsRUFBRTt3QkFDNUMsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ3BJO29CQUFDLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsS0FBSyxVQUFVLEVBQUU7d0JBRS9DLEdBQUcsQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3FCQUN6Rjt5QkFBTTt3QkFDTCxHQUFHLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztxQkFDcEM7aUJBQ0Y7cUJBQU0sSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUU7b0JBQ2pFLEdBQUcsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2lCQUNuQjtxQkFBTSxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssTUFBTSxFQUFFO29CQUNwRixHQUFHLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxZQUFZLENBQUM7aUJBQzlCO1lBQ0gsQ0FBQyxDQUFDLENBQUE7U0FDSDtJQUNILENBQUM7SUFFRCxpQkFBaUI7UUFDZixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRU0sdUJBQXVCLENBQUMsSUFBNEQ7O1FBQ3pGLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE1BQU0sRUFBRTtZQUNoRixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUM7WUFDM0UsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1NBQ3JEO1FBQ0QsSUFBSSxPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssV0FBVyxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUNoRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQ3BELElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQzNCO2FBQU07WUFDTCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1lBQ2pDLElBQUksT0FBQSxJQUFJLENBQUMsSUFBSSwwQ0FBRSxFQUFFLGFBQUssSUFBSSxDQUFDLHFCQUFxQiwwQ0FBRSxFQUFFLENBQUEsRUFBRTtnQkFDcEQsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7YUFDM0I7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDO2FBQ2hEO1NBQ0Y7UUFDRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVNLFVBQVUsQ0FBQyxJQUFzQjtRQUV0QyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFO1lBQy9CLFFBQVEsSUFBSSxDQUFDLEtBQUssRUFBRTtnQkFDbEIsS0FBSyxhQUFhLENBQUMsU0FBUztvQkFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEQsTUFBTTtnQkFDUixLQUFLLGFBQWEsQ0FBQyxhQUFhO29CQUM5QixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNwQztvQkFDRSxNQUFNO2FBQ1Q7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDMUI7SUFDSCxDQUFDO0lBRU0saUJBQWlCLENBQUMsSUFBSTtRQUMzQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRU0sZ0JBQWdCLENBQUMsSUFBSTtRQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRU0scUJBQXFCLENBQUMsSUFBSTtRQUMvQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztJQUM3QixDQUFDO0lBRU0sY0FBYyxDQUFDLGVBQWU7UUFDbkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFTSxXQUFXLENBQUMsS0FBSztRQUN0QixJQUFJLEtBQUssRUFBRTtZQUNULE1BQU0sZUFBZSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUM7WUFDdEQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEVBQUUsRUFBRSxFQUFFLGFBQWEsQ0FBQyxNQUFNLEVBQVMsQ0FBQztZQUNqRSxVQUFVLENBQUMsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsR0FBRyxlQUFlLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDMUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7U0FDNUI7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLG1CQUFtQixJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsTUFBTSxFQUFFO2dCQUM1RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7YUFDcEQ7U0FDRjtJQUNILENBQUM7SUFFTSxrQkFBa0IsQ0FBQyxJQUFJOztRQUM1QixRQUFRLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDaEIsS0FBSyxXQUFXO2dCQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsS0FBZ0IsQ0FBQztnQkFDakUsVUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLDBDQUFFLFdBQVcsRUFBRTtvQkFDNUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLEtBQWdCLENBQUM7aUJBQ2pHO2dCQUNELE1BQU07WUFDUixLQUFLLE9BQU87Z0JBQ1YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFlLENBQUM7Z0JBQzlELGlIQUFpSDtnQkFDakgsVUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLDBDQUFFLFdBQVcsRUFBRTtvQkFDNUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQWUsQ0FBQztpQkFDOUY7Z0JBQ0QsTUFBTTtTQUNUO0lBQ0gsQ0FBQztJQUVNLGlCQUFpQixDQUFDLElBQUk7UUFDM0IsUUFBUSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ3BCLEtBQUssTUFBTTtnQkFDVCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2dCQUN2RixJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFO29CQUNyQyxNQUFNLElBQUksR0FBRyxXQUFXLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDaEUsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssWUFBWSxJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxhQUFhLENBQUMsQ0FBQTtvQkFDM0YsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTt3QkFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsYUFBYSxDQUFDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7cUJBQy9FO3lCQUFNO3dCQUNMLFFBQVEsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLGlFQUFpRSxFQUFFLENBQUMsQ0FBQztxQkFDekc7b0JBQ0QsTUFBTTtpQkFDUDtnQkFDRCxNQUFNO1lBQ1IsS0FBSyxLQUFLO2dCQUNSLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGFBQWEsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3BFLE1BQU07U0FDVDtJQUNILENBQUM7SUFFTSxnQkFBZ0IsQ0FBQyxJQUEwQjtRQUNoRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxhQUFhLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFTSxtQkFBbUI7UUFDeEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsYUFBYSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRU0scUJBQXFCO1FBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGFBQWEsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVNLG9DQUFvQztRQUN6QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMzRCxDQUFDO0lBRU0sa0JBQWtCLENBQUMsS0FBSzs7UUFDN0IsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLE1BQU0saUJBQWlCLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFDO1lBQ3ZGLE1BQU0scUJBQXFCLFNBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLEVBQUUsMENBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQzdHLEdBQUcsQ0FBQyxTQUFTLEtBQUssaUJBQWlCLENBQUMsQ0FBQztZQUN2QyxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxFQUFFO2dCQUNqQyxRQUFRLENBQUM7b0JBQ1AsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsdUJBQXVCLGlCQUFpQix3REFBd0Q7b0JBQ3RILFdBQVcsRUFBRSxJQUFJO2lCQUNwQixDQUFDLENBQUM7Z0JBQ0gsT0FBTzthQUNSO1NBQ0Y7UUFDRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFTSxvQkFBb0IsQ0FBQyxJQUFJO1FBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDdEQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFO2dCQUNoRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQzthQUM3QztRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFTSx1QkFBdUI7UUFDNUIsSUFBSSxZQUFZLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQztRQUN6QyxPQUFPLFlBQVksRUFBRSxFQUFFO1lBQ3JCLE1BQU0sR0FBRyxHQUFHLGNBQWMsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0MsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDOUYsY0FBYyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNoQztTQUNGO1FBQ0QsY0FBYyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMseUJBQXlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFFTSxrQkFBa0I7UUFDdkIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztRQUNsQyw2QkFBNkI7SUFDL0IsQ0FBQztJQUVNLGdDQUFnQzs7UUFDckMsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3hCLE1BQUEsSUFBSSxDQUFDLGVBQWUsMENBQUUsK0JBQStCLEdBQUc7U0FDekQ7YUFBTTtZQUNMLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLG1CQUFtQixFQUFFLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQztTQUNqRjtJQUNILENBQUM7SUFFTSxzQkFBc0IsQ0FBQyxJQUFJO1FBQ2hDLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE1BQU0sRUFBRTtZQUNoRixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUM7WUFDM0UsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1NBQ3JEO1FBQ0QsSUFBSSxPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssV0FBVyxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUNoRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQ3BELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLE1BQU0sRUFBbUIsQ0FBQztTQUM1RDthQUFNO1lBQ0wsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztZQUNqQyxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsRUFBRTtnQkFDL0QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksTUFBTSxFQUFtQixDQUFDO2FBQzVEO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQzthQUM5QztTQUNGO1FBQ0QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBRU8sZUFBZSxDQUFDLGVBQW9DLEVBQUUsVUFBZTs7UUFDM0UsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQztRQUNsRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZUFBZSxDQUFDO1FBQzNDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLEdBQUcsZUFBZSxDQUFDLFFBQVEsQ0FBQztRQUM1RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUM7UUFDN0QsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBRW5ELFVBQUksVUFBVSxDQUFDLE1BQU0sMENBQUUsU0FBUyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxjQUFjLHFCQUNkLGVBQWUsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FDekMsSUFBSSxDQUFDLFNBQVMsS0FBSyxVQUFVLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUNyRCxDQUFDO1NBQ0g7YUFBTTtZQUNMLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQzVCO0lBQ0gsQ0FBQztJQUVPLG1CQUFtQixDQUFDLGtCQUF1Qzs7UUFDakUsSUFBSSxDQUFDLHlCQUF5QixHQUFHLEVBQUUsQ0FBQztRQUNwQyxNQUFNLFFBQVEsR0FBRyxrQkFBa0IsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQ2xGLE1BQU0sb0JBQW9CLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxLQUFLLFFBQVEsSUFBSSxrQkFBa0IsQ0FBQyxZQUFZLENBQUM7UUFDL0csNkhBQTZIO1FBQzdILDhCQUE4QjtRQUM5QixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDO1lBQ2xDLFlBQVksRUFBRSxJQUFJO1lBQ2xCLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxRQUFRO1lBQzNDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLHFCQUFxQjtZQUNyRSxVQUFVLEVBQUUsb0JBQW9CO1NBQ2pDLENBQUMsQ0FBQztRQUNILDhCQUE4QjtRQUM5QixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUU7WUFDbkQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO2dCQUM1RixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDO29CQUNsQyxZQUFZLEVBQUUsS0FBSztvQkFDbkIsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsUUFBUTtvQkFDMUUscUJBQXFCLEVBQUUsaUJBQWlCO29CQUN4QyxVQUFVLEVBQUUsQ0FBQyxvQkFBb0I7aUJBQ2xDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxNQUFNLG9CQUFvQixHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUksSUFBSSxvQkFBb0IsQ0FBQyxNQUFNLEVBQUU7WUFDL0IsSUFBSSxDQUFDLCtCQUErQixHQUFHLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsRUFDaEcsY0FBYyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzlGLElBQUksYUFBYSxDQUFDLG1CQUFtQixZQUFLLElBQUksQ0FBQyxxQkFBcUIsMENBQUUsRUFBRSxDQUFBLEVBQUU7Z0JBQ3hFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHdDQUF3QyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN4SDtTQUNGO1FBQ0QsSUFBSSxDQUFDLGNBQWMsR0FBRyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3JFLENBQUM7SUFFTyx3Q0FBd0MsQ0FBQyxVQUEyQjtRQUMxRSxNQUFNLEdBQUcsR0FBUSxFQUFFLENBQUM7UUFDcEIsS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDdEQsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7WUFDbkcsd0ZBQXdGO1NBQ3pGO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDcEIsQ0FBQztJQUVPLHFCQUFxQixDQUFDLFNBQWlCO1FBQzdDLE1BQU0sR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFVBQXlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2pFLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDdkMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzthQUMzQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEdBQUcsSUFBSSxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRU8sc0JBQXNCO1FBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdkcsQ0FBQyxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3pHLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQzdELENBQUM7SUFHTyxzQkFBc0I7UUFDNUIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRTtZQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNuRixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7Z0JBQ2hDLElBQUksSUFBSSxDQUFDLDBCQUEwQixFQUFFLEVBQUU7b0JBQ3JDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDMUM7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN2QztZQUNILENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsOENBQThDLEVBQUUsQ0FBQyxDQUFDO1lBQ3ZGLENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRU8sMEJBQTBCLENBQUMsS0FBMkI7UUFDNUQsTUFBTSxXQUFXLEdBQVcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDeEYsSUFBSSxJQUFJLENBQUMsMEJBQTBCLEVBQUUsRUFBRTtZQUNyQyxNQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFDdkUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDL0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ3RFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsdUJBQXVCLFdBQVcsQ0FBQyxNQUFNLCtCQUErQixFQUFFLENBQUMsQ0FBQztZQUNqSCxDQUFDLENBQUMsQ0FBQTtTQUNIO0lBQ0gsQ0FBQztJQUVPLDBCQUEwQjtRQUNoQyxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7UUFDN0QsT0FBTyxXQUFXLENBQUMsTUFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFTyx1QkFBdUIsQ0FBQyxLQUEyQjtRQUN6RCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9DLElBQUksV0FBVyxFQUFFO1lBQ2YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDakUsTUFBTSxXQUFXLEdBQVcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3hGLFdBQVcsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEVBQUUsQ0FBQztnQkFDakMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ3RFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsaURBQWlELEVBQUUsQ0FBQyxDQUFDO1lBQzFGLENBQUMsQ0FBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDO0lBRU8sY0FBYyxDQUFDLEtBQTJCO1FBQ2hELE9BQU8sS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUM7SUFDN0YsQ0FBQztJQUVPLGNBQWM7UUFDcEIsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDOUIsTUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsWUFBWSxDQUFDO1lBQy9ELFFBQVEsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsRUFBRTtnQkFDckMsS0FBSyxhQUFhLENBQUMsbUJBQW1CO29CQUNwQyxJQUFJLENBQUMsV0FBVyxHQUFHLGdCQUFnQixDQUFDLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUMscUNBQXFDLENBQUM7b0JBQ3ZHLE1BQU07Z0JBQ1IsS0FBSyxhQUFhLENBQUMsYUFBYTtvQkFDOUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyw0QkFBNEIsQ0FBQztvQkFDckYsTUFBTTthQUNUO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQztDQUNGLENBQUE7O1lBdFhxQyxjQUFjO1lBQWtCLE1BQU07O0FBcEQzQjtJQUE5QyxTQUFTLENBQUMsa0NBQWtDLENBQUM7cUVBQXFEO0FBRTFGO0lBQVIsS0FBSyxFQUFFOzZEQUFvRDtBQUNuRDtJQUFSLEtBQUssRUFBRTswRUFBa0M7QUFDakM7SUFBUixLQUFLLEVBQUU7b0VBQStCO0FBQzlCO0lBQVIsS0FBSyxFQUFFO3lFQUFvQztBQUNuQztJQUFSLEtBQUssRUFBRTs4REFBeUI7QUFDeEI7SUFBUixLQUFLLEVBQUU7aUVBS1A7QUFFUTtJQUFSLEtBQUssRUFBRTtvRUFTUDtBQUVTO0lBQVQsTUFBTSxFQUFFOzhEQUFzQztBQUNyQztJQUFULE1BQU0sRUFBRTt5RUFBaUQ7QUFDaEQ7SUFBVCxNQUFNLEVBQUU7c0VBQThDO0FBN0I1Qyw0QkFBNEI7SUFMeEMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLDJCQUEyQjtRQUNyQyw0dk5BQXFEOztLQUV0RCxDQUFDO0dBQ1csNEJBQTRCLENBNGF4QztTQTVhWSw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZHhEYXRhR3JpZCBmcm9tICdkZXZleHRyZW1lL3VpL2RhdGFfZ3JpZCc7XHJcbmltcG9ydCB7IENHRkZseU91dEVudW0sIENHRkV2ZW50c0VudW0sIENHRlN0b3JhZ2VLZXlzLCBDR0ZGZWF0dXJlVXBkYXRlRW51bSB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy9lbnVtcyc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQQklDb21tb25HcmlkRnJhbWV3b3JrT3B0aW9uc01vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2NvbW1vbi1ncmlkLWZyYW1ld29yay5tb2RlbCc7XHJcbmltcG9ydCB7IFBCSUdyaWRDb2x1bW4gfSBmcm9tICcuLi9jb250cmFjdHMvZ3JpZCc7XHJcbmltcG9ydCB7IFBCSUdyaWRPcHRpb25zTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvZ3JpZE9wdGlvbnMubW9kZWwnO1xyXG5pbXBvcnQgeyBTZWxlY3RlZENvbHVtbkluZm8gfSBmcm9tICcuLi9jb250cmFjdHMvY29sdW1uJztcclxuaW1wb3J0IHsgU2VsZWN0ZWRFbnRpdHlDb2x1bW4sIEdyaWRDb21wb25lbnRJbnN0YW5jZXMsIEZseU91dFNlY3Rpb24gfSBmcm9tICcuLi9jb250cmFjdHMvY29tbW9uLWdyaWQtZnJhbWV3b3JrJztcclxuaW1wb3J0IHsgYXBwVG9hc3QsIGNvbnZlcnREYXRlVG9Vc0Zvcm1hdCwgZ2V0U3RvcmFnZUtleSB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy91dGlsaXR5RnVuY3Rpb25zJztcclxuaW1wb3J0IHsgY2dmRmx5T3V0TGlzdCB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy9jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBWaWV3QVBJQXJndW1lbnRzIH0gZnJvbSAnLi4vY29udHJhY3RzL3ZpZXctc2VsZWN0aW9uJztcclxuaW1wb3J0IHsgVmlld1NlbGVjdGlvbk1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3ZpZXctc2VsZWN0aW9uLm1vZGVsJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFBhcmFtcywgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgRmx5T3V0QWN0aW9uSWNvbkNvbnRhaW5lckNvbXBvbmVudCB9IGZyb20gJy4uL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyLmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS1jb21tb24tZ3JpZC1mcmFtZXdvcmsnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb21tb24tZ3JpZC1mcmFtZXdvcmsuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbW1vbi1ncmlkLWZyYW1ld29yay5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1vbkdyaWRGcmFtZXdvcmtDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBAVmlld0NoaWxkKEZseU91dEFjdGlvbkljb25Db250YWluZXJDb21wb25lbnQpIGFwcENHRkNvbnRhaW5lcjogRmx5T3V0QWN0aW9uSWNvbkNvbnRhaW5lckNvbXBvbmVudDtcclxuXHJcbiAgQElucHV0KCkgcHVibGljIG9wdGlvbnM6IFBCSUNvbW1vbkdyaWRGcmFtZXdvcmtPcHRpb25zTW9kZWw7XHJcbiAgQElucHV0KCkgcHVibGljIHNlY3VyaXR5SWRDb2x1bW5OYW1lID0gJyc7XHJcbiAgQElucHV0KCkgcHVibGljIGlzRWRpdFNlY3VyaXR5ID0gZmFsc2U7XHJcbiAgQElucHV0KCkgcHVibGljIHNob3dFbnRpdHlQYXJhbWV0ZXIgPSBmYWxzZTtcclxuICBASW5wdXQoKSBwdWJsaWMgaXNDYWNoZWQgPSBmYWxzZTtcclxuICBASW5wdXQoKSBzZXQgY3VycmVudEZseU91dChmbHlPdXQ6IENHRkZseU91dEVudW0pIHtcclxuICAgIGlmIChmbHlPdXQpIHtcclxuICAgICAgY29uc3QgaXRlbSA9IGNnZkZseU91dExpc3QuZmluZChmID0+IGYuaWQgPT09IGZseU91dCk7XHJcbiAgICAgIHRoaXMub25GbHlPdXRTZWxlY3Rpb25DaGFuZ2UoeyBpdGVtIH0pXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBASW5wdXQoKSBzZXQgY2dmRmVhdHVyZVVwZGF0ZShvcHRpb246IENHRkZlYXR1cmVVcGRhdGVFbnVtKSB7XHJcbiAgICBzd2l0Y2ggKG9wdGlvbikge1xyXG4gICAgICBjYXNlIENHRkZlYXR1cmVVcGRhdGVFbnVtLmN1c3RvbUNvbHVtblVwZGF0ZWQ6XHJcbiAgICAgICAgdGhpcy5oYW5kbGVDZWxsQ2xpY2sodGhpcy5zZWxlY3RlZEdyaWRPcHRpb25zLCBudWxsKTtcclxuICAgICAgICBicmVhaztcclxuICAgICAgY2FzZSBDR0ZGZWF0dXJlVXBkYXRlRW51bS5jYWNoZWREYXRhOlxyXG4gICAgICAgIHRoaXMudXBkYXRlQ2FjaGVkRGF0YVNvdXJjZSgpO1xyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQE91dHB1dCgpIHB1YmxpYyBjZ2ZFdmVudCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGxvYWRDaGlsZEVudGl0eURhdGEgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBzZWxlY3Rpb25DaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAvLyNyZWdpb24gRGF0YSBNZW1iZXJzXHJcbiAgY2FjaGVkQ29sdW1uczogU2VsZWN0ZWRDb2x1bW5JbmZvW10gPSBbXTtcclxuICBjYWNoaW5nRW5hYmxlZCA9IGZhbHNlO1xyXG4gIGNvbmRpdGlvbmFsRm9ybWF0dGluZ1N0b3JhZ2VLZXkgPSBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5jb25kaXRpb25hbEZvcm1hdHRpbmddO1xyXG4gIGN1cnJlbnRTZWxlY3RlZEZseU91dDogRmx5T3V0U2VjdGlvbiA9IG51bGw7XHJcbiAgZGlzYWJsZVJlZnJlc2ggPSB0cnVlO1xyXG4gIGZseU91dEVudW1zID0gQ0dGRmx5T3V0RW51bTsgLy8gdGhpcyB2YXJpYWJsZSBpcyBmb3IgSFRNTCBtYXBwaW5nXHJcbiAgZmx5T3V0VGl0bGUgPSAnJztcclxuICBncmlkQ29tcG9uZW50SW5zdGFuY2VMaXN0OiBHcmlkQ29tcG9uZW50SW5zdGFuY2VzW10gPSBbXTtcclxuICByZXNldFNldHRpbmdzID0gZmFsc2U7XHJcbiAgc2VsZWN0ZWRDb2x1bW46IFBCSUdyaWRDb2x1bW4gPSBudWxsO1xyXG4gIHNlbGVjdGVkRW50aXR5SW5mbzogU2VsZWN0ZWRFbnRpdHlDb2x1bW4gPSB7IGNvbHVtbkxpc3Q6IFtdLCBncmlkTmFtZTogJycgfTtcclxuICBzZWxlY3RlZEdyaWRJbnN0YW5jZTogZHhEYXRhR3JpZCA9IG5ldyBPYmplY3QoKSBhcyBkeERhdGFHcmlkO1xyXG4gIHNlbGVjdGVkR3JpZE9wdGlvbnMgPSBuZXcgT2JqZWN0KCkgYXMgUEJJR3JpZE9wdGlvbnNNb2RlbDtcclxuXHJcbiAgcHJpdmF0ZSBDR0ZTdG9yYWdlS2V5Q29sbGVjdGlvbjogQXJyYXk8c3RyaW5nPiA9IFtcclxuICAgIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmNvbmRpdGlvbmFsRm9ybWF0dGluZ10sXHJcbiAgICBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5kaWN0aW9uYXJ5Rm9ybWF0RGF0YV0sXHJcbiAgICBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5mb3JtYXREYXRhXSxcclxuICAgIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmNoaWxkR3JpZFZpZXddXHJcbiAgXTtcclxuICAvLyNlbmRyZWdpb24gRGF0YSBNZW1iZXJzXHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLCBwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7IH1cclxuXHJcbiAgZ2V0IGNvbnRhaW5zVmlzaWJsZVBhcmFtZXRlcnMoKTogYm9vbGVhbiB7XHJcbiAgICBjb25zdCB2aXNpYmxlUGFyYW1zID0gdGhpcy5vcHRpb25zLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVycy5maWx0ZXIocCA9PiAhcC5IaWRlSW5EYXRhQnJvd3Nlcik7XHJcbiAgICByZXR1cm4gdmlzaWJsZVBhcmFtcy5sZW5ndGggPiAwO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5vcHRpb25zPy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnMubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMub3B0aW9ucy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnMubWFwKHZhbCA9PiB7XHJcbiAgICAgICAgaWYgKHZhbC5EZWZhdWx0U2luZ2xlVmFsdWUpIHtcclxuICAgICAgICAgIGlmICh2YWwuRGF0YVR5cGUudG9Mb3dlckNhc2UoKSA9PT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAgICAgICAgIHZhbC5WYWx1ZSA9IGlzTmFOKHZhbC5EZWZhdWx0U2luZ2xlVmFsdWUpID8gdmFsLkRlZmF1bHRTaW5nbGVWYWx1ZS50b0xvd2VyQ2FzZSgpID09PSAndHJ1ZScgOiBOdW1iZXIodmFsLkRlZmF1bHRTaW5nbGVWYWx1ZSkgPT09IDE7XHJcbiAgICAgICAgICB9IGlmICh2YWwuRGF0YVR5cGUudG9Mb3dlckNhc2UoKSA9PT0gJ2RhdGV0aW1lJykge1xyXG5cclxuICAgICAgICAgICAgdmFsLlZhbHVlID0gdmFsLkRlZmF1bHRTaW5nbGVWYWx1ZSA/IGNvbnZlcnREYXRlVG9Vc0Zvcm1hdCh2YWwuRGVmYXVsdFNpbmdsZVZhbHVlKSA6ICcnO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdmFsLlZhbHVlID0gdmFsLkRlZmF1bHRTaW5nbGVWYWx1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHZhbC5EYXRhVHlwZS50b0xvd2VyQ2FzZSgpID09PSAnYm9vbGVhbicgJiYgIXZhbC5WYWx1ZSkge1xyXG4gICAgICAgICAgdmFsLlZhbHVlID0gZmFsc2U7XHJcbiAgICAgICAgfSBlbHNlIGlmICh2YWwuVHlwZS50b0xvd2VyQ2FzZSgpID09PSAnc3FsbGlzdCcgfHwgdmFsLlR5cGUudG9Mb3dlckNhc2UoKSA9PT0gJ2xpc3QnKSB7XHJcbiAgICAgICAgICB2YWwuVmFsdWUgPSB2YWwuRGVmYXVsdFZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uR3JpZEluaXRpYWxpemVkKCk6IHZvaWQge1xyXG4gICAgdGhpcy50cnlUb0FwcGx5SW5pdGlhbFZpZXdzKCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25GbHlPdXRTZWxlY3Rpb25DaGFuZ2UoYXJnczogeyBpdGVtOiBGbHlPdXRTZWN0aW9uLCBzaG93RW50aXR5UGFyYW1ldGVyPzogYm9vbGVhbiB9KTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2UgfHwgIU9iamVjdC5rZXlzKHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2UpLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlID0gdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmdyaWRDb21wb25lbnRJbnN0YW5jZTtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRPcHRpb25zID0gdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zO1xyXG4gICAgfVxyXG4gICAgaWYgKHR5cGVvZiBhcmdzLml0ZW0gPT09ICd1bmRlZmluZWQnICYmIGFyZ3Muc2hvd0VudGl0eVBhcmFtZXRlcikge1xyXG4gICAgICB0aGlzLnNob3dFbnRpdHlQYXJhbWV0ZXIgPSBhcmdzLnNob3dFbnRpdHlQYXJhbWV0ZXI7XHJcbiAgICAgIHRoaXMuY2xvc2VDdXJyZW50Rmx5T3V0KCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNob3dFbnRpdHlQYXJhbWV0ZXIgPSBmYWxzZTtcclxuICAgICAgaWYgKGFyZ3MuaXRlbT8uaWQgPT09IHRoaXMuY3VycmVudFNlbGVjdGVkRmx5T3V0Py5pZCkge1xyXG4gICAgICAgIHRoaXMuY2xvc2VDdXJyZW50Rmx5T3V0KCk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50U2VsZWN0ZWRGbHlPdXQgPSBhcmdzLml0ZW0gfHwgbnVsbDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGhpcy5zZXRHcmlkSW5zdGFuY2VMaXN0KHRoaXMuc2VsZWN0ZWRHcmlkT3B0aW9ucyk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdmlld0V2ZW50cyhhcmdzOiBWaWV3QVBJQXJndW1lbnRzKTogdm9pZCB7XHJcblxyXG4gICAgaWYgKHRoaXMub3B0aW9ucy52aWV3RGF0YVNvdXJjZSkge1xyXG4gICAgICBzd2l0Y2ggKGFyZ3MuZXZlbnQpIHtcclxuICAgICAgICBjYXNlIENHRkV2ZW50c0VudW0uYXBwbHlWaWV3OlxyXG4gICAgICAgICAgdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmFwcGx5Vmlld1RvR3JpZChhcmdzLmRhdGEpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSBDR0ZFdmVudHNFbnVtLnVwZGF0ZUxheW91dHM6XHJcbiAgICAgICAgICB0aGlzLm9wdGlvbnMudmlld0xpc3QgPSBhcmdzLmRhdGE7XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNnZkV2ZW50LmVtaXQoYXJncyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY3VzdG9tQ29sdW1uRXZlbnQoYXJncyk6IHZvaWQge1xyXG4gICAgdGhpcy5jZ2ZFdmVudC5lbWl0KGFyZ3MpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGVudGl0eVBhcmFtRXZlbnQoYXJncyk6IHZvaWQge1xyXG4gICAgdGhpcy5jZ2ZFdmVudC5lbWl0KGFyZ3MpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRm9jdXNvdXRFbnRpdHlQYXJhbShhcmdzKTogdm9pZCB7XHJcbiAgICB0aGlzLmRpc2FibGVSZWZyZXNoID0gYXJncztcclxuICB9XHJcblxyXG4gIHB1YmxpYyBjZWxsQ2xpY2tFdmVudChjZWxsQ2xpY2tPYmplY3QpOiB2b2lkIHtcclxuICAgIHRoaXMuaGFuZGxlQ2VsbENsaWNrKHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucywgY2VsbENsaWNrT2JqZWN0KTtcclxuICAgIHRoaXMuc2VsZWN0aW9uQ2hhbmdlZC5lbWl0KGNlbGxDbGlja09iamVjdCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZ3JpZENoYW5nZWQoZXZlbnQpOiB2b2lkIHtcclxuICAgIGlmIChldmVudCkge1xyXG4gICAgICBjb25zdCBjdXJyZW50Rmx5T3V0SWQgPSB0aGlzLmN1cnJlbnRTZWxlY3RlZEZseU91dC5pZDtcclxuICAgICAgdGhpcy5jdXJyZW50U2VsZWN0ZWRGbHlPdXQgPSB7IGlkOiBDR0ZGbHlPdXRFbnVtLm5ld1RhYiB9IGFzIGFueTtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7IHRoaXMuY3VycmVudFNlbGVjdGVkRmx5T3V0LmlkID0gY3VycmVudEZseU91dElkOyB9LCAwKTtcclxuICAgICAgdGhpcy5zZWxlY3RlZENvbHVtbiA9IG51bGw7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZEdyaWRPcHRpb25zICYmIE9iamVjdC5rZXlzKHRoaXMuc2VsZWN0ZWRHcmlkT3B0aW9ucykubGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy5zZXRHcmlkSW5zdGFuY2VMaXN0KHRoaXMuc2VsZWN0ZWRHcmlkT3B0aW9ucyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkdyaWRPcHRpb25DaGFuZ2UoYXJncyk6IHZvaWQge1xyXG4gICAgc3dpdGNoIChhcmdzLmtleSkge1xyXG4gICAgICBjYXNlICdncmlkTGluZXMnOlxyXG4gICAgICAgIHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucy5zaG93Q29sdW1uTGluZXMgPSBhcmdzLnZhbHVlIGFzIGJvb2xlYW47XHJcbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucy5jaGlsZEVudGl0eUxpc3RbMF0/LmdyaWRPcHRpb25zKSB7XHJcbiAgICAgICAgICB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuY2hpbGRFbnRpdHlMaXN0WzBdLmdyaWRPcHRpb25zLnNob3dDb2x1bW5MaW5lcyA9IGFyZ3MudmFsdWUgYXMgYm9vbGVhbjtcclxuICAgICAgICB9XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgJ3RoZW1lJzpcclxuICAgICAgICB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuc2VsZWN0ZWRUaGVtZSA9IGFyZ3MudmFsdWUgYXMgc3RyaW5nO1xyXG4gICAgICAgIC8vIHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucy5zZWxlY3RlZFRoZW1lQ2xhc3MgPSBnZXRDbGFzc05hbWVCeVRoZW1lTmFtZSh0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuc2VsZWN0ZWRUaGVtZSk7XHJcbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucy5jaGlsZEVudGl0eUxpc3RbMF0/LmdyaWRPcHRpb25zKSB7XHJcbiAgICAgICAgICB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuY2hpbGRFbnRpdHlMaXN0WzBdLmdyaWRPcHRpb25zLnNlbGVjdGVkVGhlbWUgPSBhcmdzLnZhbHVlIGFzIHN0cmluZztcclxuICAgICAgICB9XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY3VzdG9tQnV0dG9uQ2xpY2soYXJncyk6IHZvaWQge1xyXG4gICAgc3dpdGNoIChhcmdzLmJ0blR5cGUpIHtcclxuICAgICAgY2FzZSAnZWRpdCc6XHJcbiAgICAgICAgY29uc3QgdmlzaWJsZUNvbHMgPSB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmdldFZpc2libGVDb2x1bW5zKCk7XHJcbiAgICAgICAgaWYgKHZpc2libGVDb2xzICYmIHZpc2libGVDb2xzLmxlbmd0aCkge1xyXG4gICAgICAgICAgY29uc3QgY29scyA9IHZpc2libGVDb2xzLm1hcChkYXRhID0+IGRhdGEuZGF0YUZpZWxkKS5maWx0ZXIoY29sID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGNvbCAmJiAoY29sLnRvTG93ZXJDYXNlKCkgPT09ICdzZWN1cml0eWlkJyB8fCBjb2wudG9Mb3dlckNhc2UoKSA9PT0gJ3NlY3VyaXR5IGlkJylcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgaWYgKGNvbHMgJiYgY29scy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5jZ2ZFdmVudC5lbWl0KHsgZXZlbnQ6IENHRkV2ZW50c0VudW0uZWRpdFNlY3VyaXR5LCBkYXRhOiBhcmdzLnJvd0RhdGEgfSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBhcHBUb2FzdCh7IHR5cGU6ICdlcnJvcicsIG1lc3NhZ2U6ICdTZWN1cml0eUlkIGlzIGEgcmVxdWlyZWQgZmllbGQuIFBsZWFzZSBzZWxlY3QgZnJvbSBjb2x1bW4gbGlzdC4nIH0pO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdhZGQnOlxyXG4gICAgICAgIHRoaXMuY2dmRXZlbnQuZW1pdCh7IGV2ZW50OiBDR0ZFdmVudHNFbnVtLmVkaXRTZWN1cml0eSwgZGF0YToge30gfSk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgYXBwbHlCdXR0b25DbGljayhhcmdzOiBTZWxlY3RlZENvbHVtbkluZm9bXSk6IHZvaWQge1xyXG4gICAgdGhpcy5jbG9zZUN1cnJlbnRGbHlPdXQoKTtcclxuICAgIHRoaXMuY2FjaGVkQ29sdW1ucyA9IGFyZ3M7XHJcbiAgICB0aGlzLmNnZkV2ZW50LmVtaXQoeyBldmVudDogQ0dGRXZlbnRzRW51bS5nZXRDYWNoZWREYXRhLCBkYXRhOiBhcmdzIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uUmVmcmVzaEVudGl0eURhdGEoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsb3NlQ3VycmVudEZseU91dCgpO1xyXG4gICAgdGhpcy5jZ2ZFdmVudC5lbWl0KHsgZXZlbnQ6IENHRkV2ZW50c0VudW0ucmVmcmVzaEVudGl0eURhdGEgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgc2F2ZU9yZGVyT2ZQYXJhbWV0ZXJzKCk6IHZvaWQge1xyXG4gICAgdGhpcy5jZ2ZFdmVudC5lbWl0KHsgZXZlbnQ6IENHRkV2ZW50c0VudW0uc2F2ZVBhcmFtZXRlck9yZGVyIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGNoYW5nZUluVmFsdWVPZkNvbmRpdGlvbmFsRm9ybWF0dGluZygpOiB2b2lkIHtcclxuICAgIHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucy5ncmlkQ29tcG9uZW50SW5zdGFuY2UucmVwYWludCgpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldENoaWxkRW50aXR5RGF0YShldmVudCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuaXNDYWNoZWQpIHtcclxuICAgICAgY29uc3QgcHJpbWFyeUtleUNvbE5hbWUgPSB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuY2hpbGRFbnRpdHlMaXN0WzBdLnByaW1hcnlLZXlDb2x1bW47XHJcbiAgICAgIGNvbnN0IGlzUHJpbWFyeUNvbHVtbkV4aXN0cyA9IHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZ2V0VmlzaWJsZUNvbHVtbnMoKT8uZmlsdGVyKGNvbCA9PlxyXG4gICAgICAgIGNvbC5kYXRhRmllbGQgPT09IHByaW1hcnlLZXlDb2xOYW1lKTtcclxuICAgICAgaWYgKCFpc1ByaW1hcnlDb2x1bW5FeGlzdHMubGVuZ3RoKSB7XHJcbiAgICAgICAgYXBwVG9hc3Qoe1xyXG4gICAgICAgICAgdHlwZTogJ2Vycm9yJywgbWVzc2FnZTogYEZvbGxvd2luZyBjb2x1bW4ocykgJHtwcmltYXJ5S2V5Q29sTmFtZX0gaXMgcmVxdWlyZWQuIFBsZWFzZSBtYWtlIHN1cmUgc2FtZSBleGlzdHMgd2l0aCB2YWx1ZS5gXHJcbiAgICAgICAgICAsIGRpc3BsYXlUaW1lOiA0MDAwXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLmxvYWRDaGlsZEVudGl0eURhdGEuZW1pdChldmVudCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2hpbGRFbnRpdHlDZWxsQ2xpY2soYXJncyk6IHZvaWQge1xyXG4gICAgdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmNoaWxkRW50aXR5TGlzdC5mb3JFYWNoKGdyaWQgPT4ge1xyXG4gICAgICBpZiAoZ3JpZC5ncmlkT3B0aW9ucy5ncmlkTmFtZSA9PT0gYXJncy5jaGlsZEdyaWRPcHRpb25zLmdyaWROYW1lKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRPcHRpb25zID0gZ3JpZC5ncmlkT3B0aW9ucztcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLmhhbmRsZUNlbGxDbGljayh0aGlzLnNlbGVjdGVkR3JpZE9wdGlvbnMsIGFyZ3MuY2VsbE9iamVjdCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgcmVzZXRHcmlkRm9ybWF0dGluZ0luZm8oKTogdm9pZCB7XHJcbiAgICBsZXQgc2Vzc2lvbkNvdW50ID0gc2Vzc2lvblN0b3JhZ2UubGVuZ3RoO1xyXG4gICAgd2hpbGUgKHNlc3Npb25Db3VudC0tKSB7XHJcbiAgICAgIGNvbnN0IGtleSA9IHNlc3Npb25TdG9yYWdlLmtleShzZXNzaW9uQ291bnQpO1xyXG4gICAgICBpZiAodGhpcy5DR0ZTdG9yYWdlS2V5Q29sbGVjdGlvbi5maW5kSW5kZXgoaXRlbSA9PiBrZXkuaW5kZXhPZihpdGVtKSA+IC0xID8gdHJ1ZSA6IGZhbHNlKSA+IC0xKSB7XHJcbiAgICAgICAgc2Vzc2lvblN0b3JhZ2UucmVtb3ZlSXRlbShrZXkpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKCd0aGVtZScpO1xyXG4gICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiBpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZS5zdGF0ZSh7fSkpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb3NlQ3VycmVudEZseU91dCgpOiB2b2lkIHtcclxuICAgIHRoaXMuY3VycmVudFNlbGVjdGVkRmx5T3V0ID0gbnVsbDtcclxuICAgIC8vIHRoaXMucmVzZXRTZXR0aW5ncyA9IHRydWU7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdXBkYXRlUGFyYW1ldGVyU2VjdGlvblZpc2liaWxpdHkoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5hcHBDR0ZDb250YWluZXIpIHtcclxuICAgICAgdGhpcy5hcHBDR0ZDb250YWluZXI/LnNob3dQYXJhbWV0ZXJzQ29udHJvbHNDb250YWluZXIoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMub25GbHlPdXRTZWxlY3Rpb25DbGljayh7IHNob3dFbnRpdHlQYXJhbWV0ZXI6ICF0aGlzLnNob3dFbnRpdHlQYXJhbWV0ZXIgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25GbHlPdXRTZWxlY3Rpb25DbGljayhhcmdzKTogdm9pZCB7XHJcbiAgICB0aGlzLnJlc2V0U2V0dGluZ3MgPSBmYWxzZTtcclxuICAgIGlmICghdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZSB8fCAhT2JqZWN0LmtleXModGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZSkubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2UgPSB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuZ3JpZENvbXBvbmVudEluc3RhbmNlO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkR3JpZE9wdGlvbnMgPSB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnM7XHJcbiAgICB9XHJcbiAgICBpZiAodHlwZW9mIGFyZ3MuaXRlbSA9PT0gJ3VuZGVmaW5lZCcgJiYgYXJncy5zaG93RW50aXR5UGFyYW1ldGVyKSB7XHJcbiAgICAgIHRoaXMuc2hvd0VudGl0eVBhcmFtZXRlciA9IGFyZ3Muc2hvd0VudGl0eVBhcmFtZXRlcjtcclxuICAgICAgdGhpcy5jdXJyZW50U2VsZWN0ZWRGbHlPdXQgPSBuZXcgT2JqZWN0KCkgYXMgRmx5T3V0U2VjdGlvbjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2hvd0VudGl0eVBhcmFtZXRlciA9IGZhbHNlO1xyXG4gICAgICBpZiAoYXJncy5pdGVtICYmIGFyZ3MuaXRlbS5pZCA9PT0gdGhpcy5jdXJyZW50U2VsZWN0ZWRGbHlPdXQuaWQpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRTZWxlY3RlZEZseU91dCA9IG5ldyBPYmplY3QoKSBhcyBGbHlPdXRTZWN0aW9uO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFNlbGVjdGVkRmx5T3V0ID0gYXJncy5pdGVtIHx8IHt9O1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLnNldEdyaWRJbnN0YW5jZUxpc3QodGhpcy5zZWxlY3RlZEdyaWRPcHRpb25zKTtcclxuICAgIHRoaXMuc2V0Rmx5T3V0VGl0bGUoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgaGFuZGxlQ2VsbENsaWNrKGRhdGFHcmlkT3B0aW9uczogUEJJR3JpZE9wdGlvbnNNb2RlbCwgY2VsbE9iamVjdDogYW55KTogdm9pZCB7XHJcbiAgICB0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlID0gZGF0YUdyaWRPcHRpb25zLmdyaWRDb21wb25lbnRJbnN0YW5jZTtcclxuICAgIHRoaXMuc2VsZWN0ZWRHcmlkT3B0aW9ucyA9IGRhdGFHcmlkT3B0aW9ucztcclxuICAgIHRoaXMuc2VsZWN0ZWRFbnRpdHlJbmZvLmdyaWROYW1lID0gZGF0YUdyaWRPcHRpb25zLmdyaWROYW1lO1xyXG4gICAgdGhpcy5zZWxlY3RlZEVudGl0eUluZm8uY29sdW1uTGlzdCA9IGRhdGFHcmlkT3B0aW9ucy5jb2x1bW5zO1xyXG4gICAgdGhpcy5zZXRHcmlkSW5zdGFuY2VMaXN0KHRoaXMuc2VsZWN0ZWRHcmlkT3B0aW9ucyk7XHJcblxyXG4gICAgaWYgKGNlbGxPYmplY3QuY29sdW1uPy5kYXRhRmllbGQpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZENvbHVtbiA9IHtcclxuICAgICAgICAuLi5kYXRhR3JpZE9wdGlvbnMuY29sdW1ucy5maWx0ZXIoKGl0ZW0pID0+XHJcbiAgICAgICAgICBpdGVtLmRhdGFGaWVsZCA9PT0gY2VsbE9iamVjdC5jb2x1bW4uZGF0YUZpZWxkKVswXVxyXG4gICAgICB9O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZENvbHVtbiA9IG51bGw7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNldEdyaWRJbnN0YW5jZUxpc3QoY3VycmVudEdyaWRPcHRpb25zOiBQQklHcmlkT3B0aW9uc01vZGVsKTogdm9pZCB7XHJcbiAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZUxpc3QgPSBbXTtcclxuICAgIGNvbnN0IGdyaWROYW1lID0gY3VycmVudEdyaWRPcHRpb25zLmdyaWROYW1lIHx8IHRoaXMub3B0aW9ucy5ncmlkT3B0aW9ucy5ncmlkTmFtZTtcclxuICAgIGNvbnN0IGlzTWFzdGVyR3JpZFNlbGVjdGVkID0gdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmdyaWROYW1lID09PSBncmlkTmFtZSAmJiBjdXJyZW50R3JpZE9wdGlvbnMuaXNNYXN0ZXJHcmlkO1xyXG4gICAgLy8gdGhpcy5jb25kaXRpb25hbEZvcm1hdHRpbmdUaXRsZSA9IGlzTWFzdGVyR3JpZFNlbGVjdGVkID8gJ0NPTkRJVElPTkFMIEZPUk1BVFRJTkcnIDogJ0NPTkRJVElPTkFMIEZPUk1BVFRJTkcgLSBDSElMRCBWSUVXJztcclxuICAgIC8vIEFkZGluZyBNYXN0ZXIgZ3JpZCBpbnN0YW5jZVxyXG4gICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2VMaXN0LnB1c2goe1xyXG4gICAgICBpc01hc3RlckdyaWQ6IHRydWUsXHJcbiAgICAgIGdyaWROYW1lOiB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuZ3JpZE5hbWUsXHJcbiAgICAgIGdyaWRDb21wb25lbnRJbnN0YW5jZTogdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmdyaWRDb21wb25lbnRJbnN0YW5jZSxcclxuICAgICAgaXNTZWxlY3RlZDogaXNNYXN0ZXJHcmlkU2VsZWN0ZWRcclxuICAgIH0pO1xyXG4gICAgLy8gQWRkaW5nIGNoaWxkIGdyaWQgaW5zdGFuY2VzXHJcbiAgICBpZiAodGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmNoaWxkRW50aXR5TGlzdC5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmNoaWxkRW50aXR5TGlzdFswXS5jaGlsZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChjaGlsZEdyaWRJbnN0YW5jZSA9PiB7XHJcbiAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2VMaXN0LnB1c2goe1xyXG4gICAgICAgICAgaXNNYXN0ZXJHcmlkOiBmYWxzZSxcclxuICAgICAgICAgIGdyaWROYW1lOiB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuY2hpbGRFbnRpdHlMaXN0WzBdLmdyaWRPcHRpb25zLmdyaWROYW1lLFxyXG4gICAgICAgICAgZ3JpZENvbXBvbmVudEluc3RhbmNlOiBjaGlsZEdyaWRJbnN0YW5jZSxcclxuICAgICAgICAgIGlzU2VsZWN0ZWQ6ICFpc01hc3RlckdyaWRTZWxlY3RlZCxcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBjb25zdCBzZWxlY3RlZEdyaWRJbnN0YW5jZSA9IHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlTGlzdC5maWx0ZXIoaXRlbSA9PiBpdGVtLmlzU2VsZWN0ZWQgJiYgT2JqZWN0LmtleXMoaXRlbS5ncmlkQ29tcG9uZW50SW5zdGFuY2UpLmxlbmd0aCk7XHJcbiAgICBpZiAoc2VsZWN0ZWRHcmlkSW5zdGFuY2UubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuY29uZGl0aW9uYWxGb3JtYXR0aW5nU3RvcmFnZUtleSA9IGdldFN0b3JhZ2VLZXkoc2VsZWN0ZWRHcmlkSW5zdGFuY2VbMF0uZ3JpZENvbXBvbmVudEluc3RhbmNlLFxyXG4gICAgICAgIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmNvbmRpdGlvbmFsRm9ybWF0dGluZ10sIHNlbGVjdGVkR3JpZEluc3RhbmNlWzBdLmlzTWFzdGVyR3JpZCk7XHJcbiAgICAgIGlmIChDR0ZGbHlPdXRFbnVtLmNvbmRpdGlvbkZvcm1hdHRpbmcgPT09IHRoaXMuY3VycmVudFNlbGVjdGVkRmx5T3V0Py5pZCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRFbnRpdHlJbmZvLmNvbHVtbkxpc3QgPSB0aGlzLnByZXBhcmVMb29rVXBEYXRhRm9yQ29uZGl0b25hbEZvcm1hdHRpbmcodGhpcy5zZWxlY3RlZEVudGl0eUluZm8uY29sdW1uTGlzdCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMuY2FjaGluZ0VuYWJsZWQgPSBpc01hc3RlckdyaWRTZWxlY3RlZCA/IHRoaXMuaXNDYWNoZWQgOiBmYWxzZTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcHJlcGFyZUxvb2tVcERhdGFGb3JDb25kaXRvbmFsRm9ybWF0dGluZyhjb2x1bW5MaXN0OiBQQklHcmlkQ29sdW1uW10pOiBQQklHcmlkQ29sdW1uW10ge1xyXG4gICAgY29uc3QgYXJyOiBhbnkgPSBbXTtcclxuICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCBjb2x1bW5MaXN0Lmxlbmd0aDsgaW5kZXgrKykge1xyXG4gICAgICBjb2x1bW5MaXN0W2luZGV4XS5sb29rdXAgPSB7IGRhdGFTb3VyY2U6IHRoaXMuZ2V0RGF0YUxvb2tEYXRhU291cmNlKGNvbHVtbkxpc3RbaW5kZXhdLmRhdGFGaWVsZCkgfTtcclxuICAgICAgLy8gY29sdW1uTGlzdFtpbmRleF0uZmlsdGVyT3BlcmF0aW9ucyA9IGdldEZpbHRlck9wZXJhdGlvbnMoY29sdW1uTGlzdFtpbmRleF0uZGF0YVR5cGUpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGNvbHVtbkxpc3Q7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldERhdGFMb29rRGF0YVNvdXJjZShkYXRhRmllbGQ6IHN0cmluZyk6IEFycmF5PGFueT4ge1xyXG4gICAgY29uc3QgYXJyID0gW107XHJcbiAgICAodGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmRhdGFTb3VyY2UgYXMgQXJyYXk8YW55PikuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgaWYgKGFyci5pbmRleE9mKGl0ZW1bZGF0YUZpZWxkXSkgPT09IC0xKSB7XHJcbiAgICAgICAgYXJyLnB1c2goaXRlbVtkYXRhRmllbGRdKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICByZXR1cm4gWy4uLm5ldyBTZXQoYXJyKV07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHVwZGF0ZUNhY2hlZERhdGFTb3VyY2UoKTogdm9pZCB7XHJcbiAgICB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmJlZ2luVXBkYXRlKCk7XHJcbiAgICB0aGlzLmNhY2hlZENvbHVtbnMuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmdyaWRDb21wb25lbnRJbnN0YW5jZS5jb2x1bW5PcHRpb24oaXRlbS5kYXRhRmllbGQsICd2aXNpYmxlJywgaXRlbS52aXNpYmxlKTtcclxuICAgIH0pXHJcblxyXG4gICAgdGhpcy5vcHRpb25zLmdyaWRPcHRpb25zLmdyaWRDb21wb25lbnRJbnN0YW5jZS5vcHRpb24oJ2RhdGFTb3VyY2UnLCB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuZGF0YVNvdXJjZSk7XHJcbiAgICB0aGlzLm9wdGlvbnMuZ3JpZE9wdGlvbnMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcblxyXG4gIHByaXZhdGUgdHJ5VG9BcHBseUluaXRpYWxWaWV3cygpIHtcclxuICAgIGlmICh0aGlzLm9wdGlvbnMudmlld0RhdGFTb3VyY2UpIHtcclxuICAgICAgdGhpcy5vcHRpb25zLnZpZXdEYXRhU291cmNlLmdldFZpZXdzKHRoaXMub3B0aW9ucy52aWV3RGF0YVNvdXJjZS5rZXkpLnRoZW4obGF5b3V0cyA9PiB7XHJcbiAgICAgICAgdGhpcy5vcHRpb25zLnZpZXdMaXN0ID0gbGF5b3V0cztcclxuICAgICAgICBpZiAodGhpcy5pc1ZpZXdEZWZpbmVkSW5RdWVyeVBhcmFtcygpKSB7XHJcbiAgICAgICAgICB0aGlzLnRyeVRvQXBwbHlRdWVyeVBhcmFtTGF5b3V0KGxheW91dHMpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnRyeVRvQXBwbHlEZWZhdWx0TGF5b3V0KGxheW91dHMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSkuY2F0Y2goKCkgPT4ge1xyXG4gICAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ2Vycm9yJywgbWVzc2FnZTogJ1VuYWJsZSB0byByZXRyaWV2ZSB0aGUgbGF5b3V0cyBmb3IgdGhpcyBncmlkJyB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHRyeVRvQXBwbHlRdWVyeVBhcmFtTGF5b3V0KHZpZXdzOiBWaWV3U2VsZWN0aW9uTW9kZWxbXSkge1xyXG4gICAgY29uc3QgcXVlcnlQYXJhbXM6IFBhcmFtcyA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuYWN0aXZhdGVkUm91dGUuc25hcHNob3QucXVlcnlQYXJhbXMpO1xyXG4gICAgaWYgKHRoaXMuaXNWaWV3RGVmaW5lZEluUXVlcnlQYXJhbXMoKSkge1xyXG4gICAgICBjb25zdCBxdWVyeVZpZXcgPSB2aWV3cy5maW5kKGYgPT4gZi5pZCA9PT0gTnVtYmVyKHF1ZXJ5UGFyYW1zLmxheW91dCkpO1xyXG4gICAgICB0aGlzLm9wdGlvbnMudmlld0RhdGFTb3VyY2UuYXBwbHlWaWV3KHF1ZXJ5VmlldykudGhlbihncmlkVmlldyA9PiB7XHJcbiAgICAgICAgdGhpcy52aWV3RXZlbnRzKHsgZXZlbnQ6IENHRkV2ZW50c0VudW0uYXBwbHlWaWV3LCBkYXRhOiBncmlkVmlldyB9KTtcclxuICAgICAgfSkuY2F0Y2goKCkgPT4ge1xyXG4gICAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ2Vycm9yJywgbWVzc2FnZTogYFRoZSBsYXlvdXQgd2l0aCBJRDogJHtxdWVyeVBhcmFtcy5sYXlvdXR9IHdhcyBub3QgYXBwbGllZCBzdWNjZXNzZnVsbHlgIH0pO1xyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBpc1ZpZXdEZWZpbmVkSW5RdWVyeVBhcmFtcygpOiBib29sZWFuIHtcclxuICAgIGNvbnN0IHF1ZXJ5UGFyYW1zID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5xdWVyeVBhcmFtcztcclxuICAgIHJldHVybiBxdWVyeVBhcmFtcy5sYXlvdXQgJiYgcXVlcnlQYXJhbXMubGF5b3V0ID4gMDtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgdHJ5VG9BcHBseURlZmF1bHRMYXlvdXQodmlld3M6IFZpZXdTZWxlY3Rpb25Nb2RlbFtdKSB7XHJcbiAgICBjb25zdCBkZWZhdWx0VmlldyA9IHRoaXMuZ2V0RGVmYXVsdFZpZXcodmlld3MpO1xyXG4gICAgaWYgKGRlZmF1bHRWaWV3KSB7XHJcbiAgICAgIHRoaXMub3B0aW9ucy52aWV3RGF0YVNvdXJjZS5hcHBseVZpZXcoZGVmYXVsdFZpZXcpLnRoZW4oZ3JpZFZpZXcgPT4ge1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5UGFyYW1zOiBQYXJhbXMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmFjdGl2YXRlZFJvdXRlLnNuYXBzaG90LnF1ZXJ5UGFyYW1zKTtcclxuICAgICAgICBxdWVyeVBhcmFtcy5sYXlvdXQgPSBncmlkVmlldy5pZDtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXSwgeyBxdWVyeVBhcmFtcyB9KTtcclxuICAgICAgICB0aGlzLnZpZXdFdmVudHMoeyBldmVudDogQ0dGRXZlbnRzRW51bS5hcHBseVZpZXcsIGRhdGE6IGdyaWRWaWV3IH0pO1xyXG4gICAgICB9KS5jYXRjaCgoKSA9PiB7XHJcbiAgICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnZXJyb3InLCBtZXNzYWdlOiAnVGhlIGRlZmF1bHQgbGF5b3V0IHdhcyBub3QgYXBwbGllZCBzdWNjZXNzZnVsbHknIH0pO1xyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXREZWZhdWx0Vmlldyh2aWV3czogVmlld1NlbGVjdGlvbk1vZGVsW10pOiBWaWV3U2VsZWN0aW9uTW9kZWwge1xyXG4gICAgcmV0dXJuIHZpZXdzLmZpbHRlcih2aWV3ID0+IHZpZXcuaXNEZWZhdWx0IHx8IHZpZXcuaXNHbG9iYWxEZWZhdWx0KVswXSB8fCB2aWV3c1swXSB8fCBudWxsO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzZXRGbHlPdXRUaXRsZSgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmN1cnJlbnRTZWxlY3RlZEZseU91dCkge1xyXG4gICAgICBjb25zdCBpc01hc3RlclNlbGVjdGVkID0gdGhpcy5zZWxlY3RlZEdyaWRPcHRpb25zLmlzTWFzdGVyR3JpZDtcclxuICAgICAgc3dpdGNoICh0aGlzLmN1cnJlbnRTZWxlY3RlZEZseU91dC5pZCkge1xyXG4gICAgICAgIGNhc2UgQ0dGRmx5T3V0RW51bS5jb25kaXRpb25Gb3JtYXR0aW5nOlxyXG4gICAgICAgICAgdGhpcy5mbHlPdXRUaXRsZSA9IGlzTWFzdGVyU2VsZWN0ZWQgPyAnQ09ORElUSU9OQUwgRk9STUFUVElORycgOiAnQ09ORElUSU9OQUwgRk9STUFUVElORyAtIENISUxEIFZJRVcnO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSBDR0ZGbHlPdXRFbnVtLmN1c3RvbUNvbHVtbnM6XHJcbiAgICAgICAgICB0aGlzLmZseU91dFRpdGxlID0gaXNNYXN0ZXJTZWxlY3RlZCA/ICdDVVNUT00gQ09MVU1OJyA6ICdDVVNUT00gQ09MVU1OIC0gQ0hJTEQgVklFVyc7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5mbHlPdXRUaXRsZSA9ICcnO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=