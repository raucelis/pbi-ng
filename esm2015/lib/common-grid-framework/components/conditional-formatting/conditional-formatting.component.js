import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { CGFStorageKeys } from '../../utilities/enums';
import DataSource from 'devextreme/data/data_source';
import { ColorFormatComponent } from '../color-format/color-format.component';
import { appToast } from '../../utilities/utilityFunctions';
let ConditionalFormattingComponent = class ConditionalFormattingComponent {
    constructor() {
        this.groupOperations = ['and', 'or'];
        this.title = 'CONDITIONAL FORMATTING';
        this.valueChange = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this._listOfColumns = [];
        this._storageKey = CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
        this.applyType = 'Column';
        this.conditionFormattingControlVisible = false;
        this.copyOfListItems = [];
        this.filterApplyTypeCollection = ['Column', 'Row'];
        this.filterValue = null;
        this.hideColumnDropdown = true;
        this.listOfConditionalFormatting = [];
        this.operationOnColumn = '';
        this.selectedColumnData = null;
        this.selectedCondition = [];
    }
    set listOfColumns(colInfo) {
        this._listOfColumns = [];
        this.copyOfListItems = colInfo;
        colInfo.forEach(item => { this._listOfColumns.push({ dataField: item.dataField, caption: item.caption, dataType: item.dataType }); });
    }
    get listOfColumns() { return this.copyOfListItems; }
    set storageKey(key) {
        this._storageKey = key || CGFStorageKeys[CGFStorageKeys.conditionalFormatting];
        this.ngOnInit();
    }
    ngOnInit() {
        this.hideFormattingInfo();
        this.listOfConditionalFormatting = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
    }
    ngOnDestroy() {
        this.listOfColumns.forEach(item => item.lookup = null);
    }
    addConditionalFormatting() {
        this.resetInfo();
        this.showConditionalFormatting();
    }
    editConditionalFormatting() {
        var _a;
        if ((_a = this.selectedCondition) === null || _a === void 0 ? void 0 : _a.length) {
            this.showConditionalFormatting();
        }
    }
    hideFormattingInfo() {
        this.conditionFormattingControlVisible = false;
        this.resetInfo();
    }
    updateConditionalFormatting() {
        if (this.operationOnColumn.trim() === '' && this.applyType.toLowerCase() === 'column') {
            appToast({ type: 'warning', message: `Please select 'Apply To Column'.` });
            return;
        }
        // this._dataSourceFilterValue = this.formatValue(this.filterValue);
        const currentFormatting = this.columnFormatComponent.formatObject;
        const existingFormattingInfo = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
        if (existingFormattingInfo.length && this.selectedCondition.length) { // added this condition which informs whether adding or editing condition
            for (let index = 0; index < existingFormattingInfo.length; index++) {
                if (existingFormattingInfo[index].formatRuleName === this.selectedCondition[0].formatRuleName) {
                    existingFormattingInfo[index] = currentFormatting;
                    existingFormattingInfo[index].condition = this.filterValue;
                    existingFormattingInfo[index].dataField = this.applyType.toLowerCase() === 'column' ? this.operationOnColumn : '___dataFieldForRow___'; // Adding dummy dataField for rowType
                    existingFormattingInfo[index].applyType = this.applyType.toLowerCase();
                    existingFormattingInfo[index].formatRuleName = this.getFormatName(this.title, index + 1);
                    break;
                }
            }
        }
        else {
            currentFormatting.condition = this.filterValue;
            currentFormatting.dataField = this.applyType.toLowerCase() === 'column' ? this.operationOnColumn : '___dataFieldForRow___'; // Adding dummy dataField for rowType
            currentFormatting.formatRuleName = this.getFormatName(this.title);
            currentFormatting.applyType = this.applyType.toLowerCase();
            existingFormattingInfo.push(currentFormatting);
        }
        sessionStorage.setItem(this._storageKey, JSON.stringify(existingFormattingInfo));
        this.prepareListOfConditions();
        this.hideFormattingInfo();
        setTimeout(() => { this.forceToApplyFormats(); }, 100); // will update list on UI then forced refresh.
    }
    forceToApplyFormats() {
        this.valueChange.emit({ data: JSON.parse(sessionStorage.getItem(this._storageKey)) });
    }
    onConditionalItemDeleted(e) {
        const _existingFormats = JSON.parse(sessionStorage.getItem(this._storageKey));
        const deletedItemIndex = _existingFormats.findIndex(item => item.formatRuleName === e.itemData.formatRuleName && item.dataField === e.itemData.dataField);
        if (deletedItemIndex > -1) {
            if (deletedItemIndex + 1 < _existingFormats.length) {
                _existingFormats.forEach((arrItem, index) => {
                    if (index > deletedItemIndex) {
                        arrItem.formatRuleName = this.getFormatName(this.title, index, arrItem.applyType, arrItem.dataField);
                    }
                });
            }
            _existingFormats.splice(deletedItemIndex, 1);
            sessionStorage.setItem(this._storageKey, JSON.stringify(_existingFormats));
            this.prepareListOfConditions();
            this.hideFormattingInfo();
            setTimeout(() => { this.forceToApplyFormats(); }, 100); // will update list on UI then forced refresh.
        }
    }
    showConditionalFormatting() {
        var _a, _b, _c, _d;
        this.operationOnColumn = ((_a = this.selectedCondition[0]) === null || _a === void 0 ? void 0 : _a.dataField) || this.operationOnColumn;
        this.conditionFormattingControlVisible = true;
        this.filterValue = (_b = this.selectedCondition[0]) === null || _b === void 0 ? void 0 : _b.condition;
        this.selectedColumnData = this.selectedCondition[0] ? Object.assign({}, this.selectedCondition[0]) : Object.assign({
            dataField: '__null__',
            backgroundColor: '',
            condition: '',
            cssClass: ((_d = (_c = this.selectedColumnData) === null || _c === void 0 ? void 0 : _c.cssClass) === null || _d === void 0 ? void 0 : _d.replace('bold', '').replace('underline', '').replace('italic', '')) || '',
            formatRuleName: '',
            textColor: '',
        });
        if (this.selectedColumnData.applyType) {
            this.applyType = this.selectedColumnData.applyType.charAt(0).toUpperCase() + this.selectedColumnData.applyType.slice(1);
        }
        else {
            this.applyType = this.filterApplyTypeCollection[0]; // Setting column
        }
        this.onApplyTypeChange();
    }
    prepareListOfConditions() {
        const conditionalFormats = JSON.parse(sessionStorage.getItem(this._storageKey)) || [];
        this.listOfConditionalFormatting = conditionalFormats || [];
    }
    onFormatSelectionChanged() { this.editConditionalFormatting(); }
    resetInfo() {
        this.selectedCondition = [];
        this.selectedColumnData = null;
        this.operationOnColumn = '';
        this.hideColumnDropdown = true;
    }
    onApplyTypeChange() {
        this.hideColumnDropdown = this.applyType.toLowerCase() === 'column';
        if (!this.hideColumnDropdown) {
            this.operationOnColumn = '';
        }
    }
    getFormatName(entityName, counter = this.listOfConditionalFormatting.length + 1, selectedApplyType = this.applyType, selectedOperationOnColumn = this.operationOnColumn) {
        selectedApplyType = selectedApplyType.toLowerCase() === 'row' ? selectedApplyType : selectedOperationOnColumn;
        return `${entityName.length ? entityName + ': ' : ''}${selectedApplyType} Format Rule ${counter}`;
    }
    onEditorPreparing(e) {
        var _a;
        // Dynamic control change based on the operation to be done.
        const filterLookUp = ((_a = this.listOfColumns.filter(item => item.dataField === e.dataField)[0]) === null || _a === void 0 ? void 0 : _a.lookup) || null;
        if (e.filterOperation === '=' && filterLookUp) {
            e.editorName = 'dxSelectBox';
            e.editorOptions.searchEnabled = true;
            if (filterLookUp) {
                e.editorOptions.dataSource = new DataSource({ store: filterLookUp.dataSource });
            }
            e.editorOptions.onValueChanged = (event) => {
                e.setValue(event.value);
            };
        }
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
};
__decorate([
    ViewChild(ColorFormatComponent)
], ConditionalFormattingComponent.prototype, "columnFormatComponent", void 0);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "listOfColumns", null);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "groupOperations", void 0);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "storageKey", null);
__decorate([
    Input()
], ConditionalFormattingComponent.prototype, "title", void 0);
__decorate([
    Output()
], ConditionalFormattingComponent.prototype, "valueChange", void 0);
__decorate([
    Output()
], ConditionalFormattingComponent.prototype, "closeCurrentFlyOut", void 0);
ConditionalFormattingComponent = __decorate([
    Component({
        selector: 'pbi-conditional-formatting',
        template: "<div class=\"conditional-formatting-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-tools title-icon\"></i>\r\n            <div class=\"section-title\">{{title}}</div>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"widget-container accordion-data-container\">\r\n        <span class=\"add-icon-container\">\r\n            <i class=\"fas fa-plus fly-out-round-add-icon pointer\" (click)=\"addConditionalFormatting()\"></i>\r\n            <div class=\"add-rule-text\">Add Rules</div>\r\n        </span>\r\n        <span class=\"rule-list-container\" [class.dynamic-rule-list]=\"conditionFormattingControlVisible\">\r\n            <dx-list [items]=\"listOfConditionalFormatting\" allowItemDeleting=\"true\" itemDeleteMode=\"toggle\"\r\n                selectionMode=\"single\" keyExpr=\"formatRuleName\" displayExpr=\"formatRuleName\" searchExpr=\"formatRuleName\"\r\n                searchMode=\"contains\" [(selectedItems)]=\"selectedCondition\" [searchEnabled]=\"false\"\r\n                (onItemDeleted)=\"onConditionalItemDeleted($event)\" (onSelectionChanged)=\"onFormatSelectionChanged()\">\r\n                <!-- <dxo-item-dragging [data]=\"listOfConditionalFormatting\" [allowReordering]=\"true\" [onDragStart]=\"onDragStart\"\r\n                    [onDragEnd]=\"onDragEnd\"></dxo-item-dragging> -->\r\n                <div *dxTemplate=\"let data of 'item'\">\r\n                    <div class=\"list-text-wrap\">{{data.formatRuleName}}</div>\r\n                </div>\r\n            </dx-list>\r\n        </span>\r\n        <div *ngIf=\"conditionFormattingControlVisible\">\r\n            <div class=\"apply-format\">\r\n                <span class=\"action-container\">\r\n                    <!-- <i class=\"pointer\" (click)=\"updateConditionalFormatting()\"> Save </i>\r\n                <i class=\"fas fa-times pointer\" flow=\"down\" data-tooltip=\"Close\" (click)=\"hideFormattingInfo()\"></i> -->\r\n                </span>\r\n                <div class=\"applyTypeContainer\">\r\n                    <span class=\"label-padding\">Apply Type(Row/Column)</span>\r\n                    <dx-radio-group class=\"radioCollection\" [items]=\"filterApplyTypeCollection\" [(value)]=\"applyType\"\r\n                        layout=\"horizontal\" (onValueChanged)=\"onApplyTypeChange()\">\r\n                    </dx-radio-group>\r\n                </div>\r\n                <span *ngIf=\"hideColumnDropdown\">\r\n                    <div class=\"label-padding\">Apply To Column </div>\r\n                    <div class=\"apply-column\">\r\n                        <dx-select-box class=\"input-element column-list\" [dataSource]=\"_listOfColumns\"\r\n                            valueExpr=\"dataField\" displayExpr=\"caption\" [(value)]=\"operationOnColumn\"\r\n                            searchMode=\"contains\" searchEnabled=\"true\">\r\n                        </dx-select-box>\r\n                    </div>\r\n                </span>\r\n                <div class=\"conditional-filter\">\r\n                    <dx-filter-builder class=\"filter-container\" [disabled]=\"!conditionFormattingControlVisible\"\r\n                        [fields]=\"_listOfColumns\" [(value)]=\"filterValue\" [groupOperations]=\"groupOperations\"\r\n                        (onEditorPreparing)=\"onEditorPreparing($event)\">\r\n                    </dx-filter-builder>\r\n                </div>\r\n            </div>\r\n            <pbi-color-format [storageKey]=\"_storageKey\" [enableInputDataFieldCheck]=\"true\" [showBasicFormat]=\"true\"\r\n                [enableInputCaptionField]=\"false\" [enableFormat]=\"false\" [enablePin]=\"false\" [enableAlignment]=\"false\"\r\n                [enableStorage]=\"false\" [selectedColumnInfo]=\"selectedColumnData\">\r\n            </pbi-color-format>\r\n        </div>\r\n    </div>\r\n    <span class=\"button-container\" *ngIf=\"conditionFormattingControlVisible\">\r\n        <dx-button class=\"save-button\" text=\"Save\" type=\"default\" (onClick)=\"updateConditionalFormatting()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    })
], ConditionalFormattingComponent);
export { ConditionalFormattingComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZGl0aW9uYWwtZm9ybWF0dGluZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29tbW9uLWdyaWQtZnJhbWV3b3JrL2NvbXBvbmVudHMvY29uZGl0aW9uYWwtZm9ybWF0dGluZy9jb25kaXRpb25hbC1mb3JtYXR0aW5nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3ZELE9BQU8sVUFBVSxNQUFNLDZCQUE2QixDQUFDO0FBQ3JELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRzlFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQU81RCxJQUFhLDhCQUE4QixHQUEzQyxNQUFhLDhCQUE4QjtJQStCekM7UUFyQmdCLG9CQUFlLEdBQWEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFLMUMsVUFBSyxHQUFHLHdCQUF3QixDQUFDO1FBQ2hDLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNqQyx1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3pELG1CQUFjLEdBQW9CLEVBQUUsQ0FBQztRQUNyQyxnQkFBVyxHQUFHLGNBQWMsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUNuRSxjQUFTLEdBQUcsUUFBUSxDQUFDO1FBQ3JCLHNDQUFpQyxHQUFHLEtBQUssQ0FBQztRQUMxQyxvQkFBZSxHQUFHLEVBQUUsQ0FBQztRQUNyQiw4QkFBeUIsR0FBRyxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM5QyxnQkFBVyxHQUFHLElBQUksQ0FBQztRQUNuQix1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDMUIsZ0NBQTJCLEdBQUcsRUFBRSxDQUFDO1FBQ2pDLHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUN2Qix1QkFBa0IsR0FBaUIsSUFBSSxDQUFDO1FBQ3hDLHNCQUFpQixHQUFtQixFQUFFLENBQUM7SUFFdkIsQ0FBQztJQTVCUixJQUFJLGFBQWEsQ0FBQyxPQUF3QjtRQUNqRCxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQztRQUMvQixPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN4SSxDQUFDO0lBQ0QsSUFBSSxhQUFhLEtBQUssT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQztJQUczQyxJQUFJLFVBQVUsQ0FBQyxHQUFXO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxJQUFJLGNBQWMsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUMvRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQW1CRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLDJCQUEyQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEcsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUdNLHdCQUF3QjtRQUM3QixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVNLHlCQUF5Qjs7UUFDOUIsVUFBSSxJQUFJLENBQUMsaUJBQWlCLDBDQUFFLE1BQU0sRUFBRTtZQUNsQyxJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztTQUNsQztJQUNILENBQUM7SUFFTSxrQkFBa0I7UUFDdkIsSUFBSSxDQUFDLGlDQUFpQyxHQUFHLEtBQUssQ0FBQztRQUMvQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVNLDJCQUEyQjtRQUNoQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLEVBQUU7WUFDckYsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsa0NBQWtDLEVBQUUsQ0FBQyxDQUFDO1lBQzNFLE9BQU87U0FDUjtRQUNELG9FQUFvRTtRQUNwRSxNQUFNLGlCQUFpQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUM7UUFDbEUsTUFBTSxzQkFBc0IsR0FBZSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3RHLElBQUksc0JBQXNCLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsRUFBRSx5RUFBeUU7WUFDN0ksS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLHNCQUFzQixDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDbEUsSUFBSSxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxjQUFjLEtBQUssSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsRUFBRTtvQkFDN0Ysc0JBQXNCLENBQUMsS0FBSyxDQUFDLEdBQUcsaUJBQWlCLENBQUM7b0JBQ2xELHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO29CQUMzRCxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxxQ0FBcUM7b0JBQzdLLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN2RSxzQkFBc0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDekYsTUFBTTtpQkFDUDthQUNGO1NBQ0Y7YUFBTTtZQUNMLGlCQUFpQixDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQy9DLGlCQUFpQixDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLHFDQUFxQztZQUNqSyxpQkFBaUIsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDbEUsaUJBQWlCLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDM0Qsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDaEQ7UUFDRCxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7UUFDL0IsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDMUIsVUFBVSxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsOENBQThDO0lBQ3hHLENBQUM7SUFFTSxtQkFBbUI7UUFDeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN4RixDQUFDO0lBRU0sd0JBQXdCLENBQUMsQ0FBQztRQUMvQixNQUFNLGdCQUFnQixHQUFlLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUMxRixNQUFNLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzFKLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLEVBQUU7WUFDekIsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLEdBQUcsZ0JBQWdCLENBQUMsTUFBTSxFQUFFO2dCQUNsRCxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUU7b0JBQzFDLElBQUksS0FBSyxHQUFHLGdCQUFnQixFQUFFO3dCQUM1QixPQUFPLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7cUJBQ3RHO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFDRCxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDN0MsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1lBQzNFLElBQUksQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1lBQy9CLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1lBQzFCLFVBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLDhDQUE4QztTQUN2RztJQUNILENBQUM7SUFFTyx5QkFBeUI7O1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxPQUFBLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsMENBQUUsU0FBUyxLQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUN4RixJQUFJLENBQUMsaUNBQWlDLEdBQUcsSUFBSSxDQUFDO1FBQzlDLElBQUksQ0FBQyxXQUFXLFNBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQywwQ0FBRSxTQUFTLENBQUM7UUFDeEQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFNLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsRUFBRyxDQUFDLGVBRWpGO1lBQ0QsU0FBUyxFQUFFLFVBQVU7WUFDckIsZUFBZSxFQUFFLEVBQUU7WUFDbkIsU0FBUyxFQUFFLEVBQUU7WUFDYixRQUFRLEVBQUUsYUFBQSxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLFFBQVEsMENBQUUsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLEVBQUUsT0FBTyxDQUFDLFFBQVEsRUFBRSxFQUFFLE1BQUssRUFBRTtZQUNySCxjQUFjLEVBQUUsRUFBRTtZQUNsQixTQUFTLEVBQUUsRUFBRTtTQUNkLENBQ0YsQ0FBQztRQUNKLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsRUFBRTtZQUNyQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3pIO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQjtTQUN0RTtRQUNELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFTyx1QkFBdUI7UUFDN0IsTUFBTSxrQkFBa0IsR0FBZSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2xHLElBQUksQ0FBQywyQkFBMkIsR0FBRyxrQkFBa0IsSUFBSSxFQUFFLENBQUM7SUFDOUQsQ0FBQztJQUVNLHdCQUF3QixLQUFXLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUVyRSxTQUFTO1FBQ2YsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQy9CLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztJQUNqQyxDQUFDO0lBRU0saUJBQWlCO1FBQ3RCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsQ0FBQztRQUNwRSxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzVCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7U0FDN0I7SUFDSCxDQUFDO0lBRU8sYUFBYSxDQUFDLFVBQWtCLEVBQUUsVUFBa0IsSUFBSSxDQUFDLDJCQUEyQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQ3JHLGlCQUFpQixHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUUseUJBQXlCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQjtRQUN0RixpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyx5QkFBeUIsQ0FBQztRQUM5RyxPQUFPLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxHQUFHLGlCQUFpQixnQkFBZ0IsT0FBTyxFQUFFLENBQUM7SUFDcEcsQ0FBQztJQUVNLGlCQUFpQixDQUFDLENBQUM7O1FBQ3hCLDREQUE0RDtRQUM1RCxNQUFNLFlBQVksR0FBRyxPQUFBLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLDBDQUFFLE1BQU0sS0FBSSxJQUFJLENBQUM7UUFDMUcsSUFBSSxDQUFDLENBQUMsZUFBZSxLQUFLLEdBQUcsSUFBSSxZQUFZLEVBQUU7WUFDN0MsQ0FBQyxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUM7WUFDN0IsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLElBQUksWUFBWSxFQUFFO2dCQUNoQixDQUFDLENBQUMsYUFBYSxDQUFDLFVBQVUsR0FBRyxJQUFJLFVBQVUsQ0FBQyxFQUFFLEtBQUssRUFBRSxZQUFZLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQzthQUNqRjtZQUNELENBQUMsQ0FBQyxhQUFhLENBQUMsY0FBYyxHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ3pDLENBQUMsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQztTQUNIO0lBQ0gsQ0FBQztJQUVNLFdBQVc7UUFDaEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7Q0FDRixDQUFBO0FBckxrQztJQUFoQyxTQUFTLENBQUMsb0JBQW9CLENBQUM7NkVBQTZDO0FBRXBFO0lBQVIsS0FBSyxFQUFFO21FQUlQO0FBR1E7SUFBUixLQUFLLEVBQUU7dUVBQWtEO0FBQ2pEO0lBQVIsS0FBSyxFQUFFO2dFQUdQO0FBQ1E7SUFBUixLQUFLLEVBQUU7NkRBQXlDO0FBQ3ZDO0lBQVQsTUFBTSxFQUFFO21FQUF5QztBQUN4QztJQUFULE1BQU0sRUFBRTswRUFBZ0Q7QUFqQjlDLDhCQUE4QjtJQUwxQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsNEJBQTRCO1FBQ3RDLDBwSUFBc0Q7O0tBRXZELENBQUM7R0FDVyw4QkFBOEIsQ0FzTDFDO1NBdExZLDhCQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDR0ZTdG9yYWdlS2V5cyB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy9lbnVtcyc7XHJcbmltcG9ydCBEYXRhU291cmNlIGZyb20gJ2RldmV4dHJlbWUvZGF0YS9kYXRhX3NvdXJjZSc7XHJcbmltcG9ydCB7IENvbG9yRm9ybWF0Q29tcG9uZW50IH0gZnJvbSAnLi4vY29sb3ItZm9ybWF0L2NvbG9yLWZvcm1hdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb2x1bW5Gb3JtYXQgfSBmcm9tICcuLi9jb250cmFjdHMvY29sdW1uJztcclxuaW1wb3J0IHsgUEJJR3JpZENvbHVtbiB9IGZyb20gJy4uL2NvbnRyYWN0cy9ncmlkJztcclxuaW1wb3J0IHsgYXBwVG9hc3QgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvdXRpbGl0eUZ1bmN0aW9ucyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS1jb25kaXRpb25hbC1mb3JtYXR0aW5nJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29uZGl0aW9uYWwtZm9ybWF0dGluZy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29uZGl0aW9uYWwtZm9ybWF0dGluZy5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbmRpdGlvbmFsRm9ybWF0dGluZ0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQFZpZXdDaGlsZChDb2xvckZvcm1hdENvbXBvbmVudCkgY29sdW1uRm9ybWF0Q29tcG9uZW50OiBDb2xvckZvcm1hdENvbXBvbmVudDtcclxuXHJcbiAgQElucHV0KCkgc2V0IGxpc3RPZkNvbHVtbnMoY29sSW5mbzogUEJJR3JpZENvbHVtbltdKSB7XHJcbiAgICB0aGlzLl9saXN0T2ZDb2x1bW5zID0gW107XHJcbiAgICB0aGlzLmNvcHlPZkxpc3RJdGVtcyA9IGNvbEluZm87XHJcbiAgICBjb2xJbmZvLmZvckVhY2goaXRlbSA9PiB7IHRoaXMuX2xpc3RPZkNvbHVtbnMucHVzaCh7IGRhdGFGaWVsZDogaXRlbS5kYXRhRmllbGQsIGNhcHRpb246IGl0ZW0uY2FwdGlvbiwgZGF0YVR5cGU6IGl0ZW0uZGF0YVR5cGUgfSk7IH0pO1xyXG4gIH1cclxuICBnZXQgbGlzdE9mQ29sdW1ucygpIHsgcmV0dXJuIHRoaXMuY29weU9mTGlzdEl0ZW1zOyB9XHJcblxyXG4gIEBJbnB1dCgpIHB1YmxpYyBncm91cE9wZXJhdGlvbnM6IHN0cmluZ1tdID0gWydhbmQnLCAnb3InXTtcclxuICBASW5wdXQoKSBzZXQgc3RvcmFnZUtleShrZXk6IHN0cmluZykge1xyXG4gICAgdGhpcy5fc3RvcmFnZUtleSA9IGtleSB8fCBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5jb25kaXRpb25hbEZvcm1hdHRpbmddO1xyXG4gICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gIH1cclxuICBASW5wdXQoKSBwdWJsaWMgdGl0bGUgPSAnQ09ORElUSU9OQUwgRk9STUFUVElORyc7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyB2YWx1ZUNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGNsb3NlQ3VycmVudEZseU91dCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBfbGlzdE9mQ29sdW1uczogUEJJR3JpZENvbHVtbltdID0gW107XHJcbiAgX3N0b3JhZ2VLZXkgPSBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5jb25kaXRpb25hbEZvcm1hdHRpbmddO1xyXG4gIGFwcGx5VHlwZSA9ICdDb2x1bW4nO1xyXG4gIGNvbmRpdGlvbkZvcm1hdHRpbmdDb250cm9sVmlzaWJsZSA9IGZhbHNlO1xyXG4gIGNvcHlPZkxpc3RJdGVtcyA9IFtdO1xyXG4gIGZpbHRlckFwcGx5VHlwZUNvbGxlY3Rpb24gPSBbJ0NvbHVtbicsICdSb3cnXTtcclxuICBmaWx0ZXJWYWx1ZSA9IG51bGw7XHJcbiAgaGlkZUNvbHVtbkRyb3Bkb3duID0gdHJ1ZTtcclxuICBsaXN0T2ZDb25kaXRpb25hbEZvcm1hdHRpbmcgPSBbXTtcclxuICBvcGVyYXRpb25PbkNvbHVtbiA9ICcnO1xyXG4gIHNlbGVjdGVkQ29sdW1uRGF0YTogQ29sdW1uRm9ybWF0ID0gbnVsbDtcclxuICBzZWxlY3RlZENvbmRpdGlvbjogQ29sdW1uRm9ybWF0W10gPSBbXTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmhpZGVGb3JtYXR0aW5nSW5mbygpO1xyXG4gICAgdGhpcy5saXN0T2ZDb25kaXRpb25hbEZvcm1hdHRpbmcgPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0odGhpcy5fc3RvcmFnZUtleSkpIHx8IFtdO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLmxpc3RPZkNvbHVtbnMuZm9yRWFjaChpdGVtID0+IGl0ZW0ubG9va3VwID0gbnVsbCk7XHJcbiAgfVxyXG5cclxuXHJcbiAgcHVibGljIGFkZENvbmRpdGlvbmFsRm9ybWF0dGluZygpOiB2b2lkIHtcclxuICAgIHRoaXMucmVzZXRJbmZvKCk7XHJcbiAgICB0aGlzLnNob3dDb25kaXRpb25hbEZvcm1hdHRpbmcoKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBlZGl0Q29uZGl0aW9uYWxGb3JtYXR0aW5nKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRDb25kaXRpb24/Lmxlbmd0aCkge1xyXG4gICAgICB0aGlzLnNob3dDb25kaXRpb25hbEZvcm1hdHRpbmcoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBoaWRlRm9ybWF0dGluZ0luZm8oKTogdm9pZCB7XHJcbiAgICB0aGlzLmNvbmRpdGlvbkZvcm1hdHRpbmdDb250cm9sVmlzaWJsZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5yZXNldEluZm8oKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyB1cGRhdGVDb25kaXRpb25hbEZvcm1hdHRpbmcoKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5vcGVyYXRpb25PbkNvbHVtbi50cmltKCkgPT09ICcnICYmIHRoaXMuYXBwbHlUeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdjb2x1bW4nKSB7XHJcbiAgICAgIGFwcFRvYXN0KHsgdHlwZTogJ3dhcm5pbmcnLCBtZXNzYWdlOiBgUGxlYXNlIHNlbGVjdCAnQXBwbHkgVG8gQ29sdW1uJy5gIH0pO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICAvLyB0aGlzLl9kYXRhU291cmNlRmlsdGVyVmFsdWUgPSB0aGlzLmZvcm1hdFZhbHVlKHRoaXMuZmlsdGVyVmFsdWUpO1xyXG4gICAgY29uc3QgY3VycmVudEZvcm1hdHRpbmcgPSB0aGlzLmNvbHVtbkZvcm1hdENvbXBvbmVudC5mb3JtYXRPYmplY3Q7XHJcbiAgICBjb25zdCBleGlzdGluZ0Zvcm1hdHRpbmdJbmZvOiBBcnJheTxhbnk+ID0gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKHRoaXMuX3N0b3JhZ2VLZXkpKSB8fCBbXTtcclxuICAgIGlmIChleGlzdGluZ0Zvcm1hdHRpbmdJbmZvLmxlbmd0aCAmJiB0aGlzLnNlbGVjdGVkQ29uZGl0aW9uLmxlbmd0aCkgeyAvLyBhZGRlZCB0aGlzIGNvbmRpdGlvbiB3aGljaCBpbmZvcm1zIHdoZXRoZXIgYWRkaW5nIG9yIGVkaXRpbmcgY29uZGl0aW9uXHJcbiAgICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCBleGlzdGluZ0Zvcm1hdHRpbmdJbmZvLmxlbmd0aDsgaW5kZXgrKykge1xyXG4gICAgICAgIGlmIChleGlzdGluZ0Zvcm1hdHRpbmdJbmZvW2luZGV4XS5mb3JtYXRSdWxlTmFtZSA9PT0gdGhpcy5zZWxlY3RlZENvbmRpdGlvblswXS5mb3JtYXRSdWxlTmFtZSkge1xyXG4gICAgICAgICAgZXhpc3RpbmdGb3JtYXR0aW5nSW5mb1tpbmRleF0gPSBjdXJyZW50Rm9ybWF0dGluZztcclxuICAgICAgICAgIGV4aXN0aW5nRm9ybWF0dGluZ0luZm9baW5kZXhdLmNvbmRpdGlvbiA9IHRoaXMuZmlsdGVyVmFsdWU7XHJcbiAgICAgICAgICBleGlzdGluZ0Zvcm1hdHRpbmdJbmZvW2luZGV4XS5kYXRhRmllbGQgPSB0aGlzLmFwcGx5VHlwZS50b0xvd2VyQ2FzZSgpID09PSAnY29sdW1uJyA/IHRoaXMub3BlcmF0aW9uT25Db2x1bW4gOiAnX19fZGF0YUZpZWxkRm9yUm93X19fJzsgLy8gQWRkaW5nIGR1bW15IGRhdGFGaWVsZCBmb3Igcm93VHlwZVxyXG4gICAgICAgICAgZXhpc3RpbmdGb3JtYXR0aW5nSW5mb1tpbmRleF0uYXBwbHlUeXBlID0gdGhpcy5hcHBseVR5cGUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAgIGV4aXN0aW5nRm9ybWF0dGluZ0luZm9baW5kZXhdLmZvcm1hdFJ1bGVOYW1lID0gdGhpcy5nZXRGb3JtYXROYW1lKHRoaXMudGl0bGUsIGluZGV4ICsgMSk7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGN1cnJlbnRGb3JtYXR0aW5nLmNvbmRpdGlvbiA9IHRoaXMuZmlsdGVyVmFsdWU7XHJcbiAgICAgIGN1cnJlbnRGb3JtYXR0aW5nLmRhdGFGaWVsZCA9IHRoaXMuYXBwbHlUeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdjb2x1bW4nID8gdGhpcy5vcGVyYXRpb25PbkNvbHVtbiA6ICdfX19kYXRhRmllbGRGb3JSb3dfX18nOyAvLyBBZGRpbmcgZHVtbXkgZGF0YUZpZWxkIGZvciByb3dUeXBlXHJcbiAgICAgIGN1cnJlbnRGb3JtYXR0aW5nLmZvcm1hdFJ1bGVOYW1lID0gdGhpcy5nZXRGb3JtYXROYW1lKHRoaXMudGl0bGUpO1xyXG4gICAgICBjdXJyZW50Rm9ybWF0dGluZy5hcHBseVR5cGUgPSB0aGlzLmFwcGx5VHlwZS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICBleGlzdGluZ0Zvcm1hdHRpbmdJbmZvLnB1c2goY3VycmVudEZvcm1hdHRpbmcpO1xyXG4gICAgfVxyXG4gICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSh0aGlzLl9zdG9yYWdlS2V5LCBKU09OLnN0cmluZ2lmeShleGlzdGluZ0Zvcm1hdHRpbmdJbmZvKSk7XHJcbiAgICB0aGlzLnByZXBhcmVMaXN0T2ZDb25kaXRpb25zKCk7XHJcbiAgICB0aGlzLmhpZGVGb3JtYXR0aW5nSW5mbygpO1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7IHRoaXMuZm9yY2VUb0FwcGx5Rm9ybWF0cygpOyB9LCAxMDApOyAvLyB3aWxsIHVwZGF0ZSBsaXN0IG9uIFVJIHRoZW4gZm9yY2VkIHJlZnJlc2guXHJcbiAgfVxyXG5cclxuICBwdWJsaWMgZm9yY2VUb0FwcGx5Rm9ybWF0cygpOiB2b2lkIHtcclxuICAgIHRoaXMudmFsdWVDaGFuZ2UuZW1pdCh7IGRhdGE6IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSh0aGlzLl9zdG9yYWdlS2V5KSkgfSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Db25kaXRpb25hbEl0ZW1EZWxldGVkKGUpOiB2b2lkIHtcclxuICAgIGNvbnN0IF9leGlzdGluZ0Zvcm1hdHM6IEFycmF5PGFueT4gPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0odGhpcy5fc3RvcmFnZUtleSkpO1xyXG4gICAgY29uc3QgZGVsZXRlZEl0ZW1JbmRleCA9IF9leGlzdGluZ0Zvcm1hdHMuZmluZEluZGV4KGl0ZW0gPT4gaXRlbS5mb3JtYXRSdWxlTmFtZSA9PT0gZS5pdGVtRGF0YS5mb3JtYXRSdWxlTmFtZSAmJiBpdGVtLmRhdGFGaWVsZCA9PT0gZS5pdGVtRGF0YS5kYXRhRmllbGQpO1xyXG4gICAgaWYgKGRlbGV0ZWRJdGVtSW5kZXggPiAtMSkge1xyXG4gICAgICBpZiAoZGVsZXRlZEl0ZW1JbmRleCArIDEgPCBfZXhpc3RpbmdGb3JtYXRzLmxlbmd0aCkge1xyXG4gICAgICAgIF9leGlzdGluZ0Zvcm1hdHMuZm9yRWFjaCgoYXJySXRlbSwgaW5kZXgpID0+IHtcclxuICAgICAgICAgIGlmIChpbmRleCA+IGRlbGV0ZWRJdGVtSW5kZXgpIHtcclxuICAgICAgICAgICAgYXJySXRlbS5mb3JtYXRSdWxlTmFtZSA9IHRoaXMuZ2V0Rm9ybWF0TmFtZSh0aGlzLnRpdGxlLCBpbmRleCwgYXJySXRlbS5hcHBseVR5cGUsIGFyckl0ZW0uZGF0YUZpZWxkKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICBfZXhpc3RpbmdGb3JtYXRzLnNwbGljZShkZWxldGVkSXRlbUluZGV4LCAxKTtcclxuICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSh0aGlzLl9zdG9yYWdlS2V5LCBKU09OLnN0cmluZ2lmeShfZXhpc3RpbmdGb3JtYXRzKSk7XHJcbiAgICAgIHRoaXMucHJlcGFyZUxpc3RPZkNvbmRpdGlvbnMoKTtcclxuICAgICAgdGhpcy5oaWRlRm9ybWF0dGluZ0luZm8oKTtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7IHRoaXMuZm9yY2VUb0FwcGx5Rm9ybWF0cygpOyB9LCAxMDApOyAvLyB3aWxsIHVwZGF0ZSBsaXN0IG9uIFVJIHRoZW4gZm9yY2VkIHJlZnJlc2guXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHNob3dDb25kaXRpb25hbEZvcm1hdHRpbmcoKTogdm9pZCB7XHJcbiAgICB0aGlzLm9wZXJhdGlvbk9uQ29sdW1uID0gdGhpcy5zZWxlY3RlZENvbmRpdGlvblswXT8uZGF0YUZpZWxkIHx8IHRoaXMub3BlcmF0aW9uT25Db2x1bW47XHJcbiAgICB0aGlzLmNvbmRpdGlvbkZvcm1hdHRpbmdDb250cm9sVmlzaWJsZSA9IHRydWU7XHJcbiAgICB0aGlzLmZpbHRlclZhbHVlID0gdGhpcy5zZWxlY3RlZENvbmRpdGlvblswXT8uY29uZGl0aW9uO1xyXG4gICAgdGhpcy5zZWxlY3RlZENvbHVtbkRhdGEgPSB0aGlzLnNlbGVjdGVkQ29uZGl0aW9uWzBdID8geyAuLi50aGlzLnNlbGVjdGVkQ29uZGl0aW9uWzBdIH0gOlxyXG4gICAgICB7XHJcbiAgICAgICAgLi4ue1xyXG4gICAgICAgICAgZGF0YUZpZWxkOiAnX19udWxsX18nLFxyXG4gICAgICAgICAgYmFja2dyb3VuZENvbG9yOiAnJyxcclxuICAgICAgICAgIGNvbmRpdGlvbjogJycsXHJcbiAgICAgICAgICBjc3NDbGFzczogdGhpcy5zZWxlY3RlZENvbHVtbkRhdGE/LmNzc0NsYXNzPy5yZXBsYWNlKCdib2xkJywgJycpLnJlcGxhY2UoJ3VuZGVybGluZScsICcnKS5yZXBsYWNlKCdpdGFsaWMnLCAnJykgfHwgJycsXHJcbiAgICAgICAgICBmb3JtYXRSdWxlTmFtZTogJycsXHJcbiAgICAgICAgICB0ZXh0Q29sb3I6ICcnLFxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5hcHBseVR5cGUpIHtcclxuICAgICAgdGhpcy5hcHBseVR5cGUgPSB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5hcHBseVR5cGUuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YS5hcHBseVR5cGUuc2xpY2UoMSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmFwcGx5VHlwZSA9IHRoaXMuZmlsdGVyQXBwbHlUeXBlQ29sbGVjdGlvblswXTsgLy8gU2V0dGluZyBjb2x1bW5cclxuICAgIH1cclxuICAgIHRoaXMub25BcHBseVR5cGVDaGFuZ2UoKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcHJlcGFyZUxpc3RPZkNvbmRpdGlvbnMoKTogdm9pZCB7XHJcbiAgICBjb25zdCBjb25kaXRpb25hbEZvcm1hdHM6IEFycmF5PGFueT4gPSBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0odGhpcy5fc3RvcmFnZUtleSkpIHx8IFtdO1xyXG4gICAgdGhpcy5saXN0T2ZDb25kaXRpb25hbEZvcm1hdHRpbmcgPSBjb25kaXRpb25hbEZvcm1hdHMgfHwgW107XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25Gb3JtYXRTZWxlY3Rpb25DaGFuZ2VkKCk6IHZvaWQgeyB0aGlzLmVkaXRDb25kaXRpb25hbEZvcm1hdHRpbmcoKTsgfVxyXG5cclxuICBwcml2YXRlIHJlc2V0SW5mbygpOiB2b2lkIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRDb25kaXRpb24gPSBbXTtcclxuICAgIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhID0gbnVsbDtcclxuICAgIHRoaXMub3BlcmF0aW9uT25Db2x1bW4gPSAnJztcclxuICAgIHRoaXMuaGlkZUNvbHVtbkRyb3Bkb3duID0gdHJ1ZTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkFwcGx5VHlwZUNoYW5nZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuaGlkZUNvbHVtbkRyb3Bkb3duID0gdGhpcy5hcHBseVR5cGUudG9Mb3dlckNhc2UoKSA9PT0gJ2NvbHVtbic7XHJcbiAgICBpZiAoIXRoaXMuaGlkZUNvbHVtbkRyb3Bkb3duKSB7XHJcbiAgICAgIHRoaXMub3BlcmF0aW9uT25Db2x1bW4gPSAnJztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0Rm9ybWF0TmFtZShlbnRpdHlOYW1lOiBzdHJpbmcsIGNvdW50ZXI6IG51bWJlciA9IHRoaXMubGlzdE9mQ29uZGl0aW9uYWxGb3JtYXR0aW5nLmxlbmd0aCArIDEsXHJcbiAgICBzZWxlY3RlZEFwcGx5VHlwZSA9IHRoaXMuYXBwbHlUeXBlLCBzZWxlY3RlZE9wZXJhdGlvbk9uQ29sdW1uID0gdGhpcy5vcGVyYXRpb25PbkNvbHVtbik6IHN0cmluZyB7XHJcbiAgICBzZWxlY3RlZEFwcGx5VHlwZSA9IHNlbGVjdGVkQXBwbHlUeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdyb3cnID8gc2VsZWN0ZWRBcHBseVR5cGUgOiBzZWxlY3RlZE9wZXJhdGlvbk9uQ29sdW1uO1xyXG4gICAgcmV0dXJuIGAke2VudGl0eU5hbWUubGVuZ3RoID8gZW50aXR5TmFtZSArICc6ICcgOiAnJ30ke3NlbGVjdGVkQXBwbHlUeXBlfSBGb3JtYXQgUnVsZSAke2NvdW50ZXJ9YDtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkVkaXRvclByZXBhcmluZyhlKTogdm9pZCB7XHJcbiAgICAvLyBEeW5hbWljIGNvbnRyb2wgY2hhbmdlIGJhc2VkIG9uIHRoZSBvcGVyYXRpb24gdG8gYmUgZG9uZS5cclxuICAgIGNvbnN0IGZpbHRlckxvb2tVcCA9IHRoaXMubGlzdE9mQ29sdW1ucy5maWx0ZXIoaXRlbSA9PiBpdGVtLmRhdGFGaWVsZCA9PT0gZS5kYXRhRmllbGQpWzBdPy5sb29rdXAgfHwgbnVsbDtcclxuICAgIGlmIChlLmZpbHRlck9wZXJhdGlvbiA9PT0gJz0nICYmIGZpbHRlckxvb2tVcCkge1xyXG4gICAgICBlLmVkaXRvck5hbWUgPSAnZHhTZWxlY3RCb3gnO1xyXG4gICAgICBlLmVkaXRvck9wdGlvbnMuc2VhcmNoRW5hYmxlZCA9IHRydWU7XHJcbiAgICAgIGlmIChmaWx0ZXJMb29rVXApIHtcclxuICAgICAgICBlLmVkaXRvck9wdGlvbnMuZGF0YVNvdXJjZSA9IG5ldyBEYXRhU291cmNlKHsgc3RvcmU6IGZpbHRlckxvb2tVcC5kYXRhU291cmNlIH0pO1xyXG4gICAgICB9XHJcbiAgICAgIGUuZWRpdG9yT3B0aW9ucy5vblZhbHVlQ2hhbmdlZCA9IChldmVudCkgPT4ge1xyXG4gICAgICAgIGUuc2V0VmFsdWUoZXZlbnQudmFsdWUpO1xyXG4gICAgICB9O1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb3NlRmx5T3V0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jbG9zZUN1cnJlbnRGbHlPdXQuZW1pdCgpO1xyXG4gIH1cclxufVxyXG4iXX0=