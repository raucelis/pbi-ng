import { __decorate } from "tslib";
import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Subject, } from 'rxjs';
import { buffer, debounceTime } from 'rxjs/operators';
import { customActionColumnInfo } from '../../utilities/constants';
let ColumnChooserComponent = class ColumnChooserComponent {
    constructor() {
        this.refreshOnColumnUpdate = true;
        //#endregion Input Properties
        //#region Output Events
        this.applyButtonClick = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.selectedColumnData = {
            caption: '',
            columnId: -1,
            dataField: '',
            dataType: null,
            description: '',
            format: '',
            groupName: '',
            visible: true
        };
        this.bulkApplyColumnsLookup = {};
        this.columnFieldTitle = 'COLUMNS';
        this.columnSearchValue = '';
        this.columnsData = [];
        this.columnsSubject = new Subject();
        this.copyOfColumnList = [];
        this.isMasterGridSelected = true;
        this.selectAllChecked = false;
        this.selectedCols = [];
        this.selectedGridInstanceList = [];
        this.selectedGroupNames = [];
        this.showBasicFormat = JSON.parse(sessionStorage.getItem('basicFormatVisibility')) || false;
        this.showColumnList = true;
    }
    //#region Input Properties
    set columnsList(columnsList) {
        this.columnsData = [];
        this.selectedGroupNames = [];
        this.copyOfColumnList = [];
        columnsList.forEach(item => {
            this.columnsData.push(item);
            if (item.groupName && this.selectedGroupNames.indexOf(item.groupName) === -1) {
                this.selectedGroupNames.push(item.groupName);
            }
            this.copyOfColumnList.push(item);
        });
    }
    get columnsList() { return this.columnsData; }
    set selectedColumnInfo(columnItem) {
        this.selectedColumnData = columnItem;
        if (columnItem === null || columnItem === void 0 ? void 0 : columnItem.dataField) {
            this.onClickColumnItem(columnItem.dataField);
        }
        this.bringElementToViewArea((columnItem === null || columnItem === void 0 ? void 0 : columnItem.dataField) || '');
    }
    get selectedColumnInfo() { return this.selectedColumnData; }
    set gridInstanceList(gridInstanceList) {
        var _a;
        const gridInstances = gridInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length);
        this.selectedGridInstanceList = gridInstances.map(item => item.gridComponentInstance);
        if (gridInstanceList.length > 1 && ((_a = gridInstances[0]) === null || _a === void 0 ? void 0 : _a.gridName)) {
            this.isMasterGridSelected = gridInstances[0].isMasterGrid;
            this.columnFieldTitle = `COLUMNS${this.isMasterGridSelected ? '' : ' - Child View'}`.toUpperCase();
        }
    }
    get gridInstanceList() { return this.selectedGridInstanceList; }
    set debounce(num) {
        this.subscribeToColumnChanges(num);
    }
    ;
    //#endregion Output Events
    get checkMasterCheckBox() {
        const anyOneColumnHidden = this.columnsData.find(colItem => {
            return colItem.visible === false && colItem.dataField !== customActionColumnInfo.dataField;
        });
        return anyOneColumnHidden ? false : true;
    }
    set checkMasterCheckBox(args) { this.onClickMasterCheckBox(args); }
    //#region Angular Lifecycle Events
    ngOnInit() {
        if (this.selectedGridInstanceList.length) {
            setTimeout(() => {
                this.selectedGridInstanceList.forEach(grid => {
                    grid.columnOption(customActionColumnInfo.dataField, { visible: false });
                    sessionStorage.setItem('currentAppliedView', JSON.stringify(grid.state())); // holding state to make sure group-by works when we toggle selectAll
                });
            }, 0);
        }
    }
    ngOnDestroy() {
        this.selectedCols.forEach(col => {
            this.columnsData.filter(column => column.dataField === col.dataField)[0].visible = !col.visible;
        });
        const anyColumnVisible = this.columnsData.find(item => item.visible);
        if (this.selectedGridInstanceList.length) {
            this.selectedGridInstanceList.forEach(item => {
                item.columnOption(customActionColumnInfo.dataField, { visible: anyColumnVisible ? true : false });
            });
        }
        sessionStorage.removeItem('currentAppliedView');
        // Lets columns finish up rendering if any outstanding. Will unsubcribe itself when
        // destroyed parameter is set to true.
        this.destroyed = true;
        this.columnsSubject.next(null);
    }
    //#endregion Angular Lifecycle Events
    //#region Public Methods
    toggleColumnVisibility(dataField, visible) {
        var _a;
        if (this.isCached) {
            const colIndex = this.selectedCols.findIndex(data => data.dataField === dataField);
            if (colIndex !== -1) {
                if (visible) {
                    visible = !((_a = this.selectedGridInstanceList[0].getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.filter(col => col.hasOwnProperty('dataField') && col.dataField === dataField).length);
                }
                if (!visible) {
                    this.selectedCols.splice(colIndex, 1);
                }
            }
            else if (colIndex === -1) {
                this.selectedCols.push({ dataField, visible });
            }
        }
        else if (this.useBulkApply) {
            this.bulkApplyColumnsLookup[dataField.toLowerCase()] = { dataField, visible };
        }
        else {
            for (let index = 0; index < this.columnsData.length; index++) {
                if (dataField === this.columnsData[index].dataField) {
                    this.columnsData[index].visible = visible;
                    this.columnsSubject.next({ visible, dataField });
                    break;
                }
            }
        }
    }
    toggleGroupVisibility(event, groupName) {
        if (this.selectedGroupNames.indexOf(groupName) > -1) {
            this.selectedGroupNames = this.selectedGroupNames.filter(arr => arr !== groupName);
        }
        else {
            this.selectedGroupNames.push(groupName);
        }
    }
    onSearchColumnValueChange(args) {
        this.copyOfColumnList = JSON.parse(JSON.stringify(this.columnsData.filter(data => data.caption.toLowerCase().indexOf(args.value.toLowerCase()) > -1)));
        const visibleCol = this.selectedCols.filter(data => data.visible);
        if (visibleCol.length) {
            visibleCol.forEach(item => {
                this.copyOfColumnList.filter(data => data.dataField === item.dataField).map((col) => col.visible = true);
            });
        }
    }
    columnChooserClick(event) {
        const target = event.target;
        if (target && target.dataset) {
            switch (target.tagName.toLowerCase()) {
                case 'label':
                    this.onClickColumnItem(target.dataset.item);
                    break;
            }
        }
        event.stopPropagation();
    }
    onClickMasterCheckBox(val) {
        this.selectAllChecked = val;
        this.copyOfColumnList.forEach(item => {
            item.visible = val;
        });
        if (this.isCached) {
            this.columnsData.forEach(item => {
                if (item.dataField !== customActionColumnInfo.dataField) {
                    this.toggleColumnVisibility(item.dataField, val);
                }
            });
        }
        if (this.useBulkApply) {
            this.copyOfColumnList.forEach(item => {
                this.bulkApplyColumnsLookup[item.dataField.toLowerCase()] = { dataField: item.dataField, visible: item.visible };
            });
        }
        else {
            setTimeout(() => {
                this.copyOfColumnList.forEach(item => { item.visible = val; });
                this.selectedGridInstanceList.forEach(item => {
                    item.option('columns', []);
                });
                this.columnsData.forEach(item => {
                    if (item.dataField !== customActionColumnInfo.dataField) {
                        item.visible = val;
                    }
                });
                this.selectedGridInstanceList.forEach(item => {
                    item.option('columns', this.columnsData);
                });
                if (this.selectAllChecked) {
                    this.selectedGridInstanceList.forEach(item => {
                        item.state(JSON.parse(sessionStorage.getItem('currentAppliedView')));
                    });
                }
            }, 0);
        }
    }
    fetchCachedColumnsData() {
        if (this.selectedCols.length) {
            const actionColumn = this.columnsData.filter(data => data.dataField === customActionColumnInfo.dataField && data.groupName === customActionColumnInfo.groupName)[0];
            const visibleColumns = this.selectedCols.filter(data => data.visible).map(item => item.dataField);
            if (actionColumn) {
                this.selectedGridInstanceList.forEach(item => {
                    item.columnOption(actionColumn.dataField, { visible: visibleColumns.length ? true : false, fixed: true, fixedPosition: 'right' });
                });
            }
        }
        else {
            this.selectedGridInstanceList.forEach(gridInst => {
                gridInst.beginUpdate();
                this.columnsList.forEach(item => {
                    gridInst.columnOption(item.dataField, 'visible', false);
                });
                gridInst.option('dataSource', []);
                gridInst.endUpdate();
            });
        }
        // TODO: check enable and disable grid export. If not then remove below code.
        if ((this.selectedGridInstanceList[0].getVisibleColumns().filter(data => data.command !== 'empty').length) > 0) {
            this.selectedGridInstanceList[0].option('export.enabled', false);
        }
        this.applyButtonClick.emit(this.selectedCols);
        this.selectedCols = [];
    }
    formatOptionsChanged(args) {
        this.columnsData.forEach(col => {
            var _a;
            if (col.dataField === ((_a = this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField)) {
                col.caption = args.caption;
                this.selectedGridInstanceList.forEach(item => {
                    item.columnOption(col.dataField, { caption: col.caption });
                });
            }
        });
        this.copyOfColumnList.forEach(item => {
            var _a;
            if (item.dataField === ((_a = this.selectedColumnData) === null || _a === void 0 ? void 0 : _a.dataField)) {
                item.caption = args.caption;
            }
        });
    }
    onFormatToggled(val) {
        this.showBasicFormat = val;
        sessionStorage.setItem('basicFormatVisibility', JSON.stringify(val));
    }
    onBulkApplyClick() {
        this.bulkApplyColumns(Object.values(this.bulkApplyColumnsLookup));
        this.bulkApplyColumnsLookup = {};
        this.closeFlyOut();
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
    //#endregion Public Methods
    //#region Private Methods
    onClickColumnItem(dataField) {
        this.selectedColumnData = this.columnsData.filter(element => element.dataField.toLowerCase() === dataField.toLowerCase())[0];
    }
    bringElementToViewArea(dataField) {
        const dataFieldElem = window.document.querySelectorAll(`input[data-item='${dataField}']`);
        const container = window.document.getElementsByClassName('columnChooser-group-columns');
        if (container.length && dataFieldElem.length) {
            const topPos = dataFieldElem[0].offsetTop;
            container[0].scrollTop = topPos - (container[0].offsetTop + 10); // 10 is offset to enhance UX.
        }
    }
    subscribeToColumnChanges(debounce) {
        if (this.columnSubjectSubscription) {
            this.columnSubjectSubscription.unsubscribe();
        }
        this.columnSubjectSubscription = this.columnsSubject.pipe(buffer(this.columnsSubject.pipe(debounceTime(debounce))))
            .subscribe(fields => {
            fields = fields.filter(f => f);
            this.bulkApplyColumns(fields);
            if (this.destroyed) {
                this.columnSubjectSubscription.unsubscribe();
            }
        });
    }
    bulkApplyColumns(fields) {
        if (fields.length > 0) {
            this.selectedGridInstanceList.forEach(item => { item.beginCustomLoading('Loading'); item.beginUpdate(); });
            fields.forEach(f => {
                this.selectedGridInstanceList.forEach(item => item.columnOption(f.dataField, { visible: f.visible }));
            });
            if (this.refreshOnColumnUpdate) {
                this.selectedGridInstanceList.forEach(item => item.refresh(true));
            }
            else {
                this.selectedGridInstanceList.forEach(item => item.repaint());
            }
            this.selectedGridInstanceList.forEach(item => { item.endCustomLoading(); item.endUpdate(); });
        }
    }
};
__decorate([
    Input()
], ColumnChooserComponent.prototype, "columnsList", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "isCached", void 0);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "selectedColumnInfo", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "gridInstanceList", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "debounce", null);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "useBulkApply", void 0);
__decorate([
    Input()
], ColumnChooserComponent.prototype, "refreshOnColumnUpdate", void 0);
__decorate([
    Output()
], ColumnChooserComponent.prototype, "applyButtonClick", void 0);
__decorate([
    Output()
], ColumnChooserComponent.prototype, "closeCurrentFlyOut", void 0);
ColumnChooserComponent = __decorate([
    Component({
        selector: 'pbi-column-chooser',
        template: "<div class=\"column-chooser-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-cog title-icon\"></i>\r\n            <span class=\"section-title\">FIELDS</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"property-tab-in-column-chooser\" (click)=\"showColumnList = !showColumnList\">\r\n                <span>{{columnFieldTitle}}</span>\r\n                <i [class.fa-angle-down]=\"!showColumnList\" [class.fa-angle-up]=\"showColumnList\"\r\n                    class=\"fa basic-format-toggle-icon\"></i>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n\r\n    <div class=\"accordion-content columns-search-container\" *ngIf=\"showColumnList\">\r\n        <div class=\"form-group columns-search\">\r\n            <div class=\"text-container\">\r\n                <dx-text-box mode=\"search\" [(value)]=\"columnSearchValue\" valueChangeEvent=\"keyup\"\r\n                    (onValueChanged)=\"onSearchColumnValueChange($event)\" placeholder=\"Enter search text\"\r\n                    [showClearButton]=\"true\">\r\n                </dx-text-box>\r\n            </div>\r\n        </div>\r\n        <div class=\"column-list-container\">\r\n            <div class=\"columnChooser-group-columns\"\r\n                [class.columnChooser-group-columns-without-format]=\"!showBasicFormat\"\r\n                [class.columnChooser-group-columns-with-format]=\"showBasicFormat\">\r\n                <span class=\"column-visible-label\">Visible</span>\r\n                <span class=\"column-caption-label\">Caption</span>\r\n                <div class=\"column-header\">\r\n                    <input type=\"checkbox\" [(ngModel)]=\"checkMasterCheckBox\" />\r\n                    <label class=\"column-list-label\">Select All</label>\r\n                </div>\r\n                <div *ngFor=\"let groupItem of copyOfColumnList;let i = index;\">\r\n                    <div class=\"groupContainer\"\r\n                        *ngIf=\" ((i===0) || (groupItem.groupName != copyOfColumnList[i-1].groupName)) && groupItem.groupName !== '_row_actions_group_' \"\r\n                        (click)=\"toggleGroupVisibility($event, groupItem.groupName)\">\r\n                        <i class=\"dx-icon-spindown\"\r\n                            [class.dx-icon-spindown]=\"selectedGroupNames.indexOf(groupItem.groupName)  > -1 \"\r\n                            [class.dx-icon-spinright]=\"selectedGroupNames.indexOf(groupItem.groupName) === -1\"></i>\r\n                        <span>{{groupItem.groupName || 'Columns'}}</span>\r\n                    </div>\r\n                    <span *ngIf=\"selectedGroupNames.indexOf(groupItem.groupName) > -1\">\r\n                        <div class=\"cgf-columns-list\"\r\n                            [class.selectedColumn]=\"selectedColumnInfo && groupItem.dataField === selectedColumnInfo.dataField\">\r\n                            <input type=\"checkbox\" attr.data-item=\"{{groupItem.dataField}}\"\r\n                                [(ngModel)]=\"groupItem.visible\"\r\n                                (change)=\"toggleColumnVisibility(groupItem.dataField, groupItem.visible)\" />\r\n                            <label class=\"column-list-label\" (click)=\"columnChooserClick($event)\"\r\n                                attr.data-item=\"{{groupItem.dataField}}\">{{groupItem.caption}}</label>\r\n                        </div>\r\n                    </span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"accordion-content\">\r\n        <pbi-color-format [gridInstanceList]=\"selectedGridInstanceList\" [isMasterGridSelected]=\"isMasterGridSelected\"\r\n            [selectedColumnInfo]=\"selectedColumnData\" [enableInputDataFieldCheck]=\"true\"\r\n            [showBasicFormat]=\"showBasicFormat\" (formatOptionsChanged)=\"formatOptionsChanged($event)\"\r\n            (formatToggled)=\"onFormatToggled($event)\">\r\n        </pbi-color-format>\r\n    </div>\r\n    <span class=\"button-container\" *ngIf=\"isCached\">\r\n        <dx-button text=\"Apply\" class=\"cacheButton\" type=\"default\" (onClick)=\"fetchCachedColumnsData()\">\r\n        </dx-button>\r\n    </span>\r\n    <span class=\"button-container\" *ngIf=\"useBulkApply\">\r\n        <dx-button text=\"Apply\" class=\"cacheButton\" type=\"default\" (onClick)=\"onBulkApplyClick()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    })
], ColumnChooserComponent);
export { ColumnChooserComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sdW1uLWNob29zZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL2NvbHVtbi1jaG9vc2VyL2NvbHVtbi1jaG9vc2VyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsT0FBTyxFQUFFLFNBQVMsRUFBVSxLQUFLLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUkxRixPQUFPLEVBQUUsT0FBTyxHQUFpQixNQUFNLE1BQU0sQ0FBQztBQUM5QyxPQUFPLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBUW5FLElBQWEsc0JBQXNCLEdBQW5DLE1BQWEsc0JBQXNCO0lBcUZqQztRQTNDUywwQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDdEMsNkJBQTZCO1FBRTdCLHVCQUF1QjtRQUNOLHFCQUFnQixHQUF1QyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQzFFLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFXekQsdUJBQWtCLEdBQWtCO1lBQ2xDLE9BQU8sRUFBRSxFQUFFO1lBQ1gsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUNaLFNBQVMsRUFBRSxFQUFFO1lBQ2IsUUFBUSxFQUFFLElBQUk7WUFDZCxXQUFXLEVBQUUsRUFBRTtZQUNmLE1BQU0sRUFBRSxFQUFFO1lBQ1YsU0FBUyxFQUFFLEVBQUU7WUFDYixPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUM7UUFFRiwyQkFBc0IsR0FBK0IsRUFBRSxDQUFDO1FBQ3hELHFCQUFnQixHQUFHLFNBQVMsQ0FBQztRQUM3QixzQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFFdkIsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFDakIsbUJBQWMsR0FBRyxJQUFJLE9BQU8sRUFBd0IsQ0FBQztRQUNyRCxxQkFBZ0IsR0FBb0IsRUFBRSxDQUFDO1FBRXZDLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1QixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDekIsaUJBQVksR0FBeUIsRUFBRSxDQUFDO1FBQ3hDLDZCQUF3QixHQUErQixFQUFFLENBQUM7UUFDMUQsdUJBQWtCLEdBQWEsRUFBRSxDQUFDO1FBQ2xDLG9CQUFlLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLHVCQUF1QixDQUFDLENBQUMsSUFBSSxLQUFLLENBQUM7UUFDdkYsbUJBQWMsR0FBRyxJQUFJLENBQUM7SUFFTixDQUFDO0lBbkZqQiwwQkFBMEI7SUFFMUIsSUFBSSxXQUFXLENBQUMsV0FBNEI7UUFDMUMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDNUIsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUM1RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUM5QztZQUNELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsSUFBSSxXQUFXLEtBQUssT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUdyQyxJQUFJLGtCQUFrQixDQUFDLFVBQXlCO1FBQ3ZELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxVQUFVLENBQUM7UUFDckMsSUFBSSxVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsU0FBUyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDOUM7UUFDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQSxVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsU0FBUyxLQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQzNELENBQUM7SUFDRCxJQUFJLGtCQUFrQixLQUFLLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztJQUVuRCxJQUFJLGdCQUFnQixDQUFDLGdCQUEwQzs7UUFDdEUsTUFBTSxhQUFhLEdBQUcsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3pILElBQUksQ0FBQyx3QkFBd0IsR0FBRyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7UUFDdEYsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxXQUFJLGFBQWEsQ0FBQyxDQUFDLENBQUMsMENBQUUsUUFBUSxDQUFBLEVBQUU7WUFDN0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUM7WUFDMUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3BHO0lBQ0gsQ0FBQztJQUNELElBQUksZ0JBQWdCLEtBQUssT0FBTyxJQUFJLENBQUMsd0JBQStCLENBQUMsQ0FBQyxDQUFDO0lBRTlELElBQUksUUFBUSxDQUFDLEdBQVc7UUFDL0IsSUFBSSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFBQSxDQUFDO0lBUUYsMEJBQTBCO0lBRTFCLElBQUksbUJBQW1CO1FBQ3JCLE1BQU0sa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDekQsT0FBTyxPQUFPLENBQUMsT0FBTyxLQUFLLEtBQUssSUFBSSxPQUFPLENBQUMsU0FBUyxLQUFLLHNCQUFzQixDQUFDLFNBQVMsQ0FBQztRQUM3RixDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sa0JBQWtCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQzNDLENBQUM7SUFDRCxJQUFJLG1CQUFtQixDQUFDLElBQUksSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBK0JuRSxrQ0FBa0M7SUFDbEMsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLE1BQU0sRUFBRTtZQUN4QyxVQUFVLENBQUMsR0FBRyxFQUFFO2dCQUNkLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQzNDLElBQUksQ0FBQyxZQUFZLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxDQUFDLENBQUM7b0JBQ3hFLGNBQWMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFFQUFxRTtnQkFDeEcsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDUDtJQUNILENBQUM7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsU0FBUyxLQUFLLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ2xHLENBQUMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyRSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUU7WUFDeEMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztZQUNwRyxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsY0FBYyxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2hELG1GQUFtRjtRQUNuRixzQ0FBc0M7UUFDdEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUNELHFDQUFxQztJQUVyQyx3QkFBd0I7SUFDakIsc0JBQXNCLENBQUMsU0FBaUIsRUFBRSxPQUFnQjs7UUFDL0QsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLENBQUMsQ0FBQztZQUNuRixJQUFJLFFBQVEsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDbkIsSUFBSSxPQUFPLEVBQUU7b0JBQ1gsT0FBTyxHQUFHLFFBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixFQUFFLDBDQUM3RCxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFLE1BQU0sQ0FBQSxDQUFDO2lCQUN4RjtnQkFDRCxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDdkM7YUFDRjtpQkFBTSxJQUFJLFFBQVEsS0FBSyxDQUFDLENBQUMsRUFBRTtnQkFDMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLENBQUMsQ0FBQzthQUNoRDtTQUNGO2FBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQzVCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsQ0FBQztTQUMvRTthQUFNO1lBQ0wsS0FBSyxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUUsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUM1RCxJQUFJLFNBQVMsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsRUFBRTtvQkFDbkQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO29CQUMxQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDO29CQUNqRCxNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFTSxxQkFBcUIsQ0FBQyxLQUFpQixFQUFFLFNBQWlCO1FBQy9ELElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtZQUNuRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxTQUFTLENBQUMsQ0FBQztTQUNwRjthQUFNO1lBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN6QztJQUNILENBQUM7SUFFTSx5QkFBeUIsQ0FBQyxJQUF3QjtRQUN2RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQy9FLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2RSxNQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNsRSxJQUFJLFVBQVUsQ0FBQyxNQUFNLEVBQUU7WUFDckIsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQWtCLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLENBQUM7WUFDMUgsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFFTSxrQkFBa0IsQ0FBQyxLQUFpQjtRQUN6QyxNQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBMEIsQ0FBQztRQUNoRCxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsT0FBTyxFQUFFO1lBQzVCLFFBQVEsTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRTtnQkFDcEMsS0FBSyxPQUFPO29CQUNWLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM1QyxNQUFNO2FBQ1Q7U0FDRjtRQUNELEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRU0scUJBQXFCLENBQUMsR0FBWTtRQUN2QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUM7UUFDckIsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzlCLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUU7b0JBQ3ZELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUNsRDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFBQyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDbkMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkgsQ0FBQyxDQUFDLENBQUM7U0FFSjthQUNJO1lBQ0gsVUFBVSxDQUFDLEdBQUcsRUFBRTtnQkFDZCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDL0QsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQzdCLENBQUMsQ0FBQyxDQUFDO2dCQUVILElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUM5QixJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssc0JBQXNCLENBQUMsU0FBUyxFQUFFO3dCQUN2RCxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztxQkFDcEI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDM0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMzQyxDQUFDLENBQUMsQ0FBQztnQkFFSCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDekIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3ZFLENBQUMsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ1A7SUFDSCxDQUFDO0lBRU0sc0JBQXNCO1FBQzNCLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDNUIsTUFBTSxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLHNCQUFzQixDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BLLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNsRyxJQUFJLFlBQVksRUFBRTtnQkFDaEIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLEVBQUUsT0FBTyxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsYUFBYSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7Z0JBQ3BJLENBQUMsQ0FBQyxDQUFDO2FBQ0o7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDL0MsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDOUIsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDMUQsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ2xDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN2QixDQUFDLENBQUMsQ0FBQztTQUVKO1FBQ0QsNkVBQTZFO1FBQzdFLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBRSxJQUFZLENBQUMsT0FBTyxLQUFLLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN2SCxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ2xFO1FBQ0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUVNLG9CQUFvQixDQUFDLElBQUk7UUFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7O1lBQzdCLElBQUksR0FBRyxDQUFDLFNBQVMsWUFBSyxJQUFJLENBQUMsa0JBQWtCLDBDQUFFLFNBQVMsQ0FBQSxFQUFFO2dCQUN4RCxHQUFHLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQzNCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQzNDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztnQkFDN0QsQ0FBQyxDQUFDLENBQUM7YUFDSjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTs7WUFDbkMsSUFBSSxJQUFJLENBQUMsU0FBUyxZQUFLLElBQUksQ0FBQyxrQkFBa0IsMENBQUUsU0FBUyxDQUFBLEVBQUU7Z0JBQ3pELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUM3QjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVNLGVBQWUsQ0FBQyxHQUFZO1FBQ2pDLElBQUksQ0FBQyxlQUFlLEdBQUcsR0FBRyxDQUFDO1FBQzNCLGNBQWMsQ0FBQyxPQUFPLENBQUMsdUJBQXVCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFTSxnQkFBZ0I7UUFDckIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBQztRQUNsRSxJQUFJLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRU0sV0FBVztRQUNoQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUNELDJCQUEyQjtJQUUzQix5QkFBeUI7SUFFakIsaUJBQWlCLENBQUMsU0FBaUI7UUFDekMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvSCxDQUFDO0lBRU8sc0JBQXNCLENBQUMsU0FBaUI7UUFDOUMsTUFBTSxhQUFhLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxvQkFBb0IsU0FBUyxJQUFJLENBQUMsQ0FBQztRQUMxRixNQUFNLFNBQVMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLDZCQUE2QixDQUFDLENBQUM7UUFDeEYsSUFBSSxTQUFTLENBQUMsTUFBTSxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDNUMsTUFBTSxNQUFNLEdBQUksYUFBYSxDQUFDLENBQUMsQ0FBaUIsQ0FBQyxTQUFTLENBQUM7WUFDM0QsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxNQUFNLEdBQUcsQ0FBRSxTQUFTLENBQUMsQ0FBQyxDQUFpQixDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLDhCQUE4QjtTQUNqSDtJQUNILENBQUM7SUFFTyx3QkFBd0IsQ0FBQyxRQUFnQjtRQUMvQyxJQUFJLElBQUksQ0FBQyx5QkFBeUIsRUFBRTtZQUNsQyxJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDOUM7UUFDRCxJQUFJLENBQUMseUJBQXlCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDaEgsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ2xCLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBRTlCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQzlDO1FBQ0gsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU8sZ0JBQWdCLENBQUMsTUFBOEI7UUFDckQsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNyQixJQUFJLENBQUMsd0JBQXdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0csTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDakIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hHLENBQUMsQ0FBQyxDQUFBO1lBQ0YsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDbkU7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO2FBQy9EO1lBQ0QsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDL0Y7SUFDSCxDQUFDO0NBSUYsQ0FBQTtBQW5VQztJQURDLEtBQUssRUFBRTt5REFZUDtBQUdRO0lBQVIsS0FBSyxFQUFFO3dEQUEwQjtBQUN6QjtJQUFSLEtBQUssRUFBRTtnRUFNUDtBQUdRO0lBQVIsS0FBSyxFQUFFOzhEQU9QO0FBR1E7SUFBUixLQUFLLEVBQUU7c0RBRVA7QUFDUTtJQUFSLEtBQUssRUFBRTs0REFBb0I7QUFDbkI7SUFBUixLQUFLLEVBQUU7cUVBQThCO0FBSTVCO0lBQVQsTUFBTSxFQUFFO2dFQUFrRjtBQUNqRjtJQUFULE1BQU0sRUFBRTtrRUFBZ0Q7QUEvQzlDLHNCQUFzQjtJQUxsQyxTQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsb0JBQW9CO1FBQzlCLHVoSkFBOEM7O0tBRS9DLENBQUM7R0FDVyxzQkFBc0IsQ0F1VWxDO1NBdlVZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbHVtblZpc2liaWxpdHlCdWxrTG9va3VwLCBDb2x1bW5WaXNpYmlsaXR5VHlwZSwgU2VsZWN0ZWRDb2x1bW5JbmZvIH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbHVtbic7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEeFRleHRCb3hDb21wb25lbnQgfSBmcm9tICdkZXZleHRyZW1lLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBHcmlkQ29tcG9uZW50SW5zdGFuY2VzIH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbW1vbi1ncmlkLWZyYW1ld29yayc7XHJcbmltcG9ydCB7IFBCSUdyaWRDb2x1bW4gfSBmcm9tICcuLi9jb250cmFjdHMvZ3JpZCc7XHJcbmltcG9ydCB7IFN1YmplY3QsIFN1YnNjcmlwdGlvbiwgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgYnVmZmVyLCBkZWJvdW5jZVRpbWUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IGN1c3RvbUFjdGlvbkNvbHVtbkluZm8gfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvY29uc3RhbnRzJztcclxuaW1wb3J0IERldkV4cHJlc3MgZnJvbSAnZGV2ZXh0cmVtZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS1jb2x1bW4tY2hvb3NlcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2NvbHVtbi1jaG9vc2VyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9jb2x1bW4tY2hvb3Nlci5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbHVtbkNob29zZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG4gIC8vI3JlZ2lvbiBJbnB1dCBQcm9wZXJ0aWVzXHJcbiAgQElucHV0KClcclxuICBzZXQgY29sdW1uc0xpc3QoY29sdW1uc0xpc3Q6IFBCSUdyaWRDb2x1bW5bXSkge1xyXG4gICAgdGhpcy5jb2x1bW5zRGF0YSA9IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMgPSBbXTtcclxuICAgIHRoaXMuY29weU9mQ29sdW1uTGlzdCA9IFtdO1xyXG4gICAgY29sdW1uc0xpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgdGhpcy5jb2x1bW5zRGF0YS5wdXNoKGl0ZW0pO1xyXG4gICAgICBpZiAoaXRlbS5ncm91cE5hbWUgJiYgdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMuaW5kZXhPZihpdGVtLmdyb3VwTmFtZSkgPT09IC0xKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMucHVzaChpdGVtLmdyb3VwTmFtZSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jb3B5T2ZDb2x1bW5MaXN0LnB1c2goaXRlbSk7XHJcbiAgICB9KTtcclxuICB9XHJcbiAgZ2V0IGNvbHVtbnNMaXN0KCkgeyByZXR1cm4gdGhpcy5jb2x1bW5zRGF0YTsgfVxyXG5cclxuICBASW5wdXQoKSBwdWJsaWMgaXNDYWNoZWQ6IGJvb2xlYW47XHJcbiAgQElucHV0KCkgc2V0IHNlbGVjdGVkQ29sdW1uSW5mbyhjb2x1bW5JdGVtOiBQQklHcmlkQ29sdW1uKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YSA9IGNvbHVtbkl0ZW07XHJcbiAgICBpZiAoY29sdW1uSXRlbT8uZGF0YUZpZWxkKSB7XHJcbiAgICAgIHRoaXMub25DbGlja0NvbHVtbkl0ZW0oY29sdW1uSXRlbS5kYXRhRmllbGQpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5icmluZ0VsZW1lbnRUb1ZpZXdBcmVhKGNvbHVtbkl0ZW0/LmRhdGFGaWVsZCB8fCAnJyk7XHJcbiAgfVxyXG4gIGdldCBzZWxlY3RlZENvbHVtbkluZm8oKSB7IHJldHVybiB0aGlzLnNlbGVjdGVkQ29sdW1uRGF0YTsgfVxyXG5cclxuICBASW5wdXQoKSBzZXQgZ3JpZEluc3RhbmNlTGlzdChncmlkSW5zdGFuY2VMaXN0OiBHcmlkQ29tcG9uZW50SW5zdGFuY2VzW10pIHtcclxuICAgIGNvbnN0IGdyaWRJbnN0YW5jZXMgPSBncmlkSW5zdGFuY2VMaXN0LmZpbHRlcihpdGVtID0+IGl0ZW0uaXNTZWxlY3RlZCAmJiBPYmplY3Qua2V5cyhpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSkubGVuZ3RoKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0ID0gZ3JpZEluc3RhbmNlcy5tYXAoaXRlbSA9PiBpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSk7XHJcbiAgICBpZiAoZ3JpZEluc3RhbmNlTGlzdC5sZW5ndGggPiAxICYmIGdyaWRJbnN0YW5jZXNbMF0/LmdyaWROYW1lKSB7XHJcbiAgICAgIHRoaXMuaXNNYXN0ZXJHcmlkU2VsZWN0ZWQgPSBncmlkSW5zdGFuY2VzWzBdLmlzTWFzdGVyR3JpZDtcclxuICAgICAgdGhpcy5jb2x1bW5GaWVsZFRpdGxlID0gYENPTFVNTlMke3RoaXMuaXNNYXN0ZXJHcmlkU2VsZWN0ZWQgPyAnJyA6ICcgLSBDaGlsZCBWaWV3J31gLnRvVXBwZXJDYXNlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldCBncmlkSW5zdGFuY2VMaXN0KCkgeyByZXR1cm4gdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QgYXMgYW55OyB9XHJcblxyXG4gIEBJbnB1dCgpIHNldCBkZWJvdW5jZShudW06IG51bWJlcikge1xyXG4gICAgdGhpcy5zdWJzY3JpYmVUb0NvbHVtbkNoYW5nZXMobnVtKTtcclxuICB9O1xyXG4gIEBJbnB1dCgpIHVzZUJ1bGtBcHBseTogdHJ1ZTtcclxuICBASW5wdXQoKSByZWZyZXNoT25Db2x1bW5VcGRhdGUgPSB0cnVlO1xyXG4gIC8vI2VuZHJlZ2lvbiBJbnB1dCBQcm9wZXJ0aWVzXHJcblxyXG4gIC8vI3JlZ2lvbiBPdXRwdXQgRXZlbnRzXHJcbiAgQE91dHB1dCgpIHB1YmxpYyBhcHBseUJ1dHRvbkNsaWNrOiBFdmVudEVtaXR0ZXI8U2VsZWN0ZWRDb2x1bW5JbmZvW10+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgY2xvc2VDdXJyZW50Rmx5T3V0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIC8vI2VuZHJlZ2lvbiBPdXRwdXQgRXZlbnRzXHJcblxyXG4gIGdldCBjaGVja01hc3RlckNoZWNrQm94KCkge1xyXG4gICAgY29uc3QgYW55T25lQ29sdW1uSGlkZGVuID0gdGhpcy5jb2x1bW5zRGF0YS5maW5kKGNvbEl0ZW0gPT4ge1xyXG4gICAgICByZXR1cm4gY29sSXRlbS52aXNpYmxlID09PSBmYWxzZSAmJiBjb2xJdGVtLmRhdGFGaWVsZCAhPT0gY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5kYXRhRmllbGQ7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiBhbnlPbmVDb2x1bW5IaWRkZW4gPyBmYWxzZSA6IHRydWU7XHJcbiAgfVxyXG4gIHNldCBjaGVja01hc3RlckNoZWNrQm94KGFyZ3MpIHsgdGhpcy5vbkNsaWNrTWFzdGVyQ2hlY2tCb3goYXJncyk7IH1cclxuXHJcbiAgc2VsZWN0ZWRDb2x1bW5EYXRhOiBQQklHcmlkQ29sdW1uID0ge1xyXG4gICAgY2FwdGlvbjogJycsXHJcbiAgICBjb2x1bW5JZDogLTEsXHJcbiAgICBkYXRhRmllbGQ6ICcnLFxyXG4gICAgZGF0YVR5cGU6IG51bGwsXHJcbiAgICBkZXNjcmlwdGlvbjogJycsXHJcbiAgICBmb3JtYXQ6ICcnLFxyXG4gICAgZ3JvdXBOYW1lOiAnJyxcclxuICAgIHZpc2libGU6IHRydWVcclxuICB9O1xyXG5cclxuICBidWxrQXBwbHlDb2x1bW5zTG9va3VwOiBDb2x1bW5WaXNpYmlsaXR5QnVsa0xvb2t1cCA9IHt9O1xyXG4gIGNvbHVtbkZpZWxkVGl0bGUgPSAnQ09MVU1OUyc7XHJcbiAgY29sdW1uU2VhcmNoVmFsdWUgPSAnJztcclxuICBjb2x1bW5TdWJqZWN0U3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgY29sdW1uc0RhdGEgPSBbXTtcclxuICBjb2x1bW5zU3ViamVjdCA9IG5ldyBTdWJqZWN0PENvbHVtblZpc2liaWxpdHlUeXBlPigpO1xyXG4gIGNvcHlPZkNvbHVtbkxpc3Q6IFBCSUdyaWRDb2x1bW5bXSA9IFtdO1xyXG4gIGRlc3Ryb3llZDogYm9vbGVhbjtcclxuICBpc01hc3RlckdyaWRTZWxlY3RlZCA9IHRydWU7XHJcbiAgc2VsZWN0QWxsQ2hlY2tlZCA9IGZhbHNlO1xyXG4gIHNlbGVjdGVkQ29sczogU2VsZWN0ZWRDb2x1bW5JbmZvW10gPSBbXTtcclxuICBzZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3Q6IERldkV4cHJlc3MudWkuZHhEYXRhR3JpZFtdID0gW107XHJcbiAgc2VsZWN0ZWRHcm91cE5hbWVzOiBzdHJpbmdbXSA9IFtdO1xyXG4gIHNob3dCYXNpY0Zvcm1hdCA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgnYmFzaWNGb3JtYXRWaXNpYmlsaXR5JykpIHx8IGZhbHNlO1xyXG4gIHNob3dDb2x1bW5MaXN0ID0gdHJ1ZTtcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgLy8jcmVnaW9uIEFuZ3VsYXIgTGlmZWN5Y2xlIEV2ZW50c1xyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0Lmxlbmd0aCkge1xyXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdC5mb3JFYWNoKGdyaWQgPT4ge1xyXG4gICAgICAgICAgZ3JpZC5jb2x1bW5PcHRpb24oY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5kYXRhRmllbGQsIHsgdmlzaWJsZTogZmFsc2UgfSk7XHJcbiAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50QXBwbGllZFZpZXcnLFxyXG4gICAgICAgICAgICBKU09OLnN0cmluZ2lmeShncmlkLnN0YXRlKCkpKTsgLy8gaG9sZGluZyBzdGF0ZSB0byBtYWtlIHN1cmUgZ3JvdXAtYnkgd29ya3Mgd2hlbiB3ZSB0b2dnbGUgc2VsZWN0QWxsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0sIDApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29scy5mb3JFYWNoKGNvbCA9PiB7XHJcbiAgICAgIHRoaXMuY29sdW1uc0RhdGEuZmlsdGVyKGNvbHVtbiA9PiBjb2x1bW4uZGF0YUZpZWxkID09PSBjb2wuZGF0YUZpZWxkKVswXS52aXNpYmxlID0gIWNvbC52aXNpYmxlO1xyXG4gICAgfSk7XHJcbiAgICBjb25zdCBhbnlDb2x1bW5WaXNpYmxlID0gdGhpcy5jb2x1bW5zRGF0YS5maW5kKGl0ZW0gPT4gaXRlbS52aXNpYmxlKTtcclxuICAgIGlmICh0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdC5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICBpdGVtLmNvbHVtbk9wdGlvbihjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCwgeyB2aXNpYmxlOiBhbnlDb2x1bW5WaXNpYmxlID8gdHJ1ZSA6IGZhbHNlIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHNlc3Npb25TdG9yYWdlLnJlbW92ZUl0ZW0oJ2N1cnJlbnRBcHBsaWVkVmlldycpO1xyXG4gICAgLy8gTGV0cyBjb2x1bW5zIGZpbmlzaCB1cCByZW5kZXJpbmcgaWYgYW55IG91dHN0YW5kaW5nLiBXaWxsIHVuc3ViY3JpYmUgaXRzZWxmIHdoZW5cclxuICAgIC8vIGRlc3Ryb3llZCBwYXJhbWV0ZXIgaXMgc2V0IHRvIHRydWUuXHJcbiAgICB0aGlzLmRlc3Ryb3llZCA9IHRydWU7XHJcbiAgICB0aGlzLmNvbHVtbnNTdWJqZWN0Lm5leHQobnVsbCk7XHJcbiAgfVxyXG4gIC8vI2VuZHJlZ2lvbiBBbmd1bGFyIExpZmVjeWNsZSBFdmVudHNcclxuXHJcbiAgLy8jcmVnaW9uIFB1YmxpYyBNZXRob2RzXHJcbiAgcHVibGljIHRvZ2dsZUNvbHVtblZpc2liaWxpdHkoZGF0YUZpZWxkOiBzdHJpbmcsIHZpc2libGU6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmlzQ2FjaGVkKSB7XHJcbiAgICAgIGNvbnN0IGNvbEluZGV4ID0gdGhpcy5zZWxlY3RlZENvbHMuZmluZEluZGV4KGRhdGEgPT4gZGF0YS5kYXRhRmllbGQgPT09IGRhdGFGaWVsZCk7XHJcbiAgICAgIGlmIChjb2xJbmRleCAhPT0gLTEpIHtcclxuICAgICAgICBpZiAodmlzaWJsZSkge1xyXG4gICAgICAgICAgdmlzaWJsZSA9ICF0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdFswXS5nZXRWaXNpYmxlQ29sdW1ucygpPy5cclxuICAgICAgICAgICAgZmlsdGVyKGNvbCA9PiBjb2wuaGFzT3duUHJvcGVydHkoJ2RhdGFGaWVsZCcpICYmIGNvbC5kYXRhRmllbGQgPT09IGRhdGFGaWVsZCkubGVuZ3RoO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXZpc2libGUpIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRDb2xzLnNwbGljZShjb2xJbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKGNvbEluZGV4ID09PSAtMSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRDb2xzLnB1c2goeyBkYXRhRmllbGQsIHZpc2libGUgfSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAodGhpcy51c2VCdWxrQXBwbHkpIHtcclxuICAgICAgdGhpcy5idWxrQXBwbHlDb2x1bW5zTG9va3VwW2RhdGFGaWVsZC50b0xvd2VyQ2FzZSgpXSA9IHsgZGF0YUZpZWxkLCB2aXNpYmxlIH07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgdGhpcy5jb2x1bW5zRGF0YS5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICBpZiAoZGF0YUZpZWxkID09PSB0aGlzLmNvbHVtbnNEYXRhW2luZGV4XS5kYXRhRmllbGQpIHtcclxuICAgICAgICAgIHRoaXMuY29sdW1uc0RhdGFbaW5kZXhdLnZpc2libGUgPSB2aXNpYmxlO1xyXG4gICAgICAgICAgdGhpcy5jb2x1bW5zU3ViamVjdC5uZXh0KHsgdmlzaWJsZSwgZGF0YUZpZWxkIH0pO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgdG9nZ2xlR3JvdXBWaXNpYmlsaXR5KGV2ZW50OiBNb3VzZUV2ZW50LCBncm91cE5hbWU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRHcm91cE5hbWVzLmluZGV4T2YoZ3JvdXBOYW1lKSA+IC0xKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRHcm91cE5hbWVzID0gdGhpcy5zZWxlY3RlZEdyb3VwTmFtZXMuZmlsdGVyKGFyciA9PiBhcnIgIT09IGdyb3VwTmFtZSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkR3JvdXBOYW1lcy5wdXNoKGdyb3VwTmFtZSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25TZWFyY2hDb2x1bW5WYWx1ZUNoYW5nZShhcmdzOiBEeFRleHRCb3hDb21wb25lbnQpOiB2b2lkIHtcclxuICAgIHRoaXMuY29weU9mQ29sdW1uTGlzdCA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5jb2x1bW5zRGF0YS5maWx0ZXIoZGF0YSA9PlxyXG4gICAgICBkYXRhLmNhcHRpb24udG9Mb3dlckNhc2UoKS5pbmRleE9mKGFyZ3MudmFsdWUudG9Mb3dlckNhc2UoKSkgPiAtMSkpKTtcclxuICAgIGNvbnN0IHZpc2libGVDb2wgPSB0aGlzLnNlbGVjdGVkQ29scy5maWx0ZXIoZGF0YSA9PiBkYXRhLnZpc2libGUpO1xyXG4gICAgaWYgKHZpc2libGVDb2wubGVuZ3RoKSB7XHJcbiAgICAgIHZpc2libGVDb2wuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICB0aGlzLmNvcHlPZkNvbHVtbkxpc3QuZmlsdGVyKGRhdGEgPT4gZGF0YS5kYXRhRmllbGQgPT09IGl0ZW0uZGF0YUZpZWxkKS5tYXAoKGNvbDogUEJJR3JpZENvbHVtbikgPT4gY29sLnZpc2libGUgPSB0cnVlKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY29sdW1uQ2hvb3NlckNsaWNrKGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZCB7XHJcbiAgICBjb25zdCB0YXJnZXQgPSBldmVudC50YXJnZXQgYXMgSFRNTElucHV0RWxlbWVudDtcclxuICAgIGlmICh0YXJnZXQgJiYgdGFyZ2V0LmRhdGFzZXQpIHtcclxuICAgICAgc3dpdGNoICh0YXJnZXQudGFnTmFtZS50b0xvd2VyQ2FzZSgpKSB7XHJcbiAgICAgICAgY2FzZSAnbGFiZWwnOlxyXG4gICAgICAgICAgdGhpcy5vbkNsaWNrQ29sdW1uSXRlbSh0YXJnZXQuZGF0YXNldC5pdGVtKTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkNsaWNrTWFzdGVyQ2hlY2tCb3godmFsOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICB0aGlzLnNlbGVjdEFsbENoZWNrZWQgPSB2YWw7XHJcbiAgICB0aGlzLmNvcHlPZkNvbHVtbkxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgaXRlbS52aXNpYmxlID0gdmFsO1xyXG4gICAgfSk7XHJcbiAgICBpZiAodGhpcy5pc0NhY2hlZCkge1xyXG4gICAgICB0aGlzLmNvbHVtbnNEYXRhLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgaWYgKGl0ZW0uZGF0YUZpZWxkICE9PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCkge1xyXG4gICAgICAgICAgdGhpcy50b2dnbGVDb2x1bW5WaXNpYmlsaXR5KGl0ZW0uZGF0YUZpZWxkLCB2YWwpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9IGlmICh0aGlzLnVzZUJ1bGtBcHBseSkge1xyXG4gICAgICB0aGlzLmNvcHlPZkNvbHVtbkxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICB0aGlzLmJ1bGtBcHBseUNvbHVtbnNMb29rdXBbaXRlbS5kYXRhRmllbGQudG9Mb3dlckNhc2UoKV0gPSB7IGRhdGFGaWVsZDogaXRlbS5kYXRhRmllbGQsIHZpc2libGU6IGl0ZW0udmlzaWJsZSB9O1xyXG4gICAgICB9KTtcclxuXHJcbiAgICB9XHJcbiAgICBlbHNlIHtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5jb3B5T2ZDb2x1bW5MaXN0LmZvckVhY2goaXRlbSA9PiB7IGl0ZW0udmlzaWJsZSA9IHZhbDsgfSk7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgIGl0ZW0ub3B0aW9uKCdjb2x1bW5zJywgW10pO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmNvbHVtbnNEYXRhLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICBpZiAoaXRlbS5kYXRhRmllbGQgIT09IGN1c3RvbUFjdGlvbkNvbHVtbkluZm8uZGF0YUZpZWxkKSB7XHJcbiAgICAgICAgICAgIGl0ZW0udmlzaWJsZSA9IHZhbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgIGl0ZW0ub3B0aW9uKCdjb2x1bW5zJywgdGhpcy5jb2x1bW5zRGF0YSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdEFsbENoZWNrZWQpIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGl0ZW0uc3RhdGUoSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCdjdXJyZW50QXBwbGllZFZpZXcnKSkpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LCAwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBmZXRjaENhY2hlZENvbHVtbnNEYXRhKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRDb2xzLmxlbmd0aCkge1xyXG4gICAgICBjb25zdCBhY3Rpb25Db2x1bW4gPSB0aGlzLmNvbHVtbnNEYXRhLmZpbHRlcihkYXRhID0+IGRhdGEuZGF0YUZpZWxkID09PSBjdXN0b21BY3Rpb25Db2x1bW5JbmZvLmRhdGFGaWVsZCAmJiBkYXRhLmdyb3VwTmFtZSA9PT0gY3VzdG9tQWN0aW9uQ29sdW1uSW5mby5ncm91cE5hbWUpWzBdO1xyXG4gICAgICBjb25zdCB2aXNpYmxlQ29sdW1ucyA9IHRoaXMuc2VsZWN0ZWRDb2xzLmZpbHRlcihkYXRhID0+IGRhdGEudmlzaWJsZSkubWFwKGl0ZW0gPT4gaXRlbS5kYXRhRmllbGQpO1xyXG4gICAgICBpZiAoYWN0aW9uQ29sdW1uKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgIGl0ZW0uY29sdW1uT3B0aW9uKGFjdGlvbkNvbHVtbi5kYXRhRmllbGQsIHsgdmlzaWJsZTogdmlzaWJsZUNvbHVtbnMubGVuZ3RoID8gdHJ1ZSA6IGZhbHNlLCBmaXhlZDogdHJ1ZSwgZml4ZWRQb3NpdGlvbjogJ3JpZ2h0JyB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChncmlkSW5zdCA9PiB7XHJcbiAgICAgICAgZ3JpZEluc3QuYmVnaW5VcGRhdGUoKTtcclxuICAgICAgICB0aGlzLmNvbHVtbnNMaXN0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICBncmlkSW5zdC5jb2x1bW5PcHRpb24oaXRlbS5kYXRhRmllbGQsICd2aXNpYmxlJywgZmFsc2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGdyaWRJbnN0Lm9wdGlvbignZGF0YVNvdXJjZScsIFtdKTtcclxuICAgICAgICBncmlkSW5zdC5lbmRVcGRhdGUoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgfVxyXG4gICAgLy8gVE9ETzogY2hlY2sgZW5hYmxlIGFuZCBkaXNhYmxlIGdyaWQgZXhwb3J0LiBJZiBub3QgdGhlbiByZW1vdmUgYmVsb3cgY29kZS5cclxuICAgIGlmICgodGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3RbMF0uZ2V0VmlzaWJsZUNvbHVtbnMoKS5maWx0ZXIoZGF0YSA9PiAoZGF0YSBhcyBhbnkpLmNvbW1hbmQgIT09ICdlbXB0eScpLmxlbmd0aCkgPiAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0WzBdLm9wdGlvbignZXhwb3J0LmVuYWJsZWQnLCBmYWxzZSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmFwcGx5QnV0dG9uQ2xpY2suZW1pdCh0aGlzLnNlbGVjdGVkQ29scyk7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ29scyA9IFtdO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGZvcm1hdE9wdGlvbnNDaGFuZ2VkKGFyZ3MpOiB2b2lkIHtcclxuICAgIHRoaXMuY29sdW1uc0RhdGEuZm9yRWFjaChjb2wgPT4ge1xyXG4gICAgICBpZiAoY29sLmRhdGFGaWVsZCA9PT0gdGhpcy5zZWxlY3RlZENvbHVtbkRhdGE/LmRhdGFGaWVsZCkge1xyXG4gICAgICAgIGNvbC5jYXB0aW9uID0gYXJncy5jYXB0aW9uO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgICAgICBpdGVtLmNvbHVtbk9wdGlvbihjb2wuZGF0YUZpZWxkLCB7IGNhcHRpb246IGNvbC5jYXB0aW9uIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuY29weU9mQ29sdW1uTGlzdC5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICBpZiAoaXRlbS5kYXRhRmllbGQgPT09IHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhPy5kYXRhRmllbGQpIHtcclxuICAgICAgICBpdGVtLmNhcHRpb24gPSBhcmdzLmNhcHRpb247XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uRm9ybWF0VG9nZ2xlZCh2YWw6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgIHRoaXMuc2hvd0Jhc2ljRm9ybWF0ID0gdmFsO1xyXG4gICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbSgnYmFzaWNGb3JtYXRWaXNpYmlsaXR5JywgSlNPTi5zdHJpbmdpZnkodmFsKSk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25CdWxrQXBwbHlDbGljaygpOiB2b2lkIHtcclxuICAgIHRoaXMuYnVsa0FwcGx5Q29sdW1ucyhPYmplY3QudmFsdWVzKHRoaXMuYnVsa0FwcGx5Q29sdW1uc0xvb2t1cCkpO1xyXG4gICAgdGhpcy5idWxrQXBwbHlDb2x1bW5zTG9va3VwID0ge307XHJcbiAgICB0aGlzLmNsb3NlRmx5T3V0KCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xvc2VGbHlPdXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsb3NlQ3VycmVudEZseU91dC5lbWl0KCk7XHJcbiAgfVxyXG4gIC8vI2VuZHJlZ2lvbiBQdWJsaWMgTWV0aG9kc1xyXG5cclxuICAvLyNyZWdpb24gUHJpdmF0ZSBNZXRob2RzXHJcblxyXG4gIHByaXZhdGUgb25DbGlja0NvbHVtbkl0ZW0oZGF0YUZpZWxkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRDb2x1bW5EYXRhID0gdGhpcy5jb2x1bW5zRGF0YS5maWx0ZXIoZWxlbWVudCA9PiBlbGVtZW50LmRhdGFGaWVsZC50b0xvd2VyQ2FzZSgpID09PSBkYXRhRmllbGQudG9Mb3dlckNhc2UoKSlbMF07XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJyaW5nRWxlbWVudFRvVmlld0FyZWEoZGF0YUZpZWxkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgIGNvbnN0IGRhdGFGaWVsZEVsZW0gPSB3aW5kb3cuZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChgaW5wdXRbZGF0YS1pdGVtPScke2RhdGFGaWVsZH0nXWApO1xyXG4gICAgY29uc3QgY29udGFpbmVyID0gd2luZG93LmRvY3VtZW50LmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2NvbHVtbkNob29zZXItZ3JvdXAtY29sdW1ucycpO1xyXG4gICAgaWYgKGNvbnRhaW5lci5sZW5ndGggJiYgZGF0YUZpZWxkRWxlbS5sZW5ndGgpIHtcclxuICAgICAgY29uc3QgdG9wUG9zID0gKGRhdGFGaWVsZEVsZW1bMF0gYXMgSFRNTEVsZW1lbnQpLm9mZnNldFRvcDtcclxuICAgICAgY29udGFpbmVyWzBdLnNjcm9sbFRvcCA9IHRvcFBvcyAtICgoY29udGFpbmVyWzBdIGFzIEhUTUxFbGVtZW50KS5vZmZzZXRUb3AgKyAxMCk7IC8vIDEwIGlzIG9mZnNldCB0byBlbmhhbmNlIFVYLlxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBzdWJzY3JpYmVUb0NvbHVtbkNoYW5nZXMoZGVib3VuY2U6IG51bWJlcik6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuY29sdW1uU3ViamVjdFN1YnNjcmlwdGlvbikge1xyXG4gICAgICB0aGlzLmNvbHVtblN1YmplY3RTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuICAgIHRoaXMuY29sdW1uU3ViamVjdFN1YnNjcmlwdGlvbiA9IHRoaXMuY29sdW1uc1N1YmplY3QucGlwZShidWZmZXIodGhpcy5jb2x1bW5zU3ViamVjdC5waXBlKGRlYm91bmNlVGltZShkZWJvdW5jZSkpKSlcclxuICAgICAgLnN1YnNjcmliZShmaWVsZHMgPT4ge1xyXG4gICAgICAgIGZpZWxkcyA9IGZpZWxkcy5maWx0ZXIoZiA9PiBmKTtcclxuICAgICAgICB0aGlzLmJ1bGtBcHBseUNvbHVtbnMoZmllbGRzKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZGVzdHJveWVkKSB7XHJcbiAgICAgICAgICB0aGlzLmNvbHVtblN1YmplY3RTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGJ1bGtBcHBseUNvbHVtbnMoZmllbGRzOiBDb2x1bW5WaXNpYmlsaXR5VHlwZVtdKTogdm9pZCB7XHJcbiAgICBpZiAoZmllbGRzLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHsgaXRlbS5iZWdpbkN1c3RvbUxvYWRpbmcoJ0xvYWRpbmcnKTsgaXRlbS5iZWdpblVwZGF0ZSgpOyB9KTtcclxuICAgICAgZmllbGRzLmZvckVhY2goZiA9PiB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IGl0ZW0uY29sdW1uT3B0aW9uKGYuZGF0YUZpZWxkLCB7IHZpc2libGU6IGYudmlzaWJsZSB9KSk7XHJcbiAgICAgIH0pXHJcbiAgICAgIGlmICh0aGlzLnJlZnJlc2hPbkNvbHVtblVwZGF0ZSkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiBpdGVtLnJlZnJlc2godHJ1ZSkpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiBpdGVtLnJlcGFpbnQoKSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChpdGVtID0+IHsgaXRlbS5lbmRDdXN0b21Mb2FkaW5nKCk7IGl0ZW0uZW5kVXBkYXRlKCk7IH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy8jZW5kcmVnaW9uIFByaXZhdGUgTWV0aG9kc1xyXG5cclxufVxyXG4iXX0=