import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SettingExportOptions } from '../../utilities/constants';
import { CGFSettingsEnum } from '../../utilities/enums';
import { getClassNameByThemeName } from '../../utilities/utilityFunctions';
let SettingsComponent = class SettingsComponent {
    constructor(activatedRoute, router) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.cgfSettingsOptions = [];
        this.gridSwitchValueChange = new EventEmitter();
        this.closeCurrentFlyOut = new EventEmitter();
        this.hideGridBorder = false;
        this.radioGroup = ['Auto Fit'];
        this.radioGroupValue = null;
        this.showFormattingSection = true;
        this.themeList = [
            { key: 'light.regular', value: 'Cozy' },
            { key: 'light.compact', value: 'Regular' },
            { key: 'np.compact', value: 'Default' }
        ];
        this.exportActionList = [];
        this.cgfSettingsEnum = CGFSettingsEnum;
        this.currentAppliedTheme = 'np.compact';
    }
    set selectedTheme(themeName) {
        if (sessionStorage.getItem('theme')) {
            this.currentAppliedTheme = sessionStorage.getItem('theme');
        }
        else {
            this.currentAppliedTheme = themeName;
        }
    }
    get selectedTheme() { return this.currentAppliedTheme; }
    set selectedBorderSwitch(val) {
        this.hideGridBorder = val;
    }
    get selectedBorderSwitch() { return this.hideGridBorder; }
    ngOnInit() {
        this.exportActionList = SettingExportOptions.filter(option => this.cgfSettingsOptions.indexOf(option.id) > -1);
    }
    onThemeChanged() {
        sessionStorage.setItem('theme', this.currentAppliedTheme);
        const appliedThemeName = getClassNameByThemeName(this.currentAppliedTheme);
        this.gridInstanceList.forEach(item => {
            let existingClasses = item.gridComponentInstance._$element[0].className;
            if (existingClasses.indexOf('dx-swatch-default') > -1) {
                existingClasses = existingClasses.replace(new RegExp('dx-swatch-default', 'g'), '');
            }
            else if (existingClasses.indexOf('dx-swatch-regular') > -1) {
                existingClasses = existingClasses.replace(new RegExp('dx-swatch-regular', 'g'), '');
            }
            item.gridComponentInstance._$element[0].className = `${existingClasses} ${appliedThemeName}`;
        });
        this.emitToggleValue('theme', this.currentAppliedTheme);
    }
    onHideGridChange() {
        this.emitToggleValue('gridLines', this.hideGridBorder);
    }
    onClickOfDEMButton() {
        // const params = {
        //   webMenuId: getWebMenuIdByKey('DataEntityManagement'),
        //   entityId: this.activatedRoute.snapshot.queryParams.entity,
        //   activeTab: 'databrowserentity'
        // };
        // this.router.navigate(['/dataEntityManagement'], { queryParams: params });
    }
    onAutoFitChange() {
        let gridInstanceMaxCol = {
            gridComponentInstance: new Object(),
            gridName: '',
            isMasterGrid: false,
            isSelected: false
        };
        let existColCount = 0;
        this.gridInstanceList.forEach(gridInstance => {
            var _a;
            const visibleColCount = ((_a = gridInstance.gridComponentInstance.getVisibleColumns()) === null || _a === void 0 ? void 0 : _a.length) || 0;
            if (existColCount < visibleColCount) {
                gridInstanceMaxCol = gridInstance;
                existColCount = visibleColCount;
            }
        });
        gridInstanceMaxCol.gridComponentInstance.beginUpdate();
        const colCount = gridInstanceMaxCol.gridComponentInstance.columnCount();
        for (let i = 0; i < colCount; i++) {
            if (gridInstanceMaxCol.gridComponentInstance.columnOption(i, 'visible')) {
                gridInstanceMaxCol.gridComponentInstance.columnOption(i, 'width', 'auto');
            }
        }
        gridInstanceMaxCol.gridComponentInstance.endUpdate();
    }
    emitToggleValue(key, value) {
        const _data = { key, value };
        this.gridSwitchValueChange.emit(_data);
    }
    onWorkFlowEmailClick() {
        // const redirectURL =
        //   this.configService.config.WorkFlowMasterBaseURL + '/#/creator/exportreportingplatform?url=' + encodeURIComponent(location.href);
        // open(redirectURL, '_blank');
    }
    getKey() {
        // this.cgfService.getApiKey().subscribe(response => { this.generateUrlSuccess(response); }, () => {
        //   apiCallError('Error while generating key. Please try again.');
        // });
    }
    generateUrlSuccess(response) {
        // if (response.ToasterType.toLowerCase().trim() === 'success') {
        //   const urlParams = location.hash.split('?')[1];
        //   const url = `${APIEndPoints.dataBrowser.excelData}?${urlParams}&key=${response.Message}`;
        //   copyTextToClipBoard(url);
        //   appToastr({ type: 'success', message: 'Link generated and copied successfully.' });
        // } else if (response.ToasterType.toLowerCase().trim() === 'error') {
        //   appToastr({ type: 'error', message: response.Message || 'Error while generating key. Please try again.' });
        // }
    }
    exportSectionButtonClick(data) {
        switch (data.key) {
            case 'exportExcel':
                const selectedInstance = this.gridInstanceList.filter(item => item.isSelected)[0];
                if (selectedInstance) {
                    // When the selectionOnly parameter is false - the method exports all rows, when true - only the selected ones.
                    selectedInstance.gridComponentInstance.exportToExcel(false);
                }
                break;
            case 'excelLink':
                this.getKey();
                break;
            case 'emailReport':
                this.onWorkFlowEmailClick();
                break;
        }
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
};
SettingsComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router }
];
__decorate([
    Input()
], SettingsComponent.prototype, "cgfSettingsOptions", void 0);
__decorate([
    Input()
], SettingsComponent.prototype, "selectedTheme", null);
__decorate([
    Input()
], SettingsComponent.prototype, "selectedBorderSwitch", null);
__decorate([
    Input()
], SettingsComponent.prototype, "gridInstanceList", void 0);
__decorate([
    Output()
], SettingsComponent.prototype, "gridSwitchValueChange", void 0);
__decorate([
    Output()
], SettingsComponent.prototype, "closeCurrentFlyOut", void 0);
SettingsComponent = __decorate([
    Component({
        selector: 'pbi-settings',
        template: "<div class=\"settings-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-cog title-icon\"></i>\r\n            <span class=\"section-title\">Settings</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"pointer property-tab-in-view-selection\" (click)=\"showFormattingSection = !showFormattingSection\">\r\n                <span>FORMATTING</span>\r\n                <span [class.fa-angle-down]=\"!showFormattingSection\" [class.fa-angle-up]=\"showFormattingSection\"\r\n                    class=\"fa basic-format-toggle-icon\"></span>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n    <div class=\"accordion-data-container\" *ngIf=\"showFormattingSection\">\r\n        <div class=\"setting-content\">\r\n            <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.theme) > -1\">\r\n                <label>THEME</label>\r\n                <div>\r\n                    <dx-select-box class=\"theme-dropdown\" [items]=\"themeList\" valueExpr=\"key\" displayExpr=\"value\"\r\n                        placeholder=\"Select Theme\" [(value)]=\"currentAppliedTheme\" (onValueChanged)=\"onThemeChanged()\">\r\n                        <div [class.selected-theme]=\"currentAppliedTheme === data.key\" *dxTemplate=\"let data of 'item'\">\r\n                            <div class=\"theme-item\">\r\n                                <div class=\"theme-name\">\r\n                                    {{data.value}}\r\n                                </div>\r\n                                <i class=\"fa fa-check theme-icon\" *ngIf=\"currentAppliedTheme === data.key\"></i>\r\n                            </div>\r\n                        </div>\r\n                    </dx-select-box>\r\n                </div>\r\n            </div>\r\n            <div class=\"grid-control-container\">\r\n                <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.gridLines) > -1\">\r\n                    <label>GRIDLINES</label>\r\n                    <div>\r\n                        <!-- <app-bool-input [(State)]=\"hideGridBorder\" (StateChange)=\"onHideGridChange($event)\">\r\n                        </app-bool-input> -->\r\n                        <label class=\"switch\">\r\n                            <!-- <app-bool-input [(State)]=\"isPinned\"></app-bool-input> -->\r\n                            <span>\r\n                                <label class=\"switch\">\r\n                                    <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"hideGridBorder\"\r\n                                        (ngModelChange)=\"onHideGridChange()\">\r\n                                    <span class=\"slider round\"></span>\r\n                                </label>\r\n                            </span>\r\n                        </label>\r\n\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.autoFit) > -1\">\r\n                    <dx-button class=\"settings-custom-btn-dem auto-fit\" (onClick)=\"onAutoFitChange()\"\r\n                        template=\"DEMButtonTemplate\">\r\n                        <div class=\"DEMButtonTemplate\" *dxTemplate=\"let buttonData of 'DEMButtonTemplate'\">\r\n                            <i class=\"fas fa-arrows-alt-h\"></i>\r\n                            <div class=\"button-text\">Auto Fit</div>\r\n                        </div>\r\n                    </dx-button>\r\n                </div>\r\n            </div>\r\n            <div class=\"section\" *ngIf=\"exportActionList.length > 0\">\r\n                <label>EXPORTS</label>\r\n                <div class=\"export-button-containers\">\r\n                    <div *ngFor=\"let item of exportActionList\">\r\n                        <dx-button class=\"settings-custom-btn\" template=\"buttonTemplate\"\r\n                            (click)=\"exportSectionButtonClick(item)\">\r\n                            <div class=\"button-template\" *dxTemplate=\"let buttonData of 'buttonTemplate'\">\r\n                                <div>\r\n                                    <i class=\"{{item.icon}}\"></i>\r\n                                </div>\r\n                                <div class=\"button-text\">{{item.title}}</div>\r\n                            </div>\r\n                        </dx-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"section\" *ngIf=\"cgfSettingsOptions.indexOf(cgfSettingsEnum.advDem) > -1\">\r\n                <label>ADVANCED MANAGEMENT</label>\r\n                <div>\r\n                    <div>\r\n                        <dx-button class=\"settings-custom-btn-dem\" (onClick)=\"onClickOfDEMButton()\"\r\n                            template=\"DEMButtonTemplate\">\r\n                            <div class=\"DEMButtonTemplate\" *dxTemplate=\"let buttonData of 'DEMButtonTemplate'\">\r\n                                <i class=\"fas fa-wrench\"></i>\r\n                                <div class=\"button-text\">Data Entity Management</div>\r\n                            </div>\r\n                        </dx-button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
        styles: [""]
    })
], SettingsComponent);
export { SettingsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dGluZ3MuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL3NldHRpbmdzL3NldHRpbmdzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXpELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQVUzRSxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtJQWlDNUIsWUFBb0IsY0FBOEIsRUFBVSxNQUFjO1FBQXRELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7UUFoQzFELHVCQUFrQixHQUEyQixFQUFFLENBQUM7UUFrQi9DLDBCQUFxQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDM0MsdUJBQWtCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN6RCxtQkFBYyxHQUFHLEtBQUssQ0FBQztRQUN2QixlQUFVLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUMxQixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QiwwQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDN0IsY0FBUyxHQUFvQjtZQUMzQixFQUFFLEdBQUcsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRTtZQUN2QyxFQUFFLEdBQUcsRUFBRSxlQUFlLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRTtZQUMxQyxFQUFFLEdBQUcsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRTtTQUFDLENBQUM7UUFDM0MscUJBQWdCLEdBQWUsRUFBRSxDQUFDO1FBQ2xDLG9CQUFlLEdBQUcsZUFBZSxDQUFDO1FBQ2xDLHdCQUFtQixHQUFHLFlBQVksQ0FBQztJQUUyQyxDQUFDO0lBOUIvRSxJQUFJLGFBQWEsQ0FBQyxTQUFpQjtRQUNqQyxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDNUQ7YUFBTTtZQUNMLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxTQUFTLENBQUM7U0FDdEM7SUFDSCxDQUFDO0lBQ0QsSUFBSSxhQUFhLEtBQUssT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO0lBR3hELElBQUksb0JBQW9CLENBQUMsR0FBWTtRQUNuQyxJQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQztJQUM1QixDQUFDO0lBQ0QsSUFBSSxvQkFBb0IsS0FBSyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO0lBbUJuRCxRQUFRO1FBQ2IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDakgsQ0FBQztJQUVNLGNBQWM7UUFDbkIsY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDMUQsTUFBTSxnQkFBZ0IsR0FBRyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25DLElBQUksZUFBZSxHQUFZLElBQUksQ0FBQyxxQkFBNkIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1lBQ3pGLElBQUksZUFBZSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNyRCxlQUFlLEdBQUcsZUFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNyRjtpQkFBTSxJQUFJLGVBQWUsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDNUQsZUFBZSxHQUFHLGVBQWUsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsbUJBQW1CLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDckY7WUFDQSxJQUFJLENBQUMscUJBQTZCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxHQUFHLGVBQWUsSUFBSSxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3hHLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVNLGdCQUFnQjtRQUNyQixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVNLGtCQUFrQjtRQUN2QixtQkFBbUI7UUFDbkIsMERBQTBEO1FBQzFELCtEQUErRDtRQUMvRCxtQ0FBbUM7UUFDbkMsS0FBSztRQUNMLDRFQUE0RTtJQUM5RSxDQUFDO0lBRU0sZUFBZTtRQUNwQixJQUFJLGtCQUFrQixHQUEyQjtZQUMvQyxxQkFBcUIsRUFBRSxJQUFJLE1BQU0sRUFBZ0I7WUFDakQsUUFBUSxFQUFFLEVBQUU7WUFDWixZQUFZLEVBQUUsS0FBSztZQUNuQixVQUFVLEVBQUUsS0FBSztTQUNsQixDQUFDO1FBQ0YsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7O1lBQzNDLE1BQU0sZUFBZSxHQUFHLE9BQUEsWUFBWSxDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixFQUFFLDBDQUFFLE1BQU0sS0FBSSxDQUFDLENBQUM7WUFDNUYsSUFBSSxhQUFhLEdBQUcsZUFBZSxFQUFFO2dCQUNuQyxrQkFBa0IsR0FBRyxZQUFZLENBQUM7Z0JBQ2xDLGFBQWEsR0FBRyxlQUFlLENBQUM7YUFDakM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZELE1BQU0sUUFBUSxHQUFHLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3hFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDakMsSUFBSSxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxFQUFFO2dCQUN2RSxrQkFBa0IsQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQzthQUMzRTtTQUNGO1FBQ0Qsa0JBQWtCLENBQUMscUJBQXFCLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDdkQsQ0FBQztJQUVPLGVBQWUsQ0FBQyxHQUFXLEVBQUUsS0FBdUI7UUFDMUQsTUFBTSxLQUFLLEdBQWEsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRU0sb0JBQW9CO1FBQ3pCLHNCQUFzQjtRQUN0QixxSUFBcUk7UUFDckksK0JBQStCO0lBQ2pDLENBQUM7SUFFTSxNQUFNO1FBQ1gsb0dBQW9HO1FBQ3BHLG1FQUFtRTtRQUNuRSxNQUFNO0lBQ1IsQ0FBQztJQUVPLGtCQUFrQixDQUFDLFFBQVE7UUFDakMsaUVBQWlFO1FBQ2pFLG1EQUFtRDtRQUNuRCw4RkFBOEY7UUFDOUYsOEJBQThCO1FBQzlCLHdGQUF3RjtRQUN4RixzRUFBc0U7UUFDdEUsZ0hBQWdIO1FBQ2hILElBQUk7SUFDTixDQUFDO0lBRU0sd0JBQXdCLENBQUMsSUFBSTtRQUNsQyxRQUFRLElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDaEIsS0FBSyxhQUFhO2dCQUNoQixNQUFNLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xGLElBQUksZ0JBQWdCLEVBQUU7b0JBQ3BCLCtHQUErRztvQkFDL0csZ0JBQWdCLENBQUMscUJBQXFCLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM3RDtnQkFDRCxNQUFNO1lBQ1IsS0FBSyxXQUFXO2dCQUNkLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDZCxNQUFNO1lBQ1IsS0FBSyxhQUFhO2dCQUNoQixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztnQkFDNUIsTUFBTTtTQUNUO0lBQ0gsQ0FBQztJQUVNLFdBQVc7UUFDaEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7Q0FFRixDQUFBOztZQTlHcUMsY0FBYztZQUFrQixNQUFNOztBQWhDakU7SUFBUixLQUFLLEVBQUU7NkRBQXdEO0FBRWhFO0lBREMsS0FBSyxFQUFFO3NEQU9QO0FBSUQ7SUFEQyxLQUFLLEVBQUU7NkRBR1A7QUFHUTtJQUFSLEtBQUssRUFBRTsyREFBd0Q7QUFDdEQ7SUFBVCxNQUFNLEVBQUU7Z0VBQW1EO0FBQ2xEO0lBQVQsTUFBTSxFQUFFOzZEQUFnRDtBQXBCOUMsaUJBQWlCO0lBTjdCLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxjQUFjO1FBQ3hCLGczS0FBd0M7O0tBRXpDLENBQUM7R0FFVyxpQkFBaUIsQ0ErSTdCO1NBL0lZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCBkeERhdGFHcmlkIGZyb20gJ2RldmV4dHJlbWUvdWkvZGF0YV9ncmlkJztcclxuaW1wb3J0IHsgU2V0dGluZ0V4cG9ydE9wdGlvbnMgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQ0dGU2V0dGluZ3NFbnVtIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2VudW1zJztcclxuaW1wb3J0IHsgZ2V0Q2xhc3NOYW1lQnlUaGVtZU5hbWUgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvdXRpbGl0eUZ1bmN0aW9ucyc7XHJcbmltcG9ydCB7IEdyaWRDb21wb25lbnRJbnN0YW5jZXMgfSBmcm9tICcuLi9jb250cmFjdHMvY29tbW9uLWdyaWQtZnJhbWV3b3JrJztcclxuaW1wb3J0IHsgVmFsdWVTZXQgfSBmcm9tICcuLi9jb250cmFjdHMvdmlldy1zZWxlY3Rpb24nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdwYmktc2V0dGluZ3MnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9zZXR0aW5ncy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vc2V0dGluZ3MuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgU2V0dGluZ3NDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBjZ2ZTZXR0aW5nc09wdGlvbnM6IEFycmF5PENHRlNldHRpbmdzRW51bT4gPSBbXTtcclxuICBASW5wdXQoKVxyXG4gIHNldCBzZWxlY3RlZFRoZW1lKHRoZW1lTmFtZTogc3RyaW5nKSB7XHJcbiAgICBpZiAoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbSgndGhlbWUnKSkge1xyXG4gICAgICB0aGlzLmN1cnJlbnRBcHBsaWVkVGhlbWUgPSBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKCd0aGVtZScpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jdXJyZW50QXBwbGllZFRoZW1lID0gdGhlbWVOYW1lO1xyXG4gICAgfVxyXG4gIH1cclxuICBnZXQgc2VsZWN0ZWRUaGVtZSgpIHsgcmV0dXJuIHRoaXMuY3VycmVudEFwcGxpZWRUaGVtZTsgfVxyXG5cclxuICBASW5wdXQoKVxyXG4gIHNldCBzZWxlY3RlZEJvcmRlclN3aXRjaCh2YWw6IGJvb2xlYW4pIHtcclxuICAgIHRoaXMuaGlkZUdyaWRCb3JkZXIgPSB2YWw7XHJcbiAgfVxyXG4gIGdldCBzZWxlY3RlZEJvcmRlclN3aXRjaCgpIHsgcmV0dXJuIHRoaXMuaGlkZUdyaWRCb3JkZXI7IH1cclxuXHJcbiAgQElucHV0KCkgcHVibGljIGdyaWRJbnN0YW5jZUxpc3Q6IEFycmF5PEdyaWRDb21wb25lbnRJbnN0YW5jZXM+O1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgZ3JpZFN3aXRjaFZhbHVlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgY2xvc2VDdXJyZW50Rmx5T3V0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIGhpZGVHcmlkQm9yZGVyID0gZmFsc2U7XHJcbiAgcmFkaW9Hcm91cCA9IFsnQXV0byBGaXQnXTtcclxuICByYWRpb0dyb3VwVmFsdWUgPSBudWxsO1xyXG4gIHNob3dGb3JtYXR0aW5nU2VjdGlvbiA9IHRydWU7XHJcbiAgdGhlbWVMaXN0OiBBcnJheTxWYWx1ZVNldD4gPSBbXHJcbiAgICB7IGtleTogJ2xpZ2h0LnJlZ3VsYXInLCB2YWx1ZTogJ0NvenknIH0sXHJcbiAgICB7IGtleTogJ2xpZ2h0LmNvbXBhY3QnLCB2YWx1ZTogJ1JlZ3VsYXInIH0sXHJcbiAgICB7IGtleTogJ25wLmNvbXBhY3QnLCB2YWx1ZTogJ0RlZmF1bHQnIH1dO1xyXG4gIGV4cG9ydEFjdGlvbkxpc3Q6IEFycmF5PGFueT4gPSBbXTtcclxuICBjZ2ZTZXR0aW5nc0VudW0gPSBDR0ZTZXR0aW5nc0VudW07XHJcbiAgY3VycmVudEFwcGxpZWRUaGVtZSA9ICducC5jb21wYWN0JztcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhY3RpdmF0ZWRSb3V0ZTogQWN0aXZhdGVkUm91dGUsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHsgfVxyXG5cclxuICBwdWJsaWMgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmV4cG9ydEFjdGlvbkxpc3QgPSBTZXR0aW5nRXhwb3J0T3B0aW9ucy5maWx0ZXIob3B0aW9uID0+IHRoaXMuY2dmU2V0dGluZ3NPcHRpb25zLmluZGV4T2Yob3B0aW9uLmlkKSA+IC0xKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvblRoZW1lQ2hhbmdlZCgpOiB2b2lkIHtcclxuICAgIHNlc3Npb25TdG9yYWdlLnNldEl0ZW0oJ3RoZW1lJywgdGhpcy5jdXJyZW50QXBwbGllZFRoZW1lKTtcclxuICAgIGNvbnN0IGFwcGxpZWRUaGVtZU5hbWUgPSBnZXRDbGFzc05hbWVCeVRoZW1lTmFtZSh0aGlzLmN1cnJlbnRBcHBsaWVkVGhlbWUpO1xyXG4gICAgdGhpcy5ncmlkSW5zdGFuY2VMaXN0LmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIGxldCBleGlzdGluZ0NsYXNzZXM6IHN0cmluZyA9IChpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSBhcyBhbnkpLl8kZWxlbWVudFswXS5jbGFzc05hbWU7XHJcbiAgICAgIGlmIChleGlzdGluZ0NsYXNzZXMuaW5kZXhPZignZHgtc3dhdGNoLWRlZmF1bHQnKSA+IC0xKSB7XHJcbiAgICAgICAgZXhpc3RpbmdDbGFzc2VzID0gZXhpc3RpbmdDbGFzc2VzLnJlcGxhY2UobmV3IFJlZ0V4cCgnZHgtc3dhdGNoLWRlZmF1bHQnLCAnZycpLCAnJyk7XHJcbiAgICAgIH0gZWxzZSBpZiAoZXhpc3RpbmdDbGFzc2VzLmluZGV4T2YoJ2R4LXN3YXRjaC1yZWd1bGFyJykgPiAtMSkge1xyXG4gICAgICAgIGV4aXN0aW5nQ2xhc3NlcyA9IGV4aXN0aW5nQ2xhc3Nlcy5yZXBsYWNlKG5ldyBSZWdFeHAoJ2R4LXN3YXRjaC1yZWd1bGFyJywgJ2cnKSwgJycpO1xyXG4gICAgICB9XHJcbiAgICAgIChpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSBhcyBhbnkpLl8kZWxlbWVudFswXS5jbGFzc05hbWUgPSBgJHtleGlzdGluZ0NsYXNzZXN9ICR7YXBwbGllZFRoZW1lTmFtZX1gO1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmVtaXRUb2dnbGVWYWx1ZSgndGhlbWUnLCB0aGlzLmN1cnJlbnRBcHBsaWVkVGhlbWUpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uSGlkZUdyaWRDaGFuZ2UoKTogdm9pZCB7XHJcbiAgICB0aGlzLmVtaXRUb2dnbGVWYWx1ZSgnZ3JpZExpbmVzJywgdGhpcy5oaWRlR3JpZEJvcmRlcik7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25DbGlja09mREVNQnV0dG9uKCk6IHZvaWQge1xyXG4gICAgLy8gY29uc3QgcGFyYW1zID0ge1xyXG4gICAgLy8gICB3ZWJNZW51SWQ6IGdldFdlYk1lbnVJZEJ5S2V5KCdEYXRhRW50aXR5TWFuYWdlbWVudCcpLFxyXG4gICAgLy8gICBlbnRpdHlJZDogdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5xdWVyeVBhcmFtcy5lbnRpdHksXHJcbiAgICAvLyAgIGFjdGl2ZVRhYjogJ2RhdGFicm93c2VyZW50aXR5J1xyXG4gICAgLy8gfTtcclxuICAgIC8vIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL2RhdGFFbnRpdHlNYW5hZ2VtZW50J10sIHsgcXVlcnlQYXJhbXM6IHBhcmFtcyB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkF1dG9GaXRDaGFuZ2UoKTogdm9pZCB7XHJcbiAgICBsZXQgZ3JpZEluc3RhbmNlTWF4Q29sOiBHcmlkQ29tcG9uZW50SW5zdGFuY2VzID0ge1xyXG4gICAgICBncmlkQ29tcG9uZW50SW5zdGFuY2U6IG5ldyBPYmplY3QoKSBhcyBkeERhdGFHcmlkLFxyXG4gICAgICBncmlkTmFtZTogJycsXHJcbiAgICAgIGlzTWFzdGVyR3JpZDogZmFsc2UsXHJcbiAgICAgIGlzU2VsZWN0ZWQ6IGZhbHNlXHJcbiAgICB9O1xyXG4gICAgbGV0IGV4aXN0Q29sQ291bnQgPSAwO1xyXG4gICAgdGhpcy5ncmlkSW5zdGFuY2VMaXN0LmZvckVhY2goZ3JpZEluc3RhbmNlID0+IHtcclxuICAgICAgY29uc3QgdmlzaWJsZUNvbENvdW50ID0gZ3JpZEluc3RhbmNlLmdyaWRDb21wb25lbnRJbnN0YW5jZS5nZXRWaXNpYmxlQ29sdW1ucygpPy5sZW5ndGggfHwgMDtcclxuICAgICAgaWYgKGV4aXN0Q29sQ291bnQgPCB2aXNpYmxlQ29sQ291bnQpIHtcclxuICAgICAgICBncmlkSW5zdGFuY2VNYXhDb2wgPSBncmlkSW5zdGFuY2U7XHJcbiAgICAgICAgZXhpc3RDb2xDb3VudCA9IHZpc2libGVDb2xDb3VudDtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgZ3JpZEluc3RhbmNlTWF4Q29sLmdyaWRDb21wb25lbnRJbnN0YW5jZS5iZWdpblVwZGF0ZSgpO1xyXG4gICAgY29uc3QgY29sQ291bnQgPSBncmlkSW5zdGFuY2VNYXhDb2wuZ3JpZENvbXBvbmVudEluc3RhbmNlLmNvbHVtbkNvdW50KCk7XHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGNvbENvdW50OyBpKyspIHtcclxuICAgICAgaWYgKGdyaWRJbnN0YW5jZU1heENvbC5ncmlkQ29tcG9uZW50SW5zdGFuY2UuY29sdW1uT3B0aW9uKGksICd2aXNpYmxlJykpIHtcclxuICAgICAgICBncmlkSW5zdGFuY2VNYXhDb2wuZ3JpZENvbXBvbmVudEluc3RhbmNlLmNvbHVtbk9wdGlvbihpLCAnd2lkdGgnLCAnYXV0bycpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBncmlkSW5zdGFuY2VNYXhDb2wuZ3JpZENvbXBvbmVudEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBlbWl0VG9nZ2xlVmFsdWUoa2V5OiBzdHJpbmcsIHZhbHVlOiBib29sZWFuIHwgc3RyaW5nKSB7XHJcbiAgICBjb25zdCBfZGF0YTogVmFsdWVTZXQgPSB7IGtleSwgdmFsdWUgfTtcclxuICAgIHRoaXMuZ3JpZFN3aXRjaFZhbHVlQ2hhbmdlLmVtaXQoX2RhdGEpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uV29ya0Zsb3dFbWFpbENsaWNrKCk6IHZvaWQge1xyXG4gICAgLy8gY29uc3QgcmVkaXJlY3RVUkwgPVxyXG4gICAgLy8gICB0aGlzLmNvbmZpZ1NlcnZpY2UuY29uZmlnLldvcmtGbG93TWFzdGVyQmFzZVVSTCArICcvIy9jcmVhdG9yL2V4cG9ydHJlcG9ydGluZ3BsYXRmb3JtP3VybD0nICsgZW5jb2RlVVJJQ29tcG9uZW50KGxvY2F0aW9uLmhyZWYpO1xyXG4gICAgLy8gb3BlbihyZWRpcmVjdFVSTCwgJ19ibGFuaycpO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIGdldEtleSgpOiB2b2lkIHtcclxuICAgIC8vIHRoaXMuY2dmU2VydmljZS5nZXRBcGlLZXkoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4geyB0aGlzLmdlbmVyYXRlVXJsU3VjY2VzcyhyZXNwb25zZSk7IH0sICgpID0+IHtcclxuICAgIC8vICAgYXBpQ2FsbEVycm9yKCdFcnJvciB3aGlsZSBnZW5lcmF0aW5nIGtleS4gUGxlYXNlIHRyeSBhZ2Fpbi4nKTtcclxuICAgIC8vIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZW5lcmF0ZVVybFN1Y2Nlc3MocmVzcG9uc2UpOiB2b2lkIHtcclxuICAgIC8vIGlmIChyZXNwb25zZS5Ub2FzdGVyVHlwZS50b0xvd2VyQ2FzZSgpLnRyaW0oKSA9PT0gJ3N1Y2Nlc3MnKSB7XHJcbiAgICAvLyAgIGNvbnN0IHVybFBhcmFtcyA9IGxvY2F0aW9uLmhhc2guc3BsaXQoJz8nKVsxXTtcclxuICAgIC8vICAgY29uc3QgdXJsID0gYCR7QVBJRW5kUG9pbnRzLmRhdGFCcm93c2VyLmV4Y2VsRGF0YX0/JHt1cmxQYXJhbXN9JmtleT0ke3Jlc3BvbnNlLk1lc3NhZ2V9YDtcclxuICAgIC8vICAgY29weVRleHRUb0NsaXBCb2FyZCh1cmwpO1xyXG4gICAgLy8gICBhcHBUb2FzdHIoeyB0eXBlOiAnc3VjY2VzcycsIG1lc3NhZ2U6ICdMaW5rIGdlbmVyYXRlZCBhbmQgY29waWVkIHN1Y2Nlc3NmdWxseS4nIH0pO1xyXG4gICAgLy8gfSBlbHNlIGlmIChyZXNwb25zZS5Ub2FzdGVyVHlwZS50b0xvd2VyQ2FzZSgpLnRyaW0oKSA9PT0gJ2Vycm9yJykge1xyXG4gICAgLy8gICBhcHBUb2FzdHIoeyB0eXBlOiAnZXJyb3InLCBtZXNzYWdlOiByZXNwb25zZS5NZXNzYWdlIHx8ICdFcnJvciB3aGlsZSBnZW5lcmF0aW5nIGtleS4gUGxlYXNlIHRyeSBhZ2Fpbi4nIH0pO1xyXG4gICAgLy8gfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGV4cG9ydFNlY3Rpb25CdXR0b25DbGljayhkYXRhKTogdm9pZCB7XHJcbiAgICBzd2l0Y2ggKGRhdGEua2V5KSB7XHJcbiAgICAgIGNhc2UgJ2V4cG9ydEV4Y2VsJzpcclxuICAgICAgICBjb25zdCBzZWxlY3RlZEluc3RhbmNlID0gdGhpcy5ncmlkSW5zdGFuY2VMaXN0LmZpbHRlcihpdGVtID0+IGl0ZW0uaXNTZWxlY3RlZClbMF07XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkSW5zdGFuY2UpIHtcclxuICAgICAgICAgIC8vIFdoZW4gdGhlIHNlbGVjdGlvbk9ubHkgcGFyYW1ldGVyIGlzIGZhbHNlIC0gdGhlIG1ldGhvZCBleHBvcnRzIGFsbCByb3dzLCB3aGVuIHRydWUgLSBvbmx5IHRoZSBzZWxlY3RlZCBvbmVzLlxyXG4gICAgICAgICAgc2VsZWN0ZWRJbnN0YW5jZS5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZXhwb3J0VG9FeGNlbChmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGJyZWFrO1xyXG4gICAgICBjYXNlICdleGNlbExpbmsnOlxyXG4gICAgICAgIHRoaXMuZ2V0S2V5KCk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICAgIGNhc2UgJ2VtYWlsUmVwb3J0JzpcclxuICAgICAgICB0aGlzLm9uV29ya0Zsb3dFbWFpbENsaWNrKCk7XHJcbiAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2xvc2VGbHlPdXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmNsb3NlQ3VycmVudEZseU91dC5lbWl0KCk7XHJcbiAgfVxyXG5cclxufVxyXG4iXX0=