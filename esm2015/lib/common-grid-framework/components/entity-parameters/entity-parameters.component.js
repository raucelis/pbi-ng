import { __decorate } from "tslib";
import Resizable from 'devextreme/ui/resizable';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { DxTreeViewComponent } from 'devextreme-angular';
import { appToast, convertDateToUsFormat } from '../../utilities/utilityFunctions';
import { dateShortcuts } from '../../utilities/constants';
import { CGFEventsEnum } from '../../utilities/enums';
let EntityParametersComponent = class EntityParametersComponent {
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.dataBrowserEntityParameters = [];
        this.disableRefreshIcon = new EventEmitter();
        this.entityParamEvent = new EventEmitter();
        this.flyOutSelectionClick = new EventEmitter();
        this.dateShortcutsItems = dateShortcuts;
        this.disableParameterApply = true;
        this.dxDateBox = {
            dateParam: {
                type: 'date',
                onFocusOut: (e) => {
                    if (!e.component.option('isValid')) {
                        e.component.option('value', '');
                    }
                }
            }
        };
        this.gridInstances = [];
        this.sortable = null;
        this.dropDownOptions = {
            resizeEnabled: true,
            onContentReady: (e) => {
                const DOM = e.component._$content;
                const instance = Resizable.getInstance(DOM);
                instance.option('handles', 'left right');
            }
        };
    }
    ngDoCheck() {
        let formComplete = true;
        if (this.dataBrowserEntityParameters.length === 1
            && this.dataBrowserEntityParameters[0].Type.toLowerCase() === 'userid') {
            formComplete = false;
        }
        else {
            for (let i = 0; i < this.dataBrowserEntityParameters.length; i++) {
                const param = this.dataBrowserEntityParameters[i];
                if ((param.Value === null || param.Value === undefined || param.Value === '') && !param.IsOptional && param.DataType.toLowerCase() !== 'boolean') {
                    formComplete = false;
                    break;
                }
            }
        }
        if (this.disableParameterApply === formComplete) {
            this.disableParameterApply = !formComplete;
            this.disableRefreshIcon.emit(this.disableParameterApply);
        }
    }
    ngAfterViewInit() {
        let allDefaultArePresent = true;
        this.dataBrowserEntityParameters.forEach((item) => {
            const isList = item.Type.toLowerCase() === 'sqllist' || item.Type.toLowerCase() === 'list';
            if (((!item.DefaultValue && isList) || (!isList && !item.DefaultSingleValue)) && !item.IsOptional) {
                allDefaultArePresent = false;
            }
            if (item.DataType.toLowerCase() === 'boolean') {
                item.Value = (item.Value === false || (item.Value && (item.Value === '0' || item.Value === 'false'))) ? false : true;
            }
        });
        if (allDefaultArePresent) {
            this.applyParameters();
        }
        this.sortable = new window['Sortable'](document.getElementById('parameter-list-container'), {
            animation: 150,
            handle: '.drag-icon',
            onEnd: (args) => {
                if (this.dataBrowserEntityParameters[args.oldIndex].Type.toLowerCase() === 'list' || this.dataBrowserEntityParameters[args.oldIndex].Type.toLowerCase() === 'sqllist') {
                    this.gridInstances.forEach((item) => {
                        if (item.order === args.oldIndex) {
                            item.order = args.newIndex;
                        }
                    });
                }
                this.dataBrowserEntityParameters.splice(args.newIndex, 0, this.dataBrowserEntityParameters.splice(args.oldIndex, 1)[0]);
                this.dataBrowserEntityParameters.forEach((item, index) => {
                    item.Order = index;
                });
            }
        });
    }
    templateFunction(data) {
        return '<div class=\'custom-option-item\' title=\'' + data + '\'>' + data + '</div>';
    }
    applyParameters() {
        const paramterMapping = this.dataBrowserEntityParameters;
        const windowObj = window;
        const params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        let queryParamsUpdated = false;
        paramterMapping.forEach((item) => {
            const name = item.OriginalName || item.Name;
            let paramValue = null;
            if (item.DataType.toLowerCase() === 'datetime') {
                paramValue = item.dateShortcutId || windowObj.moment(item.Value).format('M/D/YYYY');
                if (paramValue.toString() !== params[name]) {
                    params[name] = paramValue;
                    queryParamsUpdated = true;
                }
            }
            else if (item.DataType.toLowerCase() === 'boolean' && !item.Value && params[name] !== 'false') {
                params[name] = false;
                queryParamsUpdated = true;
                // tslint:disable-next-line:triple-equals
            }
            else if ((item.Value == 0 || item.Value) && params[name] !== item.Value.toString()) {
                params[name] = item.Value;
                queryParamsUpdated = true;
            }
            else if (((this.IsNullEmptyOrWhiteSpace(item.Value) && !this.IsNullEmptyOrWhiteSpace(params[name]))
                || (!this.IsNullEmptyOrWhiteSpace(item.Value) && this.IsNullEmptyOrWhiteSpace(params[name]))) && item.IsOptional) {
                params[name] = !this.IsNullEmptyOrWhiteSpace(item.Value) ? item.Value : null;
                if (!this.IsNullEmptyOrWhiteSpace(params[name])) {
                    item.Value = params[name];
                }
                queryParamsUpdated = true;
            }
        });
        if (!queryParamsUpdated) {
            return;
        }
        this.router.navigate(['/dataBrowser'], { queryParams: params });
        this.entityParamEvent.emit({ event: CGFEventsEnum.applyEntityParameters, data: paramterMapping });
        this.flyOutSelectionClick.emit({ showEntityParameter: false });
    }
    // on selection change of grid records
    selectionChangedHandler(selectedRowEvent, order) {
        const result = [];
        if (selectedRowEvent.selectedRowsData.length > 100) { // 100 is the to restrict and prevent DB parameters issue.
            appToast({ type: 'error', message: 'Only 100 items can be selected.' });
            selectedRowEvent.component.deselectRows(selectedRowEvent.currentSelectedRowKeys);
            return;
        }
        selectedRowEvent.selectedRowsData.forEach((item) => { result.push(item.data); });
        this.dataBrowserEntityParameters[order].Value = result.join(',');
    }
    // when grid is loaded with content
    onContentReadyHandler(e, order) {
        const lastPage = document.querySelector('.dx-page-sizes .dx-selection');
        if (lastPage !== null && lastPage.textContent === '0') {
            lastPage.textContent = 'ALL';
        }
        if (this.dataBrowserEntityParameters[order].Value) {
            const selectedIds = [];
            const arraySelectedValues = this.dataBrowserEntityParameters[order].Value.split(',');
            arraySelectedValues.forEach(r => {
                for (let i = 0; i < this.dataBrowserEntityParameters[order].DefaultListValue.length; i++) {
                    if (r.trim() === this.dataBrowserEntityParameters[order].DefaultListValue[i].trim()) {
                        selectedIds.push(i);
                    }
                }
            });
            e.component.selectRows(selectedIds);
        }
        // overwrite text of page size 0 to All.
        setTimeout(() => {
            const elm = document.querySelector('.dx-selectbox');
            if (elm != null) {
                document.querySelector('.dx-selectbox').addEventListener('click', function () {
                    setTimeout(() => {
                        document.querySelector('.dx-scrollview-content').lastElementChild.textContent = 'ALL';
                    }, 0);
                });
            }
        }, 0);
        this.onParameterDropDownOpened();
        // when page sizes are not showing by dropdown
        const el = document.querySelector('.dx-page-size');
        if (el !== null) {
            if (el.textContent === 'ALL') {
                const elm = document.querySelector('.dx-page-sizes .dx-selection');
                elm.setAttribute('style', 'pointer-events: none');
            }
            else {
                const elm = document.querySelector('.dx-page-sizes .dx-selection');
                elm.setAttribute('style', 'pointer-events: none');
            }
        }
    }
    // to clear dropdown selection
    onValueChangedDropDownBox(e, order) {
        if (e.value === null) {
            // clear grid selection
            const gridInstance = this.getGridInstance(order);
            if (gridInstance) {
                gridInstance.clearSelection();
            }
            this.dataBrowserEntityParameters[order].Value = '';
        }
    }
    // when grid is initialized
    onInitializedHandler(e, order) {
        const object = {
            order: order,
            gridInstance: e.component
        };
        if (!this.gridInstances.filter((r) => r.order === order).length) {
            this.gridInstances.push(object);
        }
        const gridOptions = this.prepareGridOptions(this.dataBrowserEntityParameters[order]);
        setTimeout(() => {
            e.component.beginUpdate();
            if (gridOptions.DataSource.store.data.length > 100) { // 100 is the to restrict and prevent DB parameters issue..
                e.component.option('showColumnHeaders', false);
            }
            e.component.option('dataSource', gridOptions.DataSource);
            e.component.option('pager', gridOptions.Pager);
            e.component.option('paging', gridOptions.Paging);
            e.component.endUpdate();
        }, 0);
    }
    // prepare grid data source with Id as key
    prepareGridOptions(item) {
        const jsonDefaultValues = [];
        item.DefaultListValue.forEach((r, index) => {
            const object = {};
            object['Id'] = index;
            object['data'] = r;
            jsonDefaultValues.push(object);
        });
        const gridOptions = {
            DataSource: {
                store: {
                    type: 'array',
                    key: 'Id',
                    data: jsonDefaultValues
                }
            },
            Pager: { showPageSizeSelector: true, allowedPageSizes: [30, 0], showInfo: true, visible: true },
            Paging: { enabled: true, pageSize: 30 }
        };
        if (jsonDefaultValues.length <= 30) {
            gridOptions.Paging.enabled = false;
            gridOptions.Pager.visible = false;
        }
        return gridOptions;
    }
    getGridInstance(order) {
        const filterInstance = this.gridInstances.filter((r) => r.order === order)[0];
        return filterInstance ? filterInstance.gridInstance : null;
    }
    onDateValueChanged(e, item) {
        if (e.value) {
            item.Value = convertDateToUsFormat(e.value);
        }
        this.dateShortcutContentReady(item);
    }
    /**
    * Method will be called on content ready of datebox.
    * @returns void
    */
    dateShortcutContentReady(item) {
        item.dateShortcutIcon = 'fal fa-calendar-alt';
        item.dateShortcutId = '';
        if (item.Value) {
            for (let index = 0; index < this.dateShortcutsItems.length; index++) {
                if (this.dateShortcutsItems[index].Value === item.Value) {
                    item.dateShortcutIcon = 'fas fa-calendar-alt';
                    item.dateShortcutId = this.dateShortcutsItems[index].Id;
                }
            }
        }
    }
    dateShortcutOpen(item) {
        this.dateShortcutsItems.forEach(data => {
            if (data.Id === item.dateShortcutId) {
                data.IsSelected = true;
            }
            else {
                data.IsSelected = false;
            }
        });
    }
    /**
     * Method will be called on date short cut click.
     * @returns void
     */
    dateShortcutItemClick(selectedData, globalItem) {
        const item = selectedData.itemData;
        item.IsSelected = !item.IsSelected;
        if (item.IsSelected) {
            const prevSelected = this.dateShortcutsItems.filter(data => data.IsSelected);
            if (prevSelected && prevSelected.length && prevSelected[0].Id !== item.Id) {
                prevSelected[0].IsSelected = false;
            }
            globalItem.dateShortcutId = item.Id;
            globalItem.dateShortcutIcon = 'fas fa-calendar-alt';
            globalItem.Value = item.Value;
        }
        else {
            globalItem.dateShortcutIcon = 'fal fa-calendar-alt';
            globalItem.dateShortcutId = '';
        }
    }
    IsNullEmptyOrWhiteSpace(value) {
        const v = value ? `${value}`.trim() : '';
        return (v === null || v === undefined || v === '');
    }
    onParameterDropDownOpened(args) {
        if (args) {
            // resetting the width to parent control
            args.component.option('dropDownOptions.width', 254);
        }
        const colHtmlCollection = document.getElementsByClassName('entity-param-grid col');
        if (colHtmlCollection.length) {
            setTimeout(() => {
                for (let index = 0; index < colHtmlCollection.length; index++) {
                    if (index % 2 === 0) {
                        colHtmlCollection[index].setAttribute('style', 'width: 25px');
                    }
                }
            }, 0);
        }
    }
    onKeyDown_Grid(evt) {
        if (evt.component.option('dataSource').store.data.length > 100) { // 100 is the to restrict and prevent DB parameters issue.
            if (evt.event.ctrlKey && evt.event.keyCode === 65) { // ctrl+a
                appToast({ type: 'error', message: 'Only 100 items can be selected.' });
                evt.event.preventDefault();
            }
        }
    }
    onMouseOver(item) {
        item.showBorder = true;
    }
    onMouseOut(item) {
        item.showBorder = false;
    }
};
EntityParametersComponent.ctorParameters = () => [
    { type: Router },
    { type: ActivatedRoute }
];
__decorate([
    ViewChild(DxTreeViewComponent)
], EntityParametersComponent.prototype, "treeView", void 0);
__decorate([
    Input()
], EntityParametersComponent.prototype, "dataBrowserEntityParameters", void 0);
__decorate([
    Output()
], EntityParametersComponent.prototype, "disableRefreshIcon", void 0);
__decorate([
    Output()
], EntityParametersComponent.prototype, "entityParamEvent", void 0);
__decorate([
    Output()
], EntityParametersComponent.prototype, "flyOutSelectionClick", void 0);
EntityParametersComponent = __decorate([
    Component({
        selector: 'pbi-entity-parameters',
        template: "<div>\r\n    <div class=\"row no-margin\">\r\n        <div id=\"parameter-list-container\">\r\n            <div [class.parameter-item-container]=\"!item.HideInDataBrowser\"\r\n                *ngFor=\"let item of dataBrowserEntityParameters;\">\r\n                <div *ngIf=\"!item.HideInDataBrowser\" class=\"display-inline-block hideInBrowser\">\r\n                    <div class=\"display-inline-block\"\r\n                        *ngIf=\"item.DataType.toLowerCase() === 'datetime' &&  (item.Type.toLowerCase()=== 'single'|| item.Type.toLowerCase()==='sqlsingle' || item.Type.toLowerCase()==='static')\">\r\n                        <div class=\"form-group display-inline-block\">\r\n                            <label class=\"input-label label-padding-top\">{{item.Name}}\r\n                                <i class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                    (mouseout)=\"onMouseOut(item)\"></i>\r\n                            </label>\r\n                            <dx-date-box width=\"219px\" (focusOut)=\"dxDateBox.onFocusOut($event)\" [(value)]=\"item.Value\"\r\n                                (onContentReady)=\"dateShortcutContentReady(item)\"\r\n                                (onValueChanged)=\"onDateValueChanged($event, item)\"\r\n                                class=\"input-element single-date-picker\"\r\n                                invalidDateMessage='Value must be a valid date'>\r\n                                <dx-validator>\r\n                                    <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                        message=\"{{item.Name + ' is required. '}}\">\r\n                                    </dxi-validation-rule>\r\n                                </dx-validator>\r\n                            </dx-date-box>\r\n                        </div>\r\n                        <div class=\"form-group display-inline-block\" style=\"vertical-align: bottom; margin-left: 10px;\">\r\n                            <label class=\"input-label\">&nbsp;</label>\r\n                            <dx-drop-down-button\r\n                                [icon]=\"item.dateShortcutIcon ? item.dateShortcutIcon :'fal fa-calendar-alt'\"\r\n                                displayExpr=\"Name\" [items]=\"dateShortcutsItems\" valueExpr=\"Id\"\r\n                                class=\"input-dropdown-button\" itemTemplate=\"listItem\"\r\n                                (onButtonClick)=\"dateShortcutOpen(item)\"\r\n                                (onItemClick)=\"dateShortcutItemClick($event,item)\">\r\n                                <div *dxTemplate=\"let data of 'listItem'\"\r\n                                    [class.active-item]=\"data.IsSelected && (item.dateShortcutId === data.Id)\">\r\n                                    <label class=\"date-short-cut-item-label\">{{data.Name}}</label>\r\n                                </div>\r\n                                <dxo-drop-down-options width=\"300px\">\r\n                                </dxo-drop-down-options>\r\n                            </dx-drop-down-button>\r\n                        </div>\r\n                    </div>\r\n                    <ng-template [ngIf]=\"item.DataType.toLowerCase() === 'boolean'\">\r\n                        <div class=\"form-group display-inline-block\">\r\n                            <label class=\"input-label display-block\">{{item.Name}}\r\n                                <i class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                    (mouseout)=\"onMouseOut(item)\"></i>\r\n                            </label>\r\n                            <div>\r\n                                <!-- <app-bool-input [(State)]=\"item.Value\"></app-bool-input> -->\r\n                                <!-- <label class=\"switch\">\r\n                                    <span>\r\n                                        <label class=\"switch\">\r\n                                            <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"item.Value\">\r\n                                            <span class=\"slider round\"></span>\r\n                                        </label>\r\n                                    </span>\r\n                                </label> -->\r\n                            </div>\r\n                        </div>\r\n                    </ng-template>\r\n                    <div *ngIf=\"(item.Type.toLowerCase()=== 'userid')\" class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-text-box width=\"219px\" [disabled]=\"true\" [(value)]=\"item.Value\" class=\"input-element\">\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-text-box>\r\n                    </div>\r\n                    <div *ngIf=\"item.DataType.toLowerCase() === 'string' && (item.Type.toLowerCase()=== 'single'|| item.Type.toLowerCase()==='sqlsingle')\"\r\n                        class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-text-box width=\"219px\" [(value)]=\"item.Value\" class=\"input-element\">\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-text-box>\r\n                    </div>\r\n                    <div *ngIf=\"item.DataType.toLowerCase()==='integer' && (item.Type.toLowerCase()==='single' || item.Type.toLowerCase()==='sqlsingle')\"\r\n                        class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-number-box width=\"219px\" [(value)]=\"item.Value\" class=\"input-element \">\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-number-box>\r\n                    </div>\r\n                    <div *ngIf=\"(item.DataType.toLowerCase()==='datetime' || item.DataType.toLowerCase()==='integer' || item.DataType.toLowerCase()==='string') && (item.Type.toLowerCase()==='list' || item.Type.toLowerCase()==='sqllist')\"\r\n                        class=\"form-group display-inline-block\">\r\n                        <label class=\"input-label label-padding-top\">{{item.Name}} <i\r\n                                class=\"fas fa-grip-vertical drag-icon\" (mouseover)=\"onMouseOver(item)\"\r\n                                (mouseout)=\"onMouseOut(item)\"></i>\r\n                        </label>\r\n                        <dx-drop-down-box width=\"254px\" class=\"input-element\" [(value)]=\"item.Value\"\r\n                            placeholder=\"Select...\" [dataSource]=\"item.DefaultListValue\" valueExpr=\"data\"\r\n                            displayExpr=\"data\" [showClearButton]=\"true\"\r\n                            (onValueChanged)=\"onValueChangedDropDownBox($event,item.Order)\"\r\n                            (onOpened)=\"onParameterDropDownOpened($event)\" [dropDownOptions]=\"dropDownOptions\">\r\n                            <div *dxTemplate=\"let data of 'content'\">\r\n                                <dx-data-grid class=\"entity-param-grid\"\r\n                                    [columns]=\"[{dataField:'data', caption:'Select All'}]\" [keyExpr]=\"data\"\r\n                                    [selection]=\"{ mode: 'multiple', showCheckBoxesMode:'always' }\"\r\n                                    [hoverStateEnabled]=\"true\" [filterRow]=\"{ visible: true }\" [height]=\"345\"\r\n                                    width=\"auto\" [showColumnHeaders]=\"true\" [wordWrapEnabled]=\"true\"\r\n                                    (onInitialized)=\"onInitializedHandler($event, item.Order)\"\r\n                                    (onSelectionChanged)=\"selectionChangedHandler($event,item.Order)\"\r\n                                    (onContentReady)=\"onContentReadyHandler($event,item.Order)\"\r\n                                    (onKeyDown)=\"onKeyDown_Grid($event)\">\r\n                                    <dxo-load-panel [enabled]=\"true\" [shading]=\"true\"\r\n                                        [indicatorSrc]=\"'assets/img/loader/loader-indicator.gif'\">\r\n                                    </dxo-load-panel>\r\n                                </dx-data-grid>\r\n                            </div>\r\n                            <dx-validator>\r\n                                <dxi-validation-rule *ngIf=\"!item.IsOptional\" type=\"required\"\r\n                                    message=\"{{item.Name + ' is required. '}}\">\r\n                                </dxi-validation-rule>\r\n                            </dx-validator>\r\n                        </dx-drop-down-box>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row no-margin text-align-right input-form-footer-button-container \">\r\n            <dx-button class=\"btn-with-blue-bg-color\" text=\"LOAD\" (click)=\"applyParameters()\"\r\n                [disabled]=\"disableParameterApply\"></dx-button>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
        styles: [""]
    })
], EntityParametersComponent);
export { EntityParametersComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW50aXR5LXBhcmFtZXRlcnMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL2VudGl0eS1wYXJhbWV0ZXJzL2VudGl0eS1wYXJhbWV0ZXJzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxTQUFTLE1BQU0seUJBQXlCLENBQUM7QUFDaEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6RCxPQUFPLEVBQWlCLFNBQVMsRUFBVyxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFMUcsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDekQsT0FBTyxFQUFFLFFBQVEsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ25GLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFPdEQsSUFBYSx5QkFBeUIsR0FBdEMsTUFBYSx5QkFBeUI7SUF1QnBDLFlBQW9CLE1BQWMsRUFBVSxjQUE4QjtRQUF0RCxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBckIxRCxnQ0FBMkIsR0FBRyxFQUEyQyxDQUFDO1FBQ3pFLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDeEMscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN0Qyx5QkFBb0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRTNELHVCQUFrQixHQUE4QixhQUFhLENBQUM7UUFDOUQsMEJBQXFCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLGNBQVMsR0FBUTtZQUNmLFNBQVMsRUFBRTtnQkFDVCxJQUFJLEVBQUUsTUFBTTtnQkFDWixVQUFVLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRTtvQkFDaEIsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFO3dCQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztxQkFBRTtnQkFDMUUsQ0FBQzthQUNGO1NBQ0YsQ0FBQztRQUNGLGtCQUFhLEdBQUcsRUFBRSxDQUFDO1FBRW5CLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFLZCxJQUFJLENBQUMsZUFBZSxHQUFHO1lBQ3JCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLGNBQWMsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFO2dCQUNwQixNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQW9CLENBQUM7Z0JBQzdDLE1BQU0sUUFBUSxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFjLENBQUM7Z0JBQ3pELFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFDO1lBQzNDLENBQUM7U0FDRixDQUFDO0lBQ0osQ0FBQztJQUVELFNBQVM7UUFDUCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxJQUFJLENBQUMsMkJBQTJCLENBQUMsTUFBTSxLQUFLLENBQUM7ZUFDNUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLEVBQUU7WUFDeEUsWUFBWSxHQUFHLEtBQUssQ0FBQztTQUN0QjthQUFNO1lBQ0wsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2hFLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDbEQsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxFQUFFO29CQUNoSixZQUFZLEdBQUcsS0FBSyxDQUFDO29CQUNyQixNQUFNO2lCQUNQO2FBQ0Y7U0FDRjtRQUNELElBQUksSUFBSSxDQUFDLHFCQUFxQixLQUFLLFlBQVksRUFBRTtZQUMvQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsQ0FBQyxZQUFZLENBQUM7WUFDM0MsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztTQUMxRDtJQUNILENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxvQkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDaEMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQXlDLEVBQUUsRUFBRTtZQUNyRixNQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLE1BQU0sQ0FBQztZQUMzRixJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqRyxvQkFBb0IsR0FBRyxLQUFLLENBQUM7YUFDOUI7WUFDRCxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxFQUFFO2dCQUM3QyxJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ3RIO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLG9CQUFvQixFQUFFO1lBQ3hCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUN4QjtRQUVELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQywwQkFBMEIsQ0FBQyxFQUFFO1lBQzFGLFNBQVMsRUFBRSxHQUFHO1lBQ2QsTUFBTSxFQUFFLFlBQVk7WUFDcEIsS0FBSyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQ2QsSUFBSSxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxNQUFNLElBQUksSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssU0FBUyxFQUFFO29CQUNySyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO3dCQUNsQyxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLFFBQVEsRUFBRTs0QkFDaEMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO3lCQUM1QjtvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCxJQUFJLENBQUMsMkJBQTJCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4SCxJQUFJLENBQUMsMkJBQTJCLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUN2RCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztnQkFDckIsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1NBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGdCQUFnQixDQUFDLElBQVk7UUFDM0IsT0FBTyw0Q0FBNEMsR0FBRyxJQUFJLEdBQUcsS0FBSyxHQUFHLElBQUksR0FBRyxRQUFRLENBQUM7SUFDdkYsQ0FBQztJQUVELGVBQWU7UUFDYixNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUM7UUFDekQsTUFBTSxTQUFTLEdBQUcsTUFBYSxDQUFDO1FBQ2hDLE1BQU0sTUFBTSxHQUFRLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2hGLElBQUksa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUMvQixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDNUMsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsS0FBSyxVQUFVLEVBQUU7Z0JBQzlDLFVBQVUsR0FBRyxJQUFJLENBQUMsY0FBYyxJQUFJLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEYsSUFBSSxVQUFVLENBQUMsUUFBUSxFQUFFLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDO29CQUMxQixrQkFBa0IsR0FBRyxJQUFJLENBQUM7aUJBQzNCO2FBQ0Y7aUJBQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLE9BQU8sRUFBRTtnQkFDL0YsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQztnQkFDckIsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO2dCQUMxQix5Q0FBeUM7YUFDMUM7aUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsRUFBRTtnQkFDcEYsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzFCLGtCQUFrQixHQUFHLElBQUksQ0FBQzthQUMzQjtpQkFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO21CQUNoRyxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBRWxILE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDN0UsSUFBSSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtvQkFDL0MsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzNCO2dCQUNELGtCQUFrQixHQUFHLElBQUksQ0FBQzthQUMzQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3ZCLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsY0FBYyxDQUFDLEVBQUUsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGFBQWEsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLEVBQUUsZUFBZSxFQUFFLENBQUMsQ0FBQztRQUNsRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEVBQUUsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNqRSxDQUFDO0lBRUQsc0NBQXNDO0lBQ3RDLHVCQUF1QixDQUFDLGdCQUFnQixFQUFFLEtBQWE7UUFDckQsTUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxHQUFHLEdBQUcsRUFBRSxFQUFFLDBEQUEwRDtZQUM5RyxRQUFRLENBQUMsRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxpQ0FBaUMsRUFBRSxDQUFDLENBQUM7WUFDeEUsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQ2pGLE9BQU87U0FDUjtRQUNELGdCQUFnQixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNqRixJQUFJLENBQUMsMkJBQTJCLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVELG1DQUFtQztJQUNuQyxxQkFBcUIsQ0FBQyxDQUFDLEVBQUUsS0FBYTtRQUNwQyxNQUFNLFFBQVEsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFDeEUsSUFBSSxRQUFRLEtBQUssSUFBSSxJQUFJLFFBQVEsQ0FBQyxXQUFXLEtBQUssR0FBRyxFQUFFO1lBQ3JELFFBQVEsQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1NBQzlCO1FBQ0QsSUFBSSxJQUFJLENBQUMsMkJBQTJCLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxFQUFFO1lBQ2pELE1BQU0sV0FBVyxHQUFhLEVBQUUsQ0FBQztZQUNqQyxNQUFNLG1CQUFtQixHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3JGLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDOUIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3hGLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTt3QkFDbkYsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDckI7aUJBQ0Y7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUVILENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3JDO1FBRUQsd0NBQXdDO1FBQ3hDLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDZCxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1lBQ3BELElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtnQkFDZixRQUFRLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLE9BQU8sRUFBRTtvQkFDaEUsVUFBVSxDQUFDLEdBQUcsRUFBRTt3QkFDZCxRQUFRLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztvQkFDeEYsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNSLENBQUMsQ0FBQyxDQUFDO2FBQ0o7UUFDSCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDTixJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztRQUVqQyw4Q0FBOEM7UUFDOUMsTUFBTSxFQUFFLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsQ0FBQztRQUNuRCxJQUFJLEVBQUUsS0FBSyxJQUFJLEVBQUU7WUFDZixJQUFJLEVBQUUsQ0FBQyxXQUFXLEtBQUssS0FBSyxFQUFFO2dCQUM1QixNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLDhCQUE4QixDQUFDLENBQUM7Z0JBQ25FLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLHNCQUFzQixDQUFDLENBQUM7YUFDbkQ7aUJBQU07Z0JBQ0wsTUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO2dCQUNuRSxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO2FBQ25EO1NBQ0Y7SUFDSCxDQUFDO0lBQ0QsOEJBQThCO0lBQzlCLHlCQUF5QixDQUFDLENBQUMsRUFBRSxLQUFLO1FBQ2hDLElBQUksQ0FBQyxDQUFDLEtBQUssS0FBSyxJQUFJLEVBQUU7WUFDcEIsdUJBQXVCO1lBQ3ZCLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDakQsSUFBSSxZQUFZLEVBQUU7Z0JBQUUsWUFBWSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQUU7WUFDcEQsSUFBSSxDQUFDLDJCQUEyQixDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7U0FDcEQ7SUFDSCxDQUFDO0lBRUQsMkJBQTJCO0lBQzNCLG9CQUFvQixDQUFDLENBQUMsRUFBRSxLQUFLO1FBQzNCLE1BQU0sTUFBTSxHQUFHO1lBQ2IsS0FBSyxFQUFFLEtBQUs7WUFDWixZQUFZLEVBQUUsQ0FBQyxDQUFDLFNBQVM7U0FDMUIsQ0FBQztRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsQ0FBQyxNQUFNLEVBQUU7WUFDL0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDakM7UUFDRCxNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDckYsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNkLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDMUIsSUFBSSxXQUFXLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsRUFBRSxFQUFFLDJEQUEyRDtnQkFDL0csQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDaEQ7WUFDRCxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3pELENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDL0MsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNqRCxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzFCLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFRCwwQ0FBMEM7SUFDbEMsa0JBQWtCLENBQUMsSUFBUztRQUNsQyxNQUFNLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUM3QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQWEsRUFBRSxFQUFFO1lBQ2pELE1BQU0sTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNsQixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ3JCLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDbkIsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsTUFBTSxXQUFXLEdBQTZCO1lBQzVDLFVBQVUsRUFBRTtnQkFDVixLQUFLLEVBQUU7b0JBQ0wsSUFBSSxFQUFFLE9BQU87b0JBQ2IsR0FBRyxFQUFFLElBQUk7b0JBQ1QsSUFBSSxFQUFFLGlCQUFpQjtpQkFDeEI7YUFDRjtZQUNELEtBQUssRUFBRSxFQUFFLG9CQUFvQixFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUU7WUFDL0YsTUFBTSxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFO1NBQ3hDLENBQUM7UUFFRixJQUFJLGlCQUFpQixDQUFDLE1BQU0sSUFBSSxFQUFFLEVBQUU7WUFDbEMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ25DLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUNuQztRQUNELE9BQU8sV0FBVyxDQUFDO0lBQ3JCLENBQUM7SUFFTyxlQUFlLENBQUMsS0FBYTtRQUNuQyxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5RSxPQUFPLGNBQWMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQzdELENBQUM7SUFFRCxrQkFBa0IsQ0FBQyxDQUFDLEVBQUUsSUFBSTtRQUN4QixJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUU7WUFDWCxJQUFJLENBQUMsS0FBSyxHQUFHLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM3QztRQUNELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7OztNQUdFO0lBQ0Ysd0JBQXdCLENBQUMsSUFBeUM7UUFDaEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLHFCQUFxQixDQUFDO1FBQzlDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNkLEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO2dCQUNuRSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDdkQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLHFCQUFxQixDQUFDO29CQUM5QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQ3pEO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxJQUF5QztRQUN4RCxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JDLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUNuQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQzthQUN4QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzthQUN6QjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7T0FHRztJQUNILHFCQUFxQixDQUFDLFlBQVksRUFBRSxVQUErQztRQUNqRixNQUFNLElBQUksR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDO1FBQ25DLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ25DLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixNQUFNLFlBQVksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdFLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxNQUFNLElBQUksWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUN6RSxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzthQUNwQztZQUNELFVBQVUsQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUNwQyxVQUFVLENBQUMsZ0JBQWdCLEdBQUcscUJBQXFCLENBQUM7WUFDcEQsVUFBVSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQy9CO2FBQU07WUFDTCxVQUFVLENBQUMsZ0JBQWdCLEdBQUcscUJBQXFCLENBQUM7WUFDcEQsVUFBVSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7U0FDaEM7SUFDSCxDQUFDO0lBRU8sdUJBQXVCLENBQUMsS0FBYTtRQUMzQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUN6QyxPQUFPLENBQUMsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssU0FBUyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQseUJBQXlCLENBQUMsSUFBVTtRQUNsQyxJQUFJLElBQUksRUFBRTtZQUNSLHdDQUF3QztZQUN4QyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyx1QkFBdUIsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUNyRDtRQUNELE1BQU0saUJBQWlCLEdBQUcsUUFBUSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDbkYsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7WUFDNUIsVUFBVSxDQUFDLEdBQUcsRUFBRTtnQkFDZCxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsaUJBQWlCLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUM3RCxJQUFJLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNuQixpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxDQUFDO3FCQUMvRDtpQkFDRjtZQUNILENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNQO0lBQ0gsQ0FBQztJQUVNLGNBQWMsQ0FBQyxHQUFHO1FBQ3ZCLElBQUksR0FBRyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxFQUFFLEVBQUUsMERBQTBEO1lBQzFILElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLEtBQUssRUFBRSxFQUFFLEVBQUUsU0FBUztnQkFDNUQsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsaUNBQWlDLEVBQUUsQ0FBQyxDQUFDO2dCQUN4RSxHQUFHLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQzVCO1NBQ0Y7SUFDSCxDQUFDO0lBRU0sV0FBVyxDQUFDLElBQUk7UUFDckIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7SUFDekIsQ0FBQztJQUVNLFVBQVUsQ0FBQyxJQUFJO1FBQ3BCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7Q0FFRixDQUFBOztZQXBVNkIsTUFBTTtZQUEwQixjQUFjOztBQXRCMUM7SUFBL0IsU0FBUyxDQUFDLG1CQUFtQixDQUFDOzJEQUFVO0FBQ2hDO0lBQVIsS0FBSyxFQUFFOzhFQUFrRjtBQUNoRjtJQUFULE1BQU0sRUFBRTtxRUFBZ0Q7QUFDL0M7SUFBVCxNQUFNLEVBQUU7bUVBQThDO0FBQzdDO0lBQVQsTUFBTSxFQUFFO3VFQUFrRDtBQUxoRCx5QkFBeUI7SUFMckMsU0FBUyxDQUFDO1FBQ1QsUUFBUSxFQUFFLHVCQUF1QjtRQUNqQywrbFZBQWlEOztLQUVsRCxDQUFDO0dBQ1cseUJBQXlCLENBMlZyQztTQTNWWSx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVzaXphYmxlIGZyb20gJ2RldmV4dHJlbWUvdWkvcmVzaXphYmxlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGUsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEFmdGVyVmlld0luaXQsIENvbXBvbmVudCwgRG9DaGVjaywgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzRW52ZWxvcGUsIERhdGVTaG9ydEN1dFBhcmFtcywgUGFyYW1ldGVyVmFsdWVHcmlkT3B0aW9uIH0gZnJvbSAnLi4vY29udHJhY3RzL2NvbW1vbi1ncmlkLWZyYW1ld29yayc7XHJcbmltcG9ydCB7IER4VHJlZVZpZXdDb21wb25lbnQgfSBmcm9tICdkZXZleHRyZW1lLWFuZ3VsYXInO1xyXG5pbXBvcnQgeyBhcHBUb2FzdCwgY29udmVydERhdGVUb1VzRm9ybWF0IH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL3V0aWxpdHlGdW5jdGlvbnMnO1xyXG5pbXBvcnQgeyBkYXRlU2hvcnRjdXRzIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2NvbnN0YW50cyc7XHJcbmltcG9ydCB7IENHRkV2ZW50c0VudW0gfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvZW51bXMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdwYmktZW50aXR5LXBhcmFtZXRlcnMnLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9lbnRpdHktcGFyYW1ldGVycy5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZW50aXR5LXBhcmFtZXRlcnMuY29tcG9uZW50LmNzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFbnRpdHlQYXJhbWV0ZXJzQ29tcG9uZW50IGltcGxlbWVudHMgRG9DaGVjaywgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgQFZpZXdDaGlsZChEeFRyZWVWaWV3Q29tcG9uZW50KSB0cmVlVmlldztcclxuICBASW5wdXQoKSBwdWJsaWMgZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzID0gW10gYXMgRGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzRW52ZWxvcGVbXTtcclxuICBAT3V0cHV0KCkgcHVibGljIGRpc2FibGVSZWZyZXNoSWNvbiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgcHVibGljIGVudGl0eVBhcmFtRXZlbnQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIHB1YmxpYyBmbHlPdXRTZWxlY3Rpb25DbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBjaGVja0JveDogeyBjaGVja2VkOiB7fTsgfTtcclxuICBkYXRlU2hvcnRjdXRzSXRlbXM6IEFycmF5PERhdGVTaG9ydEN1dFBhcmFtcz4gPSBkYXRlU2hvcnRjdXRzO1xyXG4gIGRpc2FibGVQYXJhbWV0ZXJBcHBseSA9IHRydWU7XHJcbiAgZHhEYXRlQm94OiBhbnkgPSB7XHJcbiAgICBkYXRlUGFyYW06IHtcclxuICAgICAgdHlwZTogJ2RhdGUnLFxyXG4gICAgICBvbkZvY3VzT3V0OiAoZSkgPT4ge1xyXG4gICAgICAgIGlmICghZS5jb21wb25lbnQub3B0aW9uKCdpc1ZhbGlkJykpIHsgZS5jb21wb25lbnQub3B0aW9uKCd2YWx1ZScsICcnKTsgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfTtcclxuICBncmlkSW5zdGFuY2VzID0gW107XHJcbiAgc2VsZWN0ZWREYXRlU2hvcnRjdXQ6IHN0cmluZztcclxuICBzb3J0YWJsZSA9IG51bGw7XHJcbiAgZHJvcERvd25PcHRpb25zOiBhbnk7XHJcbiAgdGV4dEJveDogeyBzdHJpbmdUeXBlOiB7fTsgbnVtYmVyVHlwZToge307IH07XHJcblxyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlKSB7XHJcbiAgICB0aGlzLmRyb3BEb3duT3B0aW9ucyA9IHtcclxuICAgICAgcmVzaXplRW5hYmxlZDogdHJ1ZSxcclxuICAgICAgb25Db250ZW50UmVhZHk6IChlKSA9PiB7XHJcbiAgICAgICAgY29uc3QgRE9NID0gZS5jb21wb25lbnQuXyRjb250ZW50IGFzIEVsZW1lbnQ7XHJcbiAgICAgICAgY29uc3QgaW5zdGFuY2UgPSBSZXNpemFibGUuZ2V0SW5zdGFuY2UoRE9NKSBhcyBSZXNpemFibGU7XHJcbiAgICAgICAgaW5zdGFuY2Uub3B0aW9uKCdoYW5kbGVzJywgJ2xlZnQgcmlnaHQnKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIG5nRG9DaGVjaygpOiB2b2lkIHtcclxuICAgIGxldCBmb3JtQ29tcGxldGUgPSB0cnVlO1xyXG4gICAgaWYgKHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzLmxlbmd0aCA9PT0gMVxyXG4gICAgICAmJiB0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVyc1swXS5UeXBlLnRvTG93ZXJDYXNlKCkgPT09ICd1c2VyaWQnKSB7XHJcbiAgICAgIGZvcm1Db21wbGV0ZSA9IGZhbHNlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVycy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgIGNvbnN0IHBhcmFtID0gdGhpcy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNbaV07XHJcbiAgICAgICAgaWYgKChwYXJhbS5WYWx1ZSA9PT0gbnVsbCB8fCBwYXJhbS5WYWx1ZSA9PT0gdW5kZWZpbmVkIHx8IHBhcmFtLlZhbHVlID09PSAnJykgJiYgIXBhcmFtLklzT3B0aW9uYWwgJiYgcGFyYW0uRGF0YVR5cGUudG9Mb3dlckNhc2UoKSAhPT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAgICAgICBmb3JtQ29tcGxldGUgPSBmYWxzZTtcclxuICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZGlzYWJsZVBhcmFtZXRlckFwcGx5ID09PSBmb3JtQ29tcGxldGUpIHtcclxuICAgICAgdGhpcy5kaXNhYmxlUGFyYW1ldGVyQXBwbHkgPSAhZm9ybUNvbXBsZXRlO1xyXG4gICAgICB0aGlzLmRpc2FibGVSZWZyZXNoSWNvbi5lbWl0KHRoaXMuZGlzYWJsZVBhcmFtZXRlckFwcGx5KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcclxuICAgIGxldCBhbGxEZWZhdWx0QXJlUHJlc2VudCA9IHRydWU7XHJcbiAgICB0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVycy5mb3JFYWNoKChpdGVtOiBEYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNFbnZlbG9wZSkgPT4ge1xyXG4gICAgICBjb25zdCBpc0xpc3QgPSBpdGVtLlR5cGUudG9Mb3dlckNhc2UoKSA9PT0gJ3NxbGxpc3QnIHx8IGl0ZW0uVHlwZS50b0xvd2VyQ2FzZSgpID09PSAnbGlzdCc7XHJcbiAgICAgIGlmICgoKCFpdGVtLkRlZmF1bHRWYWx1ZSAmJiBpc0xpc3QpIHx8ICghaXNMaXN0ICYmICFpdGVtLkRlZmF1bHRTaW5nbGVWYWx1ZSkpICYmICFpdGVtLklzT3B0aW9uYWwpIHtcclxuICAgICAgICBhbGxEZWZhdWx0QXJlUHJlc2VudCA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChpdGVtLkRhdGFUeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdib29sZWFuJykge1xyXG4gICAgICAgIGl0ZW0uVmFsdWUgPSAoaXRlbS5WYWx1ZSA9PT0gZmFsc2UgfHwgKGl0ZW0uVmFsdWUgJiYgKGl0ZW0uVmFsdWUgPT09ICcwJyB8fCBpdGVtLlZhbHVlID09PSAnZmFsc2UnKSkpID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGlmIChhbGxEZWZhdWx0QXJlUHJlc2VudCkge1xyXG4gICAgICB0aGlzLmFwcGx5UGFyYW1ldGVycygpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuc29ydGFibGUgPSBuZXcgd2luZG93WydTb3J0YWJsZSddKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdwYXJhbWV0ZXItbGlzdC1jb250YWluZXInKSwge1xyXG4gICAgICBhbmltYXRpb246IDE1MCxcclxuICAgICAgaGFuZGxlOiAnLmRyYWctaWNvbicsXHJcbiAgICAgIG9uRW5kOiAoYXJncykgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVyc1thcmdzLm9sZEluZGV4XS5UeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdsaXN0JyB8fCB0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVyc1thcmdzLm9sZEluZGV4XS5UeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdzcWxsaXN0Jykge1xyXG4gICAgICAgICAgdGhpcy5ncmlkSW5zdGFuY2VzLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICAgICAgaWYgKGl0ZW0ub3JkZXIgPT09IGFyZ3Mub2xkSW5kZXgpIHtcclxuICAgICAgICAgICAgICBpdGVtLm9yZGVyID0gYXJncy5uZXdJbmRleDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzLnNwbGljZShhcmdzLm5ld0luZGV4LCAwLCB0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVycy5zcGxpY2UoYXJncy5vbGRJbmRleCwgMSlbMF0pO1xyXG4gICAgICAgIHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XHJcbiAgICAgICAgICBpdGVtLk9yZGVyID0gaW5kZXg7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgdGVtcGxhdGVGdW5jdGlvbihkYXRhOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgcmV0dXJuICc8ZGl2IGNsYXNzPVxcJ2N1c3RvbS1vcHRpb24taXRlbVxcJyB0aXRsZT1cXCcnICsgZGF0YSArICdcXCc+JyArIGRhdGEgKyAnPC9kaXY+JztcclxuICB9XHJcblxyXG4gIGFwcGx5UGFyYW1ldGVycygpOiB2b2lkIHtcclxuICAgIGNvbnN0IHBhcmFtdGVyTWFwcGluZyA9IHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzO1xyXG4gICAgY29uc3Qgd2luZG93T2JqID0gd2luZG93IGFzIGFueTtcclxuICAgIGNvbnN0IHBhcmFtczogYW55ID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5hY3RpdmF0ZWRSb3V0ZS5zbmFwc2hvdC5xdWVyeVBhcmFtcyk7XHJcbiAgICBsZXQgcXVlcnlQYXJhbXNVcGRhdGVkID0gZmFsc2U7XHJcbiAgICBwYXJhbXRlck1hcHBpbmcuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICBjb25zdCBuYW1lID0gaXRlbS5PcmlnaW5hbE5hbWUgfHwgaXRlbS5OYW1lO1xyXG4gICAgICBsZXQgcGFyYW1WYWx1ZSA9IG51bGw7XHJcbiAgICAgIGlmIChpdGVtLkRhdGFUeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdkYXRldGltZScpIHtcclxuICAgICAgICBwYXJhbVZhbHVlID0gaXRlbS5kYXRlU2hvcnRjdXRJZCB8fCB3aW5kb3dPYmoubW9tZW50KGl0ZW0uVmFsdWUpLmZvcm1hdCgnTS9EL1lZWVknKTtcclxuICAgICAgICBpZiAocGFyYW1WYWx1ZS50b1N0cmluZygpICE9PSBwYXJhbXNbbmFtZV0pIHtcclxuICAgICAgICAgIHBhcmFtc1tuYW1lXSA9IHBhcmFtVmFsdWU7XHJcbiAgICAgICAgICBxdWVyeVBhcmFtc1VwZGF0ZWQgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIGlmIChpdGVtLkRhdGFUeXBlLnRvTG93ZXJDYXNlKCkgPT09ICdib29sZWFuJyAmJiAhaXRlbS5WYWx1ZSAmJiBwYXJhbXNbbmFtZV0gIT09ICdmYWxzZScpIHtcclxuICAgICAgICBwYXJhbXNbbmFtZV0gPSBmYWxzZTtcclxuICAgICAgICBxdWVyeVBhcmFtc1VwZGF0ZWQgPSB0cnVlO1xyXG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTp0cmlwbGUtZXF1YWxzXHJcbiAgICAgIH0gZWxzZSBpZiAoKGl0ZW0uVmFsdWUgPT0gMCB8fCBpdGVtLlZhbHVlKSAmJiBwYXJhbXNbbmFtZV0gIT09IGl0ZW0uVmFsdWUudG9TdHJpbmcoKSkge1xyXG4gICAgICAgIHBhcmFtc1tuYW1lXSA9IGl0ZW0uVmFsdWU7XHJcbiAgICAgICAgcXVlcnlQYXJhbXNVcGRhdGVkID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIGlmICgoKHRoaXMuSXNOdWxsRW1wdHlPcldoaXRlU3BhY2UoaXRlbS5WYWx1ZSkgJiYgIXRoaXMuSXNOdWxsRW1wdHlPcldoaXRlU3BhY2UocGFyYW1zW25hbWVdKSlcclxuICAgICAgICB8fCAoIXRoaXMuSXNOdWxsRW1wdHlPcldoaXRlU3BhY2UoaXRlbS5WYWx1ZSkgJiYgdGhpcy5Jc051bGxFbXB0eU9yV2hpdGVTcGFjZShwYXJhbXNbbmFtZV0pKSkgJiYgaXRlbS5Jc09wdGlvbmFsKSB7XHJcblxyXG4gICAgICAgIHBhcmFtc1tuYW1lXSA9ICF0aGlzLklzTnVsbEVtcHR5T3JXaGl0ZVNwYWNlKGl0ZW0uVmFsdWUpID8gaXRlbS5WYWx1ZSA6IG51bGw7XHJcbiAgICAgICAgaWYgKCF0aGlzLklzTnVsbEVtcHR5T3JXaGl0ZVNwYWNlKHBhcmFtc1tuYW1lXSkpIHtcclxuICAgICAgICAgIGl0ZW0uVmFsdWUgPSBwYXJhbXNbbmFtZV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHF1ZXJ5UGFyYW1zVXBkYXRlZCA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgaWYgKCFxdWVyeVBhcmFtc1VwZGF0ZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvZGF0YUJyb3dzZXInXSwgeyBxdWVyeVBhcmFtczogcGFyYW1zIH0pO1xyXG4gICAgdGhpcy5lbnRpdHlQYXJhbUV2ZW50LmVtaXQoeyBldmVudDogQ0dGRXZlbnRzRW51bS5hcHBseUVudGl0eVBhcmFtZXRlcnMsIGRhdGE6IHBhcmFtdGVyTWFwcGluZyB9KTtcclxuICAgIHRoaXMuZmx5T3V0U2VsZWN0aW9uQ2xpY2suZW1pdCh7IHNob3dFbnRpdHlQYXJhbWV0ZXI6IGZhbHNlIH0pO1xyXG4gIH1cclxuXHJcbiAgLy8gb24gc2VsZWN0aW9uIGNoYW5nZSBvZiBncmlkIHJlY29yZHNcclxuICBzZWxlY3Rpb25DaGFuZ2VkSGFuZGxlcihzZWxlY3RlZFJvd0V2ZW50LCBvcmRlcjogbnVtYmVyKTogdm9pZCB7XHJcbiAgICBjb25zdCByZXN1bHQgPSBbXTtcclxuICAgIGlmIChzZWxlY3RlZFJvd0V2ZW50LnNlbGVjdGVkUm93c0RhdGEubGVuZ3RoID4gMTAwKSB7IC8vIDEwMCBpcyB0aGUgdG8gcmVzdHJpY3QgYW5kIHByZXZlbnQgREIgcGFyYW1ldGVycyBpc3N1ZS5cclxuICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnZXJyb3InLCBtZXNzYWdlOiAnT25seSAxMDAgaXRlbXMgY2FuIGJlIHNlbGVjdGVkLicgfSk7XHJcbiAgICAgIHNlbGVjdGVkUm93RXZlbnQuY29tcG9uZW50LmRlc2VsZWN0Um93cyhzZWxlY3RlZFJvd0V2ZW50LmN1cnJlbnRTZWxlY3RlZFJvd0tleXMpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBzZWxlY3RlZFJvd0V2ZW50LnNlbGVjdGVkUm93c0RhdGEuZm9yRWFjaCgoaXRlbSkgPT4geyByZXN1bHQucHVzaChpdGVtLmRhdGEpOyB9KTtcclxuICAgIHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzW29yZGVyXS5WYWx1ZSA9IHJlc3VsdC5qb2luKCcsJyk7XHJcbiAgfVxyXG5cclxuICAvLyB3aGVuIGdyaWQgaXMgbG9hZGVkIHdpdGggY29udGVudFxyXG4gIG9uQ29udGVudFJlYWR5SGFuZGxlcihlLCBvcmRlcjogbnVtYmVyKTogdm9pZCB7XHJcbiAgICBjb25zdCBsYXN0UGFnZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5keC1wYWdlLXNpemVzIC5keC1zZWxlY3Rpb24nKTtcclxuICAgIGlmIChsYXN0UGFnZSAhPT0gbnVsbCAmJiBsYXN0UGFnZS50ZXh0Q29udGVudCA9PT0gJzAnKSB7XHJcbiAgICAgIGxhc3RQYWdlLnRleHRDb250ZW50ID0gJ0FMTCc7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNbb3JkZXJdLlZhbHVlKSB7XHJcbiAgICAgIGNvbnN0IHNlbGVjdGVkSWRzOiBudW1iZXJbXSA9IFtdO1xyXG4gICAgICBjb25zdCBhcnJheVNlbGVjdGVkVmFsdWVzID0gdGhpcy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNbb3JkZXJdLlZhbHVlLnNwbGl0KCcsJyk7XHJcbiAgICAgIGFycmF5U2VsZWN0ZWRWYWx1ZXMuZm9yRWFjaChyID0+IHtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzW29yZGVyXS5EZWZhdWx0TGlzdFZhbHVlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICBpZiAoci50cmltKCkgPT09IHRoaXMuZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzW29yZGVyXS5EZWZhdWx0TGlzdFZhbHVlW2ldLnRyaW0oKSkge1xyXG4gICAgICAgICAgICBzZWxlY3RlZElkcy5wdXNoKGkpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgICBlLmNvbXBvbmVudC5zZWxlY3RSb3dzKHNlbGVjdGVkSWRzKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBvdmVyd3JpdGUgdGV4dCBvZiBwYWdlIHNpemUgMCB0byBBbGwuXHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgY29uc3QgZWxtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmR4LXNlbGVjdGJveCcpO1xyXG4gICAgICBpZiAoZWxtICE9IG51bGwpIHtcclxuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZHgtc2VsZWN0Ym94JykuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmR4LXNjcm9sbHZpZXctY29udGVudCcpLmxhc3RFbGVtZW50Q2hpbGQudGV4dENvbnRlbnQgPSAnQUxMJztcclxuICAgICAgICAgIH0sIDApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB9LCAwKTtcclxuICAgIHRoaXMub25QYXJhbWV0ZXJEcm9wRG93bk9wZW5lZCgpO1xyXG5cclxuICAgIC8vIHdoZW4gcGFnZSBzaXplcyBhcmUgbm90IHNob3dpbmcgYnkgZHJvcGRvd25cclxuICAgIGNvbnN0IGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmR4LXBhZ2Utc2l6ZScpO1xyXG4gICAgaWYgKGVsICE9PSBudWxsKSB7XHJcbiAgICAgIGlmIChlbC50ZXh0Q29udGVudCA9PT0gJ0FMTCcpIHtcclxuICAgICAgICBjb25zdCBlbG0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZHgtcGFnZS1zaXplcyAuZHgtc2VsZWN0aW9uJyk7XHJcbiAgICAgICAgZWxtLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCAncG9pbnRlci1ldmVudHM6IG5vbmUnKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBlbG0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZHgtcGFnZS1zaXplcyAuZHgtc2VsZWN0aW9uJyk7XHJcbiAgICAgICAgZWxtLnNldEF0dHJpYnV0ZSgnc3R5bGUnLCAncG9pbnRlci1ldmVudHM6IG5vbmUnKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICAvLyB0byBjbGVhciBkcm9wZG93biBzZWxlY3Rpb25cclxuICBvblZhbHVlQ2hhbmdlZERyb3BEb3duQm94KGUsIG9yZGVyKTogdm9pZCB7XHJcbiAgICBpZiAoZS52YWx1ZSA9PT0gbnVsbCkge1xyXG4gICAgICAvLyBjbGVhciBncmlkIHNlbGVjdGlvblxyXG4gICAgICBjb25zdCBncmlkSW5zdGFuY2UgPSB0aGlzLmdldEdyaWRJbnN0YW5jZShvcmRlcik7XHJcbiAgICAgIGlmIChncmlkSW5zdGFuY2UpIHsgZ3JpZEluc3RhbmNlLmNsZWFyU2VsZWN0aW9uKCk7IH1cclxuICAgICAgdGhpcy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNbb3JkZXJdLlZhbHVlID0gJyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvLyB3aGVuIGdyaWQgaXMgaW5pdGlhbGl6ZWRcclxuICBvbkluaXRpYWxpemVkSGFuZGxlcihlLCBvcmRlcik6IHZvaWQge1xyXG4gICAgY29uc3Qgb2JqZWN0ID0ge1xyXG4gICAgICBvcmRlcjogb3JkZXIsXHJcbiAgICAgIGdyaWRJbnN0YW5jZTogZS5jb21wb25lbnRcclxuICAgIH07XHJcbiAgICBpZiAoIXRoaXMuZ3JpZEluc3RhbmNlcy5maWx0ZXIoKHIpID0+IHIub3JkZXIgPT09IG9yZGVyKS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5ncmlkSW5zdGFuY2VzLnB1c2gob2JqZWN0KTtcclxuICAgIH1cclxuICAgIGNvbnN0IGdyaWRPcHRpb25zID0gdGhpcy5wcmVwYXJlR3JpZE9wdGlvbnModGhpcy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNbb3JkZXJdKTtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4geyAvLyBUaW1lb3V0IGlzIHRvIG1ha2Ugc3VyZSBjb21wb25lbnQgaXMgcmVhZHkgdG8gZGlnZXN0IG5ldyBjaGFuZ2VzLlxyXG4gICAgICBlLmNvbXBvbmVudC5iZWdpblVwZGF0ZSgpO1xyXG4gICAgICBpZiAoZ3JpZE9wdGlvbnMuRGF0YVNvdXJjZS5zdG9yZS5kYXRhLmxlbmd0aCA+IDEwMCkgeyAvLyAxMDAgaXMgdGhlIHRvIHJlc3RyaWN0IGFuZCBwcmV2ZW50IERCIHBhcmFtZXRlcnMgaXNzdWUuLlxyXG4gICAgICAgIGUuY29tcG9uZW50Lm9wdGlvbignc2hvd0NvbHVtbkhlYWRlcnMnLCBmYWxzZSk7XHJcbiAgICAgIH1cclxuICAgICAgZS5jb21wb25lbnQub3B0aW9uKCdkYXRhU291cmNlJywgZ3JpZE9wdGlvbnMuRGF0YVNvdXJjZSk7XHJcbiAgICAgIGUuY29tcG9uZW50Lm9wdGlvbigncGFnZXInLCBncmlkT3B0aW9ucy5QYWdlcik7XHJcbiAgICAgIGUuY29tcG9uZW50Lm9wdGlvbigncGFnaW5nJywgZ3JpZE9wdGlvbnMuUGFnaW5nKTtcclxuICAgICAgZS5jb21wb25lbnQuZW5kVXBkYXRlKCk7XHJcbiAgICB9LCAwKTtcclxuICB9XHJcblxyXG4gIC8vIHByZXBhcmUgZ3JpZCBkYXRhIHNvdXJjZSB3aXRoIElkIGFzIGtleVxyXG4gIHByaXZhdGUgcHJlcGFyZUdyaWRPcHRpb25zKGl0ZW06IGFueSk6IFBhcmFtZXRlclZhbHVlR3JpZE9wdGlvbiB7XHJcbiAgICBjb25zdCBqc29uRGVmYXVsdFZhbHVlcyA9IFtdO1xyXG4gICAgaXRlbS5EZWZhdWx0TGlzdFZhbHVlLmZvckVhY2goKHIsIGluZGV4OiBudW1iZXIpID0+IHtcclxuICAgICAgY29uc3Qgb2JqZWN0ID0ge307XHJcbiAgICAgIG9iamVjdFsnSWQnXSA9IGluZGV4O1xyXG4gICAgICBvYmplY3RbJ2RhdGEnXSA9IHI7XHJcbiAgICAgIGpzb25EZWZhdWx0VmFsdWVzLnB1c2gob2JqZWN0KTtcclxuICAgIH0pO1xyXG4gICAgY29uc3QgZ3JpZE9wdGlvbnM6IFBhcmFtZXRlclZhbHVlR3JpZE9wdGlvbiA9IHtcclxuICAgICAgRGF0YVNvdXJjZToge1xyXG4gICAgICAgIHN0b3JlOiB7XHJcbiAgICAgICAgICB0eXBlOiAnYXJyYXknLFxyXG4gICAgICAgICAga2V5OiAnSWQnLFxyXG4gICAgICAgICAgZGF0YToganNvbkRlZmF1bHRWYWx1ZXNcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIFBhZ2VyOiB7IHNob3dQYWdlU2l6ZVNlbGVjdG9yOiB0cnVlLCBhbGxvd2VkUGFnZVNpemVzOiBbMzAsIDBdLCBzaG93SW5mbzogdHJ1ZSwgdmlzaWJsZTogdHJ1ZSB9LCAvLyAwIG1lYW5zIEFsbFxyXG4gICAgICBQYWdpbmc6IHsgZW5hYmxlZDogdHJ1ZSwgcGFnZVNpemU6IDMwIH1cclxuICAgIH07XHJcblxyXG4gICAgaWYgKGpzb25EZWZhdWx0VmFsdWVzLmxlbmd0aCA8PSAzMCkge1xyXG4gICAgICBncmlkT3B0aW9ucy5QYWdpbmcuZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgICBncmlkT3B0aW9ucy5QYWdlci52aXNpYmxlID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZ3JpZE9wdGlvbnM7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldEdyaWRJbnN0YW5jZShvcmRlcjogbnVtYmVyKTogYW55IHtcclxuICAgIGNvbnN0IGZpbHRlckluc3RhbmNlID0gdGhpcy5ncmlkSW5zdGFuY2VzLmZpbHRlcigocikgPT4gci5vcmRlciA9PT0gb3JkZXIpWzBdO1xyXG4gICAgcmV0dXJuIGZpbHRlckluc3RhbmNlID8gZmlsdGVySW5zdGFuY2UuZ3JpZEluc3RhbmNlIDogbnVsbDtcclxuICB9XHJcblxyXG4gIG9uRGF0ZVZhbHVlQ2hhbmdlZChlLCBpdGVtKTogdm9pZCB7XHJcbiAgICBpZiAoZS52YWx1ZSkge1xyXG4gICAgICBpdGVtLlZhbHVlID0gY29udmVydERhdGVUb1VzRm9ybWF0KGUudmFsdWUpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5kYXRlU2hvcnRjdXRDb250ZW50UmVhZHkoaXRlbSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAqIE1ldGhvZCB3aWxsIGJlIGNhbGxlZCBvbiBjb250ZW50IHJlYWR5IG9mIGRhdGVib3guXHJcbiAgKiBAcmV0dXJucyB2b2lkXHJcbiAgKi9cclxuICBkYXRlU2hvcnRjdXRDb250ZW50UmVhZHkoaXRlbTogRGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzRW52ZWxvcGUpOiB2b2lkIHtcclxuICAgIGl0ZW0uZGF0ZVNob3J0Y3V0SWNvbiA9ICdmYWwgZmEtY2FsZW5kYXItYWx0JztcclxuICAgIGl0ZW0uZGF0ZVNob3J0Y3V0SWQgPSAnJztcclxuICAgIGlmIChpdGVtLlZhbHVlKSB7XHJcbiAgICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCB0aGlzLmRhdGVTaG9ydGN1dHNJdGVtcy5sZW5ndGg7IGluZGV4KyspIHtcclxuICAgICAgICBpZiAodGhpcy5kYXRlU2hvcnRjdXRzSXRlbXNbaW5kZXhdLlZhbHVlID09PSBpdGVtLlZhbHVlKSB7XHJcbiAgICAgICAgICBpdGVtLmRhdGVTaG9ydGN1dEljb24gPSAnZmFzIGZhLWNhbGVuZGFyLWFsdCc7XHJcbiAgICAgICAgICBpdGVtLmRhdGVTaG9ydGN1dElkID0gdGhpcy5kYXRlU2hvcnRjdXRzSXRlbXNbaW5kZXhdLklkO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZGF0ZVNob3J0Y3V0T3BlbihpdGVtOiBEYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNFbnZlbG9wZSk6IHZvaWQge1xyXG4gICAgdGhpcy5kYXRlU2hvcnRjdXRzSXRlbXMuZm9yRWFjaChkYXRhID0+IHtcclxuICAgICAgaWYgKGRhdGEuSWQgPT09IGl0ZW0uZGF0ZVNob3J0Y3V0SWQpIHtcclxuICAgICAgICBkYXRhLklzU2VsZWN0ZWQgPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGRhdGEuSXNTZWxlY3RlZCA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIE1ldGhvZCB3aWxsIGJlIGNhbGxlZCBvbiBkYXRlIHNob3J0IGN1dCBjbGljay5cclxuICAgKiBAcmV0dXJucyB2b2lkXHJcbiAgICovXHJcbiAgZGF0ZVNob3J0Y3V0SXRlbUNsaWNrKHNlbGVjdGVkRGF0YSwgZ2xvYmFsSXRlbTogRGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzRW52ZWxvcGUpOiB2b2lkIHtcclxuICAgIGNvbnN0IGl0ZW0gPSBzZWxlY3RlZERhdGEuaXRlbURhdGE7XHJcbiAgICBpdGVtLklzU2VsZWN0ZWQgPSAhaXRlbS5Jc1NlbGVjdGVkO1xyXG4gICAgaWYgKGl0ZW0uSXNTZWxlY3RlZCkge1xyXG4gICAgICBjb25zdCBwcmV2U2VsZWN0ZWQgPSB0aGlzLmRhdGVTaG9ydGN1dHNJdGVtcy5maWx0ZXIoZGF0YSA9PiBkYXRhLklzU2VsZWN0ZWQpO1xyXG4gICAgICBpZiAocHJldlNlbGVjdGVkICYmIHByZXZTZWxlY3RlZC5sZW5ndGggJiYgcHJldlNlbGVjdGVkWzBdLklkICE9PSBpdGVtLklkKSB7XHJcbiAgICAgICAgcHJldlNlbGVjdGVkWzBdLklzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICBnbG9iYWxJdGVtLmRhdGVTaG9ydGN1dElkID0gaXRlbS5JZDtcclxuICAgICAgZ2xvYmFsSXRlbS5kYXRlU2hvcnRjdXRJY29uID0gJ2ZhcyBmYS1jYWxlbmRhci1hbHQnO1xyXG4gICAgICBnbG9iYWxJdGVtLlZhbHVlID0gaXRlbS5WYWx1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGdsb2JhbEl0ZW0uZGF0ZVNob3J0Y3V0SWNvbiA9ICdmYWwgZmEtY2FsZW5kYXItYWx0JztcclxuICAgICAgZ2xvYmFsSXRlbS5kYXRlU2hvcnRjdXRJZCA9ICcnO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBJc051bGxFbXB0eU9yV2hpdGVTcGFjZSh2YWx1ZTogU3RyaW5nKSB7XHJcbiAgICBjb25zdCB2ID0gdmFsdWUgPyBgJHt2YWx1ZX1gLnRyaW0oKSA6ICcnO1xyXG4gICAgcmV0dXJuICh2ID09PSBudWxsIHx8IHYgPT09IHVuZGVmaW5lZCB8fCB2ID09PSAnJyk7XHJcbiAgfVxyXG5cclxuICBvblBhcmFtZXRlckRyb3BEb3duT3BlbmVkKGFyZ3M/OiBhbnkpOiB2b2lkIHtcclxuICAgIGlmIChhcmdzKSB7XHJcbiAgICAgIC8vIHJlc2V0dGluZyB0aGUgd2lkdGggdG8gcGFyZW50IGNvbnRyb2xcclxuICAgICAgYXJncy5jb21wb25lbnQub3B0aW9uKCdkcm9wRG93bk9wdGlvbnMud2lkdGgnLCAyNTQpO1xyXG4gICAgfVxyXG4gICAgY29uc3QgY29sSHRtbENvbGxlY3Rpb24gPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdlbnRpdHktcGFyYW0tZ3JpZCBjb2wnKTtcclxuICAgIGlmIChjb2xIdG1sQ29sbGVjdGlvbi5sZW5ndGgpIHtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgZm9yIChsZXQgaW5kZXggPSAwOyBpbmRleCA8IGNvbEh0bWxDb2xsZWN0aW9uLmxlbmd0aDsgaW5kZXgrKykge1xyXG4gICAgICAgICAgaWYgKGluZGV4ICUgMiA9PT0gMCkge1xyXG4gICAgICAgICAgICBjb2xIdG1sQ29sbGVjdGlvbltpbmRleF0uc2V0QXR0cmlidXRlKCdzdHlsZScsICd3aWR0aDogMjVweCcpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSwgMCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25LZXlEb3duX0dyaWQoZXZ0KSB7XHJcbiAgICBpZiAoZXZ0LmNvbXBvbmVudC5vcHRpb24oJ2RhdGFTb3VyY2UnKS5zdG9yZS5kYXRhLmxlbmd0aCA+IDEwMCkgeyAvLyAxMDAgaXMgdGhlIHRvIHJlc3RyaWN0IGFuZCBwcmV2ZW50IERCIHBhcmFtZXRlcnMgaXNzdWUuXHJcbiAgICAgIGlmIChldnQuZXZlbnQuY3RybEtleSAmJiBldnQuZXZlbnQua2V5Q29kZSA9PT0gNjUpIHsgLy8gY3RybCthXHJcbiAgICAgICAgYXBwVG9hc3QoeyB0eXBlOiAnZXJyb3InLCBtZXNzYWdlOiAnT25seSAxMDAgaXRlbXMgY2FuIGJlIHNlbGVjdGVkLicgfSk7XHJcbiAgICAgICAgZXZ0LmV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbk1vdXNlT3ZlcihpdGVtKTogdm9pZCB7XHJcbiAgICBpdGVtLnNob3dCb3JkZXIgPSB0cnVlO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIG9uTW91c2VPdXQoaXRlbSk6IHZvaWQge1xyXG4gICAgaXRlbS5zaG93Qm9yZGVyID0gZmFsc2U7XHJcbiAgfVxyXG5cclxufSJdfQ==