import { __decorate } from "tslib";
import { CGFFlyOutEnum } from '../../utilities/enums';
import { Component, Output, Input, EventEmitter } from '@angular/core';
import { actionTypes, cgfFlyOutList } from '../../utilities/constants';
import { appToast, openDestination, filterSelectedRowData } from '../../utilities/utilityFunctions';
let FlyOutActionIconContainerComponent = class FlyOutActionIconContainerComponent {
    constructor() {
        this.position = 'right';
        this.showLoader = false;
        this.showParametersControls = false;
        this.visibleFlyOuts = [];
        this.flyOutSelectionClick = new EventEmitter();
        this.refreshEntityData = new EventEmitter();
        this.activeFlyOut = { id: -1 };
        this.cgfLeftMenuDropDownListItems = [];
        this.cgfLeftMenus = [];
        this.currentFlyOutSection = { id: -1 };
        this.flyOutSections = [];
        this.leftMenuDropDownList = [];
        this.leftMenuList = [];
    }
    set resetContainerSettings(val) {
        if (val) {
            this.currentFlyOutSection = { id: -1 };
        }
    }
    set selectedFlyOut(val) {
        if (val) {
            this.activeFlyOut = val;
        }
        else {
            this.activeFlyOut = { id: -1 };
        }
    }
    get selectedFlyOut() { return this.activeFlyOut; }
    set cgfLeftMenuList(data) {
        this.cgfLeftMenus = data;
        if (data) {
            this.prepareExtraActions();
        }
    }
    get cgfLeftMenuList() { return this.cgfLeftMenus; }
    ngOnInit() {
        this.flyOutSections = cgfFlyOutList.filter(item => this.visibleFlyOuts.indexOf(item.id) > -1);
    }
    onFlyOutIconClick(evt, item) {
        if (item.id === CGFFlyOutEnum.newTab) {
            open(location.href, '_blank');
            return;
        }
        if (item.id === CGFFlyOutEnum.refreshData) {
            this.refreshData();
            return;
        }
        if (item.id === this.activeFlyOut.id) {
            this.activeFlyOut = { id: -1 };
        }
        else {
            this.activeFlyOut = item;
        }
        this.showParametersControls = false;
        this.flyOutSelectionClick.emit({ evt, item });
    }
    showParametersControlsContainer() {
        this.currentFlyOutSection = { id: -1 };
        this.showParametersControls = this.showParametersControls ? false : true;
        this.flyOutSelectionClick.emit({ showEntityParameter: this.showParametersControls });
    }
    refreshData() {
        if (this.disableRefreshIcon) {
            appToast({ type: 'error', message: 'Parameters are required.' });
            return;
        }
        this.showLoader = true;
        this.refreshEntityData.emit();
    }
    cgfLeftMenuClick(data) {
        openDestination(data, filterSelectedRowData(this.gridInstance), this.dataBrowserEntityParameters, null); // this.appService
    }
    onMoreItemClick(args) {
        if (!args.itemData.items) {
            openDestination(args.itemData, filterSelectedRowData(this.gridInstance), this.dataBrowserEntityParameters);
        }
    }
    onFlyOutSelectionClick(event, item) {
        if (item.id === CGFFlyOutEnum.newTab) {
            open(location.href, '_blank');
            return;
        }
        if (item.id === CGFFlyOutEnum.refreshData) {
            this.refreshData();
            return;
        }
        if (item.id === this.currentFlyOutSection.id) {
            this.currentFlyOutSection = { id: -1 };
        }
        else {
            this.currentFlyOutSection = item;
        }
        this.showParametersControls = false;
        this.flyOutSelectionClick.emit({ event, item });
    }
    prepareExtraActions() {
        this.leftMenuList = this.cgfLeftMenus.filter(x => x.IsFrequentlyUsed === true && x.ShowAsContextMenu === false
            && x.DestinationType !== actionTypes.childEntity);
        this.cgfLeftMenuDropDownListItems = this.cgfLeftMenus.filter(x => x.IsFrequentlyUsed === false && x.ShowAsContextMenu === false
            && x.DestinationType !== actionTypes.childEntity);
        this.leftMenuDropDownList = [{ text: 'More', icon: 'overflow', items: this.cgfLeftMenuDropDownListItems }];
        setTimeout(() => {
            const elm = document.querySelector('.optionsActions .dx-menu-item-popout-container');
            if (elm !== null) {
                elm.remove();
            }
        }, 100);
    }
};
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "dataBrowserEntityParameters", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "disableRefreshIcon", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "gridInstance", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "position", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "showLoader", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "showParametersControls", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "visibleFlyOuts", void 0);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "resetContainerSettings", null);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "selectedFlyOut", null);
__decorate([
    Input()
], FlyOutActionIconContainerComponent.prototype, "cgfLeftMenuList", null);
__decorate([
    Output()
], FlyOutActionIconContainerComponent.prototype, "flyOutSelectionClick", void 0);
__decorate([
    Output()
], FlyOutActionIconContainerComponent.prototype, "refreshEntityData", void 0);
FlyOutActionIconContainerComponent = __decorate([
    Component({
        selector: 'pbi-fly-out-action-icon-container',
        template: "<div [ngClass]=\"{'flex-end': position === 'right', 'flex-start': position === 'left'}\">\r\n    <ul class=\"no-list-style filter-list\">\r\n        <li *ngFor=\"let item of leftMenuList\">\r\n            <span (click)=\"cgfLeftMenuClick(item)\">\r\n                <i *ngIf=\"item.IconClassName\" class=\"{{item.IconClassName}} pointer dynamic-tooltip flyOutSelectionIcon\"\r\n                    attr.title=\"{{item.DisplayName}} \"></i>\r\n                <button *ngIf=\"!item.IconClassName\">{{item.DisplayName}}</button>\r\n            </span>\r\n        </li>\r\n    </ul>\r\n    <dx-menu cssClass=\"optionsActions\" #menu [dataSource]=\"leftMenuDropDownList\" displayExpr=\"DisplayName\"\r\n        (onItemClick)=\"onMoreItemClick($event)\" title=\"More\" class=\"left-menu-item-list-control\"\r\n        *ngIf=\"cgfLeftMenuDropDownListItems.length > 0\">\r\n    </dx-menu>\r\n    <div>\r\n        <ul class=\"icon-list\">\r\n            <li *ngFor=\"let sectionItem of flyOutSections\">\r\n                <i class=\"{{sectionItem.src}}\" [class.pointer-none-events]=\"\"\r\n                    [class.active-flyOut]=\"activeFlyOut.id === sectionItem.id\" [class.loading]=\"\"\r\n                    title=\"{{sectionItem.title}}\" (click)=\"onFlyOutIconClick($event, sectionItem)\">\r\n                </i>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n</div>\r\n",
        styles: [""]
    })
], FlyOutActionIconContainerComponent);
export { FlyOutActionIconContainerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmx5LW91dC1hY3Rpb24taWNvbi1jb250YWluZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vcGJpLW5nLyIsInNvdXJjZXMiOlsibGliL2NvbW1vbi1ncmlkLWZyYW1ld29yay9jb21wb25lbnRzL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0EsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3RELE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0UsT0FBTyxFQUFFLFdBQVcsRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBT3BHLElBQWEsa0NBQWtDLEdBQS9DLE1BQWEsa0NBQWtDO0lBK0M3QztRQTFDZ0IsYUFBUSxHQUFxQixPQUFPLENBQUM7UUFDckMsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUNuQiwyQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDL0IsbUJBQWMsR0FBb0IsRUFBRSxDQUFDO1FBNEJwQyx5QkFBb0IsR0FBUSxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQy9DLHNCQUFpQixHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFFeEQsaUJBQVksR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFCLGlDQUE0QixHQUFHLEVBQUUsQ0FBQztRQUNsQyxpQkFBWSxHQUFHLEVBQUUsQ0FBQztRQUNsQix5QkFBb0IsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ2xDLG1CQUFjLEdBQXlCLEVBQUUsQ0FBQztRQUMxQyx5QkFBb0IsR0FBRyxFQUFFLENBQUM7UUFDMUIsaUJBQVksR0FBRyxFQUFFLENBQUM7SUFFRixDQUFDO0lBcENqQixJQUFJLHNCQUFzQixDQUFDLEdBQVk7UUFDckMsSUFBSSxHQUFHLEVBQUU7WUFDUCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUN4QztJQUNILENBQUM7SUFHRCxJQUFJLGNBQWMsQ0FBQyxHQUFHO1FBQ3BCLElBQUksR0FBRyxFQUFFO1lBQ1AsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUM7U0FDekI7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUNoQztJQUNILENBQUM7SUFDRCxJQUFJLGNBQWMsS0FBSyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBR2xELElBQUksZUFBZSxDQUFDLElBQUk7UUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxJQUFJLEVBQUU7WUFDUixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUM1QjtJQUNILENBQUM7SUFDRCxJQUFJLGVBQWUsS0FBSyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO0lBZW5ELFFBQVE7UUFDTixJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNoRyxDQUFDO0lBRU0saUJBQWlCLENBQUMsR0FBZSxFQUFFLElBQW1CO1FBQzNELElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzlCLE9BQU87U0FDUjtRQUNELElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxhQUFhLENBQUMsV0FBVyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuQixPQUFPO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ2hDO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztTQUMxQjtRQUNELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFTSwrQkFBK0I7UUFDcEMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDekUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxFQUFFLG1CQUFtQixFQUFFLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVNLFdBQVc7UUFDaEIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDM0IsUUFBUSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsQ0FBQyxDQUFDO1lBQ2pFLE9BQU87U0FDUjtRQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRU0sZ0JBQWdCLENBQUMsSUFBSTtRQUMxQixlQUFlLENBQUMsSUFBSSxFQUFFLHFCQUFxQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxJQUFJLENBQUMsMkJBQTJCLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxrQkFBa0I7SUFDN0gsQ0FBQztJQUVNLGVBQWUsQ0FBQyxJQUFJO1FBQ3pCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtZQUN4QixlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7U0FDNUc7SUFDSCxDQUFDO0lBRU0sc0JBQXNCLENBQUMsS0FBVSxFQUFFLElBQW1CO1FBQzNELElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQzlCLE9BQU87U0FDUjtRQUNELElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxhQUFhLENBQUMsV0FBVyxFQUFFO1lBQ3pDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNuQixPQUFPO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLG9CQUFvQixDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUN4QzthQUFNO1lBQ0wsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQztTQUNsQztRQUNELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDcEMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFTyxtQkFBbUI7UUFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLGlCQUFpQixLQUFLLEtBQUs7ZUFDekcsQ0FBQyxDQUFDLGVBQWUsS0FBSyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLDRCQUE0QixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixLQUFLLEtBQUssSUFBSSxDQUFDLENBQUMsaUJBQWlCLEtBQUssS0FBSztlQUMxSCxDQUFDLENBQUMsZUFBZSxLQUFLLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLDRCQUE0QixFQUFFLENBQUMsQ0FBQztRQUMzRyxVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ2QsTUFBTSxHQUFHLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxnREFBZ0QsQ0FBQyxDQUFDO1lBQ3JGLElBQUksR0FBRyxLQUFLLElBQUksRUFBRTtnQkFDaEIsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ2Q7UUFDSCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDVixDQUFDO0NBRUYsQ0FBQTtBQWhJVTtJQUFSLEtBQUssRUFBRTt1RkFBMkU7QUFDMUU7SUFBUixLQUFLLEVBQUU7OEVBQW9DO0FBQ25DO0lBQVIsS0FBSyxFQUFFO3dFQUFpQztBQUNoQztJQUFSLEtBQUssRUFBRTtvRUFBNkM7QUFDNUM7SUFBUixLQUFLLEVBQUU7c0VBQTJCO0FBQzFCO0lBQVIsS0FBSyxFQUFFO2tGQUF1QztBQUN0QztJQUFSLEtBQUssRUFBRTswRUFBNkM7QUFHckQ7SUFEQyxLQUFLLEVBQUU7Z0ZBS1A7QUFHRDtJQURDLEtBQUssRUFBRTt3RUFPUDtBQUlEO0lBREMsS0FBSyxFQUFFO3lFQU1QO0FBR1M7SUFBVCxNQUFNLEVBQUU7Z0ZBQXVEO0FBQ3REO0lBQVQsTUFBTSxFQUFFOzZFQUErQztBQXJDN0Msa0NBQWtDO0lBTDlDLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxtQ0FBbUM7UUFDN0MsazNDQUE2RDs7S0FFOUQsQ0FBQztHQUNXLGtDQUFrQyxDQWtJOUM7U0FsSVksa0NBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGR4RGF0YUdyaWQgZnJvbSAnZGV2ZXh0cmVtZS91aS9kYXRhX2dyaWQnO1xyXG5pbXBvcnQgeyBDR0ZGbHlPdXRFbnVtIH0gZnJvbSAnLi4vLi4vdXRpbGl0aWVzL2VudW1zJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIE91dHB1dCwgSW5wdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNFbnZlbG9wZSwgRmx5T3V0U2VjdGlvbiB9IGZyb20gJy4uL2NvbnRyYWN0cy9jb21tb24tZ3JpZC1mcmFtZXdvcmsnO1xyXG5pbXBvcnQgeyBhY3Rpb25UeXBlcywgY2dmRmx5T3V0TGlzdCB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy9jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBhcHBUb2FzdCwgb3BlbkRlc3RpbmF0aW9uLCBmaWx0ZXJTZWxlY3RlZFJvd0RhdGEgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvdXRpbGl0eUZ1bmN0aW9ucyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3BiaS1mbHktb3V0LWFjdGlvbi1pY29uLWNvbnRhaW5lcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZseS1vdXQtYWN0aW9uLWljb24tY29udGFpbmVyLmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9mbHktb3V0LWFjdGlvbi1pY29uLWNvbnRhaW5lci5jb21wb25lbnQuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZseU91dEFjdGlvbkljb25Db250YWluZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICBASW5wdXQoKSBwdWJsaWMgZGF0YUJyb3dzZXJFbnRpdHlQYXJhbWV0ZXJzOiBEYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnNFbnZlbG9wZVtdO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBkaXNhYmxlUmVmcmVzaEljb246IGJvb2xlYW47XHJcbiAgQElucHV0KCkgcHVibGljIGdyaWRJbnN0YW5jZTogZHhEYXRhR3JpZDtcclxuICBASW5wdXQoKSBwdWJsaWMgcG9zaXRpb246ICdyaWdodCcgfCAnbGVmdCcgPSAncmlnaHQnO1xyXG4gIEBJbnB1dCgpIHB1YmxpYyBzaG93TG9hZGVyID0gZmFsc2U7XHJcbiAgQElucHV0KCkgcHVibGljIHNob3dQYXJhbWV0ZXJzQ29udHJvbHMgPSBmYWxzZTtcclxuICBASW5wdXQoKSBwdWJsaWMgdmlzaWJsZUZseU91dHM6IENHRkZseU91dEVudW1bXSA9IFtdO1xyXG5cclxuICBASW5wdXQoKVxyXG4gIHNldCByZXNldENvbnRhaW5lclNldHRpbmdzKHZhbDogYm9vbGVhbikge1xyXG4gICAgaWYgKHZhbCkge1xyXG4gICAgICB0aGlzLmN1cnJlbnRGbHlPdXRTZWN0aW9uID0geyBpZDogLTEgfTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgc2V0IHNlbGVjdGVkRmx5T3V0KHZhbCkge1xyXG4gICAgaWYgKHZhbCkge1xyXG4gICAgICB0aGlzLmFjdGl2ZUZseU91dCA9IHZhbDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuYWN0aXZlRmx5T3V0ID0geyBpZDogLTEgfTtcclxuICAgIH1cclxuICB9XHJcbiAgZ2V0IHNlbGVjdGVkRmx5T3V0KCkgeyByZXR1cm4gdGhpcy5hY3RpdmVGbHlPdXQ7IH1cclxuXHJcbiAgQElucHV0KClcclxuICBzZXQgY2dmTGVmdE1lbnVMaXN0KGRhdGEpIHtcclxuICAgIHRoaXMuY2dmTGVmdE1lbnVzID0gZGF0YTtcclxuICAgIGlmIChkYXRhKSB7XHJcbiAgICAgIHRoaXMucHJlcGFyZUV4dHJhQWN0aW9ucygpO1xyXG4gICAgfVxyXG4gIH1cclxuICBnZXQgY2dmTGVmdE1lbnVMaXN0KCkgeyByZXR1cm4gdGhpcy5jZ2ZMZWZ0TWVudXM7IH1cclxuXHJcbiAgQE91dHB1dCgpIHB1YmxpYyBmbHlPdXRTZWxlY3Rpb25DbGljazogYW55ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIEBPdXRwdXQoKSBwdWJsaWMgcmVmcmVzaEVudGl0eURhdGEgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gIGFjdGl2ZUZseU91dCA9IHsgaWQ6IC0xIH07XHJcbiAgY2dmTGVmdE1lbnVEcm9wRG93bkxpc3RJdGVtcyA9IFtdO1xyXG4gIGNnZkxlZnRNZW51cyA9IFtdO1xyXG4gIGN1cnJlbnRGbHlPdXRTZWN0aW9uID0geyBpZDogLTEgfTtcclxuICBmbHlPdXRTZWN0aW9uczogQXJyYXk8Rmx5T3V0U2VjdGlvbj4gPSBbXTtcclxuICBsZWZ0TWVudURyb3BEb3duTGlzdCA9IFtdO1xyXG4gIGxlZnRNZW51TGlzdCA9IFtdO1xyXG5cclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuZmx5T3V0U2VjdGlvbnMgPSBjZ2ZGbHlPdXRMaXN0LmZpbHRlcihpdGVtID0+IHRoaXMudmlzaWJsZUZseU91dHMuaW5kZXhPZihpdGVtLmlkKSA+IC0xKTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbkZseU91dEljb25DbGljayhldnQ6IE1vdXNlRXZlbnQsIGl0ZW06IEZseU91dFNlY3Rpb24pOiB2b2lkIHtcclxuICAgIGlmIChpdGVtLmlkID09PSBDR0ZGbHlPdXRFbnVtLm5ld1RhYikge1xyXG4gICAgICBvcGVuKGxvY2F0aW9uLmhyZWYsICdfYmxhbmsnKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgaWYgKGl0ZW0uaWQgPT09IENHRkZseU91dEVudW0ucmVmcmVzaERhdGEpIHtcclxuICAgICAgdGhpcy5yZWZyZXNoRGF0YSgpO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKGl0ZW0uaWQgPT09IHRoaXMuYWN0aXZlRmx5T3V0LmlkKSB7XHJcbiAgICAgIHRoaXMuYWN0aXZlRmx5T3V0ID0geyBpZDogLTEgfTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuYWN0aXZlRmx5T3V0ID0gaXRlbTtcclxuICAgIH1cclxuICAgIHRoaXMuc2hvd1BhcmFtZXRlcnNDb250cm9scyA9IGZhbHNlO1xyXG4gICAgdGhpcy5mbHlPdXRTZWxlY3Rpb25DbGljay5lbWl0KHsgZXZ0LCBpdGVtIH0pO1xyXG4gIH1cclxuXHJcbiAgcHVibGljIHNob3dQYXJhbWV0ZXJzQ29udHJvbHNDb250YWluZXIoKSB7XHJcbiAgICB0aGlzLmN1cnJlbnRGbHlPdXRTZWN0aW9uID0geyBpZDogLTEgfTtcclxuICAgIHRoaXMuc2hvd1BhcmFtZXRlcnNDb250cm9scyA9IHRoaXMuc2hvd1BhcmFtZXRlcnNDb250cm9scyA/IGZhbHNlIDogdHJ1ZTtcclxuICAgIHRoaXMuZmx5T3V0U2VsZWN0aW9uQ2xpY2suZW1pdCh7IHNob3dFbnRpdHlQYXJhbWV0ZXI6IHRoaXMuc2hvd1BhcmFtZXRlcnNDb250cm9scyB9KTtcclxuICB9XHJcblxyXG4gIHB1YmxpYyByZWZyZXNoRGF0YSgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmRpc2FibGVSZWZyZXNoSWNvbikge1xyXG4gICAgICBhcHBUb2FzdCh7IHR5cGU6ICdlcnJvcicsIG1lc3NhZ2U6ICdQYXJhbWV0ZXJzIGFyZSByZXF1aXJlZC4nIH0pO1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICB0aGlzLnNob3dMb2FkZXIgPSB0cnVlO1xyXG4gICAgdGhpcy5yZWZyZXNoRW50aXR5RGF0YS5lbWl0KCk7XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgY2dmTGVmdE1lbnVDbGljayhkYXRhKTogdm9pZCB7XHJcbiAgICBvcGVuRGVzdGluYXRpb24oZGF0YSwgZmlsdGVyU2VsZWN0ZWRSb3dEYXRhKHRoaXMuZ3JpZEluc3RhbmNlKSwgdGhpcy5kYXRhQnJvd3NlckVudGl0eVBhcmFtZXRlcnMsIG51bGwpOyAvLyB0aGlzLmFwcFNlcnZpY2VcclxuICB9XHJcblxyXG4gIHB1YmxpYyBvbk1vcmVJdGVtQ2xpY2soYXJncyk6IHZvaWQge1xyXG4gICAgaWYgKCFhcmdzLml0ZW1EYXRhLml0ZW1zKSB7XHJcbiAgICAgIG9wZW5EZXN0aW5hdGlvbihhcmdzLml0ZW1EYXRhLCBmaWx0ZXJTZWxlY3RlZFJvd0RhdGEodGhpcy5ncmlkSW5zdGFuY2UpLCB0aGlzLmRhdGFCcm93c2VyRW50aXR5UGFyYW1ldGVycyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwdWJsaWMgb25GbHlPdXRTZWxlY3Rpb25DbGljayhldmVudDogYW55LCBpdGVtOiBGbHlPdXRTZWN0aW9uKTogdm9pZCB7XHJcbiAgICBpZiAoaXRlbS5pZCA9PT0gQ0dGRmx5T3V0RW51bS5uZXdUYWIpIHtcclxuICAgICAgb3Blbihsb2NhdGlvbi5ocmVmLCAnX2JsYW5rJyk7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIGlmIChpdGVtLmlkID09PSBDR0ZGbHlPdXRFbnVtLnJlZnJlc2hEYXRhKSB7XHJcbiAgICAgIHRoaXMucmVmcmVzaERhdGEoKTtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChpdGVtLmlkID09PSB0aGlzLmN1cnJlbnRGbHlPdXRTZWN0aW9uLmlkKSB7XHJcbiAgICAgIHRoaXMuY3VycmVudEZseU91dFNlY3Rpb24gPSB7IGlkOiAtMSB9O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jdXJyZW50Rmx5T3V0U2VjdGlvbiA9IGl0ZW07XHJcbiAgICB9XHJcbiAgICB0aGlzLnNob3dQYXJhbWV0ZXJzQ29udHJvbHMgPSBmYWxzZTtcclxuICAgIHRoaXMuZmx5T3V0U2VsZWN0aW9uQ2xpY2suZW1pdCh7IGV2ZW50LCBpdGVtIH0pO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwcmVwYXJlRXh0cmFBY3Rpb25zKCk6IHZvaWQge1xyXG4gICAgdGhpcy5sZWZ0TWVudUxpc3QgPSB0aGlzLmNnZkxlZnRNZW51cy5maWx0ZXIoeCA9PiB4LklzRnJlcXVlbnRseVVzZWQgPT09IHRydWUgJiYgeC5TaG93QXNDb250ZXh0TWVudSA9PT0gZmFsc2VcclxuICAgICAgJiYgeC5EZXN0aW5hdGlvblR5cGUgIT09IGFjdGlvblR5cGVzLmNoaWxkRW50aXR5KTtcclxuICAgIHRoaXMuY2dmTGVmdE1lbnVEcm9wRG93bkxpc3RJdGVtcyA9IHRoaXMuY2dmTGVmdE1lbnVzLmZpbHRlcih4ID0+IHguSXNGcmVxdWVudGx5VXNlZCA9PT0gZmFsc2UgJiYgeC5TaG93QXNDb250ZXh0TWVudSA9PT0gZmFsc2VcclxuICAgICAgJiYgeC5EZXN0aW5hdGlvblR5cGUgIT09IGFjdGlvblR5cGVzLmNoaWxkRW50aXR5KTtcclxuICAgIHRoaXMubGVmdE1lbnVEcm9wRG93bkxpc3QgPSBbeyB0ZXh0OiAnTW9yZScsIGljb246ICdvdmVyZmxvdycsIGl0ZW1zOiB0aGlzLmNnZkxlZnRNZW51RHJvcERvd25MaXN0SXRlbXMgfV07XHJcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgY29uc3QgZWxtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLm9wdGlvbnNBY3Rpb25zIC5keC1tZW51LWl0ZW0tcG9wb3V0LWNvbnRhaW5lcicpO1xyXG4gICAgICBpZiAoZWxtICE9PSBudWxsKSB7XHJcbiAgICAgICAgZWxtLnJlbW92ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9LCAxMDApO1xyXG4gIH1cclxuXHJcbn1cclxuIl19