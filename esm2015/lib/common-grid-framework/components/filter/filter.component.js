import { __decorate } from "tslib";
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { applyFiltersToGrid } from '../../utilities/utilityFunctions';
let FilterComponent = class FilterComponent {
    constructor() {
        this.closeCurrentFlyOut = new EventEmitter();
        this.applyButtonOptions = {
            text: 'Apply',
            type: 'default'
        };
        this.gridOptions = new Object();
        this.listOfColumns = [];
        this.title = 'FILTERS';
        this.showFilterSection = true;
    }
    set filterGridOptions(filterGridOptions) {
        var _a, _b;
        this.gridOptions = filterGridOptions;
        this.filterValue = ((_b = (_a = filterGridOptions === null || filterGridOptions === void 0 ? void 0 : filterGridOptions.gridComponentInstance) === null || _a === void 0 ? void 0 : _a.option()) === null || _b === void 0 ? void 0 : _b.filterValue) || this.filterGridOptions.gridFilterValue;
        this.listOfColumns = filterGridOptions.columns.filter((c) => c.allowFiltering !== false) || [];
    }
    get filterGridOptions() { return this.gridOptions; }
    set gridInstanceList(gridInstanceList) {
        var _a;
        this.selectedGridInstanceList = gridInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length)
            .map(item => item.gridComponentInstance);
        this.title = ((_a = gridInstanceList.filter(item => item.isSelected && Object.keys(item.gridComponentInstance).length)[0]) === null || _a === void 0 ? void 0 : _a.isMasterGrid) ? 'FILTERS' : 'FILTERS - CHILD VIEW';
    }
    get gridInstanceList() { return this.selectedGridInstanceList; }
    ;
    applyFilter() {
        this.filterGridOptions.gridFilterValue = this.filterValue;
        // change
        // if (this.gridInstance) {
        //   this.gridInstance.beginUpdate();
        //   this.gridInstance.option('filterValue', this.filterValue);
        //   applyFiltersToGrid(this.gridInstance, this.filterValue);
        //   this.gridInstance.endUpdate();
        //   setTimeout(() => {
        //     const currentGridState = this.gridInstance.state();
        //     currentGridState.gridFilterValue = this.filterValue;
        //     this.gridInstance.state(currentGridState);
        //   }, 100);
        // }
        if (this.selectedGridInstanceList.length) {
            this.selectedGridInstanceList.forEach(gridInstance => {
                gridInstance.beginUpdate();
                gridInstance.option('filterValue', this.filterValue);
                applyFiltersToGrid(gridInstance, this.filterValue);
                gridInstance.endUpdate();
                setTimeout(() => {
                    const currentGridState = gridInstance.state();
                    currentGridState.gridFilterValue = this.filterValue;
                    gridInstance.state(currentGridState);
                }, 100);
            });
        }
    }
    closeFlyOut() {
        this.closeCurrentFlyOut.emit();
    }
};
__decorate([
    Input()
], FilterComponent.prototype, "filterGridOptions", null);
__decorate([
    Input()
], FilterComponent.prototype, "gridInstanceList", null);
__decorate([
    Output()
], FilterComponent.prototype, "closeCurrentFlyOut", void 0);
FilterComponent = __decorate([
    Component({
        selector: 'pbi-filter',
        template: "<div class=\"filter-container\">\r\n    <div class=\"header-title\">\r\n        <div class=\"align-label-icon\">\r\n            <i class=\"fas fa-filter title-icon\"></i>\r\n            <span class=\"section-title\">{{title}}</span>\r\n        </div>\r\n        <i class=\"fas fa-times pointer\" (click)=\"closeFlyOut()\"></i>\r\n    </div>\r\n    <div class=\"active-properties-tab\">\r\n        <ul>\r\n            <li class=\"pointer property-tab-in-view-selection\" (click)=\"showFilterSection = !showFilterSection\">\r\n                <span>VISUAL</span>\r\n                <span [class.fa-angle-down]=\"!showFilterSection\" [class.fa-angle-up]=\"showFilterSection\"\r\n                    class=\"fa basic-format-toggle-icon\"></span>\r\n            </li>\r\n        </ul>\r\n    </div>\r\n    <div class=\"accordion-data-container\" *ngIf=\"showFilterSection\">\r\n        <div>\r\n            <div class=\"filter-options\">\r\n                <dx-filter-builder class=\"filter-builder\" [fields]=\"listOfColumns\" [(value)]=\"filterValue\"\r\n                    [allowHierarchicalFields]=\"true\">\r\n                </dx-filter-builder>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <span class=\"button-container\">\r\n        <dx-button class=\"apply-filter-button\" [text]=\"applyButtonOptions.text\" [type]=\"applyButtonOptions.type\"\r\n            (onClick)=\"applyFilter()\">\r\n        </dx-button>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    })
], FilterComponent);
export { FilterComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvY29tcG9uZW50cy9maWx0ZXIvZmlsdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQWUsS0FBSyxFQUFFLE1BQU0sRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUkvRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQVF0RSxJQUFhLGVBQWUsR0FBNUIsTUFBYSxlQUFlO0lBMEIxQjtRQVhpQix1QkFBa0IsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXpELHVCQUFrQixHQUFxQjtZQUNyQyxJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxTQUFTO1NBQ2hCLENBQUM7UUFFRixnQkFBVyxHQUFHLElBQUksTUFBTSxFQUF5QixDQUFDO1FBQ2xELGtCQUFhLEdBQW9CLEVBQUUsQ0FBQztRQUVwQyxVQUFLLEdBQUcsU0FBUyxDQUFDO1FBR2xCLHNCQUFpQixHQUFHLElBQUksQ0FBQztJQUZULENBQUM7SUF4QmpCLElBQUksaUJBQWlCLENBQUMsaUJBQWlCOztRQUNyQyxJQUFJLENBQUMsV0FBVyxHQUFHLGlCQUFpQixDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLEdBQUcsYUFBQSxpQkFBaUIsYUFBakIsaUJBQWlCLHVCQUFqQixpQkFBaUIsQ0FBRSxxQkFBcUIsMENBQUUsTUFBTSw0Q0FBSSxXQUFXLEtBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQztRQUM3SCxJQUFJLENBQUMsYUFBYSxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxjQUFjLEtBQUssS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pHLENBQUM7SUFDRCxJQUFJLGlCQUFpQixLQUFLLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7SUFFM0MsSUFBSSxnQkFBZ0IsQ0FBQyxnQkFBK0M7O1FBQzNFLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxDQUFDO2FBQy9ILEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxLQUFLLEdBQUcsT0FBQSxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLDBDQUFFLFlBQVksRUFBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQztJQUN4SyxDQUFDO0lBQ0QsSUFBSSxnQkFBZ0IsS0FBSyxPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7SUFNOUQsQ0FBQztJQVVILFdBQVc7UUFDVCxJQUFJLENBQUMsaUJBQWlCLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDMUQsU0FBUztRQUNULDJCQUEyQjtRQUMzQixxQ0FBcUM7UUFDckMsK0RBQStEO1FBQy9ELDZEQUE2RDtRQUM3RCxtQ0FBbUM7UUFDbkMsdUJBQXVCO1FBQ3ZCLDBEQUEwRDtRQUMxRCwyREFBMkQ7UUFDM0QsaURBQWlEO1FBQ2pELGFBQWE7UUFDYixJQUFJO1FBRUosSUFBSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFO1lBQ3hDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEVBQUU7Z0JBQ25ELFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDM0IsWUFBWSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNyRCxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNuRCxZQUFZLENBQUMsU0FBUyxFQUFFLENBQUM7Z0JBQ3pCLFVBQVUsQ0FBQyxHQUFHLEVBQUU7b0JBQ2QsTUFBTSxnQkFBZ0IsR0FBRyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQzlDLGdCQUFnQixDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO29CQUNwRCxZQUFZLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3ZDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNWLENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRU0sV0FBVztRQUNoQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQztDQUNGLENBQUE7QUE3REM7SUFEQyxLQUFLLEVBQUU7d0RBS1A7QUFHUTtJQUFSLEtBQUssRUFBRTt1REFJUDtBQUVTO0lBQVQsTUFBTSxFQUFFOzJEQUFnRDtBQWY5QyxlQUFlO0lBTDNCLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxZQUFZO1FBQ3RCLHE4Q0FBc0M7O0tBRXZDLENBQUM7R0FDVyxlQUFlLENBK0QzQjtTQS9EWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT3V0cHV0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGV2RXh0cmVtZUJ1dHRvbiB9IGZyb20gJy4uL2NvbnRyYWN0cy9kZXZleHRyZW1lV2lkZ2V0cyc7XHJcbmltcG9ydCB7IEdyaWRDb21wb25lbnRJbnN0YW5jZXMgfSBmcm9tICcuLi9jb250cmFjdHMvY29tbW9uLWdyaWQtZnJhbWV3b3JrJztcclxuaW1wb3J0IHsgUEJJR3JpZE9wdGlvbnNNb2RlbCB9IGZyb20gJy4uL21vZGVscy9ncmlkT3B0aW9ucy5tb2RlbCc7XHJcbmltcG9ydCB7IGFwcGx5RmlsdGVyc1RvR3JpZCB9IGZyb20gJy4uLy4uL3V0aWxpdGllcy91dGlsaXR5RnVuY3Rpb25zJztcclxuaW1wb3J0IHsgUEJJR3JpZENvbHVtbiB9IGZyb20gJy4uL2NvbnRyYWN0cy9ncmlkJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAncGJpLWZpbHRlcicsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL2ZpbHRlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vZmlsdGVyLmNvbXBvbmVudC5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRmlsdGVyQ29tcG9uZW50IHtcclxuICBASW5wdXQoKVxyXG4gIHNldCBmaWx0ZXJHcmlkT3B0aW9ucyhmaWx0ZXJHcmlkT3B0aW9ucykge1xyXG4gICAgdGhpcy5ncmlkT3B0aW9ucyA9IGZpbHRlckdyaWRPcHRpb25zO1xyXG4gICAgdGhpcy5maWx0ZXJWYWx1ZSA9IGZpbHRlckdyaWRPcHRpb25zPy5ncmlkQ29tcG9uZW50SW5zdGFuY2U/Lm9wdGlvbigpPy5maWx0ZXJWYWx1ZSB8fCB0aGlzLmZpbHRlckdyaWRPcHRpb25zLmdyaWRGaWx0ZXJWYWx1ZTtcclxuICAgIHRoaXMubGlzdE9mQ29sdW1ucyA9IGZpbHRlckdyaWRPcHRpb25zLmNvbHVtbnMuZmlsdGVyKChjKSA9PiBjLmFsbG93RmlsdGVyaW5nICE9PSBmYWxzZSkgfHwgW107XHJcbiAgfVxyXG4gIGdldCBmaWx0ZXJHcmlkT3B0aW9ucygpIHsgcmV0dXJuIHRoaXMuZ3JpZE9wdGlvbnM7IH1cclxuXHJcbiAgQElucHV0KCkgc2V0IGdyaWRJbnN0YW5jZUxpc3QoZ3JpZEluc3RhbmNlTGlzdDogQXJyYXk8R3JpZENvbXBvbmVudEluc3RhbmNlcz4pIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRHcmlkSW5zdGFuY2VMaXN0ID0gZ3JpZEluc3RhbmNlTGlzdC5maWx0ZXIoaXRlbSA9PiBpdGVtLmlzU2VsZWN0ZWQgJiYgT2JqZWN0LmtleXMoaXRlbS5ncmlkQ29tcG9uZW50SW5zdGFuY2UpLmxlbmd0aClcclxuICAgICAgLm1hcChpdGVtID0+IGl0ZW0uZ3JpZENvbXBvbmVudEluc3RhbmNlKTtcclxuICAgIHRoaXMudGl0bGUgPSBncmlkSW5zdGFuY2VMaXN0LmZpbHRlcihpdGVtID0+IGl0ZW0uaXNTZWxlY3RlZCAmJiBPYmplY3Qua2V5cyhpdGVtLmdyaWRDb21wb25lbnRJbnN0YW5jZSkubGVuZ3RoKVswXT8uaXNNYXN0ZXJHcmlkID8gJ0ZJTFRFUlMnIDogJ0ZJTFRFUlMgLSBDSElMRCBWSUVXJztcclxuICB9XHJcbiAgZ2V0IGdyaWRJbnN0YW5jZUxpc3QoKSB7IHJldHVybiB0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdDsgfVxyXG4gIEBPdXRwdXQoKSBwdWJsaWMgY2xvc2VDdXJyZW50Rmx5T3V0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICBhcHBseUJ1dHRvbk9wdGlvbnM6IERldkV4dHJlbWVCdXR0b24gPSB7XHJcbiAgICB0ZXh0OiAnQXBwbHknLFxyXG4gICAgdHlwZTogJ2RlZmF1bHQnXHJcbiAgfTs7XHJcbiAgZmlsdGVyVmFsdWU6IGFueTtcclxuICBncmlkT3B0aW9ucyA9IG5ldyBPYmplY3QoKSBhcyBQQklHcmlkT3B0aW9uc01vZGVsO1xyXG4gIGxpc3RPZkNvbHVtbnM6IFBCSUdyaWRDb2x1bW5bXSA9IFtdO1xyXG4gIHNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdDogQXJyYXk8YW55PjtcclxuICB0aXRsZSA9ICdGSUxURVJTJztcclxuICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICBzaG93RmlsdGVyU2VjdGlvbiA9IHRydWU7XHJcblxyXG4gIGFwcGx5RmlsdGVyKCk6IHZvaWQge1xyXG4gICAgdGhpcy5maWx0ZXJHcmlkT3B0aW9ucy5ncmlkRmlsdGVyVmFsdWUgPSB0aGlzLmZpbHRlclZhbHVlO1xyXG4gICAgLy8gY2hhbmdlXHJcbiAgICAvLyBpZiAodGhpcy5ncmlkSW5zdGFuY2UpIHtcclxuICAgIC8vICAgdGhpcy5ncmlkSW5zdGFuY2UuYmVnaW5VcGRhdGUoKTtcclxuICAgIC8vICAgdGhpcy5ncmlkSW5zdGFuY2Uub3B0aW9uKCdmaWx0ZXJWYWx1ZScsIHRoaXMuZmlsdGVyVmFsdWUpO1xyXG4gICAgLy8gICBhcHBseUZpbHRlcnNUb0dyaWQodGhpcy5ncmlkSW5zdGFuY2UsIHRoaXMuZmlsdGVyVmFsdWUpO1xyXG4gICAgLy8gICB0aGlzLmdyaWRJbnN0YW5jZS5lbmRVcGRhdGUoKTtcclxuICAgIC8vICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAvLyAgICAgY29uc3QgY3VycmVudEdyaWRTdGF0ZSA9IHRoaXMuZ3JpZEluc3RhbmNlLnN0YXRlKCk7XHJcbiAgICAvLyAgICAgY3VycmVudEdyaWRTdGF0ZS5ncmlkRmlsdGVyVmFsdWUgPSB0aGlzLmZpbHRlclZhbHVlO1xyXG4gICAgLy8gICAgIHRoaXMuZ3JpZEluc3RhbmNlLnN0YXRlKGN1cnJlbnRHcmlkU3RhdGUpO1xyXG4gICAgLy8gICB9LCAxMDApO1xyXG4gICAgLy8gfVxyXG5cclxuICAgIGlmICh0aGlzLnNlbGVjdGVkR3JpZEluc3RhbmNlTGlzdC5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZEdyaWRJbnN0YW5jZUxpc3QuZm9yRWFjaChncmlkSW5zdGFuY2UgPT4ge1xyXG4gICAgICAgIGdyaWRJbnN0YW5jZS5iZWdpblVwZGF0ZSgpO1xyXG4gICAgICAgIGdyaWRJbnN0YW5jZS5vcHRpb24oJ2ZpbHRlclZhbHVlJywgdGhpcy5maWx0ZXJWYWx1ZSk7XHJcbiAgICAgICAgYXBwbHlGaWx0ZXJzVG9HcmlkKGdyaWRJbnN0YW5jZSwgdGhpcy5maWx0ZXJWYWx1ZSk7XHJcbiAgICAgICAgZ3JpZEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgY3VycmVudEdyaWRTdGF0ZSA9IGdyaWRJbnN0YW5jZS5zdGF0ZSgpO1xyXG4gICAgICAgICAgY3VycmVudEdyaWRTdGF0ZS5ncmlkRmlsdGVyVmFsdWUgPSB0aGlzLmZpbHRlclZhbHVlO1xyXG4gICAgICAgICAgZ3JpZEluc3RhbmNlLnN0YXRlKGN1cnJlbnRHcmlkU3RhdGUpO1xyXG4gICAgICAgIH0sIDEwMCk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHVibGljIGNsb3NlRmx5T3V0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5jbG9zZUN1cnJlbnRGbHlPdXQuZW1pdCgpO1xyXG4gIH1cclxufVxyXG4iXX0=