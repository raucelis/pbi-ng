import { rowTypeInfo, customActionColumnInfo } from '../../utilities/constants';
import { CGFStorageKeys } from '../../utilities/enums';
import { getStorageKey, getClassNameByThemeName, applyFilterCssClass, appToast, ToDictionary } from '../../utilities/utilityFunctions';
export class PBIGridOptionsModel {
    // TODO: Parameterized constructor for assigning values
    constructor() {
        /* https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxDataGrid/Configuration/ for configuration of the dxGrid */
        //#region Grid Configuration Members
        // baseColumnsList: Array<IMS_Web.IGridColumn> = [];
        // columns: Array<IMS_Web.IGridColumn> = [];
        /* Specifies the shortcut key that sets focus on the widget.
            IE, Chrome, Safari, Opera 15+: [ALT] + accesskey
            Opera prior version 15: [SHIFT][ESC] + accesskey
            Firefox: [ALT][SHIFT] + accesskey */
        this.accessKey = 'g';
        this.allowAdding = false;
        this.allowColumnReordering = true;
        this.allowColumnResizing = true;
        this.allowDataExport = false;
        this.allowDeleting = false;
        this.allowReordering = false; // make sure rows can be re-ordered
        this.allowSelectedDataExport = false;
        this.allowUpdating = false;
        this.allowedPageSizes = [50, 150, 300];
        this.autoExpandAll = false;
        this.autoNavigateToFocusedRow = true;
        this.columnAutoWidth = false;
        this.columnMinWidth = 50;
        this.columnRenderingMode = 'virtual';
        this.columnResizingMode = 'widget';
        this.columns = [];
        this.contextMenuMappingList = [];
        this.dataSource = [];
        this.dateSerializationFormat = 'yyyy-MM-ddtHH:mm:ss';
        this.disabled = false;
        this.editingMode = 'row';
        this.enableActiveState = false;
        this.enableCache = true;
        this.enableCellHint = true;
        this.enableColumnChooser = false;
        this.enableColumnFixing = true;
        this.enableColumnHiding = true;
        this.enableContextGrpMenu = true;
        this.enableContextMenu = true;
        this.enableErrorRow = true;
        this.enableGridFormatting = false;
        this.enableLoadPanel = false;
        this.enableStateStoring = false;
        this.filterSyncEnabled = true;
        this.filterValue = null;
        this.gridComponentInstance = null;
        this.gridFilterValue = null;
        this.gridName = 'PortfolioBI Data Grid';
        this.highlightChanges = false;
        this.hoverStateEnabled = true;
        this.isMasterGrid = true;
        this.listGroupedColumns = [];
        this.noDataText = 'No Data';
        this.pageSize = 50;
        this.pagingEnabled = true;
        this.refreshMode = 'full';
        this.remoteOperationsEnabled = false;
        this.repaintChangesOnly = false;
        this.rowAlternationEnabled = false;
        this.scrollMode = 'standard';
        this.selectedTheme = '';
        this.selectedThemeClass = ''; // Empty means regular theme will be applied.
        this.selectionMode = 'single';
        this.showBorders = true;
        this.showColumnHeaders = true;
        this.showColumnLines = true;
        this.showDragIcons = false;
        this.showFilterPanel = true; // flag to show/hide filter info at bottom of grid.
        this.showFilterRow = true;
        this.showGroupPanel = true;
        this.showHeaderFilter = true;
        this.showPageInfo = true;
        this.showPageSizeSelector = false;
        this.showPager = true;
        this.showRowLines = true;
        this.showScrollbars = true;
        this.stateStorageType = null;
        this.showSearchPanel = false;
        this.sortingType = 'multiple';
        this.useIcons = false;
        this.stringSpecificOperators = ['contains', 'notcontains', 'startswith', 'endswith'];
        this.childEntityList = [];
        this.isMasterDetailEnabled = false;
    }
    applyViewToGrid(viewJson) {
        this.applyStateProperties(viewJson === null || viewJson === void 0 ? void 0 : viewJson.state.gridState);
    }
    onContextMenuPreparing(cellObject) {
        var _a;
        if (!this.enableContextMenu) {
            return;
        }
        // Added code to disable ungroupAll, when no column is grouped.
        if (cellObject.target === 'header' || cellObject.target === 'headerPanel') {
            const groupCount = cellObject.component.columnOption('groupIndex:0');
            if (!groupCount) {
                if (cellObject.items) {
                    cellObject.items.forEach((item) => {
                        if (item.value === 'ungroupAll') {
                            item.disabled = true;
                        }
                    });
                }
            }
            (_a = cellObject.items) === null || _a === void 0 ? void 0 : _a.push({
                disabled: false,
                icon: '',
                onItemClick: () => {
                    this.gridComponentInstance.beginUpdate();
                    const colCount = this.gridComponentInstance.columnCount();
                    for (let i = 0; i < colCount; i++) {
                        if (this.gridComponentInstance.columnOption(i, 'visible')) {
                            this.gridComponentInstance.columnOption(i, 'width', 'auto');
                        }
                    }
                    this.gridComponentInstance.endUpdate();
                },
                text: 'Auto Fit',
                value: 'autoFit',
            });
        }
        if (cellObject.row && cellObject.row.rowType === 'group') {
            cellObject.items.push({
                text: 'Expand All',
                onItemClick: () => { cellObject.component.expandAll(void 0); }
            });
            cellObject.items.push({
                text: 'Collapse All',
                onItemClick: () => { cellObject.component.collapseAll(void 0); }
            });
        }
        if (cellObject.row && cellObject.row.rowType === 'data') {
            const isNumberType = cellObject.column.dataType === 'number' ? true : false;
            cellObject.items = [{
                    visible: isNumberType,
                    text: 'Sum',
                    onItemClick: () => {
                        this.addOrRemoveAggregationSummary(cellObject, 'sum');
                    }
                },
                {
                    visible: isNumberType,
                    text: 'Avg',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'avg'); }
                },
                {
                    text: 'Max',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'max'); }
                },
                {
                    text: 'Min',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'min'); }
                },
                {
                    text: 'Count',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'count'); }
                },
                {
                    text: 'Reset',
                    onItemClick: () => { this.addOrRemoveAggregationSummary(cellObject, 'reset'); }
                }];
        }
    }
    ;
    onCellPrepared(cellInfo) {
        if (cellInfo.column && cellInfo.column.hasOwnProperty('dataField')) {
            // Added try catch so that in case formatting logic fails, user can still use CGF entity.
            try {
                this.cellFormat(cellInfo, true);
            }
            catch (error) {
                console.error(error);
                console.log('Cell formatting broke while exporting.', cellInfo);
            }
        }
    }
    onRowPrepared(args) {
        if (args.rowType === rowTypeInfo.data) {
            this.setCustomColumnData(args);
        }
        this.formatRows(args.rowType, args.data, args.rowElement, null);
    }
    cellFormat(cellInfo, isCellPrepared, excelGridCellInfo = null) {
        var _a, _b, _c;
        if (cellInfo) {
            let dataValue = null;
            switch (cellInfo.rowType) {
                case rowTypeInfo.totalFooter:
                    if (((_a = cellInfo.summaryItems) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                        dataValue = (_b = cellInfo.summaryItems[0]) === null || _b === void 0 ? void 0 : _b.value;
                    }
                    break;
                case rowTypeInfo.group:
                    if (!cellInfo.column.hasOwnProperty('command')
                        || (cellInfo.column.hasOwnProperty('command')
                            && cellInfo.column.command !== 'expand')) {
                        if ((_c = cellInfo.totalItem) === null || _c === void 0 ? void 0 : _c.summaryCells) {
                            const filterItem = cellInfo.totalItem.summaryCells.filter(item => { var _a; return cellInfo.column.dataField === ((_a = item[0]) === null || _a === void 0 ? void 0 : _a.column); })[0];
                            if (filterItem === null || filterItem === void 0 ? void 0 : filterItem.length) {
                                dataValue = filterItem[0].value;
                            }
                            else {
                                dataValue = cellInfo.value;
                            }
                        }
                    }
                    break;
                case rowTypeInfo.data:
                    dataValue = cellInfo.value;
                    break;
            }
            if (this.allowedRowTypes(cellInfo.rowType)) {
                if (!this.listGroupedColumns.length && cellInfo.component) {
                    this.listGroupedColumns = this.getGroupedColumnList(cellInfo.component);
                }
                this.prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue);
            }
            // else if (typeof (dataValue) === 'boolean' && cellInfo.rowType === rowTypeInfo.data) {
            //     this.prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue);
            // }
        }
    }
    allowedRowTypes(type) {
        switch (type) {
            case rowTypeInfo.data:
            case rowTypeInfo.group:
            case rowTypeInfo.totalFooter:
                return true;
        }
        return false;
    }
    prepareFormattingInfo(cellInfo, isCellPrepared, excelGridCellInfo, dataValue) {
        const rowTypeFilterForFormatting = 'row';
        const columnFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid))) || [];
        const baseFormatCollection = this.isMasterGrid ? JSON.parse(sessionStorage.getItem(CGFStorageKeys[CGFStorageKeys.dictionaryFormatData])) || [] : [];
        const conditionalFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid))) || [];
        const conditionFilterItemsForColumns = conditionalFormatCollection.filter(arrItem => arrItem.dataField === cellInfo.column.dataField && arrItem.applyType !== rowTypeFilterForFormatting);
        let conditionalFormattingRequired = false;
        const formatData = columnFormatCollection.find(arrItem => arrItem.dataField === cellInfo.column.dataField) ||
            baseFormatCollection.find(arrItem => arrItem.dataField === cellInfo.column.dataField);
        let applyBasicFormatting = true;
        conditionFilterItemsForColumns.forEach(filterItem => {
            var _a, _b, _c, _d, _e;
            if (filterItem.condition && cellInfo.rowType === 'data') {
                conditionalFormattingRequired = this.checkConditionSatisfied(filterItem, cellInfo.data);
            }
            if (conditionalFormattingRequired) {
                let _formattingData = Object.assign({}, formatData);
                if ((_formattingData === null || _formattingData === void 0 ? void 0 : _formattingData.dataField) === filterItem.dataField) {
                    for (const key in _formattingData) {
                        if (_formattingData.hasOwnProperty(key)) {
                            _formattingData.textColor = filterItem.textColor || _formattingData.textColor;
                            _formattingData.backgroundColor = filterItem.backgroundColor || _formattingData.backgroundColor;
                            // reset then apply
                            _formattingData.cssClass = (_c = (_b = (_a = _formattingData.cssClass) === null || _a === void 0 ? void 0 : _a.replace('bold', '')) === null || _b === void 0 ? void 0 : _b.replace('underline', '')) === null || _c === void 0 ? void 0 : _c.replace('italic', '');
                            _formattingData.cssClass = (_e = (_d = filterItem.cssClass) === null || _d === void 0 ? void 0 : _d.concat(' ')) === null || _e === void 0 ? void 0 : _e.concat(_formattingData.cssClass);
                        }
                    }
                }
                else {
                    _formattingData = Object.assign({}, filterItem);
                }
                this.applyFormatting(dataValue, _formattingData, isCellPrepared, cellInfo, excelGridCellInfo);
                applyBasicFormatting = false;
            }
        });
        if (formatData && applyBasicFormatting) {
            this.applyFormatting(dataValue, formatData, isCellPrepared, cellInfo, excelGridCellInfo);
        }
        else if (cellInfo.value !== cellInfo.text) { // !IMPORTANT Check why we need to use this condition. Since it worked in previous versions.
            cellInfo.text = cellInfo.value;
        }
    }
    checkConditionSatisfied(conditionInfo, rowData) {
        let conditionPassed = false;
        const updatedValue = this.startValidation(JSON.parse(JSON.stringify(conditionInfo.condition)), rowData);
        if (JSON.stringify(conditionInfo.condition) !== JSON.stringify(updatedValue)) {
            conditionPassed = this.transformAndRunValidation(updatedValue);
        }
        return conditionPassed;
    }
    startValidation(value, rowData) {
        if (value && Array.isArray(value[0])) {
            return value.map((item) => {
                return Array.isArray(item[0]) ? this.startValidation(item, rowData) : this.validate(item, rowData);
            });
        }
        return this.validate(value, rowData);
    }
    validate(item, rowData) {
        if (typeof item === 'string' && this.isReservedKeyWord(item)) {
            return item;
        }
        let _dataType = item[2] ? typeof item[2] : 'string';
        if (item[2] === 0 || item[2] === false) {
            _dataType = typeof item[2];
        }
        switch (_dataType) {
            case 'number':
                item = this.validateNumberType(item, rowData);
                break;
            case 'boolean':
                item = this.validateBooleanType(item, rowData);
                break;
            case 'string':
                if (item[2] && !isNaN(new Date(item[2]).getTime())) { // condition to check whether string is date type
                    if (this.stringSpecificOperators.indexOf(item[1]) > -1) { // condition since new date will return values for strings like '99'
                        item = this.validateStringType(item, rowData);
                    }
                    else {
                        item = this.validateDateType(item, rowData);
                    }
                }
                else {
                    item = this.validateStringType(item, rowData);
                }
                break;
            case 'object':
                if (Array.isArray(item[2])) {
                    if (typeof item[2][0] === 'string') {
                        item = this.validateDateType(item, rowData);
                    }
                    else if (typeof item[2][0] === 'number') {
                        item = this.validateNumberType(item, rowData);
                    }
                }
                break;
        }
        return item;
    }
    isReservedKeyWord(value) {
        switch (value) {
            case 'or':
            case 'and':
                return true;
        }
    }
    validateNumberType(item, rowData) {
        const filterVal1 = item[2][0] || item[2];
        const filterVal2 = item[2][1] || item[2];
        const isPassed = this.validateOperatorAndOperands(item[1], rowData[item[0]], filterVal1, filterVal2);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    }
    validateBooleanType(item, rowData) {
        const isPassed = this.validateOperatorAndOperands(item[1], rowData[item[0]], item[2]);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed.toString(); // this conversion(only for boolean) will be used in parent functions.
        }
        return item;
    }
    validateStringType(item, rowData) {
        var _a, _b;
        if (item[2] !== null) {
            item[2] = (_a = item[2]) === null || _a === void 0 ? void 0 : _a.toLowerCase();
        }
        let dataFieldValueInRow = rowData[item[0]];
        if (typeof rowData[item[0]] === 'string') {
            dataFieldValueInRow = ((_b = rowData[item[0]]) === null || _b === void 0 ? void 0 : _b.toLowerCase()) || null;
        }
        const isPassed = this.validateOperatorAndOperands(item[1], dataFieldValueInRow, item[2] === undefined ? '' : item[2]);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    }
    validateDateType(item, rowData) {
        let filterVal1 = item[2][0].length > 6 ? item[2][0] : item[2]; // conditions to check string is date only and 6 is since we have mm/dd/yyyy format
        let filterVal2 = item[2][1].length > 6 ? item[2][1] : item[2];
        filterVal1 = this.convertDateForComparison(filterVal1).getTime();
        filterVal2 = this.convertDateForComparison(filterVal2).getTime();
        let rowColData = this.convertDateForComparison(rowData[item[0]]).getTime();
        rowColData = isNaN(rowColData) ? '' : rowColData;
        const isPassed = this.validateOperatorAndOperands(item[1], rowColData, filterVal1, filterVal2);
        if (typeof isPassed === 'boolean') {
            item[2] = isPassed;
        }
        return item;
    }
    convertDateForComparison(dateValue) {
        const dateTime = new Date(dateValue);
        const formattedDate = (dateTime.getMonth() + 1) + '-' + dateTime.getDate() + '-' + dateTime.getFullYear();
        return new Date(formattedDate);
    }
    validateOperatorAndOperands(operator, rowColumnData, filterValue, filterValue2) {
        if (typeof (rowColumnData) !== 'undefined') {
            switch (operator) {
                case 'contains':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.indexOf(filterValue)) > -1 ? true : false;
                case 'notcontains':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.indexOf(filterValue)) === -1 ? false : true;
                case 'startswith':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.startsWith(filterValue)) ? true : false;
                case 'endswith':
                    return (rowColumnData === null || rowColumnData === void 0 ? void 0 : rowColumnData.endsWith(filterValue)) ? true : false;
                case 'isblank':
                    return rowColumnData === null || rowColumnData === '' ? true : false;
                case 'isnotblank':
                    return rowColumnData !== null && rowColumnData !== '' ? true : false;
                case 'between':
                    // we have assumption that fist value is min and second is max
                    return rowColumnData > filterValue && rowColumnData < filterValue2 ? true : false;
                case '=':
                    return rowColumnData === filterValue ? true : false;
                case '<>':
                    return rowColumnData !== filterValue ? true : false;
                case '<':
                    return rowColumnData < filterValue ? true : false;
                case '>':
                    return rowColumnData > filterValue ? true : false;
                case '>=':
                    return rowColumnData >= filterValue ? true : false;
                case '<=':
                    return rowColumnData <= filterValue ? true : false;
            }
        }
        return void 0;
    }
    transformAndRunValidation(value) {
        let evalString = '';
        value.forEach(item => {
            if (Array.isArray(item) && !Array.isArray(item[2])) {
                evalString += item[2];
            }
            else if (typeof item === 'string' || typeof item === 'boolean') {
                switch (item) {
                    case 'and':
                        evalString += ' && ';
                        break;
                    case 'or':
                        evalString += ' || ';
                        break;
                    case true:
                    case false:
                        evalString += ` ${item} `;
                        break;
                    case 'true':
                    case 'false':
                        evalString += ` ${JSON.parse(item)} `;
                        break;
                }
            }
            else if (Array.isArray(item)) {
                evalString = this.transformAndRunValidation(item);
            }
        });
        try {
            // tslint:disable-next-line: no-eval
            return eval(evalString);
        }
        catch (e) {
            console.error(e);
            return false;
        }
    }
    getGroupedColumnList(gridComponent) {
        const listGroupedColumns = [];
        const count = gridComponent.option('columns').length;
        for (let i = 0; i < count; i++) {
            const groupedColum = gridComponent.columnOption('groupIndex:' + i.toString());
            if (groupedColum) {
                listGroupedColumns.push(groupedColum.dataField.toLowerCase());
            }
        }
        return listGroupedColumns;
    }
    applyFormatting(dataValue, formatData, isCellPrepared, cellInfo, excelGridCellInfo) {
        var _a, _b;
        for (const _key in formatData) {
            if (formatData.hasOwnProperty(_key)) {
                const keyValue = formatData[_key] || '';
                switch (_key) {
                    case 'cssClass':
                        if (isCellPrepared) {
                            const classArray = `${keyValue}`.split(' ');
                            classArray.forEach(item => { if (item.trim()) {
                                cellInfo.cellElement.classList.add(item.trim());
                            } });
                        }
                        else if ((keyValue === null || keyValue === void 0 ? void 0 : keyValue.length) > 0) {
                            this.excelCellFormat(`${keyValue}`, excelGridCellInfo, dataValue);
                        }
                        break;
                    case 'format':
                        let assigneeObject = '';
                        if (isCellPrepared && formatData.dataType !== 'boolean') {
                            assigneeObject = dataValue; // first reset than apply format.
                        }
                        if (typeof keyValue !== 'string') {
                            switch (keyValue.type) {
                                case 'currency':
                                    assigneeObject = this.prepareUSDFormat(keyValue.precision, dataValue);
                                    break;
                                case 'percent':
                                    assigneeObject = this.preparePercentFormat(keyValue.precision, dataValue);
                                    break;
                                case 'comma':
                                    assigneeObject = this.prepareCommaFormat(keyValue.precision, dataValue);
                                    break;
                                case 'fixedPoint':
                                    assigneeObject = dataValue.toFixed(keyValue.precision);
                                    break;
                            }
                        }
                        else if (formatData[_key] === 'shortDate') {
                            const date = new Date(dataValue);
                            assigneeObject = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
                        }
                        if (isCellPrepared && assigneeObject) { // added "AND" condition to prevent overriding value for boolean column
                            cellInfo.cellElement.innerText = assigneeObject;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.value = assigneeObject || dataValue;
                        }
                        break;
                    case 'alignment':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.textAlign = keyValue;
                        }
                        else {
                            excelGridCellInfo.alignment = { horizontal: keyValue };
                        }
                        if (cellInfo.rowType === 'totalFooter' && ((_b = (_a = cellInfo.cellElement) === null || _a === void 0 ? void 0 : _a.childNodes) === null || _b === void 0 ? void 0 : _b.length)) {
                            cellInfo.cellElement.childNodes[0].style.textAlign = keyValue;
                        }
                        break;
                    case 'textColor':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.color = keyValue;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { color: { argb: keyValue === null || keyValue === void 0 ? void 0 : keyValue.replace('#', 'ff') } });
                        }
                        break;
                    case 'backgroundColor':
                        if (isCellPrepared) {
                            cellInfo.cellElement.style.backgroundColor = keyValue;
                        }
                        else if (excelGridCellInfo) {
                            excelGridCellInfo.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: keyValue === null || keyValue === void 0 ? void 0 : keyValue.replace('#', 'ff') } };
                        }
                        break;
                    // case 'fixed': // !Important this case has been handled in databrowser.component.ts
                }
            }
        }
    }
    excelCellFormat(classList, excelGridCellInfo, val) {
        let classListData = classList.split(' ');
        let decimalPlacesData;
        classListData = classListData.filter((item) => {
            if (!item.toLowerCase().includes('decimaladded')) {
                return item;
            }
            else {
                decimalPlacesData = item;
            }
        });
        if (decimalPlacesData) {
            classListData.unshift(decimalPlacesData);
        }
        for (const formatClass of classListData) {
            switch (formatClass.toLowerCase()) {
                case 'bold':
                    excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { bold: true });
                    break;
                case 'underline':
                    excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { underline: true });
                    break;
                case 'italic':
                    excelGridCellInfo.font = Object.assign(Object.assign({}, excelGridCellInfo.font), { italic: true });
                    break;
            }
            if (formatClass.toLowerCase().includes('decimaladded')) {
                const precisionData = formatClass.split('-');
                if (precisionData.length > 0) {
                    excelGridCellInfo.value = val === null || val === void 0 ? void 0 : val.toFixed(Number(precisionData[1]));
                }
            }
        }
    }
    prepareUSDFormat(precision, value) {
        precision = precision === 0 ? 0 : precision || 2; // Default to 2 Decimal Places;
        return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    }
    preparePercentFormat(precision, value) {
        precision = precision === 0 ? 0 : precision || 3; // Default to 3 Decimal Places;
        return new Intl.NumberFormat('en-US', { style: 'percent', minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    }
    prepareCommaFormat(precision, value) {
        precision = precision === 0 ? 0 : precision || 0; // Default to 2 Decimal Places;
        return new Intl.NumberFormat('en-US', { useGrouping: true, minimumFractionDigits: precision, maximumFractionDigits: precision }).format(value);
    }
    addOrRemoveAggregationSummary(argsEvt, typeOfAggregation) {
        this.gridComponentInstance.beginUpdate();
        const totalSummaryItems = argsEvt.component.option('summary.totalItems') || [], groupSummaryItems = argsEvt.component.option('summary.groupItems') || [], indexOfTotalSummary = this.checkForExistingSummaryType(totalSummaryItems, argsEvt.column.dataField), indexOfGroupSummary = this.checkForExistingSummaryType(groupSummaryItems, argsEvt.column.dataField);
        const isNotTypeCountWithFormat = argsEvt.column.format && typeOfAggregation !== 'count';
        const summaryObj = {
            column: argsEvt.column.dataField,
            summaryType: typeOfAggregation,
            alignByColumn: true,
            displayFormat: '{0}',
            valueFormat: isNotTypeCountWithFormat ? argsEvt.column.format : { type: 'fixedPoint', precision: 0 }
            // showInGroupFooter: true
        };
        if (typeOfAggregation === 'reset') {
            if (indexOfTotalSummary !== -1 || indexOfGroupSummary !== -1) {
                totalSummaryItems.splice(indexOfTotalSummary, 1);
                groupSummaryItems.splice(indexOfGroupSummary, 1);
                this.gridComponentInstance.option('summary.totalItems', totalSummaryItems);
                this.gridComponentInstance.option('summary.groupItems', groupSummaryItems);
                // Making sure filters are reapplied
                if (argsEvt.component.state().filters) {
                    argsEvt.component.filter(argsEvt.component.state().filters);
                }
            }
            this.gridComponentInstance.endUpdate();
            return;
        }
        if (indexOfTotalSummary === -1 || indexOfGroupSummary === -1) {
            totalSummaryItems.push(summaryObj);
            groupSummaryItems.push(summaryObj);
        }
        else {
            totalSummaryItems.splice(indexOfTotalSummary, 1);
            groupSummaryItems.splice(indexOfGroupSummary, 1);
            totalSummaryItems.push(summaryObj);
            groupSummaryItems.push(summaryObj);
        }
        argsEvt.component.option('summary.totalItems', totalSummaryItems);
        argsEvt.component.option('summary.groupItems', groupSummaryItems);
        // Making sure filters are reapplied
        if (argsEvt.component.state().filters) {
            argsEvt.component.filter(argsEvt.component.state().filters);
        }
        this.gridComponentInstance.endUpdate();
    }
    checkForExistingSummaryType(summaryItemsCollection, columnName) {
        for (let i = 0; i < summaryItemsCollection.length; i++) {
            if (summaryItemsCollection[i].column === columnName) {
                return i;
            }
        }
        return -1;
    }
    applyStateProperties(viewJson) {
        var _a, _b, _c, _d, _e;
        if (!this.gridComponentInstance || !this.gridComponentInstance.NAME) {
            return;
        }
        this.selectedTheme = '';
        this.gridComponentInstance.beginCustomLoading('Refreshing Grid');
        this.gridComponentInstance.beginUpdate();
        if (viewJson) {
            if (viewJson.columns) {
                const viewColumnsLookup = ToDictionary((viewJson === null || viewJson === void 0 ? void 0 : viewJson.columns) || [], a => a.dataField.toLowerCase());
                for (let i = 0; i < this.columns.length; i++) {
                    const key = this.columns[i].dataField.toLowerCase();
                    if (viewColumnsLookup.hasOwnProperty(key)) {
                        this.columns[i].visible = viewColumnsLookup[key].visible;
                    }
                    else {
                        this.columns[i].visible = false;
                    }
                }
            }
            // Below loop is to OVERRIDE CAPTION OF COLUMN
            if (viewJson.visibleColumns && viewJson.visibleColumns.length) {
                for (let index = 0; index < viewJson.visibleColumns.length; index++) {
                    for (let _index = 0; _index < this.columns.length; _index++) {
                        const dataField = viewJson.visibleColumns[index].dataField;
                        if (dataField && dataField !== customActionColumnInfo.dataField &&
                            this.columns[_index].dataField.toLowerCase() === dataField.toLowerCase()) {
                            this.columns[_index].caption = viewJson.visibleColumns[index].caption;
                            // resetting already applied filter class.
                            this.columns[_index].cssClass = (_a = this.columns[_index].cssClass) === null || _a === void 0 ? void 0 : _a.replace(/filterApplied/g, '');
                            break;
                        }
                    }
                }
            }
            if (((_c = (_b = viewJson.summary) === null || _b === void 0 ? void 0 : _b.groupItems) === null || _c === void 0 ? void 0 : _c.length) || ((_e = (_d = viewJson.summary) === null || _d === void 0 ? void 0 : _d.totalItems) === null || _e === void 0 ? void 0 : _e.length)) {
                this.gridComponentInstance.option('summary', viewJson.summary);
            }
            else {
                viewJson.summary.groupItems = [];
                viewJson.summary.totalItems = [];
                this.gridComponentInstance.option('summary', viewJson.summary);
            }
            const formatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid)) || '""');
            if (formatCollection) {
                viewJson.columnFormattingInfo = Object.assign([], true, formatCollection, viewJson.columnFormattingInfo);
            }
            sessionStorage.setItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid), JSON.stringify(viewJson.columnFormattingInfo ? viewJson.columnFormattingInfo : ''));
            sessionStorage.setItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid), JSON.stringify(viewJson.conditionalFormattingInfo ? viewJson.conditionalFormattingInfo : null));
            this.gridFilterValue = viewJson ? viewJson.filterValue : null;
            if (this.gridFilterValue) {
                this.gridComponentInstance.option('filterValue', this.gridFilterValue);
                setTimeout(() => {
                    applyFilterCssClass(this.gridFilterValue, this.gridComponentInstance);
                }, 10);
            }
            if (viewJson.hasOwnProperty(CGFStorageKeys[CGFStorageKeys.childGridView])) {
                // this.setChildGridLayoutInfo(layoutJson);
            }
            appToast({ message: 'Selected view settings has been applied.', type: 'success' });
        }
        else {
            sessionStorage.removeItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.formatData], this.isMasterGrid));
            sessionStorage.removeItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid));
            this.columns.forEach(column => {
                if (column.cssClass && column.cssClass.toLowerCase().trim() === 'filterapplied') {
                    column.cssClass = '';
                }
            });
        }
        this.selectedTheme = viewJson ? viewJson.selectedTheme || 'np.compact' : 'np.compact';
        sessionStorage.setItem('theme', this.selectedTheme);
        this.showColumnLines = viewJson ? viewJson.isGridBorderVisible || false : false;
        this.selectedThemeClass = getClassNameByThemeName(this.selectedTheme);
        this.gridComponentInstance.endUpdate();
        this.gridComponentInstance.state(viewJson);
        this.gridComponentInstance.endCustomLoading();
    }
    setCustomColumnData(args) {
        const customCols = args.columns.filter(col => col.expression && col.groupName === 'Custom Columns');
        customCols.forEach(col => {
            args.data[col.dataField] = args.values[col.index];
            this.columns.filter(column => column.dataField === col.dataField)[0].dataType = col.dataType;
        });
    }
    formatRows(dataType, rowData, rowElement, excelCell) {
        if (dataType === rowTypeInfo.data) {
            const conditionalFormatCollection = JSON.parse(sessionStorage.getItem(getStorageKey(this.gridComponentInstance, CGFStorageKeys[CGFStorageKeys.conditionalFormatting], this.isMasterGrid))) || [];
            const conditionFilterItems = conditionalFormatCollection.filter(arrItem => arrItem.applyType === 'row');
            let conditionalFormattingRequired = false;
            conditionFilterItems.forEach(filterItem => {
                var _a;
                if (filterItem.condition) {
                    conditionalFormattingRequired = this.checkConditionSatisfied(filterItem, rowData);
                    if (conditionalFormattingRequired) {
                        if (rowElement) {
                            for (let index = 0; index < rowElement.children.length; index++) {
                                const existingClasses = ((_a = rowElement.children[index].classList) === null || _a === void 0 ? void 0 : _a.toString().replace('bold', '').replace('underline', '').replace('italic', '')) || '';
                                rowElement.children[index].style.backgroundColor = filterItem.backgroundColor || rowElement.children[index].style.backgroundColor;
                                rowElement.children[index].style.color = filterItem.textColor || rowElement.children[index].style.color;
                                rowElement.children[index].classList = existingClasses.concat(' ').concat(filterItem.cssClass);
                            }
                        }
                        else if (excelCell) {
                            if (filterItem.backgroundColor) {
                                excelCell.fill = {
                                    type: 'pattern', pattern: 'solid',
                                    fgColor: { argb: filterItem.backgroundColor.replace('#', 'ff') }
                                };
                            }
                            if (filterItem.textColor) {
                                excelCell.font = {
                                    color: { argb: filterItem.textColor.replace('#', 'ff') }
                                };
                            }
                            if (filterItem.cssClass) {
                                this.excelCellFormat(filterItem.cssClass, excelCell, null);
                            }
                        }
                    }
                }
            });
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZE9wdGlvbnMubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29tbW9uLWdyaWQtZnJhbWV3b3JrL2NvbXBvbmVudHMvbW9kZWxzL2dyaWRPcHRpb25zLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXVCQSxPQUFPLEVBQUUsV0FBVyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDaEYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRXZELE9BQU8sRUFBRSxhQUFhLEVBQUUsdUJBQXVCLEVBQUUsbUJBQW1CLEVBQUUsUUFBUSxFQUFFLFlBQVksRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBSXZJLE1BQU0sT0FBTyxtQkFBbUI7SUFxSDVCLHVEQUF1RDtJQUN2RDtRQXJIQSwrSEFBK0g7UUFFL0gsb0NBQW9DO1FBQ3BDLG9EQUFvRDtRQUNwRCw0Q0FBNEM7UUFDNUM7OztnREFHd0M7UUFDeEMsY0FBUyxHQUFHLEdBQUcsQ0FBQztRQUNoQixnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQiwwQkFBcUIsR0FBRyxJQUFJLENBQUM7UUFDN0Isd0JBQW1CLEdBQUcsSUFBSSxDQUFDO1FBQzNCLG9CQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLG9CQUFlLEdBQUcsS0FBSyxDQUFDLENBQUMsbUNBQW1DO1FBQzVELDRCQUF1QixHQUFHLEtBQUssQ0FBQztRQUNoQyxrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixxQkFBZ0IsR0FBRyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDbEMsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsNkJBQXdCLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLG9CQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLG1CQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLHdCQUFtQixHQUFHLFNBQVMsQ0FBQztRQUNoQyx1QkFBa0IsR0FBRyxRQUFRLENBQUM7UUFDOUIsWUFBTyxHQUFvQixFQUFFLENBQUM7UUFDOUIsMkJBQXNCLEdBQVEsRUFBRSxDQUFDO1FBQ2pDLGVBQVUsR0FBeUYsRUFBRSxDQUFDO1FBQ3RHLDRCQUF1QixHQUFHLHFCQUFxQixDQUFDO1FBQ2hELGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFFcEIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ25CLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLHdCQUFtQixHQUFHLEtBQUssQ0FBQztRQUM1Qix1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFDMUIsdUJBQWtCLEdBQUcsSUFBSSxDQUFDO1FBQzFCLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUM1QixzQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDekIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIseUJBQW9CLEdBQUcsS0FBSyxDQUFDO1FBQzdCLG9CQUFlLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLHVCQUFrQixHQUFHLEtBQUssQ0FBQztRQUMzQixzQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDekIsZ0JBQVcsR0FBbUMsSUFBSSxDQUFDO1FBQ25ELDBCQUFxQixHQUFlLElBQUksQ0FBQztRQUN6QyxvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixhQUFRLEdBQUcsdUJBQXVCLENBQUM7UUFFbkMscUJBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLHNCQUFpQixHQUFHLElBQUksQ0FBQztRQUN6QixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQix1QkFBa0IsR0FBYSxFQUFFLENBQUM7UUFDbEMsZUFBVSxHQUFHLFNBQVMsQ0FBQztRQUN2QixhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2Qsa0JBQWEsR0FBRyxJQUFJLENBQUM7UUFDckIsZ0JBQVcsR0FBbUMsTUFBTSxDQUFDO1FBQ3JELDRCQUF1QixHQUFHLEtBQUssQ0FBQztRQUNoQyx1QkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDM0IsMEJBQXFCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLGVBQVUsR0FBRyxVQUFVLENBQUM7UUFFeEIsa0JBQWEsR0FBRyxFQUFFLENBQUM7UUFDbkIsdUJBQWtCLEdBQUcsRUFBRSxDQUFDLENBQUUsNkNBQTZDO1FBQ3ZFLGtCQUFhLEdBQUcsUUFBUSxDQUFDO1FBQ3pCLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBQ25CLHNCQUFpQixHQUFHLElBQUksQ0FBQztRQUN6QixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixvQkFBZSxHQUFHLElBQUksQ0FBQyxDQUFDLG1EQUFtRDtRQUMzRSxrQkFBYSxHQUFHLElBQUksQ0FBQztRQUNyQixtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0QixxQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDeEIsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIseUJBQW9CLEdBQUcsS0FBSyxDQUFDO1FBQzdCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDcEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIscUJBQWdCLEdBQWlELElBQUksQ0FBQztRQUN0RSxvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUN4QixnQkFBVyxHQUFHLFVBQVUsQ0FBQztRQUN6QixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBRVQsNEJBQXVCLEdBQUcsQ0FBQyxVQUFVLEVBQUUsYUFBYSxFQUFFLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUV4RixvQkFBZSxHQUFlLEVBQUUsQ0FBQztRQUNqQywwQkFBcUIsR0FBRyxLQUFLLENBQUM7SUE4QmQsQ0FBQztJQUVWLGVBQWUsQ0FBQyxRQUFzQjtRQUN6QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRU0sc0JBQXNCLENBQUMsVUFBNkM7O1FBQ3ZFLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFBRSxPQUFPO1NBQUU7UUFDeEMsK0RBQStEO1FBQy9ELElBQUksVUFBVSxDQUFDLE1BQU0sS0FBSyxRQUFRLElBQUksVUFBVSxDQUFDLE1BQU0sS0FBSyxhQUFhLEVBQUU7WUFDdkUsTUFBTSxVQUFVLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDYixJQUFJLFVBQVUsQ0FBQyxLQUFLLEVBQUU7b0JBQ2xCLFVBQVUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7d0JBQzlCLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxZQUFZLEVBQUU7NEJBQzdCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO3lCQUN4QjtvQkFDTCxDQUFDLENBQUMsQ0FBQztpQkFDTjthQUNKO1lBQ0QsTUFBQSxVQUFVLENBQUMsS0FBSywwQ0FBRSxJQUFJLENBQUM7Z0JBQ25CLFFBQVEsRUFBRSxLQUFLO2dCQUNmLElBQUksRUFBRSxFQUFFO2dCQUNSLFdBQVcsRUFBRSxHQUFHLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN6QyxNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQzFELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLEVBQUUsQ0FBQyxFQUFFLEVBQUU7d0JBQy9CLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsU0FBUyxDQUFDLEVBQUU7NEJBQ3ZELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQzt5QkFDL0Q7cUJBQ0o7b0JBQ0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUMzQyxDQUFDO2dCQUNELElBQUksRUFBRSxVQUFVO2dCQUNoQixLQUFLLEVBQUUsU0FBUzthQUNuQixFQUFFO1NBQ047UUFFRCxJQUFJLFVBQVUsQ0FBQyxHQUFHLElBQUksVUFBVSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEtBQUssT0FBTyxFQUFFO1lBQ3RELFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNsQixJQUFJLEVBQUUsWUFBWTtnQkFDbEIsV0FBVyxFQUFFLEdBQUcsRUFBRSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2pFLENBQUMsQ0FBQztZQUNILFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO2dCQUNsQixJQUFJLEVBQUUsY0FBYztnQkFDcEIsV0FBVyxFQUFFLEdBQUcsRUFBRSxHQUFHLFVBQVUsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ25FLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxVQUFVLENBQUMsR0FBRyxJQUFJLFVBQVUsQ0FBQyxHQUFHLENBQUMsT0FBTyxLQUFLLE1BQU0sRUFBRTtZQUNyRCxNQUFNLFlBQVksR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzVFLFVBQVUsQ0FBQyxLQUFLLEdBQUcsQ0FBQztvQkFDaEIsT0FBTyxFQUFFLFlBQVk7b0JBQ3JCLElBQUksRUFBRSxLQUFLO29CQUNYLFdBQVcsRUFBRSxHQUFHLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDMUQsQ0FBQztpQkFDSjtnQkFDRDtvQkFDSSxPQUFPLEVBQUUsWUFBWTtvQkFDckIsSUFBSSxFQUFFLEtBQUs7b0JBQ1gsV0FBVyxFQUFFLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNoRjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsS0FBSztvQkFDWCxXQUFXLEVBQUUsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2hGO2dCQUNEO29CQUNJLElBQUksRUFBRSxLQUFLO29CQUNYLFdBQVcsRUFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDaEY7Z0JBQ0Q7b0JBQ0ksSUFBSSxFQUFFLE9BQU87b0JBQ2IsV0FBVyxFQUFFLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsRjtnQkFDRDtvQkFDSSxJQUFJLEVBQUUsT0FBTztvQkFDYixXQUFXLEVBQUUsR0FBRyxFQUFFLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ2xGLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQztJQUFBLENBQUM7SUFFSyxjQUFjLENBQUMsUUFBbUM7UUFDckQsSUFBSSxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ2hFLHlGQUF5RjtZQUN6RixJQUFJO2dCQUNBLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ25DO1lBQUMsT0FBTyxLQUFLLEVBQUU7Z0JBQ1osT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3Q0FBd0MsRUFBRSxRQUFRLENBQUMsQ0FBQzthQUNuRTtTQUNKO0lBQ0wsQ0FBQztJQUVNLGFBQWEsQ0FBQyxJQUE4QjtRQUMvQyxJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssV0FBVyxDQUFDLElBQUksRUFBRTtZQUNuQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEM7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFTyxVQUFVLENBQUMsUUFBbUMsRUFBRSxjQUF1QixFQUFFLG9CQUE0QyxJQUFJOztRQUM3SCxJQUFJLFFBQVEsRUFBRTtZQUNWLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQztZQUNyQixRQUFRLFFBQVEsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3RCLEtBQUssV0FBVyxDQUFDLFdBQVc7b0JBQ3hCLElBQUksT0FBQSxRQUFRLENBQUMsWUFBWSwwQ0FBRSxNQUFNLElBQUcsQ0FBQyxFQUFFO3dCQUNuQyxTQUFTLFNBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsMENBQUUsS0FBSyxDQUFDO3FCQUMvQztvQkFDRCxNQUFNO2dCQUNWLEtBQUssV0FBVyxDQUFDLEtBQUs7b0JBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUM7MkJBQ3ZDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDOytCQUNyQyxRQUFRLENBQUMsTUFBYyxDQUFDLE9BQU8sS0FBSyxRQUFRLENBQUMsRUFBRTt3QkFDdkQsVUFBSSxRQUFRLENBQUMsU0FBUywwQ0FBRSxZQUFZLEVBQUU7NEJBQ2xDLE1BQU0sVUFBVSxHQUFHLFFBQVEsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxXQUM3RCxPQUFBLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxZQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsMENBQUUsTUFBTSxDQUFBLENBQUEsRUFBQSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQ3RELElBQUksVUFBVSxhQUFWLFVBQVUsdUJBQVYsVUFBVSxDQUFFLE1BQU0sRUFBRTtnQ0FDcEIsU0FBUyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7NkJBQ25DO2lDQUFNO2dDQUNILFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDOzZCQUM5Qjt5QkFDSjtxQkFDSjtvQkFDRCxNQUFNO2dCQUNWLEtBQUssV0FBVyxDQUFDLElBQUk7b0JBQ2pCLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDO29CQUMzQixNQUFNO2FBQ2I7WUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsU0FBUyxFQUFFO29CQUN2RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDM0U7Z0JBQ0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFFBQVEsRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxDQUFDLENBQUM7YUFDdEY7WUFDRCx3RkFBd0Y7WUFDeEYsMEZBQTBGO1lBQzFGLElBQUk7U0FDUDtJQUNMLENBQUM7SUFFTyxlQUFlLENBQUMsSUFBWTtRQUNoQyxRQUFRLElBQUksRUFBRTtZQUNWLEtBQUssV0FBVyxDQUFDLElBQUksQ0FBQztZQUN0QixLQUFLLFdBQVcsQ0FBQyxLQUFLLENBQUM7WUFDdkIsS0FBSyxXQUFXLENBQUMsV0FBVztnQkFDeEIsT0FBTyxJQUFJLENBQUM7U0FDbkI7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRU8scUJBQXFCLENBQUMsUUFBbUMsRUFBRSxjQUF1QixFQUFFLGlCQUF5QyxFQUFFLFNBQWM7UUFDakosTUFBTSwwQkFBMEIsR0FBRyxLQUFLLENBQUM7UUFDekMsTUFBTSxzQkFBc0IsR0FDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN0SixNQUFNLG9CQUFvQixHQUFtQixJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNwSyxNQUFNLDJCQUEyQixHQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxjQUFjLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDakssTUFBTSw4QkFBOEIsR0FBRywyQkFBMkIsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxJQUFJLE9BQU8sQ0FBQyxTQUFTLEtBQUssMEJBQTBCLENBQUMsQ0FBQztRQUMxTCxJQUFJLDZCQUE2QixHQUFHLEtBQUssQ0FBQztRQUMxQyxNQUFNLFVBQVUsR0FBaUIsc0JBQXNCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLFNBQVMsS0FBSyxRQUFRLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztZQUNwSCxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxLQUFLLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUYsSUFBSSxvQkFBb0IsR0FBRyxJQUFJLENBQUM7UUFDaEMsOEJBQThCLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFOztZQUNoRCxJQUFJLFVBQVUsQ0FBQyxTQUFTLElBQUksUUFBUSxDQUFDLE9BQU8sS0FBSyxNQUFNLEVBQUU7Z0JBQ3JELDZCQUE2QixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzNGO1lBQ0QsSUFBSSw2QkFBNkIsRUFBRTtnQkFDL0IsSUFBSSxlQUFlLHFCQUFRLFVBQVUsQ0FBRSxDQUFDO2dCQUN4QyxJQUFJLENBQUEsZUFBZSxhQUFmLGVBQWUsdUJBQWYsZUFBZSxDQUFFLFNBQVMsTUFBSyxVQUFVLENBQUMsU0FBUyxFQUFFO29CQUNyRCxLQUFLLE1BQU0sR0FBRyxJQUFJLGVBQWUsRUFBRTt3QkFDL0IsSUFBSSxlQUFlLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFOzRCQUNyQyxlQUFlLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQyxTQUFTLElBQUksZUFBZSxDQUFDLFNBQVMsQ0FBQzs0QkFDOUUsZUFBZSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUMsZUFBZSxJQUFJLGVBQWUsQ0FBQyxlQUFlLENBQUM7NEJBQ2hHLG1CQUFtQjs0QkFDbkIsZUFBZSxDQUFDLFFBQVEscUJBQUcsZUFBZSxDQUFDLFFBQVEsMENBQUUsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLDJDQUFHLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSwyQ0FBRyxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDOzRCQUMxSCxlQUFlLENBQUMsUUFBUSxlQUFHLFVBQVUsQ0FBQyxRQUFRLDBDQUFFLE1BQU0sQ0FBQyxHQUFHLDJDQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7eUJBQ2pHO3FCQUNKO2lCQUNKO3FCQUFNO29CQUNILGVBQWUscUJBQVEsVUFBVSxDQUFFLENBQUM7aUJBQ3ZDO2dCQUNELElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLGVBQWUsRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQzlGLG9CQUFvQixHQUFHLEtBQUssQ0FBQzthQUNoQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxVQUFVLElBQUksb0JBQW9CLEVBQUU7WUFDcEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztTQUM1RjthQUFNLElBQUksUUFBUSxDQUFDLEtBQUssS0FBSyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUUsNEZBQTRGO1lBQ3ZJLFFBQVEsQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztTQUNsQztJQUNMLENBQUM7SUFFTyx1QkFBdUIsQ0FBQyxhQUEyQixFQUFFLE9BQVk7UUFDckUsSUFBSSxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzVCLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3hHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsRUFBRTtZQUMxRSxlQUFlLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ2xFO1FBQ0QsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQztJQUVPLGVBQWUsQ0FBQyxLQUFLLEVBQUUsT0FBTztRQUNsQyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ2xDLE9BQU8sS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQW9CLEVBQUUsRUFBRTtnQkFDdEMsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDdkcsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVPLFFBQVEsQ0FBQyxJQUFJLEVBQUUsT0FBTztRQUMxQixJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDMUQsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUNwRCxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUNwQyxTQUFTLEdBQUcsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDOUI7UUFDRCxRQUFRLFNBQVMsRUFBRTtZQUNmLEtBQUssUUFBUTtnQkFDVCxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDOUMsTUFBTTtZQUNWLEtBQUssU0FBUztnQkFDVixJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDL0MsTUFBTTtZQUNWLEtBQUssUUFBUTtnQkFDVCxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLEVBQUUsaURBQWlEO29CQUNuRyxJQUFJLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsRUFBRSxvRUFBb0U7d0JBQzFILElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO3FCQUNqRDt5QkFBTTt3QkFDSCxJQUFJLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztxQkFDL0M7aUJBQ0o7cUJBQU07b0JBQ0gsSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7aUJBQ2pEO2dCQUNELE1BQU07WUFDVixLQUFLLFFBQVE7Z0JBQ1QsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO29CQUN4QixJQUFJLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLFFBQVEsRUFBRTt3QkFDaEMsSUFBSSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7cUJBQy9DO3lCQUFNLElBQUksT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxFQUFFO3dCQUN2QyxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztxQkFDakQ7aUJBQ0o7Z0JBQ0QsTUFBTTtTQUNiO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVPLGlCQUFpQixDQUFDLEtBQWE7UUFDbkMsUUFBUSxLQUFLLEVBQUU7WUFDWCxLQUFLLElBQUksQ0FBQztZQUNWLEtBQUssS0FBSztnQkFDTixPQUFPLElBQUksQ0FBQztTQUNuQjtJQUNMLENBQUM7SUFFTyxrQkFBa0IsQ0FBQyxJQUFnQixFQUFFLE9BQVk7UUFDckQsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUFDLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbkYsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3JHLElBQUksT0FBTyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQztTQUFFO1FBQzFELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxtQkFBbUIsQ0FBQyxJQUFnQixFQUFFLE9BQVk7UUFDdEQsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDdEYsSUFBSSxPQUFPLFFBQVEsS0FBSyxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFFLHNFQUFzRTtTQUN6RztRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxrQkFBa0IsQ0FBQyxJQUFnQixFQUFFLE9BQVk7O1FBQ3JELElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBRTtZQUNsQixJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQywwQ0FBRSxXQUFXLEVBQUUsQ0FBQztTQUNwQztRQUNELElBQUksbUJBQW1CLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzNDLElBQUksT0FBTyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxFQUFFO1lBQ3RDLG1CQUFtQixHQUFHLE9BQUEsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQywwQ0FBRSxXQUFXLE9BQU0sSUFBSSxDQUFDO1NBQ2pFO1FBQ0QsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxtQkFBbUIsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3RILElBQUksT0FBTyxRQUFRLEtBQUssU0FBUyxFQUFFO1lBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLFFBQVEsQ0FBQztTQUFFO1FBQzFELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFTyxnQkFBZ0IsQ0FBQyxJQUFnQixFQUFFLE9BQVk7UUFDbkQsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsbUZBQW1GO1FBQ2xKLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5RCxVQUFVLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2pFLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDakUsSUFBSSxVQUFVLEdBQVEsSUFBSSxDQUFDLHdCQUF3QixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2hGLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDO1FBQ2pELE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUMvRixJQUFJLE9BQU8sUUFBUSxLQUFLLFNBQVMsRUFBRTtZQUMvQixJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsUUFBUSxDQUFDO1NBQ3RCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVPLHdCQUF3QixDQUFDLFNBQWlCO1FBQzlDLE1BQU0sUUFBUSxHQUFHLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3JDLE1BQU0sYUFBYSxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsR0FBRyxRQUFRLENBQUMsT0FBTyxFQUFFLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMxRyxPQUFPLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFTywyQkFBMkIsQ0FBQyxRQUFnQixFQUFFLGFBQWtCLEVBQUUsV0FBc0MsRUFBRSxZQUF3QztRQUN0SixJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxXQUFXLEVBQUU7WUFDeEMsUUFBUSxRQUFRLEVBQUU7Z0JBQ2QsS0FBSyxVQUFVO29CQUNYLE9BQU8sQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsT0FBTyxDQUFDLFdBQVcsS0FBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ25FLEtBQUssYUFBYTtvQkFDZCxPQUFPLENBQUEsYUFBYSxhQUFiLGFBQWEsdUJBQWIsYUFBYSxDQUFFLE9BQU8sQ0FBQyxXQUFXLE9BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNyRSxLQUFLLFlBQVk7b0JBQ2IsT0FBTyxDQUFBLGFBQWEsYUFBYixhQUFhLHVCQUFiLGFBQWEsQ0FBRSxVQUFVLENBQUMsV0FBVyxHQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDakUsS0FBSyxVQUFVO29CQUNYLE9BQU8sQ0FBQSxhQUFhLGFBQWIsYUFBYSx1QkFBYixhQUFhLENBQUUsUUFBUSxDQUFDLFdBQVcsR0FBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQy9ELEtBQUssU0FBUztvQkFDVixPQUFPLGFBQWEsS0FBSyxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pFLEtBQUssWUFBWTtvQkFDYixPQUFPLGFBQWEsS0FBSyxJQUFJLElBQUksYUFBYSxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pFLEtBQUssU0FBUztvQkFDViw4REFBOEQ7b0JBQzlELE9BQU8sYUFBYSxHQUFHLFdBQVcsSUFBSSxhQUFhLEdBQUcsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDdEYsS0FBSyxHQUFHO29CQUNKLE9BQU8sYUFBYSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3hELEtBQUssSUFBSTtvQkFDTCxPQUFPLGFBQWEsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUN4RCxLQUFLLEdBQUc7b0JBQ0osT0FBTyxhQUFhLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDdEQsS0FBSyxHQUFHO29CQUNKLE9BQU8sYUFBYSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3RELEtBQUssSUFBSTtvQkFDTCxPQUFPLGFBQWEsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUN2RCxLQUFLLElBQUk7b0JBQ0wsT0FBTyxhQUFhLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUMxRDtTQUNKO1FBQ0QsT0FBTyxLQUFLLENBQUMsQ0FBQztJQUNsQixDQUFDO0lBRU8seUJBQXlCLENBQUMsS0FBWTtRQUMxQyxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUM7UUFDcEIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNqQixJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUNoRCxVQUFVLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3pCO2lCQUFNLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxJQUFJLE9BQU8sSUFBSSxLQUFLLFNBQVMsRUFBRTtnQkFDOUQsUUFBUSxJQUFJLEVBQUU7b0JBQ1YsS0FBSyxLQUFLO3dCQUNOLFVBQVUsSUFBSSxNQUFNLENBQUM7d0JBQ3JCLE1BQU07b0JBQ1YsS0FBSyxJQUFJO3dCQUNMLFVBQVUsSUFBSSxNQUFNLENBQUM7d0JBQ3JCLE1BQU07b0JBQ1YsS0FBSyxJQUFJLENBQUM7b0JBQ1YsS0FBSyxLQUFLO3dCQUNOLFVBQVUsSUFBSSxJQUFJLElBQUksR0FBRyxDQUFDO3dCQUMxQixNQUFNO29CQUNWLEtBQUssTUFBTSxDQUFDO29CQUNaLEtBQUssT0FBTzt3QkFDUixVQUFVLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7d0JBQ3RDLE1BQU07aUJBQ2I7YUFDSjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzVCLFVBQVUsR0FBRyxJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDckQ7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUk7WUFDQSxvQ0FBb0M7WUFDcEMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDM0I7UUFBQyxPQUFPLENBQUMsRUFBRTtZQUNSLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDakIsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDO0lBRU8sb0JBQW9CLENBQUMsYUFBa0I7UUFDM0MsTUFBTSxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDOUIsTUFBTSxLQUFLLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDckQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QixNQUFNLFlBQVksR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUM5RSxJQUFJLFlBQVksRUFBRTtnQkFDZCxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO2FBQ2pFO1NBQ0o7UUFDRCxPQUFPLGtCQUFrQixDQUFDO0lBQzlCLENBQUM7SUFFTyxlQUFlLENBQUMsU0FBaUMsRUFBRSxVQUF3QixFQUFFLGNBQXVCLEVBQUUsUUFBbUMsRUFBRSxpQkFBeUM7O1FBQ3hMLEtBQUssTUFBTSxJQUFJLElBQUksVUFBVSxFQUFFO1lBQzNCLElBQUksVUFBVSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDakMsTUFBTSxRQUFRLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDeEMsUUFBUSxJQUFJLEVBQUU7b0JBQ1YsS0FBSyxVQUFVO3dCQUNYLElBQUksY0FBYyxFQUFFOzRCQUNoQixNQUFNLFVBQVUsR0FBRyxHQUFHLFFBQVEsRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQzs0QkFDNUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksRUFBRSxFQUFFO2dDQUFFLFFBQVEsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQzs2QkFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUN6Rzs2QkFBTSxJQUFJLENBQUEsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLE1BQU0sSUFBRyxDQUFDLEVBQUU7NEJBQzdCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxRQUFRLEVBQUUsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLENBQUMsQ0FBQzt5QkFDckU7d0JBQ0QsTUFBTTtvQkFDVixLQUFLLFFBQVE7d0JBQ1QsSUFBSSxjQUFjLEdBQTJCLEVBQUUsQ0FBQzt3QkFDaEQsSUFBSSxjQUFjLElBQUksVUFBVSxDQUFDLFFBQVEsS0FBSyxTQUFTLEVBQUU7NEJBQ3JELGNBQWMsR0FBRyxTQUFTLENBQUMsQ0FBQyxpQ0FBaUM7eUJBQ2hFO3dCQUNELElBQUksT0FBTyxRQUFRLEtBQUssUUFBUSxFQUFFOzRCQUM5QixRQUFRLFFBQVEsQ0FBQyxJQUFJLEVBQUU7Z0NBQ25CLEtBQUssVUFBVTtvQ0FDWCxjQUFjLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBbUIsQ0FBQyxDQUFDO29DQUNoRixNQUFNO2dDQUNWLEtBQUssU0FBUztvQ0FDVixjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBbUIsQ0FBQyxDQUFDO29DQUNwRixNQUFNO2dDQUNWLEtBQUssT0FBTztvQ0FDUixjQUFjLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBbUIsQ0FBQyxDQUFDO29DQUNsRixNQUFNO2dDQUNWLEtBQUssWUFBWTtvQ0FDYixjQUFjLEdBQUksU0FBb0IsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDO29DQUNuRSxNQUFNOzZCQUNiO3lCQUNKOzZCQUFNLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLFdBQVcsRUFBRTs0QkFDekMsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7NEJBQ2pDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7eUJBQzVGO3dCQUNELElBQUksY0FBYyxJQUFJLGNBQWMsRUFBRSxFQUFHLHVFQUF1RTs0QkFDNUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsY0FBd0IsQ0FBQzt5QkFDN0Q7NkJBQU0sSUFBSSxpQkFBaUIsRUFBRTs0QkFDMUIsaUJBQWlCLENBQUMsS0FBSyxHQUFHLGNBQWMsSUFBSSxTQUFTLENBQUM7eUJBQ3pEO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxXQUFXO3dCQUNaLElBQUksY0FBYyxFQUFFOzRCQUNoQixRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO3lCQUNuRDs2QkFBTTs0QkFDSCxpQkFBaUIsQ0FBQyxTQUFTLEdBQUcsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLENBQUM7eUJBQzFEO3dCQUNELElBQUksUUFBUSxDQUFDLE9BQU8sS0FBSyxhQUFhLGlCQUFJLFFBQVEsQ0FBQyxXQUFXLDBDQUFFLFVBQVUsMENBQUUsTUFBTSxDQUFBLEVBQUU7NEJBQy9FLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO3lCQUMxRTt3QkFDRCxNQUFNO29CQUNWLEtBQUssV0FBVzt3QkFDWixJQUFJLGNBQWMsRUFBRTs0QkFDaEIsUUFBUSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQzt5QkFDL0M7NkJBQU0sSUFBSSxpQkFBaUIsRUFBRTs0QkFDMUIsaUJBQWlCLENBQUMsSUFBSSxtQ0FDZixpQkFBaUIsQ0FBQyxJQUFJLEdBQUssRUFBRSxLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUNsRixDQUFDO3lCQUNMO3dCQUNELE1BQU07b0JBQ1YsS0FBSyxpQkFBaUI7d0JBQ2xCLElBQUksY0FBYyxFQUFFOzRCQUNoQixRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDO3lCQUN6RDs2QkFBTSxJQUFJLGlCQUFpQixFQUFFOzRCQUMxQixpQkFBaUIsQ0FBQyxJQUFJLEdBQUcsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLFFBQVEsYUFBUixRQUFRLHVCQUFSLFFBQVEsQ0FBRSxPQUFPLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxFQUFFLEVBQUUsQ0FBQzt5QkFDbkg7d0JBQ0QsTUFBTTtvQkFDVixxRkFBcUY7aUJBQ3hGO2FBQ0o7U0FDSjtJQUNMLENBQUM7SUFFTyxlQUFlLENBQUMsU0FBaUIsRUFBRSxpQkFBc0IsRUFBRSxHQUFRO1FBQ3ZFLElBQUksYUFBYSxHQUFhLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDbkQsSUFBSSxpQkFBeUIsQ0FBQztRQUM5QixhQUFhLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUFFO2dCQUM5QyxPQUFPLElBQUksQ0FBQzthQUNmO2lCQUFNO2dCQUNILGlCQUFpQixHQUFHLElBQUksQ0FBQzthQUM1QjtRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxpQkFBaUIsRUFBRTtZQUNuQixhQUFhLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDNUM7UUFDRCxLQUFLLE1BQU0sV0FBVyxJQUFJLGFBQWEsRUFBRTtZQUNyQyxRQUFRLFdBQVcsQ0FBQyxXQUFXLEVBQUUsRUFBRTtnQkFDL0IsS0FBSyxNQUFNO29CQUNQLGlCQUFpQixDQUFDLElBQUksbUNBQ2YsaUJBQWlCLENBQUMsSUFBSSxHQUFLLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxDQUMvQyxDQUFDO29CQUNGLE1BQU07Z0JBQ1YsS0FBSyxXQUFXO29CQUNaLGlCQUFpQixDQUFDLElBQUksbUNBQ2YsaUJBQWlCLENBQUMsSUFBSSxHQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxDQUNwRCxDQUFDO29CQUNGLE1BQU07Z0JBQ1YsS0FBSyxRQUFRO29CQUNULGlCQUFpQixDQUFDLElBQUksbUNBQ2YsaUJBQWlCLENBQUMsSUFBSSxHQUFLLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUNqRCxDQUFDO29CQUNGLE1BQU07YUFDYjtZQUNELElBQUksV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDcEQsTUFBTSxhQUFhLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDN0MsSUFBSSxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDMUIsaUJBQWlCLENBQUMsS0FBSyxHQUFHLEdBQUcsYUFBSCxHQUFHLHVCQUFILEdBQUcsQ0FBRSxPQUFPLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3BFO2FBQ0o7U0FDSjtJQUNMLENBQUM7SUFFTyxnQkFBZ0IsQ0FBQyxTQUFpQixFQUFFLEtBQWE7UUFDckQsU0FBUyxHQUFHLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLCtCQUErQjtRQUNqRixPQUFPLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUscUJBQXFCLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BLLENBQUM7SUFFTyxvQkFBb0IsQ0FBQyxTQUFpQixFQUFFLEtBQWE7UUFDekQsU0FBUyxHQUFHLFNBQVMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxDQUFDLCtCQUErQjtRQUNqRixPQUFPLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLHFCQUFxQixFQUFFLFNBQVMsRUFBRSxxQkFBcUIsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNsSixDQUFDO0lBRU8sa0JBQWtCLENBQUMsU0FBaUIsRUFBRSxLQUFhO1FBQ3ZELFNBQVMsR0FBRyxTQUFTLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsSUFBSSxDQUFDLENBQUMsQ0FBQywrQkFBK0I7UUFDakYsT0FBTyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLEVBQUUsV0FBVyxFQUFFLElBQUksRUFBRSxxQkFBcUIsRUFBRSxTQUFTLEVBQUUscUJBQXFCLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkosQ0FBQztJQUVPLDZCQUE2QixDQUFDLE9BQU8sRUFBRSxpQkFBeUI7UUFDcEUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3pDLE1BQU0saUJBQWlCLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLEVBQzFFLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxFQUN4RSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsMkJBQTJCLENBQUMsaUJBQWlCLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFDbkcsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLDJCQUEyQixDQUFDLGlCQUFpQixFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDeEcsTUFBTSx3QkFBd0IsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxpQkFBaUIsS0FBSyxPQUFPLENBQUM7UUFDeEYsTUFBTSxVQUFVLEdBQUc7WUFDZixNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxTQUFTO1lBQ2hDLFdBQVcsRUFBRSxpQkFBaUI7WUFDOUIsYUFBYSxFQUFFLElBQUk7WUFDbkIsYUFBYSxFQUFFLEtBQUs7WUFDcEIsV0FBVyxFQUFFLHdCQUF3QixDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLFNBQVMsRUFBRSxDQUFDLEVBQUU7WUFDcEcsMEJBQTBCO1NBQzdCLENBQUM7UUFDRixJQUFJLGlCQUFpQixLQUFLLE9BQU8sRUFBRTtZQUMvQixJQUFJLG1CQUFtQixLQUFLLENBQUMsQ0FBQyxJQUFJLG1CQUFtQixLQUFLLENBQUMsQ0FBQyxFQUFFO2dCQUMxRCxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pELGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDakQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO2dCQUMzRSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQzNFLG9DQUFvQztnQkFDcEMsSUFBSSxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLE9BQU8sRUFBRTtvQkFDbkMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDL0Q7YUFDSjtZQUNELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUsQ0FBQztZQUN2QyxPQUFPO1NBQ1Y7UUFDRCxJQUFJLG1CQUFtQixLQUFLLENBQUMsQ0FBQyxJQUFJLG1CQUFtQixLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQzFELGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUNuQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDdEM7YUFBTTtZQUNILGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNqRCxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDakQsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ25DLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN0QztRQUNELE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLG9CQUFvQixFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDbEUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUNsRSxvQ0FBb0M7UUFDcEMsSUFBSSxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLE9BQU8sRUFBRTtZQUNuQyxPQUFPLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQy9EO1FBQ0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQzNDLENBQUM7SUFFTywyQkFBMkIsQ0FBQyxzQkFBc0IsRUFBRSxVQUFVO1FBQ2xFLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxzQkFBc0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDcEQsSUFBSSxzQkFBc0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEtBQUssVUFBVSxFQUFFO2dCQUNqRCxPQUFPLENBQUMsQ0FBQzthQUNaO1NBQ0o7UUFDRCxPQUFPLENBQUMsQ0FBQyxDQUFDO0lBQ2QsQ0FBQztJQUVPLG9CQUFvQixDQUFDLFFBQTJCOztRQUNwRCxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixJQUFJLENBQUUsSUFBSSxDQUFDLHFCQUE2QixDQUFDLElBQUksRUFBRTtZQUFFLE9BQU87U0FBRTtRQUN6RixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMscUJBQXFCLENBQUMsa0JBQWtCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUNqRSxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDekMsSUFBSSxRQUFRLEVBQUU7WUFDVixJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUU7Z0JBQ2xCLE1BQU0saUJBQWlCLEdBQUcsWUFBWSxDQUFDLENBQUEsUUFBUSxhQUFSLFFBQVEsdUJBQVIsUUFBUSxDQUFFLE9BQU8sS0FBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7Z0JBQ2hHLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDMUMsTUFBTSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3BELElBQUksaUJBQWlCLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO3dCQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUM7cUJBQzVEO3lCQUFNO3dCQUNILElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztxQkFDbkM7aUJBQ0o7YUFDSjtZQUNELDhDQUE4QztZQUM5QyxJQUFJLFFBQVEsQ0FBQyxjQUFjLElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7Z0JBQzNELEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsRUFBRTtvQkFDakUsS0FBSyxJQUFJLE1BQU0sR0FBRyxDQUFDLEVBQUUsTUFBTSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLE1BQU0sRUFBRSxFQUFFO3dCQUN6RCxNQUFNLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQzt3QkFDM0QsSUFBSSxTQUFTLElBQUksU0FBUyxLQUFLLHNCQUFzQixDQUFDLFNBQVM7NEJBQzNELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxLQUFLLFNBQVMsQ0FBQyxXQUFXLEVBQUUsRUFBRTs0QkFDMUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUM7NEJBQ3RFLDBDQUEwQzs0QkFDMUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLFNBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLDBDQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsQ0FBQzs0QkFDN0YsTUFBTTt5QkFDVDtxQkFDSjtpQkFDSjthQUNKO1lBQ0QsSUFBSSxhQUFBLFFBQVEsQ0FBQyxPQUFPLDBDQUFFLFVBQVUsMENBQUUsTUFBTSxrQkFBSSxRQUFRLENBQUMsT0FBTywwQ0FBRSxVQUFVLDBDQUFFLE1BQU0sQ0FBQSxFQUFFO2dCQUM5RSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDbEU7aUJBQU07Z0JBQ0gsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO2dCQUNqQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNsRTtZQUNELE1BQU0sZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUN0RCxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLGNBQWMsQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLENBQUM7WUFDdEgsSUFBSSxnQkFBZ0IsRUFBRTtnQkFDbEIsUUFBUSxDQUFDLG9CQUFvQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQzthQUM1RztZQUNELGNBQWMsQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxjQUFjLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsRUFDMUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN4RixjQUFjLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQzNELGNBQWMsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQ3hFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDcEcsSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUM5RCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDdkUsVUFBVSxDQUFDLEdBQUcsRUFBRTtvQkFDWixtQkFBbUIsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2dCQUMxRSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7YUFDVjtZQUNELElBQUksUUFBUSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3ZFLDJDQUEyQzthQUM5QztZQUNELFFBQVEsQ0FBQyxFQUFFLE9BQU8sRUFBRSwwQ0FBMEMsRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQztTQUN0RjthQUFNO1lBQ0gsY0FBYyxDQUFDLFVBQVUsQ0FDckIsYUFBYSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxjQUFjLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQzdHLGNBQWMsQ0FBQyxVQUFVLENBQ3JCLGFBQWEsQ0FBQyxJQUFJLENBQUMscUJBQXFCLEVBQUUsY0FBYyxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQ3hILElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFO2dCQUMxQixJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxlQUFlLEVBQUU7b0JBQzdFLE1BQU0sQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO2lCQUN4QjtZQUNMLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLENBQUMsYUFBYSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGFBQWEsSUFBSSxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztRQUN0RixjQUFjLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNoRixJQUFJLENBQUMsa0JBQWtCLEdBQUcsdUJBQXVCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFTyxtQkFBbUIsQ0FBQyxJQUFTO1FBQ2pDLE1BQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLFVBQVUsSUFBSSxHQUFHLENBQUMsU0FBUyxLQUFLLGdCQUFnQixDQUFDLENBQUM7UUFDcEcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEtBQUssR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ2pHLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLFVBQVUsQ0FBQyxRQUFnQixFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUztRQUMvRCxJQUFJLFFBQVEsS0FBSyxXQUFXLENBQUMsSUFBSSxFQUFFO1lBQy9CLE1BQU0sMkJBQTJCLEdBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLGNBQWMsQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNqSyxNQUFNLG9CQUFvQixHQUFHLDJCQUEyQixDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEtBQUssS0FBSyxDQUFDLENBQUM7WUFDeEcsSUFBSSw2QkFBNkIsR0FBRyxLQUFLLENBQUM7WUFDMUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFOztnQkFDdEMsSUFBSSxVQUFVLENBQUMsU0FBUyxFQUFFO29CQUN0Qiw2QkFBNkIsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUNsRixJQUFJLDZCQUE2QixFQUFFO3dCQUMvQixJQUFJLFVBQVUsRUFBRTs0QkFDWixLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0NBQzdELE1BQU0sZUFBZSxHQUFHLE9BQUEsVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLDBDQUFFLFFBQVEsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsRUFBRSxPQUFPLENBQUMsUUFBUSxFQUFFLEVBQUUsTUFBSyxFQUFFLENBQUM7Z0NBQ2xKLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUMsZUFBZSxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQztnQ0FDbEksVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxTQUFTLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO2dDQUN4RyxVQUFVLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUM7NkJBQ2xHO3lCQUNKOzZCQUFNLElBQUksU0FBUyxFQUFFOzRCQUNsQixJQUFJLFVBQVUsQ0FBQyxlQUFlLEVBQUU7Z0NBQzVCLFNBQVMsQ0FBQyxJQUFJLEdBQUc7b0NBQ2IsSUFBSSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsT0FBTztvQ0FDakMsT0FBTyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsRUFBRTtpQ0FDbkUsQ0FBQzs2QkFDTDs0QkFDRCxJQUFJLFVBQVUsQ0FBQyxTQUFTLEVBQUU7Z0NBQ3RCLFNBQVMsQ0FBQyxJQUFJLEdBQUc7b0NBQ2IsS0FBSyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsRUFBRTtpQ0FDM0QsQ0FBQzs2QkFDTDs0QkFDRCxJQUFJLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0NBQ3JCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7NkJBQzlEO3lCQUNKO3FCQUNKO2lCQUNKO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Q0FFSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBQQklHcmlkQ29uZmlndXJhdGlvbixcclxuICAgIFBCSUdyaWRFdmVudHMsXHJcbiAgICBQQklHcmlkQ29sdW1uLFxyXG4gICAgR3JpZENlbGxDbGlja0FyZ3VtZW50cyxcclxuICAgIEdyaWRDZWxsUHJlcGFyZWRBcmd1bWVudHMsXHJcbiAgICBHcmlkQmFzZUFyZ3VtZW50cyxcclxuICAgIEdyaWRDb250ZXh0TWVudVByZXBhcmluZ0FyZ3VtZW50cyxcclxuICAgIEdyaWRFZGl0aW5nU3RhcnRBcmd1bWVudHMsXHJcbiAgICBHcmlkRWRpdG9yUHJlcGFyZWRBcmd1bWVudHMsXHJcbiAgICBHcmlkRWRpdG9yUHJlcGFyaW5nQXJndW1lbnRzLFxyXG4gICAgR3JpZEV4cG9ydGluZ0FyZ3VtZW50cyxcclxuICAgIEdyaWRJbml0TmV3Um93QXJndW1lbnRzLFxyXG4gICAgR3JpZEluaXRpYWxpemVkQXJndW1lbnRzLFxyXG4gICAgR3JpZFJvd0NsaWNrQXJndW1lbnRzLFxyXG4gICAgR3JpZFJvd1ByZXBhcmVkQXJndW1lbnRzLFxyXG4gICAgR3JpZFJvd1JlbW92ZWRBcmd1bWVudHMsXHJcbiAgICBHcmlkUm93VmFsaWRhdGluZ0FyZ3VtZW50cyxcclxuICAgIEdyaWRTZWxlY3Rpb25DaGFuZ2VkQXJndW1lbnRzLFxyXG4gICAgR3JpZFJvd1VwZGF0aW5nQXJndW1lbnRzXHJcbn0gZnJvbSAnLi4vY29udHJhY3RzL2dyaWQnO1xyXG5pbXBvcnQgRGV2RXhwcmVzcyBmcm9tICdkZXZleHRyZW1lJztcclxuaW1wb3J0IGR4RGF0YUdyaWQsIHsgZHhEYXRhR3JpZENvbHVtbiB9IGZyb20gJ2RldmV4dHJlbWUvdWkvZGF0YV9ncmlkJztcclxuaW1wb3J0IHsgcm93VHlwZUluZm8sIGN1c3RvbUFjdGlvbkNvbHVtbkluZm8gfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQ0dGU3RvcmFnZUtleXMgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvZW51bXMnO1xyXG5pbXBvcnQgeyBDb2x1bW5Gb3JtYXQgfSBmcm9tICcuLi9jb250cmFjdHMvY29sdW1uJztcclxuaW1wb3J0IHsgZ2V0U3RvcmFnZUtleSwgZ2V0Q2xhc3NOYW1lQnlUaGVtZU5hbWUsIGFwcGx5RmlsdGVyQ3NzQ2xhc3MsIGFwcFRvYXN0LCBUb0RpY3Rpb25hcnkgfSBmcm9tICcuLi8uLi91dGlsaXRpZXMvdXRpbGl0eUZ1bmN0aW9ucyc7XHJcbmltcG9ydCB7IEdyaWRTdGF0ZVNhdmVJbmZvIH0gZnJvbSAnLi4vY29udHJhY3RzL2dyaWQnO1xyXG5pbXBvcnQgeyBHcmlkVmlld0luZm8gfSBmcm9tICcuLi9jb250cmFjdHMvdmlldy1zZWxlY3Rpb24nO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBCSUdyaWRPcHRpb25zTW9kZWwgaW1wbGVtZW50cyBQQklHcmlkQ29uZmlndXJhdGlvbiwgUEJJR3JpZEV2ZW50cyB7XHJcbiAgICAvKiBodHRwczovL2pzLmRldmV4cHJlc3MuY29tL0RvY3VtZW50YXRpb24vQXBpUmVmZXJlbmNlL1VJX1dpZGdldHMvZHhEYXRhR3JpZC9Db25maWd1cmF0aW9uLyBmb3IgY29uZmlndXJhdGlvbiBvZiB0aGUgZHhHcmlkICovXHJcblxyXG4gICAgLy8jcmVnaW9uIEdyaWQgQ29uZmlndXJhdGlvbiBNZW1iZXJzXHJcbiAgICAvLyBiYXNlQ29sdW1uc0xpc3Q6IEFycmF5PElNU19XZWIuSUdyaWRDb2x1bW4+ID0gW107XHJcbiAgICAvLyBjb2x1bW5zOiBBcnJheTxJTVNfV2ViLklHcmlkQ29sdW1uPiA9IFtdO1xyXG4gICAgLyogU3BlY2lmaWVzIHRoZSBzaG9ydGN1dCBrZXkgdGhhdCBzZXRzIGZvY3VzIG9uIHRoZSB3aWRnZXQuXHJcbiAgICAgICAgSUUsIENocm9tZSwgU2FmYXJpLCBPcGVyYSAxNSs6IFtBTFRdICsgYWNjZXNza2V5XHJcbiAgICAgICAgT3BlcmEgcHJpb3IgdmVyc2lvbiAxNTogW1NISUZUXVtFU0NdICsgYWNjZXNza2V5XHJcbiAgICAgICAgRmlyZWZveDogW0FMVF1bU0hJRlRdICsgYWNjZXNza2V5ICovXHJcbiAgICBhY2Nlc3NLZXkgPSAnZyc7XHJcbiAgICBhbGxvd0FkZGluZyA9IGZhbHNlO1xyXG4gICAgYWxsb3dDb2x1bW5SZW9yZGVyaW5nID0gdHJ1ZTtcclxuICAgIGFsbG93Q29sdW1uUmVzaXppbmcgPSB0cnVlO1xyXG4gICAgYWxsb3dEYXRhRXhwb3J0ID0gZmFsc2U7XHJcbiAgICBhbGxvd0RlbGV0aW5nID0gZmFsc2U7XHJcbiAgICBhbGxvd1Jlb3JkZXJpbmcgPSBmYWxzZTsgLy8gbWFrZSBzdXJlIHJvd3MgY2FuIGJlIHJlLW9yZGVyZWRcclxuICAgIGFsbG93U2VsZWN0ZWREYXRhRXhwb3J0ID0gZmFsc2U7XHJcbiAgICBhbGxvd1VwZGF0aW5nID0gZmFsc2U7XHJcbiAgICBhbGxvd2VkUGFnZVNpemVzID0gWzUwLCAxNTAsIDMwMF07XHJcbiAgICBhdXRvRXhwYW5kQWxsID0gZmFsc2U7XHJcbiAgICBhdXRvTmF2aWdhdGVUb0ZvY3VzZWRSb3cgPSB0cnVlO1xyXG4gICAgY29sdW1uQXV0b1dpZHRoID0gZmFsc2U7XHJcbiAgICBjb2x1bW5NaW5XaWR0aCA9IDUwO1xyXG4gICAgY29sdW1uUmVuZGVyaW5nTW9kZSA9ICd2aXJ0dWFsJztcclxuICAgIGNvbHVtblJlc2l6aW5nTW9kZSA9ICd3aWRnZXQnO1xyXG4gICAgY29sdW1uczogUEJJR3JpZENvbHVtbltdID0gW107XHJcbiAgICBjb250ZXh0TWVudU1hcHBpbmdMaXN0OiBhbnkgPSBbXTtcclxuICAgIGRhdGFTb3VyY2U6IHN0cmluZyB8IEFycmF5PGFueT4gfCBEZXZFeHByZXNzLmRhdGEuRGF0YVNvdXJjZSB8IERldkV4cHJlc3MuZGF0YS5EYXRhU291cmNlT3B0aW9ucyA9IFtdO1xyXG4gICAgZGF0ZVNlcmlhbGl6YXRpb25Gb3JtYXQgPSAneXl5eS1NTS1kZHRISDptbTpzcyc7XHJcbiAgICBkaXNhYmxlZCA9IGZhbHNlO1xyXG4gICAgZWRpdGluZ01vZGUgPSAncm93JztcclxuICAgIGVsZW1lbnRBdHRyOiBhbnk7XHJcbiAgICBlbmFibGVBY3RpdmVTdGF0ZSA9IGZhbHNlO1xyXG4gICAgZW5hYmxlQ2FjaGUgPSB0cnVlO1xyXG4gICAgZW5hYmxlQ2VsbEhpbnQgPSB0cnVlO1xyXG4gICAgZW5hYmxlQ29sdW1uQ2hvb3NlciA9IGZhbHNlO1xyXG4gICAgZW5hYmxlQ29sdW1uRml4aW5nID0gdHJ1ZTtcclxuICAgIGVuYWJsZUNvbHVtbkhpZGluZyA9IHRydWU7XHJcbiAgICBlbmFibGVDb250ZXh0R3JwTWVudSA9IHRydWU7XHJcbiAgICBlbmFibGVDb250ZXh0TWVudSA9IHRydWU7XHJcbiAgICBlbmFibGVFcnJvclJvdyA9IHRydWU7XHJcbiAgICBlbmFibGVHcmlkRm9ybWF0dGluZyA9IGZhbHNlO1xyXG4gICAgZW5hYmxlTG9hZFBhbmVsID0gZmFsc2U7XHJcbiAgICBlbmFibGVTdGF0ZVN0b3JpbmcgPSBmYWxzZTtcclxuICAgIGZpbHRlclN5bmNFbmFibGVkID0gdHJ1ZTtcclxuICAgIGZpbHRlclZhbHVlOiBzdHJpbmcgfCBBcnJheTxhbnk+IHwgRnVuY3Rpb24gPSBudWxsO1xyXG4gICAgZ3JpZENvbXBvbmVudEluc3RhbmNlOiBkeERhdGFHcmlkID0gbnVsbDtcclxuICAgIGdyaWRGaWx0ZXJWYWx1ZSA9IG51bGw7XHJcbiAgICBncmlkTmFtZSA9ICdQb3J0Zm9saW9CSSBEYXRhIEdyaWQnO1xyXG4gICAgaGVpZ2h0OiBudW1iZXIgfCBzdHJpbmcgfCAoKCkgPT4gbnVtYmVyIHwgc3RyaW5nKTtcclxuICAgIGhpZ2hsaWdodENoYW5nZXMgPSBmYWxzZTtcclxuICAgIGhvdmVyU3RhdGVFbmFibGVkID0gdHJ1ZTtcclxuICAgIGlzTWFzdGVyR3JpZCA9IHRydWU7XHJcbiAgICBsaXN0R3JvdXBlZENvbHVtbnM6IHN0cmluZ1tdID0gW107XHJcbiAgICBub0RhdGFUZXh0ID0gJ05vIERhdGEnO1xyXG4gICAgcGFnZVNpemUgPSA1MDtcclxuICAgIHBhZ2luZ0VuYWJsZWQgPSB0cnVlO1xyXG4gICAgcmVmcmVzaE1vZGU6ICdmdWxsJyB8ICdyZXNoYXBlJyB8ICdyZXBhaW50JyA9ICdmdWxsJztcclxuICAgIHJlbW90ZU9wZXJhdGlvbnNFbmFibGVkID0gZmFsc2U7XHJcbiAgICByZXBhaW50Q2hhbmdlc09ubHkgPSBmYWxzZTtcclxuICAgIHJvd0FsdGVybmF0aW9uRW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgc2Nyb2xsTW9kZSA9ICdzdGFuZGFyZCc7XHJcbiAgICBzZWxlY3RlZFJvd0RhdGE6IGFueTtcclxuICAgIHNlbGVjdGVkVGhlbWUgPSAnJztcclxuICAgIHNlbGVjdGVkVGhlbWVDbGFzcyA9ICcnOyAgLy8gRW1wdHkgbWVhbnMgcmVndWxhciB0aGVtZSB3aWxsIGJlIGFwcGxpZWQuXHJcbiAgICBzZWxlY3Rpb25Nb2RlID0gJ3NpbmdsZSc7XHJcbiAgICBzaG93Qm9yZGVycyA9IHRydWU7XHJcbiAgICBzaG93Q29sdW1uSGVhZGVycyA9IHRydWU7XHJcbiAgICBzaG93Q29sdW1uTGluZXMgPSB0cnVlO1xyXG4gICAgc2hvd0RyYWdJY29ucyA9IGZhbHNlO1xyXG4gICAgc2hvd0ZpbHRlclBhbmVsID0gdHJ1ZTsgLy8gZmxhZyB0byBzaG93L2hpZGUgZmlsdGVyIGluZm8gYXQgYm90dG9tIG9mIGdyaWQuXHJcbiAgICBzaG93RmlsdGVyUm93ID0gdHJ1ZTtcclxuICAgIHNob3dHcm91cFBhbmVsID0gdHJ1ZTtcclxuICAgIHNob3dIZWFkZXJGaWx0ZXIgPSB0cnVlO1xyXG4gICAgc2hvd1BhZ2VJbmZvID0gdHJ1ZTtcclxuICAgIHNob3dQYWdlU2l6ZVNlbGVjdG9yID0gZmFsc2U7XHJcbiAgICBzaG93UGFnZXIgPSB0cnVlO1xyXG4gICAgc2hvd1Jvd0xpbmVzID0gdHJ1ZTtcclxuICAgIHNob3dTY3JvbGxiYXJzID0gdHJ1ZTtcclxuICAgIHN0YXRlU3RvcmFnZVR5cGU6ICdjdXN0b20nIHwgJ2xvY2FsU3RvcmFnZScgfCAnc2Vzc2lvblN0b3JhZ2UnID0gbnVsbDtcclxuICAgIHNob3dTZWFyY2hQYW5lbCA9IGZhbHNlO1xyXG4gICAgc29ydGluZ1R5cGUgPSAnbXVsdGlwbGUnO1xyXG4gICAgdXNlSWNvbnMgPSBmYWxzZTtcclxuXHJcbiAgICBwcml2YXRlIHN0cmluZ1NwZWNpZmljT3BlcmF0b3JzID0gWydjb250YWlucycsICdub3Rjb250YWlucycsICdzdGFydHN3aXRoJywgJ2VuZHN3aXRoJ107XHJcblxyXG4gICAgY2hpbGRFbnRpdHlMaXN0OiBBcnJheTxhbnk+ID0gW107XHJcbiAgICBpc01hc3RlckRldGFpbEVuYWJsZWQgPSBmYWxzZTtcclxuXHJcbiAgICAvLyBFdmVuIHRob3VnaCBpdCBpcyBhIGZ1bmN0aW9uIGl0IGlzIGJlaW5nIHVzZWQgYSBjb25maWd1cmF0aW9uIGZvciB0aGUgZ3JpZCdzIGNvbHVtblxyXG4gICAgY3VzdG9taXplQ29sdW1uczogKGNvbHVtbnM6IGR4RGF0YUdyaWRDb2x1bW5bXSkgPT4gYW55O1xyXG5cclxuICAgIC8vI2VuZHJlZ2lvbiBHcmlkIENvbmZpZ3VyYXRpb24gTWVtYmVyc1xyXG5cclxuICAgIC8vI3JlZ2lvbiBHcmlkIEV2ZW50c1xyXG4gICAgb25DZWxsQ2xpY2s6IChjZWxsT2JqZWN0OiBHcmlkQ2VsbENsaWNrQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICAvLyBvbkNlbGxQcmVwYXJlZDogKGNlbGxJbmZvOiBHcmlkQ2VsbFByZXBhcmVkQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvbkNvbnRlbnRSZWFkeTogKGdyaWRPYmplY3Q6IEdyaWRCYXNlQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICAvLyBvbkNvbnRleHRNZW51UHJlcGFyaW5nOiAoY2VsbE9iamVjdDogR3JpZENvbnRleHRNZW51UHJlcGFyaW5nQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvbkVkaXRpbmdTdGFydDogKHJvd0l0ZW06IEdyaWRFZGl0aW5nU3RhcnRBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uRWRpdG9yUHJlcGFyZWQ6IChyb3c6IEdyaWRFZGl0b3JQcmVwYXJlZEFyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgb25FZGl0b3JQcmVwYXJpbmc6IChyb3dPYmplY3Q6IEdyaWRFZGl0b3JQcmVwYXJpbmdBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uRXhwb3J0aW5nOiAoYXJnczogR3JpZEV4cG9ydGluZ0FyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgb25Jbml0TmV3Um93OiAoYXJnczogR3JpZEluaXROZXdSb3dBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uSW5pdGlhbGl6ZWQ6IChncmlkT2JqZWN0OiBHcmlkSW5pdGlhbGl6ZWRBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uUmVvcmRlcjogKGU6IGFueSkgPT4gdm9pZDtcclxuICAgIG9uUm93Q2xpY2s6IChyb3dJdGVtOiBHcmlkUm93Q2xpY2tBcmd1bWVudHMpID0+IGFueTtcclxuICAgIG9uUm93UmVtb3ZlZDogKHJvd0l0ZW06IEdyaWRSb3dSZW1vdmVkQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvblJvd1VwZGF0aW5nOiAocm93SXRlbTogR3JpZFJvd1VwZGF0aW5nQXJndW1lbnRzKSA9PiBhbnk7XHJcbiAgICBvblJvd1ZhbGlkYXRpbmc6IChyb3dJdGVtOiBHcmlkUm93VmFsaWRhdGluZ0FyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgb25TZWxlY3Rpb25DaGFuZ2VkOiAoc2VsZWN0ZWRJdGVtOiBHcmlkU2VsZWN0aW9uQ2hhbmdlZEFyZ3VtZW50cykgPT4gYW55O1xyXG4gICAgLy8jZW5kcmVnaW9uIEdyaWQgRXZlbnRzXHJcblxyXG4gICAgb25DdXN0b21CdXR0b25DbGljazogKGFyZ3M6IHsgZXZ0OiBNb3VzZUV2ZW50LCByb3dEYXRhOiBhbnksIGJ0blR5cGU6IHN0cmluZyB9KSA9PiBhbnk7XHJcbiAgICBvblVzZXJEZWZpbmVkQ3VzdG9tQnV0dG9uQ2xpY2s6IChhcmdzOiB7IGV2dDogTW91c2VFdmVudCwgcm93RGF0YTogYW55LCBhY3Rpb25JdGVtOiBhbnkgfSkgPT4gYW55O1xyXG5cclxuICAgIC8vIFRPRE86IFBhcmFtZXRlcml6ZWQgY29uc3RydWN0b3IgZm9yIGFzc2lnbmluZyB2YWx1ZXNcclxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gICAgcHVibGljIGFwcGx5Vmlld1RvR3JpZCh2aWV3SnNvbjogR3JpZFZpZXdJbmZvKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hcHBseVN0YXRlUHJvcGVydGllcyh2aWV3SnNvbj8uc3RhdGUuZ3JpZFN0YXRlKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25Db250ZXh0TWVudVByZXBhcmluZyhjZWxsT2JqZWN0OiBHcmlkQ29udGV4dE1lbnVQcmVwYXJpbmdBcmd1bWVudHMpOiB2b2lkIHtcclxuICAgICAgICBpZiAoIXRoaXMuZW5hYmxlQ29udGV4dE1lbnUpIHsgcmV0dXJuOyB9XHJcbiAgICAgICAgLy8gQWRkZWQgY29kZSB0byBkaXNhYmxlIHVuZ3JvdXBBbGwsIHdoZW4gbm8gY29sdW1uIGlzIGdyb3VwZWQuXHJcbiAgICAgICAgaWYgKGNlbGxPYmplY3QudGFyZ2V0ID09PSAnaGVhZGVyJyB8fCBjZWxsT2JqZWN0LnRhcmdldCA9PT0gJ2hlYWRlclBhbmVsJykge1xyXG4gICAgICAgICAgICBjb25zdCBncm91cENvdW50ID0gY2VsbE9iamVjdC5jb21wb25lbnQuY29sdW1uT3B0aW9uKCdncm91cEluZGV4OjAnKTtcclxuICAgICAgICAgICAgaWYgKCFncm91cENvdW50KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2VsbE9iamVjdC5pdGVtcykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNlbGxPYmplY3QuaXRlbXMuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXRlbS52YWx1ZSA9PT0gJ3VuZ3JvdXBBbGwnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLmRpc2FibGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNlbGxPYmplY3QuaXRlbXM/LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgaWNvbjogJycsXHJcbiAgICAgICAgICAgICAgICBvbkl0ZW1DbGljazogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmJlZ2luVXBkYXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY29sQ291bnQgPSB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5jb2x1bW5Db3VudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgY29sQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuY29sdW1uT3B0aW9uKGksICd2aXNpYmxlJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmNvbHVtbk9wdGlvbihpLCAnd2lkdGgnLCAnYXV0bycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdBdXRvIEZpdCcsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogJ2F1dG9GaXQnLFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjZWxsT2JqZWN0LnJvdyAmJiBjZWxsT2JqZWN0LnJvdy5yb3dUeXBlID09PSAnZ3JvdXAnKSB7XHJcbiAgICAgICAgICAgIGNlbGxPYmplY3QuaXRlbXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICB0ZXh0OiAnRXhwYW5kIEFsbCcsXHJcbiAgICAgICAgICAgICAgICBvbkl0ZW1DbGljazogKCkgPT4geyBjZWxsT2JqZWN0LmNvbXBvbmVudC5leHBhbmRBbGwodm9pZCAwKTsgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY2VsbE9iamVjdC5pdGVtcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdDb2xsYXBzZSBBbGwnLFxyXG4gICAgICAgICAgICAgICAgb25JdGVtQ2xpY2s6ICgpID0+IHsgY2VsbE9iamVjdC5jb21wb25lbnQuY29sbGFwc2VBbGwodm9pZCAwKTsgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjZWxsT2JqZWN0LnJvdyAmJiBjZWxsT2JqZWN0LnJvdy5yb3dUeXBlID09PSAnZGF0YScpIHtcclxuICAgICAgICAgICAgY29uc3QgaXNOdW1iZXJUeXBlID0gY2VsbE9iamVjdC5jb2x1bW4uZGF0YVR5cGUgPT09ICdudW1iZXInID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICBjZWxsT2JqZWN0Lml0ZW1zID0gW3tcclxuICAgICAgICAgICAgICAgIHZpc2libGU6IGlzTnVtYmVyVHlwZSxcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdTdW0nLFxyXG4gICAgICAgICAgICAgICAgb25JdGVtQ2xpY2s6ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZE9yUmVtb3ZlQWdncmVnYXRpb25TdW1tYXJ5KGNlbGxPYmplY3QsICdzdW0nKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdmlzaWJsZTogaXNOdW1iZXJUeXBlLFxyXG4gICAgICAgICAgICAgICAgdGV4dDogJ0F2ZycsXHJcbiAgICAgICAgICAgICAgICBvbkl0ZW1DbGljazogKCkgPT4geyB0aGlzLmFkZE9yUmVtb3ZlQWdncmVnYXRpb25TdW1tYXJ5KGNlbGxPYmplY3QsICdhdmcnKTsgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0ZXh0OiAnTWF4JyxcclxuICAgICAgICAgICAgICAgIG9uSXRlbUNsaWNrOiAoKSA9PiB7IHRoaXMuYWRkT3JSZW1vdmVBZ2dyZWdhdGlvblN1bW1hcnkoY2VsbE9iamVjdCwgJ21heCcpOyB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRleHQ6ICdNaW4nLFxyXG4gICAgICAgICAgICAgICAgb25JdGVtQ2xpY2s6ICgpID0+IHsgdGhpcy5hZGRPclJlbW92ZUFnZ3JlZ2F0aW9uU3VtbWFyeShjZWxsT2JqZWN0LCAnbWluJyk7IH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGV4dDogJ0NvdW50JyxcclxuICAgICAgICAgICAgICAgIG9uSXRlbUNsaWNrOiAoKSA9PiB7IHRoaXMuYWRkT3JSZW1vdmVBZ2dyZWdhdGlvblN1bW1hcnkoY2VsbE9iamVjdCwgJ2NvdW50Jyk7IH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgdGV4dDogJ1Jlc2V0JyxcclxuICAgICAgICAgICAgICAgIG9uSXRlbUNsaWNrOiAoKSA9PiB7IHRoaXMuYWRkT3JSZW1vdmVBZ2dyZWdhdGlvblN1bW1hcnkoY2VsbE9iamVjdCwgJ3Jlc2V0Jyk7IH1cclxuICAgICAgICAgICAgfV07XHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBwdWJsaWMgb25DZWxsUHJlcGFyZWQoY2VsbEluZm86IEdyaWRDZWxsUHJlcGFyZWRBcmd1bWVudHMpOiB2b2lkIHtcclxuICAgICAgICBpZiAoY2VsbEluZm8uY29sdW1uICYmIGNlbGxJbmZvLmNvbHVtbi5oYXNPd25Qcm9wZXJ0eSgnZGF0YUZpZWxkJykpIHtcclxuICAgICAgICAgICAgLy8gQWRkZWQgdHJ5IGNhdGNoIHNvIHRoYXQgaW4gY2FzZSBmb3JtYXR0aW5nIGxvZ2ljIGZhaWxzLCB1c2VyIGNhbiBzdGlsbCB1c2UgQ0dGIGVudGl0eS5cclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2VsbEZvcm1hdChjZWxsSW5mbywgdHJ1ZSk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdDZWxsIGZvcm1hdHRpbmcgYnJva2Ugd2hpbGUgZXhwb3J0aW5nLicsIGNlbGxJbmZvKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25Sb3dQcmVwYXJlZChhcmdzOiBHcmlkUm93UHJlcGFyZWRBcmd1bWVudHMpOiB2b2lkIHtcclxuICAgICAgICBpZiAoYXJncy5yb3dUeXBlID09PSByb3dUeXBlSW5mby5kYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0Q3VzdG9tQ29sdW1uRGF0YShhcmdzKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5mb3JtYXRSb3dzKGFyZ3Mucm93VHlwZSwgYXJncy5kYXRhLCBhcmdzLnJvd0VsZW1lbnQsIG51bGwpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY2VsbEZvcm1hdChjZWxsSW5mbzogR3JpZENlbGxQcmVwYXJlZEFyZ3VtZW50cywgaXNDZWxsUHJlcGFyZWQ6IGJvb2xlYW4sIGV4Y2VsR3JpZENlbGxJbmZvOiBHcmlkRXhwb3J0aW5nQXJndW1lbnRzID0gbnVsbCkge1xyXG4gICAgICAgIGlmIChjZWxsSW5mbykge1xyXG4gICAgICAgICAgICBsZXQgZGF0YVZhbHVlID0gbnVsbDtcclxuICAgICAgICAgICAgc3dpdGNoIChjZWxsSW5mby5yb3dUeXBlKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIHJvd1R5cGVJbmZvLnRvdGFsRm9vdGVyOlxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjZWxsSW5mby5zdW1tYXJ5SXRlbXM/Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YVZhbHVlID0gY2VsbEluZm8uc3VtbWFyeUl0ZW1zWzBdPy52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIHJvd1R5cGVJbmZvLmdyb3VwOlxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghY2VsbEluZm8uY29sdW1uLmhhc093blByb3BlcnR5KCdjb21tYW5kJylcclxuICAgICAgICAgICAgICAgICAgICAgICAgfHwgKGNlbGxJbmZvLmNvbHVtbi5oYXNPd25Qcm9wZXJ0eSgnY29tbWFuZCcpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAmJiAoY2VsbEluZm8uY29sdW1uIGFzIGFueSkuY29tbWFuZCAhPT0gJ2V4cGFuZCcpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjZWxsSW5mby50b3RhbEl0ZW0/LnN1bW1hcnlDZWxscykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmlsdGVySXRlbSA9IGNlbGxJbmZvLnRvdGFsSXRlbS5zdW1tYXJ5Q2VsbHMuZmlsdGVyKGl0ZW0gPT5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZWxsSW5mby5jb2x1bW4uZGF0YUZpZWxkID09PSBpdGVtWzBdPy5jb2x1bW4pWzBdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZpbHRlckl0ZW0/Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFWYWx1ZSA9IGZpbHRlckl0ZW1bMF0udmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGFWYWx1ZSA9IGNlbGxJbmZvLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSByb3dUeXBlSW5mby5kYXRhOlxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGFWYWx1ZSA9IGNlbGxJbmZvLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmFsbG93ZWRSb3dUeXBlcyhjZWxsSW5mby5yb3dUeXBlKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLmxpc3RHcm91cGVkQ29sdW1ucy5sZW5ndGggJiYgY2VsbEluZm8uY29tcG9uZW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0R3JvdXBlZENvbHVtbnMgPSB0aGlzLmdldEdyb3VwZWRDb2x1bW5MaXN0KGNlbGxJbmZvLmNvbXBvbmVudCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByZXBhcmVGb3JtYXR0aW5nSW5mbyhjZWxsSW5mbywgaXNDZWxsUHJlcGFyZWQsIGV4Y2VsR3JpZENlbGxJbmZvLCBkYXRhVmFsdWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIC8vIGVsc2UgaWYgKHR5cGVvZiAoZGF0YVZhbHVlKSA9PT0gJ2Jvb2xlYW4nICYmIGNlbGxJbmZvLnJvd1R5cGUgPT09IHJvd1R5cGVJbmZvLmRhdGEpIHtcclxuICAgICAgICAgICAgLy8gICAgIHRoaXMucHJlcGFyZUZvcm1hdHRpbmdJbmZvKGNlbGxJbmZvLCBpc0NlbGxQcmVwYXJlZCwgZXhjZWxHcmlkQ2VsbEluZm8sIGRhdGFWYWx1ZSk7XHJcbiAgICAgICAgICAgIC8vIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhbGxvd2VkUm93VHlwZXModHlwZTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2Ugcm93VHlwZUluZm8uZGF0YTpcclxuICAgICAgICAgICAgY2FzZSByb3dUeXBlSW5mby5ncm91cDpcclxuICAgICAgICAgICAgY2FzZSByb3dUeXBlSW5mby50b3RhbEZvb3RlcjpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcmVwYXJlRm9ybWF0dGluZ0luZm8oY2VsbEluZm86IEdyaWRDZWxsUHJlcGFyZWRBcmd1bWVudHMsIGlzQ2VsbFByZXBhcmVkOiBib29sZWFuLCBleGNlbEdyaWRDZWxsSW5mbzogR3JpZEV4cG9ydGluZ0FyZ3VtZW50cywgZGF0YVZhbHVlOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCByb3dUeXBlRmlsdGVyRm9yRm9ybWF0dGluZyA9ICdyb3cnO1xyXG4gICAgICAgIGNvbnN0IGNvbHVtbkZvcm1hdENvbGxlY3Rpb246IENvbHVtbkZvcm1hdFtdID1cclxuICAgICAgICAgICAgSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGdldFN0b3JhZ2VLZXkodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UsIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmZvcm1hdERhdGFdLCB0aGlzLmlzTWFzdGVyR3JpZCkpKSB8fCBbXTtcclxuICAgICAgICBjb25zdCBiYXNlRm9ybWF0Q29sbGVjdGlvbjogQ29sdW1uRm9ybWF0W10gPSB0aGlzLmlzTWFzdGVyR3JpZCA/IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5kaWN0aW9uYXJ5Rm9ybWF0RGF0YV0pKSB8fCBbXSA6IFtdO1xyXG4gICAgICAgIGNvbnN0IGNvbmRpdGlvbmFsRm9ybWF0Q29sbGVjdGlvbjogQ29sdW1uRm9ybWF0W10gPVxyXG4gICAgICAgICAgICBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oZ2V0U3RvcmFnZUtleSh0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZSwgQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuY29uZGl0aW9uYWxGb3JtYXR0aW5nXSwgdGhpcy5pc01hc3RlckdyaWQpKSkgfHwgW107XHJcbiAgICAgICAgY29uc3QgY29uZGl0aW9uRmlsdGVySXRlbXNGb3JDb2x1bW5zID0gY29uZGl0aW9uYWxGb3JtYXRDb2xsZWN0aW9uLmZpbHRlcihhcnJJdGVtID0+IGFyckl0ZW0uZGF0YUZpZWxkID09PSBjZWxsSW5mby5jb2x1bW4uZGF0YUZpZWxkICYmIGFyckl0ZW0uYXBwbHlUeXBlICE9PSByb3dUeXBlRmlsdGVyRm9yRm9ybWF0dGluZyk7XHJcbiAgICAgICAgbGV0IGNvbmRpdGlvbmFsRm9ybWF0dGluZ1JlcXVpcmVkID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3QgZm9ybWF0RGF0YTogQ29sdW1uRm9ybWF0ID0gY29sdW1uRm9ybWF0Q29sbGVjdGlvbi5maW5kKGFyckl0ZW0gPT4gYXJySXRlbS5kYXRhRmllbGQgPT09IGNlbGxJbmZvLmNvbHVtbi5kYXRhRmllbGQpIHx8XHJcbiAgICAgICAgICAgIGJhc2VGb3JtYXRDb2xsZWN0aW9uLmZpbmQoYXJySXRlbSA9PiBhcnJJdGVtLmRhdGFGaWVsZCA9PT0gY2VsbEluZm8uY29sdW1uLmRhdGFGaWVsZCk7XHJcbiAgICAgICAgbGV0IGFwcGx5QmFzaWNGb3JtYXR0aW5nID0gdHJ1ZTtcclxuICAgICAgICBjb25kaXRpb25GaWx0ZXJJdGVtc0ZvckNvbHVtbnMuZm9yRWFjaChmaWx0ZXJJdGVtID0+IHtcclxuICAgICAgICAgICAgaWYgKGZpbHRlckl0ZW0uY29uZGl0aW9uICYmIGNlbGxJbmZvLnJvd1R5cGUgPT09ICdkYXRhJykge1xyXG4gICAgICAgICAgICAgICAgY29uZGl0aW9uYWxGb3JtYXR0aW5nUmVxdWlyZWQgPSB0aGlzLmNoZWNrQ29uZGl0aW9uU2F0aXNmaWVkKGZpbHRlckl0ZW0sIGNlbGxJbmZvLmRhdGEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChjb25kaXRpb25hbEZvcm1hdHRpbmdSZXF1aXJlZCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IF9mb3JtYXR0aW5nRGF0YSA9IHsgLi4uZm9ybWF0RGF0YSB9O1xyXG4gICAgICAgICAgICAgICAgaWYgKF9mb3JtYXR0aW5nRGF0YT8uZGF0YUZpZWxkID09PSBmaWx0ZXJJdGVtLmRhdGFGaWVsZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvciAoY29uc3Qga2V5IGluIF9mb3JtYXR0aW5nRGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoX2Zvcm1hdHRpbmdEYXRhLmhhc093blByb3BlcnR5KGtleSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIF9mb3JtYXR0aW5nRGF0YS50ZXh0Q29sb3IgPSBmaWx0ZXJJdGVtLnRleHRDb2xvciB8fCBfZm9ybWF0dGluZ0RhdGEudGV4dENvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2Zvcm1hdHRpbmdEYXRhLmJhY2tncm91bmRDb2xvciA9IGZpbHRlckl0ZW0uYmFja2dyb3VuZENvbG9yIHx8IF9mb3JtYXR0aW5nRGF0YS5iYWNrZ3JvdW5kQ29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyByZXNldCB0aGVuIGFwcGx5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBfZm9ybWF0dGluZ0RhdGEuY3NzQ2xhc3MgPSBfZm9ybWF0dGluZ0RhdGEuY3NzQ2xhc3M/LnJlcGxhY2UoJ2JvbGQnLCAnJyk/LnJlcGxhY2UoJ3VuZGVybGluZScsICcnKT8ucmVwbGFjZSgnaXRhbGljJywgJycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgX2Zvcm1hdHRpbmdEYXRhLmNzc0NsYXNzID0gZmlsdGVySXRlbS5jc3NDbGFzcz8uY29uY2F0KCcgJyk/LmNvbmNhdChfZm9ybWF0dGluZ0RhdGEuY3NzQ2xhc3MpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBfZm9ybWF0dGluZ0RhdGEgPSB7IC4uLmZpbHRlckl0ZW0gfTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMuYXBwbHlGb3JtYXR0aW5nKGRhdGFWYWx1ZSwgX2Zvcm1hdHRpbmdEYXRhLCBpc0NlbGxQcmVwYXJlZCwgY2VsbEluZm8sIGV4Y2VsR3JpZENlbGxJbmZvKTtcclxuICAgICAgICAgICAgICAgIGFwcGx5QmFzaWNGb3JtYXR0aW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGZvcm1hdERhdGEgJiYgYXBwbHlCYXNpY0Zvcm1hdHRpbmcpIHtcclxuICAgICAgICAgICAgdGhpcy5hcHBseUZvcm1hdHRpbmcoZGF0YVZhbHVlLCBmb3JtYXREYXRhLCBpc0NlbGxQcmVwYXJlZCwgY2VsbEluZm8sIGV4Y2VsR3JpZENlbGxJbmZvKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGNlbGxJbmZvLnZhbHVlICE9PSBjZWxsSW5mby50ZXh0KSB7IC8vICFJTVBPUlRBTlQgQ2hlY2sgd2h5IHdlIG5lZWQgdG8gdXNlIHRoaXMgY29uZGl0aW9uLiBTaW5jZSBpdCB3b3JrZWQgaW4gcHJldmlvdXMgdmVyc2lvbnMuXHJcbiAgICAgICAgICAgIGNlbGxJbmZvLnRleHQgPSBjZWxsSW5mby52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjaGVja0NvbmRpdGlvblNhdGlzZmllZChjb25kaXRpb25JbmZvOiBDb2x1bW5Gb3JtYXQsIHJvd0RhdGE6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBjb25kaXRpb25QYXNzZWQgPSBmYWxzZTtcclxuICAgICAgICBjb25zdCB1cGRhdGVkVmFsdWUgPSB0aGlzLnN0YXJ0VmFsaWRhdGlvbihKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGNvbmRpdGlvbkluZm8uY29uZGl0aW9uKSksIHJvd0RhdGEpO1xyXG4gICAgICAgIGlmIChKU09OLnN0cmluZ2lmeShjb25kaXRpb25JbmZvLmNvbmRpdGlvbikgIT09IEpTT04uc3RyaW5naWZ5KHVwZGF0ZWRWYWx1ZSkpIHtcclxuICAgICAgICAgICAgY29uZGl0aW9uUGFzc2VkID0gdGhpcy50cmFuc2Zvcm1BbmRSdW5WYWxpZGF0aW9uKHVwZGF0ZWRWYWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBjb25kaXRpb25QYXNzZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzdGFydFZhbGlkYXRpb24odmFsdWUsIHJvd0RhdGEpIHtcclxuICAgICAgICBpZiAodmFsdWUgJiYgQXJyYXkuaXNBcnJheSh2YWx1ZVswXSkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHZhbHVlLm1hcCgoaXRlbTogYW55W10gfCBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBBcnJheS5pc0FycmF5KGl0ZW1bMF0pID8gdGhpcy5zdGFydFZhbGlkYXRpb24oaXRlbSwgcm93RGF0YSkgOiB0aGlzLnZhbGlkYXRlKGl0ZW0sIHJvd0RhdGEpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsaWRhdGUodmFsdWUsIHJvd0RhdGEpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdmFsaWRhdGUoaXRlbSwgcm93RGF0YSk6IGFueSB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBpdGVtID09PSAnc3RyaW5nJyAmJiB0aGlzLmlzUmVzZXJ2ZWRLZXlXb3JkKGl0ZW0pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICBsZXQgX2RhdGFUeXBlID0gaXRlbVsyXSA/IHR5cGVvZiBpdGVtWzJdIDogJ3N0cmluZyc7XHJcbiAgICAgICAgaWYgKGl0ZW1bMl0gPT09IDAgfHwgaXRlbVsyXSA9PT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgX2RhdGFUeXBlID0gdHlwZW9mIGl0ZW1bMl07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHN3aXRjaCAoX2RhdGFUeXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ251bWJlcic6XHJcbiAgICAgICAgICAgICAgICBpdGVtID0gdGhpcy52YWxpZGF0ZU51bWJlclR5cGUoaXRlbSwgcm93RGF0YSk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnYm9vbGVhbic6XHJcbiAgICAgICAgICAgICAgICBpdGVtID0gdGhpcy52YWxpZGF0ZUJvb2xlYW5UeXBlKGl0ZW0sIHJvd0RhdGEpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ3N0cmluZyc6XHJcbiAgICAgICAgICAgICAgICBpZiAoaXRlbVsyXSAmJiAhaXNOYU4obmV3IERhdGUoaXRlbVsyXSkuZ2V0VGltZSgpKSkgeyAvLyBjb25kaXRpb24gdG8gY2hlY2sgd2hldGhlciBzdHJpbmcgaXMgZGF0ZSB0eXBlXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuc3RyaW5nU3BlY2lmaWNPcGVyYXRvcnMuaW5kZXhPZihpdGVtWzFdKSA+IC0xKSB7IC8vIGNvbmRpdGlvbiBzaW5jZSBuZXcgZGF0ZSB3aWxsIHJldHVybiB2YWx1ZXMgZm9yIHN0cmluZ3MgbGlrZSAnOTknXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSB0aGlzLnZhbGlkYXRlU3RyaW5nVHlwZShpdGVtLCByb3dEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtID0gdGhpcy52YWxpZGF0ZURhdGVUeXBlKGl0ZW0sIHJvd0RhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IHRoaXMudmFsaWRhdGVTdHJpbmdUeXBlKGl0ZW0sIHJvd0RhdGEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ29iamVjdCc6XHJcbiAgICAgICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShpdGVtWzJdKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgaXRlbVsyXVswXSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXRlbSA9IHRoaXMudmFsaWRhdGVEYXRlVHlwZShpdGVtLCByb3dEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVvZiBpdGVtWzJdWzBdID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpdGVtID0gdGhpcy52YWxpZGF0ZU51bWJlclR5cGUoaXRlbSwgcm93RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNSZXNlcnZlZEtleVdvcmQodmFsdWU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHN3aXRjaCAodmFsdWUpIHtcclxuICAgICAgICAgICAgY2FzZSAnb3InOlxyXG4gICAgICAgICAgICBjYXNlICdhbmQnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdmFsaWRhdGVOdW1iZXJUeXBlKGl0ZW06IEFycmF5PGFueT4sIHJvd0RhdGE6IGFueSk6IGFueSB7XHJcbiAgICAgICAgY29uc3QgZmlsdGVyVmFsMSA9IGl0ZW1bMl1bMF0gfHwgaXRlbVsyXTsgY29uc3QgZmlsdGVyVmFsMiA9IGl0ZW1bMl1bMV0gfHwgaXRlbVsyXTtcclxuICAgICAgICBjb25zdCBpc1Bhc3NlZCA9IHRoaXMudmFsaWRhdGVPcGVyYXRvckFuZE9wZXJhbmRzKGl0ZW1bMV0sIHJvd0RhdGFbaXRlbVswXV0sIGZpbHRlclZhbDEsIGZpbHRlclZhbDIpO1xyXG4gICAgICAgIGlmICh0eXBlb2YgaXNQYXNzZWQgPT09ICdib29sZWFuJykgeyBpdGVtWzJdID0gaXNQYXNzZWQ7IH1cclxuICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHZhbGlkYXRlQm9vbGVhblR5cGUoaXRlbTogQXJyYXk8YW55Piwgcm93RGF0YTogYW55KTogYW55IHtcclxuICAgICAgICBjb25zdCBpc1Bhc3NlZCA9IHRoaXMudmFsaWRhdGVPcGVyYXRvckFuZE9wZXJhbmRzKGl0ZW1bMV0sIHJvd0RhdGFbaXRlbVswXV0sIGl0ZW1bMl0pO1xyXG4gICAgICAgIGlmICh0eXBlb2YgaXNQYXNzZWQgPT09ICdib29sZWFuJykge1xyXG4gICAgICAgICAgICBpdGVtWzJdID0gaXNQYXNzZWQudG9TdHJpbmcoKTsgIC8vIHRoaXMgY29udmVyc2lvbihvbmx5IGZvciBib29sZWFuKSB3aWxsIGJlIHVzZWQgaW4gcGFyZW50IGZ1bmN0aW9ucy5cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB2YWxpZGF0ZVN0cmluZ1R5cGUoaXRlbTogQXJyYXk8YW55Piwgcm93RGF0YTogYW55KSB7XHJcbiAgICAgICAgaWYgKGl0ZW1bMl0gIT09IG51bGwpIHtcclxuICAgICAgICAgICAgaXRlbVsyXSA9IGl0ZW1bMl0/LnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBkYXRhRmllbGRWYWx1ZUluUm93ID0gcm93RGF0YVtpdGVtWzBdXTtcclxuICAgICAgICBpZiAodHlwZW9mIHJvd0RhdGFbaXRlbVswXV0gPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGRhdGFGaWVsZFZhbHVlSW5Sb3cgPSByb3dEYXRhW2l0ZW1bMF1dPy50b0xvd2VyQ2FzZSgpIHx8IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGlzUGFzc2VkID0gdGhpcy52YWxpZGF0ZU9wZXJhdG9yQW5kT3BlcmFuZHMoaXRlbVsxXSwgZGF0YUZpZWxkVmFsdWVJblJvdywgaXRlbVsyXSA9PT0gdW5kZWZpbmVkID8gJycgOiBpdGVtWzJdKTtcclxuICAgICAgICBpZiAodHlwZW9mIGlzUGFzc2VkID09PSAnYm9vbGVhbicpIHsgaXRlbVsyXSA9IGlzUGFzc2VkOyB9XHJcbiAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB2YWxpZGF0ZURhdGVUeXBlKGl0ZW06IEFycmF5PGFueT4sIHJvd0RhdGE6IGFueSk6IGFueSB7XHJcbiAgICAgICAgbGV0IGZpbHRlclZhbDEgPSBpdGVtWzJdWzBdLmxlbmd0aCA+IDYgPyBpdGVtWzJdWzBdIDogaXRlbVsyXTsgLy8gY29uZGl0aW9ucyB0byBjaGVjayBzdHJpbmcgaXMgZGF0ZSBvbmx5IGFuZCA2IGlzIHNpbmNlIHdlIGhhdmUgbW0vZGQveXl5eSBmb3JtYXRcclxuICAgICAgICBsZXQgZmlsdGVyVmFsMiA9IGl0ZW1bMl1bMV0ubGVuZ3RoID4gNiA/IGl0ZW1bMl1bMV0gOiBpdGVtWzJdO1xyXG4gICAgICAgIGZpbHRlclZhbDEgPSB0aGlzLmNvbnZlcnREYXRlRm9yQ29tcGFyaXNvbihmaWx0ZXJWYWwxKS5nZXRUaW1lKCk7XHJcbiAgICAgICAgZmlsdGVyVmFsMiA9IHRoaXMuY29udmVydERhdGVGb3JDb21wYXJpc29uKGZpbHRlclZhbDIpLmdldFRpbWUoKTtcclxuICAgICAgICBsZXQgcm93Q29sRGF0YTogYW55ID0gdGhpcy5jb252ZXJ0RGF0ZUZvckNvbXBhcmlzb24ocm93RGF0YVtpdGVtWzBdXSkuZ2V0VGltZSgpO1xyXG4gICAgICAgIHJvd0NvbERhdGEgPSBpc05hTihyb3dDb2xEYXRhKSA/ICcnIDogcm93Q29sRGF0YTtcclxuICAgICAgICBjb25zdCBpc1Bhc3NlZCA9IHRoaXMudmFsaWRhdGVPcGVyYXRvckFuZE9wZXJhbmRzKGl0ZW1bMV0sIHJvd0NvbERhdGEsIGZpbHRlclZhbDEsIGZpbHRlclZhbDIpO1xyXG4gICAgICAgIGlmICh0eXBlb2YgaXNQYXNzZWQgPT09ICdib29sZWFuJykge1xyXG4gICAgICAgICAgICBpdGVtWzJdID0gaXNQYXNzZWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY29udmVydERhdGVGb3JDb21wYXJpc29uKGRhdGVWYWx1ZTogc3RyaW5nKTogRGF0ZSB7XHJcbiAgICAgICAgY29uc3QgZGF0ZVRpbWUgPSBuZXcgRGF0ZShkYXRlVmFsdWUpO1xyXG4gICAgICAgIGNvbnN0IGZvcm1hdHRlZERhdGUgPSAoZGF0ZVRpbWUuZ2V0TW9udGgoKSArIDEpICsgJy0nICsgZGF0ZVRpbWUuZ2V0RGF0ZSgpICsgJy0nICsgZGF0ZVRpbWUuZ2V0RnVsbFllYXIoKTtcclxuICAgICAgICByZXR1cm4gbmV3IERhdGUoZm9ybWF0dGVkRGF0ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB2YWxpZGF0ZU9wZXJhdG9yQW5kT3BlcmFuZHMob3BlcmF0b3I6IHN0cmluZywgcm93Q29sdW1uRGF0YTogYW55LCBmaWx0ZXJWYWx1ZTogc3RyaW5nIHwgbnVtYmVyIHwgYm9vbGVhbiwgZmlsdGVyVmFsdWUyPzogc3RyaW5nIHwgbnVtYmVyIHwgYm9vbGVhbik6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0eXBlb2YgKHJvd0NvbHVtbkRhdGEpICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICBzd2l0Y2ggKG9wZXJhdG9yKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdjb250YWlucyc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGE/LmluZGV4T2YoZmlsdGVyVmFsdWUpID4gLTEgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdub3Rjb250YWlucyc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGE/LmluZGV4T2YoZmlsdGVyVmFsdWUpID09PSAtMSA/IGZhbHNlIDogdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ3N0YXJ0c3dpdGgnOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhPy5zdGFydHNXaXRoKGZpbHRlclZhbHVlKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ2VuZHN3aXRoJzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YT8uZW5kc1dpdGgoZmlsdGVyVmFsdWUpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnaXNibGFuayc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGEgPT09IG51bGwgfHwgcm93Q29sdW1uRGF0YSA9PT0gJycgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdpc25vdGJsYW5rJzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YSAhPT0gbnVsbCAmJiByb3dDb2x1bW5EYXRhICE9PSAnJyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ2JldHdlZW4nOlxyXG4gICAgICAgICAgICAgICAgICAgIC8vIHdlIGhhdmUgYXNzdW1wdGlvbiB0aGF0IGZpc3QgdmFsdWUgaXMgbWluIGFuZCBzZWNvbmQgaXMgbWF4XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGEgPiBmaWx0ZXJWYWx1ZSAmJiByb3dDb2x1bW5EYXRhIDwgZmlsdGVyVmFsdWUyID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnPSc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvd0NvbHVtbkRhdGEgPT09IGZpbHRlclZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnPD4nOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhICE9PSBmaWx0ZXJWYWx1ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJzwnOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhIDwgZmlsdGVyVmFsdWUgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjYXNlICc+JzpcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcm93Q29sdW1uRGF0YSA+IGZpbHRlclZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnPj0nOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhID49IGZpbHRlclZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnPD0nOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByb3dDb2x1bW5EYXRhIDw9IGZpbHRlclZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB2b2lkIDA7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB0cmFuc2Zvcm1BbmRSdW5WYWxpZGF0aW9uKHZhbHVlOiBhbnlbXSkge1xyXG4gICAgICAgIGxldCBldmFsU3RyaW5nID0gJyc7XHJcbiAgICAgICAgdmFsdWUuZm9yRWFjaChpdGVtID0+IHtcclxuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkoaXRlbSkgJiYgIUFycmF5LmlzQXJyYXkoaXRlbVsyXSkpIHtcclxuICAgICAgICAgICAgICAgIGV2YWxTdHJpbmcgKz0gaXRlbVsyXTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgaXRlbSA9PT0gJ3N0cmluZycgfHwgdHlwZW9mIGl0ZW0gPT09ICdib29sZWFuJykge1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnYW5kJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZhbFN0cmluZyArPSAnICYmICc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ29yJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXZhbFN0cmluZyArPSAnIHx8ICc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgdHJ1ZTpcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIGZhbHNlOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBldmFsU3RyaW5nICs9IGAgJHtpdGVtfSBgO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICd0cnVlJzpcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdmYWxzZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGV2YWxTdHJpbmcgKz0gYCAke0pTT04ucGFyc2UoaXRlbSl9IGA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkoaXRlbSkpIHtcclxuICAgICAgICAgICAgICAgIGV2YWxTdHJpbmcgPSB0aGlzLnRyYW5zZm9ybUFuZFJ1blZhbGlkYXRpb24oaXRlbSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG5vLWV2YWxcclxuICAgICAgICAgICAgcmV0dXJuIGV2YWwoZXZhbFN0cmluZyk7XHJcbiAgICAgICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGUpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0R3JvdXBlZENvbHVtbkxpc3QoZ3JpZENvbXBvbmVudDogYW55KTogc3RyaW5nW10ge1xyXG4gICAgICAgIGNvbnN0IGxpc3RHcm91cGVkQ29sdW1ucyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IGNvdW50ID0gZ3JpZENvbXBvbmVudC5vcHRpb24oJ2NvbHVtbnMnKS5sZW5ndGg7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBjb3VudDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGdyb3VwZWRDb2x1bSA9IGdyaWRDb21wb25lbnQuY29sdW1uT3B0aW9uKCdncm91cEluZGV4OicgKyBpLnRvU3RyaW5nKCkpO1xyXG4gICAgICAgICAgICBpZiAoZ3JvdXBlZENvbHVtKSB7XHJcbiAgICAgICAgICAgICAgICBsaXN0R3JvdXBlZENvbHVtbnMucHVzaChncm91cGVkQ29sdW0uZGF0YUZpZWxkLnRvTG93ZXJDYXNlKCkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBsaXN0R3JvdXBlZENvbHVtbnM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhcHBseUZvcm1hdHRpbmcoZGF0YVZhbHVlOiBzdHJpbmcgfCBudW1iZXIgfCBEYXRlLCBmb3JtYXREYXRhOiBDb2x1bW5Gb3JtYXQsIGlzQ2VsbFByZXBhcmVkOiBib29sZWFuLCBjZWxsSW5mbzogR3JpZENlbGxQcmVwYXJlZEFyZ3VtZW50cywgZXhjZWxHcmlkQ2VsbEluZm86IEdyaWRFeHBvcnRpbmdBcmd1bWVudHMpOiB2b2lkIHtcclxuICAgICAgICBmb3IgKGNvbnN0IF9rZXkgaW4gZm9ybWF0RGF0YSkge1xyXG4gICAgICAgICAgICBpZiAoZm9ybWF0RGF0YS5oYXNPd25Qcm9wZXJ0eShfa2V5KSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qga2V5VmFsdWUgPSBmb3JtYXREYXRhW19rZXldIHx8ICcnO1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoIChfa2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnY3NzQ2xhc3MnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNDZWxsUHJlcGFyZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGNsYXNzQXJyYXkgPSBgJHtrZXlWYWx1ZX1gLnNwbGl0KCcgJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGFzc0FycmF5LmZvckVhY2goaXRlbSA9PiB7IGlmIChpdGVtLnRyaW0oKSkgeyBjZWxsSW5mby5jZWxsRWxlbWVudC5jbGFzc0xpc3QuYWRkKGl0ZW0udHJpbSgpKTsgfSB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChrZXlWYWx1ZT8ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5leGNlbENlbGxGb3JtYXQoYCR7a2V5VmFsdWV9YCwgZXhjZWxHcmlkQ2VsbEluZm8sIGRhdGFWYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnZm9ybWF0JzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGFzc2lnbmVlT2JqZWN0OiBzdHJpbmcgfCBudW1iZXIgfCBEYXRlID0gJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc0NlbGxQcmVwYXJlZCAmJiBmb3JtYXREYXRhLmRhdGFUeXBlICE9PSAnYm9vbGVhbicpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFzc2lnbmVlT2JqZWN0ID0gZGF0YVZhbHVlOyAvLyBmaXJzdCByZXNldCB0aGFuIGFwcGx5IGZvcm1hdC5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGtleVZhbHVlICE9PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3dpdGNoIChrZXlWYWx1ZS50eXBlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAnY3VycmVuY3knOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhc3NpZ25lZU9iamVjdCA9IHRoaXMucHJlcGFyZVVTREZvcm1hdChrZXlWYWx1ZS5wcmVjaXNpb24sIGRhdGFWYWx1ZSBhcyBudW1iZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdwZXJjZW50JzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzaWduZWVPYmplY3QgPSB0aGlzLnByZXBhcmVQZXJjZW50Rm9ybWF0KGtleVZhbHVlLnByZWNpc2lvbiwgZGF0YVZhbHVlIGFzIG51bWJlcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgJ2NvbW1hJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzaWduZWVPYmplY3QgPSB0aGlzLnByZXBhcmVDb21tYUZvcm1hdChrZXlWYWx1ZS5wcmVjaXNpb24sIGRhdGFWYWx1ZSBhcyBudW1iZXIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjYXNlICdmaXhlZFBvaW50JzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYXNzaWduZWVPYmplY3QgPSAoZGF0YVZhbHVlIGFzIG51bWJlcikudG9GaXhlZChrZXlWYWx1ZS5wcmVjaXNpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChmb3JtYXREYXRhW19rZXldID09PSAnc2hvcnREYXRlJykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGF0ZSA9IG5ldyBEYXRlKGRhdGFWYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhc3NpZ25lZU9iamVjdCA9IChkYXRlLmdldE1vbnRoKCkgKyAxKSArICcvJyArIGRhdGUuZ2V0RGF0ZSgpICsgJy8nICsgZGF0ZS5nZXRGdWxsWWVhcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpc0NlbGxQcmVwYXJlZCAmJiBhc3NpZ25lZU9iamVjdCkgeyAgLy8gYWRkZWQgXCJBTkRcIiBjb25kaXRpb24gdG8gcHJldmVudCBvdmVycmlkaW5nIHZhbHVlIGZvciBib29sZWFuIGNvbHVtblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2VsbEluZm8uY2VsbEVsZW1lbnQuaW5uZXJUZXh0ID0gYXNzaWduZWVPYmplY3QgYXMgc3RyaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGV4Y2VsR3JpZENlbGxJbmZvKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGNlbEdyaWRDZWxsSW5mby52YWx1ZSA9IGFzc2lnbmVlT2JqZWN0IHx8IGRhdGFWYWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdhbGlnbm1lbnQnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXNDZWxsUHJlcGFyZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNlbGxJbmZvLmNlbGxFbGVtZW50LnN0eWxlLnRleHRBbGlnbiA9IGtleVZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhjZWxHcmlkQ2VsbEluZm8uYWxpZ25tZW50ID0geyBob3Jpem9udGFsOiBrZXlWYWx1ZSB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjZWxsSW5mby5yb3dUeXBlID09PSAndG90YWxGb290ZXInICYmIGNlbGxJbmZvLmNlbGxFbGVtZW50Py5jaGlsZE5vZGVzPy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIChjZWxsSW5mby5jZWxsRWxlbWVudC5jaGlsZE5vZGVzWzBdIGFzIGFueSkuc3R5bGUudGV4dEFsaWduID0ga2V5VmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAndGV4dENvbG9yJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzQ2VsbFByZXBhcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZWxsSW5mby5jZWxsRWxlbWVudC5zdHlsZS5jb2xvciA9IGtleVZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGV4Y2VsR3JpZENlbGxJbmZvKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGNlbEdyaWRDZWxsSW5mby5mb250ID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC4uLmV4Y2VsR3JpZENlbGxJbmZvLmZvbnQsIC4uLnsgY29sb3I6IHsgYXJnYjoga2V5VmFsdWU/LnJlcGxhY2UoJyMnLCAnZmYnKSB9IH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnYmFja2dyb3VuZENvbG9yJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGlzQ2VsbFByZXBhcmVkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjZWxsSW5mby5jZWxsRWxlbWVudC5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSBrZXlWYWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChleGNlbEdyaWRDZWxsSW5mbykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhjZWxHcmlkQ2VsbEluZm8uZmlsbCA9IHsgdHlwZTogJ3BhdHRlcm4nLCBwYXR0ZXJuOiAnc29saWQnLCBmZ0NvbG9yOiB7IGFyZ2I6IGtleVZhbHVlPy5yZXBsYWNlKCcjJywgJ2ZmJykgfSB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGNhc2UgJ2ZpeGVkJzogLy8gIUltcG9ydGFudCB0aGlzIGNhc2UgaGFzIGJlZW4gaGFuZGxlZCBpbiBkYXRhYnJvd3Nlci5jb21wb25lbnQudHNcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGV4Y2VsQ2VsbEZvcm1hdChjbGFzc0xpc3Q6IHN0cmluZywgZXhjZWxHcmlkQ2VsbEluZm86IGFueSwgdmFsOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBsZXQgY2xhc3NMaXN0RGF0YTogc3RyaW5nW10gPSBjbGFzc0xpc3Quc3BsaXQoJyAnKTtcclxuICAgICAgICBsZXQgZGVjaW1hbFBsYWNlc0RhdGE6IHN0cmluZztcclxuICAgICAgICBjbGFzc0xpc3REYXRhID0gY2xhc3NMaXN0RGF0YS5maWx0ZXIoKGl0ZW06IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICBpZiAoIWl0ZW0udG9Mb3dlckNhc2UoKS5pbmNsdWRlcygnZGVjaW1hbGFkZGVkJykpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZGVjaW1hbFBsYWNlc0RhdGEgPSBpdGVtO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKGRlY2ltYWxQbGFjZXNEYXRhKSB7XHJcbiAgICAgICAgICAgIGNsYXNzTGlzdERhdGEudW5zaGlmdChkZWNpbWFsUGxhY2VzRGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvciAoY29uc3QgZm9ybWF0Q2xhc3Mgb2YgY2xhc3NMaXN0RGF0YSkge1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGZvcm1hdENsYXNzLnRvTG93ZXJDYXNlKCkpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgJ2JvbGQnOlxyXG4gICAgICAgICAgICAgICAgICAgIGV4Y2VsR3JpZENlbGxJbmZvLmZvbnQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC4uLmV4Y2VsR3JpZENlbGxJbmZvLmZvbnQsIC4uLnsgYm9sZDogdHJ1ZSB9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgJ3VuZGVybGluZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgZXhjZWxHcmlkQ2VsbEluZm8uZm9udCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLi4uZXhjZWxHcmlkQ2VsbEluZm8uZm9udCwgLi4ueyB1bmRlcmxpbmU6IHRydWUgfVxyXG4gICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlICdpdGFsaWMnOlxyXG4gICAgICAgICAgICAgICAgICAgIGV4Y2VsR3JpZENlbGxJbmZvLmZvbnQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC4uLmV4Y2VsR3JpZENlbGxJbmZvLmZvbnQsIC4uLnsgaXRhbGljOiB0cnVlIH1cclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChmb3JtYXRDbGFzcy50b0xvd2VyQ2FzZSgpLmluY2x1ZGVzKCdkZWNpbWFsYWRkZWQnKSkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcHJlY2lzaW9uRGF0YSA9IGZvcm1hdENsYXNzLnNwbGl0KCctJyk7XHJcbiAgICAgICAgICAgICAgICBpZiAocHJlY2lzaW9uRGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXhjZWxHcmlkQ2VsbEluZm8udmFsdWUgPSB2YWw/LnRvRml4ZWQoTnVtYmVyKHByZWNpc2lvbkRhdGFbMV0pKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHByZXBhcmVVU0RGb3JtYXQocHJlY2lzaW9uOiBudW1iZXIsIHZhbHVlOiBudW1iZXIpOiBzdHJpbmcge1xyXG4gICAgICAgIHByZWNpc2lvbiA9IHByZWNpc2lvbiA9PT0gMCA/IDAgOiBwcmVjaXNpb24gfHwgMjsgLy8gRGVmYXVsdCB0byAyIERlY2ltYWwgUGxhY2VzO1xyXG4gICAgICAgIHJldHVybiBuZXcgSW50bC5OdW1iZXJGb3JtYXQoJ2VuLVVTJywgeyBzdHlsZTogJ2N1cnJlbmN5JywgY3VycmVuY3k6ICdVU0QnLCBtaW5pbXVtRnJhY3Rpb25EaWdpdHM6IHByZWNpc2lvbiwgbWF4aW11bUZyYWN0aW9uRGlnaXRzOiBwcmVjaXNpb24gfSkuZm9ybWF0KHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHByZXBhcmVQZXJjZW50Rm9ybWF0KHByZWNpc2lvbjogbnVtYmVyLCB2YWx1ZTogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgICAgICBwcmVjaXNpb24gPSBwcmVjaXNpb24gPT09IDAgPyAwIDogcHJlY2lzaW9uIHx8IDM7IC8vIERlZmF1bHQgdG8gMyBEZWNpbWFsIFBsYWNlcztcclxuICAgICAgICByZXR1cm4gbmV3IEludGwuTnVtYmVyRm9ybWF0KCdlbi1VUycsIHsgc3R5bGU6ICdwZXJjZW50JywgbWluaW11bUZyYWN0aW9uRGlnaXRzOiBwcmVjaXNpb24sIG1heGltdW1GcmFjdGlvbkRpZ2l0czogcHJlY2lzaW9uIH0pLmZvcm1hdCh2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcmVwYXJlQ29tbWFGb3JtYXQocHJlY2lzaW9uOiBudW1iZXIsIHZhbHVlOiBudW1iZXIpOiBzdHJpbmcge1xyXG4gICAgICAgIHByZWNpc2lvbiA9IHByZWNpc2lvbiA9PT0gMCA/IDAgOiBwcmVjaXNpb24gfHwgMDsgLy8gRGVmYXVsdCB0byAyIERlY2ltYWwgUGxhY2VzO1xyXG4gICAgICAgIHJldHVybiBuZXcgSW50bC5OdW1iZXJGb3JtYXQoJ2VuLVVTJywgeyB1c2VHcm91cGluZzogdHJ1ZSwgbWluaW11bUZyYWN0aW9uRGlnaXRzOiBwcmVjaXNpb24sIG1heGltdW1GcmFjdGlvbkRpZ2l0czogcHJlY2lzaW9uIH0pLmZvcm1hdCh2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhZGRPclJlbW92ZUFnZ3JlZ2F0aW9uU3VtbWFyeShhcmdzRXZ0LCB0eXBlT2ZBZ2dyZWdhdGlvbjogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuYmVnaW5VcGRhdGUoKTtcclxuICAgICAgICBjb25zdCB0b3RhbFN1bW1hcnlJdGVtcyA9IGFyZ3NFdnQuY29tcG9uZW50Lm9wdGlvbignc3VtbWFyeS50b3RhbEl0ZW1zJykgfHwgW10sXHJcbiAgICAgICAgICAgIGdyb3VwU3VtbWFyeUl0ZW1zID0gYXJnc0V2dC5jb21wb25lbnQub3B0aW9uKCdzdW1tYXJ5Lmdyb3VwSXRlbXMnKSB8fCBbXSxcclxuICAgICAgICAgICAgaW5kZXhPZlRvdGFsU3VtbWFyeSA9IHRoaXMuY2hlY2tGb3JFeGlzdGluZ1N1bW1hcnlUeXBlKHRvdGFsU3VtbWFyeUl0ZW1zLCBhcmdzRXZ0LmNvbHVtbi5kYXRhRmllbGQpLFxyXG4gICAgICAgICAgICBpbmRleE9mR3JvdXBTdW1tYXJ5ID0gdGhpcy5jaGVja0ZvckV4aXN0aW5nU3VtbWFyeVR5cGUoZ3JvdXBTdW1tYXJ5SXRlbXMsIGFyZ3NFdnQuY29sdW1uLmRhdGFGaWVsZCk7XHJcbiAgICAgICAgY29uc3QgaXNOb3RUeXBlQ291bnRXaXRoRm9ybWF0ID0gYXJnc0V2dC5jb2x1bW4uZm9ybWF0ICYmIHR5cGVPZkFnZ3JlZ2F0aW9uICE9PSAnY291bnQnO1xyXG4gICAgICAgIGNvbnN0IHN1bW1hcnlPYmogPSB7XHJcbiAgICAgICAgICAgIGNvbHVtbjogYXJnc0V2dC5jb2x1bW4uZGF0YUZpZWxkLFxyXG4gICAgICAgICAgICBzdW1tYXJ5VHlwZTogdHlwZU9mQWdncmVnYXRpb24sXHJcbiAgICAgICAgICAgIGFsaWduQnlDb2x1bW46IHRydWUsXHJcbiAgICAgICAgICAgIGRpc3BsYXlGb3JtYXQ6ICd7MH0nLFxyXG4gICAgICAgICAgICB2YWx1ZUZvcm1hdDogaXNOb3RUeXBlQ291bnRXaXRoRm9ybWF0ID8gYXJnc0V2dC5jb2x1bW4uZm9ybWF0IDogeyB0eXBlOiAnZml4ZWRQb2ludCcsIHByZWNpc2lvbjogMCB9XHJcbiAgICAgICAgICAgIC8vIHNob3dJbkdyb3VwRm9vdGVyOiB0cnVlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAodHlwZU9mQWdncmVnYXRpb24gPT09ICdyZXNldCcpIHtcclxuICAgICAgICAgICAgaWYgKGluZGV4T2ZUb3RhbFN1bW1hcnkgIT09IC0xIHx8IGluZGV4T2ZHcm91cFN1bW1hcnkgIT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICB0b3RhbFN1bW1hcnlJdGVtcy5zcGxpY2UoaW5kZXhPZlRvdGFsU3VtbWFyeSwgMSk7XHJcbiAgICAgICAgICAgICAgICBncm91cFN1bW1hcnlJdGVtcy5zcGxpY2UoaW5kZXhPZkdyb3VwU3VtbWFyeSwgMSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZS5vcHRpb24oJ3N1bW1hcnkudG90YWxJdGVtcycsIHRvdGFsU3VtbWFyeUl0ZW1zKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLm9wdGlvbignc3VtbWFyeS5ncm91cEl0ZW1zJywgZ3JvdXBTdW1tYXJ5SXRlbXMpO1xyXG4gICAgICAgICAgICAgICAgLy8gTWFraW5nIHN1cmUgZmlsdGVycyBhcmUgcmVhcHBsaWVkXHJcbiAgICAgICAgICAgICAgICBpZiAoYXJnc0V2dC5jb21wb25lbnQuc3RhdGUoKS5maWx0ZXJzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXJnc0V2dC5jb21wb25lbnQuZmlsdGVyKGFyZ3NFdnQuY29tcG9uZW50LnN0YXRlKCkuZmlsdGVycyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZW5kVXBkYXRlKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGluZGV4T2ZUb3RhbFN1bW1hcnkgPT09IC0xIHx8IGluZGV4T2ZHcm91cFN1bW1hcnkgPT09IC0xKSB7XHJcbiAgICAgICAgICAgIHRvdGFsU3VtbWFyeUl0ZW1zLnB1c2goc3VtbWFyeU9iaik7XHJcbiAgICAgICAgICAgIGdyb3VwU3VtbWFyeUl0ZW1zLnB1c2goc3VtbWFyeU9iaik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdG90YWxTdW1tYXJ5SXRlbXMuc3BsaWNlKGluZGV4T2ZUb3RhbFN1bW1hcnksIDEpO1xyXG4gICAgICAgICAgICBncm91cFN1bW1hcnlJdGVtcy5zcGxpY2UoaW5kZXhPZkdyb3VwU3VtbWFyeSwgMSk7XHJcbiAgICAgICAgICAgIHRvdGFsU3VtbWFyeUl0ZW1zLnB1c2goc3VtbWFyeU9iaik7XHJcbiAgICAgICAgICAgIGdyb3VwU3VtbWFyeUl0ZW1zLnB1c2goc3VtbWFyeU9iaik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGFyZ3NFdnQuY29tcG9uZW50Lm9wdGlvbignc3VtbWFyeS50b3RhbEl0ZW1zJywgdG90YWxTdW1tYXJ5SXRlbXMpO1xyXG4gICAgICAgIGFyZ3NFdnQuY29tcG9uZW50Lm9wdGlvbignc3VtbWFyeS5ncm91cEl0ZW1zJywgZ3JvdXBTdW1tYXJ5SXRlbXMpO1xyXG4gICAgICAgIC8vIE1ha2luZyBzdXJlIGZpbHRlcnMgYXJlIHJlYXBwbGllZFxyXG4gICAgICAgIGlmIChhcmdzRXZ0LmNvbXBvbmVudC5zdGF0ZSgpLmZpbHRlcnMpIHtcclxuICAgICAgICAgICAgYXJnc0V2dC5jb21wb25lbnQuZmlsdGVyKGFyZ3NFdnQuY29tcG9uZW50LnN0YXRlKCkuZmlsdGVycyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmVuZFVwZGF0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY2hlY2tGb3JFeGlzdGluZ1N1bW1hcnlUeXBlKHN1bW1hcnlJdGVtc0NvbGxlY3Rpb24sIGNvbHVtbk5hbWUpIHtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHN1bW1hcnlJdGVtc0NvbGxlY3Rpb24ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKHN1bW1hcnlJdGVtc0NvbGxlY3Rpb25baV0uY29sdW1uID09PSBjb2x1bW5OYW1lKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhcHBseVN0YXRlUHJvcGVydGllcyh2aWV3SnNvbjogR3JpZFN0YXRlU2F2ZUluZm8pOiB2b2lkIHtcclxuICAgICAgICBpZiAoIXRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlIHx8ICEodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UgYXMgYW55KS5OQU1FKSB7IHJldHVybjsgfVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRUaGVtZSA9ICcnO1xyXG4gICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmJlZ2luQ3VzdG9tTG9hZGluZygnUmVmcmVzaGluZyBHcmlkJyk7XHJcbiAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuYmVnaW5VcGRhdGUoKTtcclxuICAgICAgICBpZiAodmlld0pzb24pIHtcclxuICAgICAgICAgICAgaWYgKHZpZXdKc29uLmNvbHVtbnMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHZpZXdDb2x1bW5zTG9va3VwID0gVG9EaWN0aW9uYXJ5KHZpZXdKc29uPy5jb2x1bW5zIHx8IFtdLCBhID0+IGEuZGF0YUZpZWxkLnRvTG93ZXJDYXNlKCkpO1xyXG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmNvbHVtbnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBrZXkgPSB0aGlzLmNvbHVtbnNbaV0uZGF0YUZpZWxkLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHZpZXdDb2x1bW5zTG9va3VwLmhhc093blByb3BlcnR5KGtleSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2x1bW5zW2ldLnZpc2libGUgPSB2aWV3Q29sdW1uc0xvb2t1cFtrZXldLnZpc2libGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2x1bW5zW2ldLnZpc2libGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gQmVsb3cgbG9vcCBpcyB0byBPVkVSUklERSBDQVBUSU9OIE9GIENPTFVNTlxyXG4gICAgICAgICAgICBpZiAodmlld0pzb24udmlzaWJsZUNvbHVtbnMgJiYgdmlld0pzb24udmlzaWJsZUNvbHVtbnMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKGxldCBpbmRleCA9IDA7IGluZGV4IDwgdmlld0pzb24udmlzaWJsZUNvbHVtbnMubGVuZ3RoOyBpbmRleCsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgX2luZGV4ID0gMDsgX2luZGV4IDwgdGhpcy5jb2x1bW5zLmxlbmd0aDsgX2luZGV4KyspIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGF0YUZpZWxkID0gdmlld0pzb24udmlzaWJsZUNvbHVtbnNbaW5kZXhdLmRhdGFGaWVsZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGFGaWVsZCAmJiBkYXRhRmllbGQgIT09IGN1c3RvbUFjdGlvbkNvbHVtbkluZm8uZGF0YUZpZWxkICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbHVtbnNbX2luZGV4XS5kYXRhRmllbGQudG9Mb3dlckNhc2UoKSA9PT0gZGF0YUZpZWxkLnRvTG93ZXJDYXNlKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uc1tfaW5kZXhdLmNhcHRpb24gPSB2aWV3SnNvbi52aXNpYmxlQ29sdW1uc1tpbmRleF0uY2FwdGlvbjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIHJlc2V0dGluZyBhbHJlYWR5IGFwcGxpZWQgZmlsdGVyIGNsYXNzLlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2x1bW5zW19pbmRleF0uY3NzQ2xhc3MgPSB0aGlzLmNvbHVtbnNbX2luZGV4XS5jc3NDbGFzcz8ucmVwbGFjZSgvZmlsdGVyQXBwbGllZC9nLCAnJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodmlld0pzb24uc3VtbWFyeT8uZ3JvdXBJdGVtcz8ubGVuZ3RoIHx8IHZpZXdKc29uLnN1bW1hcnk/LnRvdGFsSXRlbXM/Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2Uub3B0aW9uKCdzdW1tYXJ5Jywgdmlld0pzb24uc3VtbWFyeSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB2aWV3SnNvbi5zdW1tYXJ5Lmdyb3VwSXRlbXMgPSBbXTtcclxuICAgICAgICAgICAgICAgIHZpZXdKc29uLnN1bW1hcnkudG90YWxJdGVtcyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2Uub3B0aW9uKCdzdW1tYXJ5Jywgdmlld0pzb24uc3VtbWFyeSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgZm9ybWF0Q29sbGVjdGlvbiA9IEpTT04ucGFyc2Uoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShcclxuICAgICAgICAgICAgICAgIGdldFN0b3JhZ2VLZXkodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UsIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmZvcm1hdERhdGFdLCB0aGlzLmlzTWFzdGVyR3JpZCkpIHx8ICdcIlwiJyk7XHJcbiAgICAgICAgICAgIGlmIChmb3JtYXRDb2xsZWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICB2aWV3SnNvbi5jb2x1bW5Gb3JtYXR0aW5nSW5mbyA9IE9iamVjdC5hc3NpZ24oW10sIHRydWUsIGZvcm1hdENvbGxlY3Rpb24sIHZpZXdKc29uLmNvbHVtbkZvcm1hdHRpbmdJbmZvKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKGdldFN0b3JhZ2VLZXkodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UsIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmZvcm1hdERhdGFdLCB0aGlzLmlzTWFzdGVyR3JpZCksXHJcbiAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeSh2aWV3SnNvbi5jb2x1bW5Gb3JtYXR0aW5nSW5mbyA/IHZpZXdKc29uLmNvbHVtbkZvcm1hdHRpbmdJbmZvIDogJycpKTtcclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShnZXRTdG9yYWdlS2V5KHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLFxyXG4gICAgICAgICAgICAgICAgQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuY29uZGl0aW9uYWxGb3JtYXR0aW5nXSwgdGhpcy5pc01hc3RlckdyaWQpLFxyXG4gICAgICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkodmlld0pzb24uY29uZGl0aW9uYWxGb3JtYXR0aW5nSW5mbyA/IHZpZXdKc29uLmNvbmRpdGlvbmFsRm9ybWF0dGluZ0luZm8gOiBudWxsKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZ3JpZEZpbHRlclZhbHVlID0gdmlld0pzb24gPyB2aWV3SnNvbi5maWx0ZXJWYWx1ZSA6IG51bGw7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmdyaWRGaWx0ZXJWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2Uub3B0aW9uKCdmaWx0ZXJWYWx1ZScsIHRoaXMuZ3JpZEZpbHRlclZhbHVlKTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGFwcGx5RmlsdGVyQ3NzQ2xhc3ModGhpcy5ncmlkRmlsdGVyVmFsdWUsIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlKTtcclxuICAgICAgICAgICAgICAgIH0sIDEwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodmlld0pzb24uaGFzT3duUHJvcGVydHkoQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuY2hpbGRHcmlkVmlld10pKSB7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLnNldENoaWxkR3JpZExheW91dEluZm8obGF5b3V0SnNvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgYXBwVG9hc3QoeyBtZXNzYWdlOiAnU2VsZWN0ZWQgdmlldyBzZXR0aW5ncyBoYXMgYmVlbiBhcHBsaWVkLicsIHR5cGU6ICdzdWNjZXNzJyB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5yZW1vdmVJdGVtKFxyXG4gICAgICAgICAgICAgICAgZ2V0U3RvcmFnZUtleSh0aGlzLmdyaWRDb21wb25lbnRJbnN0YW5jZSwgQ0dGU3RvcmFnZUtleXNbQ0dGU3RvcmFnZUtleXMuZm9ybWF0RGF0YV0sIHRoaXMuaXNNYXN0ZXJHcmlkKSk7XHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLnJlbW92ZUl0ZW0oXHJcbiAgICAgICAgICAgICAgICBnZXRTdG9yYWdlS2V5KHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLCBDR0ZTdG9yYWdlS2V5c1tDR0ZTdG9yYWdlS2V5cy5jb25kaXRpb25hbEZvcm1hdHRpbmddLCB0aGlzLmlzTWFzdGVyR3JpZCkpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGNvbHVtbi5jc3NDbGFzcyAmJiBjb2x1bW4uY3NzQ2xhc3MudG9Mb3dlckNhc2UoKS50cmltKCkgPT09ICdmaWx0ZXJhcHBsaWVkJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbHVtbi5jc3NDbGFzcyA9ICcnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFRoZW1lID0gdmlld0pzb24gPyB2aWV3SnNvbi5zZWxlY3RlZFRoZW1lIHx8ICducC5jb21wYWN0JyA6ICducC5jb21wYWN0JztcclxuICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKCd0aGVtZScsIHRoaXMuc2VsZWN0ZWRUaGVtZSk7XHJcbiAgICAgICAgdGhpcy5zaG93Q29sdW1uTGluZXMgPSB2aWV3SnNvbiA/IHZpZXdKc29uLmlzR3JpZEJvcmRlclZpc2libGUgfHwgZmFsc2UgOiBmYWxzZTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkVGhlbWVDbGFzcyA9IGdldENsYXNzTmFtZUJ5VGhlbWVOYW1lKHRoaXMuc2VsZWN0ZWRUaGVtZSk7XHJcbiAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UuZW5kVXBkYXRlKCk7XHJcbiAgICAgICAgdGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2Uuc3RhdGUodmlld0pzb24pO1xyXG4gICAgICAgIHRoaXMuZ3JpZENvbXBvbmVudEluc3RhbmNlLmVuZEN1c3RvbUxvYWRpbmcoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNldEN1c3RvbUNvbHVtbkRhdGEoYXJnczogYW55KTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgY3VzdG9tQ29scyA9IGFyZ3MuY29sdW1ucy5maWx0ZXIoY29sID0+IGNvbC5leHByZXNzaW9uICYmIGNvbC5ncm91cE5hbWUgPT09ICdDdXN0b20gQ29sdW1ucycpO1xyXG4gICAgICAgIGN1c3RvbUNvbHMuZm9yRWFjaChjb2wgPT4ge1xyXG4gICAgICAgICAgICBhcmdzLmRhdGFbY29sLmRhdGFGaWVsZF0gPSBhcmdzLnZhbHVlc1tjb2wuaW5kZXhdO1xyXG4gICAgICAgICAgICB0aGlzLmNvbHVtbnMuZmlsdGVyKGNvbHVtbiA9PiBjb2x1bW4uZGF0YUZpZWxkID09PSBjb2wuZGF0YUZpZWxkKVswXS5kYXRhVHlwZSA9IGNvbC5kYXRhVHlwZTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGZvcm1hdFJvd3MoZGF0YVR5cGU6IHN0cmluZywgcm93RGF0YSwgcm93RWxlbWVudCwgZXhjZWxDZWxsKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGRhdGFUeXBlID09PSByb3dUeXBlSW5mby5kYXRhKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbmRpdGlvbmFsRm9ybWF0Q29sbGVjdGlvbjogQ29sdW1uRm9ybWF0W10gPVxyXG4gICAgICAgICAgICAgICAgSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGdldFN0b3JhZ2VLZXkodGhpcy5ncmlkQ29tcG9uZW50SW5zdGFuY2UsIENHRlN0b3JhZ2VLZXlzW0NHRlN0b3JhZ2VLZXlzLmNvbmRpdGlvbmFsRm9ybWF0dGluZ10sIHRoaXMuaXNNYXN0ZXJHcmlkKSkpIHx8IFtdO1xyXG4gICAgICAgICAgICBjb25zdCBjb25kaXRpb25GaWx0ZXJJdGVtcyA9IGNvbmRpdGlvbmFsRm9ybWF0Q29sbGVjdGlvbi5maWx0ZXIoYXJySXRlbSA9PiBhcnJJdGVtLmFwcGx5VHlwZSA9PT0gJ3JvdycpO1xyXG4gICAgICAgICAgICBsZXQgY29uZGl0aW9uYWxGb3JtYXR0aW5nUmVxdWlyZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgY29uZGl0aW9uRmlsdGVySXRlbXMuZm9yRWFjaChmaWx0ZXJJdGVtID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChmaWx0ZXJJdGVtLmNvbmRpdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbmRpdGlvbmFsRm9ybWF0dGluZ1JlcXVpcmVkID0gdGhpcy5jaGVja0NvbmRpdGlvblNhdGlzZmllZChmaWx0ZXJJdGVtLCByb3dEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29uZGl0aW9uYWxGb3JtYXR0aW5nUmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJvd0VsZW1lbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCByb3dFbGVtZW50LmNoaWxkcmVuLmxlbmd0aDsgaW5kZXgrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGV4aXN0aW5nQ2xhc3NlcyA9IHJvd0VsZW1lbnQuY2hpbGRyZW5baW5kZXhdLmNsYXNzTGlzdD8udG9TdHJpbmcoKS5yZXBsYWNlKCdib2xkJywgJycpLnJlcGxhY2UoJ3VuZGVybGluZScsICcnKS5yZXBsYWNlKCdpdGFsaWMnLCAnJykgfHwgJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93RWxlbWVudC5jaGlsZHJlbltpbmRleF0uc3R5bGUuYmFja2dyb3VuZENvbG9yID0gZmlsdGVySXRlbS5iYWNrZ3JvdW5kQ29sb3IgfHwgcm93RWxlbWVudC5jaGlsZHJlbltpbmRleF0uc3R5bGUuYmFja2dyb3VuZENvbG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvd0VsZW1lbnQuY2hpbGRyZW5baW5kZXhdLnN0eWxlLmNvbG9yID0gZmlsdGVySXRlbS50ZXh0Q29sb3IgfHwgcm93RWxlbWVudC5jaGlsZHJlbltpbmRleF0uc3R5bGUuY29sb3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93RWxlbWVudC5jaGlsZHJlbltpbmRleF0uY2xhc3NMaXN0ID0gZXhpc3RpbmdDbGFzc2VzLmNvbmNhdCgnICcpLmNvbmNhdChmaWx0ZXJJdGVtLmNzc0NsYXNzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChleGNlbENlbGwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWx0ZXJJdGVtLmJhY2tncm91bmRDb2xvcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4Y2VsQ2VsbC5maWxsID0ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAncGF0dGVybicsIHBhdHRlcm46ICdzb2xpZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZnQ29sb3I6IHsgYXJnYjogZmlsdGVySXRlbS5iYWNrZ3JvdW5kQ29sb3IucmVwbGFjZSgnIycsICdmZicpIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZpbHRlckl0ZW0udGV4dENvbG9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhjZWxDZWxsLmZvbnQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiB7IGFyZ2I6IGZpbHRlckl0ZW0udGV4dENvbG9yLnJlcGxhY2UoJyMnLCAnZmYnKSB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaWx0ZXJJdGVtLmNzc0NsYXNzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5leGNlbENlbGxGb3JtYXQoZmlsdGVySXRlbS5jc3NDbGFzcywgZXhjZWxDZWxsLCBudWxsKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=