import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { appToast } from '../utilities/utilityFunctions';
import * as i0 from "@angular/core";
let CGFUtilityService = class CGFUtilityService {
    constructor() { }
    applyFiltersToGrid(gridInstance, filterValue) {
        if (gridInstance) {
            const listOfVisibleColumns = gridInstance.getVisibleColumns();
            let cssClasses;
            for (let i = 0; i < listOfVisibleColumns.length; i++) {
                cssClasses = gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass');
                if (cssClasses) {
                    gridInstance.columnOption(listOfVisibleColumns[i].dataField, 'cssClass', cssClasses.replace(new RegExp('filterApplied', 'g'), '').trim());
                }
            }
            if (filterValue) {
                this.applyFilterClass(filterValue, gridInstance);
                const alertOptions = {
                    type: 'warning', message: 'To permanently apply this filter,please go to the \'View Selection\' and click \'Save\''
                        + ' in order to associate this filter with a view.'
                };
                appToast(alertOptions);
            }
        }
    }
    applyFilterClass(filter = [], gridInstance) {
        let i;
        let cssClasses;
        let columnInfo;
        for (i = 0; filter && i < filter.length; i++) {
            if (Array.isArray(filter[i])) {
                if (Array.isArray(filter[i][0])) {
                    this.applyFilterClass(filter[i], gridInstance);
                }
                else {
                    columnInfo = gridInstance.columnOption(filter[i][0]);
                    if (columnInfo && (columnInfo.dataField === filter[i][0])) {
                        cssClasses = gridInstance.columnOption(filter[i][0], 'cssClass');
                        gridInstance.columnOption(filter[i][0], 'cssClass', cssClasses + ' filterApplied');
                    }
                }
            }
            else {
                columnInfo = gridInstance.columnOption(filter[i]);
                if (columnInfo && (columnInfo.dataField === filter[i])) {
                    cssClasses = gridInstance.columnOption(filter[i], 'cssClass');
                    gridInstance.columnOption(filter[i], 'cssClass', cssClasses ? cssClasses + ' filterApplied' : 'filterApplied');
                }
            }
        }
    }
    getStorageKey(gridInstance, keyValue, isMasterGrid = true) {
        const masterDetailInfo = gridInstance === null || gridInstance === void 0 ? void 0 : gridInstance.option('masterDetail');
        return isMasterGrid ? keyValue : `${keyValue}_${masterDetailInfo.template}`;
    }
};
CGFUtilityService.ɵprov = i0.ɵɵdefineInjectable({ factory: function CGFUtilityService_Factory() { return new CGFUtilityService(); }, token: CGFUtilityService, providedIn: "root" });
CGFUtilityService = __decorate([
    Injectable({ providedIn: 'root' })
], CGFUtilityService);
export { CGFUtilityService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2dmLXV0aWxpdHkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb21tb24tZ3JpZC1mcmFtZXdvcmsvc2VydmljZXMvY2dmLXV0aWxpdHkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sK0JBQStCLENBQUM7O0FBR3pELElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBRTVCLGdCQUFnQixDQUFDO0lBRVYsa0JBQWtCLENBQUMsWUFBaUIsRUFBRSxXQUFpQjtRQUM1RCxJQUFJLFlBQVksRUFBRTtZQUNoQixNQUFNLG9CQUFvQixHQUFHLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQzlELElBQUksVUFBa0IsQ0FBQztZQUN2QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsb0JBQW9CLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUNwRCxVQUFVLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ3RGLElBQUksVUFBVSxFQUFFO29CQUNkLFlBQVksQ0FBQyxZQUFZLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUN6RCxVQUFVLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLE1BQU0sQ0FBQyxlQUFlLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztpQkFDaEY7YUFDRjtZQUVELElBQUksV0FBVyxFQUFFO2dCQUNmLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ2pELE1BQU0sWUFBWSxHQUFHO29CQUNuQixJQUFJLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSx5RkFBeUY7MEJBQy9HLGlEQUFpRDtpQkFDdEQsQ0FBQztnQkFDRixRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDeEI7U0FDRjtJQUNILENBQUM7SUFFTSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsRUFBRSxFQUFFLFlBQVk7UUFDL0MsSUFBSSxDQUFTLENBQUM7UUFBQyxJQUFJLFVBQWtCLENBQUM7UUFBQyxJQUFJLFVBQXlCLENBQUM7UUFDckUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLE1BQU0sSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzVCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDL0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQztpQkFDaEQ7cUJBQU07b0JBQ0wsVUFBVSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3JELElBQUksVUFBVSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTt3QkFDekQsVUFBVSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO3dCQUNqRSxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLEVBQUUsVUFBVSxHQUFHLGdCQUFnQixDQUFDLENBQUM7cUJBQ3BGO2lCQUNGO2FBQ0Y7aUJBQU07Z0JBQ0wsVUFBVSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xELElBQUksVUFBVSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTtvQkFDdEQsVUFBVSxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxDQUFDO29CQUM5RCxZQUFZLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2lCQUNoSDthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBRU0sYUFBYSxDQUFDLFlBQXdCLEVBQUUsUUFBZ0IsRUFBRSxZQUFZLEdBQUcsSUFBSTtRQUNsRixNQUFNLGdCQUFnQixHQUFHLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDOUQsT0FBTyxZQUFZLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsR0FBRyxRQUFRLElBQUksZ0JBQWdCLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDOUUsQ0FBQztDQUNGLENBQUE7O0FBdERZLGlCQUFpQjtJQUQ3QixVQUFVLENBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLENBQUM7R0FDdEIsaUJBQWlCLENBc0Q3QjtTQXREWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBCSUdyaWRDb2x1bW4gfSBmcm9tICcuLi9jb21wb25lbnRzL2NvbnRyYWN0cy9ncmlkJztcclxuaW1wb3J0IGR4RGF0YUdyaWQgZnJvbSAnZGV2ZXh0cmVtZS91aS9kYXRhX2dyaWQnO1xyXG5pbXBvcnQgeyBhcHBUb2FzdCB9IGZyb20gJy4uL3V0aWxpdGllcy91dGlsaXR5RnVuY3Rpb25zJztcclxuXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXHJcbmV4cG9ydCBjbGFzcyBDR0ZVdGlsaXR5U2VydmljZSB7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIHB1YmxpYyBhcHBseUZpbHRlcnNUb0dyaWQoZ3JpZEluc3RhbmNlOiBhbnksIGZpbHRlclZhbHVlPzogYW55KTogdm9pZCB7XHJcbiAgICBpZiAoZ3JpZEluc3RhbmNlKSB7XHJcbiAgICAgIGNvbnN0IGxpc3RPZlZpc2libGVDb2x1bW5zID0gZ3JpZEluc3RhbmNlLmdldFZpc2libGVDb2x1bW5zKCk7XHJcbiAgICAgIGxldCBjc3NDbGFzc2VzOiBzdHJpbmc7XHJcbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbGlzdE9mVmlzaWJsZUNvbHVtbnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICBjc3NDbGFzc2VzID0gZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihsaXN0T2ZWaXNpYmxlQ29sdW1uc1tpXS5kYXRhRmllbGQsICdjc3NDbGFzcycpO1xyXG4gICAgICAgIGlmIChjc3NDbGFzc2VzKSB7XHJcbiAgICAgICAgICBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGxpc3RPZlZpc2libGVDb2x1bW5zW2ldLmRhdGFGaWVsZCxcclxuICAgICAgICAgICAgJ2Nzc0NsYXNzJywgY3NzQ2xhc3Nlcy5yZXBsYWNlKG5ldyBSZWdFeHAoJ2ZpbHRlckFwcGxpZWQnLCAnZycpLCAnJykudHJpbSgpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChmaWx0ZXJWYWx1ZSkge1xyXG4gICAgICAgIHRoaXMuYXBwbHlGaWx0ZXJDbGFzcyhmaWx0ZXJWYWx1ZSwgZ3JpZEluc3RhbmNlKTtcclxuICAgICAgICBjb25zdCBhbGVydE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICB0eXBlOiAnd2FybmluZycsIG1lc3NhZ2U6ICdUbyBwZXJtYW5lbnRseSBhcHBseSB0aGlzIGZpbHRlcixwbGVhc2UgZ28gdG8gdGhlIFxcJ1ZpZXcgU2VsZWN0aW9uXFwnIGFuZCBjbGljayBcXCdTYXZlXFwnJ1xyXG4gICAgICAgICAgICArICcgaW4gb3JkZXIgdG8gYXNzb2NpYXRlIHRoaXMgZmlsdGVyIHdpdGggYSB2aWV3LidcclxuICAgICAgICB9O1xyXG4gICAgICAgIGFwcFRvYXN0KGFsZXJ0T3B0aW9ucyk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBhcHBseUZpbHRlckNsYXNzKGZpbHRlciA9IFtdLCBncmlkSW5zdGFuY2UpOiB2b2lkIHtcclxuICAgIGxldCBpOiBudW1iZXI7IGxldCBjc3NDbGFzc2VzOiBzdHJpbmc7IGxldCBjb2x1bW5JbmZvOiBQQklHcmlkQ29sdW1uO1xyXG4gICAgZm9yIChpID0gMDsgZmlsdGVyICYmIGkgPCBmaWx0ZXIubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZmlsdGVyW2ldKSkge1xyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KGZpbHRlcltpXVswXSkpIHtcclxuICAgICAgICAgIHRoaXMuYXBwbHlGaWx0ZXJDbGFzcyhmaWx0ZXJbaV0sIGdyaWRJbnN0YW5jZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbHVtbkluZm8gPSBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXVswXSk7XHJcbiAgICAgICAgICBpZiAoY29sdW1uSW5mbyAmJiAoY29sdW1uSW5mby5kYXRhRmllbGQgPT09IGZpbHRlcltpXVswXSkpIHtcclxuICAgICAgICAgICAgY3NzQ2xhc3NlcyA9IGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldWzBdLCAnY3NzQ2xhc3MnKTtcclxuICAgICAgICAgICAgZ3JpZEluc3RhbmNlLmNvbHVtbk9wdGlvbihmaWx0ZXJbaV1bMF0sICdjc3NDbGFzcycsIGNzc0NsYXNzZXMgKyAnIGZpbHRlckFwcGxpZWQnKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29sdW1uSW5mbyA9IGdyaWRJbnN0YW5jZS5jb2x1bW5PcHRpb24oZmlsdGVyW2ldKTtcclxuICAgICAgICBpZiAoY29sdW1uSW5mbyAmJiAoY29sdW1uSW5mby5kYXRhRmllbGQgPT09IGZpbHRlcltpXSkpIHtcclxuICAgICAgICAgIGNzc0NsYXNzZXMgPSBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXSwgJ2Nzc0NsYXNzJyk7XHJcbiAgICAgICAgICBncmlkSW5zdGFuY2UuY29sdW1uT3B0aW9uKGZpbHRlcltpXSwgJ2Nzc0NsYXNzJywgY3NzQ2xhc3NlcyA/IGNzc0NsYXNzZXMgKyAnIGZpbHRlckFwcGxpZWQnIDogJ2ZpbHRlckFwcGxpZWQnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHB1YmxpYyBnZXRTdG9yYWdlS2V5KGdyaWRJbnN0YW5jZTogZHhEYXRhR3JpZCwga2V5VmFsdWU6IHN0cmluZywgaXNNYXN0ZXJHcmlkID0gdHJ1ZSk6IHN0cmluZyB7XHJcbiAgICBjb25zdCBtYXN0ZXJEZXRhaWxJbmZvID0gZ3JpZEluc3RhbmNlPy5vcHRpb24oJ21hc3RlckRldGFpbCcpO1xyXG4gICAgcmV0dXJuIGlzTWFzdGVyR3JpZCA/IGtleVZhbHVlIDogYCR7a2V5VmFsdWV9XyR7bWFzdGVyRGV0YWlsSW5mby50ZW1wbGF0ZX1gO1xyXG4gIH1cclxufVxyXG4iXX0=