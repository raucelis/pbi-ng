import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let SearchService = class SearchService {
    /**
     * Returns elements of the array that contain the given string, case insensitive, anywhere in their object hierarchy.
     * @param input The array to search.
     * @param text The string to match, case insensitive.
     * @param exclude The list of properties to exclude from the search.
     */
    deepArrayContains(input, text, exclude) {
        if (!text) {
            return input;
        }
        let textLower = text.toLowerCase();
        let filtered = this.deepArrayFilter(input, val => val !== null && val !== undefined && val.toString().toLowerCase().indexOf(textLower) > -1, exclude);
        return Array.from(filtered);
    }
    /**
     * Returns elements of the array that match the provided function.
     * @param input The array to search.
     * @param filter The filter function to apply.
     */
    *deepArrayFilter(input, filter, exclude) {
        for (const item of input) {
            if (this.deepMatch(item, filter, exclude)) {
                yield item;
            }
        }
    }
    /**
     * Finds if any properties of the given object, or their children, match the given string, case insensitive.
     * @param obj The object to search.
     * @param text The string to match, case insensitive.
     */
    deepContains(obj, text) {
        if (!text) {
            return true;
        }
        const textLower = text.toLowerCase();
        return this.deepMatch(obj, val => val !== null && val !== undefined && val.toString().toLowerCase().indexOf(textLower) > -1);
    }
    /**
     * Finds if any properties of the given object, or their children, match the given filter.
     * @param obj The object to search.
     * @param filter The filter function to apply.
     */
    deepMatch(obj, filter, exclude) {
        for (const prop in obj) {
            if (!obj.hasOwnProperty(prop))
                continue;
            if (exclude && exclude.includes(prop))
                continue;
            const propVal = obj[prop];
            if (Array.isArray(propVal)) {
                for (const item of propVal) {
                    if (this.deepMatch(item, filter, exclude)) {
                        return true;
                    }
                }
            }
            else {
                if (propVal instanceof Date) {
                    let a = propVal.toLocaleDateString();
                    let b = a.split('/').join('-');
                    if (filter(a) || filter(b)) {
                        return true;
                    }
                }
                else if (filter(propVal)) {
                    return true;
                }
            }
        }
        return false;
    }
    treeWalk(arr, childPath, callback) {
        if (!arr) {
            return undefined;
        }
        for (let item of arr) {
            callback(item);
            const children = item[childPath];
            this.treeWalk(children, childPath, callback);
        }
    }
};
SearchService = __decorate([
    Injectable()
], SearchService);
export { SearchService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9wYmktbmcvIiwic291cmNlcyI6WyJsaWIvY29yZS9zZXJ2aWNlcy9zZWFyY2guc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxJQUFhLGFBQWEsR0FBMUIsTUFBYSxhQUFhO0lBQ3pCOzs7OztPQUtHO0lBQ0gsaUJBQWlCLENBQUksS0FBVSxFQUFFLElBQVksRUFBRSxPQUFrQjtRQUNoRSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQUUsT0FBTyxLQUFLLENBQUM7U0FBRTtRQUM1QixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDbkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssSUFBSSxJQUFJLEdBQUcsS0FBSyxTQUFTLElBQUksR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN0SixPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxDQUFDLGVBQWUsQ0FBSSxLQUFVLEVBQUUsTUFBa0MsRUFBRSxPQUFrQjtRQUNyRixLQUFLLE1BQU0sSUFBSSxJQUFJLEtBQUssRUFBRTtZQUN6QixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDMUMsTUFBTSxJQUFJLENBQUM7YUFDWDtTQUNEO0lBQ0YsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxZQUFZLENBQUksR0FBTSxFQUFFLElBQVk7UUFDbkMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUFFLE9BQU8sSUFBSSxDQUFDO1NBQUU7UUFDM0IsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssSUFBSSxJQUFJLEdBQUcsS0FBSyxTQUFTLElBQUksR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzlILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsU0FBUyxDQUFJLEdBQU0sRUFBRSxNQUFrQyxFQUFFLE9BQWtCO1FBQzFFLEtBQUssTUFBTSxJQUFJLElBQUksR0FBRyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQztnQkFBRSxTQUFTO1lBQ3hDLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUFFLFNBQVM7WUFFaEQsTUFBTSxPQUFPLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzFCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtnQkFDM0IsS0FBSyxNQUFNLElBQUksSUFBSSxPQUFPLEVBQUU7b0JBQzNCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxFQUFFO3dCQUMxQyxPQUFPLElBQUksQ0FBQztxQkFDWjtpQkFDRDthQUNEO2lCQUNJO2dCQUVKLElBQUksT0FBTyxZQUFZLElBQUksRUFBRTtvQkFDNUIsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLGtCQUFrQixFQUFFLENBQUM7b0JBQ3JDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMvQixJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUU7d0JBQzNCLE9BQU8sSUFBSSxDQUFDO3FCQUNaO2lCQUNEO3FCQUFNLElBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUMzQixPQUFPLElBQUksQ0FBQztpQkFDWjthQUNEO1NBQ0Q7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNkLENBQUM7SUFFRCxRQUFRLENBQUksR0FBUSxFQUFFLFNBQWtCLEVBQUUsUUFBMkI7UUFDcEUsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUFFLE9BQU8sU0FBUyxDQUFDO1NBQUU7UUFFL0IsS0FBSyxJQUFJLElBQUksSUFBSSxHQUFHLEVBQUU7WUFDckIsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRWYsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBZSxDQUFDO1lBQy9DLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUM3QztJQUNGLENBQUM7Q0FDRCxDQUFBO0FBbkZZLGFBQWE7SUFEekIsVUFBVSxFQUFFO0dBQ0EsYUFBYSxDQW1GekI7U0FuRlksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFNlYXJjaFNlcnZpY2Uge1xyXG5cdC8qKlxyXG5cdCAqIFJldHVybnMgZWxlbWVudHMgb2YgdGhlIGFycmF5IHRoYXQgY29udGFpbiB0aGUgZ2l2ZW4gc3RyaW5nLCBjYXNlIGluc2Vuc2l0aXZlLCBhbnl3aGVyZSBpbiB0aGVpciBvYmplY3QgaGllcmFyY2h5LlxyXG5cdCAqIEBwYXJhbSBpbnB1dCBUaGUgYXJyYXkgdG8gc2VhcmNoLlxyXG5cdCAqIEBwYXJhbSB0ZXh0IFRoZSBzdHJpbmcgdG8gbWF0Y2gsIGNhc2UgaW5zZW5zaXRpdmUuXHJcblx0ICogQHBhcmFtIGV4Y2x1ZGUgVGhlIGxpc3Qgb2YgcHJvcGVydGllcyB0byBleGNsdWRlIGZyb20gdGhlIHNlYXJjaC5cclxuXHQgKi9cclxuXHRkZWVwQXJyYXlDb250YWluczxUPihpbnB1dDogVFtdLCB0ZXh0OiBzdHJpbmcsIGV4Y2x1ZGU/OiBzdHJpbmdbXSk6IFRbXSB7XHJcblx0XHRpZiAoIXRleHQpIHsgcmV0dXJuIGlucHV0OyB9XHJcblx0XHRsZXQgdGV4dExvd2VyID0gdGV4dC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0bGV0IGZpbHRlcmVkID0gdGhpcy5kZWVwQXJyYXlGaWx0ZXIoaW5wdXQsIHZhbCA9PiB2YWwgIT09IG51bGwgJiYgdmFsICE9PSB1bmRlZmluZWQgJiYgdmFsLnRvU3RyaW5nKCkudG9Mb3dlckNhc2UoKS5pbmRleE9mKHRleHRMb3dlcikgPiAtMSwgZXhjbHVkZSk7XHJcblx0XHRyZXR1cm4gQXJyYXkuZnJvbShmaWx0ZXJlZCk7XHJcblx0fVxyXG5cclxuXHQvKipcclxuXHQgKiBSZXR1cm5zIGVsZW1lbnRzIG9mIHRoZSBhcnJheSB0aGF0IG1hdGNoIHRoZSBwcm92aWRlZCBmdW5jdGlvbi5cclxuXHQgKiBAcGFyYW0gaW5wdXQgVGhlIGFycmF5IHRvIHNlYXJjaC5cclxuXHQgKiBAcGFyYW0gZmlsdGVyIFRoZSBmaWx0ZXIgZnVuY3Rpb24gdG8gYXBwbHkuXHJcblx0ICovXHJcblx0KmRlZXBBcnJheUZpbHRlcjxUPihpbnB1dDogVFtdLCBmaWx0ZXI6IChwcm9wZXJ0eTogYW55KSA9PiBib29sZWFuLCBleGNsdWRlPzogc3RyaW5nW10pIHtcclxuXHRcdGZvciAoY29uc3QgaXRlbSBvZiBpbnB1dCkge1xyXG5cdFx0XHRpZiAodGhpcy5kZWVwTWF0Y2goaXRlbSwgZmlsdGVyLCBleGNsdWRlKSkge1xyXG5cdFx0XHRcdHlpZWxkIGl0ZW07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIEZpbmRzIGlmIGFueSBwcm9wZXJ0aWVzIG9mIHRoZSBnaXZlbiBvYmplY3QsIG9yIHRoZWlyIGNoaWxkcmVuLCBtYXRjaCB0aGUgZ2l2ZW4gc3RyaW5nLCBjYXNlIGluc2Vuc2l0aXZlLlxyXG5cdCAqIEBwYXJhbSBvYmogVGhlIG9iamVjdCB0byBzZWFyY2guXHJcblx0ICogQHBhcmFtIHRleHQgVGhlIHN0cmluZyB0byBtYXRjaCwgY2FzZSBpbnNlbnNpdGl2ZS5cclxuXHQgKi9cclxuXHRkZWVwQ29udGFpbnM8VD4ob2JqOiBULCB0ZXh0OiBzdHJpbmcpIHtcclxuXHRcdGlmICghdGV4dCkgeyByZXR1cm4gdHJ1ZTsgfVxyXG5cdFx0Y29uc3QgdGV4dExvd2VyID0gdGV4dC50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0cmV0dXJuIHRoaXMuZGVlcE1hdGNoKG9iaiwgdmFsID0+IHZhbCAhPT0gbnVsbCAmJiB2YWwgIT09IHVuZGVmaW5lZCAmJiB2YWwudG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpLmluZGV4T2YodGV4dExvd2VyKSA+IC0xKTtcclxuXHR9XHJcblxyXG5cdC8qKlxyXG5cdCAqIEZpbmRzIGlmIGFueSBwcm9wZXJ0aWVzIG9mIHRoZSBnaXZlbiBvYmplY3QsIG9yIHRoZWlyIGNoaWxkcmVuLCBtYXRjaCB0aGUgZ2l2ZW4gZmlsdGVyLlxyXG5cdCAqIEBwYXJhbSBvYmogVGhlIG9iamVjdCB0byBzZWFyY2guXHJcblx0ICogQHBhcmFtIGZpbHRlciBUaGUgZmlsdGVyIGZ1bmN0aW9uIHRvIGFwcGx5LlxyXG5cdCAqL1xyXG5cdGRlZXBNYXRjaDxUPihvYmo6IFQsIGZpbHRlcjogKHByb3BlcnR5OiBhbnkpID0+IGJvb2xlYW4sIGV4Y2x1ZGU/OiBzdHJpbmdbXSk6IGJvb2xlYW4ge1xyXG5cdFx0Zm9yIChjb25zdCBwcm9wIGluIG9iaikge1xyXG5cdFx0XHRpZiAoIW9iai5oYXNPd25Qcm9wZXJ0eShwcm9wKSkgY29udGludWU7XHJcblx0XHRcdGlmIChleGNsdWRlICYmIGV4Y2x1ZGUuaW5jbHVkZXMocHJvcCkpIGNvbnRpbnVlO1xyXG5cclxuXHRcdFx0Y29uc3QgcHJvcFZhbCA9IG9ialtwcm9wXTtcclxuXHRcdFx0aWYgKEFycmF5LmlzQXJyYXkocHJvcFZhbCkpIHtcclxuXHRcdFx0XHRmb3IgKGNvbnN0IGl0ZW0gb2YgcHJvcFZhbCkge1xyXG5cdFx0XHRcdFx0aWYgKHRoaXMuZGVlcE1hdGNoKGl0ZW0sIGZpbHRlciwgZXhjbHVkZSkpIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdGVsc2Uge1xyXG5cclxuXHRcdFx0XHRpZiAocHJvcFZhbCBpbnN0YW5jZW9mIERhdGUpIHtcclxuXHRcdFx0XHRcdGxldCBhID0gcHJvcFZhbC50b0xvY2FsZURhdGVTdHJpbmcoKTtcclxuXHRcdFx0XHRcdGxldCBiID0gYS5zcGxpdCgnLycpLmpvaW4oJy0nKTtcclxuXHRcdFx0XHRcdGlmIChmaWx0ZXIoYSkgfHwgZmlsdGVyKGIpKSB7XHJcblx0XHRcdFx0XHRcdHJldHVybiB0cnVlO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0gZWxzZSBpZiAoZmlsdGVyKHByb3BWYWwpKSB7XHJcblx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gZmFsc2U7XHJcblx0fVxyXG5cclxuXHR0cmVlV2FsazxUPihhcnI6IFRbXSwgY2hpbGRQYXRoOiBrZXlvZiBULCBjYWxsYmFjazogKGl0ZW06IFQpID0+IHZvaWQpIHtcclxuXHRcdGlmICghYXJyKSB7IHJldHVybiB1bmRlZmluZWQ7IH1cclxuXHJcblx0XHRmb3IgKGxldCBpdGVtIG9mIGFycikge1xyXG5cdFx0XHRjYWxsYmFjayhpdGVtKTtcclxuXHJcblx0XHRcdGNvbnN0IGNoaWxkcmVuID0gaXRlbVtjaGlsZFBhdGhdIGFzIGFueSBhcyBUW107XHJcblx0XHRcdHRoaXMudHJlZVdhbGsoY2hpbGRyZW4sIGNoaWxkUGF0aCwgY2FsbGJhY2spO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXX0=