import { __decorate } from "tslib";
import { Component, EventEmitter, HostBinding, Input, Output, HostListener } from '@angular/core';
import { Router } from '@angular/router';
let NavigationMenuComponent = class NavigationMenuComponent {
    constructor(router) {
        this.router = router;
        this._config = {
            showToggleButton: true,
            collapsed: true,
            sections: []
        };
        this.action = new EventEmitter();
    }
    get collapsed() {
        return this.config ? this.config.collapsed : true;
    }
    onDocumentClick(target) {
        let el = target.closest('pbi-navigation-menu');
        if (el === null)
            this.dismiss();
    }
    onKeyDown(event) {
        this.keyboardEvent = event;
    }
    onKeyUp(event) {
        this.keyboardEvent = null;
    }
    get config() {
        return this._config;
    }
    set config(v) {
        if (v) {
            this._config = v;
        }
    }
    get shiftKey() {
        return this.keyboardEvent && this.keyboardEvent.shiftKey;
    }
    get ctrlKey() {
        return this.keyboardEvent && this.keyboardEvent.ctrlKey;
    }
    get newTab() {
        return this.ctrlKey || this.shiftKey;
    }
    ngOnInit() { }
    toggle() {
        if (!this.config) {
            return;
        }
        this.config.collapsed = !this.config.collapsed;
    }
    dismiss() {
        this.activeSectionConfig = null;
        this.config.collapsed = true;
    }
    browse(config, event) {
        event.stopImmediatePropagation();
        if (this.isBrowsing(config)) {
            this.activeSectionConfig = null;
            return;
        }
        if (config.content) {
            this.activeSectionConfig = config;
        }
        else {
            this.navigateTo(config, this.newTab);
        }
    }
    isBrowsing(config) {
        return this.activeSectionConfig === config;
    }
    navigateTo(config, newTab) {
        this.dismiss();
        newTab ? this.windowNavigateTo(config) : this.ngNavigateTo(config);
        this.keyboardEvent = null;
    }
    handleAction(actionId) {
        this.dismiss();
        this.action.emit(actionId);
    }
    windowNavigateTo(config) {
        const link = config.link;
        const queryParams = config.queryParams;
        if (this.isExternalLink(link))
            this.openNewBrowserTab(link, queryParams);
        else
            this.openNewBrowserTab(this.config.useHash ? `#${link}` : link, queryParams);
    }
    ngNavigateTo(config) {
        const link = config.link;
        const queryParams = config.queryParams;
        if (this.isExternalLink(link))
            this.openNewBrowserTab(link, queryParams);
        else {
            //TODO: run validation checks if ever needed.
            queryParams ? this.router.navigate([link], { queryParams: queryParams }) : this.router.navigate([link]);
        }
    }
    isExternalLink(link) {
        return link.includes('http://') || link.includes('https://');
    }
    openNewBrowserTab(link, queryParams) {
        let l = this.config.href ? `${this.config.href}${link}` : `${link}`;
        if (queryParams) {
            let params = [];
            for (let property in queryParams) {
                if (!queryParams.hasOwnProperty(property)) {
                    continue;
                }
                params.push(`${property}=${queryParams[property]}`);
            }
            l += `?${params.join('&')}`;
        }
        this.shiftKey ? window.open(l) : window.open(l, '_blank');
    }
};
NavigationMenuComponent.ctorParameters = () => [
    { type: Router }
];
__decorate([
    HostBinding('class.collapsed')
], NavigationMenuComponent.prototype, "collapsed", null);
__decorate([
    HostListener('document:click', ['$event.target'])
], NavigationMenuComponent.prototype, "onDocumentClick", null);
__decorate([
    HostListener('document:keydown', ['$event'])
], NavigationMenuComponent.prototype, "onKeyDown", null);
__decorate([
    HostListener('document:keyup', ['$event'])
], NavigationMenuComponent.prototype, "onKeyUp", null);
__decorate([
    Input()
], NavigationMenuComponent.prototype, "config", null);
__decorate([
    Output()
], NavigationMenuComponent.prototype, "action", void 0);
NavigationMenuComponent = __decorate([
    Component({
        selector: 'pbi-navigation-menu',
        template: "<ng-template [ngIf]=\"config.showToggleButton\">\r\n    <div (click)=\"toggle()\" class=\"navigation-item toggle\">\r\n        <div>\r\n            <div>\r\n                <i class=\"fa fa-bars icon\"></i><span></span>\r\n            </div>\r\n            <div></div>\r\n        </div>\r\n    </div>\r\n</ng-template>\r\n<ng-template [ngIf]=\"config\">\r\n    <div *ngFor=\"let sectionConfig of config.sections\" class=\"navigation-item\">\r\n        <div [title]=\"sectionConfig.title\">\r\n            <div (click)=\"browse(sectionConfig, $event)\">\r\n                <i class=\"{{sectionConfig.icon}} icon\"></i><span class=\"text\">{{sectionConfig.title}}</span>\r\n            </div>\r\n            <div>\r\n                <div *ngIf=\"sectionConfig.openNewTab\" (click)=\"navigateTo(sectionConfig, true)\">\r\n                    <i class=\"fa fa-external-link icon\"></i>\r\n                </div>\r\n            </div>\r\n            <div>\r\n                <div *ngIf=\"sectionConfig.content\" (click)=\"browse(sectionConfig, $event)\">\r\n                    <i *ngIf=\"isBrowsing(sectionConfig); else notBrowsing\" class=\"fa fa-angle-down icon\"></i>\r\n                    <ng-template #notBrowsing>\r\n                        <i class=\"fa fa-angle-right icon\"></i>\r\n                    </ng-template>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <ng-template [ngIf]=\"isBrowsing(sectionConfig)\">\r\n            <pbi-navigation-section [config]=\"sectionConfig\" (action)=\"handleAction($event)\" (navigateTo)=\"navigateTo($event, newTab)\" (navigateToNewTab)=\"navigateTo($event, true)\"></pbi-navigation-section>\r\n        </ng-template>\r\n    </div>\r\n</ng-template>"
    })
], NavigationMenuComponent);
export { NavigationMenuComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1tZW51LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBVSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUcsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBUXpDLElBQWEsdUJBQXVCLEdBQXBDLE1BQWEsdUJBQXVCO0lBK0NoQyxZQUE2QixNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQTVCbkMsWUFBTyxHQUF5QjtZQUNwQyxnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLFNBQVMsRUFBRSxJQUFJO1lBQ2YsUUFBUSxFQUFFLEVBQUU7U0FDZixDQUFDO1FBU1EsV0FBTSxHQUF5QixJQUFJLFlBQVksRUFBVSxDQUFDO0lBZXJCLENBQUM7SUE1Q2hELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN0RCxDQUFDO0lBRUQsZUFBZSxDQUFDLE1BQW1CO1FBQy9CLElBQUksRUFBRSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUMvQyxJQUFJLEVBQUUsS0FBSyxJQUFJO1lBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRCxTQUFTLENBQUMsS0FBb0I7UUFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDL0IsQ0FBQztJQUVELE9BQU8sQ0FBQyxLQUFvQjtRQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztJQUM5QixDQUFDO0lBTUQsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7SUFDUSxJQUFJLE1BQU0sQ0FBQyxDQUF1QjtRQUN2QyxJQUFHLENBQUMsRUFBQztZQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1NBQ3BCO0lBQ0wsQ0FBQztJQUtELElBQVksUUFBUTtRQUNoQixPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7SUFDN0QsQ0FBQztJQUNELElBQVksT0FBTztRQUNmLE9BQU8sSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQztJQUM1RCxDQUFDO0lBRUQsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekMsQ0FBQztJQUlNLFFBQVEsS0FBVyxDQUFDO0lBRXBCLE1BQU07UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUFFLE9BQU87U0FBRTtRQUM3QixJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO0lBQ25ELENBQUM7SUFFTSxPQUFPO1FBQ1YsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDakMsQ0FBQztJQUVNLE1BQU0sQ0FBQyxNQUErQixFQUFFLEtBQWlCO1FBQzVELEtBQUssQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1FBQ2pDLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUN6QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1lBQ2hDLE9BQU87U0FDVjtRQUNELElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUNoQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsTUFBTSxDQUFDO1NBQ3JDO2FBQU07WUFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDeEM7SUFDTCxDQUFDO0lBRU0sVUFBVSxDQUFDLE1BQStCO1FBQzdDLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixLQUFLLE1BQU0sQ0FBQztJQUMvQyxDQUFDO0lBRU0sVUFBVSxDQUFDLE1BQTJCLEVBQUUsTUFBZ0I7UUFDM0QsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ2YsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7SUFDOUIsQ0FBQztJQUVNLFlBQVksQ0FBQyxRQUFnQjtRQUNoQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBR08sZ0JBQWdCLENBQUMsTUFBMkI7UUFDaEQsTUFBTSxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUN6QixNQUFNLFdBQVcsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7WUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDOztZQUNwRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztJQUN0RixDQUFDO0lBRU8sWUFBWSxDQUFDLE1BQTJCO1FBQzVDLE1BQU0sSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDekIsTUFBTSxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUN2QyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQUUsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQzthQUNwRTtZQUNELDZDQUE2QztZQUM3QyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQzNHO0lBQ0wsQ0FBQztJQUVPLGNBQWMsQ0FBQyxJQUFZO1FBQy9CLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFTyxpQkFBaUIsQ0FBQyxJQUFZLEVBQUUsV0FBaUI7UUFDckQsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxFQUFFLENBQUM7UUFDcEUsSUFBSSxXQUFXLEVBQUU7WUFDYixJQUFJLE1BQU0sR0FBYSxFQUFFLENBQUM7WUFDMUIsS0FBSyxJQUFJLFFBQVEsSUFBSSxXQUFXLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUFFLFNBQVM7aUJBQUU7Z0JBQ3hELE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLElBQUksV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN2RDtZQUNELENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztTQUMvQjtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzlELENBQUM7Q0FDSixDQUFBOztZQTVFd0MsTUFBTTs7QUE1QzNDO0lBREMsV0FBVyxDQUFDLGlCQUFpQixDQUFDO3dEQUc5QjtBQUVEO0lBREMsWUFBWSxDQUFDLGdCQUFnQixFQUFFLENBQUMsZUFBZSxDQUFDLENBQUM7OERBSWpEO0FBRUQ7SUFEQyxZQUFZLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQzt3REFHNUM7QUFFRDtJQURDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3NEQUcxQztBQVNRO0lBQVIsS0FBSyxFQUFFO3FEQUlQO0FBQ1M7SUFBVCxNQUFNLEVBQUU7dURBQTJEO0FBaEMzRCx1QkFBdUI7SUFKbkMsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLHFCQUFxQjtRQUMvQiwwc0RBQStDO0tBQ2xELENBQUM7R0FDVyx1QkFBdUIsQ0EySG5DO1NBM0hZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBIb3N0QmluZGluZywgSW5wdXQsIE91dHB1dCwgT25Jbml0LCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IE5hdmlnYXRpb25NZW51Q29uZmlnLCBOYXZpZ2F0aW9uU2VjdGlvbkNvbmZpZywgTmF2aWdhYmxlSXRlbUNvbmZpZyB9IGZyb20gJy4uL2NvbnRyYWN0cy9uYXZpZ2F0aW9uJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdwYmktbmF2aWdhdGlvbi1tZW51JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9uYXZpZ2F0aW9uLW1lbnUuY29tcG9uZW50Lmh0bWwnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOYXZpZ2F0aW9uTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5jb2xsYXBzZWQnKVxyXG4gICAgZ2V0IGNvbGxhcHNlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jb25maWcgPyB0aGlzLmNvbmZpZy5jb2xsYXBzZWQgOiB0cnVlO1xyXG4gICAgfVxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6Y2xpY2snLCBbJyRldmVudC50YXJnZXQnXSlcclxuICAgIG9uRG9jdW1lbnRDbGljayh0YXJnZXQ6IEhUTUxFbGVtZW50KSB7XHJcbiAgICAgICAgbGV0IGVsID0gdGFyZ2V0LmNsb3Nlc3QoJ3BiaS1uYXZpZ2F0aW9uLW1lbnUnKTtcclxuICAgICAgICBpZiAoZWwgPT09IG51bGwpIHRoaXMuZGlzbWlzcygpO1xyXG4gICAgfVxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5ZG93bicsIFsnJGV2ZW50J10pXHJcbiAgICBvbktleURvd24oZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICB0aGlzLmtleWJvYXJkRXZlbnQgPSBldmVudDtcclxuICAgIH1cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleXVwJywgWyckZXZlbnQnXSlcclxuICAgIG9uS2V5VXAoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICB0aGlzLmtleWJvYXJkRXZlbnQgPSBudWxsO1xyXG4gICAgfVxyXG4gICAgcHJpdmF0ZSBfY29uZmlnOiBOYXZpZ2F0aW9uTWVudUNvbmZpZyA9IHtcclxuICAgICAgICBzaG93VG9nZ2xlQnV0dG9uOiB0cnVlLFxyXG4gICAgICAgIGNvbGxhcHNlZDogdHJ1ZSxcclxuICAgICAgICBzZWN0aW9uczogW11cclxuICAgIH07XHJcbiAgICBnZXQgY29uZmlnKCk6IE5hdmlnYXRpb25NZW51Q29uZmlne1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jb25maWc7XHJcbiAgICB9XHJcbiAgICBASW5wdXQoKSBzZXQgY29uZmlnKHY6IE5hdmlnYXRpb25NZW51Q29uZmlnKXtcclxuICAgICAgICBpZih2KXtcclxuICAgICAgICAgICAgdGhpcy5fY29uZmlnID0gdjtcclxuICAgICAgICB9XHJcbiAgICB9IFxyXG4gICAgQE91dHB1dCgpIGFjdGlvbjogRXZlbnRFbWl0dGVyPHN0cmluZz4gPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuICAgIHByaXZhdGUgYWN0aXZlU2VjdGlvbkNvbmZpZzogTmF2aWdhdGlvblNlY3Rpb25Db25maWc7XHJcbiAgICBwcml2YXRlIGtleWJvYXJkRXZlbnQ6IEtleWJvYXJkRXZlbnQ7XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgc2hpZnRLZXkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMua2V5Ym9hcmRFdmVudCAmJiB0aGlzLmtleWJvYXJkRXZlbnQuc2hpZnRLZXk7XHJcbiAgICB9XHJcbiAgICBwcml2YXRlIGdldCBjdHJsS2V5KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmtleWJvYXJkRXZlbnQgJiYgdGhpcy5rZXlib2FyZEV2ZW50LmN0cmxLZXk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IG5ld1RhYigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jdHJsS2V5IHx8IHRoaXMuc2hpZnRLZXk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByZWFkb25seSByb3V0ZXI6IFJvdXRlcikgeyB9XHJcblxyXG4gICAgcHVibGljIG5nT25Jbml0KCk6IHZvaWQgeyB9XHJcblxyXG4gICAgcHVibGljIHRvZ2dsZSgpOiB2b2lkIHtcclxuICAgICAgICBpZiAoIXRoaXMuY29uZmlnKSB7IHJldHVybjsgfVxyXG4gICAgICAgIHRoaXMuY29uZmlnLmNvbGxhcHNlZCA9ICF0aGlzLmNvbmZpZy5jb2xsYXBzZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGRpc21pc3MoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVTZWN0aW9uQ29uZmlnID0gbnVsbDtcclxuICAgICAgICB0aGlzLmNvbmZpZy5jb2xsYXBzZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBicm93c2UoY29uZmlnOiBOYXZpZ2F0aW9uU2VjdGlvbkNvbmZpZywgZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcclxuICAgICAgICBpZiAodGhpcy5pc0Jyb3dzaW5nKGNvbmZpZykpIHtcclxuICAgICAgICAgICAgdGhpcy5hY3RpdmVTZWN0aW9uQ29uZmlnID0gbnVsbDtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY29uZmlnLmNvbnRlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5hY3RpdmVTZWN0aW9uQ29uZmlnID0gY29uZmlnO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubmF2aWdhdGVUbyhjb25maWcsIHRoaXMubmV3VGFiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGlzQnJvd3NpbmcoY29uZmlnOiBOYXZpZ2F0aW9uU2VjdGlvbkNvbmZpZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFjdGl2ZVNlY3Rpb25Db25maWcgPT09IGNvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgbmF2aWdhdGVUbyhjb25maWc6IE5hdmlnYWJsZUl0ZW1Db25maWcsIG5ld1RhYj86IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmRpc21pc3MoKTtcclxuICAgICAgICBuZXdUYWIgPyB0aGlzLndpbmRvd05hdmlnYXRlVG8oY29uZmlnKSA6IHRoaXMubmdOYXZpZ2F0ZVRvKGNvbmZpZyk7XHJcbiAgICAgICAgdGhpcy5rZXlib2FyZEV2ZW50ID0gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaGFuZGxlQWN0aW9uKGFjdGlvbklkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmRpc21pc3MoKTtcclxuICAgICAgICB0aGlzLmFjdGlvbi5lbWl0KGFjdGlvbklkKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgcHJpdmF0ZSB3aW5kb3dOYXZpZ2F0ZVRvKGNvbmZpZzogTmF2aWdhYmxlSXRlbUNvbmZpZyk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGxpbmsgPSBjb25maWcubGluaztcclxuICAgICAgICBjb25zdCBxdWVyeVBhcmFtcyA9IGNvbmZpZy5xdWVyeVBhcmFtcztcclxuICAgICAgICBpZiAodGhpcy5pc0V4dGVybmFsTGluayhsaW5rKSkgdGhpcy5vcGVuTmV3QnJvd3NlclRhYihsaW5rLCBxdWVyeVBhcmFtcyk7XHJcbiAgICAgICAgZWxzZSB0aGlzLm9wZW5OZXdCcm93c2VyVGFiKHRoaXMuY29uZmlnLnVzZUhhc2ggPyBgIyR7bGlua31gIDogbGluaywgcXVlcnlQYXJhbXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbmdOYXZpZ2F0ZVRvKGNvbmZpZzogTmF2aWdhYmxlSXRlbUNvbmZpZyk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGxpbmsgPSBjb25maWcubGluaztcclxuICAgICAgICBjb25zdCBxdWVyeVBhcmFtcyA9IGNvbmZpZy5xdWVyeVBhcmFtcztcclxuICAgICAgICBpZiAodGhpcy5pc0V4dGVybmFsTGluayhsaW5rKSkgdGhpcy5vcGVuTmV3QnJvd3NlclRhYihsaW5rLCBxdWVyeVBhcmFtcyk7XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIC8vVE9ETzogcnVuIHZhbGlkYXRpb24gY2hlY2tzIGlmIGV2ZXIgbmVlZGVkLlxyXG4gICAgICAgICAgICBxdWVyeVBhcmFtcyA/IHRoaXMucm91dGVyLm5hdmlnYXRlKFtsaW5rXSwgeyBxdWVyeVBhcmFtczogcXVlcnlQYXJhbXMgfSkgOiB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbbGlua10pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzRXh0ZXJuYWxMaW5rKGxpbms6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBsaW5rLmluY2x1ZGVzKCdodHRwOi8vJykgfHwgbGluay5pbmNsdWRlcygnaHR0cHM6Ly8nKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9wZW5OZXdCcm93c2VyVGFiKGxpbms6IHN0cmluZywgcXVlcnlQYXJhbXM/OiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBsZXQgbCA9IHRoaXMuY29uZmlnLmhyZWYgPyBgJHt0aGlzLmNvbmZpZy5ocmVmfSR7bGlua31gIDogYCR7bGlua31gO1xyXG4gICAgICAgIGlmIChxdWVyeVBhcmFtcykge1xyXG4gICAgICAgICAgICBsZXQgcGFyYW1zOiBzdHJpbmdbXSA9IFtdO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBwcm9wZXJ0eSBpbiBxdWVyeVBhcmFtcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFxdWVyeVBhcmFtcy5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkpIHsgY29udGludWU7IH1cclxuICAgICAgICAgICAgICAgIHBhcmFtcy5wdXNoKGAke3Byb3BlcnR5fT0ke3F1ZXJ5UGFyYW1zW3Byb3BlcnR5XX1gKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBsICs9IGA/JHtwYXJhbXMuam9pbignJicpfWA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2hpZnRLZXkgPyB3aW5kb3cub3BlbihsKSA6IHdpbmRvdy5vcGVuKGwsICdfYmxhbmsnKTtcclxuICAgIH1cclxufSJdfQ==