import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter, OnInit, ElementRef, Renderer2, HostBinding, ViewChild } from '@angular/core';
let NavigationContentComponent = class NavigationContentComponent {
    constructor(el, renderer) {
        this.el = el;
        this.renderer = renderer;
        this.navigateTo = new EventEmitter();
        this.navigateToNewTab = new EventEmitter();
        this.activeItemConfig = null;
    }
    set config(v) {
        this._config = v;
        this.computeContentMaxHeight();
    }
    get config() {
        return this._config;
    }
    set top(v) {
        this.renderer.setStyle(this.el.nativeElement, 'top', `${v}px`);
        this.computeContentMaxHeight();
    }
    get browsing() {
        return !!this.activeItemConfig;
    }
    get contentTop() {
        return this._contentTop;
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.computeContentMaxHeight();
    }
    browse(config, event) {
        event.stopImmediatePropagation();
        if (this.isBrowsing(config)) {
            this.activeItemConfig = null;
            return;
        }
        if (config.content) {
            const target = event.target;
            const p = target.closest('.navigation-item');
            this._contentTop = p.offsetTop - this.items.nativeElement.scrollTop;
            this.activeItemConfig = config;
        }
        else {
            this.onNavigateTo(config);
        }
    }
    browseOrNavigate(config, event) {
        config.link ? this.onNavigateTo(config) : this.browse(config, event);
    }
    isBrowsing(config) {
        return this.activeItemConfig === config;
    }
    onNavigateTo(config, newTab) {
        this.activeItemConfig = null;
        newTab ? this.navigateToNewTab.emit(config) : this.navigateTo.emit(config);
    }
    computeContentMaxHeight() {
        if (!this.items) {
            return;
        }
        this.activeItemConfig = null;
        let top = this.el.nativeElement.offsetTop;
        let parent = this.el.nativeElement.offsetParent;
        while (parent) {
            top += parent.offsetTop;
            parent = parent.offsetParent;
        }
        const maxHeight = window.innerHeight - top;
        this.renderer.setStyle(this.items.nativeElement, 'max-height', `${maxHeight}px`);
    }
};
NavigationContentComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
__decorate([
    Input()
], NavigationContentComponent.prototype, "config", null);
__decorate([
    Input()
], NavigationContentComponent.prototype, "top", null);
__decorate([
    HostBinding('class.browsing')
], NavigationContentComponent.prototype, "browsing", null);
__decorate([
    Output()
], NavigationContentComponent.prototype, "navigateTo", void 0);
__decorate([
    Output()
], NavigationContentComponent.prototype, "navigateToNewTab", void 0);
__decorate([
    ViewChild('items')
], NavigationContentComponent.prototype, "items", void 0);
NavigationContentComponent = __decorate([
    Component({
        selector: 'pbi-navigation-content',
        template: "<div #items class=\"items\">\r\n    <div *ngFor=\"let groupConfig of config.groups\" class=\"navigation-group\">\r\n        <span *ngIf=\"groupConfig.title\">{{groupConfig.title}}</span>\r\n        <div *ngFor=\"let itemConfig of groupConfig.items\"  class=\"navigation-item\">\r\n            <div [title]=\"itemConfig.title\">\r\n                <div (click)=\"browseOrNavigate(itemConfig, $event)\">\r\n                    <i class=\"{{itemConfig.icon}} icon\"></i>\r\n                    <span class=\"text\">{{itemConfig.title}}</span>\r\n                </div>\r\n                <div>\r\n                    <div *ngIf=\"itemConfig.openNewTab\" (click)=\"onNavigateTo(itemConfig, true)\">\r\n                        <i class=\"fa fa-external-link icon\"></i>\r\n                    </div>\r\n                </div>\r\n                <div>\r\n                    <div *ngIf=\"itemConfig.content\" (click)=\"browse(itemConfig, $event)\">\r\n                        <i *ngIf=\"isBrowsing(itemConfig); else notBrowsing\" class=\"fa fa-angle-down icon\"></i>\r\n                        <ng-template #notBrowsing>\r\n                            <i class=\"fa fa-angle-right icon\"></i>\r\n                        </ng-template>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <ng-template [ngIf]=\"isBrowsing(itemConfig)\">\r\n                <pbi-navigation-content class=\"child\" \r\n                    [config]=\"itemConfig.content\" \r\n                    (navigateTo)=\"onNavigateTo($event)\" \r\n                    (navigateToNewTab)=\"onNavigateTo($event, true)\">\r\n                </pbi-navigation-content>\r\n            </ng-template>\r\n        </div>\r\n    </div>\r\n</div>\r\n<ng-template [ngIf]=\"activeItemConfig\">\r\n    <pbi-navigation-content \r\n        class=\"flyout\"\r\n        [config]=\"activeItemConfig.content\"\r\n        [top]=\"contentTop\" \r\n        (navigateTo)=\"onNavigateTo($event)\" \r\n        (navigateToNewTab)=\"onNavigateTo($event, true)\">\r\n    </pbi-navigation-content>\r\n</ng-template> "
    })
], NavigationContentComponent);
export { NavigationContentComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1jb250ZW50LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLWNvbnRlbnQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFROUgsSUFBYSwwQkFBMEIsR0FBdkMsTUFBYSwwQkFBMEI7SUEwQm5DLFlBQ1ksRUFBYyxFQUNMLFFBQW1CO1FBRDVCLE9BQUUsR0FBRixFQUFFLENBQVk7UUFDTCxhQUFRLEdBQVIsUUFBUSxDQUFXO1FBWDlCLGVBQVUsR0FBc0MsSUFBSSxZQUFZLEVBQXVCLENBQUM7UUFDeEYscUJBQWdCLEdBQXNDLElBQUksWUFBWSxFQUF1QixDQUFDO1FBRWpHLHFCQUFnQixHQUF5QixJQUFJLENBQUM7SUFRVCxDQUFDO0lBekJwQyxJQUFJLE1BQU0sQ0FBQyxDQUEwQjtRQUMxQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztRQUNqQixJQUFJLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztJQUNuQyxDQUFDO0lBQ0QsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFBO0lBQ3ZCLENBQUM7SUFDUSxJQUFJLEdBQUcsQ0FBQyxDQUFTO1FBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUM4QixJQUFJLFFBQVE7UUFDdkMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO0lBQ25DLENBQUM7SUFNRCxJQUFJLFVBQVU7UUFDVixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztJQU1NLFFBQVEsS0FBVyxDQUFDO0lBQ3BCLGVBQWU7UUFDbEIsSUFBSSxDQUFDLHVCQUF1QixFQUFFLENBQUM7SUFDbkMsQ0FBQztJQUVNLE1BQU0sQ0FBQyxNQUE0QixFQUFFLEtBQWlCO1FBQ3pELEtBQUssQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1FBQ2pDLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBQzdCLE9BQU87U0FDVjtRQUNELElBQUksTUFBTSxDQUFDLE9BQU8sRUFBRTtZQUNoQixNQUFNLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBcUIsQ0FBQztZQUMzQyxNQUFNLENBQUMsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFnQixDQUFDO1lBQzVELElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUM7WUFDcEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQU0sQ0FBQztTQUNsQzthQUFNO1lBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUM3QjtJQUNMLENBQUM7SUFFTSxnQkFBZ0IsQ0FBQyxNQUE0QixFQUFFLEtBQWlCO1FBQ25FLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3pFLENBQUM7SUFFTSxVQUFVLENBQUMsTUFBNEI7UUFDMUMsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssTUFBTSxDQUFDO0lBQzVDLENBQUM7SUFFTSxZQUFZLENBQUMsTUFBMkIsRUFBRSxNQUFnQjtRQUM3RCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDL0UsQ0FBQztJQUVPLHVCQUF1QjtRQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUFFLE9BQU87U0FBRTtRQUU1QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1FBQzdCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQztRQUMxQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDaEQsT0FBTyxNQUFNLEVBQUU7WUFDWCxHQUFHLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQztZQUN4QixNQUFNLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQztTQUNoQztRQUNELE1BQU0sU0FBUyxHQUFHLE1BQU0sQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO1FBQzNDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLFlBQVksRUFBRSxHQUFHLFNBQVMsSUFBSSxDQUFDLENBQUM7SUFDckYsQ0FBQztDQUNKLENBQUE7O1lBbERtQixVQUFVO1lBQ0ssU0FBUzs7QUF6Qi9CO0lBQVIsS0FBSyxFQUFFO3dEQUdQO0FBSVE7SUFBUixLQUFLLEVBQUU7cURBR1A7QUFDOEI7SUFBOUIsV0FBVyxDQUFDLGdCQUFnQixDQUFDOzBEQUU3QjtBQUNTO0lBQVQsTUFBTSxFQUFFOzhEQUF5RjtBQUN4RjtJQUFULE1BQU0sRUFBRTtvRUFBK0Y7QUFDcEY7SUFBbkIsU0FBUyxDQUFDLE9BQU8sQ0FBQzt5REFBMkI7QUFuQnJDLDBCQUEwQjtJQUp0QyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsd0JBQXdCO1FBQ2xDLHFqRUFBa0Q7S0FDckQsQ0FBQztHQUNXLDBCQUEwQixDQTZFdEM7U0E3RVksMEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIE9uSW5pdCwgRWxlbWVudFJlZiwgUmVuZGVyZXIyLCBIb3N0QmluZGluZywgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uQ29udGVudENvbmZpZywgTmF2aWdhdGlvbkl0ZW1Db25maWcsIE5hdmlnYWJsZUl0ZW1Db25maWcgfSBmcm9tICcuLi9jb250cmFjdHMvbmF2aWdhdGlvbic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAncGJpLW5hdmlnYXRpb24tY29udGVudCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbmF2aWdhdGlvbi1jb250ZW50LmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmF2aWdhdGlvbkNvbnRlbnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIHByaXZhdGUgX2NvbmZpZzogTmF2aWdhdGlvbkNvbnRlbnRDb25maWc7XHJcbiAgICBASW5wdXQoKSBzZXQgY29uZmlnKHY6IE5hdmlnYXRpb25Db250ZW50Q29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy5fY29uZmlnID0gdjtcclxuICAgICAgICB0aGlzLmNvbXB1dGVDb250ZW50TWF4SGVpZ2h0KCk7XHJcbiAgICB9XHJcbiAgICBnZXQgY29uZmlnKCk6IE5hdmlnYXRpb25Db250ZW50Q29uZmlnIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29uZmlnXHJcbiAgICB9XHJcbiAgICBASW5wdXQoKSBzZXQgdG9wKHY6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5lbC5uYXRpdmVFbGVtZW50LCAndG9wJywgYCR7dn1weGApO1xyXG4gICAgICAgIHRoaXMuY29tcHV0ZUNvbnRlbnRNYXhIZWlnaHQoKTtcclxuICAgIH1cclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MuYnJvd3NpbmcnKSBnZXQgYnJvd3NpbmcoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICEhdGhpcy5hY3RpdmVJdGVtQ29uZmlnO1xyXG4gICAgfVxyXG4gICAgQE91dHB1dCgpIG5hdmlnYXRlVG86IEV2ZW50RW1pdHRlcjxOYXZpZ2FibGVJdGVtQ29uZmlnPiA9IG5ldyBFdmVudEVtaXR0ZXI8TmF2aWdhYmxlSXRlbUNvbmZpZz4oKTtcclxuICAgIEBPdXRwdXQoKSBuYXZpZ2F0ZVRvTmV3VGFiOiBFdmVudEVtaXR0ZXI8TmF2aWdhYmxlSXRlbUNvbmZpZz4gPSBuZXcgRXZlbnRFbWl0dGVyPE5hdmlnYWJsZUl0ZW1Db25maWc+KCk7XHJcbiAgICBAVmlld0NoaWxkKCdpdGVtcycpIHByaXZhdGUgaXRlbXM6IEVsZW1lbnRSZWY7XHJcbiAgICBwdWJsaWMgYWN0aXZlSXRlbUNvbmZpZzogTmF2aWdhdGlvbkl0ZW1Db25maWcgPSBudWxsO1xyXG4gICAgcHJpdmF0ZSBfY29udGVudFRvcDogbnVtYmVyO1xyXG4gICAgZ2V0IGNvbnRlbnRUb3AoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29udGVudFRvcDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGVsOiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHByaXZhdGUgcmVhZG9ubHkgcmVuZGVyZXI6IFJlbmRlcmVyMikgeyB9XHJcblxyXG4gICAgcHVibGljIG5nT25Jbml0KCk6IHZvaWQgeyB9XHJcbiAgICBwdWJsaWMgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY29tcHV0ZUNvbnRlbnRNYXhIZWlnaHQoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYnJvd3NlKGNvbmZpZzogTmF2aWdhdGlvbkl0ZW1Db25maWcsIGV2ZW50OiBNb3VzZUV2ZW50KTogdm9pZCB7XHJcbiAgICAgICAgZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNCcm93c2luZyhjb25maWcpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWN0aXZlSXRlbUNvbmZpZyA9IG51bGw7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGNvbmZpZy5jb250ZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRhcmdldCA9IGV2ZW50LnRhcmdldCBhcyBIVE1MRWxlbWVudDtcclxuICAgICAgICAgICAgY29uc3QgcCA9IHRhcmdldC5jbG9zZXN0KCcubmF2aWdhdGlvbi1pdGVtJykgYXMgSFRNTEVsZW1lbnQ7XHJcbiAgICAgICAgICAgIHRoaXMuX2NvbnRlbnRUb3AgPSBwLm9mZnNldFRvcCAtIHRoaXMuaXRlbXMubmF0aXZlRWxlbWVudC5zY3JvbGxUb3A7XHJcbiAgICAgICAgICAgIHRoaXMuYWN0aXZlSXRlbUNvbmZpZyA9IGNvbmZpZztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm9uTmF2aWdhdGVUbyhjb25maWcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYnJvd3NlT3JOYXZpZ2F0ZShjb25maWc6IE5hdmlnYXRpb25JdGVtQ29uZmlnLCBldmVudDogTW91c2VFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIGNvbmZpZy5saW5rID8gdGhpcy5vbk5hdmlnYXRlVG8oY29uZmlnKSA6IHRoaXMuYnJvd3NlKGNvbmZpZywgZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBpc0Jyb3dzaW5nKGNvbmZpZzogTmF2aWdhdGlvbkl0ZW1Db25maWcpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hY3RpdmVJdGVtQ29uZmlnID09PSBjb25maWc7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uTmF2aWdhdGVUbyhjb25maWc6IE5hdmlnYWJsZUl0ZW1Db25maWcsIG5ld1RhYj86IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmFjdGl2ZUl0ZW1Db25maWcgPSBudWxsO1xyXG4gICAgICAgIG5ld1RhYiA/IHRoaXMubmF2aWdhdGVUb05ld1RhYi5lbWl0KGNvbmZpZykgOiB0aGlzLm5hdmlnYXRlVG8uZW1pdChjb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY29tcHV0ZUNvbnRlbnRNYXhIZWlnaHQoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCF0aGlzLml0ZW1zKSB7IHJldHVybjsgfVxyXG4gICAgICAgIFxyXG4gICAgICAgIHRoaXMuYWN0aXZlSXRlbUNvbmZpZyA9IG51bGw7XHJcbiAgICAgICAgbGV0IHRvcCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5vZmZzZXRUb3A7XHJcbiAgICAgICAgbGV0IHBhcmVudCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5vZmZzZXRQYXJlbnQ7XHJcbiAgICAgICAgd2hpbGUgKHBhcmVudCkge1xyXG4gICAgICAgICAgICB0b3AgKz0gcGFyZW50Lm9mZnNldFRvcDtcclxuICAgICAgICAgICAgcGFyZW50ID0gcGFyZW50Lm9mZnNldFBhcmVudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgbWF4SGVpZ2h0ID0gd2luZG93LmlubmVySGVpZ2h0IC0gdG9wO1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIuc2V0U3R5bGUodGhpcy5pdGVtcy5uYXRpdmVFbGVtZW50LCAnbWF4LWhlaWdodCcsIGAke21heEhlaWdodH1weGApO1xyXG4gICAgfVxyXG59Il19