import { __decorate } from "tslib";
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { SearchService } from '../../services/search.service';
let NavigationSectionComponent = class NavigationSectionComponent {
    constructor(searchService) {
        this.searchService = searchService;
        this.action = new EventEmitter();
        this.navigateTo = new EventEmitter();
        this.navigateToNewTab = new EventEmitter();
    }
    ;
    get contentConfig() {
        return this.config ?
            this.searchQuery ? this._filteredContentConfig : this.config.content :
            null;
    }
    get infoItems() {
        let items = [];
        if (this.config.informational && this.config.content.groups) {
            items = this.config.content.groups[0].items;
        }
        return items;
    }
    ngOnInit() { }
    search() {
        if (!this.searchQuery) {
            return;
        }
        let items = [];
        this.flattenTree(this.config.content, items);
        this._filteredContentConfig = {
            groups: [{
                    title: items.length > 0 ? 'results' : 'no matches',
                    items: items
                }]
        };
    }
    onActionClick(action) {
        this.action.emit(action.id);
    }
    onNavigateTo(config, newTab) {
        newTab ? this.navigateToNewTab.emit(config) : this.navigateTo.emit(config);
    }
    flattenTree(config, items) {
        config.groups.forEach(g => {
            g.items.forEach(i => {
                if (i.content) {
                    this.flattenTree(i.content, items);
                }
                else if (this.searchService.deepContains(i, this.searchQuery)) {
                    items.push(i);
                }
            });
        });
    }
};
NavigationSectionComponent.ctorParameters = () => [
    { type: SearchService }
];
__decorate([
    Input()
], NavigationSectionComponent.prototype, "config", void 0);
__decorate([
    Output()
], NavigationSectionComponent.prototype, "action", void 0);
__decorate([
    Output()
], NavigationSectionComponent.prototype, "navigateTo", void 0);
__decorate([
    Output()
], NavigationSectionComponent.prototype, "navigateToNewTab", void 0);
NavigationSectionComponent = __decorate([
    Component({
        selector: 'pbi-navigation-section',
        template: "<ng-template [ngIf]=\"config\">\r\n    <ng-template [ngIf]=\"config.searchable\">\r\n        <div class=\"search-box dark\">\r\n            <div class=\"form-group\" role=\"search\">\r\n                <input type=\"text\" class=\"form-control\" [(ngModel)]=\"searchQuery\" name=\"search\" (keyup)=\"search()\" autocomplete=\"off\" />\r\n            </div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template [ngIf]=\"config.actions\">\r\n        <div class=\"actions\">\r\n            <div>\r\n                <div *ngFor=\"let action of config.actions\" class=\"navigation-action\" (click)=\"onActionClick(action)\">\r\n                    <i [class]=\"action.icon\" [title]=\"action.title\"></i>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template [ngIf]=\"config.informational\" [ngIfElse]=\"navigationContent\">\r\n        <div class=\"info\">\r\n            <div *ngFor=\"let item of infoItems\">{{item.title}}</div>\r\n        </div>\r\n    </ng-template>\r\n    <ng-template #navigationContent>\r\n        <pbi-navigation-content [config]=\"contentConfig\" (navigateTo)=\"onNavigateTo($event)\" (navigateToNewTab)=\"onNavigateTo($event, true)\"></pbi-navigation-content>\r\n    </ng-template>\r\n</ng-template>"
    })
], NavigationSectionComponent);
export { NavigationSectionComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi1zZWN0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3BiaS1uZy8iLCJzb3VyY2VzIjpbImxpYi9jb3JlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLXNlY3Rpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQU05RCxJQUFhLDBCQUEwQixHQUF2QyxNQUFhLDBCQUEwQjtJQXlCbkMsWUFDcUIsYUFBNEI7UUFBNUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUF2QnZDLFdBQU0sR0FBeUIsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUMxRCxlQUFVLEdBQXNDLElBQUksWUFBWSxFQUF1QixDQUFDO1FBQ3hGLHFCQUFnQixHQUFzQyxJQUFJLFlBQVksRUFBdUIsQ0FBQztJQXNCeEcsQ0FBQztJQXhCbUUsQ0FBQztJQU1yRSxJQUFJLGFBQWE7UUFDYixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDO0lBQ2IsQ0FBQztJQUVELElBQUksU0FBUztRQUNULElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNmLElBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFDO1lBQ3ZELEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO1NBQzlDO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQVFNLFFBQVEsS0FBVSxDQUFDO0lBRW5CLE1BQU07UUFDVCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUFFLE9BQU87U0FBRTtRQUNsQyxJQUFJLEtBQUssR0FBMkIsRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLHNCQUFzQixHQUFHO1lBQzFCLE1BQU0sRUFBRSxDQUFDO29CQUNMLEtBQUssRUFBRSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZO29CQUNsRCxLQUFLLEVBQUUsS0FBSztpQkFDZixDQUFDO1NBQ0wsQ0FBQztJQUNOLENBQUM7SUFFTSxhQUFhLENBQUMsTUFBOEI7UUFDL0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFFTSxZQUFZLENBQUMsTUFBMkIsRUFBRSxNQUFnQjtRQUM3RCxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFTyxXQUFXLENBQUMsTUFBK0IsRUFBRSxLQUE2QjtRQUM5RSxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN0QixDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFO29CQUNYLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDdEM7cUJBQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUFDO29CQUMzRCxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsQjtZQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUosQ0FBQTs7WUFyQ3VDLGFBQWE7O0FBeEJ4QztJQUFSLEtBQUssRUFBRTswREFBaUM7QUFDL0I7SUFBVCxNQUFNLEVBQUU7MERBQTJEO0FBQzFEO0lBQVQsTUFBTSxFQUFFOzhEQUF5RjtBQUN4RjtJQUFULE1BQU0sRUFBRTtvRUFBK0Y7QUFML0YsMEJBQTBCO0lBSnRDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx3QkFBd0I7UUFDbEMsOHdDQUFrRDtLQUNyRCxDQUFDO0dBQ1csMEJBQTBCLENBK0R0QztTQS9EWSwwQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25TZWN0aW9uQ29uZmlnLCBOYXZpZ2F0aW9uQ29udGVudENvbmZpZywgTmF2aWdhdGlvbkFjdGlvbkNvbmZpZywgTmF2aWdhYmxlSXRlbUNvbmZpZywgTmF2aWdhdGlvbkl0ZW1Db25maWcgfSBmcm9tICcuLi9jb250cmFjdHMvbmF2aWdhdGlvbic7XHJcbmltcG9ydCB7IFNlYXJjaFNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9zZWFyY2guc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAncGJpLW5hdmlnYXRpb24tc2VjdGlvbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbmF2aWdhdGlvbi1zZWN0aW9uLmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmF2aWdhdGlvblNlY3Rpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGNvbmZpZzogTmF2aWdhdGlvblNlY3Rpb25Db25maWc7XHJcbiAgICBAT3V0cHV0KCkgYWN0aW9uOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpOztcclxuICAgIEBPdXRwdXQoKSBuYXZpZ2F0ZVRvOiBFdmVudEVtaXR0ZXI8TmF2aWdhYmxlSXRlbUNvbmZpZz4gPSBuZXcgRXZlbnRFbWl0dGVyPE5hdmlnYWJsZUl0ZW1Db25maWc+KCk7XHJcbiAgICBAT3V0cHV0KCkgbmF2aWdhdGVUb05ld1RhYjogRXZlbnRFbWl0dGVyPE5hdmlnYWJsZUl0ZW1Db25maWc+ID0gbmV3IEV2ZW50RW1pdHRlcjxOYXZpZ2FibGVJdGVtQ29uZmlnPigpO1xyXG5cclxuICAgIHByaXZhdGUgX2ZpbHRlcmVkQ29udGVudENvbmZpZzogTmF2aWdhdGlvbkNvbnRlbnRDb25maWc7XHJcblxyXG4gICAgZ2V0IGNvbnRlbnRDb25maWcoKTogTmF2aWdhdGlvbkNvbnRlbnRDb25maWcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNvbmZpZyA/XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoUXVlcnkgPyB0aGlzLl9maWx0ZXJlZENvbnRlbnRDb25maWcgOiB0aGlzLmNvbmZpZy5jb250ZW50IDpcclxuICAgICAgICAgICAgbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaW5mb0l0ZW1zKCk6IE5hdmlnYXRpb25JdGVtQ29uZmlnW117XHJcbiAgICAgICAgbGV0IGl0ZW1zID0gW107XHJcbiAgICAgICAgaWYodGhpcy5jb25maWcuaW5mb3JtYXRpb25hbCAmJiB0aGlzLmNvbmZpZy5jb250ZW50Lmdyb3Vwcyl7XHJcbiAgICAgICAgICAgIGl0ZW1zID0gdGhpcy5jb25maWcuY29udGVudC5ncm91cHNbMF0uaXRlbXNcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGl0ZW1zO1xyXG4gICAgfVxyXG5cclxuICAgIHNlYXJjaFF1ZXJ5OiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSByZWFkb25seSBzZWFyY2hTZXJ2aWNlOiBTZWFyY2hTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG5nT25Jbml0KCk6IHZvaWQge31cclxuXHJcbiAgICBwdWJsaWMgc2VhcmNoKCk6IHZvaWQge1xyXG4gICAgICAgIGlmICghdGhpcy5zZWFyY2hRdWVyeSkgeyByZXR1cm47IH1cclxuICAgICAgICBsZXQgaXRlbXM6IE5hdmlnYXRpb25JdGVtQ29uZmlnW10gPSBbXTtcclxuICAgICAgICB0aGlzLmZsYXR0ZW5UcmVlKHRoaXMuY29uZmlnLmNvbnRlbnQsIGl0ZW1zKTtcclxuICAgICAgICB0aGlzLl9maWx0ZXJlZENvbnRlbnRDb25maWcgPSB7XHJcbiAgICAgICAgICAgIGdyb3VwczogW3tcclxuICAgICAgICAgICAgICAgIHRpdGxlOiBpdGVtcy5sZW5ndGggPiAwID8gJ3Jlc3VsdHMnIDogJ25vIG1hdGNoZXMnLFxyXG4gICAgICAgICAgICAgICAgaXRlbXM6IGl0ZW1zXHJcbiAgICAgICAgICAgIH1dXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgb25BY3Rpb25DbGljayhhY3Rpb246IE5hdmlnYXRpb25BY3Rpb25Db25maWcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmFjdGlvbi5lbWl0KGFjdGlvbi5pZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIG9uTmF2aWdhdGVUbyhjb25maWc6IE5hdmlnYWJsZUl0ZW1Db25maWcsIG5ld1RhYj86IGJvb2xlYW4pIHtcclxuICAgICAgICBuZXdUYWIgPyB0aGlzLm5hdmlnYXRlVG9OZXdUYWIuZW1pdChjb25maWcpIDogdGhpcy5uYXZpZ2F0ZVRvLmVtaXQoY29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGZsYXR0ZW5UcmVlKGNvbmZpZzogTmF2aWdhdGlvbkNvbnRlbnRDb25maWcsIGl0ZW1zOiBOYXZpZ2F0aW9uSXRlbUNvbmZpZ1tdKTogdm9pZCB7XHJcbiAgICAgICAgY29uZmlnLmdyb3Vwcy5mb3JFYWNoKGcgPT4ge1xyXG4gICAgICAgICAgICBnLml0ZW1zLmZvckVhY2goaSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoaS5jb250ZW50KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mbGF0dGVuVHJlZShpLmNvbnRlbnQsIGl0ZW1zKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zZWFyY2hTZXJ2aWNlLmRlZXBDb250YWlucyhpLCB0aGlzLnNlYXJjaFF1ZXJ5KSl7XHJcbiAgICAgICAgICAgICAgICAgICAgIGl0ZW1zLnB1c2goaSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufSJdfQ==